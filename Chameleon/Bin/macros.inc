;----------High-Level Call
;Usage:
;   ccall FuncName, param1, param2, param 3...
;   pcall FuncName, param1, param2, param 3...
;   stdcall FuncName, param1, param2, param 3...
;   fastcall FuncName, param1, param2, param 3...
%macro pcall 2-9 ;Pascal Call: push first value first, no cleanup
%define _j %1
%rep %0-1
    %rotate 1
    push %1
%endrep
    call _j
%endmacro

%macro ccall 2-9 ;C Call: push last value first, cleanup
%define _j %1
%assign __params %0-1
%rep %0-1
    %rotate -1
    push %1
%endrep
    call _j
    %assign __params __params * 4
    add esp, __params
%endmacro

%macro stdcall 2-9 ;Windows StdCall: push last value first, no cleanup
%define _j %1
%rep %0-1
    %rotate -1
    push %1
%endrep
    call _j
%endmacro

%macro fastcall 2-9 ;Borland FastCall: use registers, no cleanup
%define _j %1
%assign __pnum 1
%rep %0-4
    %rotate -1
    %if __pnum = 1
     mov eax, %1
    %elif __pnum = 2
     mov edx, %1
    %elif __pnum = 3
     mov ebx, %1
    %else
     push %1
    %endif
    %assign __pnum __pnum+1
%endrep
    call _j
%endmacro

%imacro INVOKE 0-100
%push invoke
	%assign %$args 0

%ifdef __stdcall_defined_%1
	%define %1 __stdcall_defined_%1
	extern __stdcall_defined_%1
%endif

%ifdef __cdecl_defined_%1
	declare_cdecl %1
	%define %1 __cdecl_defined_%1
	extern __cdecl_defined_%1
	declare_cdecl %1
%endif

	%ifdef _UNDERSCORE_
		%define _proc _%1
	%else
		%define _proc %1
	%endif
	%rep %0-1
		%rotate -1
		%ifstr	%1

			jmp	%%endstr_%$args
			%%str_%$args:	db %1, 0
			%%endstr_%$args:
			push	dword %%str_%$args
		%else
			push	dword %1
		%endif
		%assign %$args %$args+4
	%endrep
	call	_proc
	%if %$args <> 0
	%rotate -1
	%ifdef %1_defined
			add	esp, %$args
		%endif
	%endif
	%undef	_proc

%pop
%endmacro

%imacro PROC 1-100
%push proc
	%assign %$arg 8
	%ifdef __entry_%1
		%ifndef __PROCEDURE__
			%ifdef _UNDERSCORE_
				global	_main
				_main:
			%else
				global main
				main:
			%endif
			%define __PROCEDURE__ _end_%1
			%define _entry_
			%define __leave_present__
		%else
			%error "missing endproc directive."
		%endif
	%else
		%ifndef __PROCEDURE__
			%ifdef _UNDERSCORE_
				_%1:
			%else
				%1:
			%endif
			push ebp
			mov  ebp, esp
			%define __PROCEDURE__ _end_%1
			%rep %0-1
				%rotate 1
				%1 argd
			%endrep
		%else
			%error "missing endproc directive."
		%endif
	%endif
%endmacro

%imacro ARGD 0-1 4
	%ifdef __PROCEDURE__
		%00 equ %$arg
		%assign %$arg %1+%$arg
	%else
		%error "arguments must be defined within a proc/endproc block."
	%endif
%endmacro

