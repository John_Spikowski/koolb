 10    '
 20    '   Microsoft RemLine - Line Number Removal Utility
 30    '   Copyright (C) Microsoft Corporation 1985-1990
 40    '
 50    '   REMLINE.BAS is a program to remove line numbers from Microsoft Basic
 60    '   Programs. It removes only those line numbers that are not the object
 70    '   of one of the following statements: GOSUB, RETURN, GOTO, THEN, ELSE,
 80    '   RESUME, RESTORE, or RUN.
 90    '
 100    '   When REMLINE is run, it will ask for the name of the file to be
 110    '   processed and the name of the file or device to receive the
 120    '   reformatted output. If no extension is given, .BAS is assumed (except
 130    '   for output devices). If filenames are not given, REMLINE prompts for
 140    '   file names. If both filenames are the same, REMLINE saves the original
 150    '   file with the extension .BAK.
 160    '
 170    '   REMLINE makes several assumptions about the program:
 180    '
 190    '     1. It must be correct syntactically, and must run in BASICA or
 200    '        GW-BASIC interpreter.
 210    '     2. There is a 400 line limit. To process larger files, change
 220    '        MaxLines constant.
 230    '     3. The first number encountered on a line is considered a line
 240    '        number; thus some continuation lines (in a compiler-specific
 250    '        construction) may not be handled correctly.
 260    '     4. REMLINE can handle simple statements that test the ERL function
 270    '        using  relational operators such as =, <, and >. For example,
 280    '        the following statement is handled correctly:
 290    '
 300    '             IF ERL = 100 THEN END
 310    '
 320    '        Line 100 is not removed from the source code. However, more
 330    '        complex expressions that contain the +, -, AND, OR, XOR, EQV,
 340    '        MOD, or IMP operators may not be handled correctly. For example,
 350    '        in the following statement REMLINE does not recognize line 105
 360    '        as a referenced line number and removes it from the source code:
 370    '
 380    '             IF ERL + 5 = 105 THEN END
 390    '
 400    '   If you do not like the way REMLINE formats its output, you can modify
 410    '   the output lines in SUB GenOutFile. An example is shown in comments.
 420    DEFINT A-Z
 430    
 440    ' Function and Subprocedure declarations
 450    DECLARE FUNCTION GetToken$ (Search$, Delim$)
 460    DECLARE FUNCTION StrSpn% (InString$, Separator$)
 470    DECLARE FUNCTION StrBrk% (InString$, Separator$)
 480    DECLARE FUNCTION IsDigit% (Char$)
 490    DECLARE SUB GetFileNames ()
 500    DECLARE SUB BuildTable ()
 510    DECLARE SUB GenOutFile ()
 520    DECLARE SUB InitKeyTable ()
 530    
 540    ' Global and constant data
 550    CONST TRUE = -1
 560    CONST false = 0
 570    CONST MaxLines = 400
 580    
 590    DIM SHARED LineTable!(MaxLines)
 600    DIM SHARED LineCount
 610    DIM SHARED Seps$, InputFile$, OutputFile$, TmpFile$
 620    
 630    ' Keyword search data
 640    CONST KeyWordCount = 9
 650    DIM SHARED KeyWordTable$(KeyWordCount)
 660    
 670    KeyData:
 680       DATA THEN, ELSE, GOSUB, GOTO, RESUME, RETURN, RESTORE, RUN, ERL, ""
 690    
 700    ' Start of module-level program code
 710       Seps$ = " ,:=<>()" + CHR$(9)
 720       InitKeyTable
 730       GetFileNames
 740       ON ERROR GOTO FileErr1
 750       OPEN InputFile$ FOR INPUT AS 1
 760       ON ERROR GOTO 0
 770       COLOR 7: PRINT "Working"; : COLOR 23: PRINT " . . .": COLOR 7: PRINT
 780       BuildTable
 790       CLOSE #1
 800       OPEN InputFile$ FOR INPUT AS 1
 810       ON ERROR GOTO FileErr2
 820       OPEN OutputFile$ FOR OUTPUT AS 2
 830       ON ERROR GOTO 0
 840       GenOutFile
 850       CLOSE #1, #2
 860       IF OutputFile$ <> "CON" THEN CLS
 870    
 880    END
 890    
 900    FileErr1:
 910       CLS
 920       PRINT "      Invalid file name": PRINT
 930       INPUT "      New input file name (ENTER to terminate): ", InputFile$
 940       IF InputFile$ = "" THEN END
 950    FileErr2:
 960       INPUT "      Output file name (ENTER to print to screen) :", OutputFile$
 970       PRINT
 980       IF (OutputFile$ = "") THEN OutputFile$ = "CON"
 990       IF TmpFile$ = "" THEN
 1000          RESUME
 1010       ELSE
 1020          TmpFile$ = ""
 1030          RESUME NEXT
 1040       END IF
 1050    
 1060    '
 1070    ' BuildTable:
 1080    '   Examines the entire text file looking for line numbers that are
 1090    '   the object of GOTO, GOSUB, etc. As each is found, it is entered
 1100    '   into a table of line numbers. The table is used during a second
 1110    '   pass (see GenOutFile), when all line numbers not in the list
 1120    '   are removed.
 1130    ' Input:
 1140    '   Uses globals KeyWordTable$, KeyWordCount, and Seps$
 1150    ' Output:
 1160    '   Modifies LineTable! and LineCount
 1170    '
 1180    SUB BuildTable STATIC
 1190    
 1200       DO WHILE NOT EOF(1)
 1210          ' Get line and first token
 1220          LINE INPUT #1, InLin$
 1230          Token$ = GetToken$(InLin$, Seps$)
 1240          DO WHILE (Token$ <> "")
 1250             FOR KeyIndex = 1 TO KeyWordCount
 1260                ' See if token is keyword
 1270                IF (KeyWordTable$(KeyIndex) = UCASE$(Token$)) THEN
 1280                   ' Get possible line number after keyword
 1290                   Token$ = GetToken$("", Seps$)
 1300                   ' Check each token to see if it is a line number
 1310                   ' (the LOOP is necessary for the multiple numbers
 1320                   ' of ON GOSUB or ON GOTO). A non-numeric token will
 1330                   ' terminate search.
 1340                   DO WHILE (IsDigit(LEFT$(Token$, 1)))
 1350                      LineCount = LineCount + 1
 1360                      LineTable!(LineCount) = VAL(Token$)
 1370                      Token$ = GetToken$("", Seps$)
 1380                      IF Token$ <> "" THEN KeyIndex = 0
 1390                   LOOP
 1400                END IF
 1410             NEXT KeyIndex
 1420             ' Get next token
 1430             Token$ = GetToken$("", Seps$)
 1440          LOOP
 1450       LOOP
 1460    
 1470    END SUB
 1480    
 1490    '
 1500    ' GenOutFile:
 1510    '  Generates an output file with unreferenced line numbers removed.
 1520    ' Input:
 1530    '  Uses globals LineTable!, LineCount, and Seps$
 1540    ' Output:
 1550    '  Processed file
 1560    '
 1570    SUB GenOutFile STATIC
 1580    
 1590       ' Speed up by eliminating comma and colon (can't separate first token)
 1600       Sep$ = " " + CHR$(9)
 1610       DO WHILE NOT EOF(1)
 1620          LINE INPUT #1, InLin$
 1630          IF (InLin$ <> "") THEN
 1640             ' Get first token and process if it is a line number
 1650             Token$ = GetToken$(InLin$, Sep$)
 1660             IF IsDigit(LEFT$(Token$, 1)) THEN
 1670                LineNumber! = VAL(Token$)
 1680                FoundNumber = false
 1690                ' See if line number is in table of referenced line numbers
 1700                FOR index = 1 TO LineCount
 1710                   IF (LineNumber! = LineTable!(index)) THEN
 1720                      FoundNumber = TRUE
 1730                   END IF
 1740                NEXT index
 1750                ' Modify line strings
 1760                IF (NOT FoundNumber) THEN
 1770                   Token$ = SPACE$(LEN(Token$))
 1780                   MID$(InLin$, StrSpn(InLin$, Sep$), LEN(Token$)) = Token$
 1790                END IF
 1800                  
 1810                ' You can replace the previous lines with your own
 1820                ' code to reformat output. For example, try these lines:
 1830                   
 1840                'TmpPos1 = StrSpn(InLin$, Sep$) + LEN(Token$)
 1850                'TmpPos2 = TmpPos1 + StrSpn(MID$(InLin$, TmpPos1), Sep$)
 1860                '
 1870                'IF FoundNumber THEN
 1880                '   InLin$ = LEFT$(InLin$, TmpPos1 - 1) + CHR$(9) + MID$(InLin$, TmpPos2)
 1890                'ELSE
 1900                '   InLin$ = CHR$(9) + MID$(InLin$, TmpPos2)
 1910                'END IF
 1920    
 1930             END IF
 1940          END IF
 1950          ' Print line to file or console (PRINT is faster than console device)
 1960          IF OutputFile$ = "CON" THEN
 1970             PRINT InLin$
 1980          ELSE
 1990             PRINT #2, InLin$
 2000          END IF
 2010       LOOP
 2020    
 2030    END SUB
 2040    
 2050    '
 2060    ' GetFileNames:
 2070    '  Gets a file name by prompting the user.
 2080    ' Input:
 2090    '  User input
 2100    ' Output:
 2110    '  Defines InputFiles$ and OutputFiles$
 2120    '
 2130    SUB GetFileNames STATIC
 2140    
 2150        CLS
 2160        PRINT " Microsoft RemLine: Line Number Removal Utility"
 2170        PRINT "       (.BAS assumed if no extension given)"
 2180        PRINT
 2190        INPUT "      Input file name (ENTER to terminate): ", InputFile$
 2200        IF InputFile$ = "" THEN END
 2210        INPUT "      Output file name (ENTER to print to screen): ", OutputFile$
 2220        PRINT
 2230        IF (OutputFile$ = "") THEN OutputFile$ = "CON"
 2240    
 2250       IF INSTR(InputFile$, ".") = 0 THEN
 2260          InputFile$ = InputFile$ + ".BAS"
 2270       END IF
 2280    
 2290       IF INSTR(OutputFile$, ".") = 0 THEN
 2300          SELECT CASE OutputFile$
 2310             CASE "CON", "SCRN", "PRN", "COM1", "COM2", "LPT1", "LPT2", "LPT3"
 2320                EXIT SUB
 2330             CASE ELSE
 2340                OutputFile$ = OutputFile$ + ".BAS"
 2350          END SELECT
 2360       END IF
 2370    
 2380       DO WHILE InputFile$ = OutputFile$
 2390          TmpFile$ = LEFT$(InputFile$, INSTR(InputFile$, ".")) + "BAK"
 2400          ON ERROR GOTO FileErr1
 2410          NAME InputFile$ AS TmpFile$
 2420          ON ERROR GOTO 0
 2430          IF TmpFile$ <> "" THEN InputFile$ = TmpFile$
 2440       LOOP
 2450    
 2460    END SUB
 2470    
 2480    '
 2490    ' GetToken$:
 2500    '  Extracts tokens from a string. A token is a word that is surrounded
 2510    '  by separators, such as spaces or commas. Tokens are extracted and
 2520    '  analyzed when parsing sentences or commands. To use the GetToken$
 2530    '  function, pass the string to be parsed on the first call, then pass
 2540    '  a null string on subsequent calls until the function returns a null
 2550    '  to indicate that the entire string has been parsed.
 2560    ' Input:
 2570    '  Search$ = string to search
 2580    '  Delim$  = String of separators
 2590    ' Output:
 2600    '  GetToken$ = next token
 2610    '
 2620    FUNCTION GetToken$ (Search$, Delim$) STATIC
 2630    
 2640       ' Note that SaveStr$ and BegPos must be static from call to call
 2650       ' (other variables are only static for efficiency).
 2660       ' If first call, make a copy of the string
 2670       IF (Search$ <> "") THEN
 2680          BegPos = 1
 2690          SaveStr$ = Search$
 2700       END IF
 2710      
 2720       ' Find the start of the next token
 2730       NewPos = StrSpn(MID$(SaveStr$, BegPos, LEN(SaveStr$)), Delim$)
 2740       IF NewPos THEN
 2750          ' Set position to start of token
 2760          BegPos = NewPos + BegPos - 1
 2770       ELSE
 2780          ' If no new token, quit and return null
 2790          GetToken$ = ""
 2800          EXIT FUNCTION
 2810       END IF
 2820    
 2830       ' Find end of token
 2840       NewPos = StrBrk(MID$(SaveStr$, BegPos, LEN(SaveStr$)), Delim$)
 2850       IF NewPos THEN
 2860          ' Set position to end of token
 2870          NewPos = BegPos + NewPos - 1
 2880       ELSE
 2890          ' If no end of token, return set to end a value
 2900          NewPos = LEN(SaveStr$) + 1
 2910       END IF
 2920       ' Cut token out of search string
 2930       GetToken$ = MID$(SaveStr$, BegPos, NewPos - BegPos)
 2940       ' Set new starting position
 2950       BegPos = NewPos
 2960    
 2970    END FUNCTION
 2980    
 2990    '
 3000    ' InitKeyTable:
 3010    '  Initializes a keyword table. Keywords must be recognized so that
 3020    '  line numbers can be distinguished from numeric constants.
 3030    ' Input:
 3040    '  Uses KeyData
 3050    ' Output:
 3060    '  Modifies global array KeyWordTable$
 3070    '
 3080    SUB InitKeyTable STATIC
 3090    
 3100       RESTORE KeyData
 3110       FOR Count = 1 TO KeyWordCount
 3120          READ KeyWord$
 3130          KeyWordTable$(Count) = KeyWord$
 3140       NEXT
 3150    
 3160    END SUB
 3170    
 3180    '
 3190    ' IsDigit:
 3200    '  Returns true if character passed is a decimal digit. Since any
 3210    '  Basic token starting with a digit is a number, the function only
 3220    '  needs to check the first digit. Doesn't check for negative numbers,
 3230    '  but that's not needed here.
 3240    ' Input:
 3250    '  Char$ - initial character of string to check
 3260    ' Output:
 3270    '  IsDigit - true if within 0 - 9
 3280    '
 3290    FUNCTION IsDigit (Char$) STATIC
 3300    
 3310       IF (Char$ = "") THEN
 3320          IsDigit = false
 3330       ELSE
 3340          CharAsc = ASC(Char$)
 3350          IsDigit = (CharAsc >= ASC("0")) AND (CharAsc <= ASC("9"))
 3360       END IF
 3370    
 3380    END FUNCTION
 3390    
 3400    '
 3410    ' StrBrk:
 3420    '  Searches InString$ to find the first character from among those in
 3430    '  Separator$. Returns the index of that character. This function can
 3440    '  be used to find the end of a token.
 3450    ' Input:
 3460    '  InString$ = string to search
 3470    '  Separator$ = characters to search for
 3480    ' Output:
 3490    '  StrBrk = index to first match in InString$ or 0 if none match
 3500    '
 3510    FUNCTION StrBrk (InString$, Separator$) STATIC
 3520    
 3530       Ln = LEN(InString$)
 3540       BegPos = 1
 3550       ' Look for end of token (first character that is a delimiter).
 3560       DO WHILE INSTR(Separator$, MID$(InString$, BegPos, 1)) = 0
 3570          IF BegPos > Ln THEN
 3580             StrBrk = 0
 3590             EXIT FUNCTION
 3600          ELSE
 3610             BegPos = BegPos + 1
 3620          END IF
 3630       LOOP
 3640       StrBrk = BegPos
 3650      
 3660    END FUNCTION
 3670    
 3680    '
 3690    ' StrSpn:
 3700    '  Searches InString$ to find the first character that is not one of
 3710    '  those in Separator$. Returns the index of that character. This
 3720    '  function can be used to find the start of a token.
 3730    ' Input:
 3740    '  InString$ = string to search
 3750    '  Separator$ = characters to search for
 3760    ' Output:
 3770    '  StrSpn = index to first nonmatch in InString$ or 0 if all match
 3780    '
 3790    FUNCTION StrSpn% (InString$, Separator$) STATIC
 3800    
 3810       Ln = LEN(InString$)
 3820       BegPos = 1
 3830       ' Look for start of a token (character that isn't a delimiter).
 3840       DO WHILE INSTR(Separator$, MID$(InString$, BegPos, 1))
 3850          IF BegPos > Ln THEN
 3860             StrSpn = 0
 3870             EXIT FUNCTION
 3880          ELSE
 3890             BegPos = BegPos + 1
 3900          END IF
 3910       LOOP
 3920       StrSpn = BegPos
 3930    
 3940    END FUNCTION
 3950    
