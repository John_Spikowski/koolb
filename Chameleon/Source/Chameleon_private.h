/* THIS FILE WILL BE OVERWRITTEN BY DEV-C++ */
/* DO NOT EDIT ! */

#ifndef CHAMELEON_PRIVATE_H
#define CHAMELEON_PRIVATE_H

/* VERSION DEFINITIONS */
#define VER_STRING	"0.0.1.24"
#define VER_MAJOR	0
#define VER_MINOR	0
#define VER_RELEASE	1
#define VER_BUILD	24
#define COMPANY_NAME	"Chris Morningstar"
#define FILE_VERSION	"1"
#define FILE_DESCRIPTION	"Chameleon Compiler"
#define INTERNAL_NAME	""
#define LEGAL_COPYRIGHT	"None"
#define LEGAL_TRADEMARKS	"None"
#define ORIGINAL_FILENAME	"Chameleon.exe"
#define PRODUCT_NAME	"Chameleon"
#define PRODUCT_VERSION	"1"

#endif /*CHAMELEON_PRIVATE_H*/
