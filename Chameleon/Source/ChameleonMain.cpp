/******************************************************************************
 *|--------------------------------------------------------------------------|*
 *|                                 Chameleon Compiler                       |*
 *|Original By Brian C. Becker, adapted and re-purposed by Chris Morningstar |*
 *|                                                                          |*
 *| Main.cpp - Controls all the other files and starts everything            |*
 *|--------------------------------------------------------------------------|*
 *****************************************************************************/

// Disables the old MS Visual C++ 6 warnings about debugging symbols exceeding 255 chars
#pragma warning(disable:4786)

void CompilerExit(int nReturnCode);

//Include the necessary libraries
#include <string> 
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <map>
#include <algorithm>
using namespace std;

//Set the OS version
#define Windows
//#define Linux

#ifdef Windows
#include <windows.h>
#endif

//Available app types
enum AppType{GUI, Console, DLL};

//Directive defaults
int  AppType  = Console;
bool Optimize = false;
bool Compress = false;
bool Mangle   = true;
bool Pause = false;
bool XPStyle = false;

//Any need to run the resource compiler?
bool UseGoRc = false;

//Globals to keep track of the compile time
double StartTime;
double TempTime;

//Path where Chameleon was run from.
string ExePath;
//Include directory default.
string IncPath;

//Include the rest of our modules and objects
#include "Misc.h"
#include "Read.h"
  Reading Read;
#include "Database.h"
  Database Data;
#include "Write.h"
  Writing Write;
#include "Assembly.h"
  Assembly Asm;
#include "Errors.h"
  Errors Error;
#include "Compiler.h"
  Compiler CompileIt;

//Our core routines for compiling
void Start();
void Compile(int argc, char *argv[]);
void Stop();

/******************************************************************************
main - prints out the welcome and then starts the ball rolling
******************************************************************************/
int main(int argc, char *argv[]){
  printf("\n          Welcome to Chameleon BASIC\r\n");
  #ifdef Windows
 // printf("\n          Your open-source Windows BASIC compiler!\n\r\n");
  #endif
  #ifdef Linux
  printf("\n           Your open-source Linux BASIC compiler!\n\r\n");
  #endif
  Start();
  Compile(argc, argv);
  Stop();
  CompilerExit(0);
}

/******************************************************************************
Start - keeps track of the time needed to compile
******************************************************************************/
void Start(){
  StartTime = clock();
  TempTime  = StartTime;
  return;
}

/******************************************************************************
Compile - gets the file to compile, open it, passes it off to the CompileIt 
object, and then assembles and links the app.
******************************************************************************/
void Compile(int argc, char *argv[]){
  string FileName;
  string TargetOS;
 
 if (argc >= 2) {
    FileName = argv[1];
    IncPath = "";
}
 if (argc == 4){ //Yeah, for now it has to be the third arg.
    IncPath = argv[2];
    if (0 == stricmp(argv[3], "pause"))
      Pause = true;
    else { 
      printf("Usage: Chameleon <filename> [IncPath] [Pause]\n");
      printf("Arguments Given:\n");
      printf("0. %s \n", argv[0]);
      printf("1. %s \n", argv[1]);
      printf("2. %s \n", argv[2]);
      printf("3. %s \n", argv[3]);
      printf("4. %s \n", argv[4]);
      Pause = true;
      CompilerExit(1);
    }
  }
  else if (argc == 3){
    IncPath = argv[2];
  }
  else {       
      printf("Usage: Chameleon <filename> [IncPath] [Pause]\n");
      Pause = true;
      CompilerExit(1);
  }

  //Where are we running from?
  char strPathAndFile[MAX_PATH];
  GetModuleFileName(NULL, strPathAndFile, MAX_PATH);
  ExePath = GetPathOnly(strPathAndFile);

  //Get the Reading object to open the file
  Read.OpenBook(FileName);
  printf("Currently compiling \"%s\":\r\n", FileName.c_str());
  //Tell the CompileIt object to generate the assembly language
  CompileIt.Compile(Read, false);
  //Clean up the assembly language
  Asm.FinishUp();
  //The compile time is....<drum roll, please>
  printf(" - Compile time  ->  %f seconds\r\n", (double)(clock() - TempTime) / 
                                            (double)CLK_TCK);
  TempTime = clock();
  //Assemble and link the app
  Write.BuildApp(FileName);
  return;
}

/******************************************************************************
Stop - prints out the total time necessary for compiling, assembling, & linking
******************************************************************************/
void Stop(){
  printf("   -------------------------------\r\n");
  printf(" - Total time    ->  %f seconds\r\n", (double)(clock() - StartTime) / 
                                            (double)CLOCKS_PER_SEC);
  return;
}


