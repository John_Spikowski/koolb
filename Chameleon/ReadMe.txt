This project was adopted from KoolB 16.  An abandoned project on yahoo.  It's kind of funny because this project was almost ahead of rapid Q.  I've not recorded my changes in this file, but will shortly.

Changes:

* Implemented "Byte" data type.  This one took
  took awhile.  

* Implemented "Word" data type.  This one didn't
  take as long.  Just glad I didn't try to do
  both of them at once.

  Remember, these are unsigned in the scope of
  things and will happily accept a negative number.
  For example, by = -1 and then Print by, would
  result int the value 255 being printed.

* CreateWindowEx hMenu problem solved.  This was
  discussed in the yahoo groups.  Turns out that
  all the talk was a workaround at best.  The
  problem does not appear on all machines.  The
  computer I use at home worked fine with the "hello"
  program.  But I took the exe to work and saw what
  the fuss was about.  Maybe someone else can explain,
  but it appears that some CPUs are able to continue
  (by perhaps some type of internal reset) after a FPU 
  stack overflow.  Run a program like Pass12.bas in 
  ./JustTests to see if your machine is a "victim" of  
  stack overflow (ver 15).  Anyhow, the fix was a pop 
  after a store when calling the msvcrt. 

* Callback function can't be used because of destroyed ESI.
  I implemented the second best of two solutions.  The stack
  was unbalanced at function exit.  To balance it, I added
  the instruction ADD ESP,<size> at each function exit before
  popping the ESI/EDI/EBX registers.  The better solution would
  be to allocate stack space (SUB ESP,<size>) immediatelly 
  following the MOV EBP,ESP statement and then push the registers
  EBX/EDI/ESI.

* Removed Linux code.
  Before anyone "bashes" me on this let me explain.
  I don't have Linux and know little about it.
  It's sort of like this:  Have you ever tried to
  listen to two people talk at the same time and
  try to comprehend what each is saying?  Sorry,
  it was just getting in my way.  Since I have not
  added large amounts of code, it could be easily
  added back at this point should anyone care.

* Assigning UDT to another UDT was causing a crash.
  See CopyUDT.bas.

* $Include problem with constants fixed.
  When constants are in an include file,
  it no longer causes an error.

* $Optimize On now eliminates unused code.
  See notes in OptimizeOn.bas.

* $Resource added. 
  Icon, bitmap, wave, cursor, dialog, and RCDATA supported.

* Removed default icon.

* Integer array assignment problem is no more.
  Run IntArrayAssignment.bas on version 15.

* $Const now allows negative constant values.
  GetNextWord skipped over the "-".
  See NegativeConstants.bas.

* "Mucked" up the directory structure.
  All the "workers" now "live in" Bin.
  Libraries and include files are in Inc.

* Hexadecimal numbers now supported.
  Use the form 0x instead of figuring
  out the integer value.

* An include path may be used from the command line (argv[2]).

* $Include now behaves as follows:
  If it contains path component, go directly after the include.
  If it has no path, look first in the current directory, and
  then look at the path supplied on the command line.

* Console may be paused by using "Pause" as argv[3].
  Gotta have argv[2] to use it though.  Lame huh?

* Pause statement added for console.  May not sound like
  much, but when used in conjunction with the command line 
  pause option, I may never use another batch file again 
  when working with this compiler.

* Got ByRef to work with API functions.

* Changed functions that use structures to call ByRef.
  It just seems so much easier to write:

  GetClientRect(hWnd, rc), as opposed to

  GetClientRect(hWnd, AddressOf(rc))

  Also changed most functions to String types for lpString references.
  For calls like LoadIcon and LoadCursor that use resources, I added a
  function called MakeIntResource.  For others like GetModuleHandle,
  I left them alone.  Maybe something could be worked out for
  functions like InvalidateRect that has a reference to a RECT, but
  sometimes you really want to pass it NULL (0).  How about an "Any"
  parameter?

* Editor specifically for KoolB included.
  It's got it's problems ... But it does 
  the job. The edit control it uses has 
  the ability to do color syntax highlighting, 
  but I have not added this feature (yet?).
  Find/Replace needs work.

* Now using upx instead of pepack.  Pepack couldn't do the XPManifest
  thing (or I just don't know how).  Perhaps on the do list is get
  a source copy of upx and modify it so that output is only on errors.

Issues:

* Having trouble passing file names with spaces to the compiler from the editor.  


Mark

