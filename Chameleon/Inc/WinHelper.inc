'*****************
'* WinHelper.inc *
'*****************

$IfNDef WinHelperinc
$Define WinHelperinc

Function ShowMessage(Message As String) As Integer
  Result = MessageBox(0, Message, "Message:", 0)
End Function

Function LoWord(dwValue As Integer) As Word
  $Asm
    mov eax,dword[ebp+8]   ;dwValue
    and eax,65535
    mov dword[ebp-4],eax
  $End Asm
End Function

Function HiWord(dwValue As Integer) As Word
  $Asm
    mov eax,dword[ebp+8]
    and ecx,16
    shr eax,cl 
    mov dword[ebp-4],eax
  $End Asm
End Function

Function MakeIntResource(nInteger As Integer) As String
  $Asm
    extern sprintf
    jmp forward
    strFormat db '#%010ld',0
    forward:
    stdcall HeapAlloc,dword[HandleToHeap],8,12 
    mov ebx,eax 
    ccall sprintf,eax,strFormat,dword[ebp+8]
    mov dword[ebp-4],ebx
  $End Asm
End Function

$End If
