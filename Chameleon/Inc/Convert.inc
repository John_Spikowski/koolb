'***************
'* Convert.inc *
'***************

$IfNDef Convertinc
$Define Convertinc

$Include "String.inc"

Declare Function gcvt Lib "msvcrt.dll" Alias "_gcvt" Call C (Number As Double, NumDigits As Integer, ByRef S As String) As Integer
Declare Function Val Lib "msvcrt.dll" Alias "atof" Call C (S As String) As Double

Function IntToStr(nInteger As Integer) As String
  $Asm
    extern sprintf
    jmp forward
    strFormat db '%ld',0
    forward:
    stdcall HeapAlloc,dword[HandleToHeap],8,11 
    mov ebx,eax 
    ccall sprintf,eax,strFormat,dword[ebp+8]
    mov dword[ebp-4],ebx
  $End ASM
End Function

Function Str$(Number As Double) As String
  Dim NumDigits As Integer
  NumDigits = 100
  Result = Space$(NumDigits + 1)
  gcvt(Number, NumDigits, Result)
End Function

Function StrI(nInteger As Integer) As String
  $Asm
    extern sprintf
    stdcall HeapAlloc,dword[HandleToHeap],8,11 
    mov ebx,eax 
    invoke sprintf,eax,"%ld",dword[ebp+8]
    mov dword[ebp-4],ebx
  $End ASM
End Function

Function StrD(fValue As Double) As String
  $Asm
    extern sprintf
    stdcall HeapAlloc,dword[HandleToHeap],8,128 
    mov ebx,eax 
    invoke sprintf,eax,"%f",dword[ebp+8],dword[ebp+12]
    mov dword[ebp-4],ebx
  $End ASM
End Function

$End If