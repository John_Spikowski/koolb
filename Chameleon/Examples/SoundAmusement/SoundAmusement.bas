'**********************
'* SoundAmusement.bas *
'**********************

$AppType GUI 
$Optimize On
$Compress On
$XPStyle

$Resource IDW_WAVE1 As "AnyKey.wav"
$Resource IDW_WAVE2 As "ComputersCanDoThat.wav"
$Resource IDW_WAVE3 As "Excellent.wav"
$Resource IDW_WAVE4 As "WhatsUpDoc.wav"
$Resource IDW_WAVE5 As "BugElmer.wav"
$Resource IDW_WAVE6 As "ThatsAllFolks.wav"
$Resource IDW_WAVE7 As "Arcade.wav"
$Resource IDW_WAVE8 As "Laugh.wav"
$Resource IDW_WAVE9 As "Applause.wav"
$Resource IDI_APPICON As "SoundAmusement.ico"

$Include "Windows.inc" 

Dim message As MSG 
Dim rct As RECT
Dim wcex As WNDCLASSEX 
Dim hInst As Integer
Dim hWindow As Integer
Dim strClassName As String
Dim strAppTitle As String

Function SetChildFonts(hControl As Integer, lParam As Integer) As Integer
  SendMessage(hControl, WM_SETFONT, lParam, 0)
  Result = 1     'Brian, I see what you mean.  This works now.
End Function

'Sub SetChildFonts(hControl As Integer, lParam As Integer)
'  SendMessage(hControl, WM_SETFONT, lParam, 0)
'  $Asm 
'    mov eax,1   ;It works, just not what I want.
'  $End Asm    
'End Sub

Function CenterOnDesktop(hWindow As Integer) As Integer
  Dim xPos As Integer
  Dim yPos As Integer
  GetWindowRect(hWindow, rct)
  xPos = (GetSystemMetrics(SM_CXSCREEN) - (rct.right - rct.left)) / 2
  yPos = (GetSystemMetrics(SM_CYSCREEN) - (rct.bottom - rct.top)) / 2
  SetWindowPos(hWindow, HWND_TOP, xPos, yPos, 0, 0, _
               SWP_NOZORDER + SWP_NOSIZE + SWP_NOACTIVATE)
End Function

Function OnCreate(hWnd As Integer, uMsg As Integer, _ 
                  wParam As Integer, lParam As Integer) As Integer
  CreateWindowEx(0, "Button", "Simpsons", _
                 BS_GROUPBOX + WS_CHILD + WS_VISIBLE, _
                 8, 5, 120, 132, hWnd, 1, hInst, 0)
  CreateWindowEx(0, "Button", "Any Key", _
                 BS_PUSHBUTTON + WS_CHILD + WS_VISIBLE, _
                 31, 26, 75, 25, hWnd, 101, hInst, 0)
  CreateWindowEx(0, "Button", "Do That", _
                 BS_PUSHBUTTON + WS_CHILD + WS_VISIBLE, _
                 31, 63, 75, 25, hWnd, 102, hInst, 0)
  CreateWindowEx(0, "Button", "Excellent", _
                 BS_PUSHBUTTON + WS_CHILD + WS_VISIBLE, _
                 31, 98, 75, 25, hWnd, 103, hInst, 0)

  CreateWindowEx(0, "Button", "Loney Tunes", _
                 BS_GROUPBOX + WS_CHILD + WS_VISIBLE, _
                 141, 5, 120, 132, hWnd, 2, hInst, 0)
  CreateWindowEx(0, "Button", "Whats Up", _
                 BS_PUSHBUTTON + WS_CHILD + WS_VISIBLE, _
                 163, 26, 75, 25, hWnd, 104, hInst, 0)
  CreateWindowEx(0, "Button", "Elmer/Bugs", _
                 BS_PUSHBUTTON + WS_CHILD + WS_VISIBLE, _
                 163, 63, 75, 25, hWnd, 105, hInst, 0)
  CreateWindowEx(0, "Button", "Thats All", _
                 BS_PUSHBUTTON + WS_CHILD + WS_VISIBLE, _
                 163, 98, 75, 25, hWnd, 106, hInst, 0)
 
  CreateWindowEx(0, "Button", "Other", _
                 BS_GROUPBOX + WS_CHILD + WS_VISIBLE, _
                 275, 5, 120, 132, hWnd, 3, hInst, 0)
  CreateWindowEx(0, "Button", "Arcade", _
                 BS_PUSHBUTTON + WS_CHILD + WS_VISIBLE, _
                 298, 26, 75, 25, hWnd, 107, hInst, 0)
  CreateWindowEx(0, "Button", "Laugh", _
                 BS_PUSHBUTTON + WS_CHILD + WS_VISIBLE, _
                 298, 63, 75, 25, hWnd, 108, hInst, 0)
  CreateWindowEx(0, "Button", "Applause", _
                 BS_PUSHBUTTON + WS_CHILD + WS_VISIBLE, _
                 298, 98, 75, 25, hWnd, 109, hInst, 0)
 
  CreateWindowEx(0, "Button", "Exit", _
                 BS_PUSHBUTTON + WS_CHILD + WS_VISIBLE, _
                 163, 144, 75, 25, hWnd, 110, hInst, 0)

  EnumChildWindows(hWnd, CodePtr(SetChildFonts), GetStockObject(ANSI_VAR_FONT))

' Enumerate workaround.
'  Dim hChild As Integer
'  Dim i As Integer
'  i = 1
'  hChild = GetTopWindow(hWnd)
'  While i <= 13  ' Group boxes included (10 + 3)
'    SendMessage(hChild, WM_SETFONT, GetStockObject(ANSI_VAR_FONT), 0)
'    hChild = GetNextWindow(hChild, GW_HWNDNEXT)
'    i = i + 1
'  Wend
 
  Result = 0
End Function

Function OnCommand(hWnd As Integer, uMsg As Integer, _ 
                   wParam As Integer, lParam As Integer) As Integer
  Dim wControlId As Integer
  wControlId = LoWord(wparam)
  If wControlId = 101 Then 
    PlaySound(MakeIntResource(IDW_WAVE1), hInst, _
              SND_RESOURCE + SND_ASYNC)
  End If
  If wControlId = 102 Then 
    PlaySound(MakeIntResource(IDW_WAVE2), hInst, _
              SND_RESOURCE + SND_ASYNC)
  End If
  If wControlId = 103 Then 
    PlaySound(MakeIntResource(IDW_WAVE3), hInst, _
              SND_RESOURCE + SND_ASYNC)
  End If
  If wControlId = 104 Then 
    PlaySound(MakeIntResource(IDW_WAVE4), hInst, _
              SND_RESOURCE + SND_ASYNC)
  End If
  If wControlId = 105 Then 
    PlaySound(MakeIntResource(IDW_WAVE5), hInst, _
              SND_RESOURCE + SND_ASYNC)
  End If
  If wControlId = 106 Then 
    PlaySound(MakeIntResource(IDW_WAVE6), hInst, _
              SND_RESOURCE + SND_ASYNC)
  End If
  If wControlId = 107 Then 
    PlaySound(MakeIntResource(IDW_WAVE7), hInst, _
              SND_RESOURCE + SND_ASYNC)
  End If
  If wControlId = 108 Then 
    PlaySound(MakeIntResource(IDW_WAVE8), hInst, _
              SND_RESOURCE + SND_ASYNC)
  End If
  If wControlId = 109 Then 
    PlaySound(MakeIntResource(IDW_WAVE9), hInst, _
              SND_RESOURCE + SND_ASYNC)
  End If
  If wControlId = 110 Then 
    DestroyWindow(hWindow)
  End If
  Result = 0
End Function

Function WindowProc(hWnd As Integer, uMsg As Integer, _ 
                    wParam As Integer, lParam As Integer) As Integer
  If uMsg = WM_CREATE Then 
    Result = OnCreate(hWnd, uMsg, wParam, lParam)
  ElseIf uMsg = WM_COMMAND Then
    Result = OnCommand(hWnd, uMsg, wParam, lParam)
  ElseIf uMsg = WM_DESTROY Then 
    PostQuitMessage(0) 
    Result = 0
  Else 
    Result = DefWindowProc(hWnd, uMsg, wParam, lParam)
  End If 
End Function

'***

strAppTitle = "Sound Amusement" 
strClassName = "SoundAmusement"

hInst = GetModuleHandle(0)
wcex.cbSize = SizeOf(WNDCLASSEX) 
wcex.style = CS_VREDRAW + CS_HREDRAW
wcex.lpfnwndproc = CodePtr(WindowProc) 
wcex.cbClsExtra = 0 
wcex.cbWndExtra = 0 
wcex.hInstance = hInst
wcex.hIcon = LoadIcon(hInst, MakeIntResource(IDI_APPICON)) 
wcex.hCursor = LoadCursor(0, MakeIntResource(IDC_ARROW)) 
wcex.hbrBackground = COLOR_BTNFACE + 1
wcex.lpszMenuName = "" 
wcex.lpszClassName = strClassName 
wcex.hIconSm = 0 

If (RegisterClassEx(wcex)) = 0 Then 
  MessageBox(0, "RegisterClassEx failed.", strAppTitle, MB_OK)
  ExitProcess(0)
End If 

hWindow = CreateWindowEx(0, strClassName, strAppTitle, _
                         WS_OVERLAPPED + WS_CAPTION + WS_SYSMENU + WS_MINIMIZEBOX, _
                         0, 0, 410, 210, 0, 0, hInst, 0)
If hWindow = 0 Then 
  MessageBox(0, "CreateWindowEx failed.", strAppTitle, MB_OK)
  ExitProcess(0)
End If 

CenterOnDesktop(hWindow)
ShowWindow(hWindow, SW_SHOWNORMAL) 
UpdateWindow(hWindow) 

While GetMessage(message, 0, 0, 0) > 0
  TranslateMessage(message) 
  DispatchMessage(message) 
Wend 
