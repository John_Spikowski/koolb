'*****************
'* SetCursor.bas *
'*****************

$AppType Gui
$Optimize On
$Compress On
$XPStyle

$Include "Windows.inc" 

$Const IDC_BTN_RESTORE = 110
$Const IDC_BTN_EXIT = 111
$Const True = -1
$Const False = 0

$Resource Bugs As "bugs.cur"
$Resource CookieMonster As "cookiemonster.cur"
$Resource Daffy As "daffy.cur"
$Resource FrogFace As "frogface.cur"
$Resource MagicWand As "majicwnd.cur"
$Resource Mickey As "mickey.cur"
$Resource Pen As "pen.cur"
$Resource Reminder As "reminder.cur"
$Resource Rocker As "rocket.cur"
$Resource Sylvester As "sylvester.cur"

Dim message As MSG 
Dim wcex As WNDCLASSEX 
Dim ps As PAINTSTRUCT 
Dim rct As RECT
Dim hWindow As Integer
Dim strClassName As String
Dim strAppTitle As String
Dim hCursor As Integer
Dim hRestoreCursor As Integer

Function SetChildFonts(hControl As Integer, lParam As Integer) As Integer
  SendMessage(hControl, WM_SETFONT, lParam, 0)
  Result = 1    
End Function

Function CenterOnDesktop(hWindow As Integer) As Integer
  Dim xPos As Integer
  Dim yPos As Integer
  GetWindowRect(hWindow, rct)
  xPos = (GetSystemMetrics(SM_CXSCREEN) - (rct.right - rct.left)) / 2
  yPos = (GetSystemMetrics(SM_CYSCREEN) - (rct.bottom - rct.top)) / 2
  SetWindowPos(hWindow, HWND_TOP, xPos, yPos, 0, 0, _
               SWP_NOZORDER + SWP_NOSIZE + SWP_NOACTIVATE)
End Function

Function OnCreate(hWnd As Integer, uMsg As Integer, _ 
                  wParam As Integer, lParam As Integer) As Integer
  CreateWindowEx(0, "Button", "", _
                 BS_GROUPBOX + WS_CHILD + WS_VISIBLE, _
                 8, 8, 220, 209, hWnd, 0, 0x400000, 0)
  CreateWindowEx(0, "Button", "Bugs", _
                 BS_PUSHBUTTON + WS_CHILD + WS_VISIBLE, _
                 25, 24, 85, 23, hWnd, 100, 0x400000, 0)
  CreateWindowEx(0, "Button", "Cookie Monster", _
                 BS_PUSHBUTTON + WS_CHILD + WS_VISIBLE, _
                 123, 24, 85, 23, hWnd, 101, 0x400000, 0)
  CreateWindowEx(0, "Button", "Daffy", _
                 BS_PUSHBUTTON + WS_CHILD + WS_VISIBLE, _
                 25, 52, 85, 23, hWnd, 102, 0x400000, 0)
  CreateWindowEx(0, "Button", "Frog Face", _
                 BS_PUSHBUTTON + WS_CHILD + WS_VISIBLE, _
                 123, 52, 85, 23, hWnd, 103, 0x400000, 0)
  CreateWindowEx(0, "Button", "Magic Wand", _
                 BS_PUSHBUTTON + WS_CHILD + WS_VISIBLE, _
                 25, 80, 85, 23, hWnd, 104, 0x400000, 0)
  CreateWindowEx(0, "Button", "Mickey", _
                 BS_PUSHBUTTON + WS_CHILD + WS_VISIBLE, _
                 123, 80, 85, 23, hWnd, 105, 0x400000, 0)
  CreateWindowEx(0, "Button", "Pen", _
                 BS_PUSHBUTTON + WS_CHILD + WS_VISIBLE, _
                 25, 108, 85, 23, hWnd, 106, 0x400000, 0)
  CreateWindowEx(0, "Button", "Reminder", _
                 BS_PUSHBUTTON + WS_CHILD + WS_VISIBLE, _
                 123, 108, 85, 23, hWnd, 107, 0x400000, 0)
  CreateWindowEx(0, "Button", "Rocket", _
                 BS_PUSHBUTTON + WS_CHILD + WS_VISIBLE, _
                 25, 136, 85, 23, hWnd, 108, 0x400000, 0)
  CreateWindowEx(0, "Button", "Sylvester", _
                 BS_PUSHBUTTON + WS_CHILD + WS_VISIBLE, _
                 123, 136, 85, 23, hWnd, 109, 0x400000, 0)
  CreateWindowEx(0, "Button", "Restore", _
                 BS_PUSHBUTTON + WS_CHILD + WS_VISIBLE, _
                 25, 178, 85, 23, hWnd, IDC_BTN_RESTORE, 0x400000, 0)
  CreateWindowEx(0, "Button", "Exit", _
                 BS_PUSHBUTTON + WS_CHILD + WS_VISIBLE, _
                 123, 178, 85, 23, hWnd, IDC_BTN_EXIT, 0x400000, 0)

  EnumChildWindows(hWnd, CodePtr(SetChildFonts), GetStockObject(ANSI_VAR_FONT))
  EnableWindow(GetDlgItem(hWnd, IDC_BTN_RESTORE), False)
  CenterOnDesktop(hWnd)
  SetCursor(hRestoreCursor)
End Function

Function OnCommand(hWnd As Integer, uMsg As Integer, _ 
                   wParam As Integer, lParam As Integer) As Integer
  Dim wControlId As Integer
  wControlId = LoWord(wparam)                   
  If wControlId >= 100 And wControlId <= 109 Then
    hCursor = LoadCursor(0x400000, MakeIntResource(wControlId))
    SetCursor(hCursor) 
    EnableWindow(GetDlgItem(hWnd, IDC_BTN_RESTORE), True)
  End If
  If wControlId = IDC_BTN_RESTORE Then 
    SetCursor(hRestoreCursor)
    EnableWindow(GetDlgItem(hWnd, IDC_BTN_RESTORE), False)
  End If
  If wControlId = IDC_BTN_EXIT Then   
    DestroyWindow(hWindow)
  End If
  Result = 0
End Function

Function OnSetCursor(hWnd As Integer, uMsg As Integer, _ 
                     wParam As Integer, lParam As Integer) As Integer
  Result = 1
End Function

Function WindowProc(hWnd As Integer, uMsg As Integer, _ 
                    wParam As Integer, lParam As Integer) As Integer
  If uMsg = WM_CREATE Then 
    Result = OnCreate(hWnd, uMsg, wParam, lParam)
  ElseIf uMsg = WM_COMMAND Then 
    Result = OnCommand(hWnd, uMsg, wParam, lParam)
  ElseIf uMsg = WM_SETCURSOR Then 
    Result = OnSetCursor(hWnd, uMsg, wParam, lParam)
  ElseIf uMsg = WM_DESTROY Then 
    PostQuitMessage(0) 
    Result = 0
  Else 
    Result = DefWindowProc(hWnd, uMsg, wParam, lParam)
  End If 
End Function

'***

strAppTitle = "SetCursor" 
strClassName = "SetCursorClass"

hRestoreCursor = LoadCursor(0, MakeIntResource(IDC_ARROW))
wcex.cbSize = SizeOf(WNDCLASSEX) 
wcex.style = CS_VREDRAW + CS_HREDRAW + CS_CLASSDC + CS_PARENTDC
wcex.lpfnwndproc = CodePtr(WindowProc) 
wcex.cbClsExtra = 0 
wcex.cbWndExtra = 0 
wcex.hInstance = 0x400000
wcex.hIcon = LoadIcon(0, MakeIntResource(IDI_APPLICATION)) 
wcex.hCursor = hRestoreCursor
wcex.hbrBackground = COLOR_BTNFACE + 1
wcex.lpszMenuName = "" 
wcex.lpszClassName = strClassName 
wcex.hIconSm = 0 

If (RegisterClassEx(wcex)) = 0 Then 
  MessageBox(0, "RegisterClassEx failed.", strAppTitle, MB_OK)
  ExitProcess(0)
End If 

hWindow = CreateWindowEx(0, strClassName, strAppTitle, _
                         WS_OVERLAPPED + WS_VISIBLE + _ 
                         WS_SYSMENU + WS_MINIMIZEBOX, _
                         0, 0, 243, 262, _ 
                         0, 0, 0x400000, 0) 
If hWindow = 0 Then 
  MessageBox(0, "CreateWindowEx failed.", strAppTitle, MB_OK)
  ExitProcess(0)
End If 

While GetMessage(message, 0, 0, 0) > 0
  TranslateMessage(message) 
  DispatchMessage(message) 
Wend 
