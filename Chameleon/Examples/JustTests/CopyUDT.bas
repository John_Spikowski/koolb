'***************
'* CopyUDT.bas *
'***************

Type IntType
  x As Integer
  y As Integer
End Type

Dim it As IntType
Dim itCopy As IntType

it.x = 78
it.y = 89

itCopy = it

Print itCopy.x
Print itCopy.y

Pause