'*******************
'* WordTesting.bas *
'*******************

' Simple assignment

Dim w As Word

w = 65535 : Print w

'wy?? = 101 : Print wy??

' Math

w = w + 10 : Print w

w = 0x8000 : Print w      ' Positive 32768
w = -32769 : Print w 

' UDT Assigrment

Type WordType
  x As Word
  y As Word
End Type

Dim wt As WordType

wt.x = 256
wt.y = 257

Print wt.x
Print wt.y

' Copy UDT

Dim wtCopy As WordType

wtCopy = wt 

Print wtCopy.x
Print wtCopy.y

' Array Assigrment

Dim WordArray(4) As Word

WordArray(1) = w
WordArray(2) = 2
WordArray(3) = 3
WordArray(4) = 4 

Print WordArray(1)
Print WordArray(2)
Print WordArray(3)
Print WordArray(4)

WordArray(1) = 4567
WordArray(3) = WordArray(1)

Print "WordArray(3) ", WordArray(3)
 
' Copy Array

Dim WordArrayCopy(4) As Word

WordArrayCopy = WordArray 

Print WordArrayCopy(1)
Print WordArrayCopy(2)
Print WordArrayCopy(3)
Print WordArrayCopy(4)

' Function: Return a Word

Function ReturnWord() As Word
  Result = 42
End Function

Print ReturnWord()

' Subroutine: Send a Word

Sub SendWord(wd As Word)
  wd = wd + 1
  Print wd
End Sub

SendWord(60000)

' Function: Local variable

Function WordLocal(wd As Word) As Word
  Dim wdwork As Word
  wdwork = wd + 1
  Print "wdwork = wd + 1 ", wdwork
  Result = -1
End Function

WordLocal(99)

Pause