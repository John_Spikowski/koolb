'*******************
'* ByteTesting.bas *
'*******************

' Simple assignment

Dim b As Byte

b = 255 : Print b

'by? = 101 : Print by?

' Math

b = b + 10 : Print b

b = 0x81 : Print b      ' Positive 129 
b = -127 : Print b 

' UDT Assigrment

Type ByteType
  x As Byte
  y As Byte
End Type

Dim bt As ByteType

bt.x = 3
bt.y = 8

Print bt.x
Print bt.y

' Copy UDT

Dim btCopy As ByteType

btCopy = bt 

Print btCopy.x
Print btCopy.y

' Array Assigrment

Dim ByteArray(4) As Byte

ByteArray(1) = b
ByteArray(2) = 2
ByteArray(3) = 3
ByteArray(4) = 4 

Print ByteArray(1)
Print ByteArray(2)
Print ByteArray(3)
Print ByteArray(4)

' Copy Array

Dim ByteArrayCopy(4) As Byte

ByteArrayCopy = ByteArray 

Print ByteArrayCopy(1)
Print ByteArrayCopy(2)
Print ByteArrayCopy(3)
Print ByteArrayCopy(4)

' Function: Return a byte

Function ReturnByte() As Byte
  Result = 42
End Function

Print ReturnByte()

' Subroutine: Send a byte

Sub SendByte(ch As Byte)
  ch = ch + 1
  Print ch
End Sub

SendByte(77)

' Function: Local variable

Function ByteLocal(ch As Byte) As Byte
  Dim chwork As Byte
  chwork = ch + 1
  Print "chwork = ch + 1 ", chwork
  Result = -1
End Function

ByteLocal(99)

Pause