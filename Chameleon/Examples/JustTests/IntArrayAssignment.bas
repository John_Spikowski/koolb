'**************************
'* IntArrayAssignment.bas *
'**************************

Dim IntArray(4) As Integer

IntArray(1) = 1
IntArray(2) = 2
IntArray(3) = 3
IntArray(4) = 4 

Print IntArray(1)
Print IntArray(2)
Print IntArray(3)
Print IntArray(4)

Pause

