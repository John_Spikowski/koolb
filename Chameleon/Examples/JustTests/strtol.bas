'**************
'* strtol.bas *
'**************

Declare Function strtol Lib "msvcrt.dll" Alias "strtol" Call C _
(startPtr As Integer, endPtr As Integer, base As Integer) As Integer

Dim num As String
Dim i As Integer

i = 12
print i

num = "123"
i = strtol(AddressOf(num), 0, 10)
Print i 

num = "0xff"
i = strtol(AddressOf(num), 0, 16)
Print i 

num = "0XFFFe"
i = strtol(AddressOf(num), 0, 16)
Print i 

i = 0x12
Print i 

num = "0xFFFFffff"
i = strtol(AddressOf(num), 0, 16)
Print i 

Print "See, this guy really did return an long."
Print "Just not the one I wanted."
Print "Too bad, I wanted a dword"
Print "You know, an unsigned long."

Pause
