'*****************
'* HexValues.bas *
'*****************

$Include "HexValues.inc"

Dim nHexNum As Integer

nHexNum = 0xffffffff
Print "nHexNum = 0xffffffff ", nHexNum

Print "Pizza is selling for: ", PIZZA_PRICE, " dollars"

nHexNum = PIZZA_PRICE - 10

Print "nHexNum = PIZZA_PRICE - 10"

Print "Now it is only ", nHexNum

Print "Ever wonder what the hex number 0xABCD is in hex?"
Print "It is ", 0xABCD

Pause