;Library functions to import to the KoolB app
extern ExitProcess
extern MessageBoxA
extern lstrcat
extern lstrlen
extern lstrcpy
extern HeapCreate
extern HeapAlloc
extern HeapFree
extern GetModuleHandleA
extern GetCommandLineA
extern GetStdHandle
extern floor
extern WriteFile
extern _gcvt
extern lstrlenA
extern RtlMoveMemory
extern lstrcpyA
extern system
extern HeapDestroy



;Initialize everything to prep the app to run
section .text
%include "C:\KoolB\16.00\Bin\MACROS.INC"
global START
START:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH ESI
PUSH EDI
stdcall HeapCreate,0,16384,0
MOV dword[HandleToHeap],EAX
CMP dword[HandleToHeap],0
JNE Label1
JMP NoMemory
Label1:
stdcall GetModuleHandleA,0
MOV dword[Internal_HInstance],EAX
CALL GetCommandLineA
MOV dword[Internal_CommandLine],EAX
stdcall GetStdHandle,-10
MOV dword[HandleToInput],EAX
stdcall GetStdHandle,-11
MOV dword[HandleToOutput],EAX
CMP dword[HandleToInput],-1
JNE Label2
JMP NoConsole
Label2:
CMP dword[HandleToOutput],-1
JNE Label3
JMP NoConsole
Label3:



;The main body where the app actually runs
PUSH dword[Number_1+4]
PUSH dword[Number_1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EAX
MOV word[Scope0__W_Word],AX
XOR EAX,EAX
MOV AX,word[Scope0__W_Word]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label7
JMP NoMemory
Label7:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label4:
CMP byte[EBX+ECX],0
JE Label5
INC ECX
CMP ECX,EDI
JL Label4
JMP Label6
Label5:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label6
MOV byte[EBX+ECX],0
Label6:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
XOR EAX,EAX
MOV AX,word[Scope0__W_Word]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_2+4]
PUSH dword[Number_2]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EAX
MOV word[Scope0__W_Word],AX
XOR EAX,EAX
MOV AX,word[Scope0__W_Word]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label11
JMP NoMemory
Label11:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label8:
CMP byte[EBX+ECX],0
JE Label9
INC ECX
CMP ECX,EDI
JL Label8
JMP Label10
Label9:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label10
MOV byte[EBX+ECX],0
Label10:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_3+4]
PUSH dword[Number_3]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EAX
MOV word[Scope0__W_Word],AX
XOR EAX,EAX
MOV AX,word[Scope0__W_Word]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label15
JMP NoMemory
Label15:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label12:
CMP byte[EBX+ECX],0
JE Label13
INC ECX
CMP ECX,EDI
JL Label12
JMP Label14
Label13:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label14
MOV byte[EBX+ECX],0
Label14:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_4+4]
PUSH dword[Number_4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EAX
MOV word[Scope0__W_Word],AX
XOR EAX,EAX
MOV AX,word[Scope0__W_Word]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label19
JMP NoMemory
Label19:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label16:
CMP byte[EBX+ECX],0
JE Label17
INC ECX
CMP ECX,EDI
JL Label16
JMP Label18
Label17:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label18
MOV byte[EBX+ECX],0
Label18:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_5+4]
PUSH dword[Number_5]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EAX
MOV word[Scope0__WT_UDT+Scope0__WORDTYPE_TYPE.Scope0__X_Word],AX
PUSH dword[Number_6+4]
PUSH dword[Number_6]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EAX
MOV word[Scope0__WT_UDT+Scope0__WORDTYPE_TYPE.Scope0__Y_Word],AX
XOR EAX,EAX
MOV AX,word[Scope0__WT_UDT+Scope0__WORDTYPE_TYPE.Scope0__X_Word]
MOV dword[TempQWord1], EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label23
JMP NoMemory
Label23:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label20:
CMP byte[EBX+ECX],0
JE Label21
INC ECX
CMP ECX,EDI
JL Label20
JMP Label22
Label21:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label22
MOV byte[EBX+ECX],0
Label22:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
XOR EAX,EAX
MOV AX,word[Scope0__WT_UDT+Scope0__WORDTYPE_TYPE.Scope0__Y_Word]
MOV dword[TempQWord1], EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label27
JMP NoMemory
Label27:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label24:
CMP byte[EBX+ECX],0
JE Label25
INC ECX
CMP ECX,EDI
JL Label24
JMP Label26
Label25:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label26
MOV byte[EBX+ECX],0
Label26:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EDI,Scope0__WORDTYPE_TYPE_size
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label28
JMP NoMemory
Label28:
MOV EBX,EAX
stdcall RtlMoveMemory,EBX,Scope0__WT_UDT,EDI
PUSH EBX
POP EBX
stdcall RtlMoveMemory,Scope0__WTCOPY_UDT,EBX,Scope0__WORDTYPE_TYPE_size
stdcall HeapFree,dword[HandleToHeap],0,EBX
XOR EAX,EAX
MOV AX,word[Scope0__WTCOPY_UDT+Scope0__WORDTYPE_TYPE.Scope0__X_Word]
MOV dword[TempQWord1], EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label32
JMP NoMemory
Label32:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label29:
CMP byte[EBX+ECX],0
JE Label30
INC ECX
CMP ECX,EDI
JL Label29
JMP Label31
Label30:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label31
MOV byte[EBX+ECX],0
Label31:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
XOR EAX,EAX
MOV AX,word[Scope0__WTCOPY_UDT+Scope0__WORDTYPE_TYPE.Scope0__Y_Word]
MOV dword[TempQWord1], EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label36
JMP NoMemory
Label36:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label33:
CMP byte[EBX+ECX],0
JE Label34
INC ECX
CMP ECX,EDI
JL Label33
JMP Label35
Label34:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label35
MOV byte[EBX+ECX],0
Label35:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_7+4]
PUSH dword[Number_7]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
INC EBX
MOV dword[Scope0__WORDARRAY_Word_Size],EBX
MOV EAX,2
MUL EBX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label37
JMP NoMemory
Label37:
MOV dword[Scope0__WORDARRAY_Word],EAX
PUSH dword[Number_8+4]
PUSH dword[Number_8]
XOR EAX,EAX
MOV AX,word[Scope0__W_Word]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
CMP EBX,dword[Scope0__WORDARRAY_Word_Size]
JGE Label38
CMP EBX,0
JLE Label38
MOV EDI, dword[Scope0__WORDARRAY_Word]
PUSH dword[TempQWord1 + 4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EAX
MOV word[EDI+EBX*2],AX
Label38:
PUSH dword[Number_9+4]
PUSH dword[Number_9]
PUSH dword[Number_10+4]
PUSH dword[Number_10]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
CMP EBX,dword[Scope0__WORDARRAY_Word_Size]
JGE Label39
CMP EBX,0
JLE Label39
MOV EDI, dword[Scope0__WORDARRAY_Word]
PUSH dword[TempQWord1 + 4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EAX
MOV word[EDI+EBX*2],AX
Label39:
PUSH dword[Number_11+4]
PUSH dword[Number_11]
PUSH dword[Number_12+4]
PUSH dword[Number_12]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
CMP EBX,dword[Scope0__WORDARRAY_Word_Size]
JGE Label40
CMP EBX,0
JLE Label40
MOV EDI, dword[Scope0__WORDARRAY_Word]
PUSH dword[TempQWord1 + 4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EAX
MOV word[EDI+EBX*2],AX
Label40:
PUSH dword[Number_13+4]
PUSH dword[Number_13]
PUSH dword[Number_14+4]
PUSH dword[Number_14]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
CMP EBX,dword[Scope0__WORDARRAY_Word_Size]
JGE Label41
CMP EBX,0
JLE Label41
MOV EDI, dword[Scope0__WORDARRAY_Word]
PUSH dword[TempQWord1 + 4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EAX
MOV word[EDI+EBX*2],AX
Label41:
PUSH dword[Number_15+4]
PUSH dword[Number_15]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
CMP EBX,dword[Scope0__WORDARRAY_Word_Size]
JGE Label42
CMP EBX,0
JLE Label42
MOV EDI,dword[Scope0__WORDARRAY_Word]
XOR EAX,EAX
MOV AX,word[EDI+EBX*2]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
JMP Label43
Label42:
FINIT
FLDZ
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
Label43:
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label47
JMP NoMemory
Label47:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label44:
CMP byte[EBX+ECX],0
JE Label45
INC ECX
CMP ECX,EDI
JL Label44
JMP Label46
Label45:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label46
MOV byte[EBX+ECX],0
Label46:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_16+4]
PUSH dword[Number_16]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
CMP EBX,dword[Scope0__WORDARRAY_Word_Size]
JGE Label48
CMP EBX,0
JLE Label48
MOV EDI,dword[Scope0__WORDARRAY_Word]
XOR EAX,EAX
MOV AX,word[EDI+EBX*2]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
JMP Label49
Label48:
FINIT
FLDZ
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
Label49:
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label53
JMP NoMemory
Label53:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label50:
CMP byte[EBX+ECX],0
JE Label51
INC ECX
CMP ECX,EDI
JL Label50
JMP Label52
Label51:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label52
MOV byte[EBX+ECX],0
Label52:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_17+4]
PUSH dword[Number_17]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
CMP EBX,dword[Scope0__WORDARRAY_Word_Size]
JGE Label54
CMP EBX,0
JLE Label54
MOV EDI,dword[Scope0__WORDARRAY_Word]
XOR EAX,EAX
MOV AX,word[EDI+EBX*2]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
JMP Label55
Label54:
FINIT
FLDZ
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
Label55:
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label59
JMP NoMemory
Label59:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label56:
CMP byte[EBX+ECX],0
JE Label57
INC ECX
CMP ECX,EDI
JL Label56
JMP Label58
Label57:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label58
MOV byte[EBX+ECX],0
Label58:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_18+4]
PUSH dword[Number_18]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
CMP EBX,dword[Scope0__WORDARRAY_Word_Size]
JGE Label60
CMP EBX,0
JLE Label60
MOV EDI,dword[Scope0__WORDARRAY_Word]
XOR EAX,EAX
MOV AX,word[EDI+EBX*2]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
JMP Label61
Label60:
FINIT
FLDZ
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
Label61:
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label65
JMP NoMemory
Label65:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label62:
CMP byte[EBX+ECX],0
JE Label63
INC ECX
CMP ECX,EDI
JL Label62
JMP Label64
Label63:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label64
MOV byte[EBX+ECX],0
Label64:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_19+4]
PUSH dword[Number_19]
PUSH dword[Number_20+4]
PUSH dword[Number_20]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
CMP EBX,dword[Scope0__WORDARRAY_Word_Size]
JGE Label66
CMP EBX,0
JLE Label66
MOV EDI, dword[Scope0__WORDARRAY_Word]
PUSH dword[TempQWord1 + 4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EAX
MOV word[EDI+EBX*2],AX
Label66:
PUSH dword[Number_21+4]
PUSH dword[Number_21]
PUSH dword[Number_22+4]
PUSH dword[Number_22]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
CMP EBX,dword[Scope0__WORDARRAY_Word_Size]
JGE Label67
CMP EBX,0
JLE Label67
MOV EDI,dword[Scope0__WORDARRAY_Word]
XOR EAX,EAX
MOV AX,word[EDI+EBX*2]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
JMP Label68
Label67:
FINIT
FLDZ
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
Label68:
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
CMP EBX,dword[Scope0__WORDARRAY_Word_Size]
JGE Label69
CMP EBX,0
JLE Label69
MOV EDI, dword[Scope0__WORDARRAY_Word]
PUSH dword[TempQWord1 + 4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EAX
MOV word[EDI+EBX*2],AX
Label69:
MOV EBX,String_23
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label70
JMP NoMemory
Label70:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_23
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
PUSH dword[Number_24+4]
PUSH dword[Number_24]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
CMP EBX,dword[Scope0__WORDARRAY_Word_Size]
JGE Label71
CMP EBX,0
JLE Label71
MOV EDI,dword[Scope0__WORDARRAY_Word]
XOR EAX,EAX
MOV AX,word[EDI+EBX*2]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
JMP Label72
Label71:
FINIT
FLDZ
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
Label72:
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label76
JMP NoMemory
Label76:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label73:
CMP byte[EBX+ECX],0
JE Label74
INC ECX
CMP ECX,EDI
JL Label73
JMP Label75
Label74:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label75
MOV byte[EBX+ECX],0
Label75:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_25+4]
PUSH dword[Number_25]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
INC EBX
MOV dword[Scope0__WORDARRAYCOPY_Word_Size],EBX
MOV EAX,2
MUL EBX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label77
JMP NoMemory
Label77:
MOV dword[Scope0__WORDARRAYCOPY_Word],EAX
MOV ESI,dword[Scope0__WORDARRAY_Word_Size]
MOV EAX,2
MUL ESI
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label78
JMP NoMemory
Label78:
MOV EDI,EAX
stdcall RtlMoveMemory,EDI,dword[Scope0__WORDARRAY_Word],EBX
PUSH EDI
PUSH dword[Scope0__WORDARRAY_Word_Size]
POP EBX
POP ESI
MOV dword[Scope0__WORDARRAYCOPY_Word],ESI
MOV dword[Scope0__WORDARRAYCOPY_Word_Size],EBX
PUSH dword[Number_26+4]
PUSH dword[Number_26]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
CMP EBX,dword[Scope0__WORDARRAYCOPY_Word_Size]
JGE Label79
CMP EBX,0
JLE Label79
MOV EDI,dword[Scope0__WORDARRAYCOPY_Word]
XOR EAX,EAX
MOV AX,word[EDI+EBX*2]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
JMP Label80
Label79:
FINIT
FLDZ
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
Label80:
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label84
JMP NoMemory
Label84:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label81:
CMP byte[EBX+ECX],0
JE Label82
INC ECX
CMP ECX,EDI
JL Label81
JMP Label83
Label82:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label83
MOV byte[EBX+ECX],0
Label83:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_27+4]
PUSH dword[Number_27]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
CMP EBX,dword[Scope0__WORDARRAYCOPY_Word_Size]
JGE Label85
CMP EBX,0
JLE Label85
MOV EDI,dword[Scope0__WORDARRAYCOPY_Word]
XOR EAX,EAX
MOV AX,word[EDI+EBX*2]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
JMP Label86
Label85:
FINIT
FLDZ
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
Label86:
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label90
JMP NoMemory
Label90:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label87:
CMP byte[EBX+ECX],0
JE Label88
INC ECX
CMP ECX,EDI
JL Label87
JMP Label89
Label88:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label89
MOV byte[EBX+ECX],0
Label89:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_28+4]
PUSH dword[Number_28]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
CMP EBX,dword[Scope0__WORDARRAYCOPY_Word_Size]
JGE Label91
CMP EBX,0
JLE Label91
MOV EDI,dword[Scope0__WORDARRAYCOPY_Word]
XOR EAX,EAX
MOV AX,word[EDI+EBX*2]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
JMP Label92
Label91:
FINIT
FLDZ
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
Label92:
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label96
JMP NoMemory
Label96:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label93:
CMP byte[EBX+ECX],0
JE Label94
INC ECX
CMP ECX,EDI
JL Label93
JMP Label95
Label94:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label95
MOV byte[EBX+ECX],0
Label95:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_29+4]
PUSH dword[Number_29]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
CMP EBX,dword[Scope0__WORDARRAYCOPY_Word_Size]
JGE Label97
CMP EBX,0
JLE Label97
MOV EDI,dword[Scope0__WORDARRAYCOPY_Word]
XOR EAX,EAX
MOV AX,word[EDI+EBX*2]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
JMP Label98
Label97:
FINIT
FLDZ
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
Label98:
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label102
JMP NoMemory
Label102:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label99:
CMP byte[EBX+ECX],0
JE Label100
INC ECX
CMP ECX,EDI
JL Label99
JMP Label101
Label100:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label101
MOV byte[EBX+ECX],0
Label101:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
CALL _RETURNWORD
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label106
JMP NoMemory
Label106:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label103:
CMP byte[EBX+ECX],0
JE Label104
INC ECX
CMP ECX,EDI
JL Label103
JMP Label105
Label104:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label105
MOV byte[EBX+ECX],0
Label105:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label111
JMP NoMemory
Label111:
MOV dword[ParameterPool],EAX
PUSH dword[Number_32+4]
PUSH dword[Number_32]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _SENDWORD
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label117
JMP NoMemory
Label117:
MOV dword[ParameterPool],EAX
PUSH dword[Number_36+4]
PUSH dword[Number_36]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _WORDLOCAL
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
ccall system,ConsolePause



;Prepare the app to exit and then terminate
Exit:
stdcall HeapFree,dword[HandleToHeap],0,dword[Scope0__WORDARRAY_Word]
stdcall HeapFree,dword[HandleToHeap],0,dword[Scope0__WORDARRAYCOPY_Word]
stdcall HeapDestroy,dword[HandleToHeap]
POP EDI
POP ESI
POP EBX
MOV ESP,EBP
POP EBP
MOV EAX,dword[ExitStatus]
stdcall ExitProcess,dword[ExitStatus]
RET

NoMemory:
MOV dword[ExitStatus],1
stdcall MessageBoxA,0,NoMemMessage,Error,0
JMP Exit

NoLibrary:
MOV dword[ExitStatus],1
POP EBX
stdcall lstrlen,EBX
ADD EAX,30
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
MOV EDI,EAX
stdcall lstrcpy,EDI,NoLibFound
stdcall lstrcat,EDI,EBX
stdcall MessageBoxA,0,EDI,Error,0
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
JMP Exit

NoFunction:
MOV dword[ExitStatus],1
POP EBX
stdcall lstrlen,EBX
ADD EAX,30
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
MOV EDI,EAX
stdcall lstrcpy,EDI,NoFunctionFound
stdcall lstrcat,EDI,EBX
stdcall MessageBoxA,0,EDI,Error,0
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
JMP Exit
NoConsole:
MOV dword[ExitStatus],1
stdcall MessageBoxA,0,NoConsoleMessage,Error,0
JMP Exit


global _RETURNWORD
_RETURNWORD:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

PUSH dword[Number_30+4]
PUSH dword[Number_30]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0


global _SENDWORD
_SENDWORD:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

XOR EAX,EAX
MOV AX,word[EBP+8]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_31+4]
PUSH dword[Number_31]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EAX
MOV word[EBP+8],AX
XOR EAX,EAX
MOV AX,word[EBP+8]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label110
JMP NoMemory
Label110:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label107:
CMP byte[EBX+ECX],0
JE Label108
INC ECX
CMP ECX,EDI
JL Label107
JMP Label109
Label108:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label109
MOV byte[EBX+ECX],0
Label109:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _WORDLOCAL
_WORDLOCAL:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
SUB ESP,4

XOR EAX,EAX
MOV AX,word[EBP+8]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_33+4]
PUSH dword[Number_33]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EAX
MOV word[EBP-8],AX
MOV EBX,String_34
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label112
JMP NoMemory
Label112:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_34
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
XOR EAX,EAX
MOV AX,word[EBP-8]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label116
JMP NoMemory
Label116:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label113:
CMP byte[EBX+ECX],0
JE Label114
INC ECX
CMP ECX,EDI
JL Label113
JMP Label115
Label114:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label115
MOV byte[EBX+ECX],0
Label115:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_35+4]
PUSH dword[Number_35]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]


MOV EAX,dword[EBP-4]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4



;Data section of the KoolB app
section .data
NoMemMessage db "Could not allocate memory.",0
NoLibFound db "Could not find library: ",0
NoFunctionFound db "Cound not find function: ",0
Error db "Error!",0
ParameterPool dd 0
HandleToHeap dd 0
TempQWord1 dq 0.0
TempQWord2 dq 0.0
True dd -1
Internal_HInstance dd 0
Internal_CommandLine dd 0
NoConsoleMessage db 'Error - Cannot access the console handles for Input/Output.',0
ConsoleTemp dd 0
ConsoleNewLine db 10,0
ConsoleClear db 'CLS',0
ConsolePause db 'PAUSE',0
HandleToInput dd 0
HandleToOutput dd 0
Scope0__W_Word dw 0
Number_1 dq 65535.0
Number_2 dq 10.0
Number_3 dq 32768.0
Number_4 dq -32769.0
STRUC Scope0__WORDTYPE_TYPE
.Scope0__X_Word resw 1
.Scope0__Y_Word resw 1
ENDSTRUC
Scope0__WT_UDT ISTRUC Scope0__WORDTYPE_TYPE
IEND
Number_5 dq 256.0
Number_6 dq 257.0
Scope0__WTCOPY_UDT ISTRUC Scope0__WORDTYPE_TYPE
IEND
Number_7 dq 4.0
Scope0__WORDARRAY_Word dd 0
Scope0__WORDARRAY_Word_Size dd 0
Number_8 dq 1.0
Number_9 dq 2.0
Number_10 dq 2.0
Number_11 dq 3.0
Number_12 dq 3.0
Number_13 dq 4.0
Number_14 dq 4.0
Number_15 dq 1.0
Number_16 dq 2.0
Number_17 dq 3.0
Number_18 dq 4.0
Number_19 dq 1.0
Number_20 dq 4567.0
Number_21 dq 3.0
Number_22 dq 1.0
String_23 db "WordArray(3) ",0
Number_24 dq 3.0
Number_25 dq 4.0
Scope0__WORDARRAYCOPY_Word dd 0
Scope0__WORDARRAYCOPY_Word_Size dd 0
Number_26 dq 1.0
Number_27 dq 2.0
Number_28 dq 3.0
Number_29 dq 4.0
Number_30 dq 42.0
Number_31 dq 1.0
Number_32 dq 60000.0
Number_33 dq 1.0
String_34 db "wdwork = wd + 1 ",0
Number_35 dq -1.0
Number_36 dq 99.0
ExitStatus dd 0

