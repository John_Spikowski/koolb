'****************
'* IntToStr.bas *
'****************

$AppType Console
$Optimize On
 
Declare Function MsgBox Lib "user32" Alias "MessageBoxA" _ 
   (hWnd As Integer, _ 
    lpText As String, _ 
    caption As String, _ 
    wType As Integer) _ 
    As Integer 

Function IntToStr(nInteger As Integer) As String
  $Asm
    extern sprintf
    jmp forward
    strFormat db '%ld',0
    forward:
    stdcall HeapAlloc,dword[HandleToHeap],8,11 
    mov ebx,eax 
    ccall sprintf,eax,strFormat,dword[ebp+8]
    mov dword[ebp-4],ebx
  $End ASM
End Function

MsgBox(0, IntToStr(-159), "IntToStr", 0)
MsgBox(0, IntToStr(56789), "IntToStr", 0)
