'*******************
'* $OptimizeOn.bas *
'*******************

$Optimize On

$Include "KoolB.inc"

'***

' The size went from 56832 bytes to 3584 bytes.  Not bad.

' Would it be possible to add "if used defs" around numbers like
' Number_17 dq 1.0 such that they would not be included in the exe?

' Would Fasm throw away the unused variables?

