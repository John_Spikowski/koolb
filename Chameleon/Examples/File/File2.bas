$Optimize ON

$Include "File.inc"

Dim File As Integer

$Const FileRead = "r"
$Const FileCreateWrite = "w"
$Const FileAppend = "a"
$Const FileReadWrite = "r+"
$Const FileCreateReadWrite = "w+"
$Const FileReadAppend = "a+"

File = FileOpen("Hello.txt", FileRead)

print "Reading from Hello.txt:"

print FileReadStr(File, len("Hello, testing...."))

print FileReadInt(File)

FileClose(File)

sleep 5
