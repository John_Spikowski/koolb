'**************
'* SizeOf.bas *
'**************

$AppType Gui
$Optimize Off 
$Compress Off 

$Include "String.inc"
$Include "Convert.inc"

Declare Function MsgBox Lib "user32" Alias "MessageBoxA" _ 
   (hWnd As Integer, _ 
   lpText As String, _ 
   caption As String, _ 
   wType As Integer) _ 
   As Integer 

Type MixedTypes
  Byte1 As Byte
  Byte2 As Byte
  SomeInt As Integer
  AWord As Word
  HerDouble As Double
  Byte3 As Byte
  BWord As Word
End Type  

Dim sMessage As String

Dim chByteSize As Byte 
Dim wWordSize As Word 
Dim nIntSize As Integer 
Dim fDoubleSize As Double
Dim mt As MixedTypes
Dim iDoubleSize As Integer

chByteSize  = SizeOf(chByteSize) 
wWordSize  = SizeOf(wWordSize) 
nIntSize  = SizeOf(nIntSize) 
iDoubleSize = SizeOf(fDoubleSize)

sMessage = "Byte size is " + StrI(chByteSize) + "." + CrLf() + _ 
           "Word size is " + StrI(wWordSize) + "." + CrLf() + _ 
           "Integer size is " + StrI(nIntSize) + "." + CrLf() + _ 
           "Double size is " + StrI(iDoubleSize) + "." + CrLf() + _
           "MixedTypes size is " + StrI(SizeOf(mt)) + "." + CrLf()

MsgBox(0, sMessage, "SizeOf", 0)
