'***********************
'* BackGroundColor.bas *
'***********************

$AppType GUI
$Optimize On
$Compress Off

$Include "Windows.inc" 

$Const Black   = 0x00000000 : $Const DkGray  = 0x00808080  
$Const LtGray  = 0x00C0C0C0 : $Const White   = 0x00FFFFFF  
$Const Red     = 0x000000FF : $Const Green   = 0x0000FF00  
$Const Blue    = 0x00FF0000 : $Const Yellow  = 0x0000FFFF  
$Const Magenta = 0x00FF00FF : $Const Cyan    = 0x00FFFF00

Dim message As MSG 
Dim wcex As WNDCLASSEX 
Dim ps As PAINTSTRUCT 
Dim rct As RECT
Dim hInst As Integer
Dim hWindow As Integer
Dim hMenu As Integer
Dim hMenuPopDown As Integer
Dim dwColor As Integer       'well, no dword :)
Dim hBrush As Integer
Dim strClassName As String
Dim strAppTitle As String

Function OnCommand(hWnd As Integer, uMsg As Integer, _ 
                   wParam As Integer, lParam As Integer) As Integer
  If wParam = 1011 Then
    PostMessage(hWnd, WM_CLOSE, 0, 0) 
  Else
    If wParam = 1001 Then : dwColor = Black : End If
    If wParam = 1002 Then : dwColor = DkGray : End If
    If wParam = 1003 Then : dwColor = LtGray : End If
    If wParam = 1004 Then : dwColor = White : End If
    If wParam = 1005 Then : dwColor = Red : End If
    If wParam = 1006 Then : dwColor = Green : End If
    If wParam = 1007 Then : dwColor = Blue : End If
    If wParam = 1008 Then : dwColor = Yellow : End If
    If wParam = 1009 Then : dwColor = Magenta : End If
    If wParam = 1010 Then : dwColor = Cyan : End If
    hBrush = CreateSolidBrush(dwColor) 
    SetClassLong(hWnd, GCL_HBRBACKGROUND, hBrush)
    GetClientRect(hWnd, rct)
    InvalidateRect(hWnd, rct, 1)
    UpdateWindow(hWnd)
    If hBrush <> 0 Then 
      DeleteObject(hBrush) 
    End If
  End If
  Result = 0
End Function
    
Function WindowProc(hWnd As Integer, uMsg As Integer, _ 
                    wParam As Integer, lParam As Integer) As Integer
  If uMsg = WM_COMMAND Then 
    Result = OnCommand(hWnd, uMsg, wParam, lParam)
  ElseIf uMsg = WM_DESTROY Then 
    PostQuitMessage(0) 
    Result = 0
  Else 
    Result = DefWindowProc(hWnd, uMsg, wParam, lParam)
  End If 
End Function

'***

strAppTitle = "Back Ground Color" 
strClassName = "BackGroundColorClass"

hInst = GetModuleHandle(0)
wcex.cbSize = SizeOf(WNDCLASSEX) 
wcex.style = CS_VREDRAW + CS_HREDRAW
wcex.lpfnwndproc = CodePtr(WindowProc) 
wcex.cbClsExtra = 0 
wcex.cbWndExtra = 0 
wcex.hInstance = hInst
wcex.hIcon = LoadIcon(0, MakeIntResource(IDI_APPLICATION)) 
wcex.hCursor = LoadCursor(0, MakeIntResource(IDC_ARROW)) 
wcex.hbrBackground = GetStockObject(WHITE_BRUSH)
wcex.lpszMenuName = "" 
wcex.lpszClassName = strClassName 
wcex.hIconSm = 0 

If (RegisterClassEx(wcex)) = 0 Then 
  MessageBox(0, "RegisterClassEx failed.", strAppTitle, MB_OK)
  ExitProcess(0)
End If 

hMenu = CreateMenu() 
hMenuPopdown = CreateMenu() 

AppendMenu(hMenuPopdown, MF_STRING, 1001, "Black") 
AppendMenu(hMenuPopdown, MF_STRING, 1002, "Gray (Dark)") 
AppendMenu(hMenuPopdown, MF_STRING, 1003, "Gray (Light)") 
AppendMenu(hMenuPopdown, MF_STRING, 1004, "White") 
AppendMenu(hMenuPopdown, MF_STRING, 1005, "Red") 
AppendMenu(hMenuPopdown, MF_STRING, 1006, "Green") 
AppendMenu(hMenuPopdown, MF_STRING, 1007, "Blue") 
AppendMenu(hMenuPopdown, MF_STRING, 1008, "Yellow") 
AppendMenu(hMenuPopdown, MF_STRING, 1009, "Magenta") 
AppendMenu(hMenuPopdown, MF_STRING, 1010, "Cyan") 
AppendMenu(hMenuPopdown, MF_SEPARATOR, 0, "") 
AppendMenu(hMenuPopdown, MF_STRING, 1011, "Exit") 
AppendMenu(hMenu, MF_POPUP ,hMenuPopdown, "Color")        
    
hWindow = CreateWindowEx(WS_EX_APPWINDOW + WS_EX_WINDOWEDGE, _ 
                         strClassName, strAppTitle, _
                         WS_OVERLAPPEDWINDOW + WS_VISIBLE, _ 
                         100, 100, 400, 400, _ 
                         0, hMenu, wcex.hInstance, 0) 
If hWindow = 0 Then 
  MessageBox(0, "CreateWindowEx failed.", strAppTitle, MB_OK)
  ExitProcess(0)
End If 

While GetMessage(message, 0, 0, 0) > 0
  TranslateMessage(message) 
  DispatchMessage(message) 
Wend 
