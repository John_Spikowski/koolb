' Who are you.bas - A humorous example program for the KoolB
' compiler to demonstrate the use of conditional statemtents 
' boolean expressions, and subs & functions
' By Brian C. Becker

Sub DoVisitor()
  Print " "
  Print "           Welcome vistor. Would you please identify yourself?"
  Print " "
  Input "What is your name? "; Name$
  If Name$ = "Jared" Then
    Print "A very special welcome to you, Jared!"
  Else
    Print "Welcome, " + Name$
  End If
  Print " "
  Print "Have a seat and I hope you enjoy your stay! Right now, I'll demonstrate KoolB's capabilities:"
  Sleep 2
  Dots& = 0
  While Dots& < 79
    Dots& = Dots& + 1
    Line$ = Line$ + "."
    Print Line$
    Sleep .1
  Wend
  While Dots& > 0
    Dots& = Dots& - 1
    Index& = 0
    Line$ = ""
    While Index& < Dots& + 1
      Line$ = Line$ + "."
      Index& = Index& + 1
    Wend
    Print Line$
    Sleep .1
  Wend
  Print " "
  Print "I'm terribly sorry, but I have to go now!"
  Sleep 4
  End
End Sub

Sub DoUser()
  Print " "
  Print "                               Welcome, user!"
  Print "Well, then, don't let me get in your way of your using KoolB - I'll just leave you alone and let you check out the source code to this wonderful app."
  Sleep 4
  Line$ = "                                    "
  Countdown& = 10
  While Countdown& > 0
    Print Line$;Countdown&
    Countdown& = Countdown& - 1
    Sleep Countdown& / 5 + .5
  Wend
  Print "                               Blast-off!"
  Sleep 1
  End
End Sub

Sub DoDeveloper()
  Print "Really now? You think you are a developer?"
  Print "I don't buy that - prove it to me!"
  Input "Give me your name: "; Name$
  If Name$ = "Brian" Or Name$ = "Brian Becker" Or Name$ = "Brian C. Becker" Or Name$ = "Jared" Then
    Print "Hmm, you seem to check out - but here's one last question to make sure you are who you say you are."
    Input "How old are you: "; Age#
    If Age# < 10 And Not Age# = 2 Or Age# > 80 Then
      Print "Security! Have these people shown to the doors right now!"
      Sleep 4
      End
    End If
    Print "Welcome developer!"
    Print "You have way to much work to do on KoolB to be messing around with this - go do it now!"
    Sleep 4
    End
  End If
  Print "Bah! I didn't think you were a developer! You just a fake - don't even bother coming in!"
  Sleep 5
  While 1 = 1
    Print "Ha! HA! HAAA!!! You deserve this for lying! Hint: Control + C "
  WEnd
End Sub


Str$ = "" + ""

Print "                   Welcome to the wonderful word of Wizz!"
Print " "
Print "Are you a:"
Print "  1. Visitor"
Print "  2. User"
Print "  3. Developer"
Print " "
Input "So! What will it be? "; Choice#
If Choice# = 1 Then
  DoVisitor()
ElseIf Choice# = 2 Then
  DoUser()
ElseIf Choice# = 3 Then
  DoDeveloper()
Else
  Print "Hey, stop messing with me! I'm not happy with your choice, so I'll just go and sulk. "
  Sleep 3
  End
End If