'Cut here.

$AppType GUI
$Optimize ON
$Include "Libraries/String.INC"
$Include "Libraries/System.INC"
$Include "Libraries/Convert.INC"

$CONST WS_EX_APPWINDOW = 262144
$CONST WS_BORDER = 8388608
$CONST SW_SHOW = 5
$Const CS_VREDRAW = 1
$Const CS_HREDRAW = 2

DECLARE FUNCTION DefWindowProc LIB "USER32" Alias "DefWindowProcA" _
                 (hWnd As Integer, Mesg As Integer, wParam As Integer, lParam As Integer) As Integer

DECLARE FUNCTION PostQuitMessage LIB "USER32" Alias "PostQuitMessage" _
                 (nExitCode As Integer) As Integer


'Swap winprocs and see CreateWindowEx crash!

'SUB WinProc()
'END SUB

FUNCTION WinProc  (hWnd AS Integer, uMsg AS Integer, wParam AS Integer, lParam AS Integer) AS Integer
    Result = DefWindowProc(hWnd, uMsg, wParam, lParam)
    If uMsg = 1 Then
      Result = 0
    End If
    IF uMsg = 2 Then
       PostQuitMessage(0)
    END IF
END FUNCTION

$Mangle OFF
TYPE WNDCLASS
    style As Integer
    lpfnwndproc As Integer
    cbClsextra As Integer
    cbWndExtra2 As Integer
    hInstance As Integer
    hIcon As Integer
    hCursor As Integer
    hbrBackground As Integer
    lpszMenuName As String
    lpszClassName As String
End Type

Declare Function GetLastError Lib "Kernel32.dll" Alias "GetLastError" () As Integer
Declare Function CreateMenu Lib "user32.dll" Alias "CreateMenu" () AS Integer
Declare Function ShowWindow Lib "user32.dll" Alias "ShowWindow" (ByVal hwnd As Integer, _
ByVal nCmdShow As Integer) As Integer
Declare Function LoadCursorFromFile Lib "user32" Alias "LoadCursorFromFileA" (ByVal lpFileName As String) As Integer
Declare Function RegisterClass Lib "user32.dll" Alias "RegisterClassA" (_ 
     byref Class As WNDCLASS) As Integer
Declare Function CreateWindowEx Lib "user32.dll" Alias "CreateWindowExA"( _ 
     ByVal dwExStyle As Integer, _ 
     ByVal lpClassName As String, _ 
     ByVal lpWindowName As String, _ 
     ByVal dwStyle As Integer, _ 
     ByVal x As Integer, _ 
     ByVal y As Integer, _ 
     ByVal nWidth As Integer, _ 
     ByVal nHeight As Integer, _ 
     ByVal hWndParent As Integer, _ 
     ByVal hMenu As Integer, _ 
     ByVal hInstance As Integer, _ 
     lpParam As Integer) As Integer
Declare Function GetModuleHandle Lib "kernel32.dll" Alias"GetModuleHandleA" ( _ 
     ByVal lpModuleName As String) As Integer

DIM WinClass As WNDCLASS
DIM hWnd As Integer
Dim Err As integer

Dim Class AS String
Class = "KoolBWin"

$Mangle ON


'I'm not super confindent about these values, but I think I got them
' right enough it shouldnt cuase probs.

WinClass.lpfnwndproc = AddressOf(WinProc)
WinClass.style = CS_HREDRAW + CS_VREDRAW
WinClass.cbClsextra = 0
WinClass.cbWndExtra2 = 0
WinClass.hCursor = LoadCursorFromFile("C:\Windows\Cursors\pen_i.cur")

'ShowMessage(Str$(WinClass.hCursor))

WinClass.hInstance = GetModuleHandle(Chr$(0))
'Either hInstance or MenuName are teh trouble, Im almsot sure of it. the example
'I'm going by is in VB, and VB takes numeric constants in strings, KoolB
' doesnt, Im not sure exactly what
' VB does to numerc constants.. I assume it jsut auto-Str$'s them. or
' Chr$, who knows.. VB Crazy like that.
'WinClass.lpszMenuName = 0
WinClass.lpszClassName = "KoolBWin"
WinClass.hbrBackground = 5

ShowMessage("Before Registering Class")

'Works, now
showmessage(str$(RegisterClass(WinClass)))

ShowMessage("After Registering Class") 'I never get to this point, SO, we know its registerclass thats mad.

'$Asm
'extern CreateWindowExA
'extern GetLastError
'extern ShowWindow
'PUSH 0
'PUSH dword[Internal_HInstance]
'PUSH 0
'PUSH 0
'PUSH 300
'PUSH 300
'PUSH 0
'PUSH 0
'PUSH 13565952
'PUSH dword[CLASS]
'PUSH dword[CLASS]
'PUSH 0
'CALL CreateWindowExA
'MOV dword[HWND],EAX
'CALL GetLastError
'MOV dword[ERR],EAX
'$End Asm

hWnd = CreateWindowEx(0, "KoolBWin", "WinKoolB", 13565952, 0, 0, 300, 300, 0, 0, hInstance, 0)

Err = GetLastError()

ShowMessage (Str$(hWnd) + " : " + Str$(Err))

ShowWindow(hWnd, SW_SHOW)

' A message loop?