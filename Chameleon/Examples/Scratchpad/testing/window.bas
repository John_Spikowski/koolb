$AppType GUI
$Optimize ON
$Include "Libraries/String.INC"
$Include "Libraries/System.INC"
$Include "Libraries/Convert.INC"
$CONST WS_EX_APPWINDOW = 262144
$CONST WS_BORDER = 8388608
$CONST SW_SHOW = 5
$Const CS_VREDRAW = 1
$Const CS_HREDRAW = 2
$Const WS_CAPTION = 12582912
SUB WinProc()
END SUB
$Mangle OFF
TYPE WNDCLASS
    lpfnwndproc As Integer
    cbClsextra As Integer
    cbWndExtra2 As Integer
    hInstance As Integer
    hIcon As Integer
    hCursor As Integer
    hbrBackground As Integer
    lpszMenuName As String
    lpszClassName As String
    style As Integer
End Type

Declare Function CreateMenu Lib "user32.dll" Alias "CreateMenu" () AS Integer

Declare Function ShowWindow Lib "user32.dll" Alias "ShowWindow" (ByVal hwnd As Integer, ByVal nCmdShow As Integer) As Integer
Declare Function LoadCursorFromFile Lib "user32" Alias "LoadCursorFromFileA" (ByVal lpFileName As String) As Integer
Declare Function GetLastError Lib "kernel32" Alias "GetLastError" () As Integer
Declare Function RegisterClass Lib "user32.dll" Alias "RegisterClassA" ( _ 
     byref Class As WNDCLASS) As Integer
Declare Function CreateWindowEx Lib "user32.dll" Alias "CreateWindowExA" ( _ 
     ByVal dwExStyle As Integer, _ 
     ByVal lpClassName As String, _ 
     ByVal lpWindowName As String, _ 
     ByVal dwStyle As Integer, _ 
     ByVal x As Integer, _ 
     ByVal y As Integer, _ 
     ByVal nWidth As Integer, _ 
     ByVal nHeight As Integer, _ 
     ByVal hWndParent As Integer, _ 
     ByVal hMenu As Integer, _ 
     ByVal hInstance As Integer, _ 
     lpParam As Integer) As Integer
Declare Function GetModuleHandle Lib "kernel32.dll" Alias "GetModuleHandleA" ( _ 
     ByVal lpModuleName As String) As Integer
DIM WinClass As WNDCLASS
DIM hWnd As Integer
$Mangle ON
'I'm not super confindent about these values, but I think I got them right enough it shouldnt cuase probs.
WinClass.lpfnwndproc = AddressOf(WinProc)
'ShowMessage(Str$(WinClass.lpfnwndproc))
WinClass.style = CS_HREDRAW + CS_VREDRAW
WinClass.cbClsextra = 0
WinClass.cbWndExtra2 = 0
WinClass.hCursor = LoadCursorFromFile("C:\Windows\Cursors\pen_i.cur")
'ShowMessage(Str$(WinClass.hCursor))
WinClass.hInstance = GetModuleHandle("window.exe")
'ShowMessage(Str$(WinClass.hInstance))
'Either hInstance or MenuName are teh trouble, Im almsot sure of it. the example
'I'm going by is in VB, and VB takes numeric constants in strings, KoolB doesnt, Im not sure exactly what
'VB does to numerc constants.. I assume it jsut auto-Str$'s them. or Chr$, who knows.. VB Crazy like that.
'WinClass.lpszMenuName = Chr$(0) 'I'm not sure if I should do this..
$ASM
   MOV dword [WINCLASS+WNDCLASS.LPSZMENUNAME],0 ; This should set it to NULL
$END ASM

ClassName$ = "KoolBWin"

WinClass.lpszClassName = ClassName$
WinClass.hbrBackground = 5
ShowMessage("Before Registering Class")
Result& = RegisterClassEx(WinClass)
'If Result& = 0 Then
  Error& = GetLastError()
  ShowMessage(Str$(Error&))
'End If
ShowMessage(Str$(Result&))
ShowMessage("After Registering Class") 'I never get to this point, SO, we know its registerclass thats mad.
Menu& = CreateMenu()
hWnd = CreateWindowEx(0, ClassName$, "WinKoolB", WS_CAPTION, 0, 0, 300, 300, 0, Menu&, WinClass.hInstance, 0)
hWnd = GetLastError()
ShowMessage (Str$(hWnd))
ShowWindow(hWnd, SW_SHOW)