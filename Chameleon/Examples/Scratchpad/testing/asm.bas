;Library functions to import to the KoolB app
extern ExitProcess
extern GetModuleHandleA
extern GetCommandLineA
extern HeapCreate
extern MessageBoxA
extern HeapDestroy


$Asm
;Initialize everything to prep the app to run
section .text USE32
%include "Asm\nagoa.inc"
global START
START:
PUSH 0
GetModuleHandleA
mov dword [Instance], eax
call GetCommandLineA
mov dword[CommandLine], eax
invoke WinMain, [Instance], NULL, eax, SW_SHOW

WinMain:
%define	hInstance	        ebp+8
%define   hPrevInstance	ebp+12
%define	lpCmdLine	        ebp+16
%define nCmdShow             ebp+20
enter 0,0
mov dword [WndClassEx+WNDCLASSEX.cbSize], WNDCLASSEX_size
mov dword [WndClassEx+WNDCLASSEX.style], (CS_HREDRAW)|(CS_VREDRAW)
mov dword [WndClassEx+WNDCLASSEX.lpfnWndProc], WindowProc
mov dword [WndClassEx+WNDCLASSEX.cbClsExtra], 0 
mov dword [WndClassEx+WNDCLASSEX.cbWndExtra], 0 
mov eax, dword [hInstance]
mov dword [WndClassEx+WNDCLASSEX.hInstance], eax
call LoadIconA, NULL, IDI_APPLICATION
mov dword [WndClassEx+WNDCLASSEX.hIcon], eax
mov dword [WndClassEx+WNDCLASSEX.hIconSm], eax
call LoadCursorA, NULL, IDC_ARROW
mov dword [WndClassEx+WNDCLASSEX.hCursor], eax
mov dword [WndClassEx+WNDCLASSEX.hbrBackground], COLOR_BTNFACE+1
mov dword [WndClassEx+WNDCLASSEX.lpszMenuName], NULL
mov dword [WndClassEx+WNDCLASSEX.lpszClassName], ClassName
call RegisterClassExA, WndClassEx
test eax, eax
JNZ DoAndShow
DoAndShow:
call CreateWindowExA, NULL,ClassName, WindowName, (WS_OVERLAPPED)|(WS_VISIBLE)|(WS_CAPTION)|(WS_SYSMENU),CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, [Instance], NULL
test eax, eax
je near Erro
mov dword [WindowHandle], eax
call ShowWindow, [WindowHandle], [nCmdShow]
Messageloop:
call GetMessageA, Msg, NULL, 0, 0
test eax, eax
je Fim
cmp eax, -1
je Erro
call TranslateMessage,Msg
call DispatchMessageA,Msg
jmp Messageloop
Fim:
mov eax, dword[Msg+MSG.wParam]
leave
ret 16
Erro:
xor eax,eax
leave
ret 16
WindowProc:
%define	hwnd 	ebp+8
%define	uMsg	ebp+12
%define   wParam	ebp+16
%define   lParam	ebp+20
enter 0, 0
mov eax, dword [uMsg]
cmp eax, WM_DESTROY
je near Destroy
cmp eax, WM_CREATE
je near Create
cmp eax, WM_COMMAND
je near Command
Default:
call DefWindowProcA, [hwnd], [uMsg], [wParam], [lParam]
leave
ret 16
Create:
xor eax, eax
leave
ret 16
Command:
mov eax, dword [wParam]
xor eax, eax
leave
ret 16
Destroy:
call PostQuitMessage, 0
xor eax, eax
leave
ret 16
stdcall HeapCreate,0,16384,0
MOV dword[HandleToHeap],EAX
CMP dword[HandleToHeap],0
JNE Label1
JMP NoMemory
Label1:
stdcall GetModuleHandleA,0
MOV dword[Internal_HInstance],EAX



;The main body of the app where the app actually runs



;Prepare the app to exit and then terminate the app
Exit:
stdcall HeapDestroy,dword[HandleToHeap]
POP EDI
POP ESI
POP EBX
MOV ESP,EBP
POP EBP
MOV EAX,dword[ExitStatus]
RET

NoMemory:
MOV dword[ExitStatus],1
stdcall MessageBoxA,0,NoMemMessage,Error,0
JMP Exit



;Data section of the KoolB app
section .data USE32
NoMemMessage db "Could not allocate memory. Please free up some memory and re-run this app. Thanks!",0
Error db "Error!",0
ParameterPool dd 0
HandleToHeap dd 0
TempQWord1 dq 0.0
TempQWord2 dq 0.0
True dd -1
Internal_HInstance dd 0
Internal_CommandLine dd 0
Scope0__BLAH_QFORM
ClassName db "myWindowClassName" ;
Instance dd 0
CommandLine dd 0
WndClassEx times WNDCLASSEX_size db 0
WindowName db "Blah",0
WindowHandle dd 0
Msg times MSG_size db 0
ExitStatus dd 0


$End Asm