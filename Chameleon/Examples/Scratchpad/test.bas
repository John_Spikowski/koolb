' Test DLL 
' Example program for KoolB by Brian Becker

'$AppType DLL
$Optimize Off

$Mangle OFF

TYPE ABC
  A As Integer
  B As Integer
  C As Integer
End Type

Dim Food As ABC

Dim I As Integer, B As String

Function Concat(S1 As string, S2 As String) As String
  Result = S1 + S2
End Function
$Mangle ON

Function Sqrt(Number As Double) As Double
  Result = Number ^ (1/2)
End Function

print AddressOf(Sqrt)

sleep 4
