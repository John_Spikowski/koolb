$AppType Console

$Const IDC_ARROW = 32512 
$Const IDI_WINLOGO = 32517 
$Const CS_CLASSDC = 64 
$Const WM_DESTORY = 2 
$Const WS_EX_TOPMOST = 8 
$Const WS_SYSMENU = 524288 
$Const CW_USEDEFAULT = 2147483648 

TYPE TWNDCLASSEX 
cbSize AS Integer 
style AS Integer 
lpfnWndProc AS Integer 
cbClsExtra AS Integer 
cbWndExtra AS Integer 
hInstance AS Integer 
hIcon AS Integer 
hCursor AS Integer 
hbrBackground AS Integer 
lpszMenuName AS Integer 
lpszClassName AS Integer 
hIconSm AS Integer 
END TYPE 
TYPE TMSG 
hwnd AS Integer 
message AS Integer 
wParam AS Integer 
lParam AS Integer 
time AS Integer 
x as Integer 
y as Integer 
END TYPE 

DECLARE FUNCTION RegisterClassEx LIB "USER32" ALIAS "RegisterClassExA" _ 
(WndClass AS TWNDCLASSEX) AS Integer 

DECLARE FUNCTION GetMessage LIB "USER32" ALIAS "GetMessageA" _ 
(lpMsg AS TMSG, hWnd AS Integer, wMsgFilterMin AS Integer, _ 
wMsgFilterMax AS Integer) AS Integer 

DECLARE FUNCTION LoadCursor LIB "USER32" ALIAS "LoadCursorA" _ 
(hInst AS Integer, lpCursorName AS Integer) AS Integer 

DECLARE FUNCTION LoadIcon LIB "USER32" ALIAS "LoadIconA" _ 
(hInst AS Integer, lpIconName AS Integer) AS Integer 

DECLARE FUNCTION CreateWindowEx LIB "USER32" ALIAS "CreateWindowExA" _ 
(ExStyle As Integer, ClassName As String, WindowName As String, _ 
Style As Integer, X As Integer, Y As Integer, Width As Integer, _ 
Height As Integer, WndParent As Integer, hMenu As Integer, _ 
hInstance As Integer, Param As Integer) AS Integer 

DECLARE FUNCTION UpdateWindow LIB "USER32" ALIAS "UpdateWindow" _ 
(hWnd AS Integer) AS Integer 

DECLARE FUNCTION DefWindowProc LIB "USER32" ALIAS "DefWindowProcA" _ 
(hWnd AS Integer, Msg AS Integer, wParam AS Integer, lParam AS Integer) AS Integer 

DECLARE FUNCTION PostQuitMessage LIB "USER32" ALIAS "PostQuitMessage" _ 
(nExitCode AS Integer) AS Integer 

DECLARE FUNCTION TranslateMessage LIB "USER32" ALIAS "TranslateMessage" _ 
(lpMsg AS TMSG) AS Integer 

DECLARE FUNCTION DispatchMessage LIB "USER32" ALIAS "DispatchMessageA" _ 
(lpMsg AS TMSG) AS Integer 

Declare Function GetModuleHandle Lib "kernel32.dll" Alias "GetModuleHandleA" _ 
(ByVal lpModuleName As String) As Integer 

' *** Everything checked OK to here. 

' *** 
DECLARE FUNCTION GetDesktopWindow LIB "USER32" ALIAS "GetDesktopWindow" _ 
() AS Integer 

DECLARE FUNCTION MessageBox LIB "user32" ALIAS "MessageBoxA" _ 
(hwnd As Integer, text As String, caption As String, wType As Integer) As Integer 

DIM hDtw As Integer 
DIM r As Integer 
hDtw = GetDesktopWindow() 

Sub PrintDebug(S as String) 
r = MessageBox(hDtw,S,"Debug",0) 
End Sub 
' *** 

DIM WndClassEx AS TWNDCLASSEX 
DIM Msg AS TMSG 
DIM ClassName As String 
ClassName = "KoolB" 
DIM Handle As Integer 

' *** 
Function AddrStr(S as String) As Integer 
' I'm not sure this returns the correct value. 
$Asm 
MOV EAX,dword[EBP+8] 
MOV dword[EBP-4],EAX 
$End Asm 
End Function 
' *** 

FUNCTION WindowProc (hWnd AS Integer, uMsg AS Integer, wParam AS Integer, _ 
lParam AS Integer) AS Integer 
Result = DefWindowProc(hWnd, uMsg, wParam, lParam) 
IF uMsg = WM_DESTORY THEN 
PostQuitMessage(0) 
END IF 
END FUNCTION 

' *** This will not compile under v14 *** 
WndClassEx.cbSize = SIZEOF(WndClassEx) 
' But this will: 
'WndClassEx.cbSize = 48 
' *** 

WndClassEx.style = CS_CLASSDC 
WndClassEx.lpfnWndProc = CodePTR(WindowProc) 
WndClassEx.cbClsExtra = 0 
WndClassEx.cbWndExtra = 0 
WndClassEx.hInstance = hInstance 'GetModuleHandle("win2.exe") 
WndClassEx.hbrBackground = 12632256 
WndClassEx.lpszMenuName = 0 

' *** This will not compile under v14 *** 
WndClassEx.lpszClassName = AddressOf(ClassName) 
' But this will: 
'WndClassEx.lpszClassName = AddrStr(ClassName) 
' *** 

WndClassEx.hCursor = LoadCursor(0, IDC_ARROW) 
WndClassEx.hIcon = LoadIcon(0, IDI_WINLOGO) 
WndClassEx.hIconSm = LoadIcon(0, IDI_WINLOGO) 

' *** 
PrintDebug("WndClassEx load completed") 
r = RegisterClassEx(WndClassEx) 
If r = 0 Then 
PrintDebug("RegisterClassEx FAILED") 
Else 
PrintDebug("RegisterClassEx OK") 
End If 
' *** 

Handle = CreateWindowEx(WS_EX_TOPMOST, ClassName, "KoolB", WS_SYSMENU, _ 
CW_USEDEFAULT, CW_USEDEFAULT, 320, 240, 0, 0, WndClassEx.hInstance, 0) 
UpdateWindow(Handle) 
WHILE (GetMessage(Msg, 0, 0, 0)) '-- Main message loop 
TranslateMessage(Msg) 
DispatchMessage(Msg) 
WEND 
