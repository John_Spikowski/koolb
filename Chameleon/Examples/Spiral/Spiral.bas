'**************
'* Spiral.bas *
'**************

' Based on a Petzold example. 

$AppType GUI 
$Optimize Off
$Compress Off

$Include "Windows.inc"
$Include "Math.inc"

$Const iNumRevs = 20
$Const crAppleGreen = 0x0044AA00
$Const PI = 3.14159

Dim message As MSG 
Dim wc As WNDCLASS
Dim ps As PAINTSTRUCT 
Dim strClassName As String
Dim strAppTitle As String
Dim hWindow As Integer
Dim cxClient As Integer
Dim cyClient As Integer

Function OnSize(hWnd As Integer, uMsg As Integer, _ 
                wParam As Integer, lParam As Integer) As Integer
  cxClient = LoWord(lparam)
  cyClient = HiWord(lparam)
  Result = 0
End Function

Function OnPaint(hWnd As Integer, uMsg As Integer, _ 
                wParam As Integer, lParam As Integer) As Integer
  Dim hdc As Integer
  Dim iNumPoints As Integer
  Dim ptX As Integer 
  Dim ptY As Integer    
  Dim i As Integer
  Dim fAngle As Double 
  Dim fScale As Double

  hdc = BeginPaint(hWnd, ps)
  iNumPoints = iNumRevs * 2 * (cxClient + cyClient)
  i = 0
  While i < iNumPoints
    fAngle = i * 2.0 * PI / (iNumPoints / iNumRevs)
    fScale = 1.0 - i / iNumPoints
    ptX = cxClient / 2.0 * (1.0 + fScale * Cos(fAngle))
    ptY = cyClient / 2.0 * (1.0 + fScale * Sin(fAngle))
    SetPixel(hdc, ptX, ptY, crAppleGreen)
    i = i +1
  Wend
  EndPaint(hWnd, ps) 
  Result = 0
End Function

Function WindowProc(hWnd As Integer, uMsg As Integer, _ 
                    wParam As Integer, lParam As Integer) As Integer
  If uMsg = WM_SIZE Then 
    Result = OnSize(hWnd, uMsg, wParam, lParam)
  ElseIf uMsg = WM_PAINT Then
    Result = OnPaint(hWnd, uMsg, wParam, lParam)
  ElseIf uMsg = WM_DESTROY Then 
    PostQuitMessage(0) 
    Result = 0
  Else 
    Result = DefWindowProc(hWnd, uMsg, wParam, lParam)
  End If 
End Function

'***

strAppTitle = "Spiral" 
strClassName = "SpiralClass"

wc.style = CS_HREDRAW + CS_VREDRAW
wc.lpfnWndProc = CodePtr(WindowProc) 
wc.cbClsExtra = 0
wc.hInstance = GetModuleHandle(0)
wc.hIcon = LoadIcon(0, MakeIntResource(IDI_APPLICATION)) 
wc.hCursor = LoadCursor(0, MakeIntResource(IDC_ARROW)) 
wc.hbrBackground = GetStockObject(WHITE_BRUSH)
wc.lpszMenuName = ""
wc.lpszClassName = strClassName

If (RegisterClass(wc)) = 0 Then 
  MessageBox(0, "RegisterClass failed.", strAppTitle, MB_OK)
  ExitProcess(0)
End If 

hWindow = CreateWindowEx(0, strClassName, strAppTitle, _
                         WS_OVERLAPPEDWINDOW, _
                         165, 50, 380, 435, _
                         0, 0, wc.hInstance, 0) 
If hWindow = 0 Then 
  MessageBox(0, "CreateWindowEx failed.", strAppTitle, MB_OK)
  ExitProcess(0)
End If 

ShowWindow(hWindow, SW_SHOWNORMAL)
UpdateWindow(hWindow)

While GetMessage(message, 0, 0, 0) > 0
  TranslateMessage(message) 
  DispatchMessage(message) 
Wend 
