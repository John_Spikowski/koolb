'***********************
'* HelloWinCompare.bas *
'***********************

$AppType GUI
 
' Compare exe file sizes using KoolB directives.

'***      Optimize Compress Exe Size
'***      -------- -------- --------
'***           Off      Off    68606
'***           Off       On    10240
'***            On      Off    15872
'***            On       On     5120 upx

'$Optimize Off
'$Compress Off

'$Optimize Off
'$Compress On

'$Optimize On
'$Compress Off

$Optimize On
$Compress On

$Include "KoolB.inc" 

Dim wcex As WNDCLASSEX 
Dim message As MSG 
Dim ps As PAINTSTRUCT 
Dim rc As RECT
Dim hWnd As Integer 
Dim strClassName As String
Dim r As Integer 

strClassName = "KoolBClass"

Function WindowProc(hWnd As Integer, uMsg As Integer, _ 
                    wParam As Integer, lParam As Integer) As Integer
  Dim hDc As Integer 

  If uMsg = WM_DESTROY Then 
    PostQuitMessage(0) 
    Result = 0
  ElseIf uMsg = WM_CREATE Then
    PlaySound("HelloHelloHello.wav", 0, SND_FILENAME + SND_ASYNC) 
    Result = 0
  ElseIf uMsg = WM_PAINT Then 
    hDC = BeginPaint(hWnd, ps) 
    GetClientRect(hWnd, rc)
    DrawText(hDC, "A KoolB Hello World!", -1, rc, _
             DT_SINGLELINE + DT_CENTER + DT_VCENTER)  
    EndPaint(hWnd, ps) 
    Result = 0
  Else 
    Result = DefWindowProc(hWnd, uMsg, wParam, lParam)
  End If 
End Function

wcex.cbSize = SizeOf(WNDCLASSEX) 
wcex.style = CS_VREDRAW + CS_HREDRAW + CS_CLASSDC 
wcex.lpfnwndproc = CodePtr(WindowProc) 
wcex.cbClsExtra = 0 
wcex.cbWndExtra = 0 
wcex.hInstance = GetModuleHandle(0) 
wcex.hIcon = LoadIcon(0, MakeIntResource(IDI_APPLICATION)) 
wcex.hCursor = LoadCursor(0, MakeIntResource(IDC_ARROW)) 
wcex.hbrBackground = COLOR_WINDOW + 1 
wcex.lpszMenuName = ""
wcex.lpszClassName = strClassName
wcex.hIconSm = 0 

If (RegisterClassEx(wcex)) = 0 Then 
  MessageBox(0, "RegisterClassEx failed.", "HelloWin", 0)
  ExitProcess(0)
End If 

hWnd = CreateWindowEx(0, strClassName, "HelloWin", _ 
                      WS_OVERLAPPEDWINDOW + WS_VISIBLE, _
                      CW_USEDEFAULT, CW_USEDEFAULT, _ 
                      CW_USEDEFAULT, CW_USEDEFAULT, _ 
                      0, 0, wcex.hInstance, 0) 
If hWnd = 0 Then 
  MessageBox(0, "CreateWindowEx failed.", "HelloWin", 0)
  ExitProcess(0)
End If 

While 0 = 0 
  r = GetMessage(message, 0, 0, 0) 
  If r = 0 Then
    ExitProcess(0)
  End If
  TranslateMessage(message) 
  DispatchMessage(message) 
Wend 
