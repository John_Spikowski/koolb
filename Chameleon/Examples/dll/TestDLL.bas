' Test program for KoolB generated Test.dll
' KoolB example program by Brian Becker

$AppType Console

Declare Function ConCat Lib "Test.dll" Alias "_CONCAT" (S1 As String, S1 As String) As String
Declare Function Sqrt Lib "Test.dll" Alias "_SQRT" (Number As Double) As Double

Print "Testing functions contained in Test.dll"
Print ""
Sleep 1
Print "Concat(123, abc)   =>   "; Concat("123", "abc")
Sleep 1
Print "Sqrt(9)            =>   "; Sqrt(9)
Sleep 5