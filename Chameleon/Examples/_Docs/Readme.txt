-----------------------------------------------------------------------
|                  KoolB Compiler Version 15 Release                  |
|                           Programmed By                             |
|                          Brian C. Becker                            |
|                       www.BrianCBecker.com                          |
|                             05-16-03                                |
-----------------------------------------------------------------------

-----------------------------------------------------------------------
Important Note:
-----------------------------------------------------------------------
KoolB, my school Personal Project, is now finished. Version 15 is the
last release of KoolB. No new features have been added, although
a lot of work has gone into this release, including:
 - In-depth comments for the source code
 - Cleaning up the source code and re-formatting
 - Many bug-fixes


-----------------------------------------------------------------------
About KoolB
-----------------------------------------------------------------------
KoolB is a free, open-source Windows and Linux BASIC compiler written 
in C++. It works by translating your BASIC source code to assembly 
language and then uses the Netwide Assembler and a linker to produce 
a native Windows or Linux executable. 

It has many of the features found in QBASIC and can be used as a 
learning tool (by studying the source) or for simple programming
projects.


-----------------------------------------------------------------------
KoolB's Features:
-----------------------------------------------------------------------
 - Completely cross-platform for both Windows & Linux
 - Produces small and fast programs (minimal program is 3KB)
 - Handles simple data types (Integer, Double, String)
 - Handles arrays of simple data types
 - Handles UDTs of simple data types
 - Full support for both string and numeric expressions
 - Primitive console Input & Output
 - Boolean expressions (And, Or, Not)
 - Relational operators (=, <>, etc)
 - If...Then...ElseIf...Else...End If statements
 - While...Wend loops
 - User defined functions & subs
 - Importing external functions (WinAPI & custom DLLs)
 - Directives ($Const, $Define, $IfDef, $End If, etc)
 - $Asm directive to use assembly language inline
 - Support for GUI, Console, and DLL programs
 - Optimization to remove unused functions in libraries
 - Automatic compression to your program or DLL


-----------------------------------------------------------------------
KoolB Package:
-----------------------------------------------------------------------
The KoolB distribution contains several notable items:
 - KoolB.exe
   The actual KoolB compiler. To use it, just use the command line:
   KoolB <filename>. Or, if you prefer, just open KoolB and it will
   ask you for the name of the file to compile. It is typically 
   better to use the command line so you can ready any errors your
   source code might have.

 - Readme.txt
   This file, which gives you an overview of KoolB.

 - Asm folder
   The Asm folder holds all files KoolB uses to function properly.
   It contains some assembly language macros, the Netwide Assembler 
   (NASM) to assemble the assembly language KoolB generates to
   object files, GoRC to produce object files from the resource
   file, GoLink to link the object files to an executable.

 - Docs folder
   Contains some old, outdated documentation on KoolB. For more up to
   date information, see my website at www.BrianCBecker.com

 - Examples folder
   Contains some programs written in KoolB that demonstrate KoolB's 
   capabilities.

 - Libraries folder
   Contains the standard libraries for KoolB. Modify these files to add
   new functions to the standard libraries, or if you need to modify
   a particular library for your uses. To include all libraries in your
   program, use:

   $Include "KoolB.inc"

 - Source folder
   Contains the C++ source code to KoolB. To compile, use a C++ compiler
   like the free Dev-C++ (www.bloodshed.net). If you have Dev-C++, you 
   can open the KoolB.dev project file.

 - HTML Source folder
   Contains the HTML, syntax-highlighted C++ source code for KoolB. 
   This makes reading the source code a tad bitt easier to read.


-----------------------------------------------------------------------
Bug reports, comments, suggestions, and support are available at:
-----------------------------------------------------------------------
 - http://www.BrianCBecker.com/Me.Contact
 - http://groups.yahoo.com/group/qdepartment


-----------------------------------------------------------------------
|                             Enjoy!                                  |
-----------------------------------------------------------------------
