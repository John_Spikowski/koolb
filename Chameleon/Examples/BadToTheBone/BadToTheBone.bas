'********************
'* BadToTheBone.bas *
'********************

$AppType Gui
$XPStyle

$Include "Windows.inc"
$Include "Bitwise.inc"

Dim hMod As Integer
Dim hIcon As Integer

$Const FALSE = 0
$Const TRUE = -1
$Const IDC_PLAY = 101

$Resource BadToTheBone As "BadToTheBone.dlg"
$Resource IconRes As "BadToTheBone.ico"
$Resource WaveRes As "BadToTheBone.wav"

Function DialogProc(hWndDlg As Integer, uMsg As Integer, _
                    wParam As Integer, lParam As Integer) As Integer
  If uMsg = WM_INITDIALOG Then
    hIcon = LoadIcon(hMod, MakeIntResource(IconRes))
    SendMessage(hWndDlg, WM_SETICON, ICON_SMALL, hIcon)
    Result = FALSE
  ElseIf uMsg = WM_COMMAND Then
    If wParam = ShiftLeft(BN_CLICKED, 16) + IDC_PLAY Then
      PlaySound(MakeIntResource(WaveRes), hMod, SND_RESOURCE + SND_ASYNC)
      Result = TRUE
    End if  
  ElseIf uMsg = WM_CLOSE then
    DestroyIcon(hIcon)
    EndDialog(hwndDlg, 0)
    Result = FALSE
  Else
    Result = FALSE
  End If  
End Function

hMod = GetModuleHandle(0)
DialogBoxParam(hMod, "BADTOTHEBONE", HWND_DESKTOP, CodePtr(DialogProc), 0)

