' KoolB Standard Library
' Created for KoolB Chapter 13
' Created by Brian C. Becker & Jared Ingersol

$IfNDef KoolBinc
$Define KoolBinc

$CONST False = 0
$CONST True = -1

$Include "Misc.inc"
$Include "Math.inc"
$Include "Type.inc"
$Include "System.inc"
$Include "String.inc"
$Include "Convert.inc"
$Include "Console.inc"
$Include "Time.inc"

$End If