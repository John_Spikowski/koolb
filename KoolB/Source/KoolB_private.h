/* THIS FILE WILL BE OVERWRITTEN BY DEV-C++ */
/* DO NOT EDIT ! */

#ifndef KOOLB_PRIVATE_H
#define KOOLB_PRIVATE_H

/* VERSION DEFINITIONS */
#define VER_STRING	"13.0.0.0"
#define VER_MAJOR	13
#define VER_MINOR	0
#define VER_RELEASE	0
#define VER_BUILD	0
#define COMPANY_NAME	"Brian Becker"
#define FILE_VERSION	"13"
#define FILE_DESCRIPTION	"KoolB Compiler"
#define INTERNAL_NAME	""
#define LEGAL_COPYRIGHT	"None"
#define LEGAL_TRADEMARKS	"None"
#define ORIGINAL_FILENAME	"KoolB.exe"
#define PRODUCT_NAME	"KoolB"
#define PRODUCT_VERSION	"13"

#endif /*KOOLB_PRIVATE_H*/
