/******************************************************************************
 *|--------------------------------------------------------------------------|*
 *|                        KoolB Compiler Version 15                         |*
 *|                           Brian C. Becker                                |*
 *|                                                                          |*
 *| Main.cpp - Controls all the other files and starts everything            |*
 *|--------------------------------------------------------------------------|*
 *****************************************************************************/

// Disables the old MS Visual C++ 6 warnings about debugging symbols exceeding 255 chars
#pragma warning(disable:4786)

//Include the necessary libraries
#include <string> 
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <map>
#include <algorithm>
using namespace std;

//Set the OS version
#define Windows
//#define Linux

#ifdef Windows
#include <windows.h>
#endif

//Global settings for program
enum AppType{GUI, Console, DLL};
int  AppType  = Console;
bool Optimize = false;
bool Compress = false;
bool Mangle   = true;

//Globals to keep track of the compile time
double StartTime;
double TempTime;
int    Pause = 0;

//Include the rest of our modules and objects
#include "Misc.h"
#include "Read.h"
  Reading Read;
#include "Database.h"
  Database Data;
#include "Write.h"
  Writing Write;
#include "Assembly.h"
  Assembly Asm;
#include "Errors.h"
  Errors Error;
#include "Compiler.h"
  Compiler CompileIt;

//Our core routines for compiling
void Start();
void Compile(int argc, char *argv[]);
void Stop();

/******************************************************************************
main - prints out the welcome and then starts the ball rolling
******************************************************************************/
int main(int argc, char *argv[]){
  printf("\n          Welcome to KoolB 15.01 by Brian C. Becker!\r\n");
  #ifdef Windows
  printf("\n           Your open-source Windows BASIC compiler!\n\r\n");
  #endif
  #ifdef Linux
  printf("\n            Your open-source Linux BASIC compiler!\n\r\n");
  #endif
  Start();
  Compile(argc, argv);
  Stop();
  Sleep(Pause);
  return 0;
}

/******************************************************************************
Start - keeps track of the time needed to compile
******************************************************************************/
void Start(){
  StartTime = clock();
  TempTime  = StartTime;
  return;
}

/******************************************************************************
Compile - gets the file to compile, open it, passes it off to the CompileIt 
object, and then assembles and links the app.
******************************************************************************/
void Compile(int argc, char *argv[]){
  string FileName;
  string TargetOS;
  //If we have too few or too many parameters, ask for the filename
  //Also, pause for several seconds so the user can read the output before the 
  //console window closes
  if (argc != 2){
    printf("Usage: KoolB <filename>\nEnter Filename: ");
    char * InFile = new char[MAX_PATH];
    scanf("%s", FileName.c_str());
    FileName = InFile;
    delete[] InFile;
    printf("\r\n");
    Pause = 4;
  }
  else{
    FileName = argv[1];
  }
  //Get the Reading object to open the file
  Read.OpenBook(FileName);
  printf("Currently compiling \"%s\":\r\n", FileName.c_str());
  //Tell the CompileIt object to generate the assembly language
  CompileIt.Compile(Read, false);
  //Clean up the assembly language
  Asm.FinishUp();
  //The compile time is....<drum roll, please>
  printf(" - Compile time  ->  %f seconds\r\n", (double)(clock() - TempTime) / 
                                            (double)CLK_TCK);
  TempTime = clock();
  //Assemble and link the app
  Write.BuildApp(FileName);
  return;
}

/******************************************************************************
Stop - prints out the total time necessary for compiling, assembling, & linking
******************************************************************************/
void Stop(){
  printf("   -------------------------------\r\n");
  printf(" - Total time    ->  %f seconds\r\n", (double)(clock() - StartTime) / 
                                            (double)CLOCKS_PER_SEC);
  return;
}
