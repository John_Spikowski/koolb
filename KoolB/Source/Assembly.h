/******************************************************************************
 *|--------------------------------------------------------------------------|*
 *|                        KoolB Compiler Version 15                         |*
 *|                           Brian C. Becker                                |*
 *|                                                                          |*
 *| Assembly.h - the real work horse that generates the assembly language    |*
 *|--------------------------------------------------------------------------|*
 *****************************************************************************/
 
#ifndef Assembly_h
#define Assembly_h

#include <string>
using namespace std;

//The object that outputs the raw assembly language - closely tied to the 
//Compiler object.
class Assembly{
 private:
   int LabelNumber;      //Keeps track of temporary labels we use
   int TempVariable;     //Keeps track of the variables we store in the .data
                         //section of the app

 public:
   //Intialize the LabelNumber and Tempvariable to 0
   Assembly(){LabelNumber = 0; TempVariable = 0;}
   void AddLibrary(string FunctionName);
   string GetLabel();
   void PostLabel(int Section, string Label);
   void BuildSkeleton();
   void FinishUp();
   void InitMemory();
   void GetPreSetVariables();
   void PrepareErrorMessages();

   void CreateInteger(string Name, string AsmName);
   void CreateDouble(string Name, string AsmName);
   void CreateString(string Name, string AsmName);

   void CreateIntegerArray(string Name, string AsmName);
   void CreateDoubleArray(string Name, string AsmName);
   void CreateStringArray(string Name, string AsmName);

   void StartCreatingType(string TypeName);
   void CreateTypeInteger(string Typename, string Name, string AsmName);
   void CreateTypeDouble(string Typename, string Name, string AsmName);
   void CreateTypeString(string Typename, string Name, string AsmName);
   void FinishCreatingType(string TypeName);

   void CreateUDT(string UDT, string Name, string AsmName);

   void LoadString(string String);
   void LoadNumber(string Number);
   void LoadUDTMember(string UDT, string TypeName, string Member, int Type);
   void LoadArrayItem(string Array, int Type);
   void LoadDefaults(int Type);

   void AllocMemory(int Section, string Size);
   void FreeMemory(int Section, string Name);
   void AllocStringArray(string AsmName);
   void FreeStringArray(string AsmName);
   void CopyArray(string Array);
   void CopyStringArray(string Array);
   void AddStrings();
   void GetStringLength(string String);
   void CopyUDT(string UDT);
   void CopyMemoryOfSize(string To, string From, string Size);
   void CopyString(string To, string From);
   void AppendString(string To, string From);
   void AssignIt(string Name);
   void AssignUDTMember(string UDT, string TypeName, string Member, int Type);
   void AssignArrayItem(string Array, int Type);
   
   void CalculateStart();
   void CalculateExp();
   void CalculateMultiply();
   void CalculateDivide();
   void CalculateMOD();
   void CalculateAddition();
   void CalculateSubtract();
   void CalculateEnd();
   void CompareNumbers();
   void LoadNumberRelation(int Relation);
   void CompareStrings();
   void LoadStringRelation(int Relation);
   void PushNumber(int Number);
   void PushAddress(string Name, int Type);
   
   void MOV(int Section, string WhereTo, string What);
   void PUSH(string What);
   void POP(string What);
   
   void RoundToInteger();
   void ConvertToString();
   void ConvertToNumber();
   void Negate();
   
   void InitConsole();
   void ConsoleSleep();
     void FormatTime();
   void ConsoleCls();
   void ConsolePrint(int Type);
     void ConsolePrintNewLine();
   void ConsoleInput();
   
   void EndProgram();
   void Or();
   void And();
   void Not();
   
   string StartIf();
   string StartNewIfSection(string Endlabel, string DoneIf);
   string ElseIf(string EndLabel, string DoneIf);
   string Else(string EndLabel, string DoneIf);
   void EndIf(string EndLabel, string DoneIf);
   string PrepareWhile();
   string StartWhile();
   void StopWhile(string StartWhileLabel, string EndWhileLabel);
   
   void CreateSubFunction(string Name, bool External);
   void EndCreateSubFunction(string Name, bool External);
   void InvokeSubFunction(string Name);
   void ReturnValue(string Name, string Type);
   void CleanUpReturnValue(string Name, string Type);
   void PushReturnValue(string Name, string Type);
   void LoadExternalSubFunction(string Name);
   void CallExternalSubFunction(string Name);
   void AllocateParameterPool(int Size);
   void AddToParameterPool(int Type, int Where);
   void PushParameterPool(int Type, int Where);
   void FreeParameterPool();
   void Callback(string Name);
   void AdjustStack(int Size);
   void OptimizeSubFunctions();
   
   enum Relations{Equal, NotEqual, Greater, Less, GreaterOrEqual, LessOrEqual};
};

/******************************************************************************
AddLibrary - imports an external function into the program for use in the
assembly language
******************************************************************************/
void Assembly::AddLibrary(string FunctionName){
  if (Data.IsFunctionImported(FunctionName) == false){
    Write.Line(Write.ToLibrary, "extern " + FunctionName);
  }
  return;
}

/******************************************************************************
GetLabel - returns the name of the next available label. Labels look like 
Label<Number>
******************************************************************************/
string Assembly::GetLabel(){
  string Result;
  //Get the next available one
  LabelNumber++;
  Result = "Label" + ToStr(LabelNumber);
  return Result;
}

/******************************************************************************
PostLabel - posts a label to the specified section of the program
******************************************************************************/
void Assembly::PostLabel(int Section, string Label){
  Write.Line(Section, Label + ":");
  return;
}

/******************************************************************************
BuildSkeleton - outputs the bare necesseties that we need for the program. This
function is called before we start generating any more code
******************************************************************************/
void Assembly::BuildSkeleton(){
  #ifdef Windows
    //Need some way to get out of this mess
    AddLibrary("ExitProcess");   //Immediately exit
    //Load the default icon for KoolB programs 
    Write.Line(Write.ToResource, "KoolB ICON \"./Asm/Default.ico\"");
    Write.Line(Write.ToData,     "section .data");
    Write.Line(Write.ToFireUp,   "section .text");
    //Include the assembly macros ccall and stdcall
    Write.Line(Write.ToFireUp,   "%include \"./Asm/MACROS.INC\"");
    //START is where program exection begins
    Write.Line(Write.ToFireUp,   "global START");
    PostLabel(Write.ToFireUp,    "START");
    //Let's preserve the command line
    Write.Line(Write.ToFireUp,   "PUSH EBP");
    Write.Line(Write.ToFireUp,   "MOV EBP,ESP");
    Write.Line(Write.ToFireUp,   "PUSH EBX");
    Write.Line(Write.ToFireUp,   "PUSH ESI");
    Write.Line(Write.ToFireUp,   "PUSH EDI");
    //Exit is where we exit the app
    PostLabel(Write.ToFinishUp,  "Exit");
  #endif
  #ifdef Linux
    //Things are little different for Linux
    AddLibrary("exit");         //Immediately exit
    Write.Line(Write.ToData,    "section .data");
    Write.Line(Write.ToFireUp,  "section .text");
    Write.Line(Write.ToFireUp,  "%include \"./Asm/MACROS.INC\"");
    //We start at _start instead of START (minor difference in linkers)
    Write.Line(Write.ToFireUp,  "global _start");
    PostLabel(Write.ToFireUp,   "_start");
    Write.Line(Write.ToFireUp,  "PUSH EBP");
    Write.Line(Write.ToFireUp,  "MOV EBP,ESP");
    Write.Line(Write.ToFireUp,  "PUSH EBX");
    Write.Line(Write.ToFireUp,  "PUSH ESI");
    Write.Line(Write.ToFireUp,  "PUSH EDI");
    PostLabel(Write.ToFinishUp, "Exit");
  #endif
  return;
}

/******************************************************************************
FinishUp - generates code after all other code has been generated to 'clean-up'
the program
******************************************************************************/
void Assembly::FinishUp(){
  //Set our exit status - are we exiting because of an error? By default, no
  Write.Line(Write.ToData, "ExitStatus dd 0");
  #ifdef Windows
    //OK, if we have a Windows OS and we aren't generating a DLL, destroy the 
    //heap
    AddLibrary("HeapDestroy");    //Destory our heap of memory
    if (AppType == Console || AppType == GUI){
      Write.Line(Write.ToFinishUp, "stdcall HeapDestroy,dword[HandleToHeap]");
    }
    //Unprepare the parameters
    Write.Line(Write.ToFinishUp, "POP EDI");
    Write.Line(Write.ToFinishUp, "POP ESI");
    Write.Line(Write.ToFinishUp, "POP EBX");
    Write.Line(Write.ToFinishUp, "MOV ESP,EBP");
    Write.Line(Write.ToFinishUp, "POP EBP");
    //If this is a Console or GUI app, return the status
    if (AppType == Console || AppType == GUI){
      Write.Line(Write.ToFinishUp, "MOV EAX,dword[ExitStatus]");
      Write.Line(Write.ToFinishUp, "stdcall ExitProcess,dword[ExitStatus]");
      Write.Line(Write.ToFinishUp, "RET");
    }
    //If this is a DLL, we MUST exit 1 or something catastrophic has happened
    if (AppType == DLL){
      Write.Line(Write.ToFinishUp, "MOV EAX,1");
      //Also, we need to return 12 to pop the 3 parameters off the stack
      Write.Line(Write.ToFinishUp, "RET 12");
    }
  #endif
  #ifdef Linux
    //If this is Linux, pop off the parameters and call exi
    Write.Line(Write.ToFinishUp, "POP EDI");
    Write.Line(Write.ToFinishUp, "POP ESI");
    Write.Line(Write.ToFinishUp, "POP EBX");
    Write.Line(Write.ToFinishUp, "MOV ESP,EBP");
    Write.Line(Write.ToFinishUp, "POP EBP");
    Write.Line(Write.ToFinishUp, "ccall exit,dword[ExitStatus]");
  #endif
  //Now, if we are to optimize the app, call the optimization routine
  if (Optimize == true){
    OptimizeSubFunctions();
  }
  return;
}

/******************************************************************************
InitMemory - creates a pool of memory where we can draw from for variables
******************************************************************************/
void Assembly::InitMemory(){
  //Our ParameterPool is a pool of memory for passing parameters to functions
  Write.Line(Write.ToData, "ParameterPool dd 0");
  //For Windows, we need to create a heap of memory for our program
  #ifdef Windows
    //If we successfully get the heap (pool) of memory, skip over the error
    string LabelGotMemory = GetLabel();
    //Need these WinAPI functions 
    AddLibrary("HeapCreate");   //Create a heap of memory
    AddLibrary("HeapAlloc");    //Get some memory out of that heap
    AddLibrary("HeapFree");     //Release some memory back to the heap
    AddLibrary("MessageBoxA");  //Use a MessageBox to alert the user
    //DLLs piggy back off the calling program's heap, so only create the heap
    //if we have a GUI or Console program
    if (AppType == GUI || AppType == Console){
      //Store the handle to the heap in this variable
      Write.Line(Write.ToData,   "HandleToHeap dd 0");
      //Create the heap with 16KB of memory - also set it so it can grow
      Write.Line(Write.ToFireUp, "stdcall HeapCreate,0,16384,0");
      //Store return value (EAX) the handle in HandleToHeap
      Write.Line(Write.ToFireUp, "MOV dword[HandleToHeap],EAX");
      Write.Line(Write.ToFireUp, "CMP dword[HandleToHeap],0");
      //Now if the return value is not 0 (meaning we have a valid handle),
      //skip over the error. Otherwise jump to NoMemory
      Write.Line(Write.ToFireUp, "JNE " + LabelGotMemory);
      Write.Line(Write.ToFireUp, "JMP NoMemory");
      PostLabel(Write.ToFireUp,  LabelGotMemory);
    }
  #endif
  //Linux uses the standard system memory, so no need to do anything
  //Now create some variables we need: TempQWord1 and TempQWord2 are the 
  //floating-point numbers we use to perform math
  Write.Line(Write.ToData, "TempQWord1 dq 0.0");
  Write.Line(Write.ToData, "TempQWord2 dq 0.0");
  //True is defined as negative one
  Write.Line(Write.ToData, "True dd -1");
  return;
}

/******************************************************************************
CreateInteger - creates a 4 byte integer variable
******************************************************************************/
void Assembly::CreateInteger(string Name, string AsmName){
  //If we are inside a function, create a local variable (on the stack)
  if (Data.IsInsideSubFunction() == true){
    //OK, subtract the space from the stack
    Write.Line(Write.ToFireUp, "SUB ESP,4");
    //Now, make sure the default value is 0
    //Write.Line(Write.ToFireUp, "MOV dword[ESP+4],0");
    //However, if this is the RESULT variable, we already have a special place
    //for it - ESP - 4.
    if (Name == "RESULT"){
      //Write.Line(Write.ToFireUp, "MOV dword[EBP-4],0");
    }
    //Now, let's get the relationship to EBP
    AsmName = "EBP" + Data.AddLocalVariable(Data.Integer);
  }
  //Otherwise, if this is a normal variable, just use the unique scope ID
  else{
    AsmName = Data.GetScopeID() + AsmName;
  }
  //Store the information and send it to the database
  SimpleDataInfo Info;
  Info.AsmName = AsmName;
  Info.Type    = Data.Integer;
  Data.AddSimpleData(Name, Info);
  //If we aren't in a function, create a physical place for it in the .data
  //section of the program. Size is 4 bytes
  if (Data.IsInsideSubFunction() == false){
    Write.Line(Write.ToData, AsmName + " dd 0");
  }
  return;
}

/******************************************************************************
CreateDouble - creates a 8 byte floating-point number. The same concept as 
CreateInteger
******************************************************************************/
void Assembly::CreateDouble(string Name, string AsmName){
  if (Data.IsInsideSubFunction() == true){
    Write.Line(Write.ToFireUp, "SUB ESP,8");
    Write.Line(Write.ToFireUp, "MOV dword[ESP+8],0");
    Write.Line(Write.ToFireUp, "MOV dword[ESP+4],0");
    if (Name == "RESULT"){
      Write.Line(Write.ToFireUp, "MOV dword[EBP-4],0");      
      Write.Line(Write.ToFireUp, "MOV dword[EBP-8],0"); 
    }
    AsmName = "EBP" + Data.AddLocalVariable(Data.Double);
  }
  else{
    AsmName = Data.GetScopeID() + AsmName;
  }
  SimpleDataInfo Info;
  Info.AsmName = AsmName;
  Info.Type    = Data.Double;
  Data.AddSimpleData(Name, Info);
  if (Data.IsInsideSubFunction() == false){
    Write.Line(Write.ToData, AsmName + " dq 0.0");
  }
  return;
}

/******************************************************************************
CreateString - creates a string variable
******************************************************************************/
void Assembly::CreateString(string Name, string AsmName){
  //If we are in a function, create a local variable on the stack
  if (Data.IsInsideSubFunction() == true){
    Write.Line(Write.ToFireUp, "SUB ESP,4");
    AsmName = "EBP" + Data.AddLocalVariable(Data.String);
  }
  else{
    AsmName = Data.GetScopeID() + AsmName;
  }
  //Store information in database
  SimpleDataInfo Info;
  Info.AsmName = AsmName;
  Info.Type    = Data.String;
  Data.AddSimpleData(Name, Info);
  //Create a variable to hold the address to the string in the .data section
  if (Data.IsInsideSubFunction() == false){
    Write.Line(Write.ToData,     AsmName + " dd 0");
  }
  //Allocate 1 byte of memory, and store an empty string in it. 
  AllocMemory(Write.ToFireUp,  "1");
  Write.Line(Write.ToFireUp,   "MOV dword[" + AsmName + "],EAX");
  Write.Line(Write.ToFireUp,   "MOV byte[EAX],0");
  //If we are in an function, make sure we free the memory before returning
  if (Name != "RESULT" && Data.IsInsideSubFunction() != true){
    FreeMemory(Write.ToFinishUp, "dword[" + Data.Asm(Name) + "]");
  }
  return;
}

/******************************************************************************
CreateIntegerArray - creates an array of integers
******************************************************************************/
void Assembly::CreateIntegerArray(string Name, string AsmName){
  //Store information in the database
  AsmName = Data.GetScopeID() + AsmName;
  ArrayDataInfo Info;
  Info.AsmName = AsmName;
  Info.Type    = Data.Integer;
  Data.AddArrayData(Name, Info);
  //Create variables for the array - both the array and its size
  Write.Line(Write.ToData,     AsmName + " dd 0");
  Write.Line(Write.ToData,     AsmName + "_Size dd 0");
  //Round the size on the stack to an integer
  RoundToInteger();
  //Get the size from the stack
  Write.Line(Write.ToMain,   "POP EBX");
  //Add one so we can start counting at 1 instead of 0
  Write.Line(Write.ToMain,   "INC EBX");
  //Store the size into the size variable
  Write.Line(Write.ToMain,   "MOV dword[" + AsmName + "_Size],EBX");
  Write.Line(Write.ToMain,   "MOV EAX,4");
  //Multiply the size by four to get enough size to store all the integers
  Write.Line(Write.ToMain,   "MUL EBX");
  //Create enough memory to store all those integers
  AllocMemory(Write.ToMain,  "EAX");
  //Use the array variable to access the memory
  Write.Line(Write.ToMain,   "MOV dword[" + AsmName + "],EAX");
  //Before exiting the program, free the memory
  FreeMemory(Write.ToFinishUp, "dword[" + Data.Asm(Name) + "]");
  return;
}

/******************************************************************************
CreateDoubleArray - creates an array of doubles. The only difference between
this function and the above is the size of what we are storing in the array
******************************************************************************/
void Assembly::CreateDoubleArray(string Name, string AsmName){
  AsmName = Data.GetScopeID() + AsmName;
  ArrayDataInfo Info;
  Info.AsmName = AsmName;
  Info.Type    = Data.Double;
  Data.AddArrayData(Name, Info);
  Write.Line(Write.ToData,     AsmName + " dd 0");
  Write.Line(Write.ToData,     AsmName + "_Size dd 0");
  RoundToInteger();
  Write.Line(Write.ToMain,   "POP EBX");
  Write.Line(Write.ToMain,   "INC EBX");
  Write.Line(Write.ToMain,   "MOV dword[" + AsmName + "_Size],EBX");
  Write.Line(Write.ToMain,   "MOV EAX,8");
  Write.Line(Write.ToMain,   "MUL EBX");
  AllocMemory(Write.ToMain,  "EAX");
  Write.Line(Write.ToMain,   "MOV dword[" + AsmName + "],EAX");
  FreeMemory(Write.ToFinishUp, "dword[" + AsmName + "]");
  return;
}

/******************************************************************************
CreateStringArray - Creates an array of string - very much like the above two
functions.
******************************************************************************/
void Assembly::CreateStringArray(string Name, string AsmName){
  AsmName = Data.GetScopeID() + AsmName;
  ArrayDataInfo Info;
  Info.AsmName = AsmName;
  Info.Type    = Data.String;
  Data.AddArrayData(Name, Info);
  Write.Line(Write.ToData,     AsmName + " dd 0");
  Write.Line(Write.ToData,     AsmName + "_Size dd 0");
  RoundToInteger();
  Write.Line(Write.ToMain,   "POP EBX");
  Write.Line(Write.ToMain,   "INC EBX");
  Write.Line(Write.ToMain,   "MOV dword[" + AsmName + "_Size],EBX");
  Write.Line(Write.ToMain,   "MOV EAX,4");
  Write.Line(Write.ToMain,   "MUL EBX");
  AllocMemory(Write.ToMain,  "EAX");
  Write.Line(Write.ToMain,   "MOV dword[" + AsmName + "],EAX");
  //Unlike numbers, we need to fill the array with empty strings
  AllocStringArray(AsmName);
  //Also, before we exit, we need to free all those strings
  FreeStringArray(AsmName);
  return;
}

/******************************************************************************
AllocMemory - gets some memory to store something in
******************************************************************************/
void Assembly::AllocMemory(int Section, string Size){
  //Windows uses HeapAlloc to get memory from the program's heap
  #ifdef Windows
    string MemoryOKLabel = GetLabel();
    AddLibrary("HeapAlloc");   //Get memory from the heap
    //If this is a GUI or Console program, get memory from the heap we created
    //at the beginning of the program
    if (AppType == GUI || AppType == Console){
      Write.Line(Section, "stdcall HeapAlloc,dword[HandleToHeap],8," + Size);
    }
    //If this is a DLL, get the calling program's heap and use it to get memory
    if (AppType == DLL){
      AddLibrary("GetProcessHeap");   //Get the program's default heap
      Write.Line(Section, "CALL GetProcessHeap");
      Write.Line(Section, "stdcall HeapAlloc,EAX,8," + Size);
    }
    //Make sure we actually got the memory, jump to error if we got an error
    Write.Line(Section, "CMP EAX,0");
    Write.Line(Section, "JNE " + MemoryOKLabel);
    Write.Line(Section, "JMP NoMemory");
    PostLabel(Section,  MemoryOKLabel);
  #endif
  //Under Linux, things are a bit simplier - we just get the memory from the 
  //system using malloc
  #ifdef Linux
    string MemoryOKLabel = GetLabel();
    AddLibrary("malloc");   //Gets memory from the system
    Write.Line(Section, "ccall malloc," + Size);
    Write.Line(Section, "CMP EAX,0");
    Write.Line(Section, "JNE " + MemoryOKLabel);
    Write.Line(Section, "JMP NoMemory");
    PostLabel(Section,  MemoryOKLabel);
  #endif
  return;
}

/******************************************************************************
FreeMemory - frees the memory, which is basically telling the system we don't 
need the memory any more
******************************************************************************/
void Assembly::FreeMemory(int Section, string Name){
  //Under Windows, we need to call release the memory back to the heap
  #ifdef Windows
    AddLibrary("HeapFree");   //Free memory back to the heap
    //GUI or Console apps use the heap we created earlier
    if (AppType == GUI || AppType == Console){
      Write.Line(Section, "stdcall HeapFree,dword[HandleToHeap],0," + Name);
    }
    //DLLs use the calling program's heap
    if (AppType == DLL){
      AddLibrary("GetProcessHeap");
      Write.Line(Section, "CALL GetProcessHeap");
      Write.Line(Section, "stdcall HeapFree,EAX,0," + Name);
    }
  #endif
  //Under Linux, we release the memory directly back to the system
  #ifdef Linux
    AddLibrary("free");   //Release memory back to the system
    Write.Line(Section, "ccall free," + Name);
  #endif
  return;
}

/******************************************************************************
StartCreateingType - kicks off the process of creating a TYPE
******************************************************************************/
void Assembly::StartCreatingType(string TypeName){
  //Store all the information in the database
  TypeDataInfo Info;
  if (Mangle == true){
    Info.AsmName = Data.GetScopeID() +Data.StripJunkOff(TypeName) + "_TYPE";
  }
  else{
    Info.AsmName = Data.GetScopeID() +Data.StripJunkOff(TypeName);
  }
  Info.Size    = 0;
  Data.AddTypeData(TypeName, Info);
  //Now start the declaration of the TYPE by creating the header for a STRUC
  Write.Line(Write.ToData, "STRUC " + Info.AsmName);
  return;
}

/******************************************************************************
CreateTypeInteger - creates an integer in a TYPE
******************************************************************************/
void Assembly::CreateTypeInteger(string TypeName, string Name, string AsmName){
  //Add the information to the database, which catalogs it under the right TYPE
  AsmName = Data.GetScopeID() + AsmName;
  SimpleDataInfo Info;
  Info.AsmName = AsmName;
  Info.Type    = Data.Integer;
  Data.AddSimpleDataToType(TypeName, Name, Info);
  //Reserve space for the integer in the TYPE
  Write.Line(Write.ToData, "." + AsmName + " resd 1");
  return;
}

/******************************************************************************
CreateTypeDouble - creates space for a double in a TYPE. Nearly the same as the
above function.
******************************************************************************/
void Assembly::CreateTypeDouble(string TypeName, string Name, string AsmName){
  AsmName = Data.GetScopeID() + AsmName;
  SimpleDataInfo Info;
  Info.AsmName = AsmName;
  Info.Type    = Data.Double;
  Data.AddSimpleDataToType(TypeName, Name, Info);
  Write.Line(Write.ToData, "." + AsmName + " resq 1");
  return;
}

/******************************************************************************
CreateTypeString - Creates a string in a TYPE - nearly the same as the above 
functions
******************************************************************************/
void Assembly::CreateTypeString(string TypeName, string Name, string AsmName){
  AsmName = Data.GetScopeID() + AsmName;
  SimpleDataInfo Info;
  Info.AsmName = AsmName;
  Info.Type    = Data.String;
  Data.AddSimpleDataToType(TypeName, Name, Info);
  Write.Line(Write.ToData, "." + AsmName + " resd 1");
  return;
}

/******************************************************************************
FinishCreatingType - finishes creating a TYPE by capping the TYPE declaration
with ENDSTRUC
******************************************************************************/
void Assembly::FinishCreatingType(string TypeName){
  Write.Line(Write.ToData, "ENDSTRUC");
  return;
}

/******************************************************************************
FreeStringArray - frees all the strings in an array before closing down
the application
******************************************************************************/
void Assembly::FreeStringArray(string AsmName){
  //Labels to use as loop guides
  string StartLabel = GetLabel();
  string EndLabel   = GetLabel();
  //Start with the first string in the array
  Write.Line(Write.ToFinishUp,   "MOV EBX,1");
  //StartLabel is where we start looping from
  PostLabel(Write.ToFinishUp,    StartLabel);
  //Get the array
  Write.Line(Write.ToFinishUp,   "MOV ECX,dword[" + AsmName + "]");
  //Get the array item by adding the address of the array plus the index + 4
  Write.Line(Write.ToFinishUp,   "MOV EAX,dword[ECX+EBX*4]");
  //Free the string
  FreeMemory(Write.ToFinishUp,   "EAX");
  //Go to the next index item
  Write.Line(Write.ToFinishUp,   "INC EBX");
  //Compare the current index to the size
  Write.Line(Write.ToFinishUp,   "CMP EBX,dword[" + AsmName + "_Size]");
  //As long as we are less or equal to the size, keep going
  Write.Line(Write.ToFinishUp,   "JLE " + StartLabel);
  PostLabel(Write.ToFinishUp,    EndLabel);
  //Finally, free the array
  FreeMemory(Write.ToFinishUp,   "dword[" + AsmName + "]");
  return;
}

/******************************************************************************
AllocStringArray - fills a string array with empty strings. Very similar to the
above function
******************************************************************************/
void Assembly::AllocStringArray(string AsmName){
  //Our loop guides
  string StartLabel = GetLabel();
  string EndLabel   = GetLabel();
  //Start with the first array item
  Write.Line(Write.ToMain,  "MOV EBX,1");
  PostLabel(Write.ToMain,    StartLabel);
  //Create the empty string
  AllocMemory(Write.ToMain,  "1");
  //Get the address to store the string in
  Write.Line(Write.ToMain,   "MOV ECX,dword[" + AsmName + "]");
  //Store the address of the string in the array
  Write.Line(Write.ToMain,   "MOV dword[ECX+EBX*4],EAX");
  //Now null-terimate the string
  Write.Line(Write.ToMain,   "MOV byte[EAX],0");
  //Go to the next array item
  Write.Line(Write.ToMain,   "INC EBX");
  //Before we loop around again, make sure we haven't gone past the end of the
  //array
  Write.Line(Write.ToMain,   "CMP EBX,dword[" + AsmName + "_Size]");
  Write.Line(Write.ToMain,   "JLE " + StartLabel);
  PostLabel(Write.ToMain,    EndLabel);
  return;
}

/******************************************************************************
MOV - moves integers from a register to a memory or vice versa
******************************************************************************/
void Assembly::MOV(int Section, string WhereTo, string What){
  Write.Line(Section, "MOV " + WhereTo + "," + What);
  return;
}

/******************************************************************************
CreateUDT - creates an instance of a TYPE (UDT)
******************************************************************************/
void Assembly::CreateUDT(string UDT, string Name, string AsmName){
  //Store the information in the database
  if (Mangle == true){
    AsmName = Data.GetScopeID() + Data.StripJunkOff(Name) + "_UDT";
  }
  else{
    AsmName = Data.GetScopeID() + Data.StripJunkOff(Name);
  }
  Data.AddUDTData(UDT, Name, AsmName);
  //Now create the instance of the STRUC in the .data section
  Write.Line(Write.ToData, AsmName +  " ISTRUC " + Data.Asm(UDT));
  Write.Line(Write.ToData, "IEND");
  return;
}

/******************************************************************************
CopyArray - copies an entire array to another array
******************************************************************************/
void Assembly::CopyArray(string Array){
  //Get the size into the ESI register for easy access
  Write.Line(Write.ToMain,   "MOV ESI,dword[" + Data.Asm(Array) + "_Size]");
  //If we have an array of doubles, the size of an item is 8 bytes
  if (Data.GetArrayData(Array).Type == Data.Double){
    Write.Line(Write.ToMain,   "MOV EAX,8");
  }
  //Otherwise, the size of an item is 4 bytes
  if (Data.GetArrayData(Array).Type == Data.Integer || 
      Data.GetArrayData(Array).Type == Data.String){
    Write.Line(Write.ToMain,   "MOV EAX,4");    
  }
  //So multiply array size by item size to get total size
  Write.Line(Write.ToMain,   "MUL ESI");
  Write.Line(Write.ToMain,   "MOV EBX,EAX");
  //Alloc enough memory for the new array
  AllocMemory(Write.ToMain,  "EBX");
  //If we have a string array, we need to copy each string in the array as well
  //So call CopyStringArray
  if (Data.GetArrayData(Array).Type == Data.String){
    CopyStringArray(Array);
  }
  //Otherwise, do a simple copy memory to duplicate the array
  else{
    Write.Line(Write.ToMain, "MOV EDI,EAX");
    CopyMemoryOfSize("EDI", "dword[" + Data.Asm(Array) + "]", "EBX");
  }
  //Now put the array and its size on the stack
  Write.Line(Write.ToMain, "PUSH EDI");
  Write.Line(Write.ToMain, "PUSH dword[" + Data.Asm(Array) + "_Size]");
  return;
}

/******************************************************************************
CopyStringArray - Duplicate a array of strings 
******************************************************************************/
void Assembly::CopyStringArray(string Array){
  //Our loop labels
  string StartLabel = GetLabel();
  string EndLabel   = GetLabel();
  //EBX is our counter for our array
  Write.Line(Write.ToMain,  "MOV EBX,0");
  //EDI is our duplicate array that we need to fill
  Write.Line(Write.ToMain,  "MOV EDI,EAX");
  //Start the loop
  PostLabel(Write.ToMain,    StartLabel);
  //Get the original array
  Write.Line(Write.ToMain,   "MOV ECX,dword[" + Data.Asm(Array) + "]");
  //Get the string from the array
  Write.Line(Write.ToMain,   "MOV EAX,dword[ECX+EBX*4]");
  //Get the length
  GetStringLength("EAX");
  //Alloc that much memory
  AllocMemory(Write.ToMain,  "ESI");
  //Move the address of that memory into the duplicate array
  Write.Line(Write.ToMain,   "MOV dword[EDI+EBX*4],EAX");
  //Get the original array gain
  Write.Line(Write.ToMain,   "MOV ECX,dword[" + Data.Asm(Array) + "]");
  //Now get the item of the original array
  Write.Line(Write.ToMain,   "MOV ECX,dword[ECX+EBX*4]");
  //Copy the string from the original array to the newly create memory
  CopyMemoryOfSize("EAX", "ECX", "ESI");
  //Go onto the next array item
  Write.Line(Write.ToMain,   "INC EBX");
  //Make sure we stop when we run out of array items
  Write.Line(Write.ToMain,   "CMP EBX,dword[" + Data.Asm(Array) + "_Size]");
  Write.Line(Write.ToMain,   "JLE " + StartLabel);
  PostLabel(Write.ToMain,    EndLabel);
  return;
}

/******************************************************************************
CopyMemoryOfSize - copies one memory block to another memory block
******************************************************************************/
void Assembly::CopyMemoryOfSize(string To, string From, string Size){
  #ifdef Windows
    AddLibrary("RtlMoveMemory");  //Actually, copies (not moves) memory
    Write.Line(Write.ToMain, "stdcall RtlMoveMemory," + To + "," + From + "," + 
               Size);
    return;
  #endif
  #ifdef Linux
    AddLibrary("memcpy");   //Copies memory
    Write.Line(Write.ToMain, "ccall memcpy," + To + "," + From + "," + Size);
    return;
  #endif
  return;
}

/******************************************************************************
GetStringLength - gets the leng of a string
******************************************************************************/
void Assembly::GetStringLength(string String){
  #ifdef Windows
    AddLibrary("lstrlenA");    //Returns the length of a string
    Write.Line(Write.ToMain, "stdcall lstrlenA," + String);
    //Add room for the null terminator
    Write.Line(Write.ToMain, "INC EAX");
  #endif
  #ifdef Linux
    AddLibrary("strlen");     //Returns the length of a string
    Write.Line(Write.ToMain, "ccall strlen," + String);
    //Add room for the null terminator
    Write.Line(Write.ToMain, "INC EAX");
  #endif
  return;
}

/******************************************************************************
CopyUDT - duplicates a UDT (a bug is that string values are not copied, only
reflected)
******************************************************************************/
void Assembly::CopyUDT(string UDT){
  Write.Line(Write.ToMain, "MOV EDI," + Data.Asm(Data.GetUDTData(UDT).TypeName)+
                           "_size");
  AllocMemory(Write.ToMain, "EDI");
  Write.Line(Write.ToMain, "MOV EBX,EAX");
  CopyMemoryOfSize("EBX", "" + Data.Asm(UDT), "EDI");
  Write.Line(Write.ToMain, "PUSH EBX");
  return;
}

/******************************************************************************
LoadString - loads a string into memory (either constant or variable)
******************************************************************************/
void Assembly::LoadString(string String){
  //If this is a constant string, load it
  if (Data.GetDataType(String) != Data.String){
    //We need a temporary variable to hold it
    TempVariable++;
    //Now declare the string in the .data section of the app
    Write.Line(Write.ToData, "String_" + ToStr(TempVariable) + " db \"" + 
               String + "\",0");
    //Then move it into the EBX register
    Write.Line(Write.ToMain, "MOV EBX,String_" + ToStr(TempVariable));
    //Get the length of the string
    GetStringLength("EBX");
    Write.Line(Write.ToMain,  "MOV EDI,EAX");
    //Get memory so we can load it
    AllocMemory(Write.ToMain, "EDI");
    Write.Line(Write.ToMain,  "MOV EBX,EAX");
    //Copy the string into memory
    CopyString("EBX", "String_" + ToStr(TempVariable));
    //Now put the string on the stack
    Write.Line(Write.ToMain, "PUSH EBX");
    return;
  }
  else{
    //Get the length of the string in a variable
    GetStringLength("dword[" + Data.Asm(String) + "]");
    Write.Line(Write.ToMain,  "MOV EBX,EAX");
    //Create enough room to copy it
    AllocMemory(Write.ToMain, "EBX");
    Write.Line(Write.ToMain,  "MOV EDI,EAX");
    //Duplicate the string
    CopyString("EDI", "dword[" + Data.Asm(String) + "]");
    //And then put it on the stack
    Write.Line(Write.ToMain, "PUSH EAX");
    return;
  }
}

/******************************************************************************
PUSH - puts an integer on the stack
******************************************************************************/
void Assembly::PUSH(string What){
  Write.Line(Write.ToMain, "PUSH " + What);
  return;
}

/******************************************************************************
POP - gets an integer from the stack
******************************************************************************/
void Assembly::POP(string What){
  Write.Line(Write.ToMain, "POP " + What);
  return;
}

/******************************************************************************
AddStrings - takes two strings and combines them into a single string
******************************************************************************/
void Assembly::AddStrings(){
  //Get the first string
  Write.Line(Write.ToMain,  "POP ESI");
  //Get the length
  GetStringLength("ESI");
  Write.Line(Write.ToMain,  "MOV EDI,EAX");
  //Get the second string
  Write.Line(Write.ToMain,  "POP EBX");
  //That the length of the second string
  GetStringLength("EBX");
  //Add the strings together
  Write.Line(Write.ToMain,  "ADD EAX,EDI");
  //Don't need room for two null-terminators
  Write.Line(Write.ToMain,  "DEC EAX");
  //Create enough room for both strings
  AllocMemory(Write.ToMain, "EAX");
  //Copy the first string to the new string
  CopyString("EAX", "EBX");
  //Append the second string to the new string
  AppendString("EAX", "ESI");
  //Now push the new string to the stack
  Write.Line(Write.ToMain, "PUSH EAX");
  //Free the other two
  FreeMemory(Write.ToMain, "EBX");
  FreeMemory(Write.ToMain, "ESI");
  return;
}

/******************************************************************************
CopyString - copies a string 
******************************************************************************/
void Assembly::CopyString(string To, string From){
  #ifdef Windows
    AddLibrary("lstrcpyA");   //Copy a string
    Write.Line(Write.ToMain, "stdcall lstrcpyA,"+ To + "," + From);
    return;
  #endif
  #ifdef Linux
    AddLibrary("strcpy");     //Copy a string
    Write.Line(Write.ToMain, "ccall strcpy,"+ To + "," + From);
    return;
  #endif
}

/******************************************************************************
AppendString - adds a string to an existing string
******************************************************************************/
void Assembly::AppendString(string To, string From){
  #ifdef Windows
    AddLibrary("lstrcatA");    //Combine two strings
    Write.Line(Write.ToMain, "stdcall lstrcatA,"+ To + "," + From);
    return;
  #endif
  #ifdef Linux
    AddLibrary("strcat");      //Combine two strings
    Write.Line(Write.ToMain, "ccall strcat,"+ To + "," + From);
    return;
  #endif
}

/******************************************************************************
AssignIt - assigns a variable the result of an expression. The expression is
on the stack and the Name passed to the function is the variable to store the
expression in.
******************************************************************************/
void Assembly::AssignIt(string Name){
  //Deal with assigning an array
  if (Data.GetDataType(Name) == Data.Array){
    //First, get the array and the size
    Write.Line(Write.ToMain, "POP EBX");
    Write.Line(Write.ToMain, "POP ESI");
    //Then store the information in the variable
    Write.Line(Write.ToMain, "MOV dword[" + Data.Asm(Name) + "],ESI");
    Write.Line(Write.ToMain, "MOV dword[" + Data.Asm(Name) + "_Size],EBX");
  }
  //Deal with a UDT
  if (Data.GetDataType(Name) == Data.UDT){
    //Get the UDT
    Write.Line(Write.ToMain, "POP EBX");
    //Copy over the UDT
    CopyMemoryOfSize("dword[" + Data.Asm(Name) + "]", "EBX", 
               Data.Asm(Data.GetUDTData(Name).TypeName) + "_size");
    //Then free the UDT after it is copied
    FreeMemory(Write.ToMain, "EBX");
  }
  //Deal with a string variable
  if (Data.GetDataType(Name) == Data.String){
    //Get the string
    Write.Line(Write.ToMain, "POP EBX");
    //Free the original string
    FreeMemory(Write.ToMain, "dword[" + Data.Asm(Name) + "]");
    //Then move the new string into the variable
    Write.Line(Write.ToMain, "MOV dword[" + Data.Asm(Name) + "],EBX");
    return;
  }
  //Deal with a number
  if (Data.GetDataType(Name) == Data.Number){
    if (Data.GetSimpleData(Name).Type == Data.Double){
      //If we have a double, pop both parts off the stack
      Write.Line(Write.ToMain, "POP dword[TempQWord1]");
      Write.Line(Write.ToMain, "POP dword[TempQWord1+4]");
      //Clear the floating point processor
      Write.Line(Write.ToMain, "FINIT");
      //Load the double into the FPU
      Write.Line(Write.ToMain, "FLD qword[TempQWord1]");
      //Then store it in the variable
      Write.Line(Write.ToMain, "FST qword[" + Data.Asm(Name) + "]");
    }
    if (Data.GetSimpleData(Name).Type == Data.Integer){
      //Round the number to an integer
      RoundToInteger();
      //Get the integer off the stack
      Write.Line(Write.ToMain, "POP dword[TempQWord1]");
      //Clear the FPU
      Write.Line(Write.ToMain, "FINIT");  
      //Load the integer
      Write.Line(Write.ToMain, "FILD dword[TempQWord1]");
      //Store the integer
      Write.Line(Write.ToMain, "FIST dword[" + Data.Asm(Name) + "]");
    }
  }
  return;
}

/******************************************************************************
LoadNumber - load either a constant or variable number onto the stack
******************************************************************************/
void Assembly::LoadNumber(string Number){
  //If this is a constant number, load it into a temporary variable
  if (Data.GetDataType(Number) != Data.Number){
    //Get the next available temparary variable
    TempVariable++;
    //Make sure we have a decimal point in the number
    if (Number.find(".", 0) != string::npos){
      Write.Line(Write.ToData, "Number_"+ToStr(TempVariable)+ " dq 0" + Number);
    }
    //If we can't find a ., then add .0 to the end of the number
    else{
      Write.Line(Write.ToData, "Number_" + ToStr(TempVariable) + " dq " + 
                 Number + ".0");
    }
    //Then push both parts of the double
    Write.Line(Write.ToMain,"PUSH dword[Number_" + ToStr(TempVariable) + "+4]");
    Write.Line(Write.ToMain,"PUSH dword[Number_" + ToStr(TempVariable) + "]");
  }
  //Otherwise, load the variable to the stack
  else{
    //If we are dealing with an integer, we first need to convert to a double
    if (Data.GetSimpleData(Number).Type == Data.Integer){
      //Clear the FPU
      Write.Line(Write.ToMain, "FINIT");
      //Load the integer
      Write.Line(Write.ToMain, "FILD dword[" + Data.Asm(Number) + "]");
      //Store the integer as a double
      Write.Line(Write.ToMain, "FST qword[TempQWord1]");
      //Push both parts of the double
      Write.Line(Write.ToMain, "PUSH dword[TempQWord1+4]");
      Write.Line(Write.ToMain, "PUSH dword[TempQWord1]");
    }
    //Otherwise, just push both parts of the double, and then be done with it
    else{
      Write.Line(Write.ToMain, "PUSH dword[" + Data.Asm(Number) + "+4]");
      Write.Line(Write.ToMain, "PUSH dword[" + Data.Asm(Number) + "]");
    }
  }
  return;
}

/******************************************************************************
CalculateStart - starts the calculation of two doubles
******************************************************************************/
void Assembly::CalculateStart(){
  //Get the first double from the stack and store it in a temporary double
  Write.Line(Write.ToMain, "POP dword[TempQWord1]");
  Write.Line(Write.ToMain, "POP dword[TempQWord1+4]");
  //Get the second double from the stack and store in another temporary variable
  Write.Line(Write.ToMain, "POP dword[TempQWord2]");
  Write.Line(Write.ToMain, "POP dword[TempQWord2+4]");
  //Clear the FPU
  Write.Line(Write.ToMain, "FINIT");
  //Load the two doubles
  Write.Line(Write.ToMain, "FLD qword[TempQWord1]");
  Write.Line(Write.ToMain, "FLD qword[TempQWord2]");
  return;
}

/******************************************************************************
CalculateExp - takes one double to the power of another double
******************************************************************************/
void Assembly::CalculateExp(){
  CalculateStart();
  AddLibrary("pow");   //Does exponentional math
  Write.Line(Write.ToMain, "ccall pow,dword[TempQWord2],dword[TempQWord2+4],"
                           "dword[TempQWord1],dword[TempQWord1+4]");
  CalculateEnd();
  return;
}

/******************************************************************************
CalculateMultiply - takes the two doubles in the FPU and multiplies them
******************************************************************************/
void Assembly::CalculateMultiply(){
  CalculateStart();
  Write.Line(Write.ToMain, "FMUL ST0,ST1");
  CalculateEnd();
  return;
}

/******************************************************************************
CalculateDivid - takes the two doubles in the FPU and divides them
******************************************************************************/
void Assembly::CalculateDivide(){
  CalculateStart();
  Write.Line(Write.ToMain, "FDIV ST0,ST1");
  CalculateEnd();
  return;
}

/******************************************************************************
CalculateMOD - divides one double by another and returns the remainder
******************************************************************************/
void Assembly::CalculateMOD(){
  CalculateStart();
  AddLibrary("fmod");   //Gets the modulus (remainder) of two doubles
  Write.Line(Write.ToMain, "ccall fmod,dword[TempQWord2],dword[TempQWord2+4],"
                           "dword[TempQWord1],dword[TempQWord1+4]");
  CalculateEnd();
  return;
}

/******************************************************************************
CalculateAddition - takes the two doubles on the stack and adds them 
******************************************************************************/
void Assembly::CalculateAddition(){
  CalculateStart();
  Write.Line(Write.ToMain, "FADD ST0,ST1");
  CalculateEnd();
  return;
}

/******************************************************************************
CalculateSubtract - takes the two doubles in the FPU and subtracts them
******************************************************************************/
void Assembly::CalculateSubtract(){
  CalculateStart();
  Write.Line(Write.ToMain, "FSUB ST0,ST1");
  CalculateEnd();
  return;
}

/******************************************************************************
CalculateEnd - Pushes the result of a calculation back onto the stack
******************************************************************************/
void Assembly::CalculateEnd(){
  Write.Line(Write.ToMain, "MOV EAX,dword[TempQWord1]");
  Write.Line(Write.ToMain, "FST qword[TempQWord1]");
  Write.Line(Write.ToMain, "PUSH dword[TempQWord1+4]");
  Write.Line(Write.ToMain, "PUSH dword[TempQWord1]");
  return;
}

/******************************************************************************
LoadUDTMember - Loads a member of a UDT to the stack
******************************************************************************/
void Assembly::LoadUDTMember(string UDT,string TypeName,string Member,int Type){
  //Get the assembly name of the member
  string MemberAsmName = Data.GetUDTData(UDT).Type.Members[Member].AsmName;
  //Deal with the different types differently
  if (Data.GetUDTData(UDT).Type.Members[Member].Type == Data.String){
    //Get the address of the string
    Write.Line(Write.ToMain,   "MOV ESI,dword[" + Data.Asm(UDT) + "+" + 
               Data.Asm(TypeName) + "." + MemberAsmName + "]");
    //Get the length of the string
    GetStringLength("ESI");
    Write.Line(Write.ToMain,  "MOV EBX,EAX");
    //Create a new string the same length
    AllocMemory(Write.ToMain, "EBX");
    Write.Line(Write.ToMain,  "MOV EDI,EAX");
    //Copy the UDT member string to the new string
    CopyString("EDI", "ESI");
    //Load the new string to the stack
    Write.Line(Write.ToMain, "PUSH EAX");
  }
  if (Data.GetUDTData(UDT).Type.Members[Member].Type == Data.Double){
    //Push both parts of the double
    Write.Line(Write.ToMain, "PUSH dword[" + Data.Asm(UDT) + "+" + 
               Data.Asm(TypeName) + "." + MemberAsmName + "+4]");
    Write.Line(Write.ToMain, "PUSH dword[" + Data.Asm(UDT) + "+" + 
               Data.Asm(TypeName) + "." + MemberAsmName + "]");    
  }
  if (Data.GetUDTData(UDT).Type.Members[Member].Type == Data.Integer){
    //Clear the FPU
    Write.Line(Write.ToMain, "FINIT");
    //Load the integer
    Write.Line(Write.ToMain, "FILD dword[" + Data.Asm(UDT) + "+" + 
               Data.Asm(TypeName) + "." + MemberAsmName + "]");
    //Store the integer as a double
    Write.Line(Write.ToMain, "FST qword[TempQWord1]");
    //Push both parts of the double
    Write.Line(Write.ToMain, "PUSH dword[TempQWord1+4]");
    Write.Line(Write.ToMain, "PUSH dword[TempQWord1]");
  }
  return;
}

/******************************************************************************
AssignUDTMember - Assigns a UDT member
******************************************************************************/
void Assembly::AssignUDTMember(string UDT, string TypeName, string Member,
                              int Type){
  //Get the assembly language name of the member
  string MemberAsmName = Data.GetUDTData(UDT).Type.Members[Member].AsmName;
  if (Type == Data.String){
    //Free the old string
    FreeMemory(Write.ToMain, "dword[" + Data.Asm(UDT) + "+" + 
               Data.Asm(TypeName) + "." + MemberAsmName + "]");
    //Get the new string
    Write.Line(Write.ToMain, "POP ESI");
    //Then store the new string in the UDT
    Write.Line(Write.ToMain, "MOV dword[" + Data.Asm(UDT) + "+" + 
               Data.Asm(TypeName) + "." + MemberAsmName + "],ESI");
  }
  if (Type == Data.Double){
    //Store both parts of the double in the UDT member
    Write.Line(Write.ToMain, "POP dword[" + Data.Asm(UDT) + "+" + 
               Data.Asm(TypeName) + "." + MemberAsmName + "]");
    Write.Line(Write.ToMain, "POP dword[" + Data.Asm(UDT) + "+" + 
               Data.Asm(TypeName) + "." + MemberAsmName + "+4]");
  }
  if (Type == Data.Integer){
    //Round the number on the stack to an integer
    RoundToInteger();
    //Then retrieve the integer
    Write.Line(Write.ToMain, "POP dword[TempQWord1]");
    //Clear the FPU
    Write.Line(Write.ToMain, "FINIT"); 
    //Load the integer
    Write.Line(Write.ToMain, "FILD dword[TempQWord1]");
    //And store the integer 
    Write.Line(Write.ToMain, "FIST dword[" + Data.Asm(UDT) + "+" + 
               Data.Asm(TypeName) + "." + MemberAsmName + "]");
  }
  return;
}

/******************************************************************************
AssignArrayItem - Assigns an array item
******************************************************************************/
void Assembly::AssignArrayItem(string Array, int Type){
  //Need an error in case we encounter an out of bounds error
  string OutOfBoundsLabel = GetLabel();
  //If we have a string on the stack, retrieve it
  if (Type == Data.String){
    Write.Line(Write.ToMain, "POP ESI");
  }
  //Otherwise, if we have a number, retrieve it too
  if (Type == Data.Double || Type == Data.Integer){
    Write.Line(Write.ToMain, "POP dword[TempQWord1]");
    Write.Line(Write.ToMain, "POP dword[TempQWord1+4]");
  }
  //Now round the index to an integer
  RoundToInteger();
  //Get the index
  Write.Line(Write.ToMain, "POP EBX");
  //See if the index is ont of bounds (either below 1 or above the size)
  Write.Line(Write.ToMain, "CMP EBX,dword[" + Data.Asm(Array) + "_Size]");
  Write.Line(Write.ToMain, "JGE " + OutOfBoundsLabel);
  Write.Line(Write.ToMain, "CMP EBX,0");
  Write.Line(Write.ToMain, "JLE " + OutOfBoundsLabel);
  //Deal with individual types differently
  if (Type == Data.String){
    //If we have a string, get the array
    Write.Line(Write.ToMain, "MOV EDI,dword[" + Data.Asm(Array) + "]");
    //Free the old string
    FreeMemory(Write.ToMain, "dword[EDI+EBX*4]");
    //Then move the new string into the array
    Write.Line(Write.ToMain, "MOV dword[EDI+EBX*4],ESI");
  }
  if (Type == Data.Double){
    //Get the array
    Write.Line(Write.ToMain, "MOV EDI,dword[" + Data.Asm(Array) + "]");
    //Clear the FPU
    Write.Line(Write.ToMain, "FINIT");
    //Load the number
    Write.Line(Write.ToMain, "FLD qword[TempQWord1]");
    //Store the double in the array
    Write.Line(Write.ToMain, "FST qword[EDI+EBX*8]");
  }
  if (Type == Data.Integer){
    //Put the number back on the stack
    Write.Line(Write.ToMain, "PUSH dword[TempQWord1+4]");
    Write.Line(Write.ToMain, "PUSH dword[TempQWord1]");
    //Round it to an integer
    RoundToInteger();
    //Retrieve the integer
    Write.Line(Write.ToMain, "POP dword[TempQWord1]");
    //Clear the FPU
    Write.Line(Write.ToMain, "FINIT");
    //Load the integer into the FPU
    Write.Line(Write.ToMain, "FILD dword[TempQWord1]");
    //And finally store the integer
    Write.Line(Write.ToMain, "FIST dword[EDI+EBX*4]");
  }
  //Now do the errors, if there are any
  PostLabel(Write.ToMain, OutOfBoundsLabel);
  return;
}

/******************************************************************************
LoadArrayItem - Load an item out of the array
******************************************************************************/
void Assembly::LoadArrayItem(string Array, int Type){
  //Two labels to handle out of bounds and everything's OK
  string OutOfBoundsLabel = GetLabel();
  string OKLabel          = GetLabel();
  //Round the index to an integer
  RoundToInteger();
  //Get it
  Write.Line(Write.ToMain, "POP EBX");
  //Make sure we have a valid item
  Write.Line(Write.ToMain, "CMP EBX,dword[" + Data.Asm(Array) + "_Size]");
  Write.Line(Write.ToMain, "JGE " + OutOfBoundsLabel);
  Write.Line(Write.ToMain, "CMP EBX,0");
  Write.Line(Write.ToMain, "JLE " + OutOfBoundsLabel);
  //Get the array 
  Write.Line(Write.ToMain, "MOV EDI,dword[" + Data.Asm(Array) + "]");
  if (Type == Data.String){
    //Get the string in the array
    Write.Line(Write.ToMain, "MOV ESI,dword[EDI+EBX*4]");
    //Get the length of the string
    GetStringLength("ESI");
    Write.Line(Write.ToMain,  "MOV EBX,EAX");
    //Get some memory to store a copy in
    AllocMemory(Write.ToMain, "EBX");
    Write.Line(Write.ToMain,  "MOV EBX,EAX");
    //Copy the string
    CopyString("EBX", "ESI");
    //And then push the string
    Write.Line(Write.ToMain, "PUSH EBX");
  }
  if (Type == Data.Double){
    //Clear the FPU
    Write.Line(Write.ToMain, "FINIT");
    //Load the double from the array
    Write.Line(Write.ToMain, "FLD qword[EDI+EBX*8]");
    //Store it in a temporary variable
    Write.Line(Write.ToMain, "FST qword[TempQWord1]");
    //And push both parts to the stack
    Write.Line(Write.ToMain, "PUSH dword[TempQWord1+4]");
    Write.Line(Write.ToMain, "PUSH dword[TempQWord1]");
  }
  if (Type == Data.Integer){
    //Clear the FPU
    Write.Line(Write.ToMain, "FINIT");
    //Load the integer
    Write.Line(Write.ToMain, "FILD dword[EDI+EBX*4]");
    //Store it as a double in a temporary variable
    Write.Line(Write.ToMain, "FST qword[TempQWord1]");
    //Push both parts to the stack
    Write.Line(Write.ToMain, "PUSH dword[TempQWord1+4]");
    Write.Line(Write.ToMain, "PUSH dword[TempQWord1]");
  }
  //Now jump to the OK label, otherwise, we need to deal with an error
  Write.Line(Write.ToMain, "JMP " + OKLabel);
  PostLabel(Write.ToMain,  OutOfBoundsLabel);
  //If we do have an error, load default values
  LoadDefaults(Type);
  PostLabel(Write.ToMain,  OKLabel);
  return;
}

/******************************************************************************
LoadDefaults - Loads the defalut values incase we can't access the real
values (like an out of bounds error in an array)
******************************************************************************/
void Assembly::LoadDefaults(int Type){
  if (Type == Data.String){
    //Load an empty string
    AllocMemory(Write.ToMain, "1");
    Write.Line(Write.ToMain,  "MOV byte[EAX],0");
    Write.Line(Write.ToMain,  "PUSH EAX");
  }
  if (Type == Data.Double || Type == Data.Integer){
    //Load a zero number
    Write.Line(Write.ToMain, "FINIT");
    Write.Line(Write.ToMain, "FLDZ");
    Write.Line(Write.ToMain, "FST qword[TempQWord1]");
    Write.Line(Write.ToMain, "PUSH dword[TempQWord1+4]");
    Write.Line(Write.ToMain, "PUSH dword[TempQWord1]");
  }
  return;
}

/******************************************************************************
RoundToInteger - Rounds a double down and converts it to an integer
******************************************************************************/
void Assembly::RoundToInteger(){
  AddLibrary("floor");    //Round a double down
  //Get the double from the stack
  Write.Line(Write.ToMain, "POP dword[TempQWord2]");
  Write.Line(Write.ToMain, "POP dword[TempQWord2+4]");
  //Round the double down 
  Write.Line(Write.ToMain, "ccall floor,dword[TempQWord2],dword[TempQWord2+4]");
  //Store the double as an integer
  Write.Line(Write.ToMain, "FIST dword[TempQWord2]");
  //And push the integer back to the stack
  Write.Line(Write.ToMain, "PUSH dword[TempQWord2]");
  return;
}

/******************************************************************************
InitConsole - Initializes the console 
******************************************************************************/
void Assembly::InitConsole(){
  //Need some variables
  Write.Line(Write.ToData, "NoConsoleMessage db \'Error - Cannot access the "
                           "console handles for Input/Output.\',0");
  Write.Line(Write.ToData, "ConsoleTemp dd 0");
  //A newline character so we can cap each call to PRINT with a \n
  Write.Line(Write.ToData, "ConsoleNewLine db 10,0");
  //Of course, under Windows, things are a little more compolicated
  #ifdef Windows
    //First, decare some variables
    Write.Line(Write.ToData,   "ConsoleClear db \'CLS\',0");  //Clear the screen
    Write.Line(Write.ToData,   "HandleToInput dd 0");   //Console read handle
    Write.Line(Write.ToData,   "HandleToOutput dd 0");  //Console write handle
    //We only intialize this stuff if we have a console application
    if (AppType == Console){
      //Some labels to jump to if things are fine
      string InputOKLabel  = GetLabel();
      string OutputOKLabel = GetLabel();
      AddLibrary("GetStdHandle");      //Gets console read/write/error handles
      //Get the input (read) handle
      Write.Line(Write.ToFireUp, "stdcall GetStdHandle,-10");
      Write.Line(Write.ToFireUp, "MOV dword[HandleToInput],EAX");
      //Get the output (write) handle
      Write.Line(Write.ToFireUp, "stdcall GetStdHandle,-11");
      Write.Line(Write.ToFireUp, "MOV dword[HandleToOutput],EAX");
      //Make sure that we have a valid handles
      Write.Line(Write.ToFireUp, "CMP dword[HandleToInput],-1");
      Write.Line(Write.ToFireUp, "JNE " + InputOKLabel);
      Write.Line(Write.ToFireUp, "JMP NoConsole");
      PostLabel(Write.ToFireUp,  InputOKLabel);
      Write.Line(Write.ToFireUp, "CMP dword[HandleToOutput],-1");
      Write.Line(Write.ToFireUp, "JNE " + OutputOKLabel);
      Write.Line(Write.ToFireUp, "JMP NoConsole");    
      PostLabel(Write.ToFireUp, OutputOKLabel);
      //Our error routine informing the user something went wrong
      PostLabel(Write.ToFunction,  "NoConsole");
      Write.Line(Write.ToFunction, "MOV dword[ExitStatus],1");
      Write.Line(Write.ToFunction, "stdcall MessageBoxA,0,NoConsoleMessage,"
                                   "Error,0");
      Write.Line(Write.ToFunction, "JMP Exit");
    }
  #endif
  //Linux is so much more simple!
  #ifdef Linux
    //The clear screen command is not cls, but clear under Linux
    Write.Line(Write.ToData,   "ConsoleClear db \'clear\',0");
  #endif  
}

/******************************************************************************
ConsoleSleep - Pauses the program for a set amount of seconds
******************************************************************************/
void Assembly::ConsoleSleep(){
  #ifdef Windows
    //With windows, we need convert to milliseconds
    FormatTime();
    AddLibrary("Sleep");    //Pause execution for X milliseconds
    //Get the milliseconds to sleep
    Write.Line(Write.ToMain, "POP EBX");
    //Now pause execution
    Write.Line(Write.ToMain, "stdcall Sleep,EBX");
  #endif
  #ifdef Linux
    //With Linux, we can only sleep for seconds, not milliseconds without some
    //heavy duty functions. 
    AddLibrary("sleep");   //Pauses execution for X seconds
    //So we will just round the number to an integer
    RoundToInteger();
    //Get the number
    Write.Line(Write.ToMain, "POP EBX");
    //Now pause execution
    Write.Line(Write.ToMain, "ccall sleep,EBX");
  #endif
  return;
}

/******************************************************************************
FormatTime - Converts seconds into milliseconds for use with the Windows Sleep
API call
******************************************************************************/
void Assembly::FormatTime(){
  #ifdef Windows
    //Get the double off the stack
    Write.Line(Write.ToMain, "POP dword[TempQWord1]");
    Write.Line(Write.ToMain, "POP dword[TempQWord1+4]");
    //Use 1000 as the multiplier to convert to milliseconds
    Write.Line(Write.ToMain, "MOV dword[TempQWord2],1000");
    //Clear the FPU
    Write.Line(Write.ToMain, "FINIT");
    //Load the double
    Write.Line(Write.ToMain, "FLD qword[TempQWord1]");
    //Load the integer 1000
    Write.Line(Write.ToMain, "FILD dword[TempQWord2]");
    //Multiply
    Write.Line(Write.ToMain, "FMUL ST0,ST1");
    //Round to the nearest integer
    Write.Line(Write.ToMain, "FRNDINT");
    //And store in a temporary variable
    Write.Line(Write.ToMain, "FIST dword[TempQWord1]");
    //Push back onto the stack
    Write.Line(Write.ToMain, "PUSH dword[TempQWord1]");
  #endif
  return;
}

/******************************************************************************
ConsoleCls - Clears the console screen and locates the cursor at 0,0
******************************************************************************/
void Assembly::ConsoleCls(){
  AddLibrary("system");   //Calls a shell command like: dir, ls, copy, etc
  #ifdef Windows
    //Windows calls CLS
    Write.Line(Write.ToMain, "ccall system,ConsoleClear");
  #endif
  #ifdef Linux
    //Linux calls clear
    Write.Line(Write.ToMain, "ccall system,ConsoleClear");
  #endif
  return;
}

/******************************************************************************
ConsolePrint - prints either a number or a string to the console
******************************************************************************/
void Assembly::ConsolePrint(int Type){
  //If we are printing a string, let's be about it
  if (Type == Data.String){
    //Under Windows, we do best by calling WriteFile. Why not WriteConsole?
    //Because WriteConsole cannot be re-directed to a file
    #ifdef Windows
      AddLibrary("WriteFile");     //Writes a string to a file (or console)
      //Get the string from the stack
      Write.Line(Write.ToMain, "POP EBX");
      //Get the length of the string
      GetStringLength("EBX");
      //We don't want to print out the null-terminator, so decrement length
      Write.Line(Write.ToMain, "DEC EAX");
      //Now call WriteFile on the console output handle
      Write.Line(Write.ToMain, "stdcall WriteFile,dword[HandleToOutput],EBX,"
                               "EAX,ConsoleTemp,0");
      //Now free the string
      FreeMemory(Write.ToMain, "EBX");
    #endif
    //Under Linux, a simple, yet classic printf will do
    #ifdef Linux
      AddLibrary("printf");       //Print to the console
      //Get the string
      Write.Line(Write.ToMain, "POP EBX");
      //Print the string
      Write.Line(Write.ToMain, "ccall printf,EBX");
      //Free the string
      FreeMemory(Write.ToMain, "EBX");
    #endif
  }
  if (Type == Data.Number){
    #ifdef Windows
      AddLibrary("WriteFile");   //Write a string to a file (or console)
      //Convert the number to a string
      ConvertToString();
      //Get the string
      Write.Line(Write.ToMain, "POP EBX");
      //Get the length of the string
      GetStringLength("EBX");
      //Decrement the length of the string so we don't print the null-terminator
      Write.Line(Write.ToMain, "DEC EAX");
      //Write to the console output handle
      Write.Line(Write.ToMain, "stdcall WriteFile,dword[HandleToOutput],EBX,"
                               "EAX,ConsoleTemp,0");
      //Free the string
      FreeMemory(Write.ToMain, "EBX");
    #endif
    //Things are always so much more simple under Linux!
    #ifdef Linux
      AddLibrary("printf");   //Print a string to the console
      //Convert the double to a string
      ConvertToString();
      //Get the string
      Write.Line(Write.ToMain, "POP EBX");
      //Print the string
      Write.Line(Write.ToMain, "ccall printf,EBX");
      //Free the string
      FreeMemory(Write.ToMain, "EBX");
    #endif
  }
  return;
}

/******************************************************************************
ConvertToString - Takes a double and converts it to a string. Unfortunately,
we have a slight problem. The number 3 will be converted '3.', which gets a 
little unsightly. So we have to search out and destroy the tailing period. But
we have to leave the period in numbers like 3.14.
******************************************************************************/
void Assembly::ConvertToString(){
  //Some labels
  string StartLabel = GetLabel();
  string FoundLabel = GetLabel();
  string EndLabel   = GetLabel();
  //Get the double
  Write.Line(Write.ToMain, "POP dword[TempQWord1]");
  Write.Line(Write.ToMain, "POP dword[TempQWord1+4]");
  //Get enough memory to hold 100 characters
  AllocMemory(Write.ToMain, "100");
  Write.Line(Write.ToMain, "MOV EBX,EAX");
  //Under Windows, we call _gcvt
  #ifdef Windows
    AddLibrary("_gcvt");     //Convert a double to a string
    Write.Line(Write.ToMain, "ccall _gcvt,dword[TempQWord1],"
                             "dword[TempQWord1+4],50,EBX");
  #endif
  //Under Linux, we call gcvt - go figure!
  #ifdef Linux
    AddLibrary("gcvt");      //Convert a double to a string
    Write.Line(Write.ToMain, "ccall gcvt,dword[TempQWord1],"
                             "dword[TempQWord1+4],50,EBX");
  #endif
  //Get the length of the string
  GetStringLength("EBX");
  Write.Line(Write.ToMain, "MOV EDI,EAX");
  //Null-terminate the string
  Write.Line(Write.ToMain, "MOV ECX,0");
  //Now let'd remove some periods
  PostLabel(Write.ToMain,  StartLabel);
  //First, loop backwards until we either find a the null-terminator or the 
  //beginning of the string
  Write.Line(Write.ToMain, "CMP byte[EBX+ECX],0");
  Write.Line(Write.ToMain, "JE " + FoundLabel);
  Write.Line(Write.ToMain, "INC ECX");
  Write.Line(Write.ToMain, "CMP ECX,EDI");
  Write.Line(Write.ToMain, "JL " + StartLabel);
  Write.Line(Write.ToMain, "JMP " + EndLabel);
  PostLabel(Write.ToMain,  FoundLabel);
  //Now that we have found the null-terminator, see if we have a period next
  Write.Line(Write.ToMain, "DEC ECX");
  Write.Line(Write.ToMain, "CMP byte[EBX+ECX],\'.\'");
  Write.Line(Write.ToMain, "JNE " + EndLabel);
  //If we do have <number><period><null-terminator>, replace the period with 
  //another null-terminator
  Write.Line(Write.ToMain, "MOV byte[EBX+ECX],0");
  PostLabel(Write.ToMain,  EndLabel);
  //And return the string
  Write.Line(Write.ToMain, "PUSH EBX");
  return;
}

/******************************************************************************
ConsolePrintNewLine - Prints a new line, so each PRINT is on a single line
******************************************************************************/
void Assembly::ConsolePrintNewLine(){
  #ifdef Windows
    //Write \n to the console
    Write.Line(Write.ToMain, "stdcall WriteFile,dword[HandleToOutput],"
                             "ConsoleNewLine,1,ConsoleTemp,0");
  #endif
  #ifdef Linux
    //Write \n to the console
    Write.Line(Write.ToMain, "ccall printf,ConsoleNewLine");
  #endif
  return;
}

/******************************************************************************
ConsoleInput - Reads a string from the console
******************************************************************************/
void Assembly::ConsoleInput(){
  #ifdef Windows
    AddLibrary("ReadFile");   //Read from a file (or our input console handle)
    //By default, we can accept 16KB of information
    AllocMemory(Write.ToMain, "16384");
    Write.Line(Write.ToMain,  "MOV EBX,EAX");
    //Read from the console
    Write.Line(Write.ToMain,  "stdcall ReadFile,dword[HandleToInput],EBX,16383,"
                              "ConsoleTemp,0");
    //Get the length of the string the user entered
    GetStringLength("EBX");
    //Decrease length by 3
    Write.Line(Write.ToMain,  "DEC EAX");
    Write.Line(Write.ToMain,  "DEC EAX");
    Write.Line(Write.ToMain,  "DEC EAX");
    Write.Line(Write.ToMain,  "MOV EDI,EAX");
    //Allocate memory for the new string (so we don't occupy 16KB when only 
    //several bytes might be needed
    AllocMemory(Write.ToMain, "EDI");
    Write.Line(Write.ToMain,  "MOV ESI,EAX");
    //Copy the string
    CopyMemoryOfSize("ESI", "EBX", "EDI");
    //Null-terminate the string
    Write.Line(Write.ToMain,  "MOV byte[ESI+EDI],0");
    //And add the string to the stack
    Write.Line(Write.ToMain,  "PUSH ESI");
  #endif
  #ifdef Linux
    AddLibrary("gets");     //Gets information from the console
    //Since gets can cause an access violation, set size really large (64KB)
    AllocMemory(Write.ToMain, "65536");
    Write.Line(Write.ToMain,  "MOV EBX,EAX");
    //Get the information from the console
    Write.Line(Write.ToMain,  "ccall gets,EBX");
    //Get the length of the string the user entered
    GetStringLength("EBX");
    Write.Line(Write.ToMain,  "MOV EDI,EAX");
    //Allocate memory for the new string (so we don't occupy 64KB when only 
    //several bytes might be needed
    AllocMemory(Write.ToMain, "EDI");
    Write.Line(Write.ToMain,  "MOV ESI,EAX");
    //Copy the string
    CopyMemoryOfSize("ESI", "EBX", "EDI");
    //Null-terminate the string
    Write.Line(Write.ToMain,  "MOV byte[ESI+EDI],0");
    //And add the string to the stack
    Write.Line(Write.ToMain,  "PUSH ESI");
  #endif
  return;
}

/******************************************************************************
ConvertToNumber - Converts a string to a number
******************************************************************************/
void Assembly::ConvertToNumber(){
  AddLibrary("atof");     //Convert a string to a double
  Write.Line(Write.ToMain, "POP EBX");
  Write.Line(Write.ToMain, "ccall atof,EBX");
  //Free the string
  FreeMemory(Write.ToMain, "EBX");
  //Store the number 
  Write.Line(Write.ToMain, "FST qword[TempQWord1]");
  //Push both parts of the number
  Write.Line(Write.ToMain, "PUSH dword[TempQWord1+4]");
  Write.Line(Write.ToMain, "PUSH dword[TempQWord1]");
  return;
}

/******************************************************************************
Negate - Makes a number the oposite sign. Positive numbers become negative, and
negative numbers become positive.
******************************************************************************/
void Assembly::Negate(){
  //Get the double
  Write.Line(Write.ToMain, "POP dword[TempQWord1]");
  Write.Line(Write.ToMain, "POP dword[TempQWord1+4]");
  //Clear the FPU
  Write.Line(Write.ToMain, "FINIT");
  //Load the double
  Write.Line(Write.ToMain, "FLD qword[TempQWord1]");
  //Change the sign
  Write.Line(Write.ToMain, "FCHS");
  //Store the double
  Write.Line(Write.ToMain, "FST qword[TempQWord1]");
  //And push both parts of the double back to the stack
  Write.Line(Write.ToMain, "PUSH dword[TempQWord1+4]"); 
  Write.Line(Write.ToMain, "PUSH dword[TempQWord1]");
  return;
}

/******************************************************************************
CompareNumbers - Compare two numbers
******************************************************************************/
void Assembly::CompareNumbers(){
  //Start the calculation
  CalculateStart();
  //Compare the two numbers
  Write.Line(Write.ToMain, "FCOM ST1");
  //Store the flags in the AX register
  Write.Line(Write.ToMain, "FSTSW AX");
  //Wait until that operation is done
  Write.Line(Write.ToMain, "WAIT");
  //Now convert the flags for use with the normal JMP operators
  Write.Line(Write.ToMain, "SAHF");
  //Clear the stack
  Write.Line(Write.ToMain, "FINIT");
  //Load the true value, that is the default value
  Write.Line(Write.ToMain, "FILD dword[True]");
  //We are all ready to decide how the comparison went
  return;
}

/******************************************************************************
LoadNumberRelation - Depending on what type of relation we want, load the 
correct jump operator. One thing to notice is the fact that we aren't use the
JG (jump if greater) and JL (jump if less). For some strange reason, comparisons
with FCOM can only be decided by JA (jump if above) and JB (jump if below). 
******************************************************************************/
void Assembly::LoadNumberRelation(int Relation){
  string True = GetLabel();
  if (Relation == Equal){
    Write.Line(Write.ToMain, "JE " + True);
  }
  if (Relation == NotEqual){
    Write.Line(Write.ToMain, "JNE " + True);  
  }
  if (Relation == Greater){
    Write.Line(Write.ToMain, "JA " + True);
  }
  if (Relation == Less){
    Write.Line(Write.ToMain, "JB " + True);
  }
  if (Relation == GreaterOrEqual){
    Write.Line(Write.ToMain, "JAE " + True);
  }
  if (Relation == LessOrEqual){
    Write.Line(Write.ToMain, "JBE " + True);
  }
  //Depending on how the above comparisons went, we may or may not load zero
  Write.Line(Write.ToMain, "FLDZ");
  //Now either true or false is at the top of the FPU - that is the result of
  //the comparison
  PostLabel(Write.ToMain,  True);
  //Load the result of the comparison on the stack
  CalculateEnd();
  return;
}

/******************************************************************************
CompareStrings - Compares two strings 
******************************************************************************/
void Assembly::CompareStrings(){
  //Get both strings from the stack
  Write.Line(Write.ToMain, "POP EBX");
  Write.Line(Write.ToMain, "POP EDI");
  //If we are using Windows, we will call lstrcpmA
  #ifdef Windows
    AddLibrary("lstrcmpA");  //Compare two strings
    Write.Line(Write.ToMain, "stdcall lstrcmpA,EDI,EBX");
  #endif
  //If we are using Linux, we will call strcmp
  #ifdef Linux
    AddLibrary("strcmp");    //Compare two strings
    Write.Line(Write.ToMain, "ccall strcmp,EDI,EBX");
  #endif
  //Preserve the return value
  Write.Line(Write.ToMain, "MOV ESI,EAX");
  //Now free both strings
  FreeMemory(Write.ToMain, "EBX");
  FreeMemory(Write.ToMain, "EDI");
  //Clear the FPU
  Write.Line(Write.ToMain, "FINIT");
  //Load true as the default value
  Write.Line(Write.ToMain, "FILD dword[True]");
  //Compare the the result of the string comparison to 0
  Write.Line(Write.ToMain, "CMP ESI,0");
  //We are now ready to see how the comparison went
  return;
}

/******************************************************************************
LoadStringRelation - For normal string comparison's, we have to use the JG 
(jump if greater) and JL (jump if less), so we can't use the same function as 
we did for numbers
******************************************************************************/
void Assembly::LoadStringRelation(int Relation){
  string True = GetLabel();
  if (Relation == Equal){
    Write.Line(Write.ToMain, "JE " + True);
  }
  if (Relation == NotEqual){
    Write.Line(Write.ToMain, "JNE " + True);  
  }
  if (Relation == Greater){
    Write.Line(Write.ToMain, "JG " + True);
  }
  if (Relation == Less){
    Write.Line(Write.ToMain, "JL " + True);
  }
  if (Relation == GreaterOrEqual){
    Write.Line(Write.ToMain, "JGE " + True);
  }
  if (Relation == LessOrEqual){
    Write.Line(Write.ToMain, "JLE " + True);
  }
  //Depending on how the comparison went, we may or may not load zero
  Write.Line(Write.ToMain, "FLDZ");
  //Now either true or false is at the top of the FPU - that is the result of
  //the comparison
  PostLabel(Write.ToMain,  True);
  //Now, return the result of the comparison
  CalculateEnd();
  return;
}

/******************************************************************************
StartIf - After we have the comparison done, we can use it for an if statement
******************************************************************************/
string Assembly::StartIf(){
  //Labels to decide what to do
  string EndLabel = GetLabel();
  string TempLabel = GetLabel();
  //Get the result of the comparison
  Write.Line(Write.ToMain, "POP dword[TempQWord1]");
  Write.Line(Write.ToMain, "POP dword[TempQWord1+4]");
  //Clear the FPU
  Write.Line(Write.ToMain, "FINIT");
  //Load the result of the comparison
  Write.Line(Write.ToMain, "FLD qword[TempQWord1]");
  //Load zero (false)
  Write.Line(Write.ToMain, "FLDZ");
  //Compare the two numbers
  Write.Line(Write.ToMain, "FCOM ST1");
  //Store the flags in the AX register
  Write.Line(Write.ToMain, "FSTSW AX");
  //Wait until the operation is done
  Write.Line(Write.ToMain, "WAIT");
  //Convert the flags to a usable state
  Write.Line(Write.ToMain, "SAHF");
  //If the result is not false (meaning the comparison was true), we jump over
  //the jump to the end of the if section (meaning we continue with the innards
  //of the if section)
  Write.Line(Write.ToMain, "JNE " + TempLabel);
  //Otherwise, jump to the end of the if section
  Write.Line(Write.ToMain, "JMP " + EndLabel);
  PostLabel(Write.ToMain,  TempLabel);
  //Return the EndLabel, which will be posted at the end of the if statement. It
  //is an easy way to 'drop through' an if statement without executing the code
  return EndLabel;
}

/******************************************************************************
StartNewIfSection - 
******************************************************************************/
string Assembly::StartNewIfSection(string LastEndLabel, string DoneIf){
  Write.Line(Write.ToMain, "JMP " + DoneIf);
  PostLabel(Write.ToMain,  LastEndLabel);
  return "";
}

/******************************************************************************
ElseIf - Starts a new if block
******************************************************************************/
string Assembly::ElseIf(string LastEndLabel, string DoneIf){
  //Lables to decide where to go
  string EndLabel  = GetLabel();
  string TempLabel = GetLabel();
  //Get the result of the comparison
  Write.Line(Write.ToMain, "POP dword[TempQWord1]");
  Write.Line(Write.ToMain, "POP dword[TempQWord1+4]");
  //Clear the FPU
  Write.Line(Write.ToMain, "FINIT");
  //Load the result
  Write.Line(Write.ToMain, "FLD qword[TempQWord1]");
  //Load a zero
  Write.Line(Write.ToMain, "FLDZ");
  //See if the result was fals (compare the two numbers)
  Write.Line(Write.ToMain, "FCOM ST1");
  //Store the flags
  Write.Line(Write.ToMain, "FSTSW AX");
  //Wait until its done
  Write.Line(Write.ToMain, "WAIT");
  //Convert the flags
  Write.Line(Write.ToMain, "SAHF");
  //If the result of the comparison isn't false, jump over the jump to the end 
  //of the block (meaning we execute the inside of the else if block)
  Write.Line(Write.ToMain, "JNE " + TempLabel);
  Write.Line(Write.ToMain, "JMP " + EndLabel);
  PostLabel(Write.ToMain,  TempLabel);
  //Return the end of the label for the Compiler object to post at the end of
  //the block of code
  return EndLabel;
}

/******************************************************************************
EndProgram - Cleans up the program and exits
******************************************************************************/
void Assembly::EndProgram(){
  //Jump to the exit routine, where we clean up all memory and variables, and 
  //the end the program
  Write.Line(Write.ToMain, "JMP Exit");
  return;
}

/******************************************************************************
EndIf - Ends an if statement 
******************************************************************************/
void Assembly::EndIf(string EndLabel, string DoneIf){
  //Jump the end of the block
  Write.Line(Write.ToMain, "JMP " + EndLabel);
  //Post both the end of the block and the end of the if statement labels
  PostLabel(Write.ToMain, EndLabel);
  PostLabel(Write.ToMain, DoneIf);
  return;
}

/******************************************************************************
Else - if all else fails, execute this block of code
******************************************************************************/
string Assembly::Else(string EndLabel, string DoneIf){
  //Get a label for the end of the else block
  string NextEndLabel = GetLabel();
  //If we have already executed an if statement, jump to the end of the whole
  //if statement
  Write.Line(Write.ToMain, "JMP " + DoneIf);
  //Otherwise, drop through to the else block
  PostLabel(Write.ToMain,  EndLabel);
  return NextEndLabel;
}

/******************************************************************************
PrepareWhile - Creates a place to start looping
******************************************************************************/
string Assembly::PrepareWhile(){
  //Generate, post, and return the place where we start looping
  string StartLabel = GetLabel();
  PostLabel(Write.ToMain, StartLabel);
  return StartLabel;
}

/******************************************************************************
StartWhile - Every loop we make, we make sure the condition is still true
******************************************************************************/
string Assembly::StartWhile(){
  string EndWhileLabel = GetLabel();
  string TempLabel     = GetLabel();
  //Get the numbers
  Write.Line(Write.ToMain, "POP dword[TempQWord1]");
  Write.Line(Write.ToMain, "POP dword[TempQWord1+4]");
  //Clear the FPU
  Write.Line(Write.ToMain, "FINIT");
  //Load the result of the comparison
  Write.Line(Write.ToMain, "FLD qword[TempQWord1]");
  //Load 0 (false)
  Write.Line(Write.ToMain, "FLDZ");
  //See if the result is false
  Write.Line(Write.ToMain, "FCOM ST1");
  //Store flags
  Write.Line(Write.ToMain, "FSTSW AX");
  Write.Line(Write.ToMain, "WAIT");
  //Convert flags
  Write.Line(Write.ToMain, "SAHF");
  //Decide whether or not to jump out of the loop
  Write.Line(Write.ToMain, "JNE " + TempLabel);
  Write.Line(Write.ToMain, "JMP " + EndWhileLabel);
  PostLabel(Write.ToMain,  TempLabel);
  return EndWhileLabel;
}

/******************************************************************************
StopWhile - The place where the loop ends, we can safely jump here and continue
running the rest of the program
******************************************************************************/
void Assembly::StopWhile(string StartWhileLabel, string EndWhileLabel){
  Write.Line(Write.ToMain, "JMP " + StartWhileLabel);
  PostLabel(Write.ToMain, EndWhileLabel);
  return;
}

/******************************************************************************
Or - Takes the result of two comparisons and performs a logical OR. That means
that if at least one of the two comparisons is true, we return true
******************************************************************************/
void Assembly::Or(){
  //Some labels to help us keep track of things
  string TrueLabel        = GetLabel();
  string FalseLabel       = GetLabel();
  string CompareNextLabel = GetLabel();
  string EndLabel         = GetLabel();
  Write.Line(Write.ToMain, "POP dword[TempQWord1]");
  Write.Line(Write.ToMain, "POP dword[TempQWord1+4]");
  Write.Line(Write.ToMain, "FINIT");
  Write.Line(Write.ToMain, "FLD qword[TempQWord1]");
  Write.Line(Write.ToMain, "FLDZ");
  Write.Line(Write.ToMain, "FCOM ST1");
  Write.Line(Write.ToMain, "FSTSW AX");
  Write.Line(Write.ToMain, "WAIT");
  Write.Line(Write.ToMain, "SAHF");
  //If the first condition is false, we can always test the second
  Write.Line(Write.ToMain, "JE " + CompareNextLabel);
  //However, if the first condition is true, we don't need to test the second
  //comparison. However, we do need to de-clutter the stack even if we aren't
  //doing any computation
  Write.Line(Write.ToMain, "POP dword[TempQWord1]");
  Write.Line(Write.ToMain, "POP dword[TempQWord1+4]");
  Write.Line(Write.ToMain, "JMP " + TrueLabel);
  PostLabel(Write.ToMain,  CompareNextLabel);
  Write.Line(Write.ToMain, "POP dword[TempQWord1]");
  Write.Line(Write.ToMain, "POP dword[TempQWord1+4]");
  Write.Line(Write.ToMain, "FINIT");
  Write.Line(Write.ToMain, "FLD qword[TempQWord1]");
  Write.Line(Write.ToMain, "FLDZ");
  Write.Line(Write.ToMain, "FCOM ST1");
  Write.Line(Write.ToMain, "FSTSW AX");
  Write.Line(Write.ToMain, "WAIT");
  Write.Line(Write.ToMain, "SAHF");
  //If both of the comparisons ar false, we load false
  Write.Line(Write.ToMain, "JE " + FalseLabel);
  //Otherwise, we load true
  PostLabel(Write.ToMain,  TrueLabel);
  Write.Line(Write.ToMain, "FINIT"); 
  Write.Line(Write.ToMain, "FILD dword[True]");
  Write.Line(Write.ToMain, "JMP " + EndLabel);
  PostLabel(Write.ToMain,  FalseLabel);
  Write.Line(Write.ToMain, "FINIT"); 
  Write.Line(Write.ToMain, "FLDZ");
  PostLabel(Write.ToMain,  EndLabel);
  CalculateEnd();
  return;
}

/******************************************************************************
And - peforms a logical AND and two comparisons. If both are comparisons are 
true, we return true, otherwise, we return false
******************************************************************************/
void Assembly::And(){
  //Some labels to help us
  string TrueLabel        = GetLabel();
  string FalseLabel       = GetLabel();
  string CompareNextLabel = GetLabel();
  string EndLabel         = GetLabel();
  Write.Line(Write.ToMain, "POP dword[TempQWord1]");
  Write.Line(Write.ToMain, "POP dword[TempQWord1+4]");
  Write.Line(Write.ToMain, "FINIT");
  Write.Line(Write.ToMain, "FLD qword[TempQWord1]");
  Write.Line(Write.ToMain, "FLDZ");
  Write.Line(Write.ToMain, "FCOM ST1");
  Write.Line(Write.ToMain, "FSTSW AX");
  Write.Line(Write.ToMain, "WAIT");
  Write.Line(Write.ToMain, "SAHF");
  Write.Line(Write.ToMain, "POP dword[TempQWord1]");
  Write.Line(Write.ToMain, "POP dword[TempQWord1+4]");
  //If the comparison is false, we don't need to do the other comparison, just
  //load false
  Write.Line(Write.ToMain, "JNE " + CompareNextLabel);
  //Otherwise, test the other comparison to see if it is true also
  Write.Line(Write.ToMain, "JMP " + FalseLabel);
  PostLabel(Write.ToMain,  CompareNextLabel);
  Write.Line(Write.ToMain, "FINIT");
  Write.Line(Write.ToMain, "FLD qword[TempQWord1]");
  Write.Line(Write.ToMain, "FLDZ");
  Write.Line(Write.ToMain, "FCOM ST1");
  Write.Line(Write.ToMain, "FSTSW AX");
  Write.Line(Write.ToMain, "WAIT");
  Write.Line(Write.ToMain, "SAHF");
  //If the comparison is true, load true
  Write.Line(Write.ToMain, "JNE " + TrueLabel);
  //However, even if one is false, load false
  Write.Line(Write.ToMain, "JMP " + FalseLabel);
  PostLabel(Write.ToMain,  TrueLabel);
  Write.Line(Write.ToMain, "FINIT"); 
  Write.Line(Write.ToMain, "FILD dword[True]");
  Write.Line(Write.ToMain, "JMP " + EndLabel);
  PostLabel(Write.ToMain,  FalseLabel);
  Write.Line(Write.ToMain, "FLDZ");
  PostLabel(Write.ToMain,  EndLabel);
  CalculateEnd();
  return;
}

/******************************************************************************
Not - Performs a logical NOT. If the comparison is true, we return false. If 
the comparison is false, we return true
******************************************************************************/
void Assembly::Not(){
  string FalseLabel = GetLabel();
  string TrueLabel  = GetLabel();
  Write.Line(Write.ToMain, "POP dword[TempQWord1]");
  Write.Line(Write.ToMain, "POP dword[TempQWord1+4]");
  Write.Line(Write.ToMain, "FINIT");
  Write.Line(Write.ToMain, "FLD qword[TempQWord1]");
  //Load zero (false) as the default value. This zero also serves as the 
  //test variable to test the comaprison with. Tricky, aye?
  Write.Line(Write.ToMain, "FLDZ");
  Write.Line(Write.ToMain, "FCOM ST1");
  Write.Line(Write.ToMain, "FSTSW AX");
  Write.Line(Write.ToMain, "WAIT");
  Write.Line(Write.ToMain, "SAHF");
  //If the comparison is true (not equal to false), we keep the zero (false)
  //already loaded
  Write.Line(Write.ToMain, "JNE " + FalseLabel);
  //Otherwise we load true
  Write.Line(Write.ToMain, "FINIT");
  Write.Line(Write.ToMain, "FILD dword[True]");
  PostLabel(Write.ToMain,  FalseLabel);
  CalculateEnd();
  return;
}

/******************************************************************************
CreateSubFunction - creates the beginning of function.
******************************************************************************/
void Assembly::CreateSubFunction(string Name, bool External){
  //Can't create an external function!
  if (External == false){
    Write.Line(Write.ToFunction, "");
    //If we are optimizing, add the ifdef statement to either include or
    //exclude from assembling. Later, we can define all the functions that we
    //are using; all the functions we don't define (because they weren't used)
    //will be excluded from the program
    if (Optimize == true){
      Write.Line(Write.ToFunction, "%ifdef " + Data.StripJunkOff(Name)+"_Used");
    }
    Write.Line(Write.ToFunction, "");
    //This is a global function
    Write.Line(Write.ToFunction, "global " + Data.Asm(Name));
    //The function begins at the label
    Write.Line(Write.ToFunction, Data.Asm(Name) + ":");
    //Perserve registers and the stack so we can restore them after the function
    //is finished
    Write.Line(Write.ToFunction, "PUSH EBP");
    Write.Line(Write.ToFunction, "MOV EBP,ESP");
    Write.Line(Write.ToFunction, "PUSH EBX");
    Write.Line(Write.ToFunction, "PUSH EDI");
    Write.Line(Write.ToFunction, "PUSH ESI");
  }
  return;
}

/******************************************************************************
EndCreateSubFunction - Finishes creating the function. This is where we create
the external functions, because we have all relevent information
******************************************************************************/
void Assembly::EndCreateSubFunction(string Name, bool External){
  //Deal with external and user-created functiond differntly
  if (External == false){
    //Resorte the regesters and teh stack
    Write.Line(Write.ToFunction, "POP ESI");
    Write.Line(Write.ToFunction, "POP EDI");
    Write.Line(Write.ToFunction, "POP EBX");
    Write.Line(Write.ToFunction, "MOV ESP,EBP");
    Write.Line(Write.ToFunction, "POP EBP");
    //For Windows, we will RET <Size of Parameters> so we adhere to the 
    //standard calling convention. This basically cleans off the stack
    //after returning
    #ifdef Windows
      Write.Line(Write.ToFunction, "RET " + 
                 ToStr(Data.GetSubFunctionInfo(Name).SizeOfParameters));
    #endif
    //With Linux, we like to leave things for the calling process to deal with,
    //so we just a simple RET
    #ifdef Linux
      Write.Line(Write.ToFunction, "RET");
    #endif
    //If we are optimizing, let's go ahead and end the block of the function
    if (Optimize == true){
      Write.Line(Write.ToFunction, "");
      Write.Line(Write.ToFunction, "%endif");
    }
  }
  if (External == true){
    //Optimize by surrounding the function with a ifdef conditional assembly
    //directive.
    if (Optimize == true){
      Write.Line(Write.ToData, "%ifdef " + Data.StripJunkOff(Name) + "_Used");
    }
    //OK, we need a variable to store the address to the function in
    Write.Line(Write.ToData, Data.Asm(Name) + " dd 0");
    //We also need a place to store the handle of the library that the function 
    //is in
    Write.Line(Write.ToData, Data.Asm(Name) + "_LibHandle dd 0");
    //A variable to store the real name of the external function
    Write.Line(Write.ToData, Data.Asm(Name) + "_Alias db \"" + 
                     Data.GetSubFunctionInfo(Name).ExternalInfo.Alias + "\",0");
    //A variable to store the name of the library
    Write.Line(Write.ToData, Data.Asm(Name) + "_Lib db \"" + 
                   Data.GetSubFunctionInfo(Name).ExternalInfo.Library + "\",0");
    //Also indicate which calling convention to use
    if (Data.GetSubFunctionInfo(Name).ExternalInfo.CallingConv == "STD"){
      Write.Line(Write.ToData, Data.Asm(Name) + "_Call db 0");
    }
    else{
      Write.Line(Write.ToData, Data.Asm(Name) + "_Call db 1");    
    }
    //Also if this is an external function, we have to deal with freeing the
    //library
    #ifdef Windows
      AddLibrary("FreeLibrary");    //Tells the system we don't need the library
      if (Optimize == true){
        Write.Line(Write.ToFinishUp, "%ifdef " + Data.StripJunkOff(Name) + 
                                     "_Used");
      }
      //For a DLL, the FreeLibrary has already been called because of the 
      //positioning - FreeLibrary must go in a certain place for DLLs
      if (AppType == GUI || AppType == Console){
        Write.Line(Write.ToFinishUp, "stdcall FreeLibrary,dword[" + 
                                     Data.Asm(Name) + "_LibHandle]");
      }
      if (Optimize == true){
        Write.Line(Write.ToFinishUp, "%endif");
      }
    #endif
    #ifdef Linux
      AddLibrary("dlclose");        //Tells the system we don't need the library
      Write.Line(Write.ToFinishUp, "ccall dlclose," + Data.Asm(Name) + 
                                   "_LibHandle");
    #endif
    //End the function related code
    if (Optimize == true){
      Write.Line(Write.ToData, "%endif");
    }
  }
  return;
}

/******************************************************************************
InvokeSubFunction - calls a function
******************************************************************************/
void Assembly::InvokeSubFunction(string Name){
  //If this is a normal function, just call it
  if (Data.GetSubFunctionInfo(Name).External == false){
    Write.Line(Write.ToMain, "CALL " + Data.Asm(Name));
  }
  //Otherwise, we may have to load it first
  if (Data.GetSubFunctionInfo(Name).External == true){
    CallExternalSubFunction(Name);
  }
  return;
}

/******************************************************************************
ReturnValue - Before we exit the function, make sure we put the return value 
in its rightful place
******************************************************************************/
void Assembly::ReturnValue(string Name, string Type){
  //If we are returning doubles, we need to put it on the FPU
  if (Type == "DOUBLE"){
    Write.Line(Write.ToFunction, "FINIT");
    Write.Line(Write.ToFunction, "FLD qword[EBP-8]");
  }
  //Strings and integers are stored in the EAX register
  if (Type == "INTEGER" || Type == "STRING"){
    Write.Line(Write.ToFunction, "MOV EAX,dword[EBP-4]");
  }
  return;
}

/******************************************************************************
CleanUpReturnValue - Incase the program doesn't use the return value, we need
to get rid of it so we don't cause a memory leak
******************************************************************************/
void Assembly::CleanUpReturnValue(string Name, string Type){
  //Cleare the FPU if we have a double
  if (Type == "DOUBLE"){
    Write.Line(Write.ToMain, "FINIT");
  }
  //If we have a string, free it
  if (Type == "STRING"){
    FreeMemory(Write.ToMain, "EAX");
  }
  return;
}

/******************************************************************************
PushReturnValue - However, if we are going to use the return value, push it
onto the stack
******************************************************************************/
void Assembly::PushReturnValue(string Name, string Type){
  //If we have an integer, just load it, and drop through to the second
  //if statement to actually push it onto the stack
  if (Type == "INTEGER"){
    Write.Line(Write.ToMain, "MOV dword[TempQWord1],EAX");
    Write.Line(Write.ToMain, "FINIT");
    Write.Line(Write.ToMain, "FILD dword[TempQWord1]");
  }
  //Once we have the double in the FPU, store it and push both parts
  if (Type == "DOUBLE" || Type == "INTEGER"){
    Write.Line(Write.ToMain, "FST qword[TempQWord1]");
    Write.Line(Write.ToMain, "PUSH dword[TempQWord1+4]");
    Write.Line(Write.ToMain, "PUSH dword[TempQWord1]");
  }
  //If all we have is a string, go ahead and push it
  if (Type == "STRING"){
    Write.Line(Write.ToMain, "PUSH EAX");
  }
  return;
}

/******************************************************************************
LoadExternalSubFunction - To load an function from an external library, we
need to jump through a few loops
******************************************************************************/
void Assembly::LoadExternalSubFunction(string Name){
  #ifdef Windows
    AddLibrary("LoadLibraryA");       //Get the handle to the library
    AddLibrary("GetProcAddress");     //Get the address of the function 
    string GoodLibraryLabel     = GetLabel();
    string GoodSubFunctionLabel = GetLabel();
    //See if we can get the handle of the library where the function resides
    Write.Line(Write.ToMain, "stdcall LoadLibraryA," + Data.Asm(Name) + "_Lib");
    Write.Line(Write.ToMain, "MOV dword[" + Data.Asm(Name) + "_LibHandle],EAX");
    //Verify that we actually got the handle to the library before we go
    //charging off
    Write.Line(Write.ToMain, "CMP EAX,0");
    Write.Line(Write.ToMain, "JNE " + GoodLibraryLabel);
    Write.Line(Write.ToMain, "PUSH " + Data.Asm(Name) + "_Lib");
    Write.Line(Write.ToMain, "JMP NoLibrary");
    PostLabel(Write.ToMain,  GoodLibraryLabel);
    //Now that we are sure we have a good library name, get the address of the
    //function within the library
    Write.Line(Write.ToMain, "stdcall GetProcAddress,dword[" + Data.Asm(Name) + 
                             "_LibHandle]," + Data.Asm(Name) + "_Alias");
    Write.Line(Write.ToMain, "MOV dword[" + Data.Asm(Name) + "],EAX");
    //Also make sure that we got the address, otherwise we will be calling a 
    //null pointer!
    Write.Line(Write.ToMain, "CMP EAX,0");
    Write.Line(Write.ToMain, "JNE " + GoodSubFunctionLabel);
    Write.Line(Write.ToMain, "PUSH " + Data.Asm(Name) + "_Alias");
    Write.Line(Write.ToMain, "JMP NoFunction");
    PostLabel(Write.ToMain,  GoodSubFunctionLabel);
    //If the apptype is a DLL, make sure to free the library. The reason for 
    //this is that we need to make sure the FreeLibrary goes in a certain place
    if (AppType == DLL){
      Write.Line(Write.ToFinishUp, "stdcall FreeLibrary,dword[" + 
                                   Data.Asm(Name) + "_LibHandle]");
    }
  #endif
  //Linux uses some different functions, but the idea is the same
  #ifdef Linux
    AddLibrary("dlerror");      //Reports the error in loading a library
    AddLibrary("dlopen");       //Gets the handle to a library
    AddLibrary("dlsym");        //Gets the address of a function in the library
    string GoodLibraryLabel     = GetLabel();
    string GoodSubFunctionLabel = GetLabel();
    //Get the handle to the library
    Write.Line(Write.ToMain, "ccall dlopen," + Data.Asm(Name) + "_Lib,1");
    Write.Line(Write.ToMain, "MOV dword[" + Data.Asm(Name) + "_LibHandle],EAX");
    //Make sure the handle is valid
    Write.Line(Write.ToMain, "CMP EAX,0");
    Write.Line(Write.ToMain, "JNE " + GoodLibraryLabel);
    Write.Line(Write.ToMain, "JMP Exit");
    PostLabel(Write.ToMain,  GoodLibraryLabel);
    //Get the address of the function
    Write.Line(Write.ToMain, "ccall dlysm,dword[" + Data.Asm(Name) + 
                             "_LibHandle]," + Data.Asm(Name) + "_Alias");
    Write.Line(Write.ToMain, "MOV dword[" + Data.Asm(Name) + "],EAX");
    //Ad make sure that we have a valid address, too
    Write.Line(Write.ToMain, "CMP EAX,0");
    Write.Line(Write.ToMain, "JNE " + GoodSubFunctionLabel);
    Write.Line(Write.ToMain, "JMP Exit");
    PostLabel(Write.ToMain,  GoodSubFunctionLabel);
  #endif
  return;
}

/******************************************************************************
CallExternalSubFunction - calls an external function. If the function hasn't 
been loaded yet, we will go ahead and do it. 
******************************************************************************/
void Assembly::CallExternalSubFunction(string Name){
  string CallLabel = GetLabel();
  //Check to see if we have a valid address to call
  Write.Line(Write.ToMain, "CMP dword[" + Data.Asm(Name) + "],0");
  Write.Line(Write.ToMain, "JNE " + CallLabel);
  //If not, load the address before calling
  LoadExternalSubFunction(Name);
  PostLabel(Write.ToMain,  CallLabel);
  //Call the address of the function
  Write.Line(Write.ToMain, "CALL dword[" + Data.Asm(Name) + "]");
  return;
}

/******************************************************************************
AllocateParameterPool - For functions, we need a pool of memory to store 
parameters in so we can get them normally (first to last), but push them onto
the stack in reverse order (last to first). Since we can't do that normally,
we store them temporarily until we have all the parameters in storage. Then we
push them backwards.
******************************************************************************/
void Assembly::AllocateParameterPool(int Size){
  //In case we are calling recrusive functions, push the old pool of memory
  //That way, we can restore it when we finish this function
  PUSH("dword[ParameterPool]");
  //Get enough memory for the next pool
  AllocMemory(Write.ToMain, ToStr(Size));
  //And fill the pool with memory
  Write.Line(Write.ToMain, "MOV dword[ParameterPool],EAX");
  return;
}

/******************************************************************************
AddToParameterPool - Adds a variable to the pool of parameters
******************************************************************************/
void Assembly::AddToParameterPool(int Type, int Where){
  //Get the pool of memory for the parameters
  Write.Line(Write.ToMain, "MOV EAX,dword[ParameterPool]");
  //For integers, strings, and types, just pop directly to the pool
  if (Type == Data.Integer || Type == Data.String || Type == Data.Type){
    Write.Line(Write.ToMain, "POP dword[EAX+" + ToStr(Where) + "]");
  }
  //Doubles require a double pop (no pun intended)
  if (Type == Data.Double){
    Write.Line(Write.ToMain, "POP dword[EAX+" + ToStr(Where) + "]");
    Write.Line(Write.ToMain, "POP dword[EAX+" + ToStr(Where) + "+4]");
  }
  return;
}

/******************************************************************************
PushParameterPool - Push a parameter from the pool to the stack
******************************************************************************/
void Assembly::PushParameterPool(int Type, int Where){
  Write.Line(Write.ToMain, "MOV EAX,dword[ParameterPool]");
  //If we have an integer or string, just do a simple push
  if (Type == Data.Integer  || Type == Data.String){
    Write.Line(Write.ToMain, "PUSH dword[EAX+" + ToStr(Where) + "]");
  }
  //Push both parts of a double
  if (Type == Data.Double){
    Write.Line(Write.ToMain, "PUSH dword[EAX+" + ToStr(Where) + "+4]");
    Write.Line(Write.ToMain, "PUSH dword[EAX+" + ToStr(Where) + "]");
  }  
  //Get the address of the type, and then push the actual type
  if (Type == Data.Type){
    Write.Line(Write.ToMain, "MOV EAX,dword[EAX+" + ToStr(Where) + "]");
    Write.Line(Write.ToMain, "PUSH dword[EAX]");
  }
  return;
}

/******************************************************************************
FreeparameterPool - Free the pool of parameters
******************************************************************************/
void Assembly::FreeParameterPool(){
  //Perserve the return value by pushing it to the stack
  PUSH("EAX");
  //Now get the pool of memory
  Write.Line(Write.ToMain, "MOV EAX,dword[ParameterPool]");
  //And free it
  FreeMemory(Write.ToMain, "EAX");
  //Restore the return value
  POP("EAX");
  //And get the last pool of parameters that we pushed before creating this
  //pool
  Write.Line(Write.ToMain, "POP dword[ParameterPool]");
}

/******************************************************************************
Callback - gets the address of a function
******************************************************************************/
void Assembly::Callback(string Name){
  //If this is an external function, deal with it differently
  if (Data.GetSubFunctionInfo(Name).External == true){
    string PushLabel = GetLabel();
    //Check to see if we have a valid address to push
    Write.Line(Write.ToMain, "CMP dword[" + Data.Asm(Name) + "],0");
    Write.Line(Write.ToMain, "JNE " + PushLabel);
    //If not, load the address before calling
    LoadExternalSubFunction(Name);
    PostLabel(Write.ToMain,  PushLabel);
    //Push the address of the function
    PUSH("dword[" + Data.Asm(Name) + "]");
  }
  //Othewise, a simple push will suffice
  else{
    PUSH(Data.Asm(Name));
  }
  //Pop the address
  POP("dword[TempQWord1]");
  //Clear the FPU
  Write.Line(Write.ToMain, "FINIT");
  //Load the address of the function
  Write.Line(Write.ToMain, "FILD dword[TempQWord1]");
  //Store it as a double
  Write.Line(Write.ToMain, "FST qword[TempQWord1]");
  //And push both parts
  Write.Line(Write.ToMain, "PUSH dword[TempQWord1+4]");
  Write.Line(Write.ToMain, "PUSH dword[TempQWord1]");
  return;
}

/******************************************************************************
AdjustStack - Adjusts the stack after a function that uses the C calling
convention is called. Basically, removes the parameters from the stack.
******************************************************************************/
void Assembly::AdjustStack(int Size){
  Write.Line(Write.ToMain, "ADD ESP," + ToStr(Size));
  return;
}

/******************************************************************************
GetPreSetVariables - Gets some really important pre-set variables
******************************************************************************/
void Assembly::GetPreSetVariables(){
  //Unfortunately, these variables apply only to the Windwos platform
  #ifdef Windows
    SimpleDataInfo Info;
    AddLibrary("GetModuleHandleA");    //Gets the handle to the program
    AddLibrary("GetCommandLineA");     //Gets the command line of the program
    //Create some data to hold the information
    Write.Line(Write.ToData,   "Internal_HInstance dd 0");
    Write.Line(Write.ToData,   "Internal_CommandLine dd 0");
    //OK, get the handle to the program
    Write.Line(Write.ToFireUp, "stdcall GetModuleHandleA,0");
    Write.Line(Write.ToFireUp, "MOV dword[Internal_HInstance],EAX");
    //Add the variable to the database
    Info.AsmName = "Internal_HInstance";
    Info.Type    = Data.Integer;
    Data.AddSimpleData("HINSTANCE", Info);
    //If this is a console program, get the command line as well
    //Technically, this could be used for GUI programs as well...
    if (AppType == Console){
      //OK, get the commandline
      Write.Line(Write.ToFireUp, "CALL GetCommandLineA");
      Write.Line(Write.ToFireUp, "MOV dword[Internal_CommandLine],EAX");  
      //And store it in the database
      Info.AsmName = "Internal_CommandLine";
      Info.Type    = Data.String;
      Data.AddSimpleData("COMMANDLINE$", Info);
    }
  #endif
  return;
}

/******************************************************************************
OptimizeSubFunctions - Has the database object return a formatted list of all
the functions the program uses. Then we write that list to the beginning of the
assembly language file. The assembler then leaves out all the functions that 
weren't used, which reduces bloat by a lot if you use big libraries
******************************************************************************/
void Assembly::OptimizeSubFunctions(){
  Write.Line(Write.ToLibrary, Data.GetUsedSubFunctions());
  return;
}

/******************************************************************************
PrepareErrorMessages - Creates standard messages so we can alert the user if 
anything goes wrong
******************************************************************************/
void Assembly::PrepareErrorMessages(){
  //The standard error messages
  Write.Line(Write.ToData, "NoMemMessage db \"Could not allocate memory. Please"
                           " free up some memory and re-run this app. "
                           "Thanks!\",0");
  Write.Line(Write.ToData, "NoLibFound db \"Could not find library: \",0");
  Write.Line(Write.ToData, "NoFunctionFound db \"Cound not find function: "
                           "\",0");
  Write.Line(Write.ToData, "Error db \"Error!\",0");
  #ifdef Windows
    AddLibrary("MessageBoxA");
    AddLibrary("lstrcat");
    AddLibrary("lstrlen");
    AddLibrary("lstrcpy");
    PostLabel(Write.ToFunction,  "NoMemory");
    Write.Line(Write.ToFunction, "MOV dword[ExitStatus],1");
    Write.Line(Write.ToFunction, "stdcall MessageBoxA,0,NoMemMessage,Error,0");
    Write.Line(Write.ToFunction, "JMP Exit");
    Write.Line(Write.ToFunction, "");
    PostLabel(Write.ToFunction, "NoLibrary");
    Write.Line(Write.ToFunction, "MOV dword[ExitStatus],1");
    Write.Line(Write.ToFunction, "POP EBX");
    Write.Line(Write.ToFunction, "stdcall lstrlen,EBX");
    Write.Line(Write.ToFunction, "ADD EAX,30");
    if (AppType == Console || AppType == GUI){
      Write.Line(Write.ToFunction, "stdcall HeapAlloc,dword[HandleToHeap],8,EAX");
    } 
    if (AppType == DLL){
      AddLibrary("GetProcessHeap");   //Get the program's default heap
      Write.Line(Write.ToFunction, "CALL GetProcessHeap");
      Write.Line(Write.ToFunction, "stdcall HeapAlloc,EAX,8,EAX");
    }
    Write.Line(Write.ToFunction, "MOV EDI,EAX");
    Write.Line(Write.ToFunction, "stdcall lstrcpy,EDI,NoLibFound");
    Write.Line(Write.ToFunction, "stdcall lstrcat,EDI,EBX");
    Write.Line(Write.ToFunction, "stdcall MessageBoxA,0,EDI,Error,0");
    if (AppType == Console || AppType == GUI){
      Write.Line(Write.ToFunction, "stdcall HeapAlloc,dword[HandleToHeap],8,EDI");
    } 
    if (AppType == DLL){
      Write.Line(Write.ToFunction, "CALL GetProcessHeap");
      Write.Line(Write.ToFunction, "stdcall HeapAlloc,EAX,8,EDI");
    }
    Write.Line(Write.ToFunction, "JMP Exit");
    Write.Line(Write.ToFunction, "");
    PostLabel(Write.ToFunction, "NoFunction");
    Write.Line(Write.ToFunction, "MOV dword[ExitStatus],1");
    Write.Line(Write.ToFunction, "POP EBX");
    Write.Line(Write.ToFunction, "stdcall lstrlen,EBX");
    Write.Line(Write.ToFunction, "ADD EAX,30");
    if (AppType == Console || AppType == GUI){
      Write.Line(Write.ToFunction, "stdcall HeapAlloc,dword[HandleToHeap],8,EAX");
    } 
    if (AppType == DLL){
      Write.Line(Write.ToFunction, "CALL GetProcessHeap");
      Write.Line(Write.ToFunction, "stdcall HeapAlloc,EAX,8,EAX");
    }
    Write.Line(Write.ToFunction, "MOV EDI,EAX");
    Write.Line(Write.ToFunction, "stdcall lstrcpy,EDI,NoFunctionFound");
    Write.Line(Write.ToFunction, "stdcall lstrcat,EDI,EBX");
    Write.Line(Write.ToFunction, "stdcall MessageBoxA,0,EDI,Error,0");
    if (AppType == Console || AppType == GUI){
      Write.Line(Write.ToFunction, "stdcall HeapAlloc,dword[HandleToHeap],8,EDI");
    } 
    if (AppType == DLL){
      Write.Line(Write.ToFunction, "CALL GetProcessHeap");
      Write.Line(Write.ToFunction, "stdcall HeapAlloc,EAX,8,EDI");
    }
    Write.Line(Write.ToFunction, "JMP Exit");
  #endif
  #ifdef Linux
    PostLabel(Write.ToFunction,  "NoMemory");
    Write.Line(Write.ToFunction, "MOV dword[ExitStatus],1");
    Write.Line(Write.ToFunction, "ccall puts,Error");
    Write.Line(Write.ToFunction, "ccall puts,NoMemMessage");
    Write.Line(Write.ToFunction, "JMP Exit");
  #endif
  return;
}

/******************************************************************************
PushNumber - Pushes a constant number onto the stack
******************************************************************************/
void Assembly::PushNumber(int Number){
  //Convert the number to a string and store in temporary variable
  Write.Line(Write.ToMain, "MOV dword[TempQWord1]," + ToStr(Number));
  //Clear the FPU
  Write.Line(Write.ToMain, "FINIT");
  //Load the integer
  Write.Line(Write.ToMain, "FILD dword[TempQWord1]");
  //Store the number in a doubles format
  Write.Line(Write.ToMain, "FSTP qword[TempQWord1]");
  //And finally, push both parts of the double
  Write.Line(Write.ToMain, "PUSH dword[TempQWord1+4]");
  Write.Line(Write.ToMain, "PUSH dword[TempQWord1]");
  return;
}

/******************************************************************************
PushAddress - Pushes the address of a variable 
******************************************************************************/
void Assembly::PushAddress(string Name, int Type){
  //Push the address of the number
  if (Type == Data.Number){
    //If we are inside a function, we need to get the address on the stack
    //We are effectively getting the address of a local variable
    if (Data.IsInsideSubFunction() == true){
      //Get the assembly language name of the number
      string AsmName = Data.Asm(Name);
      //Get the position of the minux
      int Pos = Data.Asm(Name).find("-", 0);
      if (Pos != string::npos){
        //Now get the number off the assembly language
        string Temp = AsmName.substr(Pos+1, Name.length());
        //Get the address of EBP
        Write.Line(Write.ToMain, "MOV EAX,EBP");
        //Subtract this from the EBP address
        Write.Line(Write.ToMain, "SUB EAX," + Temp);
        PUSH("EAX");
      }
      else{
        //Could be a parameter - do the same as above, but use plus sign
      int Pos = Data.Asm(Name).find("+", 0);
        if (Pos != string::npos){
          string Temp = AsmName.substr(Pos+1, Name.length());
          Write.Line(Write.ToMain, "MOV EAX,EBP");
          Write.Line(Write.ToMain, "ADD EAX," + Temp);
          PUSH("EAX");
        }
        else{
          //If all else fails, push the default zero
          PushNumber(0);
        }
      }
    }
    else{
      PUSH(Data.Asm(Name));
    }
  }
  //Push the contents of the variable, since it holds the address of the string
  if (Type == Data.String){
    PUSH("dword[" + Data.Asm(Name) + "]");
  }
  //Push the address of the UDT
  if (Type == Data.UDT){
    PUSH(Data.Asm(Name));
  }
  //Push the address of the function
  if (Type == Data.SubFunction){
    PUSH(Data.Asm(Name));  
  }
  //Pop the address
  POP("dword[TempQWord1]");
  //Clear the FPU
  Write.Line(Write.ToMain, "FINIT");
  //Load the integer
  Write.Line(Write.ToMain, "FILD dword[TempQWord1]");
  //Store it as a double
  Write.Line(Write.ToMain, "FST qword[TempQWord1]");
  //And push both parts of the double
  Write.Line(Write.ToMain, "PUSH dword[TempQWord1+4]");
  Write.Line(Write.ToMain, "PUSH dword[TempQWord1]");
  return;
}

#endif
