' KoolB DLL (requires new KoolB compiler)
$AppType DLL

' RapidQ calls the KoolB DLL, which in turn calls the WinAPI MessageBox function
Declare Function MessageBox Lib "User32.dll" Alias "MessageBoxA" (I As Integer, S As String, S2 As String, I2 As Integer) As Integer

' Preserve names as much as possible (ie, don't mangle)
$Mangle OFF

' Our sample function
Function Message (S As String) As Integer
  Result = MessageBox(0, S, "KoolB DLL showing you a little message:", 0)
End Function

$Mangle ON