' Test DLL 
' Example program for KoolB by Brian Becker

$AppType DLL
$Optimize Off

Function Concat(S1 As string, S2 As String) As String
  Result = S1 + S2
End Function

Function Sqrt(Number As Double) As Double
  Result = Number ^ (1/2)
End Function

