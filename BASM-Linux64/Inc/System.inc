' System.inc - System functions 
' Library for KoolB by Brian Becker

$IfNDef Systeminc
$Define Systeminc

Type SecurityAttributes
  Length As Integer
  SecurityDescriptor As Integer
  InheritHandle As Integer
End Type


Declare Function ChDir Lib "libc.so.6" Alias "chdir" (Path As String) As Integer
Declare Function Kill Lib "libc.so.6" Alias "rmdir" (FileName As String) As Integer
Declare Function CreateDirectory Lib "libc.so.6" Alias "mkdir" (PathName As String, mode As Integer) As Integer
Declare Function Randomize Lib "libc.so.6" Alias "srand" (Seed As Integer) As Integer
Declare Function Rand Lib "libc.so.6" Alias "rand" () As Integer
Declare Function Environ$ Lib "libc.so.6" Alias "getenv" (Environment As String) As String
Declare Function Rename Lib "libc.so.6" Alias "rename" (ExistingFileName As String, NewFileName As String) As Integer
Declare Function RmDir Lib "libc.so.6" Alias "rmdir" (PathName As String) As Integer
Declare Function Run Lib "libc.so.6" Alias "system" (Command As String) As Integer


Function MkDir (Directory As String) As Integer
  Dim SecAttributes As SecurityAttributes
  CreateDirectory(Directory, SecAttributes)
End Function

Function Rnd (Max As Integer) As Integer
  Result = Rand() / 32767 * Max
End Function

Function Sgn (Number As Integer) As Integer
  If Number = 0 Then
    Result = 0
  ElseIf Number > 0 Then
    Result = 1
  ElseIf Number < 0 Then
    Result = -1
  End If
End Function

Function ShowMessage(Message As String) As Integer
  Result = MessageBox(0, Message, "Message:", 0)
End Function

$End If
