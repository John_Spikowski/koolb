 Time.inc - Time functions
 Library for KoolB by Brian Becker

$IfNDef Timeinc
$Define Timeinc

$Include "Convert.inc"

Type SYSTEMTIME
  wYear As Integer
  wMonth As Integer
  wDayOfWeek As Integer
  wDay As Integer
  wHour As Integer
  wMinute As Integer
  wSecond As Integer
  wMilliseconds As Integer
End Type

Type TM
  Hour As Integer
  IsDST As Integer
  MDay As Integer
  Min As Integer
  Mon As Integer
  Sec As Integer
  WDay As Integer
  YDay As Integer
  Year As Integer
End Type



Declare Function LocalTime Lib "libc.so.6" Alias "localtime" Call C(Time As Integer) As Integer
Declare Function Time Lib "libc.so.6" Alias "time" (Time As Integer) As Integer
Declare Function AscTime Lib "libc.so.6" Alias "asctime" (Time As Integer) As Integer

Dim GlobalTM As TM, GlobalTime As Integer

Sub FillGobalTM(TMPointer As Integer, GTM As Integer)
  $Asm
    EXTERN memmove
    MOV EBX,dword[EBP+8]
    MOV ESI,dword[EBP+12]
    ccall memmove,dword[ESI],dword[EBX],36
  $End Asm
  GlobalTM.Hour = GlobalTM.Hour + 1
  GlobalTM.Mon  = GlobalTM.Mon  + 1
  GlobalTM.MDay = GlobalTM.MDay + 1
  GlobalTM.WDay = GlobalTM.WDay + 1
  GlobalTM.Year = GlobalTM.Year + 1900
End SUB

Function Date$ () As String
  Dim CurTime As Integer, Date AS TM
  Print Time(AddressOf(GlobalTime))
  Print AscTime(LocalTime(AddressOf(GlobalTime)))
  FillGobalTM(LocalTime(AddressOf(GlobalTime)), AddressOf(GlobalTM))
  Result = Str$(GlobalTM.Mon) + " " + Str$(GlobalTM.MDay) + " " + Str$(GlobalTM.Year)
End Function


$End If
