' Convert.inc - Convert functions 
' Library for KoolB by Brian Becker

Declare Function gcvt Lib "libc.so.6" Alias "_gcvt" (Number As Double, NumDigits As Integer, ByRef S As String) As Integer
Declare Function Val Lib "libc.so.6" Alias "atof" (S As String) As Double

Function Str$(Number As Double) As String
  Dim NumDigits As Integer
  NumDigits = 100
  Result = Space$(NumDigits + 1)
  gcvt(Number, NumDigits, Result)
End Function

