' Math.inc - Math functions 
' Library for KoolB by Brian Becker

$IfNDef Mathinc
$Define Mathinc

Declare Function Abs Lib "libc.so.6" Alias "fabs" (Number As Double) As Double
Declare Function Acos Lib "libc.so.6" Alias "acos" (Number As Double) As Double
Declare Function Asin Lib "libc.so.6" Alias "asin" (Number As Double) As Double
Declare Function Atan Lib "libc.so.6" Alias "atan" (Number As Double) As Double
Declare Function Atn Lib "libc.so.6" Alias "atan" (Number As Double) As Double
Declare Function Ceil Lib "libc.so.6" Alias "ceil" (Number As Double) As Double
Declare Function Cos Lib "libc.so.6" Alias "cos" (Number As Double) As Double
Declare Function Exp Lib "libc.so.6" Alias "exp" (Number As Double) As Double
Declare Function Floor Lib "libc.so.6" Alias "floor" (Number As Double) As Double
Declare Function Log Lib "libc.so.6" Alias "log" (Number As Double) As Double
Declare Function Sin Lib "libc.so.6" Alias "sin" (Number As Double) As Double
Declare Function Sqr Lib "libc.so.6" Alias "sqrt" (Number As Double) As Double
Declare Function Sqrt Lib "libc.so.6" Alias "sqrt" (Number As Double) As Double
Declare Function Tan Lib "libc.so.6" Alias "tan" (Number As Double) As Double
'~ Declare Function Timer Lib "libc.so.6" Alias "timer" () As Double
Declare Function Pow Lib "libc.so.6" Alias "pow" (Base As Double, Exponent As Double) As Double


Function Dec (Number As Double) As Double
  Result = Number - 1
End Function

Function Inc (Number As Double) As Double
  Result = Number + 1
End Function

Function Frac (Number As Double) As Double
  Result = Number - Floor(Number)
End Function

$End If
