' Type.inc - Type functions 
' Library for KoolB by Brian Becker

$IfNDef Typeinc
$Define Typeinc

Declare Function IsAlNum Lib "libc.so.6" Alias "isalnum" (Char As Integer) As Integer
Declare Function IsAlpha Lib "libc.so.6" Alias "isalpha" (Char As Integer) As Integer
Declare Function IsCntrl Lib "libc.so.6" Alias "iscntrl" (Char As Integer) As Integer
Declare Function IsDigit Lib "libc.so.6" Alias "isdigit" (Char As Integer) As Integer
Declare Function IsGraph Lib "libc.so.6" Alias "isgraph" (Char As Integer) As Integer
Declare Function IsLower Lib "libc.so.6" Alias "islower" (Char As Integer) As Integer
Declare Function IsPrint Lib "libc.so.6" Alias "isprint" (Char As Integer) As Integer
Declare Function IsPunct Lib "libc.so.6" Alias "ispunct" (Char As Integer) As Integer
Declare Function IsSpace Lib "libc.so.6" Alias "isspace" (Char As Integer) As Integer
Declare Function IsUpper Lib "libc.so.6" Alias "isupper" (Char As Integer) As Integer
Declare Function IsXDigit Lib "libc.so.6" Alias "isxdigit" (Char As Integer) As Integer

Declare Function ToLower Lib "libc.so.6" Alias "tolower" (Char As Integer) As Integer
Declare Function ToUpper Lib "libc.so.6" Alias "toupper" (Char As Integer) As Integer

$End If
