/******************************************************************************
 *|--------------------------------------------------------------------------|*
 *| BASM Linux Compiler - Fork of KoolB Version 15.02 by Brian C. Becker     |*
 *| 2014-12-01 - Armando I. Rivera (AIR) initial contributor                 |*
 *|                                                                          |*
 *| Compiler.h - the object that does the hard work and compiles the file    |*
 *|--------------------------------------------------------------------------|*
 *****************************************************************************/

#ifndef Compiler_h
#define Compiler_h

//Ahh - the real deal
class Compiler{
 private:
  Reading Read;                //The reading object we are to compile

 public:
  void Compile(Reading SourceFile, bool IsIncFile);
    bool Statement();
      void DIM();
        void GetArraySize();
        void DIMCreate(string Name);
        void DIMCreateArray(string Name);
      void NotDIM();
      void TYPE();
        void TYPECreate(string TypeName, string Name);
      void Assignment();
        int Expression(int Type, string ExtraInfo = "");
          void AddSubtract();
            void MultiplyDivideMod();
              void Exp();
                int Core(int Type, bool GetNextWord = true);
                  int LoadUDT(string UDT, int Type);
                  void LoadArray(string Array, int Type);
          void StringAdd();
          void AssignArray(string Name);
          void AssignUDT(string Name);
          string AssignUDTMember(string Name);
          void AssignArrayItem(string Name);
      bool DealWithControlStatements(string LoopCommand = "");
        void Condition();
          void Or();
            void And();
              void Not();
          int Relation(bool GetNextWord = true);
            bool IsRelation(string Word);
            void CompareNumbers(string Relation);
            void CompareStrings(string Relation);
        void If();
        void While(string StartWhileLabel);
      bool DealWithConsoleKeywords();
        void AssignInput();
    void SubFunction(bool External = false);
      void GetParameters(string Name);
      void PrepareParameters(string Name);
      void UnprepareParameters(string Name);
      void InsideSubFunction(string Name);
    void DeclareSubFunction();
      void SubFunctionLibAliasCall(string Name);
    void CallSubFunction(string Name, bool ReturnValue = false);
      void PassParameters(string Name);
    void ByRefExpression(int Type);
  bool FirstDirectives(string Directive);
    void ChooseAppType();
    void OptimizeApp();
  bool Directives(string Directive);
    void IncludeFile();
    void DropDownToAsm();
    void CompressApp();
    void IfDef();
    void IfNDef();
    void Const();
    void MangleNames();
  void PrepareProgram();
};

/******************************************************************************
Compile - takes a a reading object and begins the process of compilation. It
also can recursively compile multiple files via the $Include command, so we 
need to know if a file is the main program or just an included file
******************************************************************************/
void Compiler::Compile(Reading SourceFile, bool IsIncFile){
  //Copy the Reading object to our local Reading object
  Read = SourceFile;
  Read.GetNextWord();
  //Skip over new lines
  while (Read.WordType() == Read.EndOfLine){
    Read.GetNextWord();
  }
  //As long as this is the main program, let's intialize the program
  if (IsIncFile == false){
    PrepareProgram();
  }
  //The main compile loop that reads each line and passes control off 
  while (Read.WordType() != Read.None){
    if (Read.WordType() == Read.Identifier) {
        //If we have an Identifier, pass control off to Statement
        if (Statement() == false){
          //If Statement() is false, the user is trying to assign an undeclared
          //variable
          NotDIM();
          Assignment();
        }
      }
    else if (Read.WordType() == Read.Symbol) {
      //If we have a symbol as the first word of the line, its a directive
      if (Read.Word() == "$"){
        Read.GetNextWord();
        Directives(Read.Word());
      }
    }
    else if (Read.WordType() == Read.EndOfLine){
      //Don't do anything if end of line
    }
    else {
      //Opps, something went wrong!
      Error.BadStatement(Read);
    }
    //Make sure we have either a newline or the end of the file after each 
    //command - this is BASIC, after all
    if (Read.WordType() != Read.EndOfLine && Read.WordType() != Read.None){
      Error.EndOfLine(Read);
    }
    Read.GetNextWord();
  }
  //Make sure that we haven't missed an $End If or something along the way
  if (Data.CanExitDirective() == true){
    Error.NoEndToIfDef(Read);
  }
  return;
}

/******************************************************************************
Statement - If the first word of a line is an identifier, go through all the
commands. If none of the commands match, we may have an undeclared variable
******************************************************************************/
bool Compiler::Statement(){
  //Are we creating a variable?
  if (Read.Word() == "DIM"){
    DIM();
    return true;
  }
  //Are we defining a type?
  if (Read.Word() == "TYPE"){
    TYPE();
    return true;
  }
  //Are we defining a function?
  if (Read.Word() == "SUB" || Read.Word() == "FUNCTION"){
    SubFunction();
    return true;
  }
  //Are we declaring an external function?
  if (Read.Word() == "DECLARE"){
    DeclareSubFunction();
    return true;
  }
  //Are we trying to use built-in console commands?
  //If so, return, otherwise, continue
  if (DealWithConsoleKeywords() == true){
    return true;
  }
  //Are we trying to use control statements
  //If so, return, otherwise, continue
  if (DealWithControlStatements() == true){
    return true;
  }
  //If the word is already reserved, but not a keyword, we must be assigning 
  //something to a variable or calling a function
  if (Data.IsAlreadyReserved(Read.Word()) == true){
    if (Data.IsKeyword(Read.Word()) == true){
      return false;
    }
    if (Data.GetDataType(Read.Word()) == Data.SubFunction){
      CallSubFunction(Read.Word());
      Read.GetNextWord();
      return true;
    }
    Assignment();
    return true;
  }
  return false;
}

/******************************************************************************
NotDIM - if we are using an undeclared variable like so: PI# = 3.14, then
create the variable and continue with the assignment
Example: 
  PI# = 3.14
  ^^^---Creates an undeclared variable
******************************************************************************/
void Compiler::NotDIM(){
  string Name = Read.Word();
  char LastChar;
  //Make sure that the variable's name is valid
  if (Read.WordType() != Read.Identifier){
    Error.BadName(Read);
  }
  if (Data.AlreadyExistsInScope(Name)){
    Error.AlreadyReserved(Read);
    return;
  }
  //See what the last char is: Integer&, Double#, String$
  LastChar = Name[Name.length() - 1];
  //Make sure we have a valid last character
  if (LastChar == '&' || LastChar == '#' || LastChar == '$'){
    //If the last char is &, we create an integer
    if (LastChar == '&'){
      if (Mangle == true){
        Asm.CreateInteger(Name, Data.StripJunkOff(Name) + "_Integer");
      }
      else{
        Asm.CreateInteger(Name, Data.StripJunkOff(Name));      
      }
    }
    //If the last char is #, we create a double
    if (LastChar == '#'){
      if (Mangle == true){
        Asm.CreateDouble(Name, Data.StripJunkOff(Name) + "_Double");
      }
      else{
        Asm.CreateDouble(Name, Data.StripJunkOff(Name));
      }
    }
    //If the last char is $, we create a string
    if (LastChar == '$'){
      if (Mangle == true){
        Asm.CreateString(Name, Data.StripJunkOff(Name) + "_String");
      }
      else{
        Asm.CreateString(Name, Data.StripJunkOff(Name));      
      }
    }
  }
  else{
    //Oops, if the last char isn't &, #, or $, we don't know what type to create
    Error.NoType(Read);
  }
  return;
}

/******************************************************************************
DIM - explicitly creating a variable
Examples:
  'Create normal simple variable
  DIM A As Integer, B As Double, C As String
  
  'Create arrays
  DIM A(1) As Integer, B(5+1) As Double, C(Num*Size) As String
  
  'Create UDTs
  DIM A As MyUDT
******************************************************************************/
void Compiler::DIM(){
  string Name;
  //By defalut, we are not creating an array
  bool   IsArray = false;
  //Since we can create multiple variables with one DIM statement, loop around
  do{
    Read.GetNextWord();
    Name = Read.Word();
    //Make sure that we have a valid variable name
    if (Read.WordType() != Read.Identifier){
      if (Read.WordType() == Read.None ||
          Read.WordType() == Read.EndOfLine){
        Error.ExpectedNameAfterDIM(Read);
      }
      Error.BadName(Read);
    }
    if (Data.AlreadyExistsInScope(Name)){
      Error.AlreadyReserved(Read);
    }
    Read.GetNextWord();
    //If we have a parenthesis after the variable's name, its an array!
    if (Read.Word() == "("){
      IsArray = true;
      //Get the size of the array so we can allocate the memory correctly
      GetArraySize();
      Read.GetNextWord();
    }
    //Make sure we have the correct syntax
    if (Read.Word() != "AS"){
      Error.ExpectedAs(Read);
    }
    Read.GetNextWord();
    //Verify that we have a good variable type and then create the variable
    if (IsArray == true){
      DIMCreateArray(Name);
    }
    else{
      DIMCreate(Name);
    }
    Read.GetNextWord();
  }while(Read.Word() == ",");   //Variables to create are separated by commas
  return;
}

/******************************************************************************
DIMCreateArray - creates an array of integer, doubles, or strings
Examples:
  DIM A(100*Num) As Integer
                    ^^^^^^^---Verify that this is the correct type and then 
                              create the array
Note that you can't have an array of an array, nor can you have an array of UDTs                              
******************************************************************************/
void Compiler::DIMCreateArray(string Name){
  if (Read.Word() == "INTEGER"){
    if (Mangle == true){
      Asm.CreateIntegerArray(Name, Data.StripJunkOff(Name) + "_Integer");
    }
    else{
      Asm.CreateIntegerArray(Name, Data.StripJunkOff(Name));
    }
    return;
  }
  if (Read.Word() == "DOUBLE"){
    if (Mangle == true){
      Asm.CreateDoubleArray(Name, Data.StripJunkOff(Name) + "_Double");
    }
    else{
      Asm.CreateDoubleArray(Name, Data.StripJunkOff(Name));
    }
    return;
  }
  if (Read.Word() == "STRING"){
    if (Mangle == true){
      Asm.CreateStringArray(Name, Data.StripJunkOff(Name) + "_String");
    }
    else{
      Asm.CreateStringArray(Name, Data.StripJunkOff(Name) + "_String");
    }
    return;
  }
  //Oops, can't determine what type of variable to create
  Error.BadType(Read);
  return;
}

/******************************************************************************
DIMCreate - creates an integer, double, string, or UDT
Examples:
  DIM A As String
           ^^^^^^---Verify that this is the correct type and then create the 
                    variable
******************************************************************************/
void Compiler::DIMCreate(string Name){
  if (Read.Word() == "INTEGER"){
    if (Mangle == true){
      Asm.CreateInteger(Name, Data.StripJunkOff(Name) + "_Integer");
    }
    else{
      Asm.CreateInteger(Name, Data.StripJunkOff(Name));
    }
    return;
  }
  if (Read.Word() == "DOUBLE"){
    if (Mangle == true){
      Asm.CreateDouble(Name, Data.StripJunkOff(Name) + "_Double");
    }
    else{
      Asm.CreateDouble(Name, Data.StripJunkOff(Name));
    }
    return;
  }
  if (Read.Word() == "STRING"){
    if (Mangle == true){
      Asm.CreateString(Name, Data.StripJunkOff(Name) + "_String");
    }
    else{
      Asm.CreateString(Name, Data.StripJunkOff(Name));
    }
    return;
  }
  //Last chance is that we are creating a UDT
  if (Data.IsType(Read.Word())){
    if (Mangle == true){
      Asm.CreateUDT(Read.Word(), Name, Data.StripJunkOff(Name) + "_UDT");
    }
    else{
      Asm.CreateUDT(Read.Word(), Name, Data.StripJunkOff(Name));
    }
    return;
  }
  //Oops, we can't determine what type of variable to create
  Error.BadType(Read);
  return;
}

/******************************************************************************
GetArraySize - parses the expression inside parenthesis to see how large an 
array should be
Examples:
  DIM A(100*Num) As Integer
        ^^^^^^^---Parses the expression, the result is left on the stack
******************************************************************************/
void Compiler::GetArraySize(){
  Expression(Data.Number);
  //If we don't have
  if (Read.Word() != ")"){
    Error.ExpectedEndingParenthesis(Read);
  }
  return;
}

/******************************************************************************
TYPE - parses the definition of a type and adds it to the data part of the app
Example:
  Type MyType
    A As Integer
    B As Double
    C As String
  End Type
  
Note that you cannot have types within types, nor arrays within types  
******************************************************************************/
void Compiler::TYPE(){
  string TypeName;
  string Name;
  Read.GetNextWord();
  //Make sure that the type's name is valid
  if (Read.WordType() != Read.Identifier){
    if (Read.WordType() == Read.None ||
        Read.WordType() == Read.EndOfLine){
      Error.ExpectedNameAfterTYPE(Read);
    }
    Error.BadName(Read);
  }
  TypeName = Read.Word();
  if (Data.AlreadyExistsInScope(TypeName)){
    Error.AlreadyReserved(Read);
  }
  //Begin to creat the type
  Asm.StartCreatingType(TypeName);
  Read.GetNextWord();
  if (Read.WordType() == Read.None){
    Error.UnfinishedType(TypeName, Read);
  }
  if (Read.WordType() != Read.EndOfLine){
    Error.EndOfLine(Read);
  }
  //Start looping through the members of the type and adding them to the 
  //definition
  do{
    //Retrieve and validate the name of the next memboer
    Read.GetNextWord();
    Name = Read.Word();
    if (Read.WordType() != Read.Identifier){
      if (Read.WordType() == Read.None){
        Error.UnfinishedType(TypeName, Read);
      }
      if (Read.WordType() == Read.EndOfLine){
        continue;
      }
      Error.BadName(Read);
    }
    //If the name of the member of the type is already taken, bail out
    if (Data.IsAlreadyReservedInType(TypeName, Name)){
      Error.AlreadyReservedInType(Read);
    }
    Read.GetNextWord();
    //Check to see if we are through defining the type
    if (Name == "END" && Read.Word() == "TYPE"){
      break;
    }
    //Otherwise, enforce the rules of using <Name> As <Type>
    if (Read.Word() != "AS"){
      Error.ExpectedAs(Read);
    }
    Read.GetNextWord();
    //OK, pass control off to another routine to actually create the member
    TYPECreate(TypeName, Name);
    //If we are at the end of the file, error because we haven't finished 
    //defining the type
    if (Read.WordType() == Read.None){
      Error.UnfinishedType(TypeName, Read);
    }
    //If their is still other stuff on the line, bail out
    if (Read.WordType() != Read.EndOfLine){
      Error.EndOfLine(Read);
    }
  }while(true);          //Loop until we encounter an END TYPE or an error
  //Finish creating the type and return
  Asm.FinishCreatingType(TypeName);
  Read.GetNextWord();
  return;
}

/******************************************************************************
TYPECreate - verifies that we are creating a good variable and then adds it to
the type
  Type MyType
    A As Integer
         ^^^^^^^---Make sure we have a good data type
  End type
******************************************************************************/
void Compiler::TYPECreate(string TypeName, string Name){
  if (Read.Word() == "INTEGER" || Read.Word() == "DOUBLE"
                               || Read.Word() == "STRING"){
     if (Read.Word() == "INTEGER"){
       if (Mangle == true){
         Asm.CreateTypeInteger(TypeName, Name, Data.StripJunkOff(Name) + 
                               "_Integer");
       }
       else{
         Asm.CreateTypeInteger(TypeName, Name, Data.StripJunkOff(Name));       
       }
     }
     if (Read.Word() == "DOUBLE"){
       if (Mangle == true){
         Asm.CreateTypeDouble(TypeName, Name, Data.StripJunkOff(Name) + 
                              "_Double");
       }
       else{
         Asm.CreateTypeDouble(TypeName, Name, Data.StripJunkOff(Name));        
       }
     }
     if (Read.Word() == "STRING"){
       if (Mangle == true){
         Asm.CreateTypeString(TypeName, Name, Data.StripJunkOff(Name) + 
                              "_String");
       }
       else{
         Asm.CreateTypeString(TypeName, Name, Data.StripJunkOff(Name));
       }
     }
     Read.GetNextWord();
  }
  else{
    //Oops, we can't determine the type
    Error.BadType(Read);
  }
  return;
}

/******************************************************************************
Assignment - Takes an assignment to a variable and deals with it. Hold on for 
the ride, because this goes very in-depth with all the different types of 
expressions.

Examples:
PI# = 3.14
ABC = "Hello" + MyStr$ + "to you"
Google = 100^4 / 400 * (MyInt&-1 + (A - C^2)) + 1
******************************************************************************/
void Compiler::Assignment(){
  string Name = Read.Word();
  //Make sure we aren't trying to assign constant data!
  if (Read.IsConstData(Name) == true){
    Error.CannotModifyConstData(Read);
  }
  Read.GetNextWord();
  //If we have a period after the name, we must be assigning a member of a UDT
  if (Read.Word() == "."){
    //Get which member to assign
    string Member = AssignUDTMember(Name);
    Read.GetNextWord();
    //Make sure we have an =, otherwise, this is all useless
    if (Read.Word() != "="){
      Error.ExpectedAssignment(Read);
    }
    //OK, decide which type of expression this is: string or number
    if (Data.GetUDTData(Name).Type.Members[Member].Type == Data.String){
      Expression(Data.String);
    }
    else{
      Expression(Data.Number);
    } 
    //After the expression is parsed and the result is on the stack, assign it
    Asm.AssignUDTMember(Name, Data.GetUDTData(Name).TypeName, Member, 
                        Data.GetUDTData(Name).Type.Members[Member].Type);
    return;
  }
  //OK, if we have parenthesis, we must be assigning to an array item
  if (Read.Word() == "("){
    //Get the array item to assign
    AssignArrayItem(Name);
    Read.GetNextWord();
    //Make sure we have an equals sign
    if (Read.Word() != "="){
      Error.ExpectedAssignment(Read);
    }
    //Determine which type of expression we should parse: number or string
    if (Data.GetArrayData(Name).Type == Data.String){
      Expression(Data.String);
    }
    else{
      Expression(Data.Number);
    } 
    //Go ahead and assign it
    Asm.AssignArrayItem(Name, Data.GetArrayData(Name).Type);
    return;
  }
  //Now that we are done with the special cases (UDTs and arrays), check mundane
  //Make sure that we have an =
  if (Read.Word() != "="){
    Error.ExpectedAssignment(Read);
  }
  //If the variable we are assigning to is a number, intiate a number expression
  if (Data.GetDataType(Name) == Data.Number){
    Expression(Data.Number);
    Asm.AssignIt(Name);
    return;
  }
  //If the variable we are assigning to is a string, intiate a string expression
  if (Data.GetDataType(Name) == Data.String){
    Expression(Data.String);
    Asm.AssignIt(Name);
    return;
  }
  //We can also copy arrays
  if (Data.GetDataType(Name) == Data.Array){
    Expression(Data.Array, Name);
    Asm.AssignIt(Name);
    Read.GetNextWord();
    //If there is more on the line, we can't handle it!
    //Error: Array1 = Array2 + Array3 
    //                       ^^^^^^^^---No can do!
    if (Read.WordType() != Read.EndOfLine){
      Error.CanOnlyAssign(Read);
    }
    return;
  }
  //UDTs can be copied, as well
  if (Data.GetDataType(Name) == Data.UDT){
    Expression(Data.UDT, Name);
    Asm.AssignIt(Name);
    Read.GetNextWord();
    //Like arrays, we can only assign one UDT to another
    //Error: UDT1 = UDT2 + UDT3
    //                   ^^^^^^---No can do!
    if (Read.WordType() != Read.EndOfLine){
      Error.CanOnlyAssign(Read);
    }
    return;
  }
  return;
}

/******************************************************************************
Expression - just a junction for expression parsing: depending on the the type 
of expression, we will pass control to different functions
Example:
  A# = 3.14 * (4 + MyNum#^2)
       ^^^^^^^^^^^^^^^^^^^^^---Numeric expression
  B$ = "Hello " + Name
       ^^^^^^^^^^^^^^^---String expression
  Array1 = Array2
           ^^^^^^---Copy arrays
  UDT1 = UDT2
         ^^^^---Copy UDTs
  Print A# + 3 * MyNum#
        ^^^^^^^^^^^^^^^---Unknown expression
Note: numeric expressions are calculated as doubles, no matter what. No integer
arithmetic is performed, even on integers. Everything is calculated using 
floating-point processor and then rounded back to store in an integer, if 
necessary.        
******************************************************************************/
int Compiler::Expression(int Type, string ExtraInfo){
  //Parse numeric expression
  if (Type == Data.Number){
    //Get the first number
    Core(Data.Number);
    //Start at the lowest operator and move up to the highest
    AddSubtract();
    return Type;
  }
  //Parse string expression
  if (Type == Data.String){
    //Get the fist string
    Core(Data.String);
    //Start adding strings
    StringAdd();
    return Type;
  }
  //Copy arrays - use extra info for the name of the array
  if (Type == Data.Array){
    AssignArray(ExtraInfo);
    return Type;
  }
  //Copy UDTs - use extra info for the name of the array
  if (Type == Data.UDT){
    AssignUDT(ExtraInfo);
    return Type;
  }
  //If we don't know what type of expression, find out
  if (Type == Data.Unknown){
    //OK, get the first part of the expression and decide what it is
    Type = Core(Data.Unknown);
    //Then, either parse a numeric expression
    if (Type == Data.Number){
      AddSubtract();
      return Data.Number;
    }
    //Or string expression
    if (Type == Data.String){
      StringAdd();
      return Data.String;
    }
  }
  //Oops, can't parse expression 
  Error.UnknownExpressionType(Read);
  return -1;
}

/******************************************************************************
AddSubtract - Handles the lowest arithmetic operations: adding and subracting.
Example:
  A# = 2.14 + 1
  A# = 5.14 - 2
******************************************************************************/
void Compiler::AddSubtract(){
  //Store the operaand (+ or -), because we might have to perform more advanced
  //operations like multiplying before adding
  string Operand;
  Read.GetNextWord();
  //Before we add or subtract, make sure that we aren't multiplying or doing 
  //more important operations
  MultiplyDivideMod();
  //After doing all the operations that have a higher precedence, check for +/-
  while (Read.Word() == "+" || Read.Word() == "-"){
    Operand = Read.Word(); //Save the current word
    Core(Data.Number);     //Get the second number in the expression
    Read.GetNextWord();    //Get the next word (might be an operand like * or /)
    MultiplyDivideMod();   //Handle higher operations first like: 2 + 3 * 4
    //Now that we have the two numbers on the stack, add or subtract
    if (Operand == "+"){
      Asm.CalculateAddition();
    }
    if (Operand == "-"){
      Asm.CalculateSubtract();
    }
  }
  return;
}

/******************************************************************************
MultiplyDivideMod - handles the next level of arithmetic: multiplication,
dividing (floating point), and modulus (returns the remainder after integer 
division)
Examples
  A# = 2 * 2
  A# = 4 / 2
  A# = 6 MOD 4   ' is 2
******************************************************************************/
void Compiler::MultiplyDivideMod(){
  //Store operand, because we might have to do higher operations first
  string Operand;
  //Deal with exponents first (like: 2 * 2 ^ 3)
  Exp();
  //Once we have finished higher operations, check for *, /, and MOD
  while (Read.Word() == "*" || Read.Word() == "/" || Read.Word() == "MOD"){
    Operand = Read.Word();  //Save the current word
    Core(Data.Number);      //Retrieve the second number
    Read.GetNextWord();     //Retrieve the next operand
    Exp();                  //Check to see if next operand is ^, & deal with it
    //Now deal with multiplication, division, and remainders
    if (Operand == "*"){
      Asm.CalculateMultiply();
    }
    if (Operand == "/"){
      Asm.CalculateDivide();
    }
    if (Operand == "MOD"){
      Asm.CalculateMOD();
    }
  }
  return;
}

/******************************************************************************
Exp - deals with exponents - it is the hightest arithmetic operation
Examples:
  A# = 2 ^ 3
******************************************************************************/
void Compiler::Exp(){
  //While the operand indicates exponents, do it
  while (Read.Word() == "^"){
    //Load the second number
    Core(Data.Number);
    //Calculate it
    Asm.CalculateExp();
    //Get the next operator
    Read.GetNextWord();
  }
  return;
}

/******************************************************************************
Core - retrieves an individual component of an expression. It retrieves a
number, string, array item, UDT member, array, UDT, or unknown. It also handles
recursive expressions, or expressions containing parenthesis
Example:
  A# = 2 + 2
       ^---^---Gets numbers
  B$ = "ABC" + "DEF"
       ^^^^^---^^^^^---Gets strings
  A# = (3 + 4)/Number#
       ^^---^^-^^^^^^^---Gets numbers and parenthesis
******************************************************************************/
int Compiler::Core(int Type, bool GetNextWord){
  //By default, the number we are getting is not negative
  bool Negative = false;
  //A quick hack to allow boolean expressions
  if (GetNextWord == true){
    Read.GetNextWord();
  }
  //Check to see if we have a negative number
  if (Read.Word() == "-" && Read.WordType() != Read.String){
    Negative = true;
    Read.GetNextWord();
  }
  //If we have an parenthesis, just call expression to deal with it
  if (Read.Word() == "("){
    Type = Expression(Type);
    //Make sure we can handle things like: A# = 2 * -(1+1)
    if (Type == Data.Number && Negative == true){
      Asm.Negate();
    }
    //Make sure we can find the ending parenthesis
    if (Read.Word() != ")"){
      Error.UnmatchedParenthesis(Read);
    }
    //In case we didn't know what type of expression this was, we now know
    return Type;
  }
  //OK, in some situations, we don't know what type of expression we have
  //For example: Print "Hello" or Print 2 + 2 - we don't know if the expression
  //will be numeric or string. Once we have what type of expression we have, 
  //fall through to deal with it
  if (Type == Data.Unknown){
    //To determine, we take the first part of the expression and see if it is 
    //either a string or a number. That tells us what type the expression is
    if (Read.WordType() == Read.String){
      Type = Data.String;
    }
    if (Read.WordType() == Read.Number){
      Type = Data.Number;
    }
    //If we have an identifier (like Print A$), we get see what type of variable
    //we have
    if (Read.WordType() == Read.Identifier){
      if (Data.GetDataType(Read.Word()) == Data.String){
        Type = Data.String;
      }
      if (Data.GetDataType(Read.Word()) == Data.Number){
        Type = Data.Number;
        if (Negative == true){
          Asm.Negate();
        }
      }
      if (Data.GetDataType(Read.Word()) == Data.Array){
        Type = Data.GetArrayData(Read.Word()).Type;
        LoadArray(Read.Word(), Data.Unknown);
        if (Type == Data.Double || Type == Data.Integer){
          Type = Data.Number;
        }
        if (Negative == true){
          Asm.Negate();
        }
        return Type;
      }
      if (Data.GetDataType(Read.Word()) == Data.UDT){
        Type = LoadUDT(Read.Word(), Data.Unknown);
        if (Type == Data.Double || Type == Data.Integer){
          Type = Data.Number;
        }
        if (Negative == true){
          Asm.Negate();
        }
        return Type;
      }
      //If the expression is a function, lets call it and see what the return 
      //value is
      if (Data.GetDataType(Read.Word()) == Data.SubFunction){
        string ReturnValueType = 
                          Data.GetSubFunctionInfo(Read.Word()).ReturnValue.Type;
        if (ReturnValueType == "INTEGER" || ReturnValueType == "DOUBLE"){
          Type = Data.Number;
        }
        if (ReturnValueType == "STRING"){
          Type = Data.String;
        }
        CallSubFunction(Read.Word(), true);
        return Type;
      }
    }
    //It also could be a built in type of data
    if (Read.Word() == "CODEPTR" || Read.Word() == "CALLBACK" ||
        Read.Word() == "SIZEOF" || Read.Word() == "ADDRESSOF"){
      Type = Data.Number;   
    }
  }
  //If we have a string expression, let's do it!
  if (Type == Data.String){
    //If we have a constant string, just return it
    if (Read.WordType() == Read.String){
      Asm.LoadString(Read.Word());
      return Data.String;
    }
    //If we have an identifier, make sure it is a string and then return it
    if (Read.WordType() == Read.Identifier){
      if (Data.IsAlreadyReserved(Read.Word()) == false){
        Error.UndeclaredVariable(Read);
      }
      if (Data.GetDataType(Read.Word()) == Data.String){
        Asm.LoadString(Read.Word());
        return Data.String;
      }
      if (Data.GetDataType(Read.Word()) == Data.UDT){
        LoadUDT(Read.Word(), Data.String);
        return Data.String;
      }
      if (Data.GetDataType(Read.Word()) == Data.Array){
        LoadArray(Read.Word(), Data.String);
        return Data.String;
      }
    }
    //If we have a function, go ahead and call it
    if (Data.GetDataType(Read.Word()) == Data.SubFunction){
      string ReturnValueType = 
                          Data.GetSubFunctionInfo(Read.Word()).ReturnValue.Type;
      if (ReturnValueType != "STRING"){
        Error.ExpectedStringData(Read);
      }
      Type = Data.String;
      CallSubFunction(Read.Word(), true);
      return Type;
    }
    //Oops, we should have had a string expression
    Error.ExpectedStringData(Read);
  }
  //We could also have a number expression
  if (Type == Data.Number){
    //If we have a raw number, let's load it
    if (Read.WordType() == Read.Number){
      Asm.LoadNumber(Read.Word());
      if (Negative == true){
        Asm.Negate();
      }
      return Data.Number;
    }
    //If we have an identifier, load the variable
    if (Read.WordType() == Read.Identifier){
      if (Data.IsAlreadyReserved(Read.Word()) == false){
        Error.UndeclaredVariable(Read);
      }
      if (Data.GetDataType(Read.Word()) == Data.Number){
        Asm.LoadNumber(Read.Word());
        if (Negative == true){
          Asm.Negate();
        }
        return Data.Number;
      }
      if (Data.GetDataType(Read.Word()) == Data.UDT){
        LoadUDT(Read.Word(), Data.Number);
        if (Negative == true){
          Asm.Negate();
        }
        return Data.Number;
      }
      if (Data.GetDataType(Read.Word()) == Data.Array){
        LoadArray(Read.Word(), Data.Number);
        if (Negative == true){
          Asm.Negate();
        }
        return Data.Number;
      }
      //Deal with the SizeOf built-in function - gets the amount of memory 
      //occupied by a variable
      if (Read.Word() == "SIZEOF"){
        int DataType = 0;
        Read.GetNextWord();
        //SizeOf is a function, so make sure we have the parenthesis
        if (Read.Word() != "("){
          Error.ExpectedParameters(Read);
        }
        Read.GetNextWord();
        //Can't get the size of something that isn't a variable
        if (Data.IsAlreadyReserved(Read.Word()) != true || 
            Data.IsKeyword(Read.Word()) == true){
          Error.ExpectedVariable(Read);
        }
        DataType = Data.GetDataType(Read.Word());
        //Make sure we have a valid data type
        if (DataType == Data.Number || DataType == Data.String || 
            DataType == Data.UDT || DataType == Data.Type){
          if (DataType == Data.Number){
            if (Data.GetSimpleData(Read.Word()).Type == Data.Integer){
              //Integers are four bytes
              Asm.PushNumber(4);
            }
            else{
              //Doubles are eight bytes
              Asm.PushNumber(8);
            }
          }
          if (DataType == Data.String){
            //Strings are four bytes - to get the length, call Len
            Asm.PushNumber(4);
          }
          //UDTs are variable in size
          if (DataType == Data.UDT){
            int Size = Data.GetUDTData(Read.Word()).Type.Size;
            Asm.PushNumber(Size);
          }
          //TYPEs are also variable in size
          if (DataType == Data.Type){
            int Size = Data.GetTypeData(Read.Word()).Size;
            Asm.PushNumber(Size);
          }
        }
        else{
          //Oops - bad type
          Error.BadType(Read);
        }
        Read.GetNextWord();
        //Make sure we have the ending parenthesis
        if (Read.Word() != ")"){
          Error.ExpectedEndingParenthesis(Read);
        }
        return Data.Number;
      }
      //AddressOf - returnes the address where the variable is stored in memory
      //A built-in function
      if (Read.Word() == "ADDRESSOF"){
        int DataType = 0;
        Read.GetNextWord();
        //It is a function, so we need the parenthesis
        if (Read.Word() != "("){
          Error.ExpectedParameters(Read);
        }
        Read.GetNextWord();
        //Make sure we are getting the address of a variable
        if (Data.IsAlreadyReserved(Read.Word()) != true || 
            Data.IsKeyword(Read.Word()) == true){
          Error.ExpectedVariable(Read);
        }
        DataType = Data.GetDataType(Read.Word());
        //Make sure we have a good type
        if (DataType == Data.Number || DataType == Data.String || 
            DataType == Data.UDT){
          Asm.PushAddress(Read.Word(), DataType);
        }
        else if (DataType == Data.SubFunction){
          //Can't get the address of a non-function variable
          if (Data.GetDataType(Read.Word()) != Data.SubFunction){
            Error.ExpectedSubFunction(Read);
          }
          //If we are optimizing, mark this function as used
          if (Optimize == true){
            Data.UsingSubFunction(Read.Word());
          }
          Asm.Callback(Read.Word());
          }
          else{
          Error.BadType(Read);        
        }
        Read.GetNextWord();
        //We need the ending parenthesis
        if (Read.Word() != ")"){
          Error.ExpectedEndingParenthesis(Read);
        }
        return Data.Number;
      }
      //Built-in function to return the address of a function
      if (Read.Word() == "CALLBACK" || Read.Word() == "CODEPTR"){
        Read.GetNextWord();
        //CallBack/CodePtr is a function, so need parenthesis
        if (Read.Word() != "("){
          Error.ExpectedParameters(Read);
        }
        Read.GetNextWord();
        //Can't get the address of a non-function variable
        if (Data.GetDataType(Read.Word()) != Data.SubFunction){
          Error.ExpectedSubFunction(Read);
        }
        //If we are optimizing, mark this function as used
        if (Optimize == true){
          Data.UsingSubFunction(Read.Word());
        }
        Asm.Callback(Read.Word());
        Read.GetNextWord();
        //Make sure we have the ending parenthesis
        if (Read.Word() != ")"){
          Error.ExpectedEndingParenthesis(Read);
        }
        return Data.Number;
      }
    }
    //If we have a function, call the function for the return value
    if (Data.GetDataType(Read.Word()) == Data.SubFunction){
      //Enforce that we are getting a number
      string ReturnValueType = 
                          Data.GetSubFunctionInfo(Read.Word()).ReturnValue.Type;
      if (ReturnValueType != "INTEGER" && ReturnValueType != "DOUBLE"){
        Error.ExpectedNumberData(Read);
      }
      Type = Data.Number;
      CallSubFunction(Read.Word(), true);
      return Type;
    }
    //Oops, this isn't a numeric expression
    Error.ExpectedNumberData(Read);
  }
  //Major goof up by now - something went really wrong!
  Error.UnknownExpression(Read);
  return -1;
}

/******************************************************************************
StringAdd - adds strings together with either a + or &
Examples:
  A$ = "Hello" + " how are " & "you!"
               ^-------------^---Add strings 
******************************************************************************/
void Compiler::StringAdd(){
  Read.GetNextWord();
  while(Read.Word() == "+" || Read.Word() == "&" /*VB-Compatability*/){
    //Get the second part of the expression
    Core(Data.String);
    Asm.AddStrings();
    Read.GetNextWord();
  }
  return;
}

/******************************************************************************
AssignArray - copies one array to another
Example:
  Array1 = Array2
           ^^^^^^---Make sure we have another array and then copy
******************************************************************************/
void Compiler::AssignArray(string Name){
  Read.GetNextWord();
  //Can't assign a UDT to an array or anything!
  if (Data.GetDataType(Read.Word()) != Data.Array){
    Error.ExpectedArray(Read);
  }
  //Make sure the arrays are of the same type
  if (Data.GetArrayData(Read.Word()).Type != Data.GetArrayData(Name).Type){
    Error.TypesDoNotMatch(Read);
  }
  //Copy the array now
  Asm.CopyArray(Read.Word());
  return;
}

/******************************************************************************
AssignUDT - copies one UDT to another
Example:
  UDT1 = UDT2
         ^^^^---Make sure we have a UDT and then copy
******************************************************************************/
void Compiler::AssignUDT(string Name){
  Read.GetNextWord();
  //Make sure we are assigning a UDT to another UDT
  if (Data.IsType(Name) == true){
    if (Data.GetUDTData(Read.Word()).TypeName != Name){
      Error.TypesDoNotMatch(Read);
    }
    Asm.CopyUDT(Read.Word());
    Read.GetNextWord();
    return;
  }
  if (Data.GetDataType(Read.Word()) != Data.UDT){
    Error.ExpectedUDT(Read);
  }
  if (Data.GetUDTData(Read.Word()).TypeName != Data.GetUDTData(Name).TypeName){
    Error.TypesDoNotMatch(Read);
  }
  Asm.CopyUDT(Read.Word());
  return;
}

/******************************************************************************
LoadUDT - loads a UDT member and pushes onto the stack
Example:
  A# = MyUDT.Number
            ^^^^^^^---Load member of UDT
******************************************************************************/
int Compiler::LoadUDT(string UDT, int Type){
  string Member;
  Read.GetNextWord();
  //Make sure we have a period after the UDT and before the member
  if (Read.Word() != "."){
    Error.ExpectedPeriod(Read);
  }
  Read.GetNextWord();
  Member = Read.Word();
  //The member has to exist in the UDT
  if (Data.IsAlreadyReservedInType(Data.GetUDTData(UDT).TypeName, Member) != 
                                                                          true){
    Error.InvalidMember(Read);
  }
  //If we are trying to decide what type of expression we have, set the type to
  //the current type of the member
  if (Type == Data.Unknown){
    Type = Data.GetUDTData(UDT).Type.Members[Member].Type;
  }
  Asm.LoadUDTMember(UDT, Data.GetUDTData(UDT).TypeName, Member, Type);
  return Type;
}

/******************************************************************************
AssignUDTMember - assigns an expression to an UDT member
Example:
  MyUDT.Member = 2
       ^^^^^^^---Get the member we are assigning to
******************************************************************************/
string Compiler::AssignUDTMember(string Name){
  string Member;
  //Can't have a member that doesn't have a parent UDT
  if (Data.GetDataType(Name) != Data.UDT){
    Error.UnableToAccessMember(Read);
  }
  Read.GetNextWord();
  Member = Read.Word();
  //If the member isn't in the UDT, we can't continue
  if (Data.IsAlreadyReservedInType(Data.GetUDTData(Name).TypeName, Member) != 
                                                                          true){
    Error.InvalidMember(Read);
  }
  return Member;
}

/******************************************************************************
AssignArrayItem - Get the array item that we are assigning to and load to stack
Example:
  Array(1+2*Num) = 2
        ^^^^^^^^---Get the array item
******************************************************************************/
void Compiler::AssignArrayItem(string Name){
  //If the data type isn't an array, bail out
  if (Data.GetDataType(Name) != Data.Array){
    Error.UnableToAccessItem(Read);
  }
  //Call expression to get the array item
  Expression(Data.Number);
  //Gotta have the other parenthesis
  if (Read.Word() != ")"){
    Error.ExpectedEndingParenthesis(Read);
  }
  return;
}

/******************************************************************************
LoadArray - loads an array item to the stack for computation
Example:
  A# = Numbers(1*Num)
              ^^^^^^^---Loads the array item
******************************************************************************/
void Compiler::LoadArray(string Array, int Type){
  Read.GetNextWord();
  //We need the parenthesis to get the array item
  if (Read.Word() != "("){
    Error.ExpectedParenthesis(Read);
  }
  //Get the array item
  Expression(Data.Number);
  //Make sure we have the ending parenthesis
  if (Read.Word() != ")"){
    Error.ExpectedEndingParenthesis(Read);
  }
  //Load 'er up!
  Asm.LoadArrayItem(Array, Data.GetArrayData(Array).Type);
  return;
}

/******************************************************************************
DealWithConsoleKeywords - The built-in console keywords: Sleep, Cls, Print, and
Input. 
Sleep - pause program for a number of seconds
Cls   - clear the screen
Print - print stuff to the screen
Input - get stuff input from the screen
Note: Unlike most other builtin functions, there is no need for any parenthesis
******************************************************************************/
bool Compiler::DealWithConsoleKeywords(){
  //Sleep will get the number of seconds and then tell the assembly object to go
  //to it
  if (Read.Word() == "SLEEP"){
    Expression(Data.Number);
    Asm.ConsoleSleep();
    return true;
  }
  //A single line statement, needs no parameters
  if (Read.Word() == "CLS"){
    Asm.ConsoleCls();
    Read.GetNextWord();
    return true;
  }
  //Print is a little more complicated
  if (Read.Word() == "PRINT"){
    //First, we can print out multiple expressions separated by a ; or ,
    do{
      //The funny thing is we don't know what type of expression to expect
      //We could have PRINT 2 or PRINT "Hello"
      Asm.ConsolePrint(Expression(Data.Unknown));
    }while(Read.Word() == ";" || Read.Word() == ",");
    //Once we are done printing out all the expressions, start a new line
    Asm.ConsolePrintNewLine();
    return true;
  }
  //Input is also a bit more complex
  if (Read.Word() == "INPUT"){
    Read.GetNextWord();
    //If the word after INPUT is a string, we have a prompt, as in:
    //INPUT "Enter Number"; A#
    if (Read.WordType() == Read.String){
      Asm.LoadString(Read.Word());
      Asm.ConsolePrint(Data.String);
      Read.GetNextWord();
      //If we have a prompt for INPUT, we need to have either a ; or , to 
      //separate the prompt string from the variable to store the input in
      if (Read.Word() != ";" && Read.Word() != ","){
        Error.ExpectedSeparatorForInput(Read);
      }
      Read.GetNextWord();
    }
    //Actually get the INPUT
    Asm.ConsoleInput();
    //Now, we need to assign the input from the console
    AssignInput();
    return true;
  }
  return false;
}

/******************************************************************************
AssignInput - stores the information gotten from the console in a variable
Example:
  INPUT "Enter your number: "; A#
******************************************************************************/
void Compiler::AssignInput(){
  string Name = Read.Word();
  //There is a possibility that the variable doesn't exist, so create it
  if (Data.IsAlreadyReserved(Name) == false){
    NotDIM();
  }
  Read.GetNextWord();
  //There is also the possibility that we are assigning to a member of an UDT
  if (Read.Word() == "."){
    string Member = AssignUDTMember(Name);
    //Since the console input is automatically stored as a string, if the UDT
    //is a number, we need to convert the string to a number before assigning
    if (Data.GetUDTData(Name).Type.Members[Member].Type == Data.Double || 
        Data.GetUDTData(Name).Type.Members[Member].Type == Data.Integer){
      Asm.ConvertToNumber();   
    }
    //Assign the input
    Asm.AssignUDTMember(Name, Data.GetUDTData(Name).TypeName, Member, 
                        Data.GetUDTData(Name).Type.Members[Member].Type);
    Read.GetNextWord();
    return;
  }
  //The variable could also be an array
  if (Read.Word() == "("){
    //If so, get the array item
    AssignArrayItem(Name);
    //Now, things get a little tricky. The third item on the stack is the array
    //item. However, to use Asm.AssignArrayItem, the array item has to be
    //second on the stack. So we do a little re-ordering.
    Asm.POP("EBX");
    Asm.POP("EDI");
    Asm.POP("ESI");
    Asm.PUSH("EDI");
    Asm.PUSH("EBX");
    Asm.PUSH("ESI");
    //By default, the input is in string format. If we are assigning to a 
    //numeric array, we need to convert first
    if (Data.GetArrayData(Name).Type == Data.Integer ||
        Data.GetArrayData(Name).Type == Data.Double){
      Asm.ConvertToNumber();   
    }
    Asm.AssignArrayItem(Name, Data.GetArrayData(Name).Type);
    Read.GetNextWord();
    return;
  }
  //If we are just assigning to a simple variable, just check to see if we need
  //to convert to a number
  if (Data.GetDataType(Name) == Data.Number){
    Asm.ConvertToNumber();   
  }
  //Then assign it!
  Asm.AssignIt(Name);
  return;
}

/******************************************************************************
Condition - a very simple boolean expression parser. It doesn't handle 
parenthesis, but it does handle operation precedence. Or is the bottom, And is 
the middle, and Not is the higest. Also, relationals (=, <, >, <>, =>, and =< 
are the lowest.
Example:
  If 1 = 1 Or Not 3 <> 2 Then
     ^^^^^^^^^^^^^^^^^^^---Get the result of this boolean expression
******************************************************************************/
void Compiler::Condition(){
  //Let's start off by calling Or
  Or();
}

/******************************************************************************
Or - logical OR of two relational expressions. Returns 0 if both of the 
relationals are false, -1 otherwise
Example:
  If 1=1 OR 2=2 Then
     ^^^^^^^^^^---OR two relational statements together
******************************************************************************/
void Compiler::Or(){
  //Before we check Or, make sure we do the higher precedence operators first
  And();
  //While we have an or, or them together
  while (Read.Word() == "OR"){
    //Before we actually compute the Or, check for more Ands
    And();
    Asm.Or();
  }
  return;
}

/******************************************************************************
And - logical AND of two relational expressions. Returns 0 if either of the 
relational expressions is false, -1 otherwise
Example:
  If 1=1 AND 2<>2 Then
     ^^^^^^^^^^^^---And two relational statements together
******************************************************************************/
void Compiler::And(){
  //Before we handle the ANDs, make sure we don't have any higher precedence
  //operators out there
  Not();
  //Then loop while we have AND expressions
  while (Read.Word() == "AND"){
    //See if the next operation is a NOT - if so, take care of it
    Not();
    Asm.And();
  }
  return;
}

/******************************************************************************
Not - performs a logical NOT on one relational expression. Returns 0 if the
relation is true, -1 otherwise
Example:
  If NOT 1=1 Then
     ^^^---Gets the NOT before a relational expression to negate the expression
******************************************************************************/
void Compiler::Not(){
  //See if the next word is NOT
  Read.GetNextWord();
  if (Read.Word() == "NOT"){
    //If so, call relation and then NOT (negate) the results of the expression
    Relation();
    Asm.Not();
  }
  else{
    //If we didn't find a NOT, that means we have the first part of an 
    //expresion. Since Core (the routine that primes the pump for expressions)
    //expects to get the first part of the expression, we need to tel it not to.
    //To do this, we pass false to Relation, which passes false to Core. That 
    //way, Core knows that it already has the first part of the expression.
    //Note: this is really just a quick hack, I should really come up with a
    //more elegant solution.
    Relation(false);
  }
  return;
}

/******************************************************************************
Relation - the core routine to deal with relational expressions. Return 0 if the
expression is false, and -1 if the expression is true
Example:
  If 1=1 Then
     ^^^---Deal with an individual relational expression
******************************************************************************/
int Compiler::Relation(bool GetNextWord){
  string Relation;
  //Cal core to calculate the first part of the relational expression (the part
  //to the left of the relational operator). We don't know if this is a numeric
  //or string expression, so we play it safe and let Core figure it out for us.
  int Type = Core(Data.Unknown, GetNextWord);
  //After Core primes the pump and lets us know what type of expression we are
  //dealing with, we have two different choices: compare numeric or string 
  //expressions
  if (Type == Data.Number){
    //If we have a numeric expression, let's get the rest of the expression
    AddSubtract();
    //If we have a relational operator, go ahead and get the other expression
    if (IsRelation(Read.Word())){
      //Store which relational operator we have for future reference
      Relation = Read.Word();
      //Get the other side of the relational expression
      Core(Data.Number);
      AddSubtract();
      //Now compare them to see if the relational expression is true or false
      CompareNumbers(Relation);
    }
    return Data.Number;
  }
  if (Type == Data.String){
    //If we have a string expression, let's get the rest of the expression
    StringAdd();
    //OK, if we have a relational operator, let's do comparison
    if (IsRelation(Read.Word())){
      //Store the relational operator for future reference
      Relation = Read.Word();
      //Get the right side of the relational expression
      Core(Data.String);
      StringAdd();
      //Compare them to see if the relational expression is true or false
      CompareStrings(Relation);
      return Data.Number;
    }
    //NewFeature - If we don't have another string to compare, conver to a 
    //number for conditional comparison
    Asm.ConvertToNumber();
    return Data.String;
  }
  return (int)NULL;
}

/******************************************************************************
IsRelation - checks to see if the current word is a relational operator
******************************************************************************/
bool Compiler::IsRelation(string Word){
  if (Word == "=" || Word == "<>" || Word == "<" || Word == ">" || 
      Word == ">=" || Word == "<="){
    return true;
  }
  return false;
}

/******************************************************************************
CompareNumbers - compares the results of two numeric expressions
******************************************************************************/
void Compiler::CompareNumbers(string Relation){
  //Performs the actual computation
  Asm.CompareNumbers();
  //Now depending on the relational operator and the results, load the right
  //result
  if (Relation == "="){
    Asm.LoadNumberRelation(Asm.Equal);
  }
  if (Relation == "<>"){
    Asm.LoadNumberRelation(Asm.NotEqual);
  }
  if (Relation == ">"){
    Asm.LoadNumberRelation(Asm.Greater);
  }
  if (Relation == "<"){
    Asm.LoadNumberRelation(Asm.Less);
  }
  if (Relation == ">="){
    Asm.LoadNumberRelation(Asm.GreaterOrEqual);
  }
  if (Relation == "<="){
    Asm.LoadNumberRelation(Asm.LessOrEqual);
  }
  return;
}

/******************************************************************************
CompareStrings - compares two strings on the stack 
******************************************************************************/
void Compiler::CompareStrings(string Relation){
  //Perform the actual comparison
  Asm.CompareStrings();
  //Depending on the relational operator and the results, load true or false
  if (Relation == "="){
    Asm.LoadStringRelation(Asm.Equal);
  }
  if (Relation == "<>"){
    Asm.LoadStringRelation(Asm.NotEqual);
  }
  if (Relation == ">"){
    Asm.LoadStringRelation(Asm.Greater);
  }
  if (Relation == "<"){
    Asm.LoadStringRelation(Asm.Less);
  }
  if (Relation == ">="){
    Asm.LoadStringRelation(Asm.GreaterOrEqual);
  }
  if (Relation == "<="){
    Asm.LoadStringRelation(Asm.LessOrEqual);
  }
  return;
}

/******************************************************************************
DealWithControlStatements - handles If and While statements
Examples:
  If 1=1 Then
  ^^^^^^^^^^^---Makes sure the beginning of an if statement is valid
  
  While 1=1
  ^^^^^^^^^---Makes sure that the beginning of a while loop is valid
******************************************************************************/
bool Compiler::DealWithControlStatements(string LoopCommand){
  //If we have an IF statement, let's roll
  if (Read.Word() == "IF"){
    //First, get the condition
    Condition();
    //Then make sure we have a THEN keyword
    if (Read.Word() != "THEN"){
      Error.ExpectedThen(Read);
    }
    Read.GetNextWord();
    //Next, we expect to see an end of line
    if (Read.WordType() != Read.EndOfLine){
      Error.EndOfLine(Read);
    }
    Read.GetNextWord();
    //Go inside the IF statement
    If();
    //Return true, meaning we dealt with a control statement
    return true;
  }
  //If we have a WHILE statement, let's roll with it
  if (Read.Word() == "WHILE"){
    //Set the stage for the while and get the fixed place to loop back to
    string StartWhile = Asm.PrepareWhile();
    //Then get the conditio (the conditon will be checked every time we loop)
    Condition();
    //Make sure we have a new line
    if (Read.WordType() != Read.EndOfLine){
      Error.EndOfLine(Read);
    }
    Read.GetNextWord();
    //Go to the inside of the start statement
    While(StartWhile);
    //Yep, we handled a control statement
    return true;
  }
  //Nope, didn't find any control statements
  return false;
}

/******************************************************************************
If - handles the inside of an if statement

  <CodeBlock>
  End If
  ^^^^^^^^^^^---Handles the second and third parts of a while statement
******************************************************************************/
void Compiler::If(){
  //First, let's get the two fixed places to jump to if things are true or false
  string NextEnd = Asm.StartIf();
  string DoneIf      = Asm.GetLabel();
  //Infinate loop begins to get the inside of an If statement. The only way to
  //break out is with an End If or an error
  for(;;){
    //No, you can't have a funcion in an if statement
    if (Read.Word() == "SUB" || Read.Word() == "FUNCTION"){
      Error.SubFunctionMustBeGlobal(Read);
    }
    //If we can't find any of the normal commands, let's do some customizing
    if (Statement() == false){
      //End is always a good place to end...something
      if (Read.Word() == "END"){
        Read.GetNextWord();
        //Can't have the end of a function in an if statement either!
        if (Read.Word() == "SUB" || Read.Word() == "FUNCTION"){
          Error.MustEndIfStatementFirst(Read);
        }
        //Ah, here we go - if we encounter an END IF, the If statement is over
        if (Read.Word() == "IF"){
          Read.GetNextWord();
          break;
        }
        //No matter what type of end we have, nothing else must be on the line
        if (Read.WordType() != Read.EndOfLine && Read.WordType() != Read.None){
          Error.EndOfLine(Read);
        }
        //If we have an End all by itself, well its time to exit the program
        Asm.EndProgram();
        Read.GetNextWord();
        continue;
      }
      //If we have an elseif, we can handle that
      if (Read.Word() == "ELSEIF"){
        //Start a new section of the IF code
        Asm.StartNewIfSection(NextEnd, DoneIf);
        //Get the condition
        Condition();
        //Make sure we have THEN followec by a new line
        if (Read.Word() != "THEN"){
          Error.ExpectedThen(Read);
        }
        Read.GetNextWord();
        if (Read.WordType() != Read.EndOfLine){
          Error.EndOfLine(Read);
        }
        //Now, let's continue with the new part of the IF statement
        NextEnd = Asm.ElseIf(NextEnd, DoneIf);
        Read.GetNextWord();
        continue;
      }
      //We can handle ELSE too!
      if (Read.Word() == "ELSE"){
        //Make sure we have a new line
        Read.GetNextWord();
        if (Read.WordType() != Read.EndOfLine){
          Error.EndOfLine(Read);
        }
        //Set us up for the else part of the IF statement and then continue
        NextEnd = Asm.Else(NextEnd, DoneIf);
        Read.GetNextWord();
        continue;
      }
      //Well, if everything else fails, we must have an undeclared variable
      if (Read.WordType() != Read.EndOfLine && Read.WordType() != Read.None){
        NotDIM();
        Assignment();
        continue;
      }
    }
    //Make sure we have an end of line
    if (Read.WordType() != Read.EndOfLine){
      //If we encounter the end of the file before an End If, print out error
      if (Read.WordType() == Read.None){
        Error.NoEndIf(Read);
      }
      Error.EndOfLine(Read);
    }
    Read.GetNextWord();
  }
  //Finish off the If statement
  Asm.EndIf(NextEnd, DoneIf);
  return;
}

/******************************************************************************
While - handles a while statement
Example:
  <Block>
  WEnd
  ^^^^^^^---Makes sure we have a good end to the while statement
******************************************************************************/
void Compiler::While(string StartWhile){
  //Start the while statement
  string EndWhile = Asm.StartWhile();
  //Begin an infinite loop that breaks only if it encounters a WEnd or an error
  for (;;){
    //Sorry, but no functions in a while statement
    if (Read.Word() == "SUB" || Read.Word() == "FUNCTION"){
      Error.SubFunctionMustBeGlobal(Read);
    }
    //See if we have any commands
    if (DealWithControlStatements() == true || Statement() == true 
                                            || Read.WordType() == Read.EndOfLine
                                            || Read.WordType() == Read.None){
      //If we have a command, make sure we have a new line afterwards
      if (Read.WordType() != Read.EndOfLine){
        //Can't have the end of the file before the WEnd
        if (Read.WordType() == Read.None){
          Error.NoEndIf(Read);
        }
        Error.EndOfLine(Read);
      }
      Read.GetNextWord();
      continue;
    }
    //If we don't have a command, check for other things
    //If we have a WEND, that's it, we're out of here
    if (Read.Word() == "WEND"){
      Read.GetNextWord();
      break;
    }
    //If we find an END, its either in the wrong place or it it is trying to 
    //end a function, which is illegal
    if (Read.Word() == "END"){
      Read.GetNextWord();
      if (Read.Word() == "SUB" || Read.Word() == "FUNCTION"){
        Error.MustEndWhileLoopFirst(Read);
      }
      Error.EndInWrongPlace(Read);
    }
    //If all else fails, we are using an undeclared variable
    NotDIM();
    Assignment();
  }
  //Stop the while and we're finished
  Asm.StopWhile(StartWhile, EndWhile);
  return;
}

/******************************************************************************
SubFunction - handles the declaration of subroutines and functions, both 
user defined and external
Examples:
  Sub A () As Integer : End SUB
  Function Aaa (A As Integer, B As Double, C As String) As String : End Sub
  Declare Function Sleep Lib "Kernel32.dll" Alias "Sleep" (A As Integer) As _
    Integer
******************************************************************************/
void Compiler::SubFunction(bool External){
  string Type;
  string Name;
  string ReturnValueType;
  //Get the type of function - subroutine or function
  Type = Read.Word();
  Read.GetNextWord();
  //Get the name of the function
  Name = Read.Word();
  //Make sure the name is valid
  if (Read.WordType() != Read.Identifier){
    Error.BadName(Read);
  }
  if (Data.AlreadyExistsInScope(Name) == true){
    Error.AlreadyReserved(Read);
  }
  //Inform the writing and database object that we are entering a function
  Write.EnterSubFunction();
  Data.EnterNewScope();
  Read.GetNextWord();
  //Add a function to the database and create it
  Data.AddSubFunction(Type, Name, External);
  Asm.CreateSubFunction(Name, External);
  //If this is an external function, get the library, its alias, and which
  //calling convention to use
  if (External == true){
    SubFunctionLibAliasCall(Name);
  }
  //We need a parenthesis to get the parameters
  if (Read.Word() != "("){
    Error.ExpectedParameters(Read);
  }
  //Loop through the parameters
  do{
    Read.GetNextWord();
    //If we've come to the end, break out of the loop
    if (Read.Word() == ")"){
      break;
    }
    //Get the parameter
    GetParameters(Name);
    Read.GetNextWord();
  }while(Read.Word() == ",");    //Keep looping while we have a comma
  //Once we have the parameters, we need the ending parenthesis
  if (Read.Word() != ")"){
    Error.NoEndToParameters(Read);
  }
  Read.GetNextWord();
  //In addition, if this is a function, we need the return value
  if (Type == "FUNCTION"){
    //Expected AS after parameters to tell us what type of function this is
    if (Read.Word() != "AS"){
      Error.ExpectedAs(Read);
    }
    Read.GetNextWord();
    //Make sure the return type is a valid one
    if (Read.Word() != "INTEGER" && Read.Word() != "DOUBLE" && 
        Read.Word() != "STRING" && Data.IsType(Read.Word())){
      Error.BadType(Read);
    }
    //Store the type of the return value
    ReturnValueType = Read.Word();
    //Add that to the database
    Data.AddReturnValue(Name, Read.Word());
    //If this isn't an external function, create a RESULT variable to store
    //the return value in
    if (External == false){
      if (Read.Word() == "INTEGER" || Read.Word() == "DOUBLE" || 
          Read.Word() == "STRING"){
        if (Read.Word() == "INTEGER"){
          Asm.CreateInteger("RESULT", "EBP-4");
        }
        if (Read.Word() == "DOUBLE"){
          Asm.CreateDouble("RESULT", "EBP-8");
        }
        if (Read.Word() == "STRING"){
          Asm.CreateString("RESULT", "EBP-4");
        }
      }
      if (Data.IsType(Read.Word()) == true){
        Data.AddUDTData(Type, "RESULT", Data.GetScopeID() + 
                        Data.StripJunkOff("RESULT"));
      }
    }
    Read.GetNextWord();
  }
  //Make sure each parameter is prepared
  PrepareParameters(Name);
  //Make sure there isn't any other stuff on the line
  if (Read.WordType() != Read.EndOfLine){
    if (External == true && Read.WordType() == Read.None){
    }
    else{
      Error.EndOfLine(Read);
    }
  }
  //As long as this isn't an external function, we need to get the inner
  //block of code for the user defined function
  if (External == false){
    InsideSubFunction(Name);
  }
  //Tell the database and writing objects that we're done with the function
  Data.ExitScope();
  Write.ExitSubFunction();
  //If this was a function (and not an external one), return the results
  if (Type == "FUNCTION" && External == false){
    Asm.ReturnValue(Name, ReturnValueType);
  }
  //Finish creating the sub function!
  Asm.EndCreateSubFunction(Name, External);
  return;
}

/******************************************************************************
GetParameters - gets each, individual parameter from a function to store in
the database
Example:
  Function A (A As Integer, B As Double, C as String) As Integer
              ^^^^^^^^^^^^---Get and store a parameter of a function
******************************************************************************/
void Compiler::GetParameters(string Name){
  string Type;
  string ParameterName;
  string How;
  //Should we pass the parameter by value or by reference?
  if (Read.Word() == "BYVAL" || Read.Word() == "BYREF"){
    How = Read.Word();
    Read.GetNextWord();
  }
  //By defalut, we pass the parameter by value
  else{
    How = "BYVAL";
  }
  //Get the name of the parameter and make sure that it is valid
  ParameterName = Read.Word();
  if (Read.WordType() != Read.Identifier){
    Error.BadName(Read);
  }
  if (Data.IsAlreadyReservedInParameters(Name, ParameterName) == true){
    Error.AlreadyReservedInParameters(Read);
  }
  //Make sure that we have AS after the name of the paramater
  Read.GetNextWord();
  if (Read.Word() != "AS"){
    Error.ExpectedAs(Read);
  }
  //Get the type of the parameter and store it in the database along with the
  //rest of the information relating to the parameter
  Read.GetNextWord();
  Type = Read.Word();
  if (Type != "INTEGER" && Type != "DOUBLE" && 
      Type != "STRING" && Data.IsType(Type) == false){
    Error.BadType(Read);
  }
  if (Type == "INTEGER"){
    SimpleDataInfo Info;
    if (Mangle == true){
      Info.AsmName = Data.GetScopeID() + Data.StripJunkOff(ParameterName);
    }
    else{
      Info.AsmName = Data.StripJunkOff(ParameterName);
    }
    Info.Type = Data.Integer;
    Data.AddSimpleData(ParameterName, Info);
  }
  if (Type == "DOUBLE"){
    SimpleDataInfo Info;
    if (Mangle == true){
      Info.AsmName = Data.GetScopeID() + Data.StripJunkOff(ParameterName);
    }
    else{
      Info.AsmName = Data.StripJunkOff(ParameterName);
    }
    Info.Type = Data.Double;
    Data.AddSimpleData(ParameterName, Info);
  }
  if (Type == "STRING"){
    SimpleDataInfo Info;
    if (Mangle == true){
      Info.AsmName = Data.GetScopeID() + Data.StripJunkOff(ParameterName);
    }
    else{
      Info.AsmName = Data.StripJunkOff(ParameterName);
    }
    Info.Type = Data.String;
    Data.AddSimpleData(ParameterName, Info);
  }
  if (Data.IsType(Type) == true){
    if (Mangle == true){
      Data.AddUDTData(Type, ParameterName, Data.GetScopeID() + Data.StripJunkOff(ParameterName));
    }
    else{
      Data.AddUDTData(Type, ParameterName, Data.StripJunkOff(ParameterName));
    }
  }
  Data.AddParameter(Name, ParameterName, Type, How);
  return;
}

/******************************************************************************
InsideSubFunction - parses the block of code inside a user-defined function
******************************************************************************/
void Compiler::InsideSubFunction(string Name){
  Read.GetNextWord();
  //Start an infinite loop that will only break when we encounter End Function,
  //End Sub, or an error
  for (;;){
    //No, you idiot! You can't have a function or subroutine inside a funciton!
    if (Read.Word() == "SUB" || Read.Word() == "FUNCTION"){
      Error.SubFunctionMustBeGlobal(Read);
    }
    //Call statement to deal with normal commands, and if it doesn't find 
    //anything, let's take over
    if (Statement() == false){
      //If we have a $, we know that we are dealing with a directive, so deal 
      //with it
      if (Read.Word() == "$"){
        Read.GetNextWord();
        Directives(Read.Word());
      }
      //If we have an END, see what we are ending
      if (Read.Word() == "END"){
        Read.GetNextWord();
        //If we are ending either a sub or function (must match), let's get out
        //of here. Note that we compare the type of function to the current
        //word, so that we don't have something like this: 
        //SUB A() : PRINT "Hello" : END FUNCTION  ' Oops! Mis-matched
        if (Data.GetSubFunctionInfo(Name).Type == Read.Word()){
          Read.GetNextWord();
          break;
        }
        //If we don't match the type of function, but we do have SUB or 
        //FUNCTION, then we have mismatched everything
        if (Read.Word() == "SUB" || Read.Word() == "FUNCTION"){
          Error.MismatchedSubFunction(Read);
        }
        //Make sure we have a new line
        if (Read.WordType() != Read.EndOfLine){
          Error.EndOfLine(Read);
        }
        //If we just have END all by itself on a line, terminate the program
        Asm.EndProgram();
        Read.GetNextWord();
        continue;
      }
      //If we have result, break
      if (Read.Word() == "RESULT"){
        Read.GetNextWord();
        break;
      }
      //Make sure we have a new line afterwards
      if (Read.WordType() == Read.None){
        Error.NoEndSubFunction(Read);
      }
      if (Read.WordType() == Read.EndOfLine){
        Read.GetNextWord();
        continue;
      }
      //If all else fails, we are using an undeclared variable
      NotDIM();
      Assignment();
    }
    //Make sure we have a new line
    if (Read.WordType() != Read.EndOfLine){
      //If we encounter the end of the file before 
      if (Read.WordType() == Read.None){
        Error.NoEndIf(Read);
      }
      Error.EndOfLine(Read);
    }
    Read.GetNextWord();
  }
  return;
}

/******************************************************************************
CallSubFunction - makes sure we have a function to call and then calls it
Example:
  A# = MyFunc(Integer1, Double2, String3)
              ^^^^^^^^^^^^^^^^^^^^^^^^^^^---Call function 
******************************************************************************/
void Compiler::CallSubFunction(string Name, bool ReturnValue){
  //If we are optimizing the app, make sure we include this function because it
  //has been used
  if (Optimize == true){
    Data.UsingSubFunction(Name);
  }
  Read.GetNextWord();
  //Get the beginning parenthesis
  if (Read.Word() != "("){
    Error.ExpectedParameters(Read);
  }
  //If this is a subroutine, we don't have to worry about return values
  if (Data.GetSubFunctionInfo(Name).Type == "SUB"){
    //If we don't have any parameters, just call the sub cold turkey 
    if (Data.GetSubFunctionInfo(Name).ParamCount == 0){
      Asm.InvokeSubFunction(Name);
      Read.GetNextWord();
    }
    //Otherwise, pass the parameters and then call the sub
    else{
      PassParameters(Name);
      Asm.InvokeSubFunction(Name);
      UnprepareParameters(Name);
    }
  }
  //If this is a function, we have to worry about a return value
  if (Data.GetSubFunctionInfo(Name).Type == "FUNCTION"){
    //If we don't have any parameters, invoke the function cold-turkey
    if (Data.GetSubFunctionInfo(Name).ParamCount == 0){
      Asm.InvokeSubFunction(Name);
      Read.GetNextWord();
    }
    //Otherwise, pass the parameters first and then invoke the function
    else{
      PassParameters(Name);
      Asm.InvokeSubFunction(Name);
      UnprepareParameters(Name);    
    }
    //Now we have the return value. If we are just calling the function without
    //really needing the return value (i.e. the function isn't in an 
    //expression), we can just clean up and ignore it. 
    if (ReturnValue == false){
      Asm.CleanUpReturnValue(Name, Data.GetSubFunctionInfo(Name).ReturnValue.Type);
    }
    //Otherise, we need to add it to the stack so we can manipulate it
    else{
      Asm.PushReturnValue(Name, Data.GetSubFunctionInfo(Name).ReturnValue.Type);
    }
  }
  //Make sure we have the ending parenthesis
  if (Read.Word() != ")"){
    Error.ExpectedParameters(Read);
  }
  return;
}

/******************************************************************************
PassParameters - passes the parameters to the function. There are several 
tricky things that make this not very straightforward. First, functions expect
that the parameters be in order from first to last. Unfortunately, with a stack,
they wind up in reverse order (because the first one you push onto the stack 
is the last on to be popped off, not the first). So we have to reverse the 
parameters. Second, we have to deal with passing parameters by reference.
To get around the first problem, instead of pushing the result to each 
expression to the stack, we store it in a 'Parameter Memory Pool'. Once we have
stored all the parameters there, it is simple enough go backwards and push 
them onto the stack. The second problem is resolved by first by parsing the
expression normally, and then getting the address of the result.
******************************************************************************/
void Compiler::PassParameters(string Name){
  //Get how many parameters we have
  int ParamCount = Data.GetSubFunctionInfo(Name).ParamCount;
  //Create the pool of memory to temporarily store the parameters
  Asm.AllocateParameterPool(Data.GetSubFunctionInfo(Name).SizeOfParameters);
  //We haven't filled the pool any yet, so we are starting from 0
  Data.SetParameterPoolSizeFilled(Name, 0);
  //Now, store every parameter in the pool
  for (int i = 1; i <= ParamCount; i++){
    //Get the info first for the parameter
    string How       = Data.GetSubFunctionInfo(Name).Parameters[i].How;
    string Type      = Data.GetSubFunctionInfo(Name).Parameters[i].Type;
    string ParamName = Data.GetSubFunctionInfo(Name).Parameters[i].Name;
    string AsmName   = Data.GetSubFunctionInfo(Name).Parameters[i].AsmName;
    int    Size;
    //Deal with integers first
    if (Type == "INTEGER"){
      //Get the size of the parameter pool
      Size = Data.GetParameterPoolSizeFilled(Name);
      //Then get the parameter (either the value or address)
      if (How == "BYVAL"){
        Expression(Data.Number);
      }
      else{
        ByRefExpression(Data.Number);
      }
      //Round the result to an integer
      Asm.RoundToInteger();
      //Put the parameter in the pool
      Asm.AddToParameterPool(Data.Integer, Size);
      //Now that we have another integer in the pool, the next available slot
      //will be four bytes further in
      Data.SetParameterPoolSizeFilled(Name, Size + 4);
    }
    //Deal with a string parameter
    if (Type == "STRING"){
      //See how much of the pool has been filled
      Size = Data.GetParameterPoolSizeFilled(Name);
      //Get the result of the expression
      if (How == "BYVAL"){
        Expression(Data.String);      
      }
      else{
        ByRefExpression(Data.String);
      }
      //Add the string to the pool of parameters
      Asm.AddToParameterPool(Data.String, Size);
      //Make room for the next parameter 
      Data.SetParameterPoolSizeFilled(Name, Size + 4);
    }
    //Deal with a double
    if (Type == "DOUBLE"){
      //See how much of the pool is occupied
      Size = Data.GetParameterPoolSizeFilled(Name);
      //Get the result of the expression
      if (How == "BYVAL"){
        Expression(Data.Number);
        //If we are passing by value, the size is 8 bytes, so store and 
        //make room for the next one
        Asm.AddToParameterPool(Data.Double, Size);
        Data.SetParameterPoolSizeFilled(Name, Size + 8);
      }
      else{
        Expression(Data.Number);
        //If we are just passing by reference, we only need 4 bytes (a pointer)
        Asm.AddToParameterPool(Data.Integer, Size);
        Data.SetParameterPoolSizeFilled(Name, Size + 4);      
      }
    }
    //Now if we are passing a UDT, deal with that
    if (Data.IsType(Type) == true){
      //Get how much room is available
      Size = Data.GetParameterPoolSizeFilled(Name);
      //Get the UDT
      Expression(Data.UDT, Type);
      //Add it to the pool (only the reference)
      Asm.AddToParameterPool(Data.Type, Size);
      //Since we have added to the pool, update information
      Data.SetParameterPoolSizeFilled(Name, Size + 4);
    }
    //If we have finished all the paramters, break from our loop
    if (i == ParamCount){
      break;
    }
    //Otherwise, we need a comma to separate the parameters
    if (Read.Word() != ","){
      Error.ExpectedNextParameter(Read);
    }
  }
  //Now that we have all the parameters loaded into the parameter pool, let's
  //transfer them to the stack in reverse order. Basically, we do the opposite
  //that we did above. Push the parameter from the pool to the stack, and then
  //update the counter, telling the pool that we have successfully pushed 
  //a parameter.
  for (int I = ParamCount; I >= 1; I--){  //Count backwords, pushing as we go
    string Type     = Data.GetSubFunctionInfo(Name).Parameters[I].Type;
    string How      = Data.GetSubFunctionInfo(Name).Parameters[I].How;
    if (Type == "INTEGER"){
      Data.SetParameterPoolSizeFilled(Name, 
                                     Data.GetParameterPoolSizeFilled(Name) - 4);
      Asm.PushParameterPool(Data.Integer,Data.GetParameterPoolSizeFilled(Name));
    }
    if (Type == "STRING"){
      Data.SetParameterPoolSizeFilled(Name, 
                                     Data.GetParameterPoolSizeFilled(Name) - 4); 
      Asm.PushParameterPool(Data.String,Data.GetParameterPoolSizeFilled(Name));
    }
    if (Type == "DOUBLE"){
      Data.SetParameterPoolSizeFilled(Name, 
                                     Data.GetParameterPoolSizeFilled(Name) - 8);
      Asm.PushParameterPool(Data.Double, Data.GetParameterPoolSizeFilled(Name));
    }
    if (Data.IsType(Type) == true){
      Data.SetParameterPoolSizeFilled(Name, 
                                     Data.GetParameterPoolSizeFilled(Name) - 4); 
      if (How == "BYVAL"){
        Asm.PushParameterPool(Data.Type, Data.GetParameterPoolSizeFilled(Name));
      }
      else{
        Asm.PushParameterPool(Data.Integer,
                              Data.GetParameterPoolSizeFilled(Name));
      }
    }
  }
  return;
}

/******************************************************************************
PrepareParameters - Takes the parameters' names and converts them into 
stack names so they can be refereneced as local variables
******************************************************************************/
void Compiler::PrepareParameters(string Name){
  //The actual parameters start 8 bytes into the stack
  int offset = 8;
  int ParamCount = Data.GetSubFunctionInfo(Name).ParamCount;
  //Now loop through the parameters, locating each one in the stack
  for (int i = 1; i <= ParamCount; i++){
    //Get the information about the parameters
    string Type = Data.GetSubFunctionInfo(Name).Parameters[i].Type;
    string ParamName = Data.GetSubFunctionInfo(Name).Parameters[i].Name;
    //If the variable is a double, offset by 8
    if (Type == "DOUBLE"){
      if (Type == "DOUBLE"){
        Data.SetAsmName(ParamName, "EBP+" + ToStr(offset));
        offset += 8;
      }
    }
    //Else offset by 4 bytes
    else{
      Data.SetAsmName(ParamName, "EBP+" + ToStr(offset));
      offset += 4;
    }
  }
  return;
}

/******************************************************************************
UnprepareParameters - after the function or subroutine has been called and has
returned, we need to free the memory that the parameters occupy. Clean up jobs.
******************************************************************************/
void Compiler::UnprepareParameters(string Name){
  //Get how many parameters we have so we can deal with each
  int ParamCount = Data.GetSubFunctionInfo(Name).ParamCount;
  //If we are using the C calling convention, we need to adjust the stack
  //because the function didn't
  if (Data.GetSubFunctionInfo(Name).ExternalInfo.CallingConv == "C"){
    Asm.AdjustStack(Data.GetSubFunctionInfo(Name).SizeOfParameters);
  }
  //OK, we are starting at zero in parameter pool
  Data.SetParameterPoolSizeFilled(Name, 0);
  for (int i = 1; i <= ParamCount; i++){
    //Get information associated with the parameters
    string Type = Data.GetSubFunctionInfo(Name).Parameters[i].Type;
    string How  = Data.GetSubFunctionInfo(Name).Parameters[i].How;
    string ParamName = Data.GetSubFunctionInfo(Name).Parameters[i].Name;
    bool   External = Data.GetSubFunctionInfo(Name).External;
    string CallConv = Data.GetSubFunctionInfo(Name).ExternalInfo.CallingConv;
    //If we have a valid parameter
    if (Data.IsType(Type) == true || Type == "STRING" || Type == "DOUBLE"){
      //Strings and data types need to be freed to avoid memory leaks
      if ((Type == "STRING" || Data.IsType(Type) == true) && How != "BYREF"){
        //Preserve the return value of the parameter (located in EAX)
        Asm.PUSH("EAX");
        //Get the parameter from the parameter pool and push to the stack
        Asm.PushParameterPool(Data.String,Data.GetParameterPoolSizeFilled(Name));
        //Get the parameter from the stack
        Asm.POP("ECX");
        //Free the memory
        Asm.FreeMemory(Write.ToMain, "ECX");
        //Restore the return value
        Asm.POP("EAX");
        //Now we have dealt with that parameter, adjust the pool size
        Data.SetParameterPoolSizeFilled(Name,
                                     Data.GetParameterPoolSizeFilled(Name) + 4);
       }
      //If we ar only a double or a byref, we don't need to free any memory.
      //So let's just update the information
      if (Type == "DOUBLE"){
        Data.SetParameterPoolSizeFilled(Name,
                                     Data.GetParameterPoolSizeFilled(Name) + 4);
        if (How != "BYREF"){
          Data.SetParameterPoolSizeFilled(Name, 
                                     Data.GetParameterPoolSizeFilled(Name) + 4);
        }   
      }
    }
    //All other parameters just need to be adjusted
    else{
      Data.SetParameterPoolSizeFilled(Name, 
                                     Data.GetParameterPoolSizeFilled(Name) + 4);
    }
  }
  //We've called the function, and the function is finished, and we have freed
  //the parameters from the pool, so now get rid of the parameter pool
  Asm.FreeParameterPool();
  return;
}

/******************************************************************************
DeclareSubFunction - validates that we are indeed declaring an external function
Example:
  DECLARE SUB A()
  ^^^^^^^^^^^---Declare an external function
******************************************************************************/
void Compiler::DeclareSubFunction(){
  Read.GetNextWord();
  //If we aren't DECLAREing a sub or a function, we're in trouble
  if (Read.Word() != "SUB" && Read.Word() != "FUNCTION"){
    Error.ExpectedSubFunction(Read);
  }
  //Now let's proceed as normal to SubFunction, except we mark this function as
  //external
  SubFunction(true);
  return;
}

/******************************************************************************
SubFunctionLibAliasCall - gets the following inform
Example:
  Declare Function Sleep Lib "Kernel32.dll" Alias "Sleep" Call Std (I As _
                         ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^---Get 
                         external function information          
******************************************************************************/
void Compiler::SubFunctionLibAliasCall(string Name){
  //The three things we need to know about the library where the function is
  //located, the actual name of the function, and what calling convention to use
  string Library;
  string Alias = Name;
  string CallConv;
  //On Windows, the default calling convention is standard 
  #ifdef Windows
    CallConv = "STD";
  #endif
  //On Linux, the default calling convention is C
  #ifdef Linux
    CallConv = "C";
  #endif
  //If we have the LIB keyword, we are about to get the library where the 
  //function resides
  if (Read.Word() == "LIB"){
    Read.GetNextWord();
    //Library must be encased in a string
    if (Read.WordType() != Read.String){
      Error.InvalidLibrary(Read);
    }
    Library = Read.Word();
    Read.GetNextWord();
  }
  //If we encounter Alias, we are about to get the real name of the function
  if (Read.Word() == "ALIAS"){
    Read.GetNextWord();
    //Alias must be encased in a string
    if (Read.WordType() != Read.String){
      Error.InvalidAlias(Read);
    }
    Alias = Read.Word();
    Read.GetNextWord();
  }
  //If we encounter Call, we are being informed what calling convention to use
  if (Read.Word() == "CALL"){
    Read.GetNextWord();
    //Right now, the calling convention must either be C or Std
    if (Read.Word() != "C" && Read.Word() != "STD"){
      Error.InvalidCallingConvention(Read);
    }
    CallConv = Read.Word();
    Read.GetNextWord();
  }
  //If we don't have a library, this is not good!
  if (Library == ""){
    Error.ExpectedExternalSubFunction(Read);
  }
  //Add the information to the database
  Data.AddExternalSubFunctionData(Name, Library, Alias, CallConv);
  return;
}

/******************************************************************************
ByRefExpression - Gets a single expression and gets the address of it
******************************************************************************/
void Compiler::ByRefExpression(int Type){
  Read.GetNextWord();
  //We can only get the address of a variable
  if (Read.WordType() != Read.Identifier && Read.WordType() != Read.String){
    Error.BadName(Read);
  }
  //If the variable is a string, get the address of it
  if (Data.GetDataType(Read.Word()) == Data.String){
    if (Type != Data.String){
      Error.ExpectedStringData(Read);
    }
    Asm.PUSH("dword[" + Data.Asm(Read.Word()) + "]");
    Read.GetNextWord();
    return;
  }
  //If the variable is a number, get the address of it
  if (Data.GetDataType(Read.Word()) == Data.Integer ||
      Data.GetDataType(Read.Word()) == Data.Double){
    if (Type != Data.Number){
      Error.ExpectedNumberData(Read);
    }
    Asm.PUSH(Data.Asm(Read.Word()));   
    Read.GetNextWord();
    return;
  }
  //If the variable is a UDT, get the address of it
  if (Data.GetDataType(Read.Word()) == Data.UDT){
    if (Type != Data.UDT){
      Error.ExpectedUDT(Read);
    }
    Asm.PUSH(Data.Asm(Read.Word()));
    Read.GetNextWord();
    return;
  }
  //Oops, something bad happened!
  Error.BadName(Read);
  return;
}

/******************************************************************************
FirstDirectives - handles the directives that MUST come before any code. 
KoolB MUST know what AppType to generate before it starts generating code. 
Likewise, KoolB must know whether or not to optimize the app. If you omit these
directives at the top of the program, KoolB assumes the defaults
******************************************************************************/
bool Compiler::FirstDirectives(string Directive){
  //At the beginning of the program, we can change what type of program to 
  //generate
  if (Directive == "APPTYPE"){
    ChooseAppType();
    Read.GetNextWord();
    return true;
  }
  //We can also choose whether or not to optimize the program
  if (Directive == "OPTIMIZE"){
    OptimizeApp();
    Read.GetNextWord();
    return true;
  }
  //Must deal with AppType and Optimize first
  Error.InvalidDirective(Read);
  return false;
}

/******************************************************************************
Directives - All the directives that you can use in KoolB. The pre-processor
directives, actually, which means that they act on the BASIC source code rather
than the actual program. They really aren't executable statements.
******************************************************************************/
bool Compiler::Directives(string Directive){
  //We can't change the type of program to generate this far into the program
  //This must be done at the beginning of the program
  if (Directive == "APPTYPE"){
    Error.CannotChangeAppType(Read);
    return false;
  }
  //Neither can you optimize the app this far into the program. KoolB must know
  //whether or not to optimize the app before it encounters any code.
  if (Directive == "OPTIMIZE"){
    Error.CannotOptimize(Read);
    return false;
  }
  if (Directive == "COMPRESS"){
    CompressApp();
    Read.GetNextWord();
    return false;
  }
  if (Directive == "INCLUDE"){
    IncludeFile();
    Read.GetNextWord();
    return false;
  }
  if (Directive == "ASM"){
    DropDownToAsm();
    return false;
  }
  if (Directive == "CONST"){
    Const();
    return false;
  }
  if (Directive == "DEFINE"){
    Read.GetNextWord();
    //We can only define identifiers, not numbers or anything
    if (Read.WordType() != Read.Identifier){
      Error.DefineMustBeIdentifier(Read);
    }
    //Add the defined word to the keyword
    Data.AddDefine(Read.Word());
    Read.GetNextWord();
    return false;
  }
  if (Directive == "IFDEF"){
    IfDef();
    return false;
  }
  if (Directive == "IFNDEF"){
    IfNDef();
    return false;
  }
  if (Directive == "ENDIF"){
    //If we have already ended all the $IfDef and $IfNDef, bail out
    if (Data.CanExitDirective() == false){
      Error.CannotExitDirective(Read);
    }
    //Exit the directive
    Data.ExitDirective();
    Read.GetNextWord();
    return false;
  }
  if (Directive == "END"){
    Read.GetNextWord();
    //End If will end an $IfDef or $IfNDef
    if (Read.Word() == "IF"){
      //If we have already ended all the $IfDef and $IfNDef, bail out
      if (Data.CanExitDirective() == false){
        Error.CannotExitDirective(Read);
      }
      //Exit the directive
      Data.ExitDirective();
      Read.GetNextWord();
      return false;
    }
  }
  if (Directive == "MANGLE"){
    MangleNames();
    Read.GetNextWord();
    return false;
  }
  //Oops, bad directive!
  Error.InvalidDirective(Read);
  return false;
}

/******************************************************************************
IncludeFile - includes another BASIC source code file into the program
Examples:
  $Include "Libraries/String.inc"  ' Include from the compiler's folder
  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^---Include a file as if it was in the program
  
  $Include "MyLibrary.inc"         ' Include from the program's folder
  ^^^^^^^^^^^^^^^^^^^^^^^^---Include a file as if it was in the program
******************************************************************************/
void Compiler::IncludeFile(){
  //Store the old directive level for the current file
  int OldDirectiveLevel = Data.GetDirectiveLevel();
  //Fopr the new file, set the directive level to ground zero
  Data.SetDirectiveLevel(0);
  //Create a new reading and compiler object - but we still use the same writing
  //object so all the code goes to the same program
  Reading  NewFile;
  Compiler NewCompiler;
  Read.GetNextWord();
  //Get the name of the file
  if (Read.WordType() != Read.String){
    Error.ExpectedFileNameAfterInclude(Read);
  }
  //If the file doesn't exist, it may exist in same folder as the original 
  //source code file
  if (FileExists(Read.Word()) == false){
    //So patch the original program path and the filename of the file to include
    string NewFileName = PatchFileNames(Read.GetBookName(), Read.Word());
    //If that file exists, let's go ahead and compile it
    if (FileExists(NewFileName) == true){
      //Open the file with the reading object
      NewFile.OpenBook(NewFileName);
      //Compile it - and make sure we set the IncFile parameter to true
      NewCompiler.Compile(NewFile, true);
      //Now set the directive level back to the level it was before the $Include
      //directive was encountered
      Data.SetDirectiveLevel(OldDirectiveLevel);
      return;
    }
  }
  //Open the file with the reading object
  NewFile.OpenBook(Read.Word());
  //Compile the included file - and set the IncFile parameter to true
  NewCompiler.Compile(NewFile, true);
  //Set the directive level back to the old level
  Data.SetDirectiveLevel(OldDirectiveLevel);
  return;
}

/******************************************************************************
DropDownToAsm - allows us to write inline assembly code
Example:
  $Asm
    EXTERN ExitProcess
    PUSH 0
    CALL ExitProcess
  $End Asm
******************************************************************************/
void Compiler::DropDownToAsm(){
  string Line;
  Read.GetNextWord();
  //If we don't have an end of the line after $Asm, bail out
  if (Read.WordType() != Read.EndOfLine){
    Error.ExpectedEndOfLineAfterAsm(Read);
  } 
  //Don't set the source code to uppercase - preserve the case
  Read.SetUppercase(false);
  //Enter an infinte loop, that breaks only when we encounter $End Asm or an 
  //error
  for (;;){
    //Clear the line
    Line = "";
    Read.GetNextWord();
    //If we have just a new line, loop around again
    if (Read.WordType() == Read.EndOfLine){
      continue;
    }
    //If we encounter the end of the file, we print out an error. We didn't find
    //an $End Asm.
    if (Read.WordType() == Read.None){
      Error.NoEndToAsm(Read);
    }
    //Store the current word in Line
    Line = Read.Word();
    //Check to see if we have a $End Asm
    if (Read.Word() == "$"){
      //Set uppercase to true
      Read.SetUppercase(true);
      Read.GetNextWord();
      //Check for the existence of an End Asm - then finish up
      if (Read.Word() == "END"){
        Read.GetNextWord();
        if (Read.Word() == "ASM"){
          Read.GetNextWord();
          return;
        }
      }
      //If we don't find it, output an error
      Error.ExpectedEndAsm(Read);
    }
    //If we don't have an $ as the first word of the line, let's get the rest
    //of the assembly language on the line
    Line += Read.GetWholeLine();
    //Write the raw assembly langugage to the writing object
    Write.Line(Write.ToMain, Line);
  }
  return;
}

/******************************************************************************
ChooseAppType - sets what type of app to generate. We can generate a normal
console or GUI app (the only difference being that a console app automatically
has a console, while the GUI doesn't). Or we can generate a DLL (or SO on 
Linux), which is really just a collection of functions.
Examples:
  $AppType Console
  $AppType GUI
  $AppType DLL
******************************************************************************/
void Compiler::ChooseAppType(){
  Read.GetNextWord();
  //Choose which type of apptype
  if (Read.Word() == "CONSOLE"){
    AppType = Console;
    return;
  }
  if (Read.Word() == "GUI"){
    AppType = GUI;
    return;
  }
  if (Read.Word() == "DLL"){
    AppType = DLL;
    //We can't optimize a DLL, otherwise we'll lose all the functions (since
    //they are used at runtime, not compile time.
    Optimize = false;
    return;
  }
  //Bad type of program.
  Error.InvalidAppType(Read);
  return;
}

/******************************************************************************
PrepareProgram - prepares the program so the user can start coding. Determines
the type of program, whether or not to optimize, defines some important things
to know, and then builds the core of the program.
******************************************************************************/
void Compiler::PrepareProgram(){
  //Define either Windows or Linux so the user can tell what type of OS 
  //the source code is being compiled on
  #ifdef Windows
    Data.AddDefine("WINDOWS");
  #endif
  #ifdef Linux
    Data.AddDefine("LINUX");
  #endif
  //We haven't built the program yet
  bool BuiltProgram = false;
  //Get the AppTpe and Optimize directives so we can then build the program
  while (Read.Word() == "$" && BuiltProgram == false){
    Read.GetNextWord();
    //If we have $AppType or $Optimize, we still are getting info needed to 
    //build the app
    if (Read.Word() == "APPTYPE" || Read.Word() == "OPTIMIZE"){
      FirstDirectives(Read.Word());
    }
    //Otherwise, go ahead an build the app
    else{
      //Write the skeleton for the app
      Asm.BuildSkeleton();
      //Prepare error messages for not enough memory and so forth
      Asm.PrepareErrorMessages();
      //Initialize the memory management routines
      Asm.InitMemory();
      //Since we have a $, but not AppType or Optimize, deal with the directive
      //we have
      Directives(Read.Word());
      //Yep, we've intialized and built the program
      BuiltProgram = true;
    }
    //We should have a new line after each command
    if (Read.WordType() != Read.EndOfLine){
      Error.EndOfLine(Read);
    }
    Read.GetNextWord();
    //No sense in analyzing new lines, just skip past them
    while (Read.WordType() != Read.EndOfLine){
      Read.GetNextWord();
    }
  }
  //If we exit the $ loop without having a directive, we still need to build
  //the app
  if (BuiltProgram == false){
    Asm.BuildSkeleton();
    Asm.PrepareErrorMessages();
    Asm.InitMemory();
  }
  //If this is a console or GUI app, prepare pre-set variables:
  //CommandLine$ and HInstance
  if (AppType == Console || AppType == GUI){
    Asm.GetPreSetVariables();
    //Also, is this is a console app, intialize the console
    if (AppType == Console){
      Asm.InitConsole();
    }
  }
  //Add a define for the user so they know what type of app we are building
  if (AppType == Console){
    Data.AddDefine("COSNOLE");
  }
  if (AppType == GUI){
    Data.AddDefine("GUI");
  }
  if (AppType == DLL){
    Data.AddDefine("DLL");
  }
  return;
}

/******************************************************************************
Optimize - turns optimize on and off. It is only allowed at the beginning of 
each program. It is illegal to turn Optimize On or Off in the middle of the 
program
Examples:
  $Optimize ON
  $Optimize OFF
******************************************************************************/
void Compiler::OptimizeApp(){
  Read.GetNextWord();
  //$Optimize expects either ON or OFF
  if (Read.Word() != "ON" && Read.Word() != "OFF"){
    Error.ExpectedOnOrOff(Read);
  }
  //Turn the optimization either on or off
  if (Read.Word() == "ON"){
    Optimize = true;
  }
  if (Read.Word() == "OFF"){
    Optimize = false;
  }
  return;
}

/******************************************************************************
IfDef - If the identifier has been defined earlier, compile the code. Otherwise,
don't compile code.
Example:
  $IfDef Windows
    ' Code
  $End If
******************************************************************************/
void Compiler::IfDef(){
  Read.GetNextWord();
  //Enter a new level of directives
  Data.EnterDirective();
  //If this identifier is defined, return and continue to compile the code
  if (Data.IsDefined(Read.Word()) == true){
    Read.GetNextWord();
    return;
  }
  //Otherwise, if the identifier is not defined, eat the code until we encounter
  //an $End If or an error
  for (;;){
    //Eat the whole line
    while (Read.WordType() != Read.EndOfLine){
      Read.GetNextWord();
      //If we have come to the end of the file, error!
      if (Read.WordType() == Read.None){
        Error.NoEndToIfDef(Read);
      }
    }
    Read.GetNextWord();
    //Make sure we can handle nested $IfDefs or $IfNDefs
    if (Read.Word() == "$"){
      Read.GetNextWord();
      //If we have IfDef, recursively call IfDef
      if (Read.Word() == "IFDEF"){
        IfDef();
      }
      //If we have IfNDef, handle that too
      if (Read.Word() == "IFNDEF"){
        IfNDef();
      }
      //If we have END IF, exit the level and we're done!
      if (Read.Word() == "END"){
        Read.GetNextWord();
        if (Read.Word() == "IF"){
          Data.ExitDirective();
          break;
        }
      }
      //Or if we have an ENDIF, blast off!
      if (Read.Word() == "ENDIF"){
        Data.ExitDirective();
        break;
      }
    }
  }
  Read.GetNextWord();
  return;
}

/******************************************************************************
IfNDef - If the identifier is not defined, compile the code. Otherwise, if the
identifier is defined, don't compile the code.
Example:
  $IfNDef Windows
    ' Code
  $End If
******************************************************************************/
void Compiler::IfNDef(){
  Read.GetNextWord();
  //If the identifier isn't defined, enter a new level and then exit to compile
  //the rest of the code
  if (Data.IsDefined(Read.Word()) != true){
    Data.EnterDirective();
    Read.GetNextWord();
    return;
  }
  //Loop until we encounter an $End If or an error
  for (;;){
    //Eat all lines, no sense in processing or compiling
    while (Read.WordType() != Read.EndOfLine){
      Read.GetNextWord();
      //If we have gotten to the end of the file witout an $End If, error!
      if (Read.WordType() == Read.None){
        Error.NoEndToIfDef(Read);
      }
    }
    Read.GetNextWord();
    //See if the first word on the next line is a $
    if (Read.Word() == "$"){
      Read.GetNextWord();
      //If we have an $IfDef, deal with it so we can nest the statements
      if (Read.Word() == "IFDEF"){
        IfDef();
      }
      //Same with $IfNDef
      if (Read.Word() == "IFNDEF"){
        IfNDef();
      }
      //If we have an $End If, exit the level and return
      if (Read.Word() == "END"){
        Read.GetNextWord();
        if (Read.Word() == "IF"){
          Data.ExitDirective();
          break;
        }
      }
      //Or if we have an ENDIF, blast off!
      if (Read.Word() == "ENDIF"){
        Data.ExitDirective();
        break;
      }
    }
  }
  Read.GetNextWord();
  return;
}

/******************************************************************************
Const - Allows you to effectively do a 'search and replace' with an identifier
For example, you can declare an identifier a constant, assign it a value and 
whenever KoolB encounters that constant, it will replace the identifier with
the constant value
Examples:
  $Const WM_DESTROY = 2
  $Const US = "United States
******************************************************************************/
void Compiler::Const(){
  string Name;
  string ConstData;
  Read.GetNextWord();
  Name = Read.Word();
  //If the name of the constant isn't an identifier or is already reserved, 
  //print out an error to the user
  if (Read.WordType() != Read.Identifier){
    if (Read.WordType() == Read.None ||
        Read.WordType() == Read.EndOfLine){
      Error.ExpectedNameAfterDIM(Read);
    }
    Error.BadName(Read);
  }
  if (Data.AlreadyExistsInScope(Name)){
    Error.AlreadyReserved(Read);
  }
  Read.GetNextWord();
  //After the name, we need an equal sign!
  if (Read.Word() != "="){
    Error.ExpectedAssignment(Read);  
  }
  Read.GetNextWord();
  //Get the constant value
  ConstData = Read.Word();
  //We might have a negative number, so deal with that
  if (Read.Word() == "-"){
    Read.GetNextWord();
    //Can't negate anything but a number
    if (Read.WordType() != Read.Number){
      Error.CanOnlyNegateNumbers(Read);
    }
    //Ok, add the number to the constant value so we have - and then the number
    ConstData += Read.Word();
  }
  //If we have a number or string constant data, add it to the database
  if (Read.WordType() == Read.Number || Read.WordType() == Read.String){
    Read.AddConstData(Name, Read.Word(), Read.WordType());
    Read.GetNextWord();
    return;
  }
  //Oops, something went wrong!
  Error.ConstDataMustBeNumberOrString(Read);
  return;
}

/******************************************************************************
CompressApp - turns the compression of an app on and off. Since compression 
takes place after all the code is generated, you could have $Compress On at 
the very end of your program and it would still take effect
Example:
  $Compress ON
  $Compress OFF
******************************************************************************/
void Compiler::CompressApp(){
  Read.GetNextWord();
   //Turn the compression on or off
  if (Read.Word() == "ON"){
    Compress = true;
    return; 
  }
  if (Read.Word() == "OFF"){
    Compress = false;
    return;
  }
  Error.ExpectedOnOrOff(Read);
  return;
}

/******************************************************************************
CompressApp - turns the compression of an app on and off. Since compression 
takes place after all the code is generated, you could have $Compress On at 
the very end of your program and it would still take effect
Example:
  $Compress ON
  $Compress OFF
******************************************************************************/
void Compiler::MangleNames(){
  Read.GetNextWord();
   //Turn the compression on or off
  if (Read.Word() == "ON"){
    Mangle = true;
    return; 
  }
  if (Read.Word() == "OFF"){
    Mangle = false;
    return;
  }
  Error.ExpectedOnOrOff(Read);
  return;
}

#endif
