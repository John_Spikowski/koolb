/******************************************************************************
 *|--------------------------------------------------------------------------|*
 *| BASM Linux Compiler - Fork of KoolB Version 15.02 by Brian C. Becker     |*
 *| 2014-12-01 - Armando I. Rivera (AIR) initial contributor                 |*
 *|                                                                          |*
 *| Database.h - handles all of the 'database' stuff and keeping track of    |*
 *| everything                                                               |*
 *|--------------------------------------------------------------------------|*
 *****************************************************************************/

#ifndef Database_h
#define Database_h

//Keeps information relating to simple data types (string, integer, double)
struct SimpleDataInfo{
  string AsmName;
  int    Type;
};

//Keeps information relating to arrays
struct ArrayDataInfo{
  string AsmName;
  int    Type;
};

//Keeps information relating to types and their members
struct TypeDataInfo{
  string AsmName;
  int   Size;
  map<string, SimpleDataInfo> Members;
};

//Keeps information relating to UDTs 
struct UDTDataInfo{
  string       AsmName;
  string       TypeName;
  TypeDataInfo Type;
};

//Keeps information relating to parameters
struct Parameter{
  string AsmName;
  string Name;
  string Type;
  string How;
};

//Keeps information relating to external functions (WinAPI)
struct ExternalSubFunction{
  string Library;
  string Alias;
  string CallingConv;
};

//Keeps information relating to user-defined functions
struct SubFunctionInfo{
  string Name;
  string Type;
  int    ParamCount;
  map<int, Parameter> Parameters;
  Parameter ReturnValue;
  int    SizeOfParameters;
  int    SizeOfLocalVariables;
  bool   External;
  ExternalSubFunction ExternalInfo;
  int    ParameterPoolFilled;
  bool   Used;
  map<string, SubFunctionInfo> UsesSubFunctions;
};

//Ahh, the elusive scoping rountines. Provides levels of scoping for each type
//of data
struct Scoping{
  map<string, SimpleDataInfo> SimpleDataTypes;
  map<string, ArrayDataInfo>  ArrayDataTypes;
  map<string, TypeDataInfo>   Types;
  map<string, UDTDataInfo>    UDTDataTypes;
  map<string, SubFunctionInfo>SubFunctions;
};

//Keep track of everything in the program relating to variables and functions
class Database{
 private:
  vector<string>    KeyWord;                //List of keywords
  vector<string>    ImportedFunctions;      //List of external functions
  map<int, Scoping> Scope;                  //Provides all the scopes
  int               ScopeLevel;             //The current scope level
  int               UniqueScopeID;          //Gives each stroke a different ID
  bool              InsideSubFunction;      //Are we inside a function?
  string            InsideSubFunctionName;  //The name of the function we are in
  string            LastSubFunction;        //Last function we were in
  string            UsedSubFunctions;       //A code snippet of used functions
  vector<string>    Defined;                //A list of defined identifiers
  int               DirectiveLevel;         //Which directive level are we on

 public:
   //Intialize the database
  Database(){
    ScopeLevel         = 0;          //On scope ground zero 
    UniqueScopeID      = 0;          //Start numbering scopes at 0
    DirectiveLevel     = 0;          //On ground zero with directives
    InsideSubFunction  = false;      //Nope, not in a function
    UsedSubFunctions   = "";         //No used functions yet
    //Add all the keywords
    KeyWord.push_back("DIM");
    KeyWord.push_back("AS");
    KeyWord.push_back("INTEGER");
    KeyWord.push_back("DOUBLE");
    KeyWord.push_back("STRING");
    KeyWord.push_back("TYPE");
    KeyWord.push_back("END");
    KeyWord.push_back("SLEEP");
    KeyWord.push_back("CLS");
    KeyWord.push_back("PRINT");
    KeyWord.push_back("INPUT");
    KeyWord.push_back("IF");
    KeyWord.push_back("THEN");    
    KeyWord.push_back("ELSE");
    KeyWord.push_back("WHILE");
    KeyWord.push_back("WEND");
    KeyWord.push_back("SUB");
    KeyWord.push_back("FUNCTION");
    KeyWord.push_back("CALLBACK");
    KeyWord.push_back("CODEPTR");
    KeyWord.push_back("ADDRESSOF");
    KeyWord.push_back("SIZEOF");
    KeyWord.push_back("DECLARE");
  }
  
  bool IsKeyword(string Name);
  bool IsAlreadyReserved(string Name);
  bool AlreadyExistsInScope(string Name);
  bool IsAlreadyReservedInType(string TypeName, string Name);
  void AddSimpleData(string Name, SimpleDataInfo Info);
  void AddArrayData(string Name, ArrayDataInfo Info);
  void AddTypeData(string Name, TypeDataInfo Info);
  void AddSimpleDataToType(string TypeName, string Name, SimpleDataInfo Info);
  void AddType(string TypeName);
  bool IsType(string Name);
  void AddUDTData(string TypeName, string Name, string AsmName);
  void AddSubFunction(string Type, string Name, bool External);
  bool IsAlreadyReservedInParameters(string Name, string ParameterName);
  void AddParameter(string Name, string ParameterName, 
                    string Type, string How);
  void AddReturnValue(string Name, string Type);
  int  GetSizeOfParameters(string Name);
  string AddLocalVariable(int Type);
  void AddExternalSubFunctionData(string Name, string Library, string Alias, 
                                  string CallConv);
  void SetParameterPoolSizeFilled(string Name, int HowMuch);
  int GetParameterPoolSizeFilled(string Name);
  void UsingSubFunction(string Name);
  
  void AddDefine(string Define);
  bool IsDefined(string Define);
  void EnterDirective();
  bool CanExitDirective();
  void ExitDirective();
  int  GetDirectiveLevel();
  void SetDirectiveLevel(int Level);
  
  int GetDataType(string Name);
  SimpleDataInfo& GetSimpleData(string Name);
  ArrayDataInfo& GetArrayData(string Name);
  UDTDataInfo& GetUDTData(string Name);
  TypeDataInfo& GetTypeData(string Name);
  SubFunctionInfo& GetSubFunctionInfo(string Name);
  string GetUsedSubFunctions();

  string StripJunkOff(string Name);
  string Asm(string Name);
  void SetAsmName(string Name, string AsmName);
  
  void EnterNewScope();
  void ExitScope();
  string GetScopeID();
  bool IsInsideSubFunction();
  string ListFunctions(); 
  
  bool IsFunctionImported(string Name);

  //Define the different types of variables and functions we can have
  enum SimpleDataTypes{Integer, Double, String};
  enum Types{BASICKeyword = 3, Number, Array, Type, UDT, Boolean, Unknown};
  enum SubroutinesAndFunctions{SubFunction = 10};

};

/******************************************************************************
IsKeyword - find out if a word is a keyword
******************************************************************************/
bool Database::IsKeyword(string Name){
  if (find(KeyWord.begin(), KeyWord.end(), Name) != KeyWord.end()){
    return true;
  }
  return false;
}

/******************************************************************************
AlreadyExistsInScope - A very important function that checks to see if a word
is present in the current scope. Very useful when creating a new variable
******************************************************************************/
bool Database::AlreadyExistsInScope(string Name){
  if (find(KeyWord.begin(), KeyWord.end(), Name) != KeyWord.end()){
    return true;
  }
  if (Scope[ScopeLevel].SimpleDataTypes.find(Name) != 
      Scope[ScopeLevel].SimpleDataTypes.end()){
    return true;
  }
  if (Scope[ScopeLevel].ArrayDataTypes.find(Name) != 
      Scope[ScopeLevel].ArrayDataTypes.end()){
    return true;
  }
  if (Scope[ScopeLevel].Types.find(Name) != Scope[ScopeLevel].Types.end()){
    return true;
  }
  if (Scope[ScopeLevel].UDTDataTypes.find(Name) != 
      Scope[ScopeLevel].UDTDataTypes.end()){
    return true;
  }
  if (Scope[ScopeLevel].SubFunctions.find(Name) != 
      Scope[ScopeLevel].SubFunctions.end()){
    return true;
  }
  return false;
}

/******************************************************************************
IsAlreadyReserved - similar to the above one, but checks for the presence in 
all scopes, not just the current level
******************************************************************************/
bool Database::IsAlreadyReserved(string Name){
  if (find(KeyWord.begin(), KeyWord.end(), Name) != KeyWord.end()){
    return true;
  }
  if (Read.IsConstData(Name) == true){
    return true;
  }
  for (int Level = ScopeLevel; Level >=0; Level--){
    if (Scope[Level].SimpleDataTypes.find(Name) != 
        Scope[Level].SimpleDataTypes.end()){
      return true;
    }
    if (Scope[Level].ArrayDataTypes.find(Name) != 
        Scope[Level].ArrayDataTypes.end()){
      return true;
    }
    if (Scope[Level].Types.find(Name) != Scope[Level].Types.end()){
      return true;
    }
    if (Scope[Level].UDTDataTypes.find(Name) != 
        Scope[Level].UDTDataTypes.end()){
      return true;
    }
    if (Scope[Level].SubFunctions.find(Name) != 
        Scope[Level].SubFunctions.end()){
      return true;
    }
  }
  return false;
}

/******************************************************************************
IsAlreadyReservedInType - checks to see if we have already added a member to a 
type with the same name. Avoids duplicate member names.
******************************************************************************/
bool Database::IsAlreadyReservedInType(string TypeName, string Name){
  for (int Level = ScopeLevel; Level >=0; Level--){
    if (Scope[Level].Types.find(TypeName) != Scope[Level].Types.end()){
      if (Scope[Level].Types[TypeName].Members.find(Name) !=
          Scope[Level].Types[TypeName].Members.end()){
        return true;
      }
    }
  }
  return false;
}

/******************************************************************************
AddSimpleData - adds a simple data type with associated info to the database
******************************************************************************/
void Database::AddSimpleData(string Name, SimpleDataInfo Info){
  Scope[ScopeLevel].SimpleDataTypes[Name] = Info;
  return;
}

/******************************************************************************
AddArrayData - adds an array and associated info into the database
******************************************************************************/
void Database::AddArrayData(string Name, ArrayDataInfo Info){
  Scope[ScopeLevel].ArrayDataTypes[Name] = Info;
  return;
}

/******************************************************************************
AddTypeData - addss a type definition and associated info into the database
******************************************************************************/
void Database::AddTypeData(string Name, TypeDataInfo Info){
  Scope[ScopeLevel].Types[Name] = Info;
  return;
}

/******************************************************************************
Asm - returns the assembly language version of a name of the variable/function
******************************************************************************/
string Database::Asm(string Name){
  for (int Level = ScopeLevel; Level >=0; Level--){
    if (Scope[Level].SimpleDataTypes.find(Name) != 
        Scope[Level].SimpleDataTypes.end()){
      return Scope[Level].SimpleDataTypes[Name].AsmName;
    }
    if (Scope[Level].ArrayDataTypes.find(Name) != 
        Scope[Level].ArrayDataTypes.end()){
      return Scope[Level].ArrayDataTypes[Name].AsmName;
    }
    if (Scope[Level].Types.find(Name) != Scope[Level].Types.end()){
      return Scope[Level].Types[Name].AsmName;
    }
    if (Scope[Level].UDTDataTypes.find(Name) != 
        Scope[Level].UDTDataTypes.end()){
      return Scope[Level].UDTDataTypes[Name].AsmName;
    }
    if (Scope[Level].SubFunctions.find(Name) != 
        Scope[Level].SubFunctions.end()){
      return Scope[Level].SubFunctions[Name].Name;
    }
  }
  return "";
}

/******************************************************************************
StripJunkOff - replaces BASIC characters (&, #, $) with underscores for 
compatibility with assembly language
******************************************************************************/
string Database::StripJunkOff(string Name){
  if (Name[Name.length() - 1] == '&'){
    return Name.substr(0, Name.length() - 1) + "_";
  }
  if (Name[Name.length() - 1] == '#'){
    return Name.substr(0, Name.length() - 1) + "__";
  }
  if (Name[Name.length() - 1] == '$'){
    return Name.substr(0, Name.length() - 1) + "___";
  }
  return Name;
}

/******************************************************************************
AddSimpleDataToType - adds simple data to the definition of a type
******************************************************************************/
void Database::AddSimpleDataToType(string TypeName, string Name, 
                                   SimpleDataInfo Info){
  Scope[ScopeLevel].Types[TypeName].Members[Name] = Info;
  if (Info.Type == Double){
    Scope[ScopeLevel].Types[TypeName].Size += 8;
  }
  else{
    Scope[ScopeLevel].Types[TypeName].Size += 4;
  }
  return;
}

/******************************************************************************
IsType - goes through the scope to see if the name is a type
******************************************************************************/
bool Database::IsType(string Name){
  for (int Level = ScopeLevel; Level >=0; Level--){
    if (Scope[Level].Types.find(Name) != Scope[Level].Types.end()){
      return true;
    }
  }
  return false;
}

/******************************************************************************
AddUDTData - adds a UDT and associated information to the database
******************************************************************************/
void Database::AddUDTData(string TypeName, string Name, string AsmName){
  UDTDataInfo Info;
  for (int Level = ScopeLevel; Level >=0; Level--){
    if (Scope[Level].Types.find(TypeName) != Scope[Level].Types.end()){
      Info.Type = Scope[Level].Types[TypeName];
    }
  }
  Info.TypeName = TypeName;
  Info.AsmName = AsmName;
  Scope[ScopeLevel].UDTDataTypes[Name] = Info;
  return;
}

/******************************************************************************
GetDataType - sees what type of data or function Name is
******************************************************************************/
int Database::GetDataType(string Name){
  if (find(KeyWord.begin(), KeyWord.end(), Name) != KeyWord.end()){
    return BASICKeyword;
  }
  for (int Level = ScopeLevel; Level >=0; Level--){
    if (Scope[Level].SimpleDataTypes.find(Name) != 
        Scope[Level].SimpleDataTypes.end()){
      if (Scope[Level].SimpleDataTypes[Name].Type == String){
        return String;
      }
      else{
        return Number;
      }
    }
    if (Scope[Level].ArrayDataTypes.find(Name) != 
        Scope[Level].ArrayDataTypes.end()){
      return Array;
    }
    if (Scope[Level].Types.find(Name) != Scope[Level].Types.end()){
      return Type;
    }
    if (Scope[Level].UDTDataTypes.find(Name) != 
        Scope[Level].UDTDataTypes.end()){
      return UDT;
    }
    if (Scope[Level].SubFunctions.find(Name) != 
        Scope[Level].SubFunctions.end()){
      return SubFunction;
    }
  }
  return -1;
}

/******************************************************************************
ArrayDataInfo - returns the information associated with an array
******************************************************************************/
ArrayDataInfo& Database::GetArrayData(string Name){
  for (int Level = ScopeLevel; Level >= 0; Level--){
    if (Scope[Level].ArrayDataTypes.find(Name) != 
        Scope[Level].ArrayDataTypes.end()){
      return Scope[Level].ArrayDataTypes[Name];
    }
  }
}

/******************************************************************************
UDTDataInfo - returns the information associated with a UDT
******************************************************************************/
UDTDataInfo& Database::GetUDTData(string Name){
  for (int Level = ScopeLevel; Level >= 0; Level--){
    if (Scope[Level].UDTDataTypes.find(Name) != 
        Scope[Level].UDTDataTypes.end()){
      return Scope[Level].UDTDataTypes[Name];
    }
  }
}

/******************************************************************************
GestSimpleData - gets the information associated with a simple variable
******************************************************************************/
SimpleDataInfo& Database::GetSimpleData(string Name){
  for (int Level = ScopeLevel; Level >=0; Level--){
    if (Scope[Level].SimpleDataTypes.find(Name) != 
        Scope[Level].SimpleDataTypes.end()){
      return Scope[Level].SimpleDataTypes[Name];
    }
  }
}

/******************************************************************************
AddSubFunction - Adds a function or sub to the database
******************************************************************************/
void Database::AddSubFunction(string Type, string Name, bool External){
  ScopeLevel--;
  LastSubFunction = Name;
  if (Mangle == true){
    Scope[ScopeLevel].SubFunctions[Name].Name = "_" + StripJunkOff(Name);
  }
  else{
    Scope[ScopeLevel].SubFunctions[Name].Name = StripJunkOff(Name);
  }
  Scope[ScopeLevel].SubFunctions[Name].Type = Type;
  Scope[ScopeLevel].SubFunctions[Name].ParamCount = 0;
  Scope[ScopeLevel].SubFunctions[Name].SizeOfParameters = 0;
  Scope[ScopeLevel].SubFunctions[Name].SizeOfLocalVariables = 0;
  Scope[ScopeLevel].SubFunctions[Name].External = External;
  Scope[ScopeLevel].SubFunctions[Name].Used = false;
  if (Type == "FUNCTION" || Type == "SUB"){
    InsideSubFunction = true;
    InsideSubFunctionName = Name;
  }
  ScopeLevel++;
}

/******************************************************************************
AddParameter - adds a parameter and the associated information to a function
******************************************************************************/
void Database::AddParameter(string Name, string ParameterName, 
                            string Type, string How){
  ScopeLevel--;
  int ParamCount = ++Scope[ScopeLevel].SubFunctions[Name].ParamCount;
  Scope[ScopeLevel].SubFunctions[Name].Parameters[ParamCount].Name = 
                                                              ParameterName;
  Scope[ScopeLevel].SubFunctions[Name].Parameters[ParamCount].Type = Type;
  Scope[ScopeLevel].SubFunctions[Name].Parameters[ParamCount].How = How;
  if (Mangle == true){
    Scope[ScopeLevel].SubFunctions[Name].Parameters[ParamCount].AsmName = 
                               GetScopeID() + StripJunkOff(Name) + "__" + Type;
  }
  else{
    Scope[ScopeLevel].SubFunctions[Name].Parameters[ParamCount].AsmName = 
                                              StripJunkOff(Name);
  }
  if (Type == "DOUBLE"){
    Scope[ScopeLevel].SubFunctions[Name].SizeOfParameters += 8;
  }
  else{
    Scope[ScopeLevel].SubFunctions[Name].SizeOfParameters += 4;
  }
  ScopeLevel++;
  return;
}

/******************************************************************************
AddReturnValue - adds the return value to a function
******************************************************************************/
void Database::AddReturnValue(string Name, string Type){
  ScopeLevel--;
  Scope[ScopeLevel].SubFunctions[Name].ReturnValue.Type = Type;
  ScopeLevel++;
  return;
}

/******************************************************************************
IsAlreadyReservedInParameters - checks to see if a name is already in a 
function's parameter list
******************************************************************************/
bool Database::IsAlreadyReservedInParameters(string Name, string ParameterName){
  ScopeLevel--;
  for (int i = 1; i < Scope[ScopeLevel].SubFunctions[Name].ParamCount; i++){
    if (Scope[ScopeLevel].SubFunctions[Name].Parameters[i].Name == 
        ParameterName){
      ScopeLevel++;
      return true;
    }
  }
  ScopeLevel++;
  return false;
}

/******************************************************************************
GetSubFunctionInfo - returns information relating to a subroutine or function
******************************************************************************/
SubFunctionInfo& Database::GetSubFunctionInfo(string Name){
  for (int Level = ScopeLevel; Level >=0; Level--){
    if (Scope[Level].SubFunctions.find(Name) != 
        Scope[Level].SubFunctions.end()){
      return Scope[Level].SubFunctions[Name];
    }
  }
}

/******************************************************************************
EnterNewScope - adds another level of scoping to the program
******************************************************************************/
void Database::EnterNewScope(){
  ScopeLevel++;
  UniqueScopeID++;
  return;
}

/******************************************************************************
ExitScope - exits from the hightest level of scope 
******************************************************************************/
void Database::ExitScope(){
  InsideSubFunction = false;
  Scope.erase(ScopeLevel);
  ScopeLevel--;
  return;
}

/******************************************************************************
GetScopeID - get the unique scope ID for the current scope
******************************************************************************/
string Database::GetScopeID(){
  string Result;
  if (Mangle == true){
    Result = "Scope" + ToStr(UniqueScopeID) + "__";
  }
  else{
    Result = "";
  }
  return Result;
}

/******************************************************************************
SetAsmName - sets the assembly language name for simple data types and UDTs
******************************************************************************/
void Database::SetAsmName(string Name, string AsmName){
  for (int Level = ScopeLevel; Level >=0; Level--){
    if (Scope[Level].SimpleDataTypes.find(Name) != 
        Scope[Level].SimpleDataTypes.end()){
      Scope[Level].SimpleDataTypes[Name].AsmName = AsmName;
      return;
    }
    if (Scope[Level].UDTDataTypes.find(Name) != 
        Scope[Level].UDTDataTypes.end()){
      Scope[Level].UDTDataTypes[Name].AsmName = AsmName;
      return;
    }
  }
}

/******************************************************************************
GetTypeData - returns information related to types
******************************************************************************/
TypeDataInfo& Database::GetTypeData(string Name){
  for (int Level = ScopeLevel; Level >=0; Level--){
    if (Scope[Level].Types.find(Name) != Scope[Level].Types.end()){
      return Scope[Level].Types[Name];
    }
  }
}

/******************************************************************************
GetSizeOfParameters - gets the size of all the parameters of a function
******************************************************************************/
int Database::GetSizeOfParameters(string Name){
  for (int Level = ScopeLevel; Level >=0; Level--){
    if (Scope[Level].SubFunctions.find(Name) != 
        Scope[Level].SubFunctions.end()){
      return Scope[Level].SubFunctions[Name].SizeOfParameters;
    }
  }
}

/******************************************************************************
IsInsideSubFunction - are we inside a subroutine or function?
******************************************************************************/
bool Database::IsInsideSubFunction(){
  return InsideSubFunction;
}

/******************************************************************************
AddLocalVariable - adds a local variable to a function
******************************************************************************/
string Database::AddLocalVariable(int Type){
  ScopeLevel--;
  if (InsideSubFunction == true){
    if (Scope[ScopeLevel].SubFunctions.find(LastSubFunction) != 
        Scope[ScopeLevel].SubFunctions.end()){
      if (Type == Double){
        Scope[ScopeLevel].SubFunctions[LastSubFunction].SizeOfLocalVariables-=8;
      }
      else{
        Scope[ScopeLevel].SubFunctions[LastSubFunction].SizeOfLocalVariables-=4;      
      }
      string Size = ToStr(Scope[ScopeLevel].SubFunctions[LastSubFunction].
                          SizeOfLocalVariables);
      ScopeLevel++;
      return Size;
    }
  }
  ScopeLevel++;
}

/******************************************************************************
AddExternalSubFunctionData - adds information relating to an external function
(WinAPI) to the external function.
******************************************************************************/
void Database::AddExternalSubFunctionData(string Name, string Library, 
                                          string Alias, string CallConv){
  ScopeLevel--;
  Scope[ScopeLevel].SubFunctions[Name].External = true;  
  Scope[ScopeLevel].SubFunctions[Name].ExternalInfo.Library = Library;  
  Scope[ScopeLevel].SubFunctions[Name].ExternalInfo.Alias = Alias;  
  Scope[ScopeLevel].SubFunctions[Name].ExternalInfo.CallingConv = CallConv;  
  ScopeLevel++;
  return;
}

/******************************************************************************
SetParameterPoolSizeFilled - sets how much of the parameter pool is filled
******************************************************************************/
void Database::SetParameterPoolSizeFilled(string Name, int HowMuch){
  for (int Level = ScopeLevel; Level >=0; Level--){
    if (Scope[Level].SubFunctions.find(Name) !=Scope[Level].SubFunctions.end()){
      Scope[Level].SubFunctions[Name].ParameterPoolFilled = HowMuch;
      return;
    }
  }
}

/******************************************************************************
GetParameterPoolSizeFilled - gets how much of the parameter pool is filled
******************************************************************************/
int Database::GetParameterPoolSizeFilled(string Name){
  for (int Level = ScopeLevel; Level >=0; Level--){
    if (Scope[Level].SubFunctions.find(Name) !=Scope[Level].SubFunctions.end()){
      return Scope[Level].SubFunctions[Name].ParameterPoolFilled;
    }
  }
}

/******************************************************************************
UsingSubFunction - does a thourough check to see what other functions the 
specified function uses. Allows KoolB to cull out unused functions to reduce
the size of the program
******************************************************************************/
void Database::UsingSubFunction(string Name){
  if (InsideSubFunction == true){
    for (int Level = ScopeLevel; Level >=0; Level--){
      if (Scope[Level].SubFunctions.find(InsideSubFunctionName) != 
          Scope[Level].SubFunctions.end()){
        for (int NextLevel = ScopeLevel; NextLevel >=0; NextLevel--){
          if (Scope[NextLevel].SubFunctions.find(Name) != 
              Scope[NextLevel].SubFunctions.end()){
            Scope[Level].SubFunctions[InsideSubFunctionName].
            UsesSubFunctions[Name] = Scope[NextLevel].SubFunctions[Name];
          }
        }        
      }
    }
  }
  else{  
    for (int Level = ScopeLevel; Level >=0; Level--){
      if (Scope[Level].SubFunctions.find(Name) != 
          Scope[Level].SubFunctions.end()){
        map <string, SubFunctionInfo>::iterator UseSubFunction;
        Scope[Level].SubFunctions[Name].Used = true;
        UseSubFunction = 
                       Scope[Level].SubFunctions[Name].UsesSubFunctions.begin();
        while(UseSubFunction != 
              Scope[Level].SubFunctions[Name].UsesSubFunctions.end()){
          if (UseSubFunction->second.Used == false){
            UsingSubFunction(UseSubFunction->first);
          }
          UseSubFunction++;
        }
        UsedSubFunctions += "%define " + StripJunkOff(Name) + "_Used\r\n";
        return;
      }
    }
  }
  return;
}

/******************************************************************************
GetUsedSubFunctions - returns the code snippet containing used functions
******************************************************************************/
string Database::GetUsedSubFunctions(){
  return UsedSubFunctions;
}

/******************************************************************************
IsFunctionImported - checks to see if a function is external
******************************************************************************/
bool Database::IsFunctionImported(string Name){
  if (find(ImportedFunctions.begin(), ImportedFunctions.end(), Name) == 
      ImportedFunctions.end()){
    ImportedFunctions.push_back(Name);
    return false;
  }
  return true;
}

/******************************************************************************
AddDefine - adds a define to the database
******************************************************************************/
void Database::AddDefine(string Define){
  if (find(Defined.begin(), Defined.end(), Define) == Defined.end()){
    Defined.push_back(Define);
  }
}

/******************************************************************************
IsDefined - checks to see if a word is defined
******************************************************************************/
bool Database::IsDefined(string Define){
  if (find(Defined.begin(), Defined.end(), Define) != Defined.end()){
    return true;
  }
  return false;
}

/******************************************************************************
EnterDirective - adds a new level of directives (like nested $IfDefs)
******************************************************************************/
void Database::EnterDirective(){
  DirectiveLevel++;
}

/******************************************************************************
CanExitDirective - can we exit directives or is their one still pending
******************************************************************************/
bool Database::CanExitDirective(){
  if (DirectiveLevel > 0){
    return true;
  }
  return false;
}

/******************************************************************************
ExitDirective - exits a directive level
******************************************************************************/
void Database::ExitDirective(){
  DirectiveLevel--;
}

/******************************************************************************
GetDirectiveLevel - checks to see on what level of directive we are in
******************************************************************************/
int Database::GetDirectiveLevel(){
  return DirectiveLevel;
}

/******************************************************************************
SetDirectiveLevel - very useful for setting directive levels when moving between
files
******************************************************************************/
void Database::SetDirectiveLevel(int Level){
  DirectiveLevel = Level;
  return;
}

/******************************************************************************
ListFunctions - lists all the user-defined functions in the file so they can
be exported from a DLL
******************************************************************************/
string Database::ListFunctions(){
  string Functions = "Exit";
  map<string, SubFunctionInfo>::const_iterator Item;
  Item = Scope[ScopeLevel].SubFunctions.begin();
  while(Item != Scope[ScopeLevel].SubFunctions.end()){
    if (Item->second.External == false){
      Functions += "," + Item->second.Name;
    }
    Item++;
  }
  return Functions;
}

#endif
