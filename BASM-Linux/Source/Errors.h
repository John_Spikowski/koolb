/******************************************************************************
 *|--------------------------------------------------------------------------|*
 *| BASM Linux Compiler - Fork of KoolB Version 15.02 by Brian C. Becker     |*
 *| 2014-12-01 - Armando I. Rivera (AIR) initial contributor                 |*
 *|                                                                          |*
 *| Errors.h - A bunch of routines to format and display an error            |*
 *|--------------------------------------------------------------------------|*
 *****************************************************************************/
 
#ifndef Errors_H
#define Errors_H 
 
//Error object 
class Errors{
 private:
   string Book;          //The source code to the program that has an error
   string BookName;      //The filename of the program that has an error
   long   BookMark;      //Our current position in the file
   long   CurrentLine;   //The current line 
   string CurrentWord;   //The word or token in error
   int    TypeOfWord;    //The type of toke in error
   string CodeLine;      //The whole line of code that is in error
   long   ErrorLength;   //The position where the word in error begings

 public:
  //Core routines to format the error
  void Prepare(Reading SourceFile);
  void PrintError(string Cause);
  void PrintLocation();
  void PrintCause(string Cause);
  void PrintCode();
  void PrintErrorPosition();

  /****************************************************************************
  Individual error routines:
  Since these functions are pretty self-explanitory (especially after you
  read the errors that they print out, I will not comment further
  ****************************************************************************/
  void EndOfLine(Reading SourceFile);
  void BadStatement(Reading SourceFile);
  void BadName(Reading SourceFile);
  void ExpectedNameAfterDIM(Reading SourceFile);
  void BadType(Reading SourceFile);
  void NoType(Reading SourceFile);
  void ExpectedAs(Reading SourceFile);
  void AlreadyReserved(Reading SourceFile);
  void OnlyNumbersForArrays(Reading SourceFile);
  void OnlyIntegersForArrays(Reading SourceFile);
  void ExpectedEndingParenthesis(Reading SourceFile);
  void ExpectedNameAfterTYPE(Reading SourceFile);
  void UnfinishedType(string TypeName, Reading SourceFile);
  void ExpectedName(Reading SourceFile);
  void AlreadyReservedInType(Reading SourceFile);
  void ExpectedAssignment(Reading SourceFile);
  void ExpectedArray(Reading SourceFile);
  void ExpectedUDT(Reading SourceFile);
  void CanOnlyAssign(Reading SourceFile);
  void ExpectedStringData(Reading SourceFile);
  void ExpectedNumberData(Reading SourceFile);
  void ExpectedArrayData(Reading SourceFile);
  void TypesDoNotMatch(Reading SourceFile);
  void UnmatchedParenthesis(Reading SourceFile);
  void UnknownExpression(Reading SourceFile);
  void UndeclaredVariable(Reading SourceFile);
  void ExpectedPeriod(Reading SourceFile);
  void InvalidMember(Reading SourceFile);
  void UnableToAccessMember(Reading SourceFile);
  void UnknownExpressionType(Reading SourceFile);
  void ExpectedEndingArrayParenthesis(Reading SourceFile);
  void UnableToAccessItem(Reading SourceFile);
  void ExpectedParenthesis(Reading SourceFile);
  void ExpectedSeparatorForInput(Reading SourceFile);
  void ExpectedThen(Reading SourceFile);
  void NoEndIf(Reading SourceFile);
  void NoDo(Reading SourceFile);
  void ExpectedParameters(Reading SourceFile);
  void AlreadyReservedInParameters(Reading SourceFile);
  void NoEndToParameters(Reading SourceFile);
  void NoEndSubFunction(Reading SourceFile);
  void MustEndIfStatementFirst(Reading SourceFile);
  void MustEndWhileLoopFirst(Reading SourceFile);
  void EndInWrongPlace(Reading SourceFile);
  void MismatchedSubFunction(Reading SourceFile);
  void SubFunctionMustBeGlobal(Reading SourceFile);
  void ExpectedNextParameter(Reading Sourcefile);
  void InvalidLibrary(Reading SourceFile);
  void InvalidAlias(Reading SourceFile);
  void InvalidCallingConvention(Reading SourceFile);
  void ExpectedExternalSubFunction(Reading SourceFile);
  void ExpectedSubFunction(Reading SourceFile);
  void ExpectedFileNameAfterInclude(Reading SourceFile);
  void NoEndToAsm(Reading SourceFile);
  void ExpectedEndOfLineAfterAsm(Reading SourceFile);
  void InvalidDirective(Reading SourceFile);
  void InvalidAppType(Reading SourceFile);
  void CannotChangeAppType(Reading SourceFile);
  void CannotOptimize(Reading SourceFile);
  void ExpectedOnOrOff(Reading SourceFile);
  void DefineMustBeIdentifier(Reading SourceFile);
  void NoEndToIfDef(Reading SourceFile);
  void CannotExitDirective(Reading SourceFile);
  void CannotModifyConstData(Reading SourceFile);
  void ExpectedEndAsm(Reading SourceFile);
  void ConstDataMustBeNumberOrString(Reading SourceFile);
  void CanOnlyNegateNumbers(Reading SourceFile);  
  void ExpectedVariable(Reading SourceFile);  
};

/******************************************************************************
Prepare - queries information from the Reading object, so we can format the 
error
******************************************************************************/
void Errors::Prepare(Reading SourceFile){
  CurrentWord = SourceFile.Word();
  TypeOfWord  = SourceFile.WordType();
  Book        = SourceFile.GetBook();
  BookName    = SourceFile.GetBookName();
  BookMark    = SourceFile.GetBookMark();
  CurrentLine = SourceFile.GetCurrentLine();
  return;
}

/******************************************************************************
PrintError - The core routine that directs the whole show
******************************************************************************/
void Errors::PrintError(string Cause){
  PrintLocation();
  PrintCause(Cause);
  PrintCode();
  PrintErrorPosition();
  return;
}

/******************************************************************************
PrintLocation - prints the file and line where the error occurs
******************************************************************************/
void Errors::PrintLocation(){
  //If we are at the very end of a line, the CurrentLine variable has already
  //turned over, so need to turn it back one
  if (CurrentWord == "\r\n"){
    CurrentLine--;
  }
  printf("\nError in file \"%s\" on line %lu:\r\n", BookName.c_str(), CurrentLine);
  return;
}

/******************************************************************************
PrintCause - prints out the actual error generated by the compiler
******************************************************************************/
void Errors::PrintCause(string Cause){
  long Position;
  //Replace "" (no token) with the more descriptive End-Of-File
  Position = Cause.find("\"\"", 0);
  if (Position != string::npos){
    Cause.replace(Position, 2, "End-Of-File");
  }
  //Replace \n with the more descriptive End-Of-Line
  Position = Cause.find("\"\n\"", 0);
  if (Position != string::npos){
    Cause.replace(Position, 3, "End-Of-Line");
  }
  printf("  Cause: %s\r\n", Cause.c_str());
  return;
}

/******************************************************************************
PrintCode - extracts and prints the line that the error occurs on
******************************************************************************/
void Errors::PrintCode(){
  long Position = BookMark - 1;
  long StartPos;
  long EndPos;
  //If the current position is on a newline, back up one to avoid blank lines
  if (CurrentWord == "\r\n"){
    Position -= 1;
  }
  //Find the last newline
  StartPos = Book.rfind("\r", Position);
  //If there is no last newline, we must be on the first line of the file
  if (StartPos == string::npos){
     StartPos = 0;
  }
  else{
    //Now skip over that newline, because we want the line after it
    StartPos += 1;
  }
  //Find the next newline so we can get the line inbetween
  EndPos = Book.find("\r", Position);
  //If there is no newline, we must be at the end of the file
  if (EndPos == string::npos){
    EndPos = Book.length();
  }
  //Extract the line inbetween the two lines
  CodeLine    = Book.substr(StartPos, EndPos - StartPos);
  //Calculate where the word in error begins
  ErrorLength = BookMark - StartPos - CurrentWord.length();
  //If CurrentWord is a newline, skip past it
  if (CurrentWord == "\r\n"){
    ErrorLength++;
  }
  printf("  Line of code in error:\r\n");
  printf("    %s\r\n", CodeLine.c_str());
  return;
}

/******************************************************************************
PrintErrorPosition - puts little ^^^^s under the word in error
******************************************************************************/
void Errors::PrintErrorPosition(){
  int Position = 0;
  printf("    ");
  //Print out whitespace until we get to the beginning of the word in error
  while (Position < ErrorLength){
    //If there is a tab in the line, we need to mirror that
    if (CodeLine[Position] == '\t'){
      printf("\t");
    }
    //Otherwise, just print out a space
    else{
      printf(" ");
    }
    Position++;
  }
  //Make sure we have at least 1 ^
  Position = 1;
  //Now, for each character of the word in error, but a ^ beneath it
  while (Position < (int)CurrentWord.length()){
    printf("^");
    Position++;
  }
  printf("^--Error!\r\n");
  return;
}

void Errors::EndOfLine(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Expected the end of a statement (End-Of-Line),"
             " but got \"" + CurrentWord + "\"");
  exit(1);
  return;
}

void Errors::BadStatement(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Expected the beginning of a statement,"
             " but got \"" + CurrentWord + "\"");
  exit(1);
  return;
}

void Errors::AlreadyReserved(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("\"" + CurrentWord + "\" is already reserved,"
             " please choose a different name.");
  exit(1);
  return;
}

void Errors::BadType(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("\"" + CurrentWord + "\" is an unknown data type,"
             " please choose a valid data type.");
  exit(1);
  return;
}

void Errors::NoType(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("\"" + CurrentWord + "\" has no data type identifier,"
             " please add a valid data type like $ to the end.");
  exit(1);
  return;
}

void Errors::BadName(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("\"" + CurrentWord + "\" is not a valid name, please "
             "choose use only letters and numbers.");
  exit(1);
  return;
}

void Errors::ExpectedNameAfterDIM(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Expected the variable\'s name after \"DIM\", but "
             "got \"" + CurrentWord + "\"");
  exit(1);
  return;
}

void Errors::ExpectedAs(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Expected the word \"AS\" after the data\'s name,"
             " but got \"" + CurrentWord + "\"");
  exit(1);
  return;
}

void Errors::OnlyNumbersForArrays(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("The only valid data type to set the size of an"
             " array is numbers without any decimals.");
  exit(1);
  return;
}

void Errors::OnlyIntegersForArrays(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("\"" + CurrentWord + "\" has a decimal point,"
             " please use only whole numbers.");
  exit(1);
  return;
}

void Errors::ExpectedEndingParenthesis(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Expected an ending parenthesis (\")\"),"
             " but got \"" + CurrentWord + "\" instead.");
  exit(1);
  return;
}

void Errors::ExpectedNameAfterTYPE(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Expected to see a name for your UDT ,"
             " but got \"" + CurrentWord + "\" instead.");
  exit(1);
  return;
}

void Errors::UnfinishedType(string TypeName, Reading SourceFile){
  Prepare(SourceFile);
  PrintError("The UDT \"" + TypeName + "\" is unfinished,"
             " please use END TYPE to end it. Thanks!");
  exit(1);
  return;
}

void Errors::ExpectedName(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Expected valid name,"
             " but got \"" + CurrentWord + "\" instead.");
  exit(1);
  return;
}

void Errors::AlreadyReservedInType(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("The variable name \"" + CurrentWord + "\""
             " has already been used in this type.");
  exit(1);
  return;
}

void Errors::ExpectedAssignment(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Expected you to assign something to the variable, but"
             " got \"" + CurrentWord + "\" instead of an equals sign.");
  exit(1);
  return;
}

void Errors::ExpectedArray(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Expected an array in assignment. You cannot assign"
             " anything else to an array.");
  exit(1);
  return;
}

void Errors::ExpectedUDT(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Expected an UDT in assignment. You cannot assign"
             " anything else to an UDT.");
  exit(1);
  return;
}

void Errors::CanOnlyAssign(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("In this expression, you only assign, not add, subtract, etc."
             " For example, with types, arrays, and classes, you can only assign.");
  exit(1);
  return;
}

void Errors::ExpectedStringData(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Expected string data in expression, but"
             " got a different type instead.");
  exit(1);
  return;
}

void Errors::ExpectedNumberData(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Expected number data in expression, but"
             " got a different type instead.");
  exit(1);
  return;
}

void Errors::ExpectedArrayData(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Expected an array in the expression, but"
             " got a different type instead.");
  exit(1);
  return;
}

void Errors::TypesDoNotMatch(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("This array\'s or UDT\'s type is mismatched."
             " Please make sure that both have the same type.");
  exit(1);
  return;
}

void Errors::UnmatchedParenthesis(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("The expression to the right has missing parenthesis. Please"
             " correct this by adding matching parenthesis.");
  exit(1);
  return;
}

void Errors::UnknownExpression(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("This part of the expression is invalid.");
  exit(1);
  return;
}

void Errors::UndeclaredVariable(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Variable " + CurrentWord + " has not been declared yet."
             " You cannot autocreate a variable in an expression.");
  exit(1);
  return;
}

void Errors::ExpectedPeriod(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Expected a period after the UDT to access the member."
             " Please use a period.");
  exit(1);
  return;
}

void Errors::InvalidMember(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("The member " + CurrentWord + " does not exist in "
             " specified type. Perhaps you misspelled it.");
  exit(1);
  return;
}

void Errors::UnableToAccessMember(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Since this variable is not an UDT, you cannot"
             " access any member variables.");
  exit(1);
  return;
}

void Errors::UnknownExpressionType(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Cannot determine the type of this expression.");
  exit(1);
  return;
}

void Errors::ExpectedEndingArrayParenthesis(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Ending parenthesis for the accessing the "
             "array item was not found.");
  exit(1);
  return;
}

void Errors::UnableToAccessItem(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Since this variable is not an array, you cannot"
             " access any items from the array.");
  exit(1);
  return;
}

void Errors::ExpectedParenthesis(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("You must use parenthesises to access items"
             " in an array.");
  exit(1);
  return;
}

void Errors::ExpectedSeparatorForInput(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("The INPUT command requires a separatore between"
             " the prompt string and the storage variable.");
  exit(1);
  return;
}

void Errors::ExpectedThen(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Expected a THEN in the IF...THEN...END IF statement, "
             "but got \"" + CurrentWord + "\" instead.");
  exit(1);
  return;
}

void Errors::NoEndIf(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("One or more IF...THEN...END IF statements is "
             "missing the terminating \"END IF\" command.");
  exit(1);
  return;
}

void Errors::NoDo(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Your WHILE loop is missing a DO after the condition."
             "Please add it.");
  exit(1);
  return;
}

void Errors::ExpectedParameters(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Expected a list of parameters, not \"" + CurrentWord + "\"."
             "If your function/sub has no parameters, use () .");
  exit(1);
  return;
}

void Errors::AlreadyReservedInParameters(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("\"" + CurrentWord + "\" is already reserved in parameters."
             " Please choose a different name for this parameter.");
  exit(1);
  return;
}

void Errors::NoEndToParameters(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("There is no end to the parameters for this sub or function"
             "Please use an ending parenthesis to signal the end.");
  exit(1);
  return;
}

void Errors::NoEndSubFunction(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("One or more of your Subs or Functions is missing "
             "a terminating End Sub or End Function statement.");
  exit(1);
  return;
}

void Errors::MustEndIfStatementFirst(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("You must end any and all If statements before"
             " ending this sub or function.");
  exit(1);
  return;
}

void Errors::MustEndWhileLoopFirst(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("You must end any and all While loops before"
             " ending this sub or function.");
  exit(1);
  return;
}

void Errors::EndInWrongPlace(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("The END keyword can only appear in Subs, "
             " Functions, or If statements.");
  exit(1);
  return;
}

void Errors::MismatchedSubFunction(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("It appears that your sub has an End Function or "
             "your function has an End Sub - please correct this.");
  exit(1);
  return;
}

void Errors::SubFunctionMustBeGlobal(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Subs and functions cannot be created inside "
             "subs, functions, if statements, or while loops.");
  exit(1);
  return;
}

void Errors::ExpectedNextParameter(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Expected to see more paramters separated by a comma.");
  exit(1);
  return;
}

void Errors::InvalidLibrary(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Invalid library - you must enclose library names in quotation marks.");
  exit(1);
  return;
}

void Errors::InvalidAlias(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Invalid alias - you must enclose the alias name in quotation marks.");
  exit(1);
  return;
}

void Errors::InvalidCallingConvention(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Invalid calling convention - the calling convention may be either 'C' or 'STD'.");
  exit(1);
  return;
}

void Errors::ExpectedExternalSubFunction(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Exptected an external sub or function. You cannot DECLARE"
             " user-defined subs or functions.");
  exit(1);
  return;
}

void Errors::ExpectedSubFunction(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Expected either Sub or Function after a DECLARE or"
             " as the parameter for CALLBACK or CODEPTR.");
  exit(1);
  return;
}

void Errors::ExpectedFileNameAfterInclude(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Expected a string containing the filename "
             " of the BASIC file to include.");
  exit(1);
  return;  
}

void Errors::NoEndToAsm(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("No $End Asm statement was found before the "
             " end of the file.");
  exit(1);
  return;  
}

void Errors::ExpectedEndOfLineAfterAsm(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Expected an End-Of-Line after the $Asm statement.");
  exit(1);
  return;  
}

void Errors::InvalidDirective(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("This directive is invalid - valid directives include:"
             "$Asm, $AppType, $Include.");
  exit(1);
  return;  
}

void Errors::InvalidAppType(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("The AppType you choose is invalid - valid "
             "types are: Console, GUI, DLL.");
  exit(1);
  return;  
}

void Errors::CannotChangeAppType(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("You cannot change the AppType this far into the "
             "program, you must use $AppType before any other statement.");
  exit(1);
  return;  
}

void Errors::CannotOptimize(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("You cannot optimize an app this far into the "
             "program, you must use $Optimze before other statements.");
  exit(1);
  return;  
}

void Errors::ExpectedOnOrOff(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("The $Optimize and $Compress directives expect either "
             "ON or OFF, not \"" + CurrentWord + "\".");
  exit(1);
  return;  
}


void Errors::DefineMustBeIdentifier(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Defined words must be identifiers, not numbers or symbols.");
  exit(1);
  return;  
}

void Errors::NoEndToIfDef(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("There is a missing $End If to one or more of your $IfDef or $IfNDef statements.");
  exit(1);
  return;  
}

void Errors::CannotExitDirective(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("You have more $End If statements than $IfDef or $IfNDef statements. "
             "You must balance them out so the # of each is equal.");
  exit(1);
  return;  
}

void Errors::CannotModifyConstData(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Const data may not be modified or assigned "
             " another value.");
  exit(1);
  return; 
}

void Errors::ExpectedEndAsm(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Expected End Asm after $ in an $Asm session. You may not use"
             " directives in an $Asm session.");
  exit(1);
  return; 
}

void Errors::ConstDataMustBeNumberOrString(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("$Const data must be literal numbers (like 2) or strings"
             " (like \"Hello\").");
  exit(1);
  return; 
}

void Errors::CanOnlyNegateNumbers(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("You can only negate numbers, not strings.");
  exit(1);
  return; 
}

void Errors::ExpectedVariable(Reading SourceFile){
  Prepare(SourceFile);
  PrintError("Expected the parameter to be a variable.");
  exit(1);
  return; 
}

#endif
