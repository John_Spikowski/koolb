/******************************************************************************
 *|--------------------------------------------------------------------------|*
 *| BASM Linux Compiler - Fork of KoolB Version 15.02 by Brian C. Becker     |*
 *| 2014-12-01 - Armando I. Rivera (AIR) initial contributor                 |*
 *|                                                                          |*
 *| Misc.h - A bunch of different functions needed by the compiler           |*
 *|--------------------------------------------------------------------------|*
 *****************************************************************************/
 
#ifndef Misc_h
#define Misc_h
 
/******************************************************************************
Run - Runs external programs - especially the assembler and linker
******************************************************************************/
void Run(string Command){
  ifstream File;
  int FileStart;
  int FileEnd;
  char * Spoon;
  double StartTime = clock() / CLOCKS_PER_SEC;
  system(Command.c_str());
  //Use different notations for the paths, depending on the OS
  #ifdef Windows
    File.open(".\\results.txt", ios::in);
    //Wait until results.txt can be opened (meaning the program has finished)
    while (File.is_open() == false){
      File.open(".\\results.txt", ios::in);
      //If it takes too long, exit
      if ((clock() / CLK_TCK) - StartTime > 120){
         printf("Time out running: %s\r\n", Command.c_str());
         exit(1);
      }
    }
  #endif
  #ifdef Linux
    File.open("./results.txt", ios::in);
    //Wait until results.txt can be opend (meaning the program has finished)
    while (File.is_open() == false){
      File.open("./results.txt", ios::in);
      //If it takes too long, exit
      if ((clock() / CLK_TCK) - StartTime > 120){
         printf("Time out running: %s\r\n", Command.c_str());
         exit(1);
         
      }
    }
  #endif
  FileStart = File.tellg();
  File.seekg(0, ios::end);
  FileEnd   = File.tellg();
  File.seekg(0, ios::beg);
  //Check size of results.txt. If it contains any info (size is not 0), then an 
  //error occured
  if (FileEnd - FileStart > 0){
    #ifdef Windows
      printf("Error: NASM, GoRC, or GoLink failed. Please attach error"
      " messages to bug repots:\r\n");
    #endif
    #ifdef Linux
      printf("Error: NASM or ld failed. Please attach error messages to"
      " bug repots:\r\n");
    #endif
    printf("Ran %s and got:\r\n", Command.c_str());
    Spoon = new char[FileEnd-FileStart];
    File.read(Spoon, FileEnd-FileStart);
    Spoon[FileEnd-FileStart-1] = '\0';
    printf("%s\r\n", Spoon);
    delete[] Spoon;
    exit(1);
  }
  File.close();
  return;
}

/******************************************************************************
ToStr - Convert a number to string
******************************************************************************/
string ToStr(int Number){
  string Result;
  char * Spoon;
  Spoon = new char[1024];
  sprintf(Spoon, "%i", Number);
  Result = Spoon;
  delete[] Spoon;
  return Result;
}

/******************************************************************************
ToLong - Convert a string to a number
******************************************************************************/
long ToLong(string String){
  long Result;
  Result = atoi(String.c_str());
  return Result;
}

/******************************************************************************
StripOffExtension - removes the 3 letter extension off the end of the file
For example, you pass it 'test.bas' and it will return 'test'
******************************************************************************/
string StripOffExtension(string FileName){
  int Length = FileName.length();
  for (int i = Length; i > 0; i--){
    if (FileName[i] == '\\' || FileName[i] == '/'){
      return FileName;
    }
    if (FileName[i] == '.'){
      return FileName.substr(0, i);
    }
  }
  return FileName;
}

/******************************************************************************
FileExits - tests to see if a file exists and can be opened.
******************************************************************************/
bool FileExists(string FileName){
  ifstream File(FileName.c_str(), ios::in);
  if (File.is_open() == false){
     return false;
  }
  File.close();
  return true;
}

/******************************************************************************
GetFileNameOnly - extracts only the file name from a complete path + filename
******************************************************************************/
string GetFileNameOnly(string FileName){
  int Length = FileName.length();
  for (int i = Length; i > 0; i--){
    if (FileName[i] == '\\' || FileName[i] == '/'){
      return FileName.substr(i, Length);
    }
  }
  return FileName;
}

/******************************************************************************
GetPathOnly - extracts only the path from a complete path + filename
******************************************************************************/
string GetPathOnly(string FileName){
  int Length = FileName.length();
  for (int i = Length; i > 0; i--){
    if (FileName[i] == '\\' || FileName[i] == '/'){
      return FileName.substr(0, i+1);
    }
  }
  return FileName;
}

/******************************************************************************
PatchFileNames - takes two complete paths to different files and combines them
******************************************************************************/
string PatchFileNames(string FileName1, string FileName2){
  return GetPathOnly(FileName1) + GetFileNameOnly(FileName2);
}

/******************************************************************************
Sleep - Pauses for the specified number of seconds
******************************************************************************/
void PatchFileNames(int Pause){
  int StartTime = clock();
  while ((clock() / CLOCKS_PER_SEC) - StartTime < Pause){/*Empty loop*/}
  return;
}

#endif
