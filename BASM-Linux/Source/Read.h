/******************************************************************************
 *|--------------------------------------------------------------------------|*
 *| BASM Linux Compiler - Fork of KoolB Version 15.02 by Brian C. Becker     |*
 *| 2014-12-01 - Armando I. Rivera (AIR) initial contributor                 |*
 *|                                                                          |*
 *| Read.h - Controls making words out of the source file                    |*
 *|--------------------------------------------------------------------------|*
 *****************************************************************************/
 
#ifndef Read_h
#define Read_h

//Holds information for the $Const directive
struct ConstData{
  string Value;
  int    Type;
};

//The reading object manages the lexing and tokenizing
class Reading{
 private:
   string Book;           //The whole file
   int    BookMark;       // Where we are in the book
   int    BookSize;       // How long is the book?
   string BookName;       //The filename
   long   BookLength;     //Length of the file
   long   CurrentLine;    //The line we are on in the file
   string CurrentWord;    //The current word to analyze
   int    TypeOfWord;     //The type of word the current word is
   bool   Uppercase;      //Should we convert the word to uppercase
   map<string, ConstData> Const; //A hash of $Const replacements
 public:
   //Initially, set position to 0, line to 1, and convert to uppercase
   Reading(){CurrentLine = 1; Uppercase = true;}
   ~Reading(){}
   
   void OpenBook(string FileName);
   void GetNextWord();
     bool SkipWhiteSpace();
     void GetIdentifier();
     void GetNumber();
     void GetString();
     void GetSymbol();
   void CheckWordLength();
   void SetUppercase(bool YesNo);
   string Word();
   int WordType();
   long GetBookMark();
   long GetCurrentLine();
   string GetBookName();
   string GetBook();
   string GetWholeLine();
   void AddConstData(string Name, string Value, int Type);
   bool IsConstData(string Name);

   //The different word types available
   enum WordTypes{EndOfLine, Identifier, Number, String, Symbol, None};

};

/******************************************************************************
OpenBook - opens the file for reading and copies the contents into Book
******************************************************************************/
void Reading::OpenBook(string FileName){
  char *   Spoon;
  ifstream File(FileName.c_str(), ios::in | ios::binary);
  long     FileStart;
  long     FileEnd;
  //If can't open the file, abort!
  if (File.is_open() == false){
     printf("Error! Could not open file: %s\r\n", FileName.c_str());
     exit(1);
  }
  //Get the size of the file
  FileStart = File.tellg();
  File.seekg(0, ios::end);
  FileEnd   = File.tellg();
  File.seekg(0, ios::beg);
  //Create a spoon big enough to read the file
  BookSize = FileEnd-FileStart;
  Spoon = new char[BookSize+2];
  memset(Spoon, '\n', BookSize+1);
  //Copy file into the spoon
  File.read(Spoon, BookSize);
  //Terminate the end of the buffer
  Spoon[BookSize] = '\0';
  // Copy the spoon into our book
  Book = Spoon;
  BookMark = 0;
  delete[] Spoon;
  BookName    = FileName;
  //Copy buffer into Book
  BookLength  = Book.length();
  //Don't want memory leaks!
  File.close();
  return;
} 

/******************************************************************************
GetNextWord - retrieves the next word from the file. It really only determines
the type of word and then passes the real job off to other routines
******************************************************************************/
void Reading::GetNextWord(){
  // Clear the current word
  CurrentWord = "";
  //Check to make sure we don't go beyond the end of the file
  //If we are at the end of the file, TypeOfWord should be None
  if (!Book[BookMark] || BookMark >= BookSize){
    TypeOfWord = None;
    return;
  }
  //White space doesn't matter, unless it is an End-Of-Line. If an End-Of-Line
  //is encountered, SkipWhiteSpace returns false, and GetNextWord returns (no 
  //need looking for more words!
  if (SkipWhiteSpace() == false){
    return;
  }
  //Check to make sure we don't go beyond the end of the file
  //If we are at the end of the file, TypeOfWord should be None
  if (!Book[BookMark]){
    TypeOfWord = None;
    return;
  }
  //If the first letter of the word is alphabetic, we have an identifier
  if (isalpha(Book[BookMark])){
    GetIdentifier();
    CheckWordLength();
    return;
  }
  //If the first letter of the word is a digit or a period, we have a number
  if (isdigit(Book[BookMark]) || Book[BookMark] == '.'){
    GetNumber();
    CheckWordLength();
    return;
  }
  //If the first letter is a quote ("), we have a string
  if (Book[BookMark] == '\"'){
    GetString();
    CheckWordLength();
    return;
  }
  //If the letter is punctuation, we have a symbol
  if (ispunct(Book[BookMark])){
    GetSymbol();
    CheckWordLength();
    return;
  }
  //If the first letter doesn't match, default to None
  TypeOfWord = None;
  return;
}

/******************************************************************************
SkipWhiteSpace - does exactly that. It culls out spaces, tabs, and so forth.
When it encounters a non-whitespace character, it returns true. If it 
encounters an End-Of-Line, return false, because all the whitespace wasn't 
skipped.
******************************************************************************/
bool Reading::SkipWhiteSpace(){
  //Clear out the current word
  CurrentWord = "";
  //Comments (anything after the ' character) is also whitespace
  while(isspace(Book[BookMark]) || Book[BookMark] == '\''){
    //If we have a comment, let's find either the end of the line or the end of 
    //the file
    if (Book[BookMark] == '\''){
      BookMark++;
      while(Book[BookMark] != '\n' && Book[BookMark]){
        BookMark++;
      }
    }
    //If we have an end of line character, return false
    if (Book[BookMark] == '\n'){
      CurrentWord = Book[BookMark];
      TypeOfWord  = EndOfLine;
      BookMark++;
      CurrentLine++;
      return false;
    }
    BookMark++;
  }
  return true;
}

/******************************************************************************
GetIdentifier - Gets an identifier (alphabetic, numeric, and the underscore)
******************************************************************************/
void Reading::GetIdentifier(){
  do{
    if (!Book[BookMark]){
      break;
    }
    if (Book[BookMark] == '_' && isalnum(Book[BookMark+1]) == false){
      printf("Error in file %s on line %lu:\r\n", BookName.c_str(), CurrentLine);
      printf("Underscores may be embeded inside variable names, not at the"
              " start or the end\r\n");
      exit(1);
    }
    //Convert to uppercase if that's what we want
    if (Uppercase == true){
      CurrentWord += toupper(Book[BookMark]);
    }
    else{
      //Primarily, we want to conserve the case when doing inline assembly
      CurrentWord += Book[BookMark];
    }
    BookMark++;
  }while(isalpha(Book[BookMark]) || isdigit(Book[BookMark]) || Book[BookMark] == '_');
  if (Book[BookMark] == '&' || Book[BookMark] == '#' || Book[BookMark] == '$'){
    CurrentWord += Book[BookMark];
    BookMark++;
  }
  TypeOfWord = Identifier;
  //If the word is a constant, do replacement
  if (IsConstData(CurrentWord) == true){
    TypeOfWord  = Const[CurrentWord].Type;
    CurrentWord = Const[CurrentWord].Value;
  }
  return;
}

/******************************************************************************
GetNumber - gets either a decimal or hex number
******************************************************************************/
void Reading::GetNumber(){
  bool ReachedDot = false;
//  bool IsHex      = false;
  do{
/*    //Check to see if it is a hex number (0x perfix)
    if (toupper(Book[BookMark]) == 'X'){
      if (IsHex){
        cout << "Error on line: " << CurrentLine << endl;
        cout << "This number already is a hex number, no need for 0x prefix!"
             << endl;
        exit(1);        
      }
      if (CurrentWord[0] == '0'){
        IsHex = true;
      }
      else{
        cout << "Error on line: " << CurrentLine << endl;
        cout << "This number is not a hex number, use 0x prefix!"
             << endl;
        exit(1);
      }
    }
    //Enforce the rules for hexidecimal numbers
    if (toupper(Book[BookMark]) == 'A' || toupper(Book[BookMark]) == 'B' ||
        toupper(Book[BookMark]) == 'C' || toupper(Book[BookMark]) == 'D' || 
        toupper(Book[BookMark]) == 'E' || toupper(Book[BookMark]) == 'F'){
      if (IsHex == false){
        cout << "Error on line: " << CurrentLine << endl;
        cout << Book[BookMark] << toupper(Book[BookMark])  << endl;
        cout << "This number is not a hex number, use 0x prefix!"
             << endl;
        exit(1);
      }
    }*/
    //Deal with the period - make sure we don't have two periods 
    if (Book[BookMark] == '.'){
      if (ReachedDot == true){
        printf("Error on line: %lu\r\n", CurrentLine);
        printf("You cannot have more than one decimal point in a number!\r\n");
        exit(1);
      }
      ReachedDot = true;
    }
    CurrentWord += Book[BookMark];
    BookMark++;
  }while(isdigit(Book[BookMark]) || Book[BookMark] == '.' /*|| 
         toupper(Book[BookMark]) == 'X' || toupper(Book[BookMark]) == 'A' || 
         toupper(Book[BookMark]) == 'B' || toupper(Book[BookMark]) == 'C' || 
         toupper(Book[BookMark]) == 'D' || toupper(Book[BookMark]) == 'E' || 
         toupper(Book[BookMark]) == 'F'*/);
  //If we have a single period, then we have a symbol, not a number
  if (CurrentWord.length() == 1 && ReachedDot == true){
    TypeOfWord = Symbol;
  }
  else{
    TypeOfWord  = Number;
  }
/*  //If we have a hex number, convert it to decimal
  if (IsHex){
    int Hex;
    sscanf(CurrentWord.c_str(), "%x", &Hex);
    CurrentWord = ToStr(Hex);
  }*/
  return;
}

/******************************************************************************
GetString - retrieves a whole string
******************************************************************************/
void Reading::GetString(){
  BookMark++;
  while (Book[BookMark] != '\"'){
    CurrentWord += Book[BookMark];
    BookMark++;
    //If we encounter either the end of the file or the end of the line, report
    //error
    if (!Book[BookMark] || Book[BookMark] == '\n'){
        printf("Error on line: %lu\r\n", CurrentLine);
        printf("An end to the string does not exist!\r\n");
        exit(1);
      }
  }
  BookMark++;
  TypeOfWord = String;
  return;
}

/******************************************************************************
GetSymbol - Gets a single symbol from the file. Also deals with _ and the :
symbols. Also get the two character symbols like <>, <=, and >=
******************************************************************************/
void Reading::GetSymbol(){
  //If we have a _, continue on the next line, and then get the next word
  if (Book[BookMark] == '_'){
     Book[BookMark++];
     if(SkipWhiteSpace() == true){
        printf("Error on line: %lu\r\n", CurrentLine);
        printf("A newline should follow _\r\n");
        exit(1);
     }
     GetNextWord();
     return;
  }
  //The : serves as an end of line marker, so make it that type
  if (Book[BookMark] == ':'){
    CurrentWord = ':';
    BookMark++;
    TypeOfWord = EndOfLine;
    return;
  }
  CurrentWord = Book[BookMark];
  BookMark++;
  //Make sure that we get two character symbols
  if (CurrentWord == "<"){
    if (Book[BookMark] == '=' || Book[BookMark] == '>'){
      CurrentWord += Book[BookMark];
      BookMark++;
    }
  }
  if (CurrentWord == ">"){
    if (Book[BookMark] == '='){
      CurrentWord += Book[BookMark];
      BookMark++;
    }
  }
  TypeOfWord  = Symbol;
  return;
}

/******************************************************************************
CheckWordLength - The assembler can't handle words longer than 255 characters,
so make sure that the words aren't too long. For KoolB, the limit is 128 
characters (in case we need the other 128 characters for name mangling)
******************************************************************************/
void Reading::CheckWordLength(){
  if (CurrentWord.length() > 128 && TypeOfWord != String){
        printf("Error on line: %lu\r\n", CurrentLine);
        printf("A word cannot exceed 128 characters.\r\n");
        exit(1);
  }
  return;
}

/******************************************************************************
Word - a quick way to get the current word
******************************************************************************/
string Reading::Word(){
  return CurrentWord;
}

/******************************************************************************
GetWordType - a quick way to check the type of word
******************************************************************************/
int Reading::WordType(){
  return TypeOfWord;
}

/******************************************************************************
GetBookMark - a quick way to get the position in the file
******************************************************************************/
long Reading::GetBookMark(){
  return BookMark;
}

/******************************************************************************
GetCurrentLine - a quick way to get the current line
******************************************************************************/
long Reading::GetCurrentLine(){
  return CurrentLine;
}

/******************************************************************************
GetBookName - a quick way to get the name of the file being compiled
******************************************************************************/
string Reading::GetBookName(){
  return BookName;
}

/******************************************************************************
GetBook - a quick way to get the whole contents of the file
******************************************************************************/
string Reading::GetBook(){
  return Book;
}

/******************************************************************************
SetUppercase - toggle uppercase on and off
******************************************************************************/
void Reading::SetUppercase(bool YesNo){
  Uppercase = YesNo;
  return;
}

/******************************************************************************
GetWholeLine - gets a the current line for error reporting
******************************************************************************/
string Reading::GetWholeLine(){
  string Line = "";
  while (Book[BookMark]){
    if (Book[BookMark] == '\n'){
      BookMark++;
      break;
    }
    Line += Book[BookMark];
    BookMark++;
  }
  return Line;
}

/******************************************************************************
AddConstData - adds a constant as defined by $Const
******************************************************************************/
void Reading::AddConstData(string Name, string Value, int Type){
  Const[Name].Value = Value;
  Const[Name].Type  = Type;
  return;
}

/******************************************************************************
IsConstData - checks to see if a word is defined as a constant
******************************************************************************/
bool Reading::IsConstData(string Name){
  if (Const.find(Name) != Const.end()){
    return true;
  }
  return false;
}

#endif
