;Library functions to import to the Chameleon app
extern ExitProcess
extern MessageBoxA
extern lstrcat
extern lstrlen
extern lstrcpy
extern HeapCreate
extern HeapAlloc
extern HeapFree
extern FreeLibrary
extern floor
extern lstrlenA
extern lstrcpyA
extern LoadLibraryA
extern GetProcAddress
extern RtlMoveMemory
extern lstrcatA
extern lstrcmpA
extern WriteFile
extern _gcvt
extern GetModuleHandleA
extern GetCommandLineA
extern GetStdHandle
extern Sleep
extern HeapDestroy



;Initialize everything to prep the app to run
section .text
%include "C:\BASM\Bin\MACROS.INC"
global START
START:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH ESI
PUSH EDI
stdcall HeapCreate,0,16384,0
MOV dword[HandleToHeap],EAX
CMP dword[HandleToHeap],0
JNE Label1
JMP NoMemory
Label1:
stdcall GetModuleHandleA,0
MOV dword[Internal_HInstance],EAX
CALL GetCommandLineA
MOV dword[Internal_CommandLine],EAX
stdcall GetStdHandle,-10
MOV dword[HandleToInput],EAX
stdcall GetStdHandle,-11
MOV dword[HandleToOutput],EAX
CMP dword[HandleToInput],-1
JNE Label502
JMP NoConsole
Label502:
CMP dword[HandleToOutput],-1
JNE Label503
JMP NoConsole
Label503:



;The main body where the app actually runs
MOV EBX,String_132
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label504
JMP NoMemory
Label504:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_132
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_133+4]
PUSH dword[Number_133]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX



;Prepare the app to exit and then terminate
Exit:
stdcall FreeLibrary,dword[_BEGINPAINT_LibHandle]
stdcall FreeLibrary,dword[_ENDPAINT_LibHandle]
stdcall FreeLibrary,dword[_TEXTOUT_LibHandle]
stdcall FreeLibrary,dword[_DRAWTEXT_LibHandle]
stdcall FreeLibrary,dword[_ENUMCHILDWINDOWS_LibHandle]
stdcall FreeLibrary,dword[_GETTOPWINDOW_LibHandle]
stdcall FreeLibrary,dword[_GETNEXTWINDOW_LibHandle]
stdcall FreeLibrary,dword[_GETCLIENTRECT_LibHandle]
stdcall FreeLibrary,dword[_GETWINDOWRECT_LibHandle]
stdcall FreeLibrary,dword[_REGISTERCLASS_LibHandle]
stdcall FreeLibrary,dword[_REGISTERCLASSEX_LibHandle]
stdcall FreeLibrary,dword[_UNREGISTERCLASS_LibHandle]
stdcall FreeLibrary,dword[_GETMESSAGE_LibHandle]
stdcall FreeLibrary,dword[_TRANSLATEMESSAGE_LibHandle]
stdcall FreeLibrary,dword[_DISPATCHMESSAGE_LibHandle]
stdcall FreeLibrary,dword[_SENDMESSAGE_LibHandle]
stdcall FreeLibrary,dword[_LOADBITMAP_LibHandle]
stdcall FreeLibrary,dword[_LOADCURSOR_LibHandle]
stdcall FreeLibrary,dword[_LOADICON_LibHandle]
stdcall FreeLibrary,dword[_DESTROYICON_LibHandle]
stdcall FreeLibrary,dword[_GETCURSOR_LibHandle]
stdcall FreeLibrary,dword[_SETCURSOR_LibHandle]
stdcall FreeLibrary,dword[_COPYICON_LibHandle]
stdcall FreeLibrary,dword[_SETSYSTEMCURSOR_LibHandle]
stdcall FreeLibrary,dword[_CREATECOMPATIBLEDC_LibHandle]
stdcall FreeLibrary,dword[_CREATEWINDOWEX_LibHandle]
stdcall FreeLibrary,dword[_UPDATEWINDOW_LibHandle]
stdcall FreeLibrary,dword[_SHOWWINDOW_LibHandle]
stdcall FreeLibrary,dword[_DEFWINDOWPROC_LibHandle]
stdcall FreeLibrary,dword[_POSTQUITMESSAGE_LibHandle]
stdcall FreeLibrary,dword[_GETMODULEHANDLE_LibHandle]
stdcall FreeLibrary,dword[_GETACTIVEWINDOW_LibHandle]
stdcall FreeLibrary,dword[_EXITPROCESS_LibHandle]
stdcall FreeLibrary,dword[_MSGBOX_LibHandle]
stdcall FreeLibrary,dword[_MESSAGEBOX_LibHandle]
stdcall FreeLibrary,dword[_GETLASTERROR_LibHandle]
stdcall FreeLibrary,dword[_SETLASTERROR_LibHandle]
stdcall FreeLibrary,dword[_FORMATMESSAGE_LibHandle]
stdcall FreeLibrary,dword[_LOCALFREE_LibHandle]
stdcall FreeLibrary,dword[_GETWINDOWLONG_LibHandle]
stdcall FreeLibrary,dword[_SETWINDOWLONG_LibHandle]
stdcall FreeLibrary,dword[_CALLWINDOWPROC_LibHandle]
stdcall FreeLibrary,dword[_GETCLASSLONG_LibHandle]
stdcall FreeLibrary,dword[_SETCLASSLONG_LibHandle]
stdcall FreeLibrary,dword[_GETDESKTOPWINDOW_LibHandle]
stdcall FreeLibrary,dword[_GETPROCADDRESS_LibHandle]
stdcall FreeLibrary,dword[_DIALOGBOXPARAM_LibHandle]
stdcall FreeLibrary,dword[_ENDDIALOG_LibHandle]
stdcall FreeLibrary,dword[_GETDLGITEM_LibHandle]
stdcall FreeLibrary,dword[_INITCOMMONCONTROLS_LibHandle]
stdcall FreeLibrary,dword[_INVALIDATERECT_LibHandle]
stdcall FreeLibrary,dword[_CREATEPEN_LibHandle]
stdcall FreeLibrary,dword[_SELECTOBJECT_LibHandle]
stdcall FreeLibrary,dword[_GETSTOCKOBJECT_LibHandle]
stdcall FreeLibrary,dword[_CREATESOLIDBRUSH_LibHandle]
stdcall FreeLibrary,dword[_RECTANGLE_LibHandle]
stdcall FreeLibrary,dword[_DELETEOBJECT_LibHandle]
stdcall FreeLibrary,dword[_DELETEDC_LibHandle]
stdcall FreeLibrary,dword[_DESTROYWINDOW_LibHandle]
stdcall FreeLibrary,dword[_SETPIXEL_LibHandle]
stdcall FreeLibrary,dword[_BITBLT_LibHandle]
stdcall FreeLibrary,dword[_CREATEMENU_LibHandle]
stdcall FreeLibrary,dword[_APPENDMENU_LibHandle]
stdcall FreeLibrary,dword[_POSTMESSAGE_LibHandle]
stdcall FreeLibrary,dword[_GETSYSTEMMETRICS_LibHandle]
stdcall FreeLibrary,dword[_SETWINDOWPOS_LibHandle]
stdcall FreeLibrary,dword[_ENABLEWINDOW_LibHandle]
stdcall FreeLibrary,dword[_PLAYSOUND_LibHandle]
stdcall FreeLibrary,dword[_TIMESETEVENT_LibHandle]
stdcall FreeLibrary,dword[_TIMEKILLEVENT_LibHandle]
stdcall FreeLibrary,dword[_CREATEFONT_LibHandle]
stdcall FreeLibrary,dword[_ABS_LibHandle]
stdcall FreeLibrary,dword[_ACOS_LibHandle]
stdcall FreeLibrary,dword[_ASIN_LibHandle]
stdcall FreeLibrary,dword[_ATAN_LibHandle]
stdcall FreeLibrary,dword[_ATN_LibHandle]
stdcall FreeLibrary,dword[_CEIL_LibHandle]
stdcall FreeLibrary,dword[_EXP_LibHandle]
stdcall FreeLibrary,dword[_FLOOR_LibHandle]
stdcall FreeLibrary,dword[_LOG_LibHandle]
stdcall FreeLibrary,dword[_SQR_LibHandle]
stdcall FreeLibrary,dword[_SQRT_LibHandle]
stdcall FreeLibrary,dword[_TAN_LibHandle]
stdcall FreeLibrary,dword[_TIMER_LibHandle]
stdcall FreeLibrary,dword[_POW_LibHandle]
stdcall FreeLibrary,dword[_ISALNUM_LibHandle]
stdcall FreeLibrary,dword[_ISALPHA_LibHandle]
stdcall FreeLibrary,dword[_ISCNTRL_LibHandle]
stdcall FreeLibrary,dword[_ISDIGIT_LibHandle]
stdcall FreeLibrary,dword[_ISGRAPH_LibHandle]
stdcall FreeLibrary,dword[_ISLOWER_LibHandle]
stdcall FreeLibrary,dword[_ISPRINT_LibHandle]
stdcall FreeLibrary,dword[_ISPUNCT_LibHandle]
stdcall FreeLibrary,dword[_ISSPACE_LibHandle]
stdcall FreeLibrary,dword[_ISUPPER_LibHandle]
stdcall FreeLibrary,dword[_ISXDIGIT_LibHandle]
stdcall FreeLibrary,dword[_TOLOWER_LibHandle]
stdcall FreeLibrary,dword[_TOUPPER_LibHandle]
stdcall FreeLibrary,dword[_CHDIR_LibHandle]
stdcall FreeLibrary,dword[_KILL_LibHandle]
stdcall FreeLibrary,dword[_CREATEDIRECTORY_LibHandle]
stdcall FreeLibrary,dword[_RANDOMIZE_LibHandle]
stdcall FreeLibrary,dword[_RAND_LibHandle]
stdcall FreeLibrary,dword[_ENVIRON____LibHandle]
stdcall FreeLibrary,dword[_RENAME_LibHandle]
stdcall FreeLibrary,dword[_RMDIR_LibHandle]
stdcall FreeLibrary,dword[_RUN_LibHandle]
stdcall FreeLibrary,dword[_LEN_LibHandle]
stdcall FreeLibrary,dword[_LSTRCPY_LibHandle]
stdcall FreeLibrary,dword[_GCVT_LibHandle]
stdcall FreeLibrary,dword[_VAL_LibHandle]
stdcall FreeLibrary,dword[_GETSTDHANDLE_LibHandle]
stdcall FreeLibrary,dword[_SETCONSOLETITLEWINAPI_LibHandle]
stdcall FreeLibrary,dword[_SETCONSOLETEXTATTRIBUTE_LibHandle]
stdcall FreeLibrary,dword[_GETLOCALTIME_LibHandle]
stdcall FreeLibrary,dword[_FOPEN_LibHandle]
stdcall FreeLibrary,dword[_FWRITE_LibHandle]
stdcall FreeLibrary,dword[_FWRITE2_LibHandle]
stdcall FreeLibrary,dword[_FPUTS_LibHandle]
stdcall FreeLibrary,dword[_FREAD_LibHandle]
stdcall FreeLibrary,dword[_FREAD2_LibHandle]
stdcall FreeLibrary,dword[_FGETS_LibHandle]
stdcall FreeLibrary,dword[_FTELL_LibHandle]
stdcall FreeLibrary,dword[_FSEEK_LibHandle]
stdcall FreeLibrary,dword[_FGETPOS_LibHandle]
stdcall FreeLibrary,dword[_FSETPOS_LibHandle]
stdcall FreeLibrary,dword[_FCLOSE_LibHandle]
stdcall FreeLibrary,dword[_LTOA_LibHandle]
stdcall FreeLibrary,dword[_ERRORMSGBOX_LibHandle]
stdcall HeapDestroy,dword[HandleToHeap]
POP EDI
POP ESI
POP EBX
MOV ESP,EBP
POP EBP
MOV EAX,dword[ExitStatus]
stdcall ExitProcess,dword[ExitStatus]
RET

NoMemory:
MOV dword[ExitStatus],1
stdcall MessageBoxA,0,NoMemMessage,Error,0
JMP Exit

NoLibrary:
MOV dword[ExitStatus],1
POP EBX
stdcall lstrlen,EBX
ADD EAX,30
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
MOV EDI,EAX
stdcall lstrcpy,EDI,NoLibFound
stdcall lstrcat,EDI,EBX
stdcall MessageBoxA,0,EDI,Error,0
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
JMP Exit

NoFunction:
MOV dword[ExitStatus],1
POP EBX
stdcall lstrlen,EBX
ADD EAX,30
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
MOV EDI,EAX
stdcall lstrcpy,EDI,NoFunctionFound
stdcall lstrcat,EDI,EBX
stdcall MessageBoxA,0,EDI,Error,0
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
JMP Exit























































































































































































































global _SHOWMESSAGE
_SHOWMESSAGE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label2
JMP NoMemory
Label2:
MOV dword[ParameterPool],EAX
PUSH dword[Number_1+4]
PUSH dword[Number_1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label3
JMP NoMemory
Label3:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EBX,String_2
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label4
JMP NoMemory
Label4:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_2
PUSH EBX
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Number_3+4]
PUSH dword[Number_3]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_MESSAGEBOX],0
JNE Label5
stdcall LoadLibraryA,_MESSAGEBOX_Lib
MOV dword[_MESSAGEBOX_LibHandle],EAX
CMP EAX,0
JNE Label6
PUSH _MESSAGEBOX_Lib
JMP NoLibrary
Label6:
stdcall GetProcAddress,dword[_MESSAGEBOX_LibHandle],_MESSAGEBOX_Alias
MOV dword[_MESSAGEBOX],EAX
CMP EAX,0
JNE Label7
PUSH _MESSAGEBOX_Alias
JMP NoFunction
Label7:
Label5:
CALL dword[_MESSAGEBOX]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _LOWORD
_LOWORD:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

mov eax,dword[ebp+8]   ;dwValue
and eax,65535
mov dword[ebp-4],eax


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _HIWORD
_HIWORD:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

mov eax,dword[ebp+8]
and ecx,16
shr eax,cl 
mov dword[ebp-4],eax


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _MAKEINTRESOURCE
_MAKEINTRESOURCE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label8
JMP NoMemory
Label8:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

extern sprintf
jmp forward
strFormat db '#%010ld',0
forward:
stdcall HeapAlloc,dword[HandleToHeap],8,12 
mov ebx,eax 
ccall sprintf,eax,strFormat,dword[ebp+8]
mov dword[ebp-4],ebx


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _RETURN
_RETURN:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

mov   eax,[ebp+8] 


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _OR
_OR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

MOV EBX,dword[EBP+8]
MOV EDI,dword[EBP+12]
OR  EBX,EDI
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8


global _BOR
_BOR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

MOV EBX,dword[EBP+8]
MOV EDI,dword[EBP+12]
OR  EBX,EDI
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8


global _BAND
_BAND:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

MOV EBX,dword[EBP+8]
MOV EDI,dword[EBP+12]
AND EBX,EDI
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8


global _BXOR
_BXOR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

MOV EBX,dword[EBP+8]
MOV EDI,dword[EBP+12]
XOR EBX,EDI
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8


global _SHIFTLEFT
_SHIFTLEFT:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

mov eax,dword[ebp+8]   ;nBase
mov ecx,dword[ebp+12]  ;nBits
shl eax,cl  
mov dword[ebp-4],eax


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8


global _SIN
_SIN:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,8
MOV dword[ESP+8],0
MOV dword[ESP+4],0
MOV dword[EBP-4],0
MOV dword[EBP-8],0

fld qword[ebp+8]
fsin
fstp qword[ebp-8]


FINIT
FLD qword[EBP-8]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8


global _COS
_COS:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,8
MOV dword[ESP+8],0
MOV dword[ESP+4],0
MOV dword[EBP-4],0
MOV dword[EBP-8],0

fld qword[ebp+8]
fcos
fstp qword[ebp-8]


FINIT
FLD qword[EBP-8]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8












































global _DECREMENT
_DECREMENT:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,8
MOV dword[ESP+8],0
MOV dword[ESP+4],0
MOV dword[EBP-4],0
MOV dword[EBP-8],0

PUSH dword[EBP+8+4]
PUSH dword[EBP+8]
PUSH dword[Number_4+4]
PUSH dword[Number_4]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FST qword[EBP-8]


FINIT
FLD qword[EBP-8]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8


global _INCREMENT
_INCREMENT:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,8
MOV dword[ESP+8],0
MOV dword[ESP+4],0
MOV dword[EBP-4],0
MOV dword[EBP-8],0

PUSH dword[EBP+8+4]
PUSH dword[EBP+8]
PUSH dword[Number_5+4]
PUSH dword[Number_5]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FST qword[EBP-8]


FINIT
FLD qword[EBP-8]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8


global _FRAC
_FRAC:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,8
MOV dword[ESP+8],0
MOV dword[ESP+4],0
MOV dword[EBP-4],0
MOV dword[EBP-8],0

PUSH dword[EBP+8+4]
PUSH dword[EBP+8]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label9
JMP NoMemory
Label9:
MOV dword[ParameterPool],EAX
PUSH dword[EBP+8+4]
PUSH dword[EBP+8]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
POP dword[EAX+0+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0+4]
PUSH dword[EAX+0]
CMP dword[_FLOOR],0
JNE Label10
stdcall LoadLibraryA,_FLOOR_Lib
MOV dword[_FLOOR_LibHandle],EAX
CMP EAX,0
JNE Label11
PUSH _FLOOR_Lib
JMP NoLibrary
Label11:
stdcall GetProcAddress,dword[_FLOOR_LibHandle],_FLOOR_Alias
MOV dword[_FLOOR],EAX
CMP EAX,0
JNE Label12
PUSH _FLOOR_Alias
JMP NoFunction
Label12:
Label10:
CALL dword[_FLOOR]
ADD ESP,8
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FST qword[EBP-8]


FINIT
FLD qword[EBP-8]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8




































































global _MKDIR
_MKDIR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label13
JMP NoMemory
Label13:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label14
JMP NoMemory
Label14:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EDI,Scope113__SECURITYATTRIBUTES_TYPE_size
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label15
JMP NoMemory
Label15:
MOV EBX,EAX
stdcall RtlMoveMemory,EBX,Scope123__SECATTRIBUTES_UDT,EDI
PUSH EBX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
MOV EAX,dword[EAX+4]
PUSH dword[EAX]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_CREATEDIRECTORY],0
JNE Label16
stdcall LoadLibraryA,_CREATEDIRECTORY_Lib
MOV dword[_CREATEDIRECTORY_LibHandle],EAX
CMP EAX,0
JNE Label17
PUSH _CREATEDIRECTORY_Lib
JMP NoLibrary
Label17:
stdcall GetProcAddress,dword[_CREATEDIRECTORY_LibHandle],_CREATEDIRECTORY_Alias
MOV dword[_CREATEDIRECTORY],EAX
CMP EAX,0
JNE Label18
PUSH _CREATEDIRECTORY_Alias
JMP NoFunction
Label18:
Label16:
CALL dword[_CREATEDIRECTORY]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _RND
_RND:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

CMP dword[_RAND],0
JNE Label19
stdcall LoadLibraryA,_RAND_Lib
MOV dword[_RAND_LibHandle],EAX
CMP EAX,0
JNE Label20
PUSH _RAND_Lib
JMP NoLibrary
Label20:
stdcall GetProcAddress,dword[_RAND_LibHandle],_RAND_Alias
MOV dword[_RAND],EAX
CMP EAX,0
JNE Label21
PUSH _RAND_Alias
JMP NoFunction
Label21:
Label19:
CALL dword[_RAND]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_6+4]
PUSH dword[Number_6]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FDIV ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FMUL ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _SGN
_SGN:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_7+4]
PUSH dword[Number_7]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label22
FLDZ
Label22:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label24
JMP Label23
Label24:
PUSH dword[Number_8+4]
PUSH dword[Number_8]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label25
Label23:
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_9+4]
PUSH dword[Number_9]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JA Label26
FLDZ
Label26:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label28
JMP Label27
Label28:
PUSH dword[Number_10+4]
PUSH dword[Number_10]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label25
Label27:
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_11+4]
PUSH dword[Number_11]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JB Label29
FLDZ
Label29:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label31
JMP Label30
Label31:
PUSH dword[Number_12+4]
PUSH dword[Number_12]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label30
Label30:
Label25:


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4








global _SPACE___
_SPACE___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label32
JMP NoMemory
Label32:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4

PUSH dword[Number_13+4]
PUSH dword[Number_13]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
MOV EBX,String_14
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label33
JMP NoMemory
Label33:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_14
PUSH EBX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
Label34:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JBE Label35
FLDZ
Label35:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label37
JMP Label36
Label37:
stdcall lstrlenA,dword[EBP-4]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label38
JMP NoMemory
Label38:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-4]
PUSH EAX
MOV EBX,String_15
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label39
JMP NoMemory
Label39:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_15
PUSH EBX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label40
JMP NoMemory
Label40:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_16+4]
PUSH dword[Number_16]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
JMP Label34
Label36:


MOV EAX,dword[EBP-4]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _MID___
_MID___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label41
JMP NoMemory
Label41:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_17+4]
PUSH dword[Number_17]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JB Label42
FLDZ
Label42:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label44
JMP Label43
Label44:
PUSH dword[Number_18+4]
PUSH dword[Number_18]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP+12]
JMP Label43
Label43:
Label45:
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP+16]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label46
JMP NoMemory
Label46:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label47
JMP NoMemory
Label47:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label48
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label49
PUSH _LEN_Lib
JMP NoLibrary
Label49:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label50
PUSH _LEN_Alias
JMP NoFunction
Label50:
Label48:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JA Label51
FLDZ
Label51:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label53
JMP Label52
Label53:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label55
JMP NoMemory
Label55:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label56
JMP NoMemory
Label56:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label57
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label58
PUSH _LEN_Lib
JMP NoLibrary
Label58:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label59
PUSH _LEN_Alias
JMP NoFunction
Label59:
Label57:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_19+4]
PUSH dword[Number_19]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP+16]
JMP Label52
Label52:
Label54:
extern lstrlenA
extern lstrcpyA
extern lstrcpynA
MOV EBX,dword[EBP+8]   ;EBX = S
MOV ESI,dword[EBP+12]  ;ESI = Start
MOV EDI,dword[EBP+16]  ;EDI = Length
DEC ESI                ;Convert to zero-based 
INC EDI
ADD EBX,ESI            ;Move Ptr to Start
ADD ESI,EDI
INC ESI
stdcall HeapAlloc,dword[HandleToHeap],8,ESI
DEC ESI
stdcall lstrcpynA,EAX,EBX,EDI
MOV dword[EBP-4],EAX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 12


global _ASC
_ASC:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

MOV EAX,dword[EBP+8]
MOVSX EAX,byte[EAX]
MOV dword[EBP-4],EAX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _CHR___
_CHR___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label60
JMP NoMemory
Label60:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

stdcall HeapAlloc,dword[HandleToHeap],8,2
MOV EBX,EAX
MOV AH,byte[EBP+8]
MOV byte[EBX],AH
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _DELETE___
_DELETE___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label61
JMP NoMemory
Label61:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label62
JMP NoMemory
Label62:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label63
JMP NoMemory
Label63:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_20+4]
PUSH dword[Number_20]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_21+4]
PUSH dword[Number_21]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label64
JMP NoMemory
Label64:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label65
JMP NoMemory
Label65:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP+16]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label66
JMP NoMemory
Label66:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label67
JMP NoMemory
Label67:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label68
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label69
PUSH _LEN_Lib
JMP NoLibrary
Label69:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label70
PUSH _LEN_Alias
JMP NoFunction
Label70:
Label68:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label71
JMP NoMemory
Label71:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 12


global _INSERT___
_INSERT___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label72
JMP NoMemory
Label72:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label73
JMP NoMemory
Label73:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label74
JMP NoMemory
Label74:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_22+4]
PUSH dword[Number_22]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP+16]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_23+4]
PUSH dword[Number_23]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label75
JMP NoMemory
Label75:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label76
JMP NoMemory
Label76:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label77
JMP NoMemory
Label77:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label78
JMP NoMemory
Label78:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+16]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label79
JMP NoMemory
Label79:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label80
JMP NoMemory
Label80:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label81
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label82
PUSH _LEN_Lib
JMP NoLibrary
Label82:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label83
PUSH _LEN_Alias
JMP NoFunction
Label83:
Label81:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label84
JMP NoMemory
Label84:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 12


global _LCASE___
_LCASE___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label85
JMP NoMemory
Label85:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4
SUB ESP,4

MOV EBX,String_24
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label86
JMP NoMemory
Label86:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_24
PUSH EBX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
PUSH dword[Number_25+4]
PUSH dword[Number_25]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label87
JMP NoMemory
Label87:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label88
JMP NoMemory
Label88:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label89
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label90
PUSH _LEN_Lib
JMP NoLibrary
Label90:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label91
PUSH _LEN_Alias
JMP NoFunction
Label91:
Label89:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_26+4]
PUSH dword[Number_26]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
Label92:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JB Label93
FLDZ
Label93:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label95
JMP Label94
Label95:
stdcall lstrlenA,dword[EBP-4]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label96
JMP NoMemory
Label96:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-4]
PUSH EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label97
JMP NoMemory
Label97:
MOV dword[ParameterPool],EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label98
JMP NoMemory
Label98:
MOV dword[ParameterPool],EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label99
JMP NoMemory
Label99:
MOV dword[ParameterPool],EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label100
JMP NoMemory
Label100:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label101
JMP NoMemory
Label101:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_27+4]
PUSH dword[Number_27]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _ASC
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_TOLOWER],0
JNE Label102
stdcall LoadLibraryA,_TOLOWER_Lib
MOV dword[_TOLOWER_LibHandle],EAX
CMP EAX,0
JNE Label103
PUSH _TOLOWER_Lib
JMP NoLibrary
Label103:
stdcall GetProcAddress,dword[_TOLOWER_LibHandle],_TOLOWER_Alias
MOV dword[_TOLOWER],EAX
CMP EAX,0
JNE Label104
PUSH _TOLOWER_Alias
JMP NoFunction
Label104:
Label102:
CALL dword[_TOLOWER]
ADD ESP,4
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label105
JMP NoMemory
Label105:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_28+4]
PUSH dword[Number_28]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
JMP Label92
Label94:


MOV EAX,dword[EBP-4]
ADD ESP,12
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _UCASE___
_UCASE___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label106
JMP NoMemory
Label106:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4
SUB ESP,4

MOV EBX,String_29
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label107
JMP NoMemory
Label107:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_29
PUSH EBX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
PUSH dword[Number_30+4]
PUSH dword[Number_30]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label108
JMP NoMemory
Label108:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label109
JMP NoMemory
Label109:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label110
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label111
PUSH _LEN_Lib
JMP NoLibrary
Label111:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label112
PUSH _LEN_Alias
JMP NoFunction
Label112:
Label110:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_31+4]
PUSH dword[Number_31]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
Label113:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JB Label114
FLDZ
Label114:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label116
JMP Label115
Label116:
stdcall lstrlenA,dword[EBP-4]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label117
JMP NoMemory
Label117:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-4]
PUSH EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label118
JMP NoMemory
Label118:
MOV dword[ParameterPool],EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label119
JMP NoMemory
Label119:
MOV dword[ParameterPool],EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label120
JMP NoMemory
Label120:
MOV dword[ParameterPool],EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label121
JMP NoMemory
Label121:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label122
JMP NoMemory
Label122:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_32+4]
PUSH dword[Number_32]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _ASC
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_TOUPPER],0
JNE Label123
stdcall LoadLibraryA,_TOUPPER_Lib
MOV dword[_TOUPPER_LibHandle],EAX
CMP EAX,0
JNE Label124
PUSH _TOUPPER_Lib
JMP NoLibrary
Label124:
stdcall GetProcAddress,dword[_TOUPPER_LibHandle],_TOUPPER_Alias
MOV dword[_TOUPPER],EAX
CMP EAX,0
JNE Label125
PUSH _TOUPPER_Alias
JMP NoFunction
Label125:
Label123:
CALL dword[_TOUPPER]
ADD ESP,4
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label126
JMP NoMemory
Label126:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_33+4]
PUSH dword[Number_33]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
JMP Label113
Label115:


MOV EAX,dword[EBP-4]
ADD ESP,12
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _INSTR
_INSTR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
SUB ESP,4
SUB ESP,4
SUB ESP,4

FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_34+4]
PUSH dword[Number_34]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JB Label127
FLDZ
Label127:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label129
JMP Label128
Label129:
PUSH dword[Number_35+4]
PUSH dword[Number_35]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP+8]
JMP Label128
Label128:
Label130:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label131
JMP NoMemory
Label131:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+16]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label132
JMP NoMemory
Label132:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+16]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label133
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label134
PUSH _LEN_Lib
JMP NoLibrary
Label134:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label135
PUSH _LEN_Alias
JMP NoFunction
Label135:
Label133:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
PUSH dword[Number_36+4]
PUSH dword[Number_36]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label136
JMP NoMemory
Label136:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label137
JMP NoMemory
Label137:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label138
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label139
PUSH _LEN_Lib
JMP NoLibrary
Label139:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label140
PUSH _LEN_Alias
JMP NoFunction
Label140:
Label138:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-16]
Label141:
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-16]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JBE Label142
FLDZ
Label142:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_37+4]
PUSH dword[Number_37]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label143
FLDZ
Label143:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
POP dword[TempQWord1]
POP dword[TempQWord1+4]
JNE Label146
JMP Label145
Label146:
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label144
JMP Label145
Label144:
FINIT
FILD dword[True]
JMP Label147
Label145:
FLDZ
Label147:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label149
JMP Label148
Label149:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label150
JMP NoMemory
Label150:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label151
JMP NoMemory
Label151:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
stdcall lstrlenA,dword[EBP+16]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label152
JMP NoMemory
Label152:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+16]
PUSH EAX
POP EBX
POP EDI
stdcall lstrcmpA,EDI,EBX
MOV ESI,EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,EDI
FINIT
FILD dword[True]
CMP ESI,0
JE Label153
FLDZ
Label153:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label155
JMP Label154
Label155:
PUSH dword[Number_38+4]
PUSH dword[Number_38]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label154
Label154:
Label156:
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_39+4]
PUSH dword[Number_39]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP+8]
JMP Label141
Label148:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_40+4]
PUSH dword[Number_40]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label157
FLDZ
Label157:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label159
JMP Label158
Label159:
PUSH dword[Number_41+4]
PUSH dword[Number_41]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label158
Label158:
Label160:


MOV EAX,dword[EBP-4]
ADD ESP,16
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 12


global _LEFT___
_LEFT___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label161
JMP NoMemory
Label161:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label162
JMP NoMemory
Label162:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label163
JMP NoMemory
Label163:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_42+4]
PUSH dword[Number_42]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8


global _LTRIM___
_LTRIM___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label164
JMP NoMemory
Label164:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4
SUB ESP,4
SUB ESP,4

PUSH dword[Number_43+4]
PUSH dword[Number_43]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label165
JMP NoMemory
Label165:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label166
JMP NoMemory
Label166:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label167
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label168
PUSH _LEN_Lib
JMP NoLibrary
Label168:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label169
PUSH _LEN_Alias
JMP NoFunction
Label169:
Label167:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[Number_44+4]
PUSH dword[Number_44]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-16]
Label170:
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JB Label171
FLDZ
Label171:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-16]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_45+4]
PUSH dword[Number_45]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label172
FLDZ
Label172:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
POP dword[TempQWord1]
POP dword[TempQWord1+4]
JNE Label175
JMP Label174
Label175:
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label173
JMP Label174
Label173:
FINIT
FILD dword[True]
JMP Label176
Label174:
FLDZ
Label176:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label178
JMP Label177
Label178:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label179
JMP NoMemory
Label179:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label180
JMP NoMemory
Label180:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_46+4]
PUSH dword[Number_46]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
MOV EBX,String_47
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label181
JMP NoMemory
Label181:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_47
PUSH EBX
POP EBX
POP EDI
stdcall lstrcmpA,EDI,EBX
MOV ESI,EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,EDI
FINIT
FILD dword[True]
CMP ESI,0
JNE Label182
FLDZ
Label182:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label184
JMP Label183
Label184:
PUSH dword[Number_48+4]
PUSH dword[Number_48]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-16]
JMP Label183
Label183:
Label185:
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_49+4]
PUSH dword[Number_49]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
JMP Label170
Label177:
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_50+4]
PUSH dword[Number_50]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label186
JMP NoMemory
Label186:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label187
JMP NoMemory
Label187:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label188
JMP NoMemory
Label188:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label189
JMP NoMemory
Label189:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label190
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label191
PUSH _LEN_Lib
JMP NoLibrary
Label191:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label192
PUSH _LEN_Alias
JMP NoFunction
Label192:
Label190:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_51+4]
PUSH dword[Number_51]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,16
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _REVERSE___
_REVERSE___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label193
JMP NoMemory
Label193:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4
SUB ESP,4

PUSH dword[Number_52+4]
PUSH dword[Number_52]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label194
JMP NoMemory
Label194:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label195
JMP NoMemory
Label195:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label196
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label197
PUSH _LEN_Lib
JMP NoLibrary
Label197:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label198
PUSH _LEN_Alias
JMP NoFunction
Label198:
Label196:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
Label199:
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JBE Label200
FLDZ
Label200:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label202
JMP Label201
Label202:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label203
JMP NoMemory
Label203:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label204
JMP NoMemory
Label204:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_53+4]
PUSH dword[Number_53]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
stdcall lstrlenA,dword[EBP-4]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label205
JMP NoMemory
Label205:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-4]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label206
JMP NoMemory
Label206:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_54+4]
PUSH dword[Number_54]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
JMP Label199
Label201:


MOV EAX,dword[EBP-4]
ADD ESP,12
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _RIGHT___
_RIGHT___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label207
JMP NoMemory
Label207:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label208
JMP NoMemory
Label208:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label209
JMP NoMemory
Label209:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label210
JMP NoMemory
Label210:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label211
JMP NoMemory
Label211:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label212
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label213
PUSH _LEN_Lib
JMP NoLibrary
Label213:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label214
PUSH _LEN_Alias
JMP NoFunction
Label214:
Label212:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8


global _INSTRREV
_INSTRREV:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
SUB ESP,4
SUB ESP,4
SUB ESP,4

FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_55+4]
PUSH dword[Number_55]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label215
FLDZ
Label215:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label217
JMP Label216
Label217:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label219
JMP NoMemory
Label219:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label220
JMP NoMemory
Label220:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label221
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label222
PUSH _LEN_Lib
JMP NoLibrary
Label222:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label223
PUSH _LEN_Alias
JMP NoFunction
Label223:
Label221:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label224
JMP NoMemory
Label224:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+16]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label225
JMP NoMemory
Label225:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+16]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label226
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label227
PUSH _LEN_Lib
JMP NoLibrary
Label227:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label228
PUSH _LEN_Alias
JMP NoFunction
Label228:
Label226:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP+8]
JMP Label216
Label216:
Label218:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label229
JMP NoMemory
Label229:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+16]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label230
JMP NoMemory
Label230:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+16]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label231
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label232
PUSH _LEN_Lib
JMP NoLibrary
Label232:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label233
PUSH _LEN_Alias
JMP NoFunction
Label233:
Label231:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
PUSH dword[Number_56+4]
PUSH dword[Number_56]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
Label234:
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_57+4]
PUSH dword[Number_57]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JAE Label235
FLDZ
Label235:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_58+4]
PUSH dword[Number_58]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label236
FLDZ
Label236:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
POP dword[TempQWord1]
POP dword[TempQWord1+4]
JNE Label239
JMP Label238
Label239:
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label237
JMP Label238
Label237:
FINIT
FILD dword[True]
JMP Label240
Label238:
FLDZ
Label240:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label242
JMP Label241
Label242:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label243
JMP NoMemory
Label243:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label244
JMP NoMemory
Label244:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
stdcall lstrlenA,dword[EBP+16]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label245
JMP NoMemory
Label245:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+16]
PUSH EAX
POP EBX
POP EDI
stdcall lstrcmpA,EDI,EBX
MOV ESI,EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,EDI
FINIT
FILD dword[True]
CMP ESI,0
JE Label246
FLDZ
Label246:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label248
JMP Label247
Label248:
PUSH dword[Number_59+4]
PUSH dword[Number_59]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label247
Label247:
Label249:
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_60+4]
PUSH dword[Number_60]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP+8]
JMP Label234
Label241:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_61+4]
PUSH dword[Number_61]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label250
FLDZ
Label250:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label252
JMP Label251
Label252:
PUSH dword[Number_62+4]
PUSH dword[Number_62]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label251
Label251:
Label253:


MOV EAX,dword[EBP-4]
ADD ESP,16
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 12


global _RTRIM___
_RTRIM___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label254
JMP NoMemory
Label254:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4
SUB ESP,4

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label255
JMP NoMemory
Label255:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label256
JMP NoMemory
Label256:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label257
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label258
PUSH _LEN_Lib
JMP NoLibrary
Label258:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label259
PUSH _LEN_Alias
JMP NoFunction
Label259:
Label257:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[Number_63+4]
PUSH dword[Number_63]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
Label260:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_64+4]
PUSH dword[Number_64]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JAE Label261
FLDZ
Label261:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_65+4]
PUSH dword[Number_65]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label262
FLDZ
Label262:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
POP dword[TempQWord1]
POP dword[TempQWord1+4]
JNE Label265
JMP Label264
Label265:
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label263
JMP Label264
Label263:
FINIT
FILD dword[True]
JMP Label266
Label264:
FLDZ
Label266:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label268
JMP Label267
Label268:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label269
JMP NoMemory
Label269:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label270
JMP NoMemory
Label270:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_66+4]
PUSH dword[Number_66]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
MOV EBX,String_67
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label271
JMP NoMemory
Label271:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_67
PUSH EBX
POP EBX
POP EDI
stdcall lstrcmpA,EDI,EBX
MOV ESI,EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,EDI
FINIT
FILD dword[True]
CMP ESI,0
JNE Label272
FLDZ
Label272:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label274
JMP Label273
Label274:
PUSH dword[Number_68+4]
PUSH dword[Number_68]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
JMP Label273
Label273:
Label275:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_69+4]
PUSH dword[Number_69]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
JMP Label260
Label267:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_70+4]
PUSH dword[Number_70]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label276
JMP NoMemory
Label276:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label277
JMP NoMemory
Label277:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_71+4]
PUSH dword[Number_71]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,12
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _STRING___
_STRING___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label278
JMP NoMemory
Label278:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4

PUSH dword[Number_72+4]
PUSH dword[Number_72]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
Label279:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JBE Label280
FLDZ
Label280:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label282
JMP Label281
Label282:
stdcall lstrlenA,dword[EBP-4]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label283
JMP NoMemory
Label283:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-4]
PUSH EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label284
JMP NoMemory
Label284:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label285
JMP NoMemory
Label285:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_73+4]
PUSH dword[Number_73]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_74+4]
PUSH dword[Number_74]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label286
JMP NoMemory
Label286:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_75+4]
PUSH dword[Number_75]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
JMP Label279
Label281:


MOV EAX,dword[EBP-4]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8


global _TALLY
_TALLY:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
SUB ESP,4
SUB ESP,4

PUSH dword[Number_76+4]
PUSH dword[Number_76]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[Number_77+4]
PUSH dword[Number_77]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
Label287:
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_78+4]
PUSH dword[Number_78]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label288
FLDZ
Label288:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label290
JMP Label289
Label290:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label291
JMP NoMemory
Label291:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label292
JMP NoMemory
Label292:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label293
JMP NoMemory
Label293:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _INSTR
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_79+4]
PUSH dword[Number_79]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label294
FLDZ
Label294:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label296
JMP Label295
Label296:
PUSH dword[Number_80+4]
PUSH dword[Number_80]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
JMP Label295
Label295:
Label297:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_81+4]
PUSH dword[Number_81]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
FINIT
FILD dword[EBP-4]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_82+4]
PUSH dword[Number_82]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label287
Label289:
FINIT
FILD dword[EBP-4]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]


MOV EAX,dword[EBP-4]
ADD ESP,12
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8


global _CRLF
_CRLF:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label298
JMP NoMemory
Label298:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label299
JMP NoMemory
Label299:
MOV dword[ParameterPool],EAX
PUSH dword[Number_83+4]
PUSH dword[Number_83]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label300
JMP NoMemory
Label300:
MOV dword[ParameterPool],EAX
PUSH dword[Number_84+4]
PUSH dword[Number_84]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label301
JMP NoMemory
Label301:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0


global _VBCRLF
_VBCRLF:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label302
JMP NoMemory
Label302:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label303
JMP NoMemory
Label303:
MOV dword[ParameterPool],EAX
PUSH dword[Number_85+4]
PUSH dword[Number_85]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0


global _VBCR
_VBCR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label304
JMP NoMemory
Label304:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label305
JMP NoMemory
Label305:
MOV dword[ParameterPool],EAX
PUSH dword[Number_86+4]
PUSH dword[Number_86]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0


global _VBLF
_VBLF:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label306
JMP NoMemory
Label306:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label307
JMP NoMemory
Label307:
MOV dword[ParameterPool],EAX
PUSH dword[Number_87+4]
PUSH dword[Number_87]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0


global _VBTAB
_VBTAB:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label308
JMP NoMemory
Label308:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label309
JMP NoMemory
Label309:
MOV dword[ParameterPool],EAX
PUSH dword[Number_88+4]
PUSH dword[Number_88]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0


global _REPLACE
_REPLACE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label310
JMP NoMemory
Label310:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label311
JMP NoMemory
Label311:
MOV dword[EBP-12],EAX
MOV byte[EAX],0

stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label312
JMP NoMemory
Label312:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-12]
MOV dword[EBP-12],EBX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label313
JMP NoMemory
Label313:
MOV dword[ParameterPool],EAX
PUSH dword[Number_89+4]
PUSH dword[Number_89]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
stdcall lstrlenA,dword[EBP-12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label314
JMP NoMemory
Label314:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label315
JMP NoMemory
Label315:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _INSTR
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
Label316:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_90+4]
PUSH dword[Number_90]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JA Label317
FLDZ
Label317:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label319
JMP Label318
Label319:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label320
JMP NoMemory
Label320:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP-12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label321
JMP NoMemory
Label321:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_91+4]
PUSH dword[Number_91]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _LEFT___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
stdcall lstrlenA,dword[EBP+16]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label322
JMP NoMemory
Label322:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+16]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label323
JMP NoMemory
Label323:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label324
JMP NoMemory
Label324:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP-12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label325
JMP NoMemory
Label325:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label326
JMP NoMemory
Label326:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label327
JMP NoMemory
Label327:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label328
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label329
PUSH _LEN_Lib
JMP NoLibrary
Label329:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label330
PUSH _LEN_Alias
JMP NoFunction
Label330:
Label328:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _RIGHT___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label331
JMP NoMemory
Label331:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-12]
MOV dword[EBP-12],EBX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label332
JMP NoMemory
Label332:
MOV dword[ParameterPool],EAX
PUSH dword[Number_92+4]
PUSH dword[Number_92]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
stdcall lstrlenA,dword[EBP-12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label333
JMP NoMemory
Label333:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label334
JMP NoMemory
Label334:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _INSTR
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
JMP Label316
Label318:
stdcall lstrlenA,dword[EBP-12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label335
JMP NoMemory
Label335:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-12]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,12
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 12








global _INTTOSTR
_INTTOSTR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label336
JMP NoMemory
Label336:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

extern sprintf
jmp forward
strFormat db '%ld',0
forward:
stdcall HeapAlloc,dword[HandleToHeap],8,11 
mov ebx,eax 
ccall sprintf,eax,strFormat,dword[ebp+8]
mov dword[ebp-4],ebx


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _STR___
_STR___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label337
JMP NoMemory
Label337:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4

PUSH dword[Number_93+4]
PUSH dword[Number_93]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label338
JMP NoMemory
Label338:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_94+4]
PUSH dword[Number_94]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _SPACE___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label339
JMP NoMemory
Label339:
MOV dword[ParameterPool],EAX
PUSH dword[EBP+8+4]
PUSH dword[EBP+8]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
POP dword[EAX+0+4]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[EBP-4]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0+4]
PUSH dword[EAX+0]
CMP dword[_GCVT],0
JNE Label340
stdcall LoadLibraryA,_GCVT_Lib
MOV dword[_GCVT_LibHandle],EAX
CMP EAX,0
JNE Label341
PUSH _GCVT_Lib
JMP NoLibrary
Label341:
stdcall GetProcAddress,dword[_GCVT_LibHandle],_GCVT_Alias
MOV dword[_GCVT],EAX
CMP EAX,0
JNE Label342
PUSH _GCVT_Alias
JMP NoFunction
Label342:
Label340:
CALL dword[_GCVT]
ADD ESP,16
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


MOV EAX,dword[EBP-4]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8


global _STRI
_STRI:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label343
JMP NoMemory
Label343:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

extern sprintf
stdcall HeapAlloc,dword[HandleToHeap],8,11 
mov ebx,eax 
invoke sprintf,eax,"%ld",dword[ebp+8]
mov dword[ebp-4],ebx


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _STRD
_STRD:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label344
JMP NoMemory
Label344:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

extern sprintf
stdcall HeapAlloc,dword[HandleToHeap],8,128 
mov ebx,eax 
invoke sprintf,eax,"%f",dword[ebp+8],dword[ebp+12]
mov dword[ebp-4],ebx


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8











global _COLOR
_COLOR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label345
JMP NoMemory
Label345:
MOV dword[ParameterPool],EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label346
JMP NoMemory
Label346:
MOV dword[ParameterPool],EAX
PUSH dword[Number_95+4]
PUSH dword[Number_95]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_GETSTDHANDLE],0
JNE Label347
stdcall LoadLibraryA,_GETSTDHANDLE_Lib
MOV dword[_GETSTDHANDLE_LibHandle],EAX
CMP EAX,0
JNE Label348
PUSH _GETSTDHANDLE_Lib
JMP NoLibrary
Label348:
stdcall GetProcAddress,dword[_GETSTDHANDLE_LibHandle],_GETSTDHANDLE_Alias
MOV dword[_GETSTDHANDLE],EAX
CMP EAX,0
JNE Label349
PUSH _GETSTDHANDLE_Alias
JMP NoFunction
Label349:
Label347:
CALL dword[_GETSTDHANDLE]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label350
JMP NoMemory
Label350:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _OR
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_SETCONSOLETEXTATTRIBUTE],0
JNE Label351
stdcall LoadLibraryA,_SETCONSOLETEXTATTRIBUTE_Lib
MOV dword[_SETCONSOLETEXTATTRIBUTE_LibHandle],EAX
CMP EAX,0
JNE Label352
PUSH _SETCONSOLETEXTATTRIBUTE_Lib
JMP NoLibrary
Label352:
stdcall GetProcAddress,dword[_SETCONSOLETEXTATTRIBUTE_LibHandle],_SETCONSOLETEXTATTRIBUTE_Alias
MOV dword[_SETCONSOLETEXTATTRIBUTE],EAX
CMP EAX,0
JNE Label353
PUSH _SETCONSOLETEXTATTRIBUTE_Alias
JMP NoFunction
Label353:
Label351:
CALL dword[_SETCONSOLETEXTATTRIBUTE]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8


global _CSLRIN
_CSLRIN:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4



MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0


global _GET___
_GET___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label354
JMP NoMemory
Label354:
MOV dword[EBP-4],EAX
MOV byte[EAX],0



MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _INKEY___
_INKEY___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label355
JMP NoMemory
Label355:
MOV dword[EBP-4],EAX
MOV byte[EAX],0



MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0


global _LOCATE
_LOCATE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

extern SetConsoleCursorPosition
extern GetStdHandle
MOV AX,word[EBP+12]
PUSH AX
MOV AX,word[EBP+8]
PUSH AX
stdcall GetStdHandle,-11
PUSH EAX
CALL SetConsoleCursorPosition


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8


global _POS
_POS:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4



MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0


global _GETCONSOLEREADHANDLE
_GETCONSOLEREADHANDLE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label356
JMP NoMemory
Label356:
MOV dword[ParameterPool],EAX
PUSH dword[Number_96+4]
PUSH dword[Number_96]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_GETSTDHANDLE],0
JNE Label357
stdcall LoadLibraryA,_GETSTDHANDLE_Lib
MOV dword[_GETSTDHANDLE_LibHandle],EAX
CMP EAX,0
JNE Label358
PUSH _GETSTDHANDLE_Lib
JMP NoLibrary
Label358:
stdcall GetProcAddress,dword[_GETSTDHANDLE_LibHandle],_GETSTDHANDLE_Alias
MOV dword[_GETSTDHANDLE],EAX
CMP EAX,0
JNE Label359
PUSH _GETSTDHANDLE_Alias
JMP NoFunction
Label359:
Label357:
CALL dword[_GETSTDHANDLE]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0


global _GETCONSOLEWRITEHANDLE
_GETCONSOLEWRITEHANDLE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label360
JMP NoMemory
Label360:
MOV dword[ParameterPool],EAX
PUSH dword[Number_97+4]
PUSH dword[Number_97]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_GETSTDHANDLE],0
JNE Label361
stdcall LoadLibraryA,_GETSTDHANDLE_Lib
MOV dword[_GETSTDHANDLE_LibHandle],EAX
CMP EAX,0
JNE Label362
PUSH _GETSTDHANDLE_Lib
JMP NoLibrary
Label362:
stdcall GetProcAddress,dword[_GETSTDHANDLE_LibHandle],_GETSTDHANDLE_Alias
MOV dword[_GETSTDHANDLE],EAX
CMP EAX,0
JNE Label363
PUSH _GETSTDHANDLE_Alias
JMP NoFunction
Label363:
Label361:
CALL dword[_GETSTDHANDLE]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0


global _SETCONSOLETITLE
_SETCONSOLETITLE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label364
JMP NoMemory
Label364:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label365
JMP NoMemory
Label365:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_SETCONSOLETITLEWINAPI],0
JNE Label366
stdcall LoadLibraryA,_SETCONSOLETITLEWINAPI_Lib
MOV dword[_SETCONSOLETITLEWINAPI_LibHandle],EAX
CMP EAX,0
JNE Label367
PUSH _SETCONSOLETITLEWINAPI_Lib
JMP NoLibrary
Label367:
stdcall GetProcAddress,dword[_SETCONSOLETITLEWINAPI_LibHandle],_SETCONSOLETITLEWINAPI_Alias
MOV dword[_SETCONSOLETITLEWINAPI],EAX
CMP EAX,0
JNE Label368
PUSH _SETCONSOLETITLEWINAPI_Alias
JMP NoFunction
Label368:
Label366:
CALL dword[_SETCONSOLETITLEWINAPI]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4





global _TIME___
_TIME___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label369
JMP NoMemory
Label369:
MOV dword[ParameterPool],EAX
PUSH Scope169__RETURNTIME_UDT
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_GETLOCALTIME],0
JNE Label370
stdcall LoadLibraryA,_GETLOCALTIME_Lib
MOV dword[_GETLOCALTIME_LibHandle],EAX
CMP EAX,0
JNE Label371
PUSH _GETLOCALTIME_Lib
JMP NoLibrary
Label371:
stdcall GetProcAddress,dword[_GETLOCALTIME_LibHandle],_GETLOCALTIME_Alias
MOV dword[_GETLOCALTIME],EAX
CMP EAX,0
JNE Label372
PUSH _GETLOCALTIME_Alias
JMP NoFunction
Label372:
Label370:
CALL dword[_GETLOCALTIME]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label376
JMP NoMemory
Label376:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label373:
CMP byte[EBX+ECX],0
JE Label374
INC ECX
CMP ECX,EDI
JL Label373
JMP Label375
Label374:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label375
MOV byte[EBX+ECX],0
Label375:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
FINIT
FILD dword[Scope169__RETURNTIME_UDT+Scope168__SYSTEMTIME_TYPE.Scope168__WMILLISECONDS_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label380
JMP NoMemory
Label380:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label377:
CMP byte[EBX+ECX],0
JE Label378
INC ECX
CMP ECX,EDI
JL Label377
JMP Label379
Label378:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label379
MOV byte[EBX+ECX],0
Label379:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0






































global _FILEOPEN
_FILEOPEN:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label381
JMP NoMemory
Label381:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label382
JMP NoMemory
Label382:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label383
JMP NoMemory
Label383:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EBX,String_98
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label384
JMP NoMemory
Label384:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_98
PUSH EBX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label385
JMP NoMemory
Label385:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FOPEN],0
JNE Label386
stdcall LoadLibraryA,_FOPEN_Lib
MOV dword[_FOPEN_LibHandle],EAX
CMP EAX,0
JNE Label387
PUSH _FOPEN_Lib
JMP NoLibrary
Label387:
stdcall GetProcAddress,dword[_FOPEN_LibHandle],_FOPEN_Alias
MOV dword[_FOPEN],EAX
CMP EAX,0
JNE Label388
PUSH _FOPEN_Alias
JMP NoFunction
Label388:
Label386:
CALL dword[_FOPEN]
ADD ESP,8
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8


global _FILEWRITESTR
_FILEWRITESTR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label389
JMP NoMemory
Label389:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label390
JMP NoMemory
Label390:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FPUTS],0
JNE Label391
stdcall LoadLibraryA,_FPUTS_Lib
MOV dword[_FPUTS_LibHandle],EAX
CMP EAX,0
JNE Label392
PUSH _FPUTS_Lib
JMP NoLibrary
Label392:
stdcall GetProcAddress,dword[_FPUTS_LibHandle],_FPUTS_Alias
MOV dword[_FPUTS],EAX
CMP EAX,0
JNE Label393
PUSH _FPUTS_Alias
JMP NoFunction
Label393:
Label391:
CALL dword[_FPUTS]
ADD ESP,8
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8


global _FILEREADSTR
_FILEREADSTR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label394
JMP NoMemory
Label394:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label395
JMP NoMemory
Label395:
MOV dword[EBP-8],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label396
JMP NoMemory
Label396:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_99+4]
PUSH dword[Number_99]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _SPACE___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-8]
MOV dword[EBP-8],EBX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label397
JMP NoMemory
Label397:
MOV dword[ParameterPool],EAX
PUSH dword[EBP-8]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_100+4]
PUSH dword[Number_100]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FREAD],0
JNE Label398
stdcall LoadLibraryA,_FREAD_Lib
MOV dword[_FREAD_LibHandle],EAX
CMP EAX,0
JNE Label399
PUSH _FREAD_Lib
JMP NoLibrary
Label399:
stdcall GetProcAddress,dword[_FREAD_LibHandle],_FREAD_Alias
MOV dword[_FREAD],EAX
CMP EAX,0
JNE Label400
PUSH _FREAD_Alias
JMP NoFunction
Label400:
Label398:
CALL dword[_FREAD]
ADD ESP,16
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
stdcall lstrlenA,dword[EBP-8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label401
JMP NoMemory
Label401:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-8]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8


global _FILEWRITELINE
_FILEWRITELINE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label402
JMP NoMemory
Label402:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label403
JMP NoMemory
Label403:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label404
JMP NoMemory
Label404:
MOV dword[ParameterPool],EAX
PUSH dword[Number_101+4]
PUSH dword[Number_101]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label405
JMP NoMemory
Label405:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label406
JMP NoMemory
Label406:
MOV dword[ParameterPool],EAX
PUSH dword[Number_102+4]
PUSH dword[Number_102]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label407
JMP NoMemory
Label407:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FPUTS],0
JNE Label408
stdcall LoadLibraryA,_FPUTS_Lib
MOV dword[_FPUTS_LibHandle],EAX
CMP EAX,0
JNE Label409
PUSH _FPUTS_Lib
JMP NoLibrary
Label409:
stdcall GetProcAddress,dword[_FPUTS_LibHandle],_FPUTS_Alias
MOV dword[_FPUTS],EAX
CMP EAX,0
JNE Label410
PUSH _FPUTS_Alias
JMP NoFunction
Label410:
Label408:
CALL dword[_FPUTS]
ADD ESP,8
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8


global _FILEGETSIZE
_FILEGETSIZE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
SUB ESP,4

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label411
JMP NoMemory
Label411:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FTELL],0
JNE Label412
stdcall LoadLibraryA,_FTELL_Lib
MOV dword[_FTELL_LibHandle],EAX
CMP EAX,0
JNE Label413
PUSH _FTELL_Lib
JMP NoLibrary
Label413:
stdcall GetProcAddress,dword[_FTELL_LibHandle],_FTELL_Alias
MOV dword[_FTELL],EAX
CMP EAX,0
JNE Label414
PUSH _FTELL_Alias
JMP NoFunction
Label414:
Label412:
CALL dword[_FTELL]
ADD ESP,4
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label415
JMP NoMemory
Label415:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_103+4]
PUSH dword[Number_103]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_104+4]
PUSH dword[Number_104]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FSEEK],0
JNE Label416
stdcall LoadLibraryA,_FSEEK_Lib
MOV dword[_FSEEK_LibHandle],EAX
CMP EAX,0
JNE Label417
PUSH _FSEEK_Lib
JMP NoLibrary
Label417:
stdcall GetProcAddress,dword[_FSEEK_LibHandle],_FSEEK_Alias
MOV dword[_FSEEK],EAX
CMP EAX,0
JNE Label418
PUSH _FSEEK_Alias
JMP NoFunction
Label418:
Label416:
CALL dword[_FSEEK]
ADD ESP,12
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label419
JMP NoMemory
Label419:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FTELL],0
JNE Label420
stdcall LoadLibraryA,_FTELL_Lib
MOV dword[_FTELL_LibHandle],EAX
CMP EAX,0
JNE Label421
PUSH _FTELL_Lib
JMP NoLibrary
Label421:
stdcall GetProcAddress,dword[_FTELL_LibHandle],_FTELL_Alias
MOV dword[_FTELL],EAX
CMP EAX,0
JNE Label422
PUSH _FTELL_Alias
JMP NoFunction
Label422:
Label420:
CALL dword[_FTELL]
ADD ESP,4
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label423
JMP NoMemory
Label423:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_105+4]
PUSH dword[Number_105]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FSEEK],0
JNE Label424
stdcall LoadLibraryA,_FSEEK_Lib
MOV dword[_FSEEK_LibHandle],EAX
CMP EAX,0
JNE Label425
PUSH _FSEEK_Lib
JMP NoLibrary
Label425:
stdcall GetProcAddress,dword[_FSEEK_LibHandle],_FSEEK_Alias
MOV dword[_FSEEK],EAX
CMP EAX,0
JNE Label426
PUSH _FSEEK_Alias
JMP NoFunction
Label426:
Label424:
CALL dword[_FSEEK]
ADD ESP,12
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


MOV EAX,dword[EBP-4]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _FILEREADLINE
_FILEREADLINE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label427
JMP NoMemory
Label427:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label428
JMP NoMemory
Label428:
MOV dword[EBP-8],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label429
JMP NoMemory
Label429:
MOV dword[ParameterPool],EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label430
JMP NoMemory
Label430:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _FILEGETSIZE
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_106+4]
PUSH dword[Number_106]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _SPACE___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-8]
MOV dword[EBP-8],EBX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label431
JMP NoMemory
Label431:
MOV dword[ParameterPool],EAX
PUSH dword[EBP-8]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label432
JMP NoMemory
Label432:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _FILEGETSIZE
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_107+4]
PUSH dword[Number_107]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FGETS],0
JNE Label433
stdcall LoadLibraryA,_FGETS_Lib
MOV dword[_FGETS_LibHandle],EAX
CMP EAX,0
JNE Label434
PUSH _FGETS_Lib
JMP NoLibrary
Label434:
stdcall GetProcAddress,dword[_FGETS_LibHandle],_FGETS_Alias
MOV dword[_FGETS],EAX
CMP EAX,0
JNE Label435
PUSH _FGETS_Alias
JMP NoFunction
Label435:
Label433:
CALL dword[_FGETS]
ADD ESP,12
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
stdcall lstrlenA,dword[EBP-8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label436
JMP NoMemory
Label436:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-8]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _FILEWRITEINT
_FILEWRITEINT:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label437
JMP NoMemory
Label437:
MOV dword[ParameterPool],EAX
MOV EAX,EBP
ADD EAX,12
PUSH EAX
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_108+4]
PUSH dword[Number_108]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_109+4]
PUSH dword[Number_109]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FWRITE2],0
JNE Label438
stdcall LoadLibraryA,_FWRITE2_Lib
MOV dword[_FWRITE2_LibHandle],EAX
CMP EAX,0
JNE Label439
PUSH _FWRITE2_Lib
JMP NoLibrary
Label439:
stdcall GetProcAddress,dword[_FWRITE2_LibHandle],_FWRITE2_Alias
MOV dword[_FWRITE2],EAX
CMP EAX,0
JNE Label440
PUSH _FWRITE2_Alias
JMP NoFunction
Label440:
Label438:
CALL dword[_FWRITE2]
ADD ESP,16
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8


global _FILEREADINT
_FILEREADINT:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label441
JMP NoMemory
Label441:
MOV dword[ParameterPool],EAX
MOV EAX,EBP
SUB EAX,4
PUSH EAX
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_110+4]
PUSH dword[Number_110]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_111+4]
PUSH dword[Number_111]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FREAD2],0
JNE Label442
stdcall LoadLibraryA,_FREAD2_Lib
MOV dword[_FREAD2_LibHandle],EAX
CMP EAX,0
JNE Label443
PUSH _FREAD2_Lib
JMP NoLibrary
Label443:
stdcall GetProcAddress,dword[_FREAD2_LibHandle],_FREAD2_Alias
MOV dword[_FREAD2],EAX
CMP EAX,0
JNE Label444
PUSH _FREAD2_Alias
JMP NoFunction
Label444:
Label442:
CALL dword[_FREAD2]
ADD ESP,16
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _FILEWRITEDOUBLE
_FILEWRITEDOUBLE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label445
JMP NoMemory
Label445:
MOV dword[ParameterPool],EAX
MOV EAX,EBP
ADD EAX,12
PUSH EAX
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_112+4]
PUSH dword[Number_112]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_113+4]
PUSH dword[Number_113]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FWRITE2],0
JNE Label446
stdcall LoadLibraryA,_FWRITE2_Lib
MOV dword[_FWRITE2_LibHandle],EAX
CMP EAX,0
JNE Label447
PUSH _FWRITE2_Lib
JMP NoLibrary
Label447:
stdcall GetProcAddress,dword[_FWRITE2_LibHandle],_FWRITE2_Alias
MOV dword[_FWRITE2],EAX
CMP EAX,0
JNE Label448
PUSH _FWRITE2_Alias
JMP NoFunction
Label448:
Label446:
CALL dword[_FWRITE2]
ADD ESP,16
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 12


global _FILEREADDOUBLE
_FILEREADDOUBLE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,8
MOV dword[ESP+8],0
MOV dword[ESP+4],0
MOV dword[EBP-4],0
MOV dword[EBP-8],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label449
JMP NoMemory
Label449:
MOV dword[ParameterPool],EAX
MOV EAX,EBP
SUB EAX,8
PUSH EAX
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_114+4]
PUSH dword[Number_114]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_115+4]
PUSH dword[Number_115]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FREAD2],0
JNE Label450
stdcall LoadLibraryA,_FREAD2_Lib
MOV dword[_FREAD2_LibHandle],EAX
CMP EAX,0
JNE Label451
PUSH _FREAD2_Lib
JMP NoLibrary
Label451:
stdcall GetProcAddress,dword[_FREAD2_LibHandle],_FREAD2_Alias
MOV dword[_FREAD2],EAX
CMP EAX,0
JNE Label452
PUSH _FREAD2_Alias
JMP NoFunction
Label452:
Label450:
CALL dword[_FREAD2]
ADD ESP,16
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


FINIT
FLD qword[EBP-8]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _FILESETPOS
_FILESETPOS:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label453
JMP NoMemory
Label453:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_116+4]
PUSH dword[Number_116]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FSEEK],0
JNE Label454
stdcall LoadLibraryA,_FSEEK_Lib
MOV dword[_FSEEK_LibHandle],EAX
CMP EAX,0
JNE Label455
PUSH _FSEEK_Lib
JMP NoLibrary
Label455:
stdcall GetProcAddress,dword[_FSEEK_LibHandle],_FSEEK_Alias
MOV dword[_FSEEK],EAX
CMP EAX,0
JNE Label456
PUSH _FSEEK_Alias
JMP NoFunction
Label456:
Label454:
CALL dword[_FSEEK]
ADD ESP,12
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8


global _FILEGETPOS
_FILEGETPOS:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label457
JMP NoMemory
Label457:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FTELL],0
JNE Label458
stdcall LoadLibraryA,_FTELL_Lib
MOV dword[_FTELL_LibHandle],EAX
CMP EAX,0
JNE Label459
PUSH _FTELL_Lib
JMP NoLibrary
Label459:
stdcall GetProcAddress,dword[_FTELL_LibHandle],_FTELL_Alias
MOV dword[_FTELL],EAX
CMP EAX,0
JNE Label460
PUSH _FTELL_Alias
JMP NoFunction
Label460:
Label458:
CALL dword[_FTELL]
ADD ESP,4
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _FILECLOSE
_FILECLOSE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label461
JMP NoMemory
Label461:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FCLOSE],0
JNE Label462
stdcall LoadLibraryA,_FCLOSE_Lib
MOV dword[_FCLOSE_LibHandle],EAX
CMP EAX,0
JNE Label463
PUSH _FCLOSE_Lib
JMP NoLibrary
Label463:
stdcall GetProcAddress,dword[_FCLOSE_LibHandle],_FCLOSE_Alias
MOV dword[_FCLOSE],EAX
CMP EAX,0
JNE Label464
PUSH _FCLOSE_Alias
JMP NoFunction
Label464:
Label462:
CALL dword[_FCLOSE]
ADD ESP,4
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4





global _SHOWINT
_SHOWINT:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label465
JMP NoMemory
Label465:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label466
JMP NoMemory
Label466:
MOV dword[ParameterPool],EAX
PUSH dword[Number_117+4]
PUSH dword[Number_117]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _SPACE___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label467
JMP NoMemory
Label467:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[EBP-4]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP+16]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LTOA],0
JNE Label468
stdcall LoadLibraryA,_LTOA_Lib
MOV dword[_LTOA_LibHandle],EAX
CMP EAX,0
JNE Label469
PUSH _LTOA_Lib
JMP NoLibrary
Label469:
stdcall GetProcAddress,dword[_LTOA_LibHandle],_LTOA_Alias
MOV dword[_LTOA],EAX
CMP EAX,0
JNE Label470
PUSH _LTOA_Alias
JMP NoFunction
Label470:
Label468:
CALL dword[_LTOA]
ADD ESP,12
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label471
JMP NoMemory
Label471:
MOV dword[ParameterPool],EAX
PUSH dword[Number_118+4]
PUSH dword[Number_118]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
stdcall lstrlenA,dword[EBP-4]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label472
JMP NoMemory
Label472:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-4]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label473
JMP NoMemory
Label473:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Number_119+4]
PUSH dword[Number_119]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_MESSAGEBOX],0
JNE Label474
stdcall LoadLibraryA,_MESSAGEBOX_Lib
MOV dword[_MESSAGEBOX_LibHandle],EAX
CMP EAX,0
JNE Label475
PUSH _MESSAGEBOX_Lib
JMP NoLibrary
Label475:
stdcall GetProcAddress,dword[_MESSAGEBOX_LibHandle],_MESSAGEBOX_Alias
MOV dword[_MESSAGEBOX],EAX
CMP EAX,0
JNE Label476
PUSH _MESSAGEBOX_Alias
JMP NoFunction
Label476:
Label474:
CALL dword[_MESSAGEBOX]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 12





global _SHOWLASTERROR
_SHOWLASTERROR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
SUB ESP,4
SUB ESP,4
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label477
JMP NoMemory
Label477:
MOV dword[EBP-16],EAX
MOV byte[EAX],0
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label478
JMP NoMemory
Label478:
MOV dword[EBP-20],EAX
MOV byte[EAX],0

CMP dword[_GETLASTERROR],0
JNE Label479
stdcall LoadLibraryA,_GETLASTERROR_Lib
MOV dword[_GETLASTERROR_LibHandle],EAX
CMP EAX,0
JNE Label480
PUSH _GETLASTERROR_Lib
JMP NoLibrary
Label480:
stdcall GetProcAddress,dword[_GETLASTERROR_LibHandle],_GETLASTERROR_Alias
MOV dword[_GETLASTERROR],EAX
CMP EAX,0
JNE Label481
PUSH _GETLASTERROR_Alias
JMP NoFunction
Label481:
Label479:
CALL dword[_GETLASTERROR]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label482
JMP NoMemory
Label482:
MOV dword[ParameterPool],EAX
PUSH dword[Number_120+4]
PUSH dword[Number_120]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _SPACE___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-16]
MOV dword[EBP-16],EBX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label483
JMP NoMemory
Label483:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[EBP-16]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_121+4]
PUSH dword[Number_121]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LTOA],0
JNE Label484
stdcall LoadLibraryA,_LTOA_Lib
MOV dword[_LTOA_LibHandle],EAX
CMP EAX,0
JNE Label485
PUSH _LTOA_Lib
JMP NoLibrary
Label485:
stdcall GetProcAddress,dword[_LTOA_LibHandle],_LTOA_Alias
MOV dword[_LTOA],EAX
CMP EAX,0
JNE Label486
PUSH _LTOA_Alias
JMP NoFunction
Label486:
Label484:
CALL dword[_LTOA]
ADD ESP,12
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV EBX,String_122
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label487
JMP NoMemory
Label487:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_122
PUSH EBX
stdcall lstrlenA,dword[EBP-16]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label488
JMP NoMemory
Label488:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-16]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label489
JMP NoMemory
Label489:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-20]
MOV dword[EBP-20],EBX
PUSH dword[Number_123+4]
PUSH dword[Number_123]
PUSH dword[Number_124+4]
PUSH dword[Number_124]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_125+4]
PUSH dword[Number_125]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,28
CMP EAX,0
JNE Label490
JMP NoMemory
Label490:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_126+4]
PUSH dword[Number_126]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Number_127+4]
PUSH dword[Number_127]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,EBP
SUB EAX,4
PUSH EAX
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+16]
PUSH dword[Number_128+4]
PUSH dword[Number_128]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+20]
PUSH dword[Number_129+4]
PUSH dword[Number_129]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+24]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+24]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+20]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+16]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FORMATMESSAGE],0
JNE Label491
stdcall LoadLibraryA,_FORMATMESSAGE_Lib
MOV dword[_FORMATMESSAGE_LibHandle],EAX
CMP EAX,0
JNE Label492
PUSH _FORMATMESSAGE_Lib
JMP NoLibrary
Label492:
stdcall GetProcAddress,dword[_FORMATMESSAGE_LibHandle],_FORMATMESSAGE_Alias
MOV dword[_FORMATMESSAGE],EAX
CMP EAX,0
JNE Label493
PUSH _FORMATMESSAGE_Alias
JMP NoFunction
Label493:
Label491:
CALL dword[_FORMATMESSAGE]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label494
JMP NoMemory
Label494:
MOV dword[ParameterPool],EAX
PUSH dword[Number_130+4]
PUSH dword[Number_130]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP-4]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[EBP-20]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Number_131+4]
PUSH dword[Number_131]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_ERRORMSGBOX],0
JNE Label495
stdcall LoadLibraryA,_ERRORMSGBOX_Lib
MOV dword[_ERRORMSGBOX_LibHandle],EAX
CMP EAX,0
JNE Label496
PUSH _ERRORMSGBOX_Lib
JMP NoLibrary
Label496:
stdcall GetProcAddress,dword[_ERRORMSGBOX_LibHandle],_ERRORMSGBOX_Alias
MOV dword[_ERRORMSGBOX],EAX
CMP EAX,0
JNE Label497
PUSH _ERRORMSGBOX_Alias
JMP NoFunction
Label497:
Label495:
CALL dword[_ERRORMSGBOX]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label498
JMP NoMemory
Label498:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP-4]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LOCALFREE],0
JNE Label499
stdcall LoadLibraryA,_LOCALFREE_Lib
MOV dword[_LOCALFREE_LibHandle],EAX
CMP EAX,0
JNE Label500
PUSH _LOCALFREE_Lib
JMP NoLibrary
Label500:
stdcall GetProcAddress,dword[_LOCALFREE_LibHandle],_LOCALFREE_Alias
MOV dword[_LOCALFREE],EAX
CMP EAX,0
JNE Label501
PUSH _LOCALFREE_Alias
JMP NoFunction
Label501:
Label499:
CALL dword[_LOCALFREE]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


ADD ESP,20
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0
NoConsole:
MOV dword[ExitStatus],1
stdcall MessageBoxA,0,NoConsoleMessage,Error,0
JMP Exit



;Data section of the Chameleon app
section .data
NoMemMessage db "Could not allocate memory.",0
NoLibFound db "Could not find library: ",0
NoFunctionFound db "Cound not find function: ",0
Error db "Error!",0
ParameterPool dd 0
HandleToHeap dd 0
TempQWord1 dq 0.0
TempQWord2 dq 0.0
True dd -1
STRUC Scope0__POINT_TYPE
.Scope0__X_Integer resd 1
.Scope0__Y_Integer resd 1
ENDSTRUC
STRUC Scope0__RECT_TYPE
.Scope0__LEFT_Integer resd 1
.Scope0__TOP_Integer resd 1
.Scope0__RIGHT_Integer resd 1
.Scope0__BOTTOM_Integer resd 1
ENDSTRUC
STRUC Scope0__SIZE_TYPE
.Scope0__CX_Integer resd 1
.Scope0__CY_Integer resd 1
ENDSTRUC
STRUC Scope0__STARTUPINFO_TYPE
.Scope0__CB_Integer resd 1
.Scope0__LPRESERVED_String resd 1
.Scope0__LPDESKTOP_String resd 1
.Scope0__LPTITLE_String resd 1
.Scope0__DWX_Integer resd 1
.Scope0__DWY_Integer resd 1
.Scope0__DWXSIZE_Integer resd 1
.Scope0__DWYSIZE_Integer resd 1
.Scope0__DWXCOUNTCHARS_Integer resd 1
.Scope0__DWYCOUNTCHARS_Integer resd 1
.Scope0__DWFILLATTRIBUTE_Integer resd 1
.Scope0__DWFLAGS_Integer resd 1
.Scope0__WSHOWWINDOW_Integer resd 1
.Scope0__CBRESERVED2_Integer resd 1
.Scope0__LPRESERVED2_Integer resd 1
.Scope0__HSTDINPUT_Integer resd 1
.Scope0__HSTDOUTPUT_Integer resd 1
.Scope0__HSTDERROR_Integer resd 1
ENDSTRUC
STRUC Scope0__NMHDR_TYPE
.Scope0__HWNDFROM_Integer resd 1
.Scope0__IDFROM_Integer resd 1
.Scope0__CODE_Integer resd 1
ENDSTRUC
STRUC Scope0__WNDCLASS_TYPE
.Scope0__STYLE_Integer resd 1
.Scope0__LPFNWNDPROC_Integer resd 1
.Scope0__CBCLSEXTRA_Integer resd 1
.Scope0__CBWNDEXTRA2_Integer resd 1
.Scope0__HINSTANCE_Integer resd 1
.Scope0__HICON_Integer resd 1
.Scope0__HCURSOR_Integer resd 1
.Scope0__HBRBACKGROUND_Integer resd 1
.Scope0__LPSZMENUNAME_String resd 1
.Scope0__LPSZCLASSNAME_String resd 1
ENDSTRUC
STRUC Scope0__WNDCLASSEX_TYPE
.Scope0__CBSIZE_Integer resd 1
.Scope0__STYLE_Integer resd 1
.Scope0__LPFNWNDPROC_Integer resd 1
.Scope0__CBCLSEXTRA_Integer resd 1
.Scope0__CBWNDEXTRA_Integer resd 1
.Scope0__HINSTANCE_Integer resd 1
.Scope0__HICON_Integer resd 1
.Scope0__HCURSOR_Integer resd 1
.Scope0__HBRBACKGROUND_Integer resd 1
.Scope0__LPSZMENUNAME_String resd 1
.Scope0__LPSZCLASSNAME_String resd 1
.Scope0__HICONSM_Integer resd 1
ENDSTRUC
STRUC Scope0__CREATESTRUCT_TYPE
.Scope0__LPCREATEPARAMS_Integer resd 1
.Scope0__HINSTANCE_Integer resd 1
.Scope0__HMENU_Integer resd 1
.Scope0__HWNDPARENT_Integer resd 1
.Scope0__CY_Integer resd 1
.Scope0__CX_Integer resd 1
.Scope0__Y_Integer resd 1
.Scope0__X_Integer resd 1
.Scope0__STYLE_Integer resd 1
.Scope0__LPSZNAME_String resd 1
.Scope0__LPSZCLASS_String resd 1
.Scope0__EXSTYLE_Integer resd 1
ENDSTRUC
STRUC Scope0__MSG_TYPE
.Scope0__HWND_Integer resd 1
.Scope0__MESSAGE_Integer resd 1
.Scope0__WPARAM_Integer resd 1
.Scope0__LPARAM_Integer resd 1
.Scope0__TIME_Integer resd 1
.Scope0__X_Integer resd 1
.Scope0__Y_Integer resd 1
ENDSTRUC
STRUC Scope0__PAINTSTRUCT_TYPE
.Scope0__HDC_Integer resd 1
.Scope0__FERASE_Integer resd 1
.Scope0__LEFT_Integer resd 1
.Scope0__TOP_Integer resd 1
.Scope0__RIGHT_Integer resd 1
.Scope0__BOTTOM_Integer resd 1
.Scope0__FRESTORE_Integer resd 1
.Scope0__FINCUPDATE_Integer resd 1
.Scope0__RGBRESERVED1_Integer resd 1
.Scope0__RGBRESERVED2_Integer resd 1
.Scope0__RGBRESERVED3_Integer resd 1
.Scope0__RGBRESERVED4_Integer resd 1
.Scope0__RGBRESERVED5_Integer resd 1
.Scope0__RGBRESERVED6_Integer resd 1
.Scope0__RGBRESERVED7_Integer resd 1
.Scope0__RGBRESERVED8_Integer resd 1
ENDSTRUC
_BEGINPAINT dd 0
_BEGINPAINT_LibHandle dd 0
_BEGINPAINT_Alias db "BeginPaint",0
_BEGINPAINT_Lib db "user32",0
_BEGINPAINT_Call db 0
_ENDPAINT dd 0
_ENDPAINT_LibHandle dd 0
_ENDPAINT_Alias db "EndPaint",0
_ENDPAINT_Lib db "user32",0
_ENDPAINT_Call db 0
_TEXTOUT dd 0
_TEXTOUT_LibHandle dd 0
_TEXTOUT_Alias db "TextOutA",0
_TEXTOUT_Lib db "gdi32",0
_TEXTOUT_Call db 0
_DRAWTEXT dd 0
_DRAWTEXT_LibHandle dd 0
_DRAWTEXT_Alias db "DrawTextA",0
_DRAWTEXT_Lib db "user32",0
_DRAWTEXT_Call db 0
_ENUMCHILDWINDOWS dd 0
_ENUMCHILDWINDOWS_LibHandle dd 0
_ENUMCHILDWINDOWS_Alias db "EnumChildWindows",0
_ENUMCHILDWINDOWS_Lib db "user32",0
_ENUMCHILDWINDOWS_Call db 0
_GETTOPWINDOW dd 0
_GETTOPWINDOW_LibHandle dd 0
_GETTOPWINDOW_Alias db "GetTopWindow",0
_GETTOPWINDOW_Lib db "user32",0
_GETTOPWINDOW_Call db 0
_GETNEXTWINDOW dd 0
_GETNEXTWINDOW_LibHandle dd 0
_GETNEXTWINDOW_Alias db "GetWindow",0
_GETNEXTWINDOW_Lib db "user32",0
_GETNEXTWINDOW_Call db 0
_GETCLIENTRECT dd 0
_GETCLIENTRECT_LibHandle dd 0
_GETCLIENTRECT_Alias db "GetClientRect",0
_GETCLIENTRECT_Lib db "user32",0
_GETCLIENTRECT_Call db 0
_GETWINDOWRECT dd 0
_GETWINDOWRECT_LibHandle dd 0
_GETWINDOWRECT_Alias db "GetWindowRect",0
_GETWINDOWRECT_Lib db "user32",0
_GETWINDOWRECT_Call db 0
_REGISTERCLASS dd 0
_REGISTERCLASS_LibHandle dd 0
_REGISTERCLASS_Alias db "RegisterClassA",0
_REGISTERCLASS_Lib db "user32",0
_REGISTERCLASS_Call db 0
_REGISTERCLASSEX dd 0
_REGISTERCLASSEX_LibHandle dd 0
_REGISTERCLASSEX_Alias db "RegisterClassExA",0
_REGISTERCLASSEX_Lib db "USER32",0
_REGISTERCLASSEX_Call db 0
_UNREGISTERCLASS dd 0
_UNREGISTERCLASS_LibHandle dd 0
_UNREGISTERCLASS_Alias db "UnregisterClassA",0
_UNREGISTERCLASS_Lib db "user32",0
_UNREGISTERCLASS_Call db 0
_GETMESSAGE dd 0
_GETMESSAGE_LibHandle dd 0
_GETMESSAGE_Alias db "GetMessageA",0
_GETMESSAGE_Lib db "user32",0
_GETMESSAGE_Call db 0
_TRANSLATEMESSAGE dd 0
_TRANSLATEMESSAGE_LibHandle dd 0
_TRANSLATEMESSAGE_Alias db "TranslateMessage",0
_TRANSLATEMESSAGE_Lib db "user32",0
_TRANSLATEMESSAGE_Call db 0
_DISPATCHMESSAGE dd 0
_DISPATCHMESSAGE_LibHandle dd 0
_DISPATCHMESSAGE_Alias db "DispatchMessageA",0
_DISPATCHMESSAGE_Lib db "user32",0
_DISPATCHMESSAGE_Call db 0
_SENDMESSAGE dd 0
_SENDMESSAGE_LibHandle dd 0
_SENDMESSAGE_Alias db "SendMessageA",0
_SENDMESSAGE_Lib db "user32",0
_SENDMESSAGE_Call db 0
_LOADBITMAP dd 0
_LOADBITMAP_LibHandle dd 0
_LOADBITMAP_Alias db "LoadBitmapA",0
_LOADBITMAP_Lib db "user32",0
_LOADBITMAP_Call db 0
_LOADCURSOR dd 0
_LOADCURSOR_LibHandle dd 0
_LOADCURSOR_Alias db "LoadCursorA",0
_LOADCURSOR_Lib db "USER32",0
_LOADCURSOR_Call db 0
_LOADICON dd 0
_LOADICON_LibHandle dd 0
_LOADICON_Alias db "LoadIconA",0
_LOADICON_Lib db "user32",0
_LOADICON_Call db 0
_DESTROYICON dd 0
_DESTROYICON_LibHandle dd 0
_DESTROYICON_Alias db "DestroyIcon",0
_DESTROYICON_Lib db "user32",0
_DESTROYICON_Call db 0
_GETCURSOR dd 0
_GETCURSOR_LibHandle dd 0
_GETCURSOR_Alias db "GetCursor",0
_GETCURSOR_Lib db "user32",0
_GETCURSOR_Call db 0
_SETCURSOR dd 0
_SETCURSOR_LibHandle dd 0
_SETCURSOR_Alias db "SetCursor",0
_SETCURSOR_Lib db "user32",0
_SETCURSOR_Call db 0
_COPYICON dd 0
_COPYICON_LibHandle dd 0
_COPYICON_Alias db "CopyIcon",0
_COPYICON_Lib db "user32",0
_COPYICON_Call db 0
_SETSYSTEMCURSOR dd 0
_SETSYSTEMCURSOR_LibHandle dd 0
_SETSYSTEMCURSOR_Alias db "SetSystemCursor",0
_SETSYSTEMCURSOR_Lib db "user32",0
_SETSYSTEMCURSOR_Call db 0
_CREATECOMPATIBLEDC dd 0
_CREATECOMPATIBLEDC_LibHandle dd 0
_CREATECOMPATIBLEDC_Alias db "CreateCompatibleDC",0
_CREATECOMPATIBLEDC_Lib db "gdi32",0
_CREATECOMPATIBLEDC_Call db 0
_CREATEWINDOWEX dd 0
_CREATEWINDOWEX_LibHandle dd 0
_CREATEWINDOWEX_Alias db "CreateWindowExA",0
_CREATEWINDOWEX_Lib db "user32",0
_CREATEWINDOWEX_Call db 0
_UPDATEWINDOW dd 0
_UPDATEWINDOW_LibHandle dd 0
_UPDATEWINDOW_Alias db "UpdateWindow",0
_UPDATEWINDOW_Lib db "USER32",0
_UPDATEWINDOW_Call db 0
_SHOWWINDOW dd 0
_SHOWWINDOW_LibHandle dd 0
_SHOWWINDOW_Alias db "ShowWindow",0
_SHOWWINDOW_Lib db "USER32",0
_SHOWWINDOW_Call db 0
_DEFWINDOWPROC dd 0
_DEFWINDOWPROC_LibHandle dd 0
_DEFWINDOWPROC_Alias db "DefWindowProcA",0
_DEFWINDOWPROC_Lib db "user32",0
_DEFWINDOWPROC_Call db 0
_POSTQUITMESSAGE dd 0
_POSTQUITMESSAGE_LibHandle dd 0
_POSTQUITMESSAGE_Alias db "PostQuitMessage",0
_POSTQUITMESSAGE_Lib db "user32",0
_POSTQUITMESSAGE_Call db 0
_GETMODULEHANDLE dd 0
_GETMODULEHANDLE_LibHandle dd 0
_GETMODULEHANDLE_Alias db "GetModuleHandleA",0
_GETMODULEHANDLE_Lib db "kernel32",0
_GETMODULEHANDLE_Call db 0
_GETACTIVEWINDOW dd 0
_GETACTIVEWINDOW_LibHandle dd 0
_GETACTIVEWINDOW_Alias db "GetActiveWindow",0
_GETACTIVEWINDOW_Lib db "user32",0
_GETACTIVEWINDOW_Call db 0
_EXITPROCESS dd 0
_EXITPROCESS_LibHandle dd 0
_EXITPROCESS_Alias db "ExitProcess",0
_EXITPROCESS_Lib db "kernel32",0
_EXITPROCESS_Call db 0
_MSGBOX dd 0
_MSGBOX_LibHandle dd 0
_MSGBOX_Alias db "MessageBoxA",0
_MSGBOX_Lib db "user32",0
_MSGBOX_Call db 0
_MESSAGEBOX dd 0
_MESSAGEBOX_LibHandle dd 0
_MESSAGEBOX_Alias db "MessageBoxA",0
_MESSAGEBOX_Lib db "user32",0
_MESSAGEBOX_Call db 0
_GETLASTERROR dd 0
_GETLASTERROR_LibHandle dd 0
_GETLASTERROR_Alias db "GetLastError",0
_GETLASTERROR_Lib db "kernel32",0
_GETLASTERROR_Call db 0
_SETLASTERROR dd 0
_SETLASTERROR_LibHandle dd 0
_SETLASTERROR_Alias db "SetLastError",0
_SETLASTERROR_Lib db "kernel32",0
_SETLASTERROR_Call db 0
_FORMATMESSAGE dd 0
_FORMATMESSAGE_LibHandle dd 0
_FORMATMESSAGE_Alias db "FormatMessageA",0
_FORMATMESSAGE_Lib db "kernel32",0
_FORMATMESSAGE_Call db 0
_LOCALFREE dd 0
_LOCALFREE_LibHandle dd 0
_LOCALFREE_Alias db "LocalFree",0
_LOCALFREE_Lib db "kernel32",0
_LOCALFREE_Call db 0
_GETWINDOWLONG dd 0
_GETWINDOWLONG_LibHandle dd 0
_GETWINDOWLONG_Alias db "GetWindowIntegerA",0
_GETWINDOWLONG_Lib db "user32",0
_GETWINDOWLONG_Call db 0
_SETWINDOWLONG dd 0
_SETWINDOWLONG_LibHandle dd 0
_SETWINDOWLONG_Alias db "SetWindowIntegerA",0
_SETWINDOWLONG_Lib db "user32",0
_SETWINDOWLONG_Call db 0
_CALLWINDOWPROC dd 0
_CALLWINDOWPROC_LibHandle dd 0
_CALLWINDOWPROC_Alias db "CallWindowProcA",0
_CALLWINDOWPROC_Lib db "user32",0
_CALLWINDOWPROC_Call db 0
_GETCLASSLONG dd 0
_GETCLASSLONG_LibHandle dd 0
_GETCLASSLONG_Alias db "GetClassLongA",0
_GETCLASSLONG_Lib db "user32",0
_GETCLASSLONG_Call db 0
_SETCLASSLONG dd 0
_SETCLASSLONG_LibHandle dd 0
_SETCLASSLONG_Alias db "SetClassLongA",0
_SETCLASSLONG_Lib db "user32",0
_SETCLASSLONG_Call db 0
_GETDESKTOPWINDOW dd 0
_GETDESKTOPWINDOW_LibHandle dd 0
_GETDESKTOPWINDOW_Alias db "GetDesktopWindow",0
_GETDESKTOPWINDOW_Lib db "user32",0
_GETDESKTOPWINDOW_Call db 0
_GETPROCADDRESS dd 0
_GETPROCADDRESS_LibHandle dd 0
_GETPROCADDRESS_Alias db "GetProcAddress",0
_GETPROCADDRESS_Lib db "kernel32",0
_GETPROCADDRESS_Call db 0
_DIALOGBOXPARAM dd 0
_DIALOGBOXPARAM_LibHandle dd 0
_DIALOGBOXPARAM_Alias db "DialogBoxParamA",0
_DIALOGBOXPARAM_Lib db "user32",0
_DIALOGBOXPARAM_Call db 0
_ENDDIALOG dd 0
_ENDDIALOG_LibHandle dd 0
_ENDDIALOG_Alias db "EndDialog",0
_ENDDIALOG_Lib db "user32",0
_ENDDIALOG_Call db 0
_GETDLGITEM dd 0
_GETDLGITEM_LibHandle dd 0
_GETDLGITEM_Alias db "GetDlgItem",0
_GETDLGITEM_Lib db "user32",0
_GETDLGITEM_Call db 0
_INITCOMMONCONTROLS dd 0
_INITCOMMONCONTROLS_LibHandle dd 0
_INITCOMMONCONTROLS_Alias db "InitCommonControls",0
_INITCOMMONCONTROLS_Lib db "comctl32",0
_INITCOMMONCONTROLS_Call db 0
_INVALIDATERECT dd 0
_INVALIDATERECT_LibHandle dd 0
_INVALIDATERECT_Alias db "InvalidateRect",0
_INVALIDATERECT_Lib db "user32",0
_INVALIDATERECT_Call db 0
_CREATEPEN dd 0
_CREATEPEN_LibHandle dd 0
_CREATEPEN_Alias db "CreatePen",0
_CREATEPEN_Lib db "gdi32",0
_CREATEPEN_Call db 0
_SELECTOBJECT dd 0
_SELECTOBJECT_LibHandle dd 0
_SELECTOBJECT_Alias db "SelectObject",0
_SELECTOBJECT_Lib db "gdi32",0
_SELECTOBJECT_Call db 0
_GETSTOCKOBJECT dd 0
_GETSTOCKOBJECT_LibHandle dd 0
_GETSTOCKOBJECT_Alias db "GetStockObject",0
_GETSTOCKOBJECT_Lib db "gdi32",0
_GETSTOCKOBJECT_Call db 0
_CREATESOLIDBRUSH dd 0
_CREATESOLIDBRUSH_LibHandle dd 0
_CREATESOLIDBRUSH_Alias db "CreateSolidBrush",0
_CREATESOLIDBRUSH_Lib db "gdi32",0
_CREATESOLIDBRUSH_Call db 0
_RECTANGLE dd 0
_RECTANGLE_LibHandle dd 0
_RECTANGLE_Alias db "Rectangle",0
_RECTANGLE_Lib db "gdi32",0
_RECTANGLE_Call db 0
_DELETEOBJECT dd 0
_DELETEOBJECT_LibHandle dd 0
_DELETEOBJECT_Alias db "DeleteObject",0
_DELETEOBJECT_Lib db "gdi32",0
_DELETEOBJECT_Call db 0
_DELETEDC dd 0
_DELETEDC_LibHandle dd 0
_DELETEDC_Alias db "DeleteDC",0
_DELETEDC_Lib db "gdi32",0
_DELETEDC_Call db 0
_DESTROYWINDOW dd 0
_DESTROYWINDOW_LibHandle dd 0
_DESTROYWINDOW_Alias db "DestroyWindow",0
_DESTROYWINDOW_Lib db "user32",0
_DESTROYWINDOW_Call db 0
_SETPIXEL dd 0
_SETPIXEL_LibHandle dd 0
_SETPIXEL_Alias db "SetPixel",0
_SETPIXEL_Lib db "gdi32",0
_SETPIXEL_Call db 0
_BITBLT dd 0
_BITBLT_LibHandle dd 0
_BITBLT_Alias db "BitBlt",0
_BITBLT_Lib db "gdi32",0
_BITBLT_Call db 0
_CREATEMENU dd 0
_CREATEMENU_LibHandle dd 0
_CREATEMENU_Alias db "CreateMenu",0
_CREATEMENU_Lib db "user32",0
_CREATEMENU_Call db 0
_APPENDMENU dd 0
_APPENDMENU_LibHandle dd 0
_APPENDMENU_Alias db "AppendMenuA",0
_APPENDMENU_Lib db "user32",0
_APPENDMENU_Call db 0
_POSTMESSAGE dd 0
_POSTMESSAGE_LibHandle dd 0
_POSTMESSAGE_Alias db "PostMessageA",0
_POSTMESSAGE_Lib db "user32",0
_POSTMESSAGE_Call db 0
_GETSYSTEMMETRICS dd 0
_GETSYSTEMMETRICS_LibHandle dd 0
_GETSYSTEMMETRICS_Alias db "GetSystemMetrics",0
_GETSYSTEMMETRICS_Lib db "user32",0
_GETSYSTEMMETRICS_Call db 0
_SETWINDOWPOS dd 0
_SETWINDOWPOS_LibHandle dd 0
_SETWINDOWPOS_Alias db "SetWindowPos",0
_SETWINDOWPOS_Lib db "user32",0
_SETWINDOWPOS_Call db 0
_ENABLEWINDOW dd 0
_ENABLEWINDOW_LibHandle dd 0
_ENABLEWINDOW_Alias db "EnableWindow",0
_ENABLEWINDOW_Lib db "user32",0
_ENABLEWINDOW_Call db 0
_PLAYSOUND dd 0
_PLAYSOUND_LibHandle dd 0
_PLAYSOUND_Alias db "PlaySoundA",0
_PLAYSOUND_Lib db "winmm",0
_PLAYSOUND_Call db 0
STRUC Scope68__MMTIME_TYPE
.Scope68__WTYPE_Integer resd 1
.Scope68__U_Integer resd 1
ENDSTRUC
_TIMESETEVENT dd 0
_TIMESETEVENT_LibHandle dd 0
_TIMESETEVENT_Alias db "timeSetEvent",0
_TIMESETEVENT_Lib db "winmm.dll",0
_TIMESETEVENT_Call db 0
_TIMEKILLEVENT dd 0
_TIMEKILLEVENT_LibHandle dd 0
_TIMEKILLEVENT_Alias db "timeKillEvent",0
_TIMEKILLEVENT_Lib db "winmm.dll",0
_TIMEKILLEVENT_Call db 0
_CREATEFONT dd 0
_CREATEFONT_LibHandle dd 0
_CREATEFONT_Alias db "CreateFontA",0
_CREATEFONT_Lib db "gdi32",0
_CREATEFONT_Call db 0
Number_1 dq 0.0
String_2 db "Message:",0
Number_3 dq 0.0
_ABS dd 0
_ABS_LibHandle dd 0
_ABS_Alias db "fabs",0
_ABS_Lib db "msvcrt.dll",0
_ABS_Call db 1
_ACOS dd 0
_ACOS_LibHandle dd 0
_ACOS_Alias db "acos",0
_ACOS_Lib db "msvcrt.dll",0
_ACOS_Call db 1
_ASIN dd 0
_ASIN_LibHandle dd 0
_ASIN_Alias db "asin",0
_ASIN_Lib db "msvcrt.dll",0
_ASIN_Call db 1
_ATAN dd 0
_ATAN_LibHandle dd 0
_ATAN_Alias db "atan",0
_ATAN_Lib db "msvcrt.dll",0
_ATAN_Call db 1
_ATN dd 0
_ATN_LibHandle dd 0
_ATN_Alias db "atan",0
_ATN_Lib db "msvcrt.dll",0
_ATN_Call db 1
_CEIL dd 0
_CEIL_LibHandle dd 0
_CEIL_Alias db "ceil",0
_CEIL_Lib db "msvcrt.dll",0
_CEIL_Call db 1
_EXP dd 0
_EXP_LibHandle dd 0
_EXP_Alias db "exp",0
_EXP_Lib db "msvcrt.dll",0
_EXP_Call db 1
_FLOOR dd 0
_FLOOR_LibHandle dd 0
_FLOOR_Alias db "floor",0
_FLOOR_Lib db "msvcrt.dll",0
_FLOOR_Call db 1
_LOG dd 0
_LOG_LibHandle dd 0
_LOG_Alias db "log",0
_LOG_Lib db "msvcrt.dll",0
_LOG_Call db 1
_SQR dd 0
_SQR_LibHandle dd 0
_SQR_Alias db "sqrt",0
_SQR_Lib db "msvcrt.dll",0
_SQR_Call db 1
_SQRT dd 0
_SQRT_LibHandle dd 0
_SQRT_Alias db "sqrt",0
_SQRT_Lib db "msvcrt.dll",0
_SQRT_Call db 1
_TAN dd 0
_TAN_LibHandle dd 0
_TAN_Alias db "tan",0
_TAN_Lib db "msvcrt.dll",0
_TAN_Call db 1
_TIMER dd 0
_TIMER_LibHandle dd 0
_TIMER_Alias db "timer",0
_TIMER_Lib db "msvcrt.dll",0
_TIMER_Call db 1
_POW dd 0
_POW_LibHandle dd 0
_POW_Alias db "pow",0
_POW_Lib db "msvcrt.dll",0
_POW_Call db 1
Number_4 dq 1.0
Number_5 dq 1.0
_ISALNUM dd 0
_ISALNUM_LibHandle dd 0
_ISALNUM_Alias db "isalnum",0
_ISALNUM_Lib db "msvcrt.dll",0
_ISALNUM_Call db 1
_ISALPHA dd 0
_ISALPHA_LibHandle dd 0
_ISALPHA_Alias db "isalpha",0
_ISALPHA_Lib db "msvcrt.dll",0
_ISALPHA_Call db 1
_ISCNTRL dd 0
_ISCNTRL_LibHandle dd 0
_ISCNTRL_Alias db "iscntrl",0
_ISCNTRL_Lib db "msvcrt.dll",0
_ISCNTRL_Call db 1
_ISDIGIT dd 0
_ISDIGIT_LibHandle dd 0
_ISDIGIT_Alias db "isdigit",0
_ISDIGIT_Lib db "msvcrt.dll",0
_ISDIGIT_Call db 1
_ISGRAPH dd 0
_ISGRAPH_LibHandle dd 0
_ISGRAPH_Alias db "isgraph",0
_ISGRAPH_Lib db "msvcrt.dll",0
_ISGRAPH_Call db 1
_ISLOWER dd 0
_ISLOWER_LibHandle dd 0
_ISLOWER_Alias db "islower",0
_ISLOWER_Lib db "msvcrt.dll",0
_ISLOWER_Call db 1
_ISPRINT dd 0
_ISPRINT_LibHandle dd 0
_ISPRINT_Alias db "isprint",0
_ISPRINT_Lib db "msvcrt.dll",0
_ISPRINT_Call db 1
_ISPUNCT dd 0
_ISPUNCT_LibHandle dd 0
_ISPUNCT_Alias db "ispunct",0
_ISPUNCT_Lib db "msvcrt.dll",0
_ISPUNCT_Call db 1
_ISSPACE dd 0
_ISSPACE_LibHandle dd 0
_ISSPACE_Alias db "isspace",0
_ISSPACE_Lib db "msvcrt.dll",0
_ISSPACE_Call db 1
_ISUPPER dd 0
_ISUPPER_LibHandle dd 0
_ISUPPER_Alias db "isupper",0
_ISUPPER_Lib db "msvcrt.dll",0
_ISUPPER_Call db 1
_ISXDIGIT dd 0
_ISXDIGIT_LibHandle dd 0
_ISXDIGIT_Alias db "isxdigit",0
_ISXDIGIT_Lib db "msvcrt.dll",0
_ISXDIGIT_Call db 1
_TOLOWER dd 0
_TOLOWER_LibHandle dd 0
_TOLOWER_Alias db "tolower",0
_TOLOWER_Lib db "msvcrt.dll",0
_TOLOWER_Call db 1
_TOUPPER dd 0
_TOUPPER_LibHandle dd 0
_TOUPPER_Alias db "toupper",0
_TOUPPER_Lib db "msvcrt.dll",0
_TOUPPER_Call db 1
STRUC Scope113__SECURITYATTRIBUTES_TYPE
.Scope113__LENGTH_Integer resd 1
.Scope113__SECURITYDESCRIPTOR_Integer resd 1
.Scope113__INHERITHANDLE_Integer resd 1
ENDSTRUC
_CHDIR dd 0
_CHDIR_LibHandle dd 0
_CHDIR_Alias db "SetCurrentDirectoryA",0
_CHDIR_Lib db "kernel32.dll",0
_CHDIR_Call db 0
_KILL dd 0
_KILL_LibHandle dd 0
_KILL_Alias db "DeleteFileA",0
_KILL_Lib db "kernel32.dll",0
_KILL_Call db 0
_CREATEDIRECTORY dd 0
_CREATEDIRECTORY_LibHandle dd 0
_CREATEDIRECTORY_Alias db "CreateDirectoryA",0
_CREATEDIRECTORY_Lib db "kernel32.dll",0
_CREATEDIRECTORY_Call db 0
_RANDOMIZE dd 0
_RANDOMIZE_LibHandle dd 0
_RANDOMIZE_Alias db "srand",0
_RANDOMIZE_Lib db "msvcrt.dll",0
_RANDOMIZE_Call db 1
_RAND dd 0
_RAND_LibHandle dd 0
_RAND_Alias db "rand",0
_RAND_Lib db "msvcrt.dll",0
_RAND_Call db 1
_ENVIRON___ dd 0
_ENVIRON____LibHandle dd 0
_ENVIRON____Alias db "getenv",0
_ENVIRON____Lib db "msvcrt.dll",0
_ENVIRON____Call db 1
_RENAME dd 0
_RENAME_LibHandle dd 0
_RENAME_Alias db "MoveFileA",0
_RENAME_Lib db "kernel32.dll",0
_RENAME_Call db 0
_RMDIR dd 0
_RMDIR_LibHandle dd 0
_RMDIR_Alias db "RemoveDirectoryA",0
_RMDIR_Lib db "kernel32.dll",0
_RMDIR_Call db 0
_RUN dd 0
_RUN_LibHandle dd 0
_RUN_Alias db "system",0
_RUN_Lib db "msvcrt.dll",0
_RUN_Call db 1
Scope123__SECATTRIBUTES_UDT ISTRUC Scope113__SECURITYATTRIBUTES_TYPE
IEND
Number_6 dq 32767.0
Number_7 dq 0.0
Number_8 dq 0.0
Number_9 dq 0.0
Number_10 dq 1.0
Number_11 dq 0.0
Number_12 dq -1.0
_LEN dd 0
_LEN_LibHandle dd 0
_LEN_Alias db "lstrlenA",0
_LEN_Lib db "Kernel32.dll",0
_LEN_Call db 0
_LSTRCPY dd 0
_LSTRCPY_LibHandle dd 0
_LSTRCPY_Alias db "lstrcpyA",0
_LSTRCPY_Lib db "Kernel32.dll",0
_LSTRCPY_Call db 0
Number_13 dq 1.0
String_14 db "",0
String_15 db " ",0
Number_16 dq 1.0
Number_17 dq 1.0
Number_18 dq 1.0
Number_19 dq 1.0
Number_20 dq 1.0
Number_21 dq 1.0
Number_22 dq 1.0
Number_23 dq 1.0
String_24 db "",0
Number_25 dq 1.0
Number_26 dq 1.0
Number_27 dq 1.0
Number_28 dq 1.0
String_29 db "",0
Number_30 dq 1.0
Number_31 dq 1.0
Number_32 dq 1.0
Number_33 dq 1.0
Number_34 dq 1.0
Number_35 dq 1.0
Number_36 dq 0.0
Number_37 dq 0.0
Number_38 dq 1.0
Number_39 dq 1.0
Number_40 dq 0.0
Number_41 dq 0.0
Number_42 dq 1.0
Number_43 dq 1.0
Number_44 dq 0.0
Number_45 dq 0.0
Number_46 dq 1.0
String_47 db " ",0
Number_48 dq 1.0
Number_49 dq 1.0
Number_50 dq 1.0
Number_51 dq 1.0
Number_52 dq 1.0
Number_53 dq 1.0
Number_54 dq 1.0
Number_55 dq 0.0
Number_56 dq 0.0
Number_57 dq 1.0
Number_58 dq 0.0
Number_59 dq 1.0
Number_60 dq 1.0
Number_61 dq 0.0
Number_62 dq 0.0
Number_63 dq 0.0
Number_64 dq 1.0
Number_65 dq 0.0
Number_66 dq 1.0
String_67 db " ",0
Number_68 dq 1.0
Number_69 dq 1.0
Number_70 dq 1.0
Number_71 dq 1.0
Number_72 dq 1.0
Number_73 dq 1.0
Number_74 dq 1.0
Number_75 dq 1.0
Number_76 dq 1.0
Number_77 dq 0.0
Number_78 dq 0.0
Number_79 dq 0.0
Number_80 dq 1.0
Number_81 dq 1.0
Number_82 dq 1.0
Number_83 dq 10.0
Number_84 dq 13.0
Number_85 dq 13.0
Number_86 dq 13.0
Number_87 dq 10.0
Number_88 dq 11.0
Number_89 dq 1.0
Number_90 dq 0.0
Number_91 dq 1.0
Number_92 dq 1.0
_GCVT dd 0
_GCVT_LibHandle dd 0
_GCVT_Alias db "_gcvt",0
_GCVT_Lib db "msvcrt.dll",0
_GCVT_Call db 1
_VAL dd 0
_VAL_LibHandle dd 0
_VAL_Alias db "atof",0
_VAL_Lib db "msvcrt.dll",0
_VAL_Call db 1
Number_93 dq 100.0
Number_94 dq 1.0
_GETSTDHANDLE dd 0
_GETSTDHANDLE_LibHandle dd 0
_GETSTDHANDLE_Alias db "GetStdHandle",0
_GETSTDHANDLE_Lib db "Kernel32.dll",0
_GETSTDHANDLE_Call db 0
_SETCONSOLETITLEWINAPI dd 0
_SETCONSOLETITLEWINAPI_LibHandle dd 0
_SETCONSOLETITLEWINAPI_Alias db "SetConsoleTitleA",0
_SETCONSOLETITLEWINAPI_Lib db "Kernel32.dll",0
_SETCONSOLETITLEWINAPI_Call db 0
_SETCONSOLETEXTATTRIBUTE dd 0
_SETCONSOLETEXTATTRIBUTE_LibHandle dd 0
_SETCONSOLETEXTATTRIBUTE_Alias db "SetConsoleTextAttribute",0
_SETCONSOLETEXTATTRIBUTE_Lib db "Kernel32.dll",0
_SETCONSOLETEXTATTRIBUTE_Call db 0
Number_95 dq -11.0
Number_96 dq -10.0
Number_97 dq -11.0
STRUC Scope168__SYSTEMTIME_TYPE
.Scope168__WYEAR_Integer resd 1
.Scope168__WMONTH_Integer resd 1
.Scope168__WDAYOFWEEK_Integer resd 1
.Scope168__WDAY_Integer resd 1
.Scope168__WHOUR_Integer resd 1
.Scope168__WMINUTE_Integer resd 1
.Scope168__WSECOND_Integer resd 1
.Scope168__WMILLISECONDS_Integer resd 1
ENDSTRUC
STRUC Scope168__TM_TYPE
.Scope168__HOUR_Integer resd 1
.Scope168__ISDST_Integer resd 1
.Scope168__MDAY_Integer resd 1
.Scope168__MIN_Integer resd 1
.Scope168__MON_Integer resd 1
.Scope168__SEC_Integer resd 1
.Scope168__WDAY_Integer resd 1
.Scope168__YDAY_Integer resd 1
.Scope168__YEAR_Integer resd 1
ENDSTRUC
_GETLOCALTIME dd 0
_GETLOCALTIME_LibHandle dd 0
_GETLOCALTIME_Alias db "GetLocalTime",0
_GETLOCALTIME_Lib db "kernel32.dll",0
_GETLOCALTIME_Call db 0
Scope169__RETURNTIME_UDT ISTRUC Scope168__SYSTEMTIME_TYPE
IEND
_FOPEN dd 0
_FOPEN_LibHandle dd 0
_FOPEN_Alias db "fopen",0
_FOPEN_Lib db "Msvcrt.dll",0
_FOPEN_Call db 1
_FWRITE dd 0
_FWRITE_LibHandle dd 0
_FWRITE_Alias db "fwrite",0
_FWRITE_Lib db "Msvcrt.dll",0
_FWRITE_Call db 1
_FWRITE2 dd 0
_FWRITE2_LibHandle dd 0
_FWRITE2_Alias db "fwrite",0
_FWRITE2_Lib db "Msvcrt.dll",0
_FWRITE2_Call db 1
_FPUTS dd 0
_FPUTS_LibHandle dd 0
_FPUTS_Alias db "fputs",0
_FPUTS_Lib db "Msvcrt.dll",0
_FPUTS_Call db 1
_FREAD dd 0
_FREAD_LibHandle dd 0
_FREAD_Alias db "fread",0
_FREAD_Lib db "Msvcrt.dll",0
_FREAD_Call db 1
_FREAD2 dd 0
_FREAD2_LibHandle dd 0
_FREAD2_Alias db "fread",0
_FREAD2_Lib db "Msvcrt.dll",0
_FREAD2_Call db 1
_FGETS dd 0
_FGETS_LibHandle dd 0
_FGETS_Alias db "fgets",0
_FGETS_Lib db "Msvcrt.dll",0
_FGETS_Call db 1
_FTELL dd 0
_FTELL_LibHandle dd 0
_FTELL_Alias db "ftell",0
_FTELL_Lib db "Msvcrt.dll",0
_FTELL_Call db 1
_FSEEK dd 0
_FSEEK_LibHandle dd 0
_FSEEK_Alias db "fseek",0
_FSEEK_Lib db "Msvcrt.dll",0
_FSEEK_Call db 1
_FGETPOS dd 0
_FGETPOS_LibHandle dd 0
_FGETPOS_Alias db "fgetpos",0
_FGETPOS_Lib db "Msvcrt.dll",0
_FGETPOS_Call db 1
_FSETPOS dd 0
_FSETPOS_LibHandle dd 0
_FSETPOS_Alias db "fsetpos",0
_FSETPOS_Lib db "Msvcrt.dll",0
_FSETPOS_Call db 1
_FCLOSE dd 0
_FCLOSE_LibHandle dd 0
_FCLOSE_Alias db "fclose",0
_FCLOSE_Lib db "Msvcrt.dll",0
_FCLOSE_Call db 1
String_98 db "t",0
Number_99 dq 1.0
Number_100 dq 1.0
Number_101 dq 13.0
Number_102 dq 10.0
Number_103 dq 0.0
Number_104 dq 2.0
Number_105 dq 0.0
Number_106 dq 1.0
Number_107 dq 1.0
Number_108 dq 4.0
Number_109 dq 1.0
Number_110 dq 4.0
Number_111 dq 1.0
Number_112 dq 8.0
Number_113 dq 1.0
Number_114 dq 8.0
Number_115 dq 1.0
Number_116 dq 0.0
_LTOA dd 0
_LTOA_LibHandle dd 0
_LTOA_Alias db "_ltoa",0
_LTOA_Lib db "msvcrt.dll",0
_LTOA_Call db 1
Number_117 dq 40.0
Number_118 dq 0.0
Number_119 dq 0.0
_ERRORMSGBOX dd 0
_ERRORMSGBOX_LibHandle dd 0
_ERRORMSGBOX_Alias db "MessageBoxA",0
_ERRORMSGBOX_Lib db "user32",0
_ERRORMSGBOX_Call db 0
Number_120 dq 40.0
Number_121 dq 10.0
String_122 db "System error code ",0
Number_123 dq 256.0
Number_124 dq 4096.0
Number_125 dq 512.0
Number_126 dq 0.0
Number_127 dq 0.0
Number_128 dq 0.0
Number_129 dq 0.0
Number_130 dq 0.0
Number_131 dq 0.0
Internal_HInstance dd 0
Internal_CommandLine dd 0
NoConsoleMessage db 'Error - Cannot access the console handles for Input/Output.',0
ConsoleTemp dd 0
ConsoleNewLine db 10,0
ConsoleClear db 'CLS',0
ConsolePause db 'PAUSE',0
HandleToInput dd 0
HandleToOutput dd 0
String_132 db "Hello!",0
Number_133 dq 4.0
ExitStatus dd 0

