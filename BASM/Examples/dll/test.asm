;Library functions to import to the Chameleon app
extern ExitProcess
extern MessageBoxA
extern lstrcat
extern lstrlen
extern lstrcpy
extern GetProcessHeap
extern HeapCreate
extern HeapAlloc
extern HeapFree
extern lstrlenA
extern lstrcpyA
extern lstrcatA
extern pow
extern HeapDestroy



;Initialize everything to prep the app to run
section .text
%include "C:\BASM\Bin\MACROS.INC"
global START
START:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH ESI
PUSH EDI



;The main body where the app actually runs



;Prepare the app to exit and then terminate
Exit:
POP EDI
POP ESI
POP EBX
MOV ESP,EBP
POP EBP
MOV EAX,1
RET 12

NoMemory:
MOV dword[ExitStatus],1
stdcall MessageBoxA,0,NoMemMessage,Error,0
JMP Exit

NoLibrary:
MOV dword[ExitStatus],1
POP EBX
stdcall lstrlen,EBX
ADD EAX,30
CALL GetProcessHeap
stdcall HeapAlloc,EAX,8,EAX
MOV EDI,EAX
stdcall lstrcpy,EDI,NoLibFound
stdcall lstrcat,EDI,EBX
stdcall MessageBoxA,0,EDI,Error,0
CALL GetProcessHeap
stdcall HeapAlloc,EAX,8,EDI
JMP Exit

NoFunction:
MOV dword[ExitStatus],1
POP EBX
stdcall lstrlen,EBX
ADD EAX,30
CALL GetProcessHeap
stdcall HeapAlloc,EAX,8,EAX
MOV EDI,EAX
stdcall lstrcpy,EDI,NoFunctionFound
stdcall lstrcat,EDI,EBX
stdcall MessageBoxA,0,EDI,Error,0
CALL GetProcessHeap
stdcall HeapAlloc,EAX,8,EDI
JMP Exit


global _CONCAT
_CONCAT:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
CALL GetProcessHeap
stdcall HeapAlloc,EAX,8,1
CMP EAX,0
JNE Label2
JMP NoMemory
Label2:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
CALL GetProcessHeap
stdcall HeapAlloc,EAX,8,EBX
CMP EAX,0
JNE Label3
JMP NoMemory
Label3:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
CALL GetProcessHeap
stdcall HeapAlloc,EAX,8,EBX
CMP EAX,0
JNE Label4
JMP NoMemory
Label4:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
CALL GetProcessHeap
stdcall HeapAlloc,EAX,8,EAX
CMP EAX,0
JNE Label5
JMP NoMemory
Label5:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
CALL GetProcessHeap
stdcall HeapFree,EAX,0,EBX
CALL GetProcessHeap
stdcall HeapFree,EAX,0,ESI
POP EBX
CALL GetProcessHeap
stdcall HeapFree,EAX,0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8


global _SQRT
_SQRT:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,8
MOV dword[ESP+8],0
MOV dword[ESP+4],0
MOV dword[EBP-4],0
MOV dword[EBP-8],0

PUSH dword[EBP+8+4]
PUSH dword[EBP+8]
PUSH dword[Number_1+4]
PUSH dword[Number_1]
PUSH dword[Number_2+4]
PUSH dword[Number_2]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FDIV ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
ccall pow,dword[TempQWord2],dword[TempQWord2+4],dword[TempQWord1],dword[TempQWord1+4]
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FST qword[EBP-8]


FINIT
FLD qword[EBP-8]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8



;Data section of the Chameleon app
section .data
NoMemMessage db "Could not allocate memory.",0
NoLibFound db "Could not find library: ",0
NoFunctionFound db "Cound not find function: ",0
Error db "Error!",0
ParameterPool dd 0
TempQWord1 dq 0.0
TempQWord2 dq 0.0
True dd -1
Number_1 dq 1.0
Number_2 dq 2.0
ExitStatus dd 0

