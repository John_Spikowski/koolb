;Library functions to import to the Chameleon app
extern ExitProcess
extern MessageBoxA
extern lstrcat
extern lstrlen
extern lstrcpy
extern HeapCreate
extern HeapAlloc
extern HeapFree
extern GetModuleHandleA
extern GetCommandLineA
extern FreeLibrary
extern floor
extern lstrlenA
extern lstrcpyA
extern LoadLibraryA
extern GetProcAddress
extern fmod
extern HeapDestroy
%define GETMODULEHANDLE_Used
%define LOADICON_Used
%define MAKEINTRESOURCE_Used
%define CREATEFONT_Used
%define DEFWINDOWPROC_Used
%define DESTROYWINDOW_Used
%define TIMEKILLEVENT_Used
%define ONCLOSE_Used
%define ENABLEWINDOW_Used
%define TIMEKILLEVENT_Used
%define FORMATCLOCK_Used
%define MAKEINTRESOURCE_Used
%define PLAYSOUND_Used
%define SENDMESSAGE_Used
%define TIMEPROC_Used
%define TIMESETEVENT_Used
%define ONCOMMAND_Used
%define CREATEWINDOWEX_Used
%define ENABLEWINDOW_Used
%define GETSTOCKOBJECT_Used
%define SENDMESSAGE_Used
%define ONCREATE_Used
%define POSTQUITMESSAGE_Used
%define WINDOWPROC_Used
%define LOADICON_Used
%define MAKEINTRESOURCE_Used
%define LOADCURSOR_Used
%define MAKEINTRESOURCE_Used
%define REGISTERCLASSEX_Used
%define MESSAGEBOX_Used
%define EXITPROCESS_Used
%define CREATEWINDOWEX_Used
%define MESSAGEBOX_Used
%define EXITPROCESS_Used
%define GETSYSTEMMETRICS_Used
%define GETWINDOWRECT_Used
%define SETWINDOWPOS_Used
%define CENTERONDESKTOP_Used
%define SHOWWINDOW_Used
%define UPDATEWINDOW_Used
%define GETMESSAGE_Used
%define TRANSLATEMESSAGE_Used
%define DISPATCHMESSAGE_Used
%define DELETEOBJECT_Used

extern InitCommonControls



;Initialize everything to prep the app to run
section .text
%include "C:\BASM\Bin\MACROS.INC"
global START
START:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH ESI
PUSH EDI
stdcall HeapCreate,0,16384,0
MOV dword[HandleToHeap],EAX
CMP dword[HandleToHeap],0
JNE Label1
JMP NoMemory
Label1:
stdcall GetModuleHandleA,0
MOV dword[Internal_HInstance],EAX
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label9
JMP NoMemory
Label9:
MOV dword[Scope76__STRELAPSED_String],EAX
MOV byte[EAX],0
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label10
JMP NoMemory
Label10:
MOV dword[Scope76__STRCLASSNAME_String],EAX
MOV byte[EAX],0
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label11
JMP NoMemory
Label11:
MOV dword[Scope76__STRAPPTITLE_String],EAX
MOV byte[EAX],0
call InitCommonControls



;The main body where the app actually runs
MOV EBX,String_138
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label192
JMP NoMemory
Label192:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_138
PUSH EBX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[Scope76__STRAPPTITLE_String]
MOV dword[Scope76__STRAPPTITLE_String],EBX
MOV EBX,String_139
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label193
JMP NoMemory
Label193:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_139
PUSH EBX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[Scope76__STRCLASSNAME_String]
MOV dword[Scope76__STRCLASSNAME_String],EBX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label194
JMP NoMemory
Label194:
MOV dword[ParameterPool],EAX
PUSH dword[Number_140+4]
PUSH dword[Number_140]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_GETMODULEHANDLE],0
JNE Label195
stdcall LoadLibraryA,_GETMODULEHANDLE_Lib
MOV dword[_GETMODULEHANDLE_LibHandle],EAX
CMP EAX,0
JNE Label196
PUSH _GETMODULEHANDLE_Lib
JMP NoLibrary
Label196:
stdcall GetProcAddress,dword[_GETMODULEHANDLE_LibHandle],_GETMODULEHANDLE_Alias
MOV dword[_GETMODULEHANDLE],EAX
CMP EAX,0
JNE Label197
PUSH _GETMODULEHANDLE_Alias
JMP NoFunction
Label197:
Label195:
CALL dword[_GETMODULEHANDLE]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope76__HINST_Integer]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label198
JMP NoMemory
Label198:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Internal_HInstance]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label199
JMP NoMemory
Label199:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope0__Resource_STOPWATCHICO_101]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MAKEINTRESOURCE
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LOADICON],0
JNE Label200
stdcall LoadLibraryA,_LOADICON_Lib
MOV dword[_LOADICON_LibHandle],EAX
CMP EAX,0
JNE Label201
PUSH _LOADICON_Lib
JMP NoLibrary
Label201:
stdcall GetProcAddress,dword[_LOADICON_LibHandle],_LOADICON_Alias
MOV dword[_LOADICON],EAX
CMP EAX,0
JNE Label202
PUSH _LOADICON_Alias
JMP NoFunction
Label202:
Label200:
CALL dword[_LOADICON]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope76__HICON_Integer]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,56
CMP EAX,0
JNE Label203
JMP NoMemory
Label203:
MOV dword[ParameterPool],EAX
PUSH dword[Number_141+4]
PUSH dword[Number_141]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_142+4]
PUSH dword[Number_142]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_143+4]
PUSH dword[Number_143]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Number_144+4]
PUSH dword[Number_144]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
PUSH dword[Number_145+4]
PUSH dword[Number_145]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+16]
PUSH dword[Number_146+4]
PUSH dword[Number_146]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+20]
PUSH dword[Number_147+4]
PUSH dword[Number_147]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+24]
PUSH dword[Number_148+4]
PUSH dword[Number_148]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+28]
PUSH dword[Number_149+4]
PUSH dword[Number_149]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+32]
PUSH dword[Number_150+4]
PUSH dword[Number_150]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+36]
PUSH dword[Number_151+4]
PUSH dword[Number_151]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+40]
PUSH dword[Number_152+4]
PUSH dword[Number_152]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+44]
PUSH dword[Number_153+4]
PUSH dword[Number_153]
PUSH dword[Number_154+4]
PUSH dword[Number_154]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+48]
MOV EBX,String_155
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label204
JMP NoMemory
Label204:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_155
PUSH EBX
MOV EAX,dword[ParameterPool]
POP dword[EAX+52]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+52]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+48]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+44]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+40]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+36]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+32]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+28]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+24]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+20]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+16]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_CREATEFONT],0
JNE Label205
stdcall LoadLibraryA,_CREATEFONT_Lib
MOV dword[_CREATEFONT_LibHandle],EAX
CMP EAX,0
JNE Label206
PUSH _CREATEFONT_Lib
JMP NoLibrary
Label206:
stdcall GetProcAddress,dword[_CREATEFONT_LibHandle],_CREATEFONT_Alias
MOV dword[_CREATEFONT],EAX
CMP EAX,0
JNE Label207
PUSH _CREATEFONT_Alias
JMP NoFunction
Label207:
Label205:
CALL dword[_CREATEFONT]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+52]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope76__HFONT_Integer]
MOV dword[TempQWord1],48
FINIT
FILD dword[TempQWord1]
FSTP qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope76__WCEX_UDT+Scope0__WNDCLASSEX_TYPE.Scope0__CBSIZE_Integer]
PUSH dword[Number_156+4]
PUSH dword[Number_156]
PUSH dword[Number_157+4]
PUSH dword[Number_157]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope76__WCEX_UDT+Scope0__WNDCLASSEX_TYPE.Scope0__STYLE_Integer]
PUSH _WINDOWPROC
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope76__WCEX_UDT+Scope0__WNDCLASSEX_TYPE.Scope0__LPFNWNDPROC_Integer]
PUSH dword[Number_158+4]
PUSH dword[Number_158]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope76__WCEX_UDT+Scope0__WNDCLASSEX_TYPE.Scope0__CBCLSEXTRA_Integer]
PUSH dword[Number_159+4]
PUSH dword[Number_159]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope76__WCEX_UDT+Scope0__WNDCLASSEX_TYPE.Scope0__CBWNDEXTRA_Integer]
FINIT
FILD dword[Scope76__HINST_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope76__WCEX_UDT+Scope0__WNDCLASSEX_TYPE.Scope0__HINSTANCE_Integer]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label208
JMP NoMemory
Label208:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope76__HINST_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label209
JMP NoMemory
Label209:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope0__Resource_STOPWATCHICO_101]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MAKEINTRESOURCE
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LOADICON],0
JNE Label210
stdcall LoadLibraryA,_LOADICON_Lib
MOV dword[_LOADICON_LibHandle],EAX
CMP EAX,0
JNE Label211
PUSH _LOADICON_Lib
JMP NoLibrary
Label211:
stdcall GetProcAddress,dword[_LOADICON_LibHandle],_LOADICON_Alias
MOV dword[_LOADICON],EAX
CMP EAX,0
JNE Label212
PUSH _LOADICON_Alias
JMP NoFunction
Label212:
Label210:
CALL dword[_LOADICON]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope76__WCEX_UDT+Scope0__WNDCLASSEX_TYPE.Scope0__HICON_Integer]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label213
JMP NoMemory
Label213:
MOV dword[ParameterPool],EAX
PUSH dword[Number_160+4]
PUSH dword[Number_160]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label214
JMP NoMemory
Label214:
MOV dword[ParameterPool],EAX
PUSH dword[Number_161+4]
PUSH dword[Number_161]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MAKEINTRESOURCE
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LOADCURSOR],0
JNE Label215
stdcall LoadLibraryA,_LOADCURSOR_Lib
MOV dword[_LOADCURSOR_LibHandle],EAX
CMP EAX,0
JNE Label216
PUSH _LOADCURSOR_Lib
JMP NoLibrary
Label216:
stdcall GetProcAddress,dword[_LOADCURSOR_LibHandle],_LOADCURSOR_Alias
MOV dword[_LOADCURSOR],EAX
CMP EAX,0
JNE Label217
PUSH _LOADCURSOR_Alias
JMP NoFunction
Label217:
Label215:
CALL dword[_LOADCURSOR]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope76__WCEX_UDT+Scope0__WNDCLASSEX_TYPE.Scope0__HCURSOR_Integer]
PUSH dword[Number_162+4]
PUSH dword[Number_162]
PUSH dword[Number_163+4]
PUSH dword[Number_163]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope76__WCEX_UDT+Scope0__WNDCLASSEX_TYPE.Scope0__HBRBACKGROUND_Integer]
MOV EBX,String_164
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label218
JMP NoMemory
Label218:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_164
PUSH EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[Scope76__WCEX_UDT+Scope0__WNDCLASSEX_TYPE.Scope0__LPSZMENUNAME_String]
POP ESI
MOV dword[Scope76__WCEX_UDT+Scope0__WNDCLASSEX_TYPE.Scope0__LPSZMENUNAME_String],ESI
stdcall lstrlenA,dword[Scope76__STRCLASSNAME_String]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label219
JMP NoMemory
Label219:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[Scope76__STRCLASSNAME_String]
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,dword[Scope76__WCEX_UDT+Scope0__WNDCLASSEX_TYPE.Scope0__LPSZCLASSNAME_String]
POP ESI
MOV dword[Scope76__WCEX_UDT+Scope0__WNDCLASSEX_TYPE.Scope0__LPSZCLASSNAME_String],ESI
PUSH dword[Number_165+4]
PUSH dword[Number_165]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope76__WCEX_UDT+Scope0__WNDCLASSEX_TYPE.Scope0__HICONSM_Integer]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label220
JMP NoMemory
Label220:
MOV dword[ParameterPool],EAX
PUSH Scope76__WCEX_UDT
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_REGISTERCLASSEX],0
JNE Label221
stdcall LoadLibraryA,_REGISTERCLASSEX_Lib
MOV dword[_REGISTERCLASSEX_LibHandle],EAX
CMP EAX,0
JNE Label222
PUSH _REGISTERCLASSEX_Lib
JMP NoLibrary
Label222:
stdcall GetProcAddress,dword[_REGISTERCLASSEX_LibHandle],_REGISTERCLASSEX_Alias
MOV dword[_REGISTERCLASSEX],EAX
CMP EAX,0
JNE Label223
PUSH _REGISTERCLASSEX_Alias
JMP NoFunction
Label223:
Label221:
CALL dword[_REGISTERCLASSEX]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_166+4]
PUSH dword[Number_166]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label224
FLDZ
Label224:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label226
JMP Label225
Label226:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label228
JMP NoMemory
Label228:
MOV dword[ParameterPool],EAX
PUSH dword[Number_167+4]
PUSH dword[Number_167]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EBX,String_168
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label229
JMP NoMemory
Label229:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_168
PUSH EBX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
stdcall lstrlenA,dword[Scope76__STRAPPTITLE_String]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label230
JMP NoMemory
Label230:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[Scope76__STRAPPTITLE_String]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Number_169+4]
PUSH dword[Number_169]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_MESSAGEBOX],0
JNE Label231
stdcall LoadLibraryA,_MESSAGEBOX_Lib
MOV dword[_MESSAGEBOX_LibHandle],EAX
CMP EAX,0
JNE Label232
PUSH _MESSAGEBOX_Lib
JMP NoLibrary
Label232:
stdcall GetProcAddress,dword[_MESSAGEBOX_LibHandle],_MESSAGEBOX_Alias
MOV dword[_MESSAGEBOX],EAX
CMP EAX,0
JNE Label233
PUSH _MESSAGEBOX_Alias
JMP NoFunction
Label233:
Label231:
CALL dword[_MESSAGEBOX]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label234
JMP NoMemory
Label234:
MOV dword[ParameterPool],EAX
PUSH dword[Number_170+4]
PUSH dword[Number_170]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_EXITPROCESS],0
JNE Label235
stdcall LoadLibraryA,_EXITPROCESS_Lib
MOV dword[_EXITPROCESS_LibHandle],EAX
CMP EAX,0
JNE Label236
PUSH _EXITPROCESS_Lib
JMP NoLibrary
Label236:
stdcall GetProcAddress,dword[_EXITPROCESS_LibHandle],_EXITPROCESS_Alias
MOV dword[_EXITPROCESS],EAX
CMP EAX,0
JNE Label237
PUSH _EXITPROCESS_Alias
JMP NoFunction
Label237:
Label235:
CALL dword[_EXITPROCESS]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
JMP Label225
Label225:
Label227:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,48
CMP EAX,0
JNE Label238
JMP NoMemory
Label238:
MOV dword[ParameterPool],EAX
PUSH dword[Number_171+4]
PUSH dword[Number_171]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
stdcall lstrlenA,dword[Scope76__STRCLASSNAME_String]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label239
JMP NoMemory
Label239:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[Scope76__STRCLASSNAME_String]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
stdcall lstrlenA,dword[Scope76__STRAPPTITLE_String]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label240
JMP NoMemory
Label240:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[Scope76__STRAPPTITLE_String]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Number_172+4]
PUSH dword[Number_172]
PUSH dword[Number_173+4]
PUSH dword[Number_173]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_174+4]
PUSH dword[Number_174]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_175+4]
PUSH dword[Number_175]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
PUSH dword[Number_176+4]
PUSH dword[Number_176]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+16]
PUSH dword[Number_177+4]
PUSH dword[Number_177]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+20]
PUSH dword[Number_178+4]
PUSH dword[Number_178]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+24]
PUSH dword[Number_179+4]
PUSH dword[Number_179]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+28]
PUSH dword[Number_180+4]
PUSH dword[Number_180]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+32]
PUSH dword[Number_181+4]
PUSH dword[Number_181]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+36]
FINIT
FILD dword[Scope76__HINST_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+40]
PUSH dword[Number_182+4]
PUSH dword[Number_182]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+44]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+44]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+40]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+36]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+32]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+28]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+24]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+20]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+16]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_CREATEWINDOWEX],0
JNE Label241
stdcall LoadLibraryA,_CREATEWINDOWEX_Lib
MOV dword[_CREATEWINDOWEX_LibHandle],EAX
CMP EAX,0
JNE Label242
PUSH _CREATEWINDOWEX_Lib
JMP NoLibrary
Label242:
stdcall GetProcAddress,dword[_CREATEWINDOWEX_LibHandle],_CREATEWINDOWEX_Alias
MOV dword[_CREATEWINDOWEX],EAX
CMP EAX,0
JNE Label243
PUSH _CREATEWINDOWEX_Alias
JMP NoFunction
Label243:
Label241:
CALL dword[_CREATEWINDOWEX]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope76__HWINDOW_Integer]
FINIT
FILD dword[Scope76__HWINDOW_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_183+4]
PUSH dword[Number_183]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label244
FLDZ
Label244:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label246
JMP Label245
Label246:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label248
JMP NoMemory
Label248:
MOV dword[ParameterPool],EAX
PUSH dword[Number_184+4]
PUSH dword[Number_184]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EBX,String_185
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label249
JMP NoMemory
Label249:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_185
PUSH EBX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
stdcall lstrlenA,dword[Scope76__STRAPPTITLE_String]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label250
JMP NoMemory
Label250:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[Scope76__STRAPPTITLE_String]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Number_186+4]
PUSH dword[Number_186]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_MESSAGEBOX],0
JNE Label251
stdcall LoadLibraryA,_MESSAGEBOX_Lib
MOV dword[_MESSAGEBOX_LibHandle],EAX
CMP EAX,0
JNE Label252
PUSH _MESSAGEBOX_Lib
JMP NoLibrary
Label252:
stdcall GetProcAddress,dword[_MESSAGEBOX_LibHandle],_MESSAGEBOX_Alias
MOV dword[_MESSAGEBOX],EAX
CMP EAX,0
JNE Label253
PUSH _MESSAGEBOX_Alias
JMP NoFunction
Label253:
Label251:
CALL dword[_MESSAGEBOX]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label254
JMP NoMemory
Label254:
MOV dword[ParameterPool],EAX
PUSH dword[Number_187+4]
PUSH dword[Number_187]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_EXITPROCESS],0
JNE Label255
stdcall LoadLibraryA,_EXITPROCESS_Lib
MOV dword[_EXITPROCESS_LibHandle],EAX
CMP EAX,0
JNE Label256
PUSH _EXITPROCESS_Lib
JMP NoLibrary
Label256:
stdcall GetProcAddress,dword[_EXITPROCESS_LibHandle],_EXITPROCESS_Alias
MOV dword[_EXITPROCESS],EAX
CMP EAX,0
JNE Label257
PUSH _EXITPROCESS_Alias
JMP NoFunction
Label257:
Label255:
CALL dword[_EXITPROCESS]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
JMP Label245
Label245:
Label247:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label258
JMP NoMemory
Label258:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope76__HWINDOW_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CENTERONDESKTOP
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label259
JMP NoMemory
Label259:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope76__HWINDOW_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_188+4]
PUSH dword[Number_188]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_SHOWWINDOW],0
JNE Label260
stdcall LoadLibraryA,_SHOWWINDOW_Lib
MOV dword[_SHOWWINDOW_LibHandle],EAX
CMP EAX,0
JNE Label261
PUSH _SHOWWINDOW_Lib
JMP NoLibrary
Label261:
stdcall GetProcAddress,dword[_SHOWWINDOW_LibHandle],_SHOWWINDOW_Alias
MOV dword[_SHOWWINDOW],EAX
CMP EAX,0
JNE Label262
PUSH _SHOWWINDOW_Alias
JMP NoFunction
Label262:
Label260:
CALL dword[_SHOWWINDOW]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label263
JMP NoMemory
Label263:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope76__HWINDOW_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_UPDATEWINDOW],0
JNE Label264
stdcall LoadLibraryA,_UPDATEWINDOW_Lib
MOV dword[_UPDATEWINDOW_LibHandle],EAX
CMP EAX,0
JNE Label265
PUSH _UPDATEWINDOW_Lib
JMP NoLibrary
Label265:
stdcall GetProcAddress,dword[_UPDATEWINDOW_LibHandle],_UPDATEWINDOW_Alias
MOV dword[_UPDATEWINDOW],EAX
CMP EAX,0
JNE Label266
PUSH _UPDATEWINDOW_Alias
JMP NoFunction
Label266:
Label264:
CALL dword[_UPDATEWINDOW]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
Label267:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label268
JMP NoMemory
Label268:
MOV dword[ParameterPool],EAX
PUSH Scope76__MESSAGE_UDT
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_189+4]
PUSH dword[Number_189]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_190+4]
PUSH dword[Number_190]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Number_191+4]
PUSH dword[Number_191]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_GETMESSAGE],0
JNE Label269
stdcall LoadLibraryA,_GETMESSAGE_Lib
MOV dword[_GETMESSAGE_LibHandle],EAX
CMP EAX,0
JNE Label270
PUSH _GETMESSAGE_Lib
JMP NoLibrary
Label270:
stdcall GetProcAddress,dword[_GETMESSAGE_LibHandle],_GETMESSAGE_Alias
MOV dword[_GETMESSAGE],EAX
CMP EAX,0
JNE Label271
PUSH _GETMESSAGE_Alias
JMP NoFunction
Label271:
Label269:
CALL dword[_GETMESSAGE]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_192+4]
PUSH dword[Number_192]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JA Label272
FLDZ
Label272:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label274
JMP Label273
Label274:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label275
JMP NoMemory
Label275:
MOV dword[ParameterPool],EAX
PUSH Scope76__MESSAGE_UDT
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_TRANSLATEMESSAGE],0
JNE Label276
stdcall LoadLibraryA,_TRANSLATEMESSAGE_Lib
MOV dword[_TRANSLATEMESSAGE_LibHandle],EAX
CMP EAX,0
JNE Label277
PUSH _TRANSLATEMESSAGE_Lib
JMP NoLibrary
Label277:
stdcall GetProcAddress,dword[_TRANSLATEMESSAGE_LibHandle],_TRANSLATEMESSAGE_Alias
MOV dword[_TRANSLATEMESSAGE],EAX
CMP EAX,0
JNE Label278
PUSH _TRANSLATEMESSAGE_Alias
JMP NoFunction
Label278:
Label276:
CALL dword[_TRANSLATEMESSAGE]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label279
JMP NoMemory
Label279:
MOV dword[ParameterPool],EAX
PUSH Scope76__MESSAGE_UDT
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_DISPATCHMESSAGE],0
JNE Label280
stdcall LoadLibraryA,_DISPATCHMESSAGE_Lib
MOV dword[_DISPATCHMESSAGE_LibHandle],EAX
CMP EAX,0
JNE Label281
PUSH _DISPATCHMESSAGE_Lib
JMP NoLibrary
Label281:
stdcall GetProcAddress,dword[_DISPATCHMESSAGE_LibHandle],_DISPATCHMESSAGE_Alias
MOV dword[_DISPATCHMESSAGE],EAX
CMP EAX,0
JNE Label282
PUSH _DISPATCHMESSAGE_Alias
JMP NoFunction
Label282:
Label280:
CALL dword[_DISPATCHMESSAGE]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
JMP Label267
Label273:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label283
JMP NoMemory
Label283:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope76__HFONT_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_DELETEOBJECT],0
JNE Label284
stdcall LoadLibraryA,_DELETEOBJECT_Lib
MOV dword[_DELETEOBJECT_LibHandle],EAX
CMP EAX,0
JNE Label285
PUSH _DELETEOBJECT_Lib
JMP NoLibrary
Label285:
stdcall GetProcAddress,dword[_DELETEOBJECT_LibHandle],_DELETEOBJECT_Alias
MOV dword[_DELETEOBJECT],EAX
CMP EAX,0
JNE Label286
PUSH _DELETEOBJECT_Alias
JMP NoFunction
Label286:
Label284:
CALL dword[_DELETEOBJECT]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]



;Prepare the app to exit and then terminate
Exit:
%ifdef BEGINPAINT_Used
stdcall FreeLibrary,dword[_BEGINPAINT_LibHandle]
%endif
%ifdef ENDPAINT_Used
stdcall FreeLibrary,dword[_ENDPAINT_LibHandle]
%endif
%ifdef TEXTOUT_Used
stdcall FreeLibrary,dword[_TEXTOUT_LibHandle]
%endif
%ifdef DRAWTEXT_Used
stdcall FreeLibrary,dword[_DRAWTEXT_LibHandle]
%endif
%ifdef ENUMCHILDWINDOWS_Used
stdcall FreeLibrary,dword[_ENUMCHILDWINDOWS_LibHandle]
%endif
%ifdef GETTOPWINDOW_Used
stdcall FreeLibrary,dword[_GETTOPWINDOW_LibHandle]
%endif
%ifdef GETNEXTWINDOW_Used
stdcall FreeLibrary,dword[_GETNEXTWINDOW_LibHandle]
%endif
%ifdef GETCLIENTRECT_Used
stdcall FreeLibrary,dword[_GETCLIENTRECT_LibHandle]
%endif
%ifdef GETWINDOWRECT_Used
stdcall FreeLibrary,dword[_GETWINDOWRECT_LibHandle]
%endif
%ifdef REGISTERCLASS_Used
stdcall FreeLibrary,dword[_REGISTERCLASS_LibHandle]
%endif
%ifdef REGISTERCLASSEX_Used
stdcall FreeLibrary,dword[_REGISTERCLASSEX_LibHandle]
%endif
%ifdef UNREGISTERCLASS_Used
stdcall FreeLibrary,dword[_UNREGISTERCLASS_LibHandle]
%endif
%ifdef GETMESSAGE_Used
stdcall FreeLibrary,dword[_GETMESSAGE_LibHandle]
%endif
%ifdef TRANSLATEMESSAGE_Used
stdcall FreeLibrary,dword[_TRANSLATEMESSAGE_LibHandle]
%endif
%ifdef DISPATCHMESSAGE_Used
stdcall FreeLibrary,dword[_DISPATCHMESSAGE_LibHandle]
%endif
%ifdef SENDMESSAGE_Used
stdcall FreeLibrary,dword[_SENDMESSAGE_LibHandle]
%endif
%ifdef LOADBITMAP_Used
stdcall FreeLibrary,dword[_LOADBITMAP_LibHandle]
%endif
%ifdef LOADCURSOR_Used
stdcall FreeLibrary,dword[_LOADCURSOR_LibHandle]
%endif
%ifdef LOADICON_Used
stdcall FreeLibrary,dword[_LOADICON_LibHandle]
%endif
%ifdef DESTROYICON_Used
stdcall FreeLibrary,dword[_DESTROYICON_LibHandle]
%endif
%ifdef GETCURSOR_Used
stdcall FreeLibrary,dword[_GETCURSOR_LibHandle]
%endif
%ifdef SETCURSOR_Used
stdcall FreeLibrary,dword[_SETCURSOR_LibHandle]
%endif
%ifdef COPYICON_Used
stdcall FreeLibrary,dword[_COPYICON_LibHandle]
%endif
%ifdef SETSYSTEMCURSOR_Used
stdcall FreeLibrary,dword[_SETSYSTEMCURSOR_LibHandle]
%endif
%ifdef CREATECOMPATIBLEDC_Used
stdcall FreeLibrary,dword[_CREATECOMPATIBLEDC_LibHandle]
%endif
%ifdef CREATEWINDOWEX_Used
stdcall FreeLibrary,dword[_CREATEWINDOWEX_LibHandle]
%endif
%ifdef UPDATEWINDOW_Used
stdcall FreeLibrary,dword[_UPDATEWINDOW_LibHandle]
%endif
%ifdef SHOWWINDOW_Used
stdcall FreeLibrary,dword[_SHOWWINDOW_LibHandle]
%endif
%ifdef DEFWINDOWPROC_Used
stdcall FreeLibrary,dword[_DEFWINDOWPROC_LibHandle]
%endif
%ifdef POSTQUITMESSAGE_Used
stdcall FreeLibrary,dword[_POSTQUITMESSAGE_LibHandle]
%endif
%ifdef GETMODULEHANDLE_Used
stdcall FreeLibrary,dword[_GETMODULEHANDLE_LibHandle]
%endif
%ifdef GETACTIVEWINDOW_Used
stdcall FreeLibrary,dword[_GETACTIVEWINDOW_LibHandle]
%endif
%ifdef EXITPROCESS_Used
stdcall FreeLibrary,dword[_EXITPROCESS_LibHandle]
%endif
%ifdef MSGBOX_Used
stdcall FreeLibrary,dword[_MSGBOX_LibHandle]
%endif
%ifdef MESSAGEBOX_Used
stdcall FreeLibrary,dword[_MESSAGEBOX_LibHandle]
%endif
%ifdef GETLASTERROR_Used
stdcall FreeLibrary,dword[_GETLASTERROR_LibHandle]
%endif
%ifdef SETLASTERROR_Used
stdcall FreeLibrary,dword[_SETLASTERROR_LibHandle]
%endif
%ifdef FORMATMESSAGE_Used
stdcall FreeLibrary,dword[_FORMATMESSAGE_LibHandle]
%endif
%ifdef LOCALFREE_Used
stdcall FreeLibrary,dword[_LOCALFREE_LibHandle]
%endif
%ifdef GETWINDOWLONG_Used
stdcall FreeLibrary,dword[_GETWINDOWLONG_LibHandle]
%endif
%ifdef SETWINDOWLONG_Used
stdcall FreeLibrary,dword[_SETWINDOWLONG_LibHandle]
%endif
%ifdef CALLWINDOWPROC_Used
stdcall FreeLibrary,dword[_CALLWINDOWPROC_LibHandle]
%endif
%ifdef GETCLASSLONG_Used
stdcall FreeLibrary,dword[_GETCLASSLONG_LibHandle]
%endif
%ifdef SETCLASSLONG_Used
stdcall FreeLibrary,dword[_SETCLASSLONG_LibHandle]
%endif
%ifdef GETDESKTOPWINDOW_Used
stdcall FreeLibrary,dword[_GETDESKTOPWINDOW_LibHandle]
%endif
%ifdef GETPROCADDRESS_Used
stdcall FreeLibrary,dword[_GETPROCADDRESS_LibHandle]
%endif
%ifdef DIALOGBOXPARAM_Used
stdcall FreeLibrary,dword[_DIALOGBOXPARAM_LibHandle]
%endif
%ifdef ENDDIALOG_Used
stdcall FreeLibrary,dword[_ENDDIALOG_LibHandle]
%endif
%ifdef GETDLGITEM_Used
stdcall FreeLibrary,dword[_GETDLGITEM_LibHandle]
%endif
%ifdef INITCOMMONCONTROLS_Used
stdcall FreeLibrary,dword[_INITCOMMONCONTROLS_LibHandle]
%endif
%ifdef INVALIDATERECT_Used
stdcall FreeLibrary,dword[_INVALIDATERECT_LibHandle]
%endif
%ifdef CREATEPEN_Used
stdcall FreeLibrary,dword[_CREATEPEN_LibHandle]
%endif
%ifdef SELECTOBJECT_Used
stdcall FreeLibrary,dword[_SELECTOBJECT_LibHandle]
%endif
%ifdef GETSTOCKOBJECT_Used
stdcall FreeLibrary,dword[_GETSTOCKOBJECT_LibHandle]
%endif
%ifdef CREATESOLIDBRUSH_Used
stdcall FreeLibrary,dword[_CREATESOLIDBRUSH_LibHandle]
%endif
%ifdef RECTANGLE_Used
stdcall FreeLibrary,dword[_RECTANGLE_LibHandle]
%endif
%ifdef DELETEOBJECT_Used
stdcall FreeLibrary,dword[_DELETEOBJECT_LibHandle]
%endif
%ifdef DELETEDC_Used
stdcall FreeLibrary,dword[_DELETEDC_LibHandle]
%endif
%ifdef DESTROYWINDOW_Used
stdcall FreeLibrary,dword[_DESTROYWINDOW_LibHandle]
%endif
%ifdef SETPIXEL_Used
stdcall FreeLibrary,dword[_SETPIXEL_LibHandle]
%endif
%ifdef BITBLT_Used
stdcall FreeLibrary,dword[_BITBLT_LibHandle]
%endif
%ifdef CREATEMENU_Used
stdcall FreeLibrary,dword[_CREATEMENU_LibHandle]
%endif
%ifdef APPENDMENU_Used
stdcall FreeLibrary,dword[_APPENDMENU_LibHandle]
%endif
%ifdef POSTMESSAGE_Used
stdcall FreeLibrary,dword[_POSTMESSAGE_LibHandle]
%endif
%ifdef GETSYSTEMMETRICS_Used
stdcall FreeLibrary,dword[_GETSYSTEMMETRICS_LibHandle]
%endif
%ifdef SETWINDOWPOS_Used
stdcall FreeLibrary,dword[_SETWINDOWPOS_LibHandle]
%endif
%ifdef ENABLEWINDOW_Used
stdcall FreeLibrary,dword[_ENABLEWINDOW_LibHandle]
%endif
%ifdef PLAYSOUND_Used
stdcall FreeLibrary,dword[_PLAYSOUND_LibHandle]
%endif
%ifdef TIMESETEVENT_Used
stdcall FreeLibrary,dword[_TIMESETEVENT_LibHandle]
%endif
%ifdef TIMEKILLEVENT_Used
stdcall FreeLibrary,dword[_TIMEKILLEVENT_LibHandle]
%endif
%ifdef CREATEFONT_Used
stdcall FreeLibrary,dword[_CREATEFONT_LibHandle]
%endif
stdcall HeapFree,dword[HandleToHeap],0,dword[Scope76__STRELAPSED_String]
stdcall HeapFree,dword[HandleToHeap],0,dword[Scope76__STRCLASSNAME_String]
stdcall HeapFree,dword[HandleToHeap],0,dword[Scope76__STRAPPTITLE_String]
stdcall HeapDestroy,dword[HandleToHeap]
POP EDI
POP ESI
POP EBX
MOV ESP,EBP
POP EBP
MOV EAX,dword[ExitStatus]
stdcall ExitProcess,dword[ExitStatus]
RET

NoMemory:
MOV dword[ExitStatus],1
stdcall MessageBoxA,0,NoMemMessage,Error,0
JMP Exit

NoLibrary:
MOV dword[ExitStatus],1
POP EBX
stdcall lstrlen,EBX
ADD EAX,30
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
MOV EDI,EAX
stdcall lstrcpy,EDI,NoLibFound
stdcall lstrcat,EDI,EBX
stdcall MessageBoxA,0,EDI,Error,0
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
JMP Exit

NoFunction:
MOV dword[ExitStatus],1
POP EBX
stdcall lstrlen,EBX
ADD EAX,30
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
MOV EDI,EAX
stdcall lstrcpy,EDI,NoFunctionFound
stdcall lstrcat,EDI,EBX
stdcall MessageBoxA,0,EDI,Error,0
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
JMP Exit






















































































































































































































%ifdef SHOWMESSAGE_Used

global _SHOWMESSAGE
_SHOWMESSAGE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label2
JMP NoMemory
Label2:
MOV dword[ParameterPool],EAX
PUSH dword[Number_1+4]
PUSH dword[Number_1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label3
JMP NoMemory
Label3:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EBX,String_2
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label4
JMP NoMemory
Label4:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_2
PUSH EBX
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Number_3+4]
PUSH dword[Number_3]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_MESSAGEBOX],0
JNE Label5
stdcall LoadLibraryA,_MESSAGEBOX_Lib
MOV dword[_MESSAGEBOX_LibHandle],EAX
CMP EAX,0
JNE Label6
PUSH _MESSAGEBOX_Lib
JMP NoLibrary
Label6:
stdcall GetProcAddress,dword[_MESSAGEBOX_LibHandle],_MESSAGEBOX_Alias
MOV dword[_MESSAGEBOX],EAX
CMP EAX,0
JNE Label7
PUSH _MESSAGEBOX_Alias
JMP NoFunction
Label7:
Label5:
CALL dword[_MESSAGEBOX]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef LOWORD_Used

global _LOWORD
_LOWORD:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

mov eax,dword[ebp+8]   ;dwValue
and eax,65535
mov dword[ebp-4],eax


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef HIWORD_Used

global _HIWORD
_HIWORD:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

mov eax,dword[ebp+8]
and ecx,16
shr eax,cl 
mov dword[ebp-4],eax


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef MAKEINTRESOURCE_Used

global _MAKEINTRESOURCE
_MAKEINTRESOURCE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label8
JMP NoMemory
Label8:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

extern sprintf
jmp forward
strFormat db '#%010ld',0
forward:
stdcall HeapAlloc,dword[HandleToHeap],8,12 
mov ebx,eax 
ccall sprintf,eax,strFormat,dword[ebp+8]
mov dword[ebp-4],ebx


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef RETURN_Used

global _RETURN
_RETURN:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

mov   eax,[ebp+8] 


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef CENTERONDESKTOP_Used

global _CENTERONDESKTOP
_CENTERONDESKTOP:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
SUB ESP,4
SUB ESP,4

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label12
JMP NoMemory
Label12:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH Scope76__RCT_UDT
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_GETWINDOWRECT],0
JNE Label13
stdcall LoadLibraryA,_GETWINDOWRECT_Lib
MOV dword[_GETWINDOWRECT_LibHandle],EAX
CMP EAX,0
JNE Label14
PUSH _GETWINDOWRECT_Lib
JMP NoLibrary
Label14:
stdcall GetProcAddress,dword[_GETWINDOWRECT_LibHandle],_GETWINDOWRECT_Alias
MOV dword[_GETWINDOWRECT],EAX
CMP EAX,0
JNE Label15
PUSH _GETWINDOWRECT_Alias
JMP NoFunction
Label15:
Label13:
CALL dword[_GETWINDOWRECT]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label16
JMP NoMemory
Label16:
MOV dword[ParameterPool],EAX
PUSH dword[Number_4+4]
PUSH dword[Number_4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_GETSYSTEMMETRICS],0
JNE Label17
stdcall LoadLibraryA,_GETSYSTEMMETRICS_Lib
MOV dword[_GETSYSTEMMETRICS_LibHandle],EAX
CMP EAX,0
JNE Label18
PUSH _GETSYSTEMMETRICS_Lib
JMP NoLibrary
Label18:
stdcall GetProcAddress,dword[_GETSYSTEMMETRICS_LibHandle],_GETSYSTEMMETRICS_Alias
MOV dword[_GETSYSTEMMETRICS],EAX
CMP EAX,0
JNE Label19
PUSH _GETSYSTEMMETRICS_Alias
JMP NoFunction
Label19:
Label17:
CALL dword[_GETSYSTEMMETRICS]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[Scope76__RCT_UDT+Scope0__RECT_TYPE.Scope0__RIGHT_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[Scope76__RCT_UDT+Scope0__RECT_TYPE.Scope0__LEFT_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_5+4]
PUSH dword[Number_5]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FDIV ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label20
JMP NoMemory
Label20:
MOV dword[ParameterPool],EAX
PUSH dword[Number_6+4]
PUSH dword[Number_6]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_GETSYSTEMMETRICS],0
JNE Label21
stdcall LoadLibraryA,_GETSYSTEMMETRICS_Lib
MOV dword[_GETSYSTEMMETRICS_LibHandle],EAX
CMP EAX,0
JNE Label22
PUSH _GETSYSTEMMETRICS_Lib
JMP NoLibrary
Label22:
stdcall GetProcAddress,dword[_GETSYSTEMMETRICS_LibHandle],_GETSYSTEMMETRICS_Alias
MOV dword[_GETSYSTEMMETRICS],EAX
CMP EAX,0
JNE Label23
PUSH _GETSYSTEMMETRICS_Alias
JMP NoFunction
Label23:
Label21:
CALL dword[_GETSYSTEMMETRICS]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[Scope76__RCT_UDT+Scope0__RECT_TYPE.Scope0__BOTTOM_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[Scope76__RCT_UDT+Scope0__RECT_TYPE.Scope0__TOP_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_7+4]
PUSH dword[Number_7]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FDIV ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,28
CMP EAX,0
JNE Label24
JMP NoMemory
Label24:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_8+4]
PUSH dword[Number_8]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
PUSH dword[Number_9+4]
PUSH dword[Number_9]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+16]
PUSH dword[Number_10+4]
PUSH dword[Number_10]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+20]
PUSH dword[Number_11+4]
PUSH dword[Number_11]
PUSH dword[Number_12+4]
PUSH dword[Number_12]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_13+4]
PUSH dword[Number_13]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+24]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+24]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+20]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+16]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_SETWINDOWPOS],0
JNE Label25
stdcall LoadLibraryA,_SETWINDOWPOS_Lib
MOV dword[_SETWINDOWPOS_LibHandle],EAX
CMP EAX,0
JNE Label26
PUSH _SETWINDOWPOS_Lib
JMP NoLibrary
Label26:
stdcall GetProcAddress,dword[_SETWINDOWPOS_LibHandle],_SETWINDOWPOS_Alias
MOV dword[_SETWINDOWPOS],EAX
CMP EAX,0
JNE Label27
PUSH _SETWINDOWPOS_Alias
JMP NoFunction
Label27:
Label25:
CALL dword[_SETWINDOWPOS]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


MOV EAX,dword[EBP-4]
ADD ESP,12
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef FORMATCLOCK_Used

global _FORMATCLOCK
_FORMATCLOCK:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label28
JMP NoMemory
Label28:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

extern sprintf
stdcall HeapAlloc,dword[HandleToHeap],8,12
mov ebx,eax 
invoke sprintf,eax,"%2.2d:%2.2d:%2.2d.%1.1d",dword[ebp+8],dword[ebp+12],dword[ebp+16],dword[ebp+20]
mov dword[ebp-4],ebx


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 16

%endif

%ifdef TIMEPROC_Used

global _TIMEPROC
_TIMEPROC:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

FINIT
FILD dword[Scope76__DWTICKCOUNT_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_14+4]
PUSH dword[Number_14]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope76__DWTICKCOUNT_Integer]
FINIT
FILD dword[Scope76__DWTICKCOUNT_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_15+4]
PUSH dword[Number_15]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
ccall fmod,dword[TempQWord2],dword[TempQWord2+4],dword[TempQWord1],dword[TempQWord1+4]
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope76__DWTENTHS_Integer]
FINIT
FILD dword[Scope76__DWTICKCOUNT_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_16+4]
PUSH dword[Number_16]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FDIV ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_17+4]
PUSH dword[Number_17]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
ccall fmod,dword[TempQWord2],dword[TempQWord2+4],dword[TempQWord1],dword[TempQWord1+4]
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope76__DWSECONDS_Integer]
FINIT
FILD dword[Scope76__DWTICKCOUNT_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_18+4]
PUSH dword[Number_18]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FDIV ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_19+4]
PUSH dword[Number_19]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
ccall fmod,dword[TempQWord2],dword[TempQWord2+4],dword[TempQWord1],dword[TempQWord1+4]
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope76__DWMINUTES_Integer]
FINIT
FILD dword[Scope76__DWTICKCOUNT_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_20+4]
PUSH dword[Number_20]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FDIV ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_21+4]
PUSH dword[Number_21]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
ccall fmod,dword[TempQWord2],dword[TempQWord2+4],dword[TempQWord1],dword[TempQWord1+4]
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope76__DWHOURS_Integer]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label29
JMP NoMemory
Label29:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope76__DWHOURS_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[Scope76__DWMINUTES_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[Scope76__DWSECONDS_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
FINIT
FILD dword[Scope76__DWTENTHS_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _FORMATCLOCK
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[Scope76__STRELAPSED_String]
MOV dword[Scope76__STRELAPSED_String],EBX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label30
JMP NoMemory
Label30:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope76__HSTATICCLOCK_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_22+4]
PUSH dword[Number_22]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_23+4]
PUSH dword[Number_23]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Scope76__STRELAPSED_String]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_SENDMESSAGE],0
JNE Label31
stdcall LoadLibraryA,_SENDMESSAGE_Lib
MOV dword[_SENDMESSAGE_LibHandle],EAX
CMP EAX,0
JNE Label32
PUSH _SENDMESSAGE_Lib
JMP NoLibrary
Label32:
stdcall GetProcAddress,dword[_SENDMESSAGE_LibHandle],_SENDMESSAGE_Alias
MOV dword[_SENDMESSAGE],EAX
CMP EAX,0
JNE Label33
PUSH _SENDMESSAGE_Alias
JMP NoFunction
Label33:
Label31:
CALL dword[_SENDMESSAGE]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
FINIT
FILD dword[Scope76__DWTENTHS_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_24+4]
PUSH dword[Number_24]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label34
FLDZ
Label34:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label36
JMP Label35
Label36:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label38
JMP NoMemory
Label38:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope76__HCHKBOXCLICK_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_25+4]
PUSH dword[Number_25]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_26+4]
PUSH dword[Number_26]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Number_27+4]
PUSH dword[Number_27]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_SENDMESSAGE],0
JNE Label39
stdcall LoadLibraryA,_SENDMESSAGE_Lib
MOV dword[_SENDMESSAGE_LibHandle],EAX
CMP EAX,0
JNE Label40
PUSH _SENDMESSAGE_Lib
JMP NoLibrary
Label40:
stdcall GetProcAddress,dword[_SENDMESSAGE_LibHandle],_SENDMESSAGE_Alias
MOV dword[_SENDMESSAGE],EAX
CMP EAX,0
JNE Label41
PUSH _SENDMESSAGE_Alias
JMP NoFunction
Label41:
Label39:
CALL dword[_SENDMESSAGE]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_28+4]
PUSH dword[Number_28]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label42
FLDZ
Label42:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label44
JMP Label43
Label44:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label46
JMP NoMemory
Label46:
MOV dword[ParameterPool],EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label47
JMP NoMemory
Label47:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope0__Resource_STOPWATCHWAV_100]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MAKEINTRESOURCE
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[Scope76__HINST_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_29+4]
PUSH dword[Number_29]
PUSH dword[Number_30+4]
PUSH dword[Number_30]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_PLAYSOUND],0
JNE Label48
stdcall LoadLibraryA,_PLAYSOUND_Lib
MOV dword[_PLAYSOUND_LibHandle],EAX
CMP EAX,0
JNE Label49
PUSH _PLAYSOUND_Lib
JMP NoLibrary
Label49:
stdcall GetProcAddress,dword[_PLAYSOUND_LibHandle],_PLAYSOUND_Alias
MOV dword[_PLAYSOUND],EAX
CMP EAX,0
JNE Label50
PUSH _PLAYSOUND_Alias
JMP NoFunction
Label50:
Label48:
CALL dword[_PLAYSOUND]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
JMP Label43
Label43:
Label45:
JMP Label35
Label35:
Label37:


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 20

%endif

%ifdef ONCREATE_Used

global _ONCREATE
_ONCREATE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,48
CMP EAX,0
JNE Label51
JMP NoMemory
Label51:
MOV dword[ParameterPool],EAX
PUSH dword[Number_31+4]
PUSH dword[Number_31]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EBX,String_32
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label52
JMP NoMemory
Label52:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_32
PUSH EBX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EBX,String_33
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label53
JMP NoMemory
Label53:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_33
PUSH EBX
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Number_34+4]
PUSH dword[Number_34]
PUSH dword[Number_35+4]
PUSH dword[Number_35]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_36+4]
PUSH dword[Number_36]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
PUSH dword[Number_37+4]
PUSH dword[Number_37]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+16]
PUSH dword[Number_38+4]
PUSH dword[Number_38]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+20]
PUSH dword[Number_39+4]
PUSH dword[Number_39]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+24]
PUSH dword[Number_40+4]
PUSH dword[Number_40]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+28]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+32]
PUSH dword[Number_41+4]
PUSH dword[Number_41]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+36]
FINIT
FILD dword[Scope76__HINST_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+40]
PUSH dword[Number_42+4]
PUSH dword[Number_42]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+44]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+44]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+40]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+36]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+32]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+28]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+24]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+20]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+16]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_CREATEWINDOWEX],0
JNE Label54
stdcall LoadLibraryA,_CREATEWINDOWEX_Lib
MOV dword[_CREATEWINDOWEX_LibHandle],EAX
CMP EAX,0
JNE Label55
PUSH _CREATEWINDOWEX_Lib
JMP NoLibrary
Label55:
stdcall GetProcAddress,dword[_CREATEWINDOWEX_LibHandle],_CREATEWINDOWEX_Alias
MOV dword[_CREATEWINDOWEX],EAX
CMP EAX,0
JNE Label56
PUSH _CREATEWINDOWEX_Alias
JMP NoFunction
Label56:
Label54:
CALL dword[_CREATEWINDOWEX]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,48
CMP EAX,0
JNE Label57
JMP NoMemory
Label57:
MOV dword[ParameterPool],EAX
PUSH dword[Number_43+4]
PUSH dword[Number_43]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EBX,String_44
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label58
JMP NoMemory
Label58:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_44
PUSH EBX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EBX,String_45
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label59
JMP NoMemory
Label59:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_45
PUSH EBX
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Number_46+4]
PUSH dword[Number_46]
PUSH dword[Number_47+4]
PUSH dword[Number_47]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_48+4]
PUSH dword[Number_48]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
PUSH dword[Number_49+4]
PUSH dword[Number_49]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+16]
PUSH dword[Number_50+4]
PUSH dword[Number_50]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+20]
PUSH dword[Number_51+4]
PUSH dword[Number_51]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+24]
PUSH dword[Number_52+4]
PUSH dword[Number_52]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+28]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+32]
PUSH dword[Number_53+4]
PUSH dword[Number_53]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+36]
FINIT
FILD dword[Scope76__HINST_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+40]
PUSH dword[Number_54+4]
PUSH dword[Number_54]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+44]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+44]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+40]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+36]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+32]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+28]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+24]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+20]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+16]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_CREATEWINDOWEX],0
JNE Label60
stdcall LoadLibraryA,_CREATEWINDOWEX_Lib
MOV dword[_CREATEWINDOWEX_LibHandle],EAX
CMP EAX,0
JNE Label61
PUSH _CREATEWINDOWEX_Lib
JMP NoLibrary
Label61:
stdcall GetProcAddress,dword[_CREATEWINDOWEX_LibHandle],_CREATEWINDOWEX_Alias
MOV dword[_CREATEWINDOWEX],EAX
CMP EAX,0
JNE Label62
PUSH _CREATEWINDOWEX_Alias
JMP NoFunction
Label62:
Label60:
CALL dword[_CREATEWINDOWEX]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope76__HSTATICICON_Integer]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,48
CMP EAX,0
JNE Label63
JMP NoMemory
Label63:
MOV dword[ParameterPool],EAX
PUSH dword[Number_55+4]
PUSH dword[Number_55]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EBX,String_56
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label64
JMP NoMemory
Label64:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_56
PUSH EBX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EBX,String_57
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label65
JMP NoMemory
Label65:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_57
PUSH EBX
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Number_58+4]
PUSH dword[Number_58]
PUSH dword[Number_59+4]
PUSH dword[Number_59]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_60+4]
PUSH dword[Number_60]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
PUSH dword[Number_61+4]
PUSH dword[Number_61]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+16]
PUSH dword[Number_62+4]
PUSH dword[Number_62]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+20]
PUSH dword[Number_63+4]
PUSH dword[Number_63]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+24]
PUSH dword[Number_64+4]
PUSH dword[Number_64]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+28]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+32]
PUSH dword[Number_65+4]
PUSH dword[Number_65]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+36]
FINIT
FILD dword[Scope76__HINST_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+40]
PUSH dword[Number_66+4]
PUSH dword[Number_66]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+44]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+44]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+40]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+36]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+32]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+28]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+24]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+20]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+16]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_CREATEWINDOWEX],0
JNE Label66
stdcall LoadLibraryA,_CREATEWINDOWEX_Lib
MOV dword[_CREATEWINDOWEX_LibHandle],EAX
CMP EAX,0
JNE Label67
PUSH _CREATEWINDOWEX_Lib
JMP NoLibrary
Label67:
stdcall GetProcAddress,dword[_CREATEWINDOWEX_LibHandle],_CREATEWINDOWEX_Alias
MOV dword[_CREATEWINDOWEX],EAX
CMP EAX,0
JNE Label68
PUSH _CREATEWINDOWEX_Alias
JMP NoFunction
Label68:
Label66:
CALL dword[_CREATEWINDOWEX]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope76__HSTATICCLOCK_Integer]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,48
CMP EAX,0
JNE Label69
JMP NoMemory
Label69:
MOV dword[ParameterPool],EAX
PUSH dword[Number_67+4]
PUSH dword[Number_67]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EBX,String_68
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label70
JMP NoMemory
Label70:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_68
PUSH EBX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EBX,String_69
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label71
JMP NoMemory
Label71:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_69
PUSH EBX
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Number_70+4]
PUSH dword[Number_70]
PUSH dword[Number_71+4]
PUSH dword[Number_71]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_72+4]
PUSH dword[Number_72]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
PUSH dword[Number_73+4]
PUSH dword[Number_73]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+16]
PUSH dword[Number_74+4]
PUSH dword[Number_74]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+20]
PUSH dword[Number_75+4]
PUSH dword[Number_75]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+24]
PUSH dword[Number_76+4]
PUSH dword[Number_76]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+28]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+32]
PUSH dword[Number_77+4]
PUSH dword[Number_77]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+36]
FINIT
FILD dword[Scope76__HINST_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+40]
PUSH dword[Number_78+4]
PUSH dword[Number_78]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+44]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+44]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+40]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+36]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+32]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+28]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+24]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+20]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+16]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_CREATEWINDOWEX],0
JNE Label72
stdcall LoadLibraryA,_CREATEWINDOWEX_Lib
MOV dword[_CREATEWINDOWEX_LibHandle],EAX
CMP EAX,0
JNE Label73
PUSH _CREATEWINDOWEX_Lib
JMP NoLibrary
Label73:
stdcall GetProcAddress,dword[_CREATEWINDOWEX_LibHandle],_CREATEWINDOWEX_Alias
MOV dword[_CREATEWINDOWEX],EAX
CMP EAX,0
JNE Label74
PUSH _CREATEWINDOWEX_Alias
JMP NoFunction
Label74:
Label72:
CALL dword[_CREATEWINDOWEX]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope76__HBUTTONSTART_Integer]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,48
CMP EAX,0
JNE Label75
JMP NoMemory
Label75:
MOV dword[ParameterPool],EAX
PUSH dword[Number_79+4]
PUSH dword[Number_79]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EBX,String_80
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label76
JMP NoMemory
Label76:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_80
PUSH EBX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EBX,String_81
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label77
JMP NoMemory
Label77:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_81
PUSH EBX
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Number_82+4]
PUSH dword[Number_82]
PUSH dword[Number_83+4]
PUSH dword[Number_83]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_84+4]
PUSH dword[Number_84]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
PUSH dword[Number_85+4]
PUSH dword[Number_85]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+16]
PUSH dword[Number_86+4]
PUSH dword[Number_86]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+20]
PUSH dword[Number_87+4]
PUSH dword[Number_87]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+24]
PUSH dword[Number_88+4]
PUSH dword[Number_88]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+28]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+32]
PUSH dword[Number_89+4]
PUSH dword[Number_89]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+36]
FINIT
FILD dword[Scope76__HINST_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+40]
PUSH dword[Number_90+4]
PUSH dword[Number_90]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+44]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+44]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+40]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+36]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+32]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+28]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+24]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+20]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+16]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_CREATEWINDOWEX],0
JNE Label78
stdcall LoadLibraryA,_CREATEWINDOWEX_Lib
MOV dword[_CREATEWINDOWEX_LibHandle],EAX
CMP EAX,0
JNE Label79
PUSH _CREATEWINDOWEX_Lib
JMP NoLibrary
Label79:
stdcall GetProcAddress,dword[_CREATEWINDOWEX_LibHandle],_CREATEWINDOWEX_Alias
MOV dword[_CREATEWINDOWEX],EAX
CMP EAX,0
JNE Label80
PUSH _CREATEWINDOWEX_Alias
JMP NoFunction
Label80:
Label78:
CALL dword[_CREATEWINDOWEX]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope76__HBUTTONSTOP_Integer]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,48
CMP EAX,0
JNE Label81
JMP NoMemory
Label81:
MOV dword[ParameterPool],EAX
PUSH dword[Number_91+4]
PUSH dword[Number_91]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EBX,String_92
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label82
JMP NoMemory
Label82:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_92
PUSH EBX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EBX,String_93
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label83
JMP NoMemory
Label83:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_93
PUSH EBX
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Number_94+4]
PUSH dword[Number_94]
PUSH dword[Number_95+4]
PUSH dword[Number_95]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_96+4]
PUSH dword[Number_96]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
PUSH dword[Number_97+4]
PUSH dword[Number_97]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+16]
PUSH dword[Number_98+4]
PUSH dword[Number_98]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+20]
PUSH dword[Number_99+4]
PUSH dword[Number_99]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+24]
PUSH dword[Number_100+4]
PUSH dword[Number_100]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+28]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+32]
PUSH dword[Number_101+4]
PUSH dword[Number_101]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+36]
FINIT
FILD dword[Scope76__HINST_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+40]
PUSH dword[Number_102+4]
PUSH dword[Number_102]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+44]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+44]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+40]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+36]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+32]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+28]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+24]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+20]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+16]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_CREATEWINDOWEX],0
JNE Label84
stdcall LoadLibraryA,_CREATEWINDOWEX_Lib
MOV dword[_CREATEWINDOWEX_LibHandle],EAX
CMP EAX,0
JNE Label85
PUSH _CREATEWINDOWEX_Lib
JMP NoLibrary
Label85:
stdcall GetProcAddress,dword[_CREATEWINDOWEX_LibHandle],_CREATEWINDOWEX_Alias
MOV dword[_CREATEWINDOWEX],EAX
CMP EAX,0
JNE Label86
PUSH _CREATEWINDOWEX_Alias
JMP NoFunction
Label86:
Label84:
CALL dword[_CREATEWINDOWEX]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope76__HCHKBOXCLICK_Integer]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label87
JMP NoMemory
Label87:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope76__HSTATICICON_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_103+4]
PUSH dword[Number_103]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_104+4]
PUSH dword[Number_104]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
FINIT
FILD dword[Scope76__HICON_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_SENDMESSAGE],0
JNE Label88
stdcall LoadLibraryA,_SENDMESSAGE_Lib
MOV dword[_SENDMESSAGE_LibHandle],EAX
CMP EAX,0
JNE Label89
PUSH _SENDMESSAGE_Lib
JMP NoLibrary
Label89:
stdcall GetProcAddress,dword[_SENDMESSAGE_LibHandle],_SENDMESSAGE_Alias
MOV dword[_SENDMESSAGE],EAX
CMP EAX,0
JNE Label90
PUSH _SENDMESSAGE_Alias
JMP NoFunction
Label90:
Label88:
CALL dword[_SENDMESSAGE]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label91
JMP NoMemory
Label91:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope76__HSTATICCLOCK_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_105+4]
PUSH dword[Number_105]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[Scope76__HFONT_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Number_106+4]
PUSH dword[Number_106]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_SENDMESSAGE],0
JNE Label92
stdcall LoadLibraryA,_SENDMESSAGE_Lib
MOV dword[_SENDMESSAGE_LibHandle],EAX
CMP EAX,0
JNE Label93
PUSH _SENDMESSAGE_Lib
JMP NoLibrary
Label93:
stdcall GetProcAddress,dword[_SENDMESSAGE_LibHandle],_SENDMESSAGE_Alias
MOV dword[_SENDMESSAGE],EAX
CMP EAX,0
JNE Label94
PUSH _SENDMESSAGE_Alias
JMP NoFunction
Label94:
Label92:
CALL dword[_SENDMESSAGE]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label95
JMP NoMemory
Label95:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope76__HBUTTONSTART_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_107+4]
PUSH dword[Number_107]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label96
JMP NoMemory
Label96:
MOV dword[ParameterPool],EAX
PUSH dword[Number_108+4]
PUSH dword[Number_108]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_GETSTOCKOBJECT],0
JNE Label97
stdcall LoadLibraryA,_GETSTOCKOBJECT_Lib
MOV dword[_GETSTOCKOBJECT_LibHandle],EAX
CMP EAX,0
JNE Label98
PUSH _GETSTOCKOBJECT_Lib
JMP NoLibrary
Label98:
stdcall GetProcAddress,dword[_GETSTOCKOBJECT_LibHandle],_GETSTOCKOBJECT_Alias
MOV dword[_GETSTOCKOBJECT],EAX
CMP EAX,0
JNE Label99
PUSH _GETSTOCKOBJECT_Alias
JMP NoFunction
Label99:
Label97:
CALL dword[_GETSTOCKOBJECT]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Number_109+4]
PUSH dword[Number_109]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_SENDMESSAGE],0
JNE Label100
stdcall LoadLibraryA,_SENDMESSAGE_Lib
MOV dword[_SENDMESSAGE_LibHandle],EAX
CMP EAX,0
JNE Label101
PUSH _SENDMESSAGE_Lib
JMP NoLibrary
Label101:
stdcall GetProcAddress,dword[_SENDMESSAGE_LibHandle],_SENDMESSAGE_Alias
MOV dword[_SENDMESSAGE],EAX
CMP EAX,0
JNE Label102
PUSH _SENDMESSAGE_Alias
JMP NoFunction
Label102:
Label100:
CALL dword[_SENDMESSAGE]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label103
JMP NoMemory
Label103:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope76__HBUTTONSTOP_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_110+4]
PUSH dword[Number_110]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label104
JMP NoMemory
Label104:
MOV dword[ParameterPool],EAX
PUSH dword[Number_111+4]
PUSH dword[Number_111]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_GETSTOCKOBJECT],0
JNE Label105
stdcall LoadLibraryA,_GETSTOCKOBJECT_Lib
MOV dword[_GETSTOCKOBJECT_LibHandle],EAX
CMP EAX,0
JNE Label106
PUSH _GETSTOCKOBJECT_Lib
JMP NoLibrary
Label106:
stdcall GetProcAddress,dword[_GETSTOCKOBJECT_LibHandle],_GETSTOCKOBJECT_Alias
MOV dword[_GETSTOCKOBJECT],EAX
CMP EAX,0
JNE Label107
PUSH _GETSTOCKOBJECT_Alias
JMP NoFunction
Label107:
Label105:
CALL dword[_GETSTOCKOBJECT]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Number_112+4]
PUSH dword[Number_112]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_SENDMESSAGE],0
JNE Label108
stdcall LoadLibraryA,_SENDMESSAGE_Lib
MOV dword[_SENDMESSAGE_LibHandle],EAX
CMP EAX,0
JNE Label109
PUSH _SENDMESSAGE_Lib
JMP NoLibrary
Label109:
stdcall GetProcAddress,dword[_SENDMESSAGE_LibHandle],_SENDMESSAGE_Alias
MOV dword[_SENDMESSAGE],EAX
CMP EAX,0
JNE Label110
PUSH _SENDMESSAGE_Alias
JMP NoFunction
Label110:
Label108:
CALL dword[_SENDMESSAGE]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label111
JMP NoMemory
Label111:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope76__HCHKBOXCLICK_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_113+4]
PUSH dword[Number_113]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label112
JMP NoMemory
Label112:
MOV dword[ParameterPool],EAX
PUSH dword[Number_114+4]
PUSH dword[Number_114]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_GETSTOCKOBJECT],0
JNE Label113
stdcall LoadLibraryA,_GETSTOCKOBJECT_Lib
MOV dword[_GETSTOCKOBJECT_LibHandle],EAX
CMP EAX,0
JNE Label114
PUSH _GETSTOCKOBJECT_Lib
JMP NoLibrary
Label114:
stdcall GetProcAddress,dword[_GETSTOCKOBJECT_LibHandle],_GETSTOCKOBJECT_Alias
MOV dword[_GETSTOCKOBJECT],EAX
CMP EAX,0
JNE Label115
PUSH _GETSTOCKOBJECT_Alias
JMP NoFunction
Label115:
Label113:
CALL dword[_GETSTOCKOBJECT]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Number_115+4]
PUSH dword[Number_115]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_SENDMESSAGE],0
JNE Label116
stdcall LoadLibraryA,_SENDMESSAGE_Lib
MOV dword[_SENDMESSAGE_LibHandle],EAX
CMP EAX,0
JNE Label117
PUSH _SENDMESSAGE_Lib
JMP NoLibrary
Label117:
stdcall GetProcAddress,dword[_SENDMESSAGE_LibHandle],_SENDMESSAGE_Alias
MOV dword[_SENDMESSAGE],EAX
CMP EAX,0
JNE Label118
PUSH _SENDMESSAGE_Alias
JMP NoFunction
Label118:
Label116:
CALL dword[_SENDMESSAGE]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label119
JMP NoMemory
Label119:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope76__HBUTTONSTOP_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_116+4]
PUSH dword[Number_116]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_ENABLEWINDOW],0
JNE Label120
stdcall LoadLibraryA,_ENABLEWINDOW_Lib
MOV dword[_ENABLEWINDOW_LibHandle],EAX
CMP EAX,0
JNE Label121
PUSH _ENABLEWINDOW_Lib
JMP NoLibrary
Label121:
stdcall GetProcAddress,dword[_ENABLEWINDOW_LibHandle],_ENABLEWINDOW_Alias
MOV dword[_ENABLEWINDOW],EAX
CMP EAX,0
JNE Label122
PUSH _ENABLEWINDOW_Alias
JMP NoFunction
Label122:
Label120:
CALL dword[_ENABLEWINDOW]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[Number_117+4]
PUSH dword[Number_117]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 16

%endif

%ifdef ONCOMMAND_Used

global _ONCOMMAND
_ONCOMMAND:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

FINIT
FILD dword[EBP+20]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[Scope76__HBUTTONSTART_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label123
FLDZ
Label123:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label125
JMP Label124
Label125:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label127
JMP NoMemory
Label127:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope76__HBUTTONSTART_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_118+4]
PUSH dword[Number_118]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_ENABLEWINDOW],0
JNE Label128
stdcall LoadLibraryA,_ENABLEWINDOW_Lib
MOV dword[_ENABLEWINDOW_LibHandle],EAX
CMP EAX,0
JNE Label129
PUSH _ENABLEWINDOW_Lib
JMP NoLibrary
Label129:
stdcall GetProcAddress,dword[_ENABLEWINDOW_LibHandle],_ENABLEWINDOW_Alias
MOV dword[_ENABLEWINDOW],EAX
CMP EAX,0
JNE Label130
PUSH _ENABLEWINDOW_Alias
JMP NoFunction
Label130:
Label128:
CALL dword[_ENABLEWINDOW]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label131
JMP NoMemory
Label131:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope76__HBUTTONSTOP_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_119+4]
PUSH dword[Number_119]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_ENABLEWINDOW],0
JNE Label132
stdcall LoadLibraryA,_ENABLEWINDOW_Lib
MOV dword[_ENABLEWINDOW_LibHandle],EAX
CMP EAX,0
JNE Label133
PUSH _ENABLEWINDOW_Lib
JMP NoLibrary
Label133:
stdcall GetProcAddress,dword[_ENABLEWINDOW_LibHandle],_ENABLEWINDOW_Alias
MOV dword[_ENABLEWINDOW],EAX
CMP EAX,0
JNE Label134
PUSH _ENABLEWINDOW_Alias
JMP NoFunction
Label134:
Label132:
CALL dword[_ENABLEWINDOW]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[Number_120+4]
PUSH dword[Number_120]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope76__DWTICKCOUNT_Integer]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,20
CMP EAX,0
JNE Label135
JMP NoMemory
Label135:
MOV dword[ParameterPool],EAX
PUSH dword[Number_121+4]
PUSH dword[Number_121]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_122+4]
PUSH dword[Number_122]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH _TIMEPROC
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Number_123+4]
PUSH dword[Number_123]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
PUSH dword[Number_124+4]
PUSH dword[Number_124]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+16]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+16]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_TIMESETEVENT],0
JNE Label136
stdcall LoadLibraryA,_TIMESETEVENT_Lib
MOV dword[_TIMESETEVENT_LibHandle],EAX
CMP EAX,0
JNE Label137
PUSH _TIMESETEVENT_Lib
JMP NoLibrary
Label137:
stdcall GetProcAddress,dword[_TIMESETEVENT_LibHandle],_TIMESETEVENT_Alias
MOV dword[_TIMESETEVENT],EAX
CMP EAX,0
JNE Label138
PUSH _TIMESETEVENT_Alias
JMP NoFunction
Label138:
Label136:
CALL dword[_TIMESETEVENT]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope76__DWTIMERID_Integer]
JMP Label124
Label124:
Label126:
FINIT
FILD dword[EBP+20]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[Scope76__HBUTTONSTOP_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label139
FLDZ
Label139:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label141
JMP Label140
Label141:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label143
JMP NoMemory
Label143:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope76__HBUTTONSTART_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_125+4]
PUSH dword[Number_125]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_ENABLEWINDOW],0
JNE Label144
stdcall LoadLibraryA,_ENABLEWINDOW_Lib
MOV dword[_ENABLEWINDOW_LibHandle],EAX
CMP EAX,0
JNE Label145
PUSH _ENABLEWINDOW_Lib
JMP NoLibrary
Label145:
stdcall GetProcAddress,dword[_ENABLEWINDOW_LibHandle],_ENABLEWINDOW_Alias
MOV dword[_ENABLEWINDOW],EAX
CMP EAX,0
JNE Label146
PUSH _ENABLEWINDOW_Alias
JMP NoFunction
Label146:
Label144:
CALL dword[_ENABLEWINDOW]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label147
JMP NoMemory
Label147:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope76__HBUTTONSTOP_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_126+4]
PUSH dword[Number_126]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_ENABLEWINDOW],0
JNE Label148
stdcall LoadLibraryA,_ENABLEWINDOW_Lib
MOV dword[_ENABLEWINDOW_LibHandle],EAX
CMP EAX,0
JNE Label149
PUSH _ENABLEWINDOW_Lib
JMP NoLibrary
Label149:
stdcall GetProcAddress,dword[_ENABLEWINDOW_LibHandle],_ENABLEWINDOW_Alias
MOV dword[_ENABLEWINDOW],EAX
CMP EAX,0
JNE Label150
PUSH _ENABLEWINDOW_Alias
JMP NoFunction
Label150:
Label148:
CALL dword[_ENABLEWINDOW]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label151
JMP NoMemory
Label151:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope76__DWTIMERID_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_TIMEKILLEVENT],0
JNE Label152
stdcall LoadLibraryA,_TIMEKILLEVENT_Lib
MOV dword[_TIMEKILLEVENT_LibHandle],EAX
CMP EAX,0
JNE Label153
PUSH _TIMEKILLEVENT_Lib
JMP NoLibrary
Label153:
stdcall GetProcAddress,dword[_TIMEKILLEVENT_LibHandle],_TIMEKILLEVENT_Alias
MOV dword[_TIMEKILLEVENT],EAX
CMP EAX,0
JNE Label154
PUSH _TIMEKILLEVENT_Alias
JMP NoFunction
Label154:
Label152:
CALL dword[_TIMEKILLEVENT]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[Number_127+4]
PUSH dword[Number_127]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope76__DWTICKCOUNT_Integer]
PUSH dword[Number_128+4]
PUSH dword[Number_128]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope76__DWTIMERID_Integer]
JMP Label140
Label140:
Label142:
PUSH dword[Number_129+4]
PUSH dword[Number_129]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 16

%endif

%ifdef ONCLOSE_Used

global _ONCLOSE
_ONCLOSE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

FINIT
FILD dword[Scope76__DWTIMERID_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_130+4]
PUSH dword[Number_130]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JNE Label155
FLDZ
Label155:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label157
JMP Label156
Label157:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label159
JMP NoMemory
Label159:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope76__DWTIMERID_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_TIMEKILLEVENT],0
JNE Label160
stdcall LoadLibraryA,_TIMEKILLEVENT_Lib
MOV dword[_TIMEKILLEVENT_LibHandle],EAX
CMP EAX,0
JNE Label161
PUSH _TIMEKILLEVENT_Lib
JMP NoLibrary
Label161:
stdcall GetProcAddress,dword[_TIMEKILLEVENT_LibHandle],_TIMEKILLEVENT_Alias
MOV dword[_TIMEKILLEVENT],EAX
CMP EAX,0
JNE Label162
PUSH _TIMEKILLEVENT_Alias
JMP NoFunction
Label162:
Label160:
CALL dword[_TIMEKILLEVENT]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
JMP Label156
Label156:
Label158:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label163
JMP NoMemory
Label163:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope76__HWINDOW_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_DESTROYWINDOW],0
JNE Label164
stdcall LoadLibraryA,_DESTROYWINDOW_Lib
MOV dword[_DESTROYWINDOW_LibHandle],EAX
CMP EAX,0
JNE Label165
PUSH _DESTROYWINDOW_Lib
JMP NoLibrary
Label165:
stdcall GetProcAddress,dword[_DESTROYWINDOW_LibHandle],_DESTROYWINDOW_Alias
MOV dword[_DESTROYWINDOW],EAX
CMP EAX,0
JNE Label166
PUSH _DESTROYWINDOW_Alias
JMP NoFunction
Label166:
Label164:
CALL dword[_DESTROYWINDOW]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[Number_131+4]
PUSH dword[Number_131]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 16

%endif

%ifdef WINDOWPROC_Used

global _WINDOWPROC
_WINDOWPROC:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_132+4]
PUSH dword[Number_132]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label167
FLDZ
Label167:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label169
JMP Label168
Label169:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label171
JMP NoMemory
Label171:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP+16]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
FINIT
FILD dword[EBP+20]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _ONCREATE
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label170
Label168:
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_133+4]
PUSH dword[Number_133]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label172
FLDZ
Label172:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label174
JMP Label173
Label174:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label175
JMP NoMemory
Label175:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP+16]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
FINIT
FILD dword[EBP+20]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _ONCOMMAND
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label170
Label173:
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_134+4]
PUSH dword[Number_134]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label176
FLDZ
Label176:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label178
JMP Label177
Label178:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label179
JMP NoMemory
Label179:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP+16]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
FINIT
FILD dword[EBP+20]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _ONCLOSE
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label170
Label177:
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_135+4]
PUSH dword[Number_135]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label180
FLDZ
Label180:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label182
JMP Label181
Label182:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label183
JMP NoMemory
Label183:
MOV dword[ParameterPool],EAX
PUSH dword[Number_136+4]
PUSH dword[Number_136]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_POSTQUITMESSAGE],0
JNE Label184
stdcall LoadLibraryA,_POSTQUITMESSAGE_Lib
MOV dword[_POSTQUITMESSAGE_LibHandle],EAX
CMP EAX,0
JNE Label185
PUSH _POSTQUITMESSAGE_Lib
JMP NoLibrary
Label185:
stdcall GetProcAddress,dword[_POSTQUITMESSAGE_LibHandle],_POSTQUITMESSAGE_Alias
MOV dword[_POSTQUITMESSAGE],EAX
CMP EAX,0
JNE Label186
PUSH _POSTQUITMESSAGE_Alias
JMP NoFunction
Label186:
Label184:
CALL dword[_POSTQUITMESSAGE]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[Number_137+4]
PUSH dword[Number_137]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label170
Label181:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label188
JMP NoMemory
Label188:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP+16]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
FINIT
FILD dword[EBP+20]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_DEFWINDOWPROC],0
JNE Label189
stdcall LoadLibraryA,_DEFWINDOWPROC_Lib
MOV dword[_DEFWINDOWPROC_LibHandle],EAX
CMP EAX,0
JNE Label190
PUSH _DEFWINDOWPROC_Lib
JMP NoLibrary
Label190:
stdcall GetProcAddress,dword[_DEFWINDOWPROC_LibHandle],_DEFWINDOWPROC_Alias
MOV dword[_DEFWINDOWPROC],EAX
CMP EAX,0
JNE Label191
PUSH _DEFWINDOWPROC_Alias
JMP NoFunction
Label191:
Label189:
CALL dword[_DEFWINDOWPROC]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label187
Label187:
Label170:


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 16

%endif



;Data section of the Chameleon app
section .data
NoMemMessage db "Could not allocate memory.",0
NoLibFound db "Could not find library: ",0
NoFunctionFound db "Cound not find function: ",0
Error db "Error!",0
ParameterPool dd 0
HandleToHeap dd 0
TempQWord1 dq 0.0
TempQWord2 dq 0.0
True dd -1
Internal_HInstance dd 0
Internal_CommandLine dd 0
NoConsoleMessage db 'Error - Cannot access the console handles for Input/Output.',0
ConsoleTemp dd 0
ConsoleNewLine db 10,0
ConsoleClear db 'CLS',0
ConsolePause db 'PAUSE',0
HandleToInput dd 0
HandleToOutput dd 0
Scope0__Resource_STOPWATCHWAV_100 dd 100
Scope0__Resource_STOPWATCHICO_101 dd 101
STRUC Scope0__POINT_TYPE
.Scope0__X_Integer resd 1
.Scope0__Y_Integer resd 1
ENDSTRUC
STRUC Scope0__RECT_TYPE
.Scope0__LEFT_Integer resd 1
.Scope0__TOP_Integer resd 1
.Scope0__RIGHT_Integer resd 1
.Scope0__BOTTOM_Integer resd 1
ENDSTRUC
STRUC Scope0__SIZE_TYPE
.Scope0__CX_Integer resd 1
.Scope0__CY_Integer resd 1
ENDSTRUC
STRUC Scope0__STARTUPINFO_TYPE
.Scope0__CB_Integer resd 1
.Scope0__LPRESERVED_String resd 1
.Scope0__LPDESKTOP_String resd 1
.Scope0__LPTITLE_String resd 1
.Scope0__DWX_Integer resd 1
.Scope0__DWY_Integer resd 1
.Scope0__DWXSIZE_Integer resd 1
.Scope0__DWYSIZE_Integer resd 1
.Scope0__DWXCOUNTCHARS_Integer resd 1
.Scope0__DWYCOUNTCHARS_Integer resd 1
.Scope0__DWFILLATTRIBUTE_Integer resd 1
.Scope0__DWFLAGS_Integer resd 1
.Scope0__WSHOWWINDOW_Integer resd 1
.Scope0__CBRESERVED2_Integer resd 1
.Scope0__LPRESERVED2_Integer resd 1
.Scope0__HSTDINPUT_Integer resd 1
.Scope0__HSTDOUTPUT_Integer resd 1
.Scope0__HSTDERROR_Integer resd 1
ENDSTRUC
STRUC Scope0__NMHDR_TYPE
.Scope0__HWNDFROM_Integer resd 1
.Scope0__IDFROM_Integer resd 1
.Scope0__CODE_Integer resd 1
ENDSTRUC
STRUC Scope0__WNDCLASS_TYPE
.Scope0__STYLE_Integer resd 1
.Scope0__LPFNWNDPROC_Integer resd 1
.Scope0__CBCLSEXTRA_Integer resd 1
.Scope0__CBWNDEXTRA2_Integer resd 1
.Scope0__HINSTANCE_Integer resd 1
.Scope0__HICON_Integer resd 1
.Scope0__HCURSOR_Integer resd 1
.Scope0__HBRBACKGROUND_Integer resd 1
.Scope0__LPSZMENUNAME_String resd 1
.Scope0__LPSZCLASSNAME_String resd 1
ENDSTRUC
STRUC Scope0__WNDCLASSEX_TYPE
.Scope0__CBSIZE_Integer resd 1
.Scope0__STYLE_Integer resd 1
.Scope0__LPFNWNDPROC_Integer resd 1
.Scope0__CBCLSEXTRA_Integer resd 1
.Scope0__CBWNDEXTRA_Integer resd 1
.Scope0__HINSTANCE_Integer resd 1
.Scope0__HICON_Integer resd 1
.Scope0__HCURSOR_Integer resd 1
.Scope0__HBRBACKGROUND_Integer resd 1
.Scope0__LPSZMENUNAME_String resd 1
.Scope0__LPSZCLASSNAME_String resd 1
.Scope0__HICONSM_Integer resd 1
ENDSTRUC
STRUC Scope0__CREATESTRUCT_TYPE
.Scope0__LPCREATEPARAMS_Integer resd 1
.Scope0__HINSTANCE_Integer resd 1
.Scope0__HMENU_Integer resd 1
.Scope0__HWNDPARENT_Integer resd 1
.Scope0__CY_Integer resd 1
.Scope0__CX_Integer resd 1
.Scope0__Y_Integer resd 1
.Scope0__X_Integer resd 1
.Scope0__STYLE_Integer resd 1
.Scope0__LPSZNAME_String resd 1
.Scope0__LPSZCLASS_String resd 1
.Scope0__EXSTYLE_Integer resd 1
ENDSTRUC
STRUC Scope0__MSG_TYPE
.Scope0__HWND_Integer resd 1
.Scope0__MESSAGE_Integer resd 1
.Scope0__WPARAM_Integer resd 1
.Scope0__LPARAM_Integer resd 1
.Scope0__TIME_Integer resd 1
.Scope0__X_Integer resd 1
.Scope0__Y_Integer resd 1
ENDSTRUC
STRUC Scope0__PAINTSTRUCT_TYPE
.Scope0__HDC_Integer resd 1
.Scope0__FERASE_Integer resd 1
.Scope0__LEFT_Integer resd 1
.Scope0__TOP_Integer resd 1
.Scope0__RIGHT_Integer resd 1
.Scope0__BOTTOM_Integer resd 1
.Scope0__FRESTORE_Integer resd 1
.Scope0__FINCUPDATE_Integer resd 1
.Scope0__RGBRESERVED1_Integer resd 1
.Scope0__RGBRESERVED2_Integer resd 1
.Scope0__RGBRESERVED3_Integer resd 1
.Scope0__RGBRESERVED4_Integer resd 1
.Scope0__RGBRESERVED5_Integer resd 1
.Scope0__RGBRESERVED6_Integer resd 1
.Scope0__RGBRESERVED7_Integer resd 1
.Scope0__RGBRESERVED8_Integer resd 1
ENDSTRUC
%ifdef BEGINPAINT_Used
_BEGINPAINT dd 0
_BEGINPAINT_LibHandle dd 0
_BEGINPAINT_Alias db "BeginPaint",0
_BEGINPAINT_Lib db "user32",0
_BEGINPAINT_Call db 0
%endif
%ifdef ENDPAINT_Used
_ENDPAINT dd 0
_ENDPAINT_LibHandle dd 0
_ENDPAINT_Alias db "EndPaint",0
_ENDPAINT_Lib db "user32",0
_ENDPAINT_Call db 0
%endif
%ifdef TEXTOUT_Used
_TEXTOUT dd 0
_TEXTOUT_LibHandle dd 0
_TEXTOUT_Alias db "TextOutA",0
_TEXTOUT_Lib db "gdi32",0
_TEXTOUT_Call db 0
%endif
%ifdef DRAWTEXT_Used
_DRAWTEXT dd 0
_DRAWTEXT_LibHandle dd 0
_DRAWTEXT_Alias db "DrawTextA",0
_DRAWTEXT_Lib db "user32",0
_DRAWTEXT_Call db 0
%endif
%ifdef ENUMCHILDWINDOWS_Used
_ENUMCHILDWINDOWS dd 0
_ENUMCHILDWINDOWS_LibHandle dd 0
_ENUMCHILDWINDOWS_Alias db "EnumChildWindows",0
_ENUMCHILDWINDOWS_Lib db "user32",0
_ENUMCHILDWINDOWS_Call db 0
%endif
%ifdef GETTOPWINDOW_Used
_GETTOPWINDOW dd 0
_GETTOPWINDOW_LibHandle dd 0
_GETTOPWINDOW_Alias db "GetTopWindow",0
_GETTOPWINDOW_Lib db "user32",0
_GETTOPWINDOW_Call db 0
%endif
%ifdef GETNEXTWINDOW_Used
_GETNEXTWINDOW dd 0
_GETNEXTWINDOW_LibHandle dd 0
_GETNEXTWINDOW_Alias db "GetWindow",0
_GETNEXTWINDOW_Lib db "user32",0
_GETNEXTWINDOW_Call db 0
%endif
%ifdef GETCLIENTRECT_Used
_GETCLIENTRECT dd 0
_GETCLIENTRECT_LibHandle dd 0
_GETCLIENTRECT_Alias db "GetClientRect",0
_GETCLIENTRECT_Lib db "user32",0
_GETCLIENTRECT_Call db 0
%endif
%ifdef GETWINDOWRECT_Used
_GETWINDOWRECT dd 0
_GETWINDOWRECT_LibHandle dd 0
_GETWINDOWRECT_Alias db "GetWindowRect",0
_GETWINDOWRECT_Lib db "user32",0
_GETWINDOWRECT_Call db 0
%endif
%ifdef REGISTERCLASS_Used
_REGISTERCLASS dd 0
_REGISTERCLASS_LibHandle dd 0
_REGISTERCLASS_Alias db "RegisterClassA",0
_REGISTERCLASS_Lib db "user32",0
_REGISTERCLASS_Call db 0
%endif
%ifdef REGISTERCLASSEX_Used
_REGISTERCLASSEX dd 0
_REGISTERCLASSEX_LibHandle dd 0
_REGISTERCLASSEX_Alias db "RegisterClassExA",0
_REGISTERCLASSEX_Lib db "USER32",0
_REGISTERCLASSEX_Call db 0
%endif
%ifdef UNREGISTERCLASS_Used
_UNREGISTERCLASS dd 0
_UNREGISTERCLASS_LibHandle dd 0
_UNREGISTERCLASS_Alias db "UnregisterClassA",0
_UNREGISTERCLASS_Lib db "user32",0
_UNREGISTERCLASS_Call db 0
%endif
%ifdef GETMESSAGE_Used
_GETMESSAGE dd 0
_GETMESSAGE_LibHandle dd 0
_GETMESSAGE_Alias db "GetMessageA",0
_GETMESSAGE_Lib db "user32",0
_GETMESSAGE_Call db 0
%endif
%ifdef TRANSLATEMESSAGE_Used
_TRANSLATEMESSAGE dd 0
_TRANSLATEMESSAGE_LibHandle dd 0
_TRANSLATEMESSAGE_Alias db "TranslateMessage",0
_TRANSLATEMESSAGE_Lib db "user32",0
_TRANSLATEMESSAGE_Call db 0
%endif
%ifdef DISPATCHMESSAGE_Used
_DISPATCHMESSAGE dd 0
_DISPATCHMESSAGE_LibHandle dd 0
_DISPATCHMESSAGE_Alias db "DispatchMessageA",0
_DISPATCHMESSAGE_Lib db "user32",0
_DISPATCHMESSAGE_Call db 0
%endif
%ifdef SENDMESSAGE_Used
_SENDMESSAGE dd 0
_SENDMESSAGE_LibHandle dd 0
_SENDMESSAGE_Alias db "SendMessageA",0
_SENDMESSAGE_Lib db "user32",0
_SENDMESSAGE_Call db 0
%endif
%ifdef LOADBITMAP_Used
_LOADBITMAP dd 0
_LOADBITMAP_LibHandle dd 0
_LOADBITMAP_Alias db "LoadBitmapA",0
_LOADBITMAP_Lib db "user32",0
_LOADBITMAP_Call db 0
%endif
%ifdef LOADCURSOR_Used
_LOADCURSOR dd 0
_LOADCURSOR_LibHandle dd 0
_LOADCURSOR_Alias db "LoadCursorA",0
_LOADCURSOR_Lib db "USER32",0
_LOADCURSOR_Call db 0
%endif
%ifdef LOADICON_Used
_LOADICON dd 0
_LOADICON_LibHandle dd 0
_LOADICON_Alias db "LoadIconA",0
_LOADICON_Lib db "user32",0
_LOADICON_Call db 0
%endif
%ifdef DESTROYICON_Used
_DESTROYICON dd 0
_DESTROYICON_LibHandle dd 0
_DESTROYICON_Alias db "DestroyIcon",0
_DESTROYICON_Lib db "user32",0
_DESTROYICON_Call db 0
%endif
%ifdef GETCURSOR_Used
_GETCURSOR dd 0
_GETCURSOR_LibHandle dd 0
_GETCURSOR_Alias db "GetCursor",0
_GETCURSOR_Lib db "user32",0
_GETCURSOR_Call db 0
%endif
%ifdef SETCURSOR_Used
_SETCURSOR dd 0
_SETCURSOR_LibHandle dd 0
_SETCURSOR_Alias db "SetCursor",0
_SETCURSOR_Lib db "user32",0
_SETCURSOR_Call db 0
%endif
%ifdef COPYICON_Used
_COPYICON dd 0
_COPYICON_LibHandle dd 0
_COPYICON_Alias db "CopyIcon",0
_COPYICON_Lib db "user32",0
_COPYICON_Call db 0
%endif
%ifdef SETSYSTEMCURSOR_Used
_SETSYSTEMCURSOR dd 0
_SETSYSTEMCURSOR_LibHandle dd 0
_SETSYSTEMCURSOR_Alias db "SetSystemCursor",0
_SETSYSTEMCURSOR_Lib db "user32",0
_SETSYSTEMCURSOR_Call db 0
%endif
%ifdef CREATECOMPATIBLEDC_Used
_CREATECOMPATIBLEDC dd 0
_CREATECOMPATIBLEDC_LibHandle dd 0
_CREATECOMPATIBLEDC_Alias db "CreateCompatibleDC",0
_CREATECOMPATIBLEDC_Lib db "gdi32",0
_CREATECOMPATIBLEDC_Call db 0
%endif
%ifdef CREATEWINDOWEX_Used
_CREATEWINDOWEX dd 0
_CREATEWINDOWEX_LibHandle dd 0
_CREATEWINDOWEX_Alias db "CreateWindowExA",0
_CREATEWINDOWEX_Lib db "user32",0
_CREATEWINDOWEX_Call db 0
%endif
%ifdef UPDATEWINDOW_Used
_UPDATEWINDOW dd 0
_UPDATEWINDOW_LibHandle dd 0
_UPDATEWINDOW_Alias db "UpdateWindow",0
_UPDATEWINDOW_Lib db "USER32",0
_UPDATEWINDOW_Call db 0
%endif
%ifdef SHOWWINDOW_Used
_SHOWWINDOW dd 0
_SHOWWINDOW_LibHandle dd 0
_SHOWWINDOW_Alias db "ShowWindow",0
_SHOWWINDOW_Lib db "USER32",0
_SHOWWINDOW_Call db 0
%endif
%ifdef DEFWINDOWPROC_Used
_DEFWINDOWPROC dd 0
_DEFWINDOWPROC_LibHandle dd 0
_DEFWINDOWPROC_Alias db "DefWindowProcA",0
_DEFWINDOWPROC_Lib db "user32",0
_DEFWINDOWPROC_Call db 0
%endif
%ifdef POSTQUITMESSAGE_Used
_POSTQUITMESSAGE dd 0
_POSTQUITMESSAGE_LibHandle dd 0
_POSTQUITMESSAGE_Alias db "PostQuitMessage",0
_POSTQUITMESSAGE_Lib db "user32",0
_POSTQUITMESSAGE_Call db 0
%endif
%ifdef GETMODULEHANDLE_Used
_GETMODULEHANDLE dd 0
_GETMODULEHANDLE_LibHandle dd 0
_GETMODULEHANDLE_Alias db "GetModuleHandleA",0
_GETMODULEHANDLE_Lib db "kernel32",0
_GETMODULEHANDLE_Call db 0
%endif
%ifdef GETACTIVEWINDOW_Used
_GETACTIVEWINDOW dd 0
_GETACTIVEWINDOW_LibHandle dd 0
_GETACTIVEWINDOW_Alias db "GetActiveWindow",0
_GETACTIVEWINDOW_Lib db "user32",0
_GETACTIVEWINDOW_Call db 0
%endif
%ifdef EXITPROCESS_Used
_EXITPROCESS dd 0
_EXITPROCESS_LibHandle dd 0
_EXITPROCESS_Alias db "ExitProcess",0
_EXITPROCESS_Lib db "kernel32",0
_EXITPROCESS_Call db 0
%endif
%ifdef MSGBOX_Used
_MSGBOX dd 0
_MSGBOX_LibHandle dd 0
_MSGBOX_Alias db "MessageBoxA",0
_MSGBOX_Lib db "user32",0
_MSGBOX_Call db 0
%endif
%ifdef MESSAGEBOX_Used
_MESSAGEBOX dd 0
_MESSAGEBOX_LibHandle dd 0
_MESSAGEBOX_Alias db "MessageBoxA",0
_MESSAGEBOX_Lib db "user32",0
_MESSAGEBOX_Call db 0
%endif
%ifdef GETLASTERROR_Used
_GETLASTERROR dd 0
_GETLASTERROR_LibHandle dd 0
_GETLASTERROR_Alias db "GetLastError",0
_GETLASTERROR_Lib db "kernel32",0
_GETLASTERROR_Call db 0
%endif
%ifdef SETLASTERROR_Used
_SETLASTERROR dd 0
_SETLASTERROR_LibHandle dd 0
_SETLASTERROR_Alias db "SetLastError",0
_SETLASTERROR_Lib db "kernel32",0
_SETLASTERROR_Call db 0
%endif
%ifdef FORMATMESSAGE_Used
_FORMATMESSAGE dd 0
_FORMATMESSAGE_LibHandle dd 0
_FORMATMESSAGE_Alias db "FormatMessageA",0
_FORMATMESSAGE_Lib db "kernel32",0
_FORMATMESSAGE_Call db 0
%endif
%ifdef LOCALFREE_Used
_LOCALFREE dd 0
_LOCALFREE_LibHandle dd 0
_LOCALFREE_Alias db "LocalFree",0
_LOCALFREE_Lib db "kernel32",0
_LOCALFREE_Call db 0
%endif
%ifdef GETWINDOWLONG_Used
_GETWINDOWLONG dd 0
_GETWINDOWLONG_LibHandle dd 0
_GETWINDOWLONG_Alias db "GetWindowIntegerA",0
_GETWINDOWLONG_Lib db "user32",0
_GETWINDOWLONG_Call db 0
%endif
%ifdef SETWINDOWLONG_Used
_SETWINDOWLONG dd 0
_SETWINDOWLONG_LibHandle dd 0
_SETWINDOWLONG_Alias db "SetWindowIntegerA",0
_SETWINDOWLONG_Lib db "user32",0
_SETWINDOWLONG_Call db 0
%endif
%ifdef CALLWINDOWPROC_Used
_CALLWINDOWPROC dd 0
_CALLWINDOWPROC_LibHandle dd 0
_CALLWINDOWPROC_Alias db "CallWindowProcA",0
_CALLWINDOWPROC_Lib db "user32",0
_CALLWINDOWPROC_Call db 0
%endif
%ifdef GETCLASSLONG_Used
_GETCLASSLONG dd 0
_GETCLASSLONG_LibHandle dd 0
_GETCLASSLONG_Alias db "GetClassLongA",0
_GETCLASSLONG_Lib db "user32",0
_GETCLASSLONG_Call db 0
%endif
%ifdef SETCLASSLONG_Used
_SETCLASSLONG dd 0
_SETCLASSLONG_LibHandle dd 0
_SETCLASSLONG_Alias db "SetClassLongA",0
_SETCLASSLONG_Lib db "user32",0
_SETCLASSLONG_Call db 0
%endif
%ifdef GETDESKTOPWINDOW_Used
_GETDESKTOPWINDOW dd 0
_GETDESKTOPWINDOW_LibHandle dd 0
_GETDESKTOPWINDOW_Alias db "GetDesktopWindow",0
_GETDESKTOPWINDOW_Lib db "user32",0
_GETDESKTOPWINDOW_Call db 0
%endif
%ifdef GETPROCADDRESS_Used
_GETPROCADDRESS dd 0
_GETPROCADDRESS_LibHandle dd 0
_GETPROCADDRESS_Alias db "GetProcAddress",0
_GETPROCADDRESS_Lib db "kernel32",0
_GETPROCADDRESS_Call db 0
%endif
%ifdef DIALOGBOXPARAM_Used
_DIALOGBOXPARAM dd 0
_DIALOGBOXPARAM_LibHandle dd 0
_DIALOGBOXPARAM_Alias db "DialogBoxParamA",0
_DIALOGBOXPARAM_Lib db "user32",0
_DIALOGBOXPARAM_Call db 0
%endif
%ifdef ENDDIALOG_Used
_ENDDIALOG dd 0
_ENDDIALOG_LibHandle dd 0
_ENDDIALOG_Alias db "EndDialog",0
_ENDDIALOG_Lib db "user32",0
_ENDDIALOG_Call db 0
%endif
%ifdef GETDLGITEM_Used
_GETDLGITEM dd 0
_GETDLGITEM_LibHandle dd 0
_GETDLGITEM_Alias db "GetDlgItem",0
_GETDLGITEM_Lib db "user32",0
_GETDLGITEM_Call db 0
%endif
%ifdef INITCOMMONCONTROLS_Used
_INITCOMMONCONTROLS dd 0
_INITCOMMONCONTROLS_LibHandle dd 0
_INITCOMMONCONTROLS_Alias db "InitCommonControls",0
_INITCOMMONCONTROLS_Lib db "comctl32",0
_INITCOMMONCONTROLS_Call db 0
%endif
%ifdef INVALIDATERECT_Used
_INVALIDATERECT dd 0
_INVALIDATERECT_LibHandle dd 0
_INVALIDATERECT_Alias db "InvalidateRect",0
_INVALIDATERECT_Lib db "user32",0
_INVALIDATERECT_Call db 0
%endif
%ifdef CREATEPEN_Used
_CREATEPEN dd 0
_CREATEPEN_LibHandle dd 0
_CREATEPEN_Alias db "CreatePen",0
_CREATEPEN_Lib db "gdi32",0
_CREATEPEN_Call db 0
%endif
%ifdef SELECTOBJECT_Used
_SELECTOBJECT dd 0
_SELECTOBJECT_LibHandle dd 0
_SELECTOBJECT_Alias db "SelectObject",0
_SELECTOBJECT_Lib db "gdi32",0
_SELECTOBJECT_Call db 0
%endif
%ifdef GETSTOCKOBJECT_Used
_GETSTOCKOBJECT dd 0
_GETSTOCKOBJECT_LibHandle dd 0
_GETSTOCKOBJECT_Alias db "GetStockObject",0
_GETSTOCKOBJECT_Lib db "gdi32",0
_GETSTOCKOBJECT_Call db 0
%endif
%ifdef CREATESOLIDBRUSH_Used
_CREATESOLIDBRUSH dd 0
_CREATESOLIDBRUSH_LibHandle dd 0
_CREATESOLIDBRUSH_Alias db "CreateSolidBrush",0
_CREATESOLIDBRUSH_Lib db "gdi32",0
_CREATESOLIDBRUSH_Call db 0
%endif
%ifdef RECTANGLE_Used
_RECTANGLE dd 0
_RECTANGLE_LibHandle dd 0
_RECTANGLE_Alias db "Rectangle",0
_RECTANGLE_Lib db "gdi32",0
_RECTANGLE_Call db 0
%endif
%ifdef DELETEOBJECT_Used
_DELETEOBJECT dd 0
_DELETEOBJECT_LibHandle dd 0
_DELETEOBJECT_Alias db "DeleteObject",0
_DELETEOBJECT_Lib db "gdi32",0
_DELETEOBJECT_Call db 0
%endif
%ifdef DELETEDC_Used
_DELETEDC dd 0
_DELETEDC_LibHandle dd 0
_DELETEDC_Alias db "DeleteDC",0
_DELETEDC_Lib db "gdi32",0
_DELETEDC_Call db 0
%endif
%ifdef DESTROYWINDOW_Used
_DESTROYWINDOW dd 0
_DESTROYWINDOW_LibHandle dd 0
_DESTROYWINDOW_Alias db "DestroyWindow",0
_DESTROYWINDOW_Lib db "user32",0
_DESTROYWINDOW_Call db 0
%endif
%ifdef SETPIXEL_Used
_SETPIXEL dd 0
_SETPIXEL_LibHandle dd 0
_SETPIXEL_Alias db "SetPixel",0
_SETPIXEL_Lib db "gdi32",0
_SETPIXEL_Call db 0
%endif
%ifdef BITBLT_Used
_BITBLT dd 0
_BITBLT_LibHandle dd 0
_BITBLT_Alias db "BitBlt",0
_BITBLT_Lib db "gdi32",0
_BITBLT_Call db 0
%endif
%ifdef CREATEMENU_Used
_CREATEMENU dd 0
_CREATEMENU_LibHandle dd 0
_CREATEMENU_Alias db "CreateMenu",0
_CREATEMENU_Lib db "user32",0
_CREATEMENU_Call db 0
%endif
%ifdef APPENDMENU_Used
_APPENDMENU dd 0
_APPENDMENU_LibHandle dd 0
_APPENDMENU_Alias db "AppendMenuA",0
_APPENDMENU_Lib db "user32",0
_APPENDMENU_Call db 0
%endif
%ifdef POSTMESSAGE_Used
_POSTMESSAGE dd 0
_POSTMESSAGE_LibHandle dd 0
_POSTMESSAGE_Alias db "PostMessageA",0
_POSTMESSAGE_Lib db "user32",0
_POSTMESSAGE_Call db 0
%endif
%ifdef GETSYSTEMMETRICS_Used
_GETSYSTEMMETRICS dd 0
_GETSYSTEMMETRICS_LibHandle dd 0
_GETSYSTEMMETRICS_Alias db "GetSystemMetrics",0
_GETSYSTEMMETRICS_Lib db "user32",0
_GETSYSTEMMETRICS_Call db 0
%endif
%ifdef SETWINDOWPOS_Used
_SETWINDOWPOS dd 0
_SETWINDOWPOS_LibHandle dd 0
_SETWINDOWPOS_Alias db "SetWindowPos",0
_SETWINDOWPOS_Lib db "user32",0
_SETWINDOWPOS_Call db 0
%endif
%ifdef ENABLEWINDOW_Used
_ENABLEWINDOW dd 0
_ENABLEWINDOW_LibHandle dd 0
_ENABLEWINDOW_Alias db "EnableWindow",0
_ENABLEWINDOW_Lib db "user32",0
_ENABLEWINDOW_Call db 0
%endif
%ifdef PLAYSOUND_Used
_PLAYSOUND dd 0
_PLAYSOUND_LibHandle dd 0
_PLAYSOUND_Alias db "PlaySoundA",0
_PLAYSOUND_Lib db "winmm",0
_PLAYSOUND_Call db 0
%endif
STRUC Scope68__MMTIME_TYPE
.Scope68__WTYPE_Integer resd 1
.Scope68__U_Integer resd 1
ENDSTRUC
%ifdef TIMESETEVENT_Used
_TIMESETEVENT dd 0
_TIMESETEVENT_LibHandle dd 0
_TIMESETEVENT_Alias db "timeSetEvent",0
_TIMESETEVENT_Lib db "winmm.dll",0
_TIMESETEVENT_Call db 0
%endif
%ifdef TIMEKILLEVENT_Used
_TIMEKILLEVENT dd 0
_TIMEKILLEVENT_LibHandle dd 0
_TIMEKILLEVENT_Alias db "timeKillEvent",0
_TIMEKILLEVENT_Lib db "winmm.dll",0
_TIMEKILLEVENT_Call db 0
%endif
%ifdef CREATEFONT_Used
_CREATEFONT dd 0
_CREATEFONT_LibHandle dd 0
_CREATEFONT_Alias db "CreateFontA",0
_CREATEFONT_Lib db "gdi32",0
_CREATEFONT_Call db 0
%endif
Number_1 dq 0.0
String_2 db "Message:",0
Number_3 dq 0.0
Scope76__MESSAGE_UDT ISTRUC Scope0__MSG_TYPE
IEND
Scope76__WCEX_UDT ISTRUC Scope0__WNDCLASSEX_TYPE
IEND
Scope76__RCT_UDT ISTRUC Scope0__RECT_TYPE
IEND
Scope76__HINST_Integer dd 0
Scope76__HWINDOW_Integer dd 0
Scope76__HICON_Integer dd 0
Scope76__HFONT_Integer dd 0
Scope76__HSTATICICON_Integer dd 0
Scope76__HSTATICCLOCK_Integer dd 0
Scope76__HBUTTONSTART_Integer dd 0
Scope76__HBUTTONSTOP_Integer dd 0
Scope76__HCHKBOXCLICK_Integer dd 0
Scope76__DWTIMERID_Integer dd 0
Scope76__DWTICKCOUNT_Integer dd 0
Scope76__DWTENTHS_Integer dd 0
Scope76__DWSECONDS_Integer dd 0
Scope76__DWMINUTES_Integer dd 0
Scope76__DWHOURS_Integer dd 0
Scope76__STRELAPSED_String dd 0
Scope76__STRCLASSNAME_String dd 0
Scope76__STRAPPTITLE_String dd 0
Number_4 dq 0.0
Number_5 dq 2.0
Number_6 dq 1.0
Number_7 dq 2.0
Number_8 dq 0.0
Number_9 dq 0.0
Number_10 dq 0.0
Number_11 dq 4.0
Number_12 dq 1.0
Number_13 dq 16.0
Number_14 dq 1.0
Number_15 dq 10.0
Number_16 dq 10.0
Number_17 dq 60.0
Number_18 dq 600.0
Number_19 dq 60.0
Number_20 dq 36000.0
Number_21 dq 60.0
Number_22 dq 12.0
Number_23 dq 0.0
Number_24 dq 0.0
Number_25 dq 240.0
Number_26 dq 0.0
Number_27 dq 0.0
Number_28 dq 1.0
Number_29 dq 262148.0
Number_30 dq 1.0
Number_31 dq 0.0
String_32 db "Button",0
String_33 db "",0
Number_34 dq 7.0
Number_35 dq 1073741824.0
Number_36 dq 268435456.0
Number_37 dq 5.0
Number_38 dq 0.0
Number_39 dq 309.0
Number_40 dq 90.0
Number_41 dq 0.0
Number_42 dq 0.0
Number_43 dq 0.0
String_44 db "Static",0
String_45 db "",0
Number_46 dq 3.0
Number_47 dq 1073741824.0
Number_48 dq 268435456.0
Number_49 dq 19.0
Number_50 dq 16.0
Number_51 dq 32.0
Number_52 dq 32.0
Number_53 dq 101.0
Number_54 dq 0.0
Number_55 dq 0.0
String_56 db "Static",0
String_57 db "00:00:00.0",0
Number_58 dq 1.0
Number_59 dq 1073741824.0
Number_60 dq 268435456.0
Number_61 dq 67.0
Number_62 dq 16.0
Number_63 dq 140.0
Number_64 dq 32.0
Number_65 dq 102.0
Number_66 dq 0.0
Number_67 dq 0.0
String_68 db "Button",0
String_69 db "Start",0
Number_70 dq 0.0
Number_71 dq 1073741824.0
Number_72 dq 268435456.0
Number_73 dq 225.0
Number_74 dq 19.0
Number_75 dq 75.0
Number_76 dq 25.0
Number_77 dq 103.0
Number_78 dq 0.0
Number_79 dq 0.0
String_80 db "Button",0
String_81 db "Stop",0
Number_82 dq 0.0
Number_83 dq 1073741824.0
Number_84 dq 268435456.0
Number_85 dq 225.0
Number_86 dq 54.0
Number_87 dq 75.0
Number_88 dq 25.0
Number_89 dq 104.0
Number_90 dq 0.0
Number_91 dq 0.0
String_92 db "Button",0
String_93 db "Sound on the second",0
Number_94 dq 3.0
Number_95 dq 1073741824.0
Number_96 dq 268435456.0
Number_97 dq 21.0
Number_98 dq 60.0
Number_99 dq 129.0
Number_100 dq 17.0
Number_101 dq 104.0
Number_102 dq 0.0
Number_103 dq 370.0
Number_104 dq 1.0
Number_105 dq 48.0
Number_106 dq 0.0
Number_107 dq 48.0
Number_108 dq 12.0
Number_109 dq 0.0
Number_110 dq 48.0
Number_111 dq 12.0
Number_112 dq 0.0
Number_113 dq 48.0
Number_114 dq 12.0
Number_115 dq 0.0
Number_116 dq 0.0
Number_117 dq 0.0
Number_118 dq 0.0
Number_119 dq -1.0
Number_120 dq 0.0
Number_121 dq 100.0
Number_122 dq 50.0
Number_123 dq 0.0
Number_124 dq 1.0
Number_125 dq -1.0
Number_126 dq 0.0
Number_127 dq 0.0
Number_128 dq 0.0
Number_129 dq 0.0
Number_130 dq 0.0
Number_131 dq 0.0
Number_132 dq 1.0
Number_133 dq 273.0
Number_134 dq 16.0
Number_135 dq 2.0
Number_136 dq 0.0
Number_137 dq 0.0
String_138 db "Stop Watch",0
String_139 db "StopWatch",0
Number_140 dq 0.0
Number_141 dq 32.0
Number_142 dq 14.0
Number_143 dq 0.0
Number_144 dq 0.0
Number_145 dq 700.0
Number_146 dq 0.0
Number_147 dq 0.0
Number_148 dq 0.0
Number_149 dq 0.0
Number_150 dq 2.0
Number_151 dq 0.0
Number_152 dq 0.0
Number_153 dq 1.0
Number_154 dq 48.0
String_155 db "Courier New",0
Number_156 dq 1.0
Number_157 dq 2.0
Number_158 dq 0.0
Number_159 dq 0.0
Number_160 dq 0.0
Number_161 dq 32512.0
Number_162 dq 15.0
Number_163 dq 1.0
String_164 db "",0
Number_165 dq 0.0
Number_166 dq 0.0
Number_167 dq 0.0
String_168 db "RegisterClassEx failed.",0
Number_169 dq 0.0
Number_170 dq 0.0
Number_171 dq 0.0
Number_172 dq 0.0
Number_173 dq 12582912.0
Number_174 dq 524288.0
Number_175 dq 131072.0
Number_176 dq 0.0
Number_177 dq 0.0
Number_178 dq 327.0
Number_179 dq 131.0
Number_180 dq 0.0
Number_181 dq 0.0
Number_182 dq 0.0
Number_183 dq 0.0
Number_184 dq 0.0
String_185 db "CreateWindowEx failed.",0
Number_186 dq 0.0
Number_187 dq 0.0
Number_188 dq 1.0
Number_189 dq 0.0
Number_190 dq 0.0
Number_191 dq 0.0
Number_192 dq 0.0
ExitStatus dd 0

