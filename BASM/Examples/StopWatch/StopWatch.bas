'*****************
'* StopWatch.bas *
'*****************

$AppType GUI 
$Optimize On
$Compress On
$XPStyle 

$Resource StopWatchWav As "StopWatch.wav"
$Resource StopWatchIco As "StopWatch.ico"

$Const True = -1
$Const False = 0

$Include "Windows.inc" 

Dim message As MSG 
Dim wcex As WNDCLASSEX 
Dim rct As RECT
Dim hInst As Integer
Dim hWindow As Integer
Dim hIcon As Integer
Dim hFont As Integer
Dim hStaticIcon As Integer
Dim hStaticClock As Integer
Dim hButtonStart As Integer
Dim hButtonStop As Integer
Dim hChkBoxClick As Integer
Dim dwTimerID As Integer
Dim dwTickCount As Integer
Dim dwTenths As Integer
Dim dwSeconds As Integer
Dim dwMinutes As Integer
Dim dwHours As Integer
Dim strElapsed As String
Dim strClassName As String
Dim strAppTitle As String

Function CenterOnDesktop(hWindow As Integer) As Integer
  Dim xPos As Integer
  Dim yPos As Integer
  GetWindowRect(hWindow, rct)
  xPos = (GetSystemMetrics(SM_CXSCREEN) - (rct.right - rct.left)) / 2
  yPos = (GetSystemMetrics(SM_CYSCREEN) - (rct.bottom - rct.top)) / 2
  SetWindowPos(hWindow, HWND_TOP, xPos, yPos, 0, 0, _
               SWP_NOZORDER + SWP_NOSIZE + SWP_NOACTIVATE)
End Function

Function FormatClock(dwHours As Integer, dwMinutes As Integer, _
                     dwSeconds As Integer, dwTenths As Integer) As String
  $Asm
    extern sprintf
    stdcall HeapAlloc,dword[HandleToHeap],8,12
    mov ebx,eax 
    invoke sprintf,eax,"%2.2d:%2.2d:%2.2d.%1.1d",dword[ebp+8],dword[ebp+12],dword[ebp+16],dword[ebp+20]
    mov dword[ebp-4],ebx
  $End ASM
End Function

Sub TimeProc(uID As Integer, uMsg As Integer, dwUser As Integer, _ 
             dw1 As Integer, dw2 As Integer)
  dwTickCount = dwTickCount + 1
  dwTenths = dwTickCount Mod 10
  dwSeconds = dwTickCount / 10 Mod 60 
  dwMinutes = dwTickCount / 600 Mod 60 
  dwHours = dwTickCount / 36000 Mod 60
  strElapsed = FormatClock(dwHours, dwMinutes, dwSeconds, dwTenths)
  SendMessage(hStaticClock, WM_SETTEXT, 0, AddressOf(strElapsed))
  If dwTenths = 0 Then
    If SendMessage(hChkBoxClick, BM_GETCHECK, 0, 0) = BST_CHECKED Then
      PlaySound(MakeIntResource(StopWatchWav), hInst, SND_RESOURCE + SND_ASYNC)
    End If
  End If
End Sub

Function OnCreate(hWnd As Integer, uMsg As Integer, _ 
                  wParam As Integer, lParam As Integer) As Integer
  CreateWindowEx(0, "Button", "", _ 
                 BS_GROUPBOX + WS_CHILD + WS_VISIBLE, _ 
                 5, 0, 309, 90, hWnd, 0, hInst, 0)
  hStaticIcon =  CreateWindowEx(0, "Static", "", _ 
                                SS_ICON + WS_CHILD + WS_VISIBLE, _ 
                                19, 16, 32, 32, hWnd, 101, hInst, 0)
  hStaticClock = CreateWindowEx(0, "Static", "00:00:00.0", _ 
                                SS_CENTER + WS_CHILD + WS_VISIBLE, _ 
                                67, 16, 140, 32, hWnd, 102, hInst, 0)    
  hButtonStart = CreateWindowEx(0, "Button", "Start", _ 
                                BS_PUSHBUTTON + WS_CHILD + WS_VISIBLE, _ 
                                225, 19, 75, 25, hWnd, 103, hInst, 0)
  hButtonStop = CreateWindowEx(0, "Button", "Stop", _ 
                               BS_PUSHBUTTON + WS_CHILD + WS_VISIBLE, _ 
                               225, 54, 75, 25, hWnd, 104, hInst, 0)
  hChkBoxClick = CreateWindowEx(0, "Button", "Sound on the second", _ 
                                BS_AUTOCHECKBOX + WS_CHILD + WS_VISIBLE, _ 
                                21, 60, 129, 17, hWnd, 104, hInst, 0)

  SendMessage(hStaticIcon, STM_SETIMAGE, IMAGE_ICON, hIcon)
  SendMessage(hStaticClock, WM_SETFONT, hFont, 0)
  SendMessage(hButtonStart, WM_SETFONT, GetStockObject(ANSI_VAR_FONT), 0)
  SendMessage(hButtonStop, WM_SETFONT, GetStockObject(ANSI_VAR_FONT), 0)
  SendMessage(hChkBoxClick, WM_SETFONT, GetStockObject(ANSI_VAR_FONT), 0)
  EnableWindow(hButtonStop, false)
 
  Result = 0
End Function

Function OnCommand(hWnd As Integer, uMsg As Integer, _ 
                   wParam As Integer, lParam As Integer) As Integer
  If lParam = hButtonStart Then 
    EnableWindow(hButtonStart, false)
    EnableWindow(hButtonStop, true)
    dwTickCount = 0
    dwTimerID = timeSetEvent(100, 50, CodePtr(TimeProc), 0, TIME_PERIODIC)
  End If
  If lParam = hButtonStop Then
    EnableWindow(hButtonStart, true)
    EnableWindow(hButtonStop, false)
    timeKillEvent(dwTimerID)
    dwTickCount = 0
    dwTimerID = 0
  End If
  Result = 0
End Function

Function OnClose(hWnd As Integer, uMsg As Integer, _ 
                 wParam As Integer, lParam As Integer) As Integer
  If dwTimerID <> 0 Then
    timeKillEvent(dwTimerID) 
  End If
  DestroyWindow(hWindow)
  Result = 0
End Function

Function WindowProc(hWnd As Integer, uMsg As Integer, _ 
                    wParam As Integer, lParam As Integer) As Integer
  If uMsg = WM_CREATE Then 
    Result = OnCreate(hWnd, uMsg, wParam, lParam)
  ElseIf uMsg = WM_COMMAND Then
    Result = OnCommand(hWnd, uMsg, wParam, lParam)
  ElseIf uMsg = WM_CLOSE Then
    Result = OnClose(hWnd, uMsg, wParam, lParam)
  ElseIf uMsg = WM_DESTROY Then 
    PostQuitMessage(0) 
    Result = 0
  Else 
    Result = DefWindowProc(hWnd, uMsg, wParam, lParam)
  End If 
End Function

'***

strAppTitle = "Stop Watch" 
strClassName = "StopWatch"

hInst = GetModuleHandle(0)
hIcon = LoadIcon(hInstance, MakeIntResource(StopWatchIco))
hFont = CreateFont(32, 14, 0, 0, FW_BOLD, false, false, false, _ 
                   ANSI_CHARSET, OUT_CHARACTER_PRECIS, CLIP_DEFAULT_PRECIS, _ 
                   DEFAULT_QUALITY, FIXED_PITCH + FF_MODERN, "Courier New")

wcex.cbSize = SizeOf(WNDCLASSEX) 
wcex.style = CS_VREDRAW + CS_HREDRAW
wcex.lpfnwndproc = CodePtr(WindowProc) 
wcex.cbClsExtra = 0 
wcex.cbWndExtra = 0 
wcex.hInstance = hInst
wcex.hIcon = LoadIcon(hInst, MakeIntResource(StopWatchIco)) 
wcex.hCursor = LoadCursor(0, MakeIntResource(IDC_ARROW)) 
wcex.hbrBackground = COLOR_BTNFACE + 1
wcex.lpszMenuName = "" 
wcex.lpszClassName = strClassName 
wcex.hIconSm = 0 

If (RegisterClassEx(wcex)) = 0 Then 
  MessageBox(0, "RegisterClassEx failed.", strAppTitle, MB_OK)
  ExitProcess(0)
End If 

hWindow = CreateWindowEx(0, strClassName, strAppTitle, _ 
                         WS_OVERLAPPED + WS_CAPTION + WS_SYSMENU + WS_MINIMIZEBOX, _
                         0, 0, 327, 131, 0, 0, hInst, 0)
If hWindow = 0 Then 
  MessageBox(0, "CreateWindowEx failed.", strAppTitle, MB_OK)
  ExitProcess(0)
End If 

CenterOnDesktop(hWindow)
ShowWindow(hWindow, SW_SHOWNORMAL) 
UpdateWindow(hWindow) 

While GetMessage(message, 0, 0, 0) > 0
  TranslateMessage(message) 
  DispatchMessage(message) 
Wend 

DeleteObject(hFont)

