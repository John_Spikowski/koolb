;Library functions to import to the Chameleon app
extern ExitProcess
extern MessageBoxA
extern lstrcat
extern lstrlen
extern lstrcpy
extern GetProcessHeap
extern HeapCreate
extern HeapAlloc
extern HeapFree
extern FreeLibrary
extern floor
extern lstrlenA
extern lstrcpyA
extern LoadLibraryA
extern GetProcAddress
extern HeapDestroy



;Initialize everything to prep the app to run
section .text
%include "C:\BASM\Bin\MACROS.INC"
global START
START:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH ESI
PUSH EDI



;The main body where the app actually runs



;Prepare the app to exit and then terminate
Exit:
POP EDI
POP ESI
POP EBX
MOV ESP,EBP
POP EBP
MOV EAX,1
RET 12

NoMemory:
MOV dword[ExitStatus],1
stdcall MessageBoxA,0,NoMemMessage,Error,0
JMP Exit

NoLibrary:
MOV dword[ExitStatus],1
POP EBX
stdcall lstrlen,EBX
ADD EAX,30
CALL GetProcessHeap
stdcall HeapAlloc,EAX,8,EAX
MOV EDI,EAX
stdcall lstrcpy,EDI,NoLibFound
stdcall lstrcat,EDI,EBX
stdcall MessageBoxA,0,EDI,Error,0
CALL GetProcessHeap
stdcall HeapAlloc,EAX,8,EDI
JMP Exit

NoFunction:
MOV dword[ExitStatus],1
POP EBX
stdcall lstrlen,EBX
ADD EAX,30
CALL GetProcessHeap
stdcall HeapAlloc,EAX,8,EAX
MOV EDI,EAX
stdcall lstrcpy,EDI,NoFunctionFound
stdcall lstrcat,EDI,EBX
stdcall MessageBoxA,0,EDI,Error,0
CALL GetProcessHeap
stdcall HeapAlloc,EAX,8,EDI
JMP Exit





global MESSAGE
MESSAGE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

PUSH dword[ParameterPool]
CALL GetProcessHeap
stdcall HeapAlloc,EAX,8,16
CMP EAX,0
JNE Label2
JMP NoMemory
Label2:
MOV dword[ParameterPool],EAX
PUSH dword[Number_1+4]
PUSH dword[Number_1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
CALL GetProcessHeap
stdcall HeapAlloc,EAX,8,EBX
CMP EAX,0
JNE Label3
JMP NoMemory
Label3:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EBX,String_2
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
CALL GetProcessHeap
stdcall HeapAlloc,EAX,8,EDI
CMP EAX,0
JNE Label4
JMP NoMemory
Label4:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_2
PUSH EBX
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Number_3+4]
PUSH dword[Number_3]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_MESSAGEBOX],0
JNE Label5
stdcall LoadLibraryA,_MESSAGEBOX_Lib
MOV dword[_MESSAGEBOX_LibHandle],EAX
CMP EAX,0
JNE Label6
PUSH _MESSAGEBOX_Lib
JMP NoLibrary
Label6:
stdcall GetProcAddress,dword[_MESSAGEBOX_LibHandle],_MESSAGEBOX_Alias
MOV dword[_MESSAGEBOX],EAX
CMP EAX,0
JNE Label7
PUSH _MESSAGEBOX_Alias
JMP NoFunction
Label7:
Label5:
CALL dword[_MESSAGEBOX]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
CALL GetProcessHeap
stdcall HeapFree,EAX,0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
POP ECX
CALL GetProcessHeap
stdcall HeapFree,EAX,0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
CALL GetProcessHeap
stdcall HeapFree,EAX,0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]

stdcall FreeLibrary,dword[_MESSAGEBOX_LibHandle]

MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4



;Data section of the Chameleon app
section .data
NoMemMessage db "Could not allocate memory.",0
NoLibFound db "Could not find library: ",0
NoFunctionFound db "Cound not find function: ",0
Error db "Error!",0
ParameterPool dd 0
TempQWord1 dq 0.0
TempQWord2 dq 0.0
True dd -1
_MESSAGEBOX dd 0
_MESSAGEBOX_LibHandle dd 0
_MESSAGEBOX_Alias db "MessageBoxA",0
_MESSAGEBOX_Lib db "User32.dll",0
_MESSAGEBOX_Call db 0
Number_1 dq 0.0
String_2 db "KoolB DLL showing you a little message:",0
Number_3 dq 0.0
ExitStatus dd 0

