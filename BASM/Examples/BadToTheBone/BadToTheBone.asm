;Library functions to import to the Chameleon app
extern ExitProcess
extern MessageBoxA
extern lstrcat
extern lstrlen
extern lstrcpy
extern HeapCreate
extern HeapAlloc
extern HeapFree
extern GetModuleHandleA
extern GetCommandLineA
extern FreeLibrary
extern floor
extern lstrlenA
extern lstrcpyA
extern LoadLibraryA
extern GetProcAddress
extern HeapDestroy
extern InitCommonControls



;Initialize everything to prep the app to run
section .text
%include "C:\BASM\Bin\MACROS.INC"
global START
START:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH ESI
PUSH EDI
stdcall HeapCreate,0,16384,0
MOV dword[HandleToHeap],EAX
CMP dword[HandleToHeap],0
JNE Label1
JMP NoMemory
Label1:
stdcall GetModuleHandleA,0
MOV dword[Internal_HInstance],EAX
call InitCommonControls



;The main body where the app actually runs
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label47
JMP NoMemory
Label47:
MOV dword[ParameterPool],EAX
PUSH dword[Number_19+4]
PUSH dword[Number_19]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_GETMODULEHANDLE],0
JNE Label48
stdcall LoadLibraryA,_GETMODULEHANDLE_Lib
MOV dword[_GETMODULEHANDLE_LibHandle],EAX
CMP EAX,0
JNE Label49
PUSH _GETMODULEHANDLE_Lib
JMP NoLibrary
Label49:
stdcall GetProcAddress,dword[_GETMODULEHANDLE_LibHandle],_GETMODULEHANDLE_Alias
MOV dword[_GETMODULEHANDLE],EAX
CMP EAX,0
JNE Label50
PUSH _GETMODULEHANDLE_Alias
JMP NoFunction
Label50:
Label48:
CALL dword[_GETMODULEHANDLE]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope81__HMOD_Integer]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,20
CMP EAX,0
JNE Label51
JMP NoMemory
Label51:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope81__HMOD_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EBX,String_20
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label52
JMP NoMemory
Label52:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_20
PUSH EBX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_21+4]
PUSH dword[Number_21]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH _DIALOGPROC
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
PUSH dword[Number_22+4]
PUSH dword[Number_22]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+16]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+16]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_DIALOGBOXPARAM],0
JNE Label53
stdcall LoadLibraryA,_DIALOGBOXPARAM_Lib
MOV dword[_DIALOGBOXPARAM_LibHandle],EAX
CMP EAX,0
JNE Label54
PUSH _DIALOGBOXPARAM_Lib
JMP NoLibrary
Label54:
stdcall GetProcAddress,dword[_DIALOGBOXPARAM_LibHandle],_DIALOGBOXPARAM_Alias
MOV dword[_DIALOGBOXPARAM],EAX
CMP EAX,0
JNE Label55
PUSH _DIALOGBOXPARAM_Alias
JMP NoFunction
Label55:
Label53:
CALL dword[_DIALOGBOXPARAM]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]



;Prepare the app to exit and then terminate
Exit:
stdcall FreeLibrary,dword[_BEGINPAINT_LibHandle]
stdcall FreeLibrary,dword[_ENDPAINT_LibHandle]
stdcall FreeLibrary,dword[_TEXTOUT_LibHandle]
stdcall FreeLibrary,dword[_DRAWTEXT_LibHandle]
stdcall FreeLibrary,dword[_ENUMCHILDWINDOWS_LibHandle]
stdcall FreeLibrary,dword[_GETTOPWINDOW_LibHandle]
stdcall FreeLibrary,dword[_GETNEXTWINDOW_LibHandle]
stdcall FreeLibrary,dword[_GETCLIENTRECT_LibHandle]
stdcall FreeLibrary,dword[_GETWINDOWRECT_LibHandle]
stdcall FreeLibrary,dword[_REGISTERCLASS_LibHandle]
stdcall FreeLibrary,dword[_REGISTERCLASSEX_LibHandle]
stdcall FreeLibrary,dword[_UNREGISTERCLASS_LibHandle]
stdcall FreeLibrary,dword[_GETMESSAGE_LibHandle]
stdcall FreeLibrary,dword[_TRANSLATEMESSAGE_LibHandle]
stdcall FreeLibrary,dword[_DISPATCHMESSAGE_LibHandle]
stdcall FreeLibrary,dword[_SENDMESSAGE_LibHandle]
stdcall FreeLibrary,dword[_LOADBITMAP_LibHandle]
stdcall FreeLibrary,dword[_LOADCURSOR_LibHandle]
stdcall FreeLibrary,dword[_LOADICON_LibHandle]
stdcall FreeLibrary,dword[_DESTROYICON_LibHandle]
stdcall FreeLibrary,dword[_GETCURSOR_LibHandle]
stdcall FreeLibrary,dword[_SETCURSOR_LibHandle]
stdcall FreeLibrary,dword[_COPYICON_LibHandle]
stdcall FreeLibrary,dword[_SETSYSTEMCURSOR_LibHandle]
stdcall FreeLibrary,dword[_CREATECOMPATIBLEDC_LibHandle]
stdcall FreeLibrary,dword[_CREATEWINDOWEX_LibHandle]
stdcall FreeLibrary,dword[_UPDATEWINDOW_LibHandle]
stdcall FreeLibrary,dword[_SHOWWINDOW_LibHandle]
stdcall FreeLibrary,dword[_DEFWINDOWPROC_LibHandle]
stdcall FreeLibrary,dword[_POSTQUITMESSAGE_LibHandle]
stdcall FreeLibrary,dword[_GETMODULEHANDLE_LibHandle]
stdcall FreeLibrary,dword[_GETACTIVEWINDOW_LibHandle]
stdcall FreeLibrary,dword[_EXITPROCESS_LibHandle]
stdcall FreeLibrary,dword[_MSGBOX_LibHandle]
stdcall FreeLibrary,dword[_MESSAGEBOX_LibHandle]
stdcall FreeLibrary,dword[_GETLASTERROR_LibHandle]
stdcall FreeLibrary,dword[_SETLASTERROR_LibHandle]
stdcall FreeLibrary,dword[_FORMATMESSAGE_LibHandle]
stdcall FreeLibrary,dword[_LOCALFREE_LibHandle]
stdcall FreeLibrary,dword[_GETWINDOWLONG_LibHandle]
stdcall FreeLibrary,dword[_SETWINDOWLONG_LibHandle]
stdcall FreeLibrary,dword[_CALLWINDOWPROC_LibHandle]
stdcall FreeLibrary,dword[_GETCLASSLONG_LibHandle]
stdcall FreeLibrary,dword[_SETCLASSLONG_LibHandle]
stdcall FreeLibrary,dword[_GETDESKTOPWINDOW_LibHandle]
stdcall FreeLibrary,dword[_GETPROCADDRESS_LibHandle]
stdcall FreeLibrary,dword[_DIALOGBOXPARAM_LibHandle]
stdcall FreeLibrary,dword[_ENDDIALOG_LibHandle]
stdcall FreeLibrary,dword[_GETDLGITEM_LibHandle]
stdcall FreeLibrary,dword[_INITCOMMONCONTROLS_LibHandle]
stdcall FreeLibrary,dword[_INVALIDATERECT_LibHandle]
stdcall FreeLibrary,dword[_CREATEPEN_LibHandle]
stdcall FreeLibrary,dword[_SELECTOBJECT_LibHandle]
stdcall FreeLibrary,dword[_GETSTOCKOBJECT_LibHandle]
stdcall FreeLibrary,dword[_CREATESOLIDBRUSH_LibHandle]
stdcall FreeLibrary,dword[_RECTANGLE_LibHandle]
stdcall FreeLibrary,dword[_DELETEOBJECT_LibHandle]
stdcall FreeLibrary,dword[_DELETEDC_LibHandle]
stdcall FreeLibrary,dword[_DESTROYWINDOW_LibHandle]
stdcall FreeLibrary,dword[_SETPIXEL_LibHandle]
stdcall FreeLibrary,dword[_BITBLT_LibHandle]
stdcall FreeLibrary,dword[_CREATEMENU_LibHandle]
stdcall FreeLibrary,dword[_APPENDMENU_LibHandle]
stdcall FreeLibrary,dword[_POSTMESSAGE_LibHandle]
stdcall FreeLibrary,dword[_GETSYSTEMMETRICS_LibHandle]
stdcall FreeLibrary,dword[_SETWINDOWPOS_LibHandle]
stdcall FreeLibrary,dword[_ENABLEWINDOW_LibHandle]
stdcall FreeLibrary,dword[_PLAYSOUND_LibHandle]
stdcall FreeLibrary,dword[_TIMESETEVENT_LibHandle]
stdcall FreeLibrary,dword[_TIMEKILLEVENT_LibHandle]
stdcall FreeLibrary,dword[_CREATEFONT_LibHandle]
stdcall HeapDestroy,dword[HandleToHeap]
POP EDI
POP ESI
POP EBX
MOV ESP,EBP
POP EBP
MOV EAX,dword[ExitStatus]
stdcall ExitProcess,dword[ExitStatus]
RET

NoMemory:
MOV dword[ExitStatus],1
stdcall MessageBoxA,0,NoMemMessage,Error,0
JMP Exit

NoLibrary:
MOV dword[ExitStatus],1
POP EBX
stdcall lstrlen,EBX
ADD EAX,30
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
MOV EDI,EAX
stdcall lstrcpy,EDI,NoLibFound
stdcall lstrcat,EDI,EBX
stdcall MessageBoxA,0,EDI,Error,0
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
JMP Exit

NoFunction:
MOV dword[ExitStatus],1
POP EBX
stdcall lstrlen,EBX
ADD EAX,30
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
MOV EDI,EAX
stdcall lstrcpy,EDI,NoFunctionFound
stdcall lstrcat,EDI,EBX
stdcall MessageBoxA,0,EDI,Error,0
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
JMP Exit























































































































































































































global _SHOWMESSAGE
_SHOWMESSAGE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label2
JMP NoMemory
Label2:
MOV dword[ParameterPool],EAX
PUSH dword[Number_1+4]
PUSH dword[Number_1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label3
JMP NoMemory
Label3:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EBX,String_2
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label4
JMP NoMemory
Label4:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_2
PUSH EBX
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Number_3+4]
PUSH dword[Number_3]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_MESSAGEBOX],0
JNE Label5
stdcall LoadLibraryA,_MESSAGEBOX_Lib
MOV dword[_MESSAGEBOX_LibHandle],EAX
CMP EAX,0
JNE Label6
PUSH _MESSAGEBOX_Lib
JMP NoLibrary
Label6:
stdcall GetProcAddress,dword[_MESSAGEBOX_LibHandle],_MESSAGEBOX_Alias
MOV dword[_MESSAGEBOX],EAX
CMP EAX,0
JNE Label7
PUSH _MESSAGEBOX_Alias
JMP NoFunction
Label7:
Label5:
CALL dword[_MESSAGEBOX]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _LOWORD
_LOWORD:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

mov eax,dword[ebp+8]   ;dwValue
and eax,65535
mov dword[ebp-4],eax


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _HIWORD
_HIWORD:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

mov eax,dword[ebp+8]
and ecx,16
shr eax,cl 
mov dword[ebp-4],eax


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _MAKEINTRESOURCE
_MAKEINTRESOURCE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label8
JMP NoMemory
Label8:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

extern sprintf
jmp forward
strFormat db '#%010ld',0
forward:
stdcall HeapAlloc,dword[HandleToHeap],8,12 
mov ebx,eax 
ccall sprintf,eax,strFormat,dword[ebp+8]
mov dword[ebp-4],ebx


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _RETURN
_RETURN:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

mov   eax,[ebp+8] 


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4


global _OR
_OR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

MOV EBX,dword[EBP+8]
MOV EDI,dword[EBP+12]
OR  EBX,EDI
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8


global _BOR
_BOR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

MOV EBX,dword[EBP+8]
MOV EDI,dword[EBP+12]
OR  EBX,EDI
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8


global _BAND
_BAND:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

MOV EBX,dword[EBP+8]
MOV EDI,dword[EBP+12]
AND EBX,EDI
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8


global _BXOR
_BXOR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

MOV EBX,dword[EBP+8]
MOV EDI,dword[EBP+12]
XOR EBX,EDI
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8


global _SHIFTLEFT
_SHIFTLEFT:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

mov eax,dword[ebp+8]   ;nBase
mov ecx,dword[ebp+12]  ;nBits
shl eax,cl  
mov dword[ebp-4],eax


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8


global _DIALOGPROC
_DIALOGPROC:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_4+4]
PUSH dword[Number_4]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label9
FLDZ
Label9:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label11
JMP Label10
Label11:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label13
JMP NoMemory
Label13:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope81__HMOD_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label14
JMP NoMemory
Label14:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope81__Resource_ICONRES_101]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MAKEINTRESOURCE
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LOADICON],0
JNE Label15
stdcall LoadLibraryA,_LOADICON_Lib
MOV dword[_LOADICON_LibHandle],EAX
CMP EAX,0
JNE Label16
PUSH _LOADICON_Lib
JMP NoLibrary
Label16:
stdcall GetProcAddress,dword[_LOADICON_LibHandle],_LOADICON_Alias
MOV dword[_LOADICON],EAX
CMP EAX,0
JNE Label17
PUSH _LOADICON_Alias
JMP NoFunction
Label17:
Label15:
CALL dword[_LOADICON]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope81__HICON_Integer]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label18
JMP NoMemory
Label18:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_5+4]
PUSH dword[Number_5]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_6+4]
PUSH dword[Number_6]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
FINIT
FILD dword[Scope81__HICON_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_SENDMESSAGE],0
JNE Label19
stdcall LoadLibraryA,_SENDMESSAGE_Lib
MOV dword[_SENDMESSAGE_LibHandle],EAX
CMP EAX,0
JNE Label20
PUSH _SENDMESSAGE_Lib
JMP NoLibrary
Label20:
stdcall GetProcAddress,dword[_SENDMESSAGE_LibHandle],_SENDMESSAGE_Alias
MOV dword[_SENDMESSAGE],EAX
CMP EAX,0
JNE Label21
PUSH _SENDMESSAGE_Alias
JMP NoFunction
Label21:
Label19:
CALL dword[_SENDMESSAGE]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[Number_7+4]
PUSH dword[Number_7]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label12
Label10:
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_8+4]
PUSH dword[Number_8]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label22
FLDZ
Label22:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label24
JMP Label23
Label24:
FINIT
FILD dword[EBP+16]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label25
JMP NoMemory
Label25:
MOV dword[ParameterPool],EAX
PUSH dword[Number_9+4]
PUSH dword[Number_9]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_10+4]
PUSH dword[Number_10]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _SHIFTLEFT
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_11+4]
PUSH dword[Number_11]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label26
FLDZ
Label26:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label28
JMP Label27
Label28:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label30
JMP NoMemory
Label30:
MOV dword[ParameterPool],EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label31
JMP NoMemory
Label31:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope81__Resource_WAVERES_102]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MAKEINTRESOURCE
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[Scope81__HMOD_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_12+4]
PUSH dword[Number_12]
PUSH dword[Number_13+4]
PUSH dword[Number_13]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_PLAYSOUND],0
JNE Label32
stdcall LoadLibraryA,_PLAYSOUND_Lib
MOV dword[_PLAYSOUND_LibHandle],EAX
CMP EAX,0
JNE Label33
PUSH _PLAYSOUND_Lib
JMP NoLibrary
Label33:
stdcall GetProcAddress,dword[_PLAYSOUND_LibHandle],_PLAYSOUND_Alias
MOV dword[_PLAYSOUND],EAX
CMP EAX,0
JNE Label34
PUSH _PLAYSOUND_Alias
JMP NoFunction
Label34:
Label32:
CALL dword[_PLAYSOUND]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[Number_14+4]
PUSH dword[Number_14]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label27
Label27:
Label29:
JMP Label12
Label23:
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_15+4]
PUSH dword[Number_15]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label35
FLDZ
Label35:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label37
JMP Label36
Label37:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label38
JMP NoMemory
Label38:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope81__HICON_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_DESTROYICON],0
JNE Label39
stdcall LoadLibraryA,_DESTROYICON_Lib
MOV dword[_DESTROYICON_LibHandle],EAX
CMP EAX,0
JNE Label40
PUSH _DESTROYICON_Lib
JMP NoLibrary
Label40:
stdcall GetProcAddress,dword[_DESTROYICON_LibHandle],_DESTROYICON_Alias
MOV dword[_DESTROYICON],EAX
CMP EAX,0
JNE Label41
PUSH _DESTROYICON_Alias
JMP NoFunction
Label41:
Label39:
CALL dword[_DESTROYICON]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label42
JMP NoMemory
Label42:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_16+4]
PUSH dword[Number_16]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_ENDDIALOG],0
JNE Label43
stdcall LoadLibraryA,_ENDDIALOG_Lib
MOV dword[_ENDDIALOG_LibHandle],EAX
CMP EAX,0
JNE Label44
PUSH _ENDDIALOG_Lib
JMP NoLibrary
Label44:
stdcall GetProcAddress,dword[_ENDDIALOG_LibHandle],_ENDDIALOG_Alias
MOV dword[_ENDDIALOG],EAX
CMP EAX,0
JNE Label45
PUSH _ENDDIALOG_Alias
JMP NoFunction
Label45:
Label43:
CALL dword[_ENDDIALOG]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[Number_17+4]
PUSH dword[Number_17]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label12
Label36:
PUSH dword[Number_18+4]
PUSH dword[Number_18]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label46
Label46:
Label12:


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 16



;Data section of the Chameleon app
section .data
NoMemMessage db "Could not allocate memory.",0
NoLibFound db "Could not find library: ",0
NoFunctionFound db "Cound not find function: ",0
Error db "Error!",0
ParameterPool dd 0
HandleToHeap dd 0
TempQWord1 dq 0.0
TempQWord2 dq 0.0
True dd -1
Internal_HInstance dd 0
Internal_CommandLine dd 0
NoConsoleMessage db 'Error - Cannot access the console handles for Input/Output.',0
ConsoleTemp dd 0
ConsoleNewLine db 10,0
ConsoleClear db 'CLS',0
ConsolePause db 'PAUSE',0
HandleToInput dd 0
HandleToOutput dd 0
STRUC Scope0__POINT_TYPE
.Scope0__X_Integer resd 1
.Scope0__Y_Integer resd 1
ENDSTRUC
STRUC Scope0__RECT_TYPE
.Scope0__LEFT_Integer resd 1
.Scope0__TOP_Integer resd 1
.Scope0__RIGHT_Integer resd 1
.Scope0__BOTTOM_Integer resd 1
ENDSTRUC
STRUC Scope0__SIZE_TYPE
.Scope0__CX_Integer resd 1
.Scope0__CY_Integer resd 1
ENDSTRUC
STRUC Scope0__STARTUPINFO_TYPE
.Scope0__CB_Integer resd 1
.Scope0__LPRESERVED_String resd 1
.Scope0__LPDESKTOP_String resd 1
.Scope0__LPTITLE_String resd 1
.Scope0__DWX_Integer resd 1
.Scope0__DWY_Integer resd 1
.Scope0__DWXSIZE_Integer resd 1
.Scope0__DWYSIZE_Integer resd 1
.Scope0__DWXCOUNTCHARS_Integer resd 1
.Scope0__DWYCOUNTCHARS_Integer resd 1
.Scope0__DWFILLATTRIBUTE_Integer resd 1
.Scope0__DWFLAGS_Integer resd 1
.Scope0__WSHOWWINDOW_Integer resd 1
.Scope0__CBRESERVED2_Integer resd 1
.Scope0__LPRESERVED2_Integer resd 1
.Scope0__HSTDINPUT_Integer resd 1
.Scope0__HSTDOUTPUT_Integer resd 1
.Scope0__HSTDERROR_Integer resd 1
ENDSTRUC
STRUC Scope0__NMHDR_TYPE
.Scope0__HWNDFROM_Integer resd 1
.Scope0__IDFROM_Integer resd 1
.Scope0__CODE_Integer resd 1
ENDSTRUC
STRUC Scope0__WNDCLASS_TYPE
.Scope0__STYLE_Integer resd 1
.Scope0__LPFNWNDPROC_Integer resd 1
.Scope0__CBCLSEXTRA_Integer resd 1
.Scope0__CBWNDEXTRA2_Integer resd 1
.Scope0__HINSTANCE_Integer resd 1
.Scope0__HICON_Integer resd 1
.Scope0__HCURSOR_Integer resd 1
.Scope0__HBRBACKGROUND_Integer resd 1
.Scope0__LPSZMENUNAME_String resd 1
.Scope0__LPSZCLASSNAME_String resd 1
ENDSTRUC
STRUC Scope0__WNDCLASSEX_TYPE
.Scope0__CBSIZE_Integer resd 1
.Scope0__STYLE_Integer resd 1
.Scope0__LPFNWNDPROC_Integer resd 1
.Scope0__CBCLSEXTRA_Integer resd 1
.Scope0__CBWNDEXTRA_Integer resd 1
.Scope0__HINSTANCE_Integer resd 1
.Scope0__HICON_Integer resd 1
.Scope0__HCURSOR_Integer resd 1
.Scope0__HBRBACKGROUND_Integer resd 1
.Scope0__LPSZMENUNAME_String resd 1
.Scope0__LPSZCLASSNAME_String resd 1
.Scope0__HICONSM_Integer resd 1
ENDSTRUC
STRUC Scope0__CREATESTRUCT_TYPE
.Scope0__LPCREATEPARAMS_Integer resd 1
.Scope0__HINSTANCE_Integer resd 1
.Scope0__HMENU_Integer resd 1
.Scope0__HWNDPARENT_Integer resd 1
.Scope0__CY_Integer resd 1
.Scope0__CX_Integer resd 1
.Scope0__Y_Integer resd 1
.Scope0__X_Integer resd 1
.Scope0__STYLE_Integer resd 1
.Scope0__LPSZNAME_String resd 1
.Scope0__LPSZCLASS_String resd 1
.Scope0__EXSTYLE_Integer resd 1
ENDSTRUC
STRUC Scope0__MSG_TYPE
.Scope0__HWND_Integer resd 1
.Scope0__MESSAGE_Integer resd 1
.Scope0__WPARAM_Integer resd 1
.Scope0__LPARAM_Integer resd 1
.Scope0__TIME_Integer resd 1
.Scope0__X_Integer resd 1
.Scope0__Y_Integer resd 1
ENDSTRUC
STRUC Scope0__PAINTSTRUCT_TYPE
.Scope0__HDC_Integer resd 1
.Scope0__FERASE_Integer resd 1
.Scope0__LEFT_Integer resd 1
.Scope0__TOP_Integer resd 1
.Scope0__RIGHT_Integer resd 1
.Scope0__BOTTOM_Integer resd 1
.Scope0__FRESTORE_Integer resd 1
.Scope0__FINCUPDATE_Integer resd 1
.Scope0__RGBRESERVED1_Integer resd 1
.Scope0__RGBRESERVED2_Integer resd 1
.Scope0__RGBRESERVED3_Integer resd 1
.Scope0__RGBRESERVED4_Integer resd 1
.Scope0__RGBRESERVED5_Integer resd 1
.Scope0__RGBRESERVED6_Integer resd 1
.Scope0__RGBRESERVED7_Integer resd 1
.Scope0__RGBRESERVED8_Integer resd 1
ENDSTRUC
_BEGINPAINT dd 0
_BEGINPAINT_LibHandle dd 0
_BEGINPAINT_Alias db "BeginPaint",0
_BEGINPAINT_Lib db "user32",0
_BEGINPAINT_Call db 0
_ENDPAINT dd 0
_ENDPAINT_LibHandle dd 0
_ENDPAINT_Alias db "EndPaint",0
_ENDPAINT_Lib db "user32",0
_ENDPAINT_Call db 0
_TEXTOUT dd 0
_TEXTOUT_LibHandle dd 0
_TEXTOUT_Alias db "TextOutA",0
_TEXTOUT_Lib db "gdi32",0
_TEXTOUT_Call db 0
_DRAWTEXT dd 0
_DRAWTEXT_LibHandle dd 0
_DRAWTEXT_Alias db "DrawTextA",0
_DRAWTEXT_Lib db "user32",0
_DRAWTEXT_Call db 0
_ENUMCHILDWINDOWS dd 0
_ENUMCHILDWINDOWS_LibHandle dd 0
_ENUMCHILDWINDOWS_Alias db "EnumChildWindows",0
_ENUMCHILDWINDOWS_Lib db "user32",0
_ENUMCHILDWINDOWS_Call db 0
_GETTOPWINDOW dd 0
_GETTOPWINDOW_LibHandle dd 0
_GETTOPWINDOW_Alias db "GetTopWindow",0
_GETTOPWINDOW_Lib db "user32",0
_GETTOPWINDOW_Call db 0
_GETNEXTWINDOW dd 0
_GETNEXTWINDOW_LibHandle dd 0
_GETNEXTWINDOW_Alias db "GetWindow",0
_GETNEXTWINDOW_Lib db "user32",0
_GETNEXTWINDOW_Call db 0
_GETCLIENTRECT dd 0
_GETCLIENTRECT_LibHandle dd 0
_GETCLIENTRECT_Alias db "GetClientRect",0
_GETCLIENTRECT_Lib db "user32",0
_GETCLIENTRECT_Call db 0
_GETWINDOWRECT dd 0
_GETWINDOWRECT_LibHandle dd 0
_GETWINDOWRECT_Alias db "GetWindowRect",0
_GETWINDOWRECT_Lib db "user32",0
_GETWINDOWRECT_Call db 0
_REGISTERCLASS dd 0
_REGISTERCLASS_LibHandle dd 0
_REGISTERCLASS_Alias db "RegisterClassA",0
_REGISTERCLASS_Lib db "user32",0
_REGISTERCLASS_Call db 0
_REGISTERCLASSEX dd 0
_REGISTERCLASSEX_LibHandle dd 0
_REGISTERCLASSEX_Alias db "RegisterClassExA",0
_REGISTERCLASSEX_Lib db "USER32",0
_REGISTERCLASSEX_Call db 0
_UNREGISTERCLASS dd 0
_UNREGISTERCLASS_LibHandle dd 0
_UNREGISTERCLASS_Alias db "UnregisterClassA",0
_UNREGISTERCLASS_Lib db "user32",0
_UNREGISTERCLASS_Call db 0
_GETMESSAGE dd 0
_GETMESSAGE_LibHandle dd 0
_GETMESSAGE_Alias db "GetMessageA",0
_GETMESSAGE_Lib db "user32",0
_GETMESSAGE_Call db 0
_TRANSLATEMESSAGE dd 0
_TRANSLATEMESSAGE_LibHandle dd 0
_TRANSLATEMESSAGE_Alias db "TranslateMessage",0
_TRANSLATEMESSAGE_Lib db "user32",0
_TRANSLATEMESSAGE_Call db 0
_DISPATCHMESSAGE dd 0
_DISPATCHMESSAGE_LibHandle dd 0
_DISPATCHMESSAGE_Alias db "DispatchMessageA",0
_DISPATCHMESSAGE_Lib db "user32",0
_DISPATCHMESSAGE_Call db 0
_SENDMESSAGE dd 0
_SENDMESSAGE_LibHandle dd 0
_SENDMESSAGE_Alias db "SendMessageA",0
_SENDMESSAGE_Lib db "user32",0
_SENDMESSAGE_Call db 0
_LOADBITMAP dd 0
_LOADBITMAP_LibHandle dd 0
_LOADBITMAP_Alias db "LoadBitmapA",0
_LOADBITMAP_Lib db "user32",0
_LOADBITMAP_Call db 0
_LOADCURSOR dd 0
_LOADCURSOR_LibHandle dd 0
_LOADCURSOR_Alias db "LoadCursorA",0
_LOADCURSOR_Lib db "USER32",0
_LOADCURSOR_Call db 0
_LOADICON dd 0
_LOADICON_LibHandle dd 0
_LOADICON_Alias db "LoadIconA",0
_LOADICON_Lib db "user32",0
_LOADICON_Call db 0
_DESTROYICON dd 0
_DESTROYICON_LibHandle dd 0
_DESTROYICON_Alias db "DestroyIcon",0
_DESTROYICON_Lib db "user32",0
_DESTROYICON_Call db 0
_GETCURSOR dd 0
_GETCURSOR_LibHandle dd 0
_GETCURSOR_Alias db "GetCursor",0
_GETCURSOR_Lib db "user32",0
_GETCURSOR_Call db 0
_SETCURSOR dd 0
_SETCURSOR_LibHandle dd 0
_SETCURSOR_Alias db "SetCursor",0
_SETCURSOR_Lib db "user32",0
_SETCURSOR_Call db 0
_COPYICON dd 0
_COPYICON_LibHandle dd 0
_COPYICON_Alias db "CopyIcon",0
_COPYICON_Lib db "user32",0
_COPYICON_Call db 0
_SETSYSTEMCURSOR dd 0
_SETSYSTEMCURSOR_LibHandle dd 0
_SETSYSTEMCURSOR_Alias db "SetSystemCursor",0
_SETSYSTEMCURSOR_Lib db "user32",0
_SETSYSTEMCURSOR_Call db 0
_CREATECOMPATIBLEDC dd 0
_CREATECOMPATIBLEDC_LibHandle dd 0
_CREATECOMPATIBLEDC_Alias db "CreateCompatibleDC",0
_CREATECOMPATIBLEDC_Lib db "gdi32",0
_CREATECOMPATIBLEDC_Call db 0
_CREATEWINDOWEX dd 0
_CREATEWINDOWEX_LibHandle dd 0
_CREATEWINDOWEX_Alias db "CreateWindowExA",0
_CREATEWINDOWEX_Lib db "user32",0
_CREATEWINDOWEX_Call db 0
_UPDATEWINDOW dd 0
_UPDATEWINDOW_LibHandle dd 0
_UPDATEWINDOW_Alias db "UpdateWindow",0
_UPDATEWINDOW_Lib db "USER32",0
_UPDATEWINDOW_Call db 0
_SHOWWINDOW dd 0
_SHOWWINDOW_LibHandle dd 0
_SHOWWINDOW_Alias db "ShowWindow",0
_SHOWWINDOW_Lib db "USER32",0
_SHOWWINDOW_Call db 0
_DEFWINDOWPROC dd 0
_DEFWINDOWPROC_LibHandle dd 0
_DEFWINDOWPROC_Alias db "DefWindowProcA",0
_DEFWINDOWPROC_Lib db "user32",0
_DEFWINDOWPROC_Call db 0
_POSTQUITMESSAGE dd 0
_POSTQUITMESSAGE_LibHandle dd 0
_POSTQUITMESSAGE_Alias db "PostQuitMessage",0
_POSTQUITMESSAGE_Lib db "user32",0
_POSTQUITMESSAGE_Call db 0
_GETMODULEHANDLE dd 0
_GETMODULEHANDLE_LibHandle dd 0
_GETMODULEHANDLE_Alias db "GetModuleHandleA",0
_GETMODULEHANDLE_Lib db "kernel32",0
_GETMODULEHANDLE_Call db 0
_GETACTIVEWINDOW dd 0
_GETACTIVEWINDOW_LibHandle dd 0
_GETACTIVEWINDOW_Alias db "GetActiveWindow",0
_GETACTIVEWINDOW_Lib db "user32",0
_GETACTIVEWINDOW_Call db 0
_EXITPROCESS dd 0
_EXITPROCESS_LibHandle dd 0
_EXITPROCESS_Alias db "ExitProcess",0
_EXITPROCESS_Lib db "kernel32",0
_EXITPROCESS_Call db 0
_MSGBOX dd 0
_MSGBOX_LibHandle dd 0
_MSGBOX_Alias db "MessageBoxA",0
_MSGBOX_Lib db "user32",0
_MSGBOX_Call db 0
_MESSAGEBOX dd 0
_MESSAGEBOX_LibHandle dd 0
_MESSAGEBOX_Alias db "MessageBoxA",0
_MESSAGEBOX_Lib db "user32",0
_MESSAGEBOX_Call db 0
_GETLASTERROR dd 0
_GETLASTERROR_LibHandle dd 0
_GETLASTERROR_Alias db "GetLastError",0
_GETLASTERROR_Lib db "kernel32",0
_GETLASTERROR_Call db 0
_SETLASTERROR dd 0
_SETLASTERROR_LibHandle dd 0
_SETLASTERROR_Alias db "SetLastError",0
_SETLASTERROR_Lib db "kernel32",0
_SETLASTERROR_Call db 0
_FORMATMESSAGE dd 0
_FORMATMESSAGE_LibHandle dd 0
_FORMATMESSAGE_Alias db "FormatMessageA",0
_FORMATMESSAGE_Lib db "kernel32",0
_FORMATMESSAGE_Call db 0
_LOCALFREE dd 0
_LOCALFREE_LibHandle dd 0
_LOCALFREE_Alias db "LocalFree",0
_LOCALFREE_Lib db "kernel32",0
_LOCALFREE_Call db 0
_GETWINDOWLONG dd 0
_GETWINDOWLONG_LibHandle dd 0
_GETWINDOWLONG_Alias db "GetWindowIntegerA",0
_GETWINDOWLONG_Lib db "user32",0
_GETWINDOWLONG_Call db 0
_SETWINDOWLONG dd 0
_SETWINDOWLONG_LibHandle dd 0
_SETWINDOWLONG_Alias db "SetWindowIntegerA",0
_SETWINDOWLONG_Lib db "user32",0
_SETWINDOWLONG_Call db 0
_CALLWINDOWPROC dd 0
_CALLWINDOWPROC_LibHandle dd 0
_CALLWINDOWPROC_Alias db "CallWindowProcA",0
_CALLWINDOWPROC_Lib db "user32",0
_CALLWINDOWPROC_Call db 0
_GETCLASSLONG dd 0
_GETCLASSLONG_LibHandle dd 0
_GETCLASSLONG_Alias db "GetClassLongA",0
_GETCLASSLONG_Lib db "user32",0
_GETCLASSLONG_Call db 0
_SETCLASSLONG dd 0
_SETCLASSLONG_LibHandle dd 0
_SETCLASSLONG_Alias db "SetClassLongA",0
_SETCLASSLONG_Lib db "user32",0
_SETCLASSLONG_Call db 0
_GETDESKTOPWINDOW dd 0
_GETDESKTOPWINDOW_LibHandle dd 0
_GETDESKTOPWINDOW_Alias db "GetDesktopWindow",0
_GETDESKTOPWINDOW_Lib db "user32",0
_GETDESKTOPWINDOW_Call db 0
_GETPROCADDRESS dd 0
_GETPROCADDRESS_LibHandle dd 0
_GETPROCADDRESS_Alias db "GetProcAddress",0
_GETPROCADDRESS_Lib db "kernel32",0
_GETPROCADDRESS_Call db 0
_DIALOGBOXPARAM dd 0
_DIALOGBOXPARAM_LibHandle dd 0
_DIALOGBOXPARAM_Alias db "DialogBoxParamA",0
_DIALOGBOXPARAM_Lib db "user32",0
_DIALOGBOXPARAM_Call db 0
_ENDDIALOG dd 0
_ENDDIALOG_LibHandle dd 0
_ENDDIALOG_Alias db "EndDialog",0
_ENDDIALOG_Lib db "user32",0
_ENDDIALOG_Call db 0
_GETDLGITEM dd 0
_GETDLGITEM_LibHandle dd 0
_GETDLGITEM_Alias db "GetDlgItem",0
_GETDLGITEM_Lib db "user32",0
_GETDLGITEM_Call db 0
_INITCOMMONCONTROLS dd 0
_INITCOMMONCONTROLS_LibHandle dd 0
_INITCOMMONCONTROLS_Alias db "InitCommonControls",0
_INITCOMMONCONTROLS_Lib db "comctl32",0
_INITCOMMONCONTROLS_Call db 0
_INVALIDATERECT dd 0
_INVALIDATERECT_LibHandle dd 0
_INVALIDATERECT_Alias db "InvalidateRect",0
_INVALIDATERECT_Lib db "user32",0
_INVALIDATERECT_Call db 0
_CREATEPEN dd 0
_CREATEPEN_LibHandle dd 0
_CREATEPEN_Alias db "CreatePen",0
_CREATEPEN_Lib db "gdi32",0
_CREATEPEN_Call db 0
_SELECTOBJECT dd 0
_SELECTOBJECT_LibHandle dd 0
_SELECTOBJECT_Alias db "SelectObject",0
_SELECTOBJECT_Lib db "gdi32",0
_SELECTOBJECT_Call db 0
_GETSTOCKOBJECT dd 0
_GETSTOCKOBJECT_LibHandle dd 0
_GETSTOCKOBJECT_Alias db "GetStockObject",0
_GETSTOCKOBJECT_Lib db "gdi32",0
_GETSTOCKOBJECT_Call db 0
_CREATESOLIDBRUSH dd 0
_CREATESOLIDBRUSH_LibHandle dd 0
_CREATESOLIDBRUSH_Alias db "CreateSolidBrush",0
_CREATESOLIDBRUSH_Lib db "gdi32",0
_CREATESOLIDBRUSH_Call db 0
_RECTANGLE dd 0
_RECTANGLE_LibHandle dd 0
_RECTANGLE_Alias db "Rectangle",0
_RECTANGLE_Lib db "gdi32",0
_RECTANGLE_Call db 0
_DELETEOBJECT dd 0
_DELETEOBJECT_LibHandle dd 0
_DELETEOBJECT_Alias db "DeleteObject",0
_DELETEOBJECT_Lib db "gdi32",0
_DELETEOBJECT_Call db 0
_DELETEDC dd 0
_DELETEDC_LibHandle dd 0
_DELETEDC_Alias db "DeleteDC",0
_DELETEDC_Lib db "gdi32",0
_DELETEDC_Call db 0
_DESTROYWINDOW dd 0
_DESTROYWINDOW_LibHandle dd 0
_DESTROYWINDOW_Alias db "DestroyWindow",0
_DESTROYWINDOW_Lib db "user32",0
_DESTROYWINDOW_Call db 0
_SETPIXEL dd 0
_SETPIXEL_LibHandle dd 0
_SETPIXEL_Alias db "SetPixel",0
_SETPIXEL_Lib db "gdi32",0
_SETPIXEL_Call db 0
_BITBLT dd 0
_BITBLT_LibHandle dd 0
_BITBLT_Alias db "BitBlt",0
_BITBLT_Lib db "gdi32",0
_BITBLT_Call db 0
_CREATEMENU dd 0
_CREATEMENU_LibHandle dd 0
_CREATEMENU_Alias db "CreateMenu",0
_CREATEMENU_Lib db "user32",0
_CREATEMENU_Call db 0
_APPENDMENU dd 0
_APPENDMENU_LibHandle dd 0
_APPENDMENU_Alias db "AppendMenuA",0
_APPENDMENU_Lib db "user32",0
_APPENDMENU_Call db 0
_POSTMESSAGE dd 0
_POSTMESSAGE_LibHandle dd 0
_POSTMESSAGE_Alias db "PostMessageA",0
_POSTMESSAGE_Lib db "user32",0
_POSTMESSAGE_Call db 0
_GETSYSTEMMETRICS dd 0
_GETSYSTEMMETRICS_LibHandle dd 0
_GETSYSTEMMETRICS_Alias db "GetSystemMetrics",0
_GETSYSTEMMETRICS_Lib db "user32",0
_GETSYSTEMMETRICS_Call db 0
_SETWINDOWPOS dd 0
_SETWINDOWPOS_LibHandle dd 0
_SETWINDOWPOS_Alias db "SetWindowPos",0
_SETWINDOWPOS_Lib db "user32",0
_SETWINDOWPOS_Call db 0
_ENABLEWINDOW dd 0
_ENABLEWINDOW_LibHandle dd 0
_ENABLEWINDOW_Alias db "EnableWindow",0
_ENABLEWINDOW_Lib db "user32",0
_ENABLEWINDOW_Call db 0
_PLAYSOUND dd 0
_PLAYSOUND_LibHandle dd 0
_PLAYSOUND_Alias db "PlaySoundA",0
_PLAYSOUND_Lib db "winmm",0
_PLAYSOUND_Call db 0
STRUC Scope68__MMTIME_TYPE
.Scope68__WTYPE_Integer resd 1
.Scope68__U_Integer resd 1
ENDSTRUC
_TIMESETEVENT dd 0
_TIMESETEVENT_LibHandle dd 0
_TIMESETEVENT_Alias db "timeSetEvent",0
_TIMESETEVENT_Lib db "winmm.dll",0
_TIMESETEVENT_Call db 0
_TIMEKILLEVENT dd 0
_TIMEKILLEVENT_LibHandle dd 0
_TIMEKILLEVENT_Alias db "timeKillEvent",0
_TIMEKILLEVENT_Lib db "winmm.dll",0
_TIMEKILLEVENT_Call db 0
_CREATEFONT dd 0
_CREATEFONT_LibHandle dd 0
_CREATEFONT_Alias db "CreateFontA",0
_CREATEFONT_Lib db "gdi32",0
_CREATEFONT_Call db 0
Number_1 dq 0.0
String_2 db "Message:",0
Number_3 dq 0.0
Scope81__HMOD_Integer dd 0
Scope81__HICON_Integer dd 0
Scope81__Resource_BADTOTHEBONE_100 dd 100
Scope81__Resource_ICONRES_101 dd 101
Scope81__Resource_WAVERES_102 dd 102
Number_4 dq 272.0
Number_5 dq 128.0
Number_6 dq 0.0
Number_7 dq 0.0
Number_8 dq 273.0
Number_9 dq 0.0
Number_10 dq 16.0
Number_11 dq 101.0
Number_12 dq 262148.0
Number_13 dq 1.0
Number_14 dq -1.0
Number_15 dq 16.0
Number_16 dq 0.0
Number_17 dq 0.0
Number_18 dq 0.0
Number_19 dq 0.0
String_20 db "BADTOTHEBONE",0
Number_21 dq 0.0
Number_22 dq 0.0
ExitStatus dd 0

