$AppType Console
$Optimize ON

$Include "File.inc"

Dim File As Integer

File = FileOpen("Hello.txt", FileRead)
print "Reading from Hello.txt:"
print FileReadStr(File, len("Hello, testing...."))
print FileReadInt(File)
FileClose(File)
