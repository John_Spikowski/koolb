$AppType Console
$Optimize ON

$Include "File.inc"

Dim File As Integer
File = FileOpen("Hello.txt", FileCreateReadWrite)
FileWriteStr(File, "Hello, testing....")
FileWriteInt(File, 123)
FileClose(File)
Print "Wrote stuff to hello.txt!"
