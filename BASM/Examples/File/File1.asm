;Library functions to import to the Chameleon app
extern ExitProcess
extern MessageBoxA
extern lstrcat
extern lstrlen
extern lstrcpy
extern HeapCreate
extern HeapAlloc
extern HeapFree
extern FreeLibrary
extern floor
extern lstrlenA
extern lstrcpyA
extern lstrcatA
extern LoadLibraryA
extern GetProcAddress
extern lstrcmpA
extern GetModuleHandleA
extern GetCommandLineA
extern GetStdHandle
extern WriteFile
extern HeapDestroy
%define FOPEN_Used
%define FILEOPEN_Used
%define FPUTS_Used
%define FILEWRITESTR_Used
%define FWRITE2_Used
%define FILEWRITEINT_Used
%define FCLOSE_Used
%define FILECLOSE_Used




;Initialize everything to prep the app to run
section .text
%include "C:\BASM\Bin\MACROS.INC"
global START
START:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH ESI
PUSH EDI
stdcall HeapCreate,0,16384,0
MOV dword[HandleToHeap],EAX
CMP dword[HandleToHeap],0
JNE Label1
JMP NoMemory
Label1:
stdcall GetModuleHandleA,0
MOV dword[Internal_HInstance],EAX
CALL GetCommandLineA
MOV dword[Internal_CommandLine],EAX
stdcall GetStdHandle,-10
MOV dword[HandleToInput],EAX
stdcall GetStdHandle,-11
MOV dword[HandleToOutput],EAX
CMP dword[HandleToInput],-1
JNE Label390
JMP NoConsole
Label390:
CMP dword[HandleToOutput],-1
JNE Label391
JMP NoConsole
Label391:



;The main body where the app actually runs
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label392
JMP NoMemory
Label392:
MOV dword[ParameterPool],EAX
MOV EBX,String_100
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label393
JMP NoMemory
Label393:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_100
PUSH EBX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EBX,String_101
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label394
JMP NoMemory
Label394:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_101
PUSH EBX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _FILEOPEN
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope63__FILE_Integer]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label395
JMP NoMemory
Label395:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope63__FILE_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EBX,String_102
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label396
JMP NoMemory
Label396:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_102
PUSH EBX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _FILEWRITESTR
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label397
JMP NoMemory
Label397:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope63__FILE_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_103+4]
PUSH dword[Number_103]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _FILEWRITEINT
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label398
JMP NoMemory
Label398:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope63__FILE_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _FILECLOSE
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV EBX,String_104
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label399
JMP NoMemory
Label399:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_104
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0



;Prepare the app to exit and then terminate
Exit:
%ifdef ISALNUM_Used
stdcall FreeLibrary,dword[_ISALNUM_LibHandle]
%endif
%ifdef ISALPHA_Used
stdcall FreeLibrary,dword[_ISALPHA_LibHandle]
%endif
%ifdef ISCNTRL_Used
stdcall FreeLibrary,dword[_ISCNTRL_LibHandle]
%endif
%ifdef ISDIGIT_Used
stdcall FreeLibrary,dword[_ISDIGIT_LibHandle]
%endif
%ifdef ISGRAPH_Used
stdcall FreeLibrary,dword[_ISGRAPH_LibHandle]
%endif
%ifdef ISLOWER_Used
stdcall FreeLibrary,dword[_ISLOWER_LibHandle]
%endif
%ifdef ISPRINT_Used
stdcall FreeLibrary,dword[_ISPRINT_LibHandle]
%endif
%ifdef ISPUNCT_Used
stdcall FreeLibrary,dword[_ISPUNCT_LibHandle]
%endif
%ifdef ISSPACE_Used
stdcall FreeLibrary,dword[_ISSPACE_LibHandle]
%endif
%ifdef ISUPPER_Used
stdcall FreeLibrary,dword[_ISUPPER_LibHandle]
%endif
%ifdef ISXDIGIT_Used
stdcall FreeLibrary,dword[_ISXDIGIT_LibHandle]
%endif
%ifdef TOLOWER_Used
stdcall FreeLibrary,dword[_TOLOWER_LibHandle]
%endif
%ifdef TOUPPER_Used
stdcall FreeLibrary,dword[_TOUPPER_LibHandle]
%endif
%ifdef LEN_Used
stdcall FreeLibrary,dword[_LEN_LibHandle]
%endif
%ifdef LSTRCPY_Used
stdcall FreeLibrary,dword[_LSTRCPY_LibHandle]
%endif
%ifdef FOPEN_Used
stdcall FreeLibrary,dword[_FOPEN_LibHandle]
%endif
%ifdef FWRITE_Used
stdcall FreeLibrary,dword[_FWRITE_LibHandle]
%endif
%ifdef FWRITE2_Used
stdcall FreeLibrary,dword[_FWRITE2_LibHandle]
%endif
%ifdef FPUTS_Used
stdcall FreeLibrary,dword[_FPUTS_LibHandle]
%endif
%ifdef FREAD_Used
stdcall FreeLibrary,dword[_FREAD_LibHandle]
%endif
%ifdef FREAD2_Used
stdcall FreeLibrary,dword[_FREAD2_LibHandle]
%endif
%ifdef FGETS_Used
stdcall FreeLibrary,dword[_FGETS_LibHandle]
%endif
%ifdef FTELL_Used
stdcall FreeLibrary,dword[_FTELL_LibHandle]
%endif
%ifdef FSEEK_Used
stdcall FreeLibrary,dword[_FSEEK_LibHandle]
%endif
%ifdef FGETPOS_Used
stdcall FreeLibrary,dword[_FGETPOS_LibHandle]
%endif
%ifdef FSETPOS_Used
stdcall FreeLibrary,dword[_FSETPOS_LibHandle]
%endif
%ifdef FCLOSE_Used
stdcall FreeLibrary,dword[_FCLOSE_LibHandle]
%endif
stdcall HeapDestroy,dword[HandleToHeap]
POP EDI
POP ESI
POP EBX
MOV ESP,EBP
POP EBP
MOV EAX,dword[ExitStatus]
stdcall ExitProcess,dword[ExitStatus]
RET

NoMemory:
MOV dword[ExitStatus],1
stdcall MessageBoxA,0,NoMemMessage,Error,0
JMP Exit

NoLibrary:
MOV dword[ExitStatus],1
POP EBX
stdcall lstrlen,EBX
ADD EAX,30
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
MOV EDI,EAX
stdcall lstrcpy,EDI,NoLibFound
stdcall lstrcat,EDI,EBX
stdcall MessageBoxA,0,EDI,Error,0
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
JMP Exit

NoFunction:
MOV dword[ExitStatus],1
POP EBX
stdcall lstrlen,EBX
ADD EAX,30
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
MOV EDI,EAX
stdcall lstrcpy,EDI,NoFunctionFound
stdcall lstrcat,EDI,EBX
stdcall MessageBoxA,0,EDI,Error,0
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
JMP Exit














































%ifdef SPACE____Used

global _SPACE___
_SPACE___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label2
JMP NoMemory
Label2:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4

PUSH dword[Number_1+4]
PUSH dword[Number_1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
MOV EBX,String_2
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label3
JMP NoMemory
Label3:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_2
PUSH EBX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
Label4:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JBE Label5
FLDZ
Label5:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label7
JMP Label6
Label7:
stdcall lstrlenA,dword[EBP-4]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label8
JMP NoMemory
Label8:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-4]
PUSH EAX
MOV EBX,String_3
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label9
JMP NoMemory
Label9:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_3
PUSH EBX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label10
JMP NoMemory
Label10:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_4+4]
PUSH dword[Number_4]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
JMP Label4
Label6:


MOV EAX,dword[EBP-4]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef MID____Used

global _MID___
_MID___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label11
JMP NoMemory
Label11:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_5+4]
PUSH dword[Number_5]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JB Label12
FLDZ
Label12:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label14
JMP Label13
Label14:
PUSH dword[Number_6+4]
PUSH dword[Number_6]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP+12]
JMP Label13
Label13:
Label15:
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP+16]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label16
JMP NoMemory
Label16:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label17
JMP NoMemory
Label17:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label18
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label19
PUSH _LEN_Lib
JMP NoLibrary
Label19:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label20
PUSH _LEN_Alias
JMP NoFunction
Label20:
Label18:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JA Label21
FLDZ
Label21:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label23
JMP Label22
Label23:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label25
JMP NoMemory
Label25:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label26
JMP NoMemory
Label26:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label27
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label28
PUSH _LEN_Lib
JMP NoLibrary
Label28:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label29
PUSH _LEN_Alias
JMP NoFunction
Label29:
Label27:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_7+4]
PUSH dword[Number_7]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP+16]
JMP Label22
Label22:
Label24:
extern lstrlenA
extern lstrcpyA
extern lstrcpynA
MOV EBX,dword[EBP+8]   ;EBX = S
MOV ESI,dword[EBP+12]  ;ESI = Start
MOV EDI,dword[EBP+16]  ;EDI = Length
DEC ESI                ;Convert to zero-based 
INC EDI
ADD EBX,ESI            ;Move Ptr to Start
ADD ESI,EDI
INC ESI
stdcall HeapAlloc,dword[HandleToHeap],8,ESI
DEC ESI
stdcall lstrcpynA,EAX,EBX,EDI
MOV dword[EBP-4],EAX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 12

%endif

%ifdef ASC_Used

global _ASC
_ASC:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

MOV EAX,dword[EBP+8]
MOVSX EAX,byte[EAX]
MOV dword[EBP-4],EAX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef CHR____Used

global _CHR___
_CHR___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label30
JMP NoMemory
Label30:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

stdcall HeapAlloc,dword[HandleToHeap],8,2
MOV EBX,EAX
MOV AH,byte[EBP+8]
MOV byte[EBX],AH
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef DELETE____Used

global _DELETE___
_DELETE___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label31
JMP NoMemory
Label31:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label32
JMP NoMemory
Label32:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label33
JMP NoMemory
Label33:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_8+4]
PUSH dword[Number_8]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_9+4]
PUSH dword[Number_9]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label34
JMP NoMemory
Label34:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label35
JMP NoMemory
Label35:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP+16]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label36
JMP NoMemory
Label36:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label37
JMP NoMemory
Label37:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label38
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label39
PUSH _LEN_Lib
JMP NoLibrary
Label39:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label40
PUSH _LEN_Alias
JMP NoFunction
Label40:
Label38:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label41
JMP NoMemory
Label41:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 12

%endif

%ifdef INSERT____Used

global _INSERT___
_INSERT___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label42
JMP NoMemory
Label42:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label43
JMP NoMemory
Label43:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label44
JMP NoMemory
Label44:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_10+4]
PUSH dword[Number_10]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP+16]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_11+4]
PUSH dword[Number_11]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label45
JMP NoMemory
Label45:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label46
JMP NoMemory
Label46:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label47
JMP NoMemory
Label47:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label48
JMP NoMemory
Label48:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+16]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label49
JMP NoMemory
Label49:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label50
JMP NoMemory
Label50:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label51
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label52
PUSH _LEN_Lib
JMP NoLibrary
Label52:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label53
PUSH _LEN_Alias
JMP NoFunction
Label53:
Label51:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label54
JMP NoMemory
Label54:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 12

%endif

%ifdef LCASE____Used

global _LCASE___
_LCASE___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label55
JMP NoMemory
Label55:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4
SUB ESP,4

MOV EBX,String_12
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label56
JMP NoMemory
Label56:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_12
PUSH EBX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
PUSH dword[Number_13+4]
PUSH dword[Number_13]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label57
JMP NoMemory
Label57:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label58
JMP NoMemory
Label58:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label59
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label60
PUSH _LEN_Lib
JMP NoLibrary
Label60:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label61
PUSH _LEN_Alias
JMP NoFunction
Label61:
Label59:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_14+4]
PUSH dword[Number_14]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
Label62:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JB Label63
FLDZ
Label63:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label65
JMP Label64
Label65:
stdcall lstrlenA,dword[EBP-4]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label66
JMP NoMemory
Label66:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-4]
PUSH EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label67
JMP NoMemory
Label67:
MOV dword[ParameterPool],EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label68
JMP NoMemory
Label68:
MOV dword[ParameterPool],EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label69
JMP NoMemory
Label69:
MOV dword[ParameterPool],EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label70
JMP NoMemory
Label70:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label71
JMP NoMemory
Label71:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_15+4]
PUSH dword[Number_15]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _ASC
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_TOLOWER],0
JNE Label72
stdcall LoadLibraryA,_TOLOWER_Lib
MOV dword[_TOLOWER_LibHandle],EAX
CMP EAX,0
JNE Label73
PUSH _TOLOWER_Lib
JMP NoLibrary
Label73:
stdcall GetProcAddress,dword[_TOLOWER_LibHandle],_TOLOWER_Alias
MOV dword[_TOLOWER],EAX
CMP EAX,0
JNE Label74
PUSH _TOLOWER_Alias
JMP NoFunction
Label74:
Label72:
CALL dword[_TOLOWER]
ADD ESP,4
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label75
JMP NoMemory
Label75:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_16+4]
PUSH dword[Number_16]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
JMP Label62
Label64:


MOV EAX,dword[EBP-4]
ADD ESP,12
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef UCASE____Used

global _UCASE___
_UCASE___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label76
JMP NoMemory
Label76:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4
SUB ESP,4

MOV EBX,String_17
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label77
JMP NoMemory
Label77:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_17
PUSH EBX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
PUSH dword[Number_18+4]
PUSH dword[Number_18]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label78
JMP NoMemory
Label78:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label79
JMP NoMemory
Label79:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label80
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label81
PUSH _LEN_Lib
JMP NoLibrary
Label81:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label82
PUSH _LEN_Alias
JMP NoFunction
Label82:
Label80:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_19+4]
PUSH dword[Number_19]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
Label83:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JB Label84
FLDZ
Label84:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label86
JMP Label85
Label86:
stdcall lstrlenA,dword[EBP-4]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label87
JMP NoMemory
Label87:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-4]
PUSH EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label88
JMP NoMemory
Label88:
MOV dword[ParameterPool],EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label89
JMP NoMemory
Label89:
MOV dword[ParameterPool],EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label90
JMP NoMemory
Label90:
MOV dword[ParameterPool],EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label91
JMP NoMemory
Label91:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label92
JMP NoMemory
Label92:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_20+4]
PUSH dword[Number_20]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _ASC
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_TOUPPER],0
JNE Label93
stdcall LoadLibraryA,_TOUPPER_Lib
MOV dword[_TOUPPER_LibHandle],EAX
CMP EAX,0
JNE Label94
PUSH _TOUPPER_Lib
JMP NoLibrary
Label94:
stdcall GetProcAddress,dword[_TOUPPER_LibHandle],_TOUPPER_Alias
MOV dword[_TOUPPER],EAX
CMP EAX,0
JNE Label95
PUSH _TOUPPER_Alias
JMP NoFunction
Label95:
Label93:
CALL dword[_TOUPPER]
ADD ESP,4
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label96
JMP NoMemory
Label96:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_21+4]
PUSH dword[Number_21]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
JMP Label83
Label85:


MOV EAX,dword[EBP-4]
ADD ESP,12
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef INSTR_Used

global _INSTR
_INSTR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
SUB ESP,4
SUB ESP,4
SUB ESP,4

FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_22+4]
PUSH dword[Number_22]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JB Label97
FLDZ
Label97:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label99
JMP Label98
Label99:
PUSH dword[Number_23+4]
PUSH dword[Number_23]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP+8]
JMP Label98
Label98:
Label100:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label101
JMP NoMemory
Label101:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+16]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label102
JMP NoMemory
Label102:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+16]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label103
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label104
PUSH _LEN_Lib
JMP NoLibrary
Label104:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label105
PUSH _LEN_Alias
JMP NoFunction
Label105:
Label103:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
PUSH dword[Number_24+4]
PUSH dword[Number_24]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label106
JMP NoMemory
Label106:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label107
JMP NoMemory
Label107:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label108
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label109
PUSH _LEN_Lib
JMP NoLibrary
Label109:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label110
PUSH _LEN_Alias
JMP NoFunction
Label110:
Label108:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-16]
Label111:
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-16]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JBE Label112
FLDZ
Label112:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_25+4]
PUSH dword[Number_25]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label113
FLDZ
Label113:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
POP dword[TempQWord1]
POP dword[TempQWord1+4]
JNE Label116
JMP Label115
Label116:
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label114
JMP Label115
Label114:
FINIT
FILD dword[True]
JMP Label117
Label115:
FLDZ
Label117:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label119
JMP Label118
Label119:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label120
JMP NoMemory
Label120:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label121
JMP NoMemory
Label121:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
stdcall lstrlenA,dword[EBP+16]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label122
JMP NoMemory
Label122:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+16]
PUSH EAX
POP EBX
POP EDI
stdcall lstrcmpA,EDI,EBX
MOV ESI,EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,EDI
FINIT
FILD dword[True]
CMP ESI,0
JE Label123
FLDZ
Label123:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label125
JMP Label124
Label125:
PUSH dword[Number_26+4]
PUSH dword[Number_26]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label124
Label124:
Label126:
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_27+4]
PUSH dword[Number_27]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP+8]
JMP Label111
Label118:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_28+4]
PUSH dword[Number_28]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label127
FLDZ
Label127:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label129
JMP Label128
Label129:
PUSH dword[Number_29+4]
PUSH dword[Number_29]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label128
Label128:
Label130:


MOV EAX,dword[EBP-4]
ADD ESP,16
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 12

%endif

%ifdef LEFT____Used

global _LEFT___
_LEFT___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label131
JMP NoMemory
Label131:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label132
JMP NoMemory
Label132:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label133
JMP NoMemory
Label133:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_30+4]
PUSH dword[Number_30]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef LTRIM____Used

global _LTRIM___
_LTRIM___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label134
JMP NoMemory
Label134:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4
SUB ESP,4
SUB ESP,4

PUSH dword[Number_31+4]
PUSH dword[Number_31]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label135
JMP NoMemory
Label135:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label136
JMP NoMemory
Label136:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label137
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label138
PUSH _LEN_Lib
JMP NoLibrary
Label138:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label139
PUSH _LEN_Alias
JMP NoFunction
Label139:
Label137:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[Number_32+4]
PUSH dword[Number_32]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-16]
Label140:
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JB Label141
FLDZ
Label141:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-16]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_33+4]
PUSH dword[Number_33]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label142
FLDZ
Label142:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
POP dword[TempQWord1]
POP dword[TempQWord1+4]
JNE Label145
JMP Label144
Label145:
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label143
JMP Label144
Label143:
FINIT
FILD dword[True]
JMP Label146
Label144:
FLDZ
Label146:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label148
JMP Label147
Label148:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label149
JMP NoMemory
Label149:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label150
JMP NoMemory
Label150:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_34+4]
PUSH dword[Number_34]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
MOV EBX,String_35
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label151
JMP NoMemory
Label151:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_35
PUSH EBX
POP EBX
POP EDI
stdcall lstrcmpA,EDI,EBX
MOV ESI,EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,EDI
FINIT
FILD dword[True]
CMP ESI,0
JNE Label152
FLDZ
Label152:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label154
JMP Label153
Label154:
PUSH dword[Number_36+4]
PUSH dword[Number_36]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-16]
JMP Label153
Label153:
Label155:
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_37+4]
PUSH dword[Number_37]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
JMP Label140
Label147:
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_38+4]
PUSH dword[Number_38]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label156
JMP NoMemory
Label156:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label157
JMP NoMemory
Label157:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label158
JMP NoMemory
Label158:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label159
JMP NoMemory
Label159:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label160
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label161
PUSH _LEN_Lib
JMP NoLibrary
Label161:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label162
PUSH _LEN_Alias
JMP NoFunction
Label162:
Label160:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_39+4]
PUSH dword[Number_39]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,16
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef REVERSE____Used

global _REVERSE___
_REVERSE___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label163
JMP NoMemory
Label163:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4
SUB ESP,4

PUSH dword[Number_40+4]
PUSH dword[Number_40]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label164
JMP NoMemory
Label164:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label165
JMP NoMemory
Label165:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label166
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label167
PUSH _LEN_Lib
JMP NoLibrary
Label167:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label168
PUSH _LEN_Alias
JMP NoFunction
Label168:
Label166:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
Label169:
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JBE Label170
FLDZ
Label170:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label172
JMP Label171
Label172:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label173
JMP NoMemory
Label173:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label174
JMP NoMemory
Label174:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_41+4]
PUSH dword[Number_41]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
stdcall lstrlenA,dword[EBP-4]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label175
JMP NoMemory
Label175:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-4]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label176
JMP NoMemory
Label176:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_42+4]
PUSH dword[Number_42]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
JMP Label169
Label171:


MOV EAX,dword[EBP-4]
ADD ESP,12
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef RIGHT____Used

global _RIGHT___
_RIGHT___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label177
JMP NoMemory
Label177:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label178
JMP NoMemory
Label178:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label179
JMP NoMemory
Label179:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label180
JMP NoMemory
Label180:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label181
JMP NoMemory
Label181:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label182
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label183
PUSH _LEN_Lib
JMP NoLibrary
Label183:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label184
PUSH _LEN_Alias
JMP NoFunction
Label184:
Label182:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef INSTRREV_Used

global _INSTRREV
_INSTRREV:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
SUB ESP,4
SUB ESP,4
SUB ESP,4

FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_43+4]
PUSH dword[Number_43]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label185
FLDZ
Label185:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label187
JMP Label186
Label187:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label189
JMP NoMemory
Label189:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label190
JMP NoMemory
Label190:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label191
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label192
PUSH _LEN_Lib
JMP NoLibrary
Label192:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label193
PUSH _LEN_Alias
JMP NoFunction
Label193:
Label191:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label194
JMP NoMemory
Label194:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+16]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label195
JMP NoMemory
Label195:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+16]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label196
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label197
PUSH _LEN_Lib
JMP NoLibrary
Label197:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label198
PUSH _LEN_Alias
JMP NoFunction
Label198:
Label196:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP+8]
JMP Label186
Label186:
Label188:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label199
JMP NoMemory
Label199:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+16]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label200
JMP NoMemory
Label200:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+16]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label201
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label202
PUSH _LEN_Lib
JMP NoLibrary
Label202:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label203
PUSH _LEN_Alias
JMP NoFunction
Label203:
Label201:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
PUSH dword[Number_44+4]
PUSH dword[Number_44]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
Label204:
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_45+4]
PUSH dword[Number_45]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JAE Label205
FLDZ
Label205:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_46+4]
PUSH dword[Number_46]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label206
FLDZ
Label206:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
POP dword[TempQWord1]
POP dword[TempQWord1+4]
JNE Label209
JMP Label208
Label209:
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label207
JMP Label208
Label207:
FINIT
FILD dword[True]
JMP Label210
Label208:
FLDZ
Label210:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label212
JMP Label211
Label212:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label213
JMP NoMemory
Label213:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label214
JMP NoMemory
Label214:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
stdcall lstrlenA,dword[EBP+16]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label215
JMP NoMemory
Label215:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+16]
PUSH EAX
POP EBX
POP EDI
stdcall lstrcmpA,EDI,EBX
MOV ESI,EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,EDI
FINIT
FILD dword[True]
CMP ESI,0
JE Label216
FLDZ
Label216:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label218
JMP Label217
Label218:
PUSH dword[Number_47+4]
PUSH dword[Number_47]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label217
Label217:
Label219:
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_48+4]
PUSH dword[Number_48]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP+8]
JMP Label204
Label211:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_49+4]
PUSH dword[Number_49]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label220
FLDZ
Label220:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label222
JMP Label221
Label222:
PUSH dword[Number_50+4]
PUSH dword[Number_50]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label221
Label221:
Label223:


MOV EAX,dword[EBP-4]
ADD ESP,16
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 12

%endif

%ifdef RTRIM____Used

global _RTRIM___
_RTRIM___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label224
JMP NoMemory
Label224:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4
SUB ESP,4

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label225
JMP NoMemory
Label225:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label226
JMP NoMemory
Label226:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label227
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label228
PUSH _LEN_Lib
JMP NoLibrary
Label228:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label229
PUSH _LEN_Alias
JMP NoFunction
Label229:
Label227:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[Number_51+4]
PUSH dword[Number_51]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
Label230:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_52+4]
PUSH dword[Number_52]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JAE Label231
FLDZ
Label231:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_53+4]
PUSH dword[Number_53]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label232
FLDZ
Label232:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
POP dword[TempQWord1]
POP dword[TempQWord1+4]
JNE Label235
JMP Label234
Label235:
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label233
JMP Label234
Label233:
FINIT
FILD dword[True]
JMP Label236
Label234:
FLDZ
Label236:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label238
JMP Label237
Label238:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label239
JMP NoMemory
Label239:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label240
JMP NoMemory
Label240:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_54+4]
PUSH dword[Number_54]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
MOV EBX,String_55
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label241
JMP NoMemory
Label241:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_55
PUSH EBX
POP EBX
POP EDI
stdcall lstrcmpA,EDI,EBX
MOV ESI,EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,EDI
FINIT
FILD dword[True]
CMP ESI,0
JNE Label242
FLDZ
Label242:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label244
JMP Label243
Label244:
PUSH dword[Number_56+4]
PUSH dword[Number_56]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
JMP Label243
Label243:
Label245:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_57+4]
PUSH dword[Number_57]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
JMP Label230
Label237:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_58+4]
PUSH dword[Number_58]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label246
JMP NoMemory
Label246:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label247
JMP NoMemory
Label247:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_59+4]
PUSH dword[Number_59]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,12
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef STRING____Used

global _STRING___
_STRING___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label248
JMP NoMemory
Label248:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4

PUSH dword[Number_60+4]
PUSH dword[Number_60]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
Label249:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JBE Label250
FLDZ
Label250:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label252
JMP Label251
Label252:
stdcall lstrlenA,dword[EBP-4]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label253
JMP NoMemory
Label253:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-4]
PUSH EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label254
JMP NoMemory
Label254:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label255
JMP NoMemory
Label255:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_61+4]
PUSH dword[Number_61]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_62+4]
PUSH dword[Number_62]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label256
JMP NoMemory
Label256:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_63+4]
PUSH dword[Number_63]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
JMP Label249
Label251:


MOV EAX,dword[EBP-4]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef TALLY_Used

global _TALLY
_TALLY:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
SUB ESP,4
SUB ESP,4

PUSH dword[Number_64+4]
PUSH dword[Number_64]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[Number_65+4]
PUSH dword[Number_65]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
Label257:
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_66+4]
PUSH dword[Number_66]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label258
FLDZ
Label258:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label260
JMP Label259
Label260:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label261
JMP NoMemory
Label261:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label262
JMP NoMemory
Label262:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label263
JMP NoMemory
Label263:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _INSTR
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_67+4]
PUSH dword[Number_67]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label264
FLDZ
Label264:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label266
JMP Label265
Label266:
PUSH dword[Number_68+4]
PUSH dword[Number_68]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
JMP Label265
Label265:
Label267:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_69+4]
PUSH dword[Number_69]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
FINIT
FILD dword[EBP-4]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_70+4]
PUSH dword[Number_70]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label257
Label259:
FINIT
FILD dword[EBP-4]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]


MOV EAX,dword[EBP-4]
ADD ESP,12
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef CRLF_Used

global _CRLF
_CRLF:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label268
JMP NoMemory
Label268:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label269
JMP NoMemory
Label269:
MOV dword[ParameterPool],EAX
PUSH dword[Number_71+4]
PUSH dword[Number_71]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label270
JMP NoMemory
Label270:
MOV dword[ParameterPool],EAX
PUSH dword[Number_72+4]
PUSH dword[Number_72]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label271
JMP NoMemory
Label271:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0

%endif

%ifdef VBCRLF_Used

global _VBCRLF
_VBCRLF:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label272
JMP NoMemory
Label272:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label273
JMP NoMemory
Label273:
MOV dword[ParameterPool],EAX
PUSH dword[Number_73+4]
PUSH dword[Number_73]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0

%endif

%ifdef VBCR_Used

global _VBCR
_VBCR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label274
JMP NoMemory
Label274:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label275
JMP NoMemory
Label275:
MOV dword[ParameterPool],EAX
PUSH dword[Number_74+4]
PUSH dword[Number_74]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0

%endif

%ifdef VBLF_Used

global _VBLF
_VBLF:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label276
JMP NoMemory
Label276:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label277
JMP NoMemory
Label277:
MOV dword[ParameterPool],EAX
PUSH dword[Number_75+4]
PUSH dword[Number_75]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0

%endif

%ifdef VBTAB_Used

global _VBTAB
_VBTAB:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label278
JMP NoMemory
Label278:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label279
JMP NoMemory
Label279:
MOV dword[ParameterPool],EAX
PUSH dword[Number_76+4]
PUSH dword[Number_76]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0

%endif

%ifdef REPLACE_Used

global _REPLACE
_REPLACE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label280
JMP NoMemory
Label280:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label281
JMP NoMemory
Label281:
MOV dword[EBP-12],EAX
MOV byte[EAX],0

stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label282
JMP NoMemory
Label282:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-12]
MOV dword[EBP-12],EBX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label283
JMP NoMemory
Label283:
MOV dword[ParameterPool],EAX
PUSH dword[Number_77+4]
PUSH dword[Number_77]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
stdcall lstrlenA,dword[EBP-12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label284
JMP NoMemory
Label284:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label285
JMP NoMemory
Label285:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _INSTR
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
Label286:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_78+4]
PUSH dword[Number_78]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JA Label287
FLDZ
Label287:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label289
JMP Label288
Label289:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label290
JMP NoMemory
Label290:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP-12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label291
JMP NoMemory
Label291:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_79+4]
PUSH dword[Number_79]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _LEFT___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
stdcall lstrlenA,dword[EBP+16]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label292
JMP NoMemory
Label292:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+16]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label293
JMP NoMemory
Label293:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label294
JMP NoMemory
Label294:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP-12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label295
JMP NoMemory
Label295:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label296
JMP NoMemory
Label296:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label297
JMP NoMemory
Label297:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label298
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label299
PUSH _LEN_Lib
JMP NoLibrary
Label299:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label300
PUSH _LEN_Alias
JMP NoFunction
Label300:
Label298:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _RIGHT___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label301
JMP NoMemory
Label301:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-12]
MOV dword[EBP-12],EBX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label302
JMP NoMemory
Label302:
MOV dword[ParameterPool],EAX
PUSH dword[Number_80+4]
PUSH dword[Number_80]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
stdcall lstrlenA,dword[EBP-12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label303
JMP NoMemory
Label303:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label304
JMP NoMemory
Label304:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _INSTR
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
JMP Label286
Label288:
stdcall lstrlenA,dword[EBP-12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label305
JMP NoMemory
Label305:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-12]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,12
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 12

%endif





































%ifdef FILEOPEN_Used

global _FILEOPEN
_FILEOPEN:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label306
JMP NoMemory
Label306:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label307
JMP NoMemory
Label307:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label308
JMP NoMemory
Label308:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EBX,String_81
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label309
JMP NoMemory
Label309:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_81
PUSH EBX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label310
JMP NoMemory
Label310:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FOPEN],0
JNE Label311
stdcall LoadLibraryA,_FOPEN_Lib
MOV dword[_FOPEN_LibHandle],EAX
CMP EAX,0
JNE Label312
PUSH _FOPEN_Lib
JMP NoLibrary
Label312:
stdcall GetProcAddress,dword[_FOPEN_LibHandle],_FOPEN_Alias
MOV dword[_FOPEN],EAX
CMP EAX,0
JNE Label313
PUSH _FOPEN_Alias
JMP NoFunction
Label313:
Label311:
CALL dword[_FOPEN]
ADD ESP,8
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef FILEWRITESTR_Used

global _FILEWRITESTR
_FILEWRITESTR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label314
JMP NoMemory
Label314:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label315
JMP NoMemory
Label315:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FPUTS],0
JNE Label316
stdcall LoadLibraryA,_FPUTS_Lib
MOV dword[_FPUTS_LibHandle],EAX
CMP EAX,0
JNE Label317
PUSH _FPUTS_Lib
JMP NoLibrary
Label317:
stdcall GetProcAddress,dword[_FPUTS_LibHandle],_FPUTS_Alias
MOV dword[_FPUTS],EAX
CMP EAX,0
JNE Label318
PUSH _FPUTS_Alias
JMP NoFunction
Label318:
Label316:
CALL dword[_FPUTS]
ADD ESP,8
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef FILEREADSTR_Used

global _FILEREADSTR
_FILEREADSTR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label319
JMP NoMemory
Label319:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label320
JMP NoMemory
Label320:
MOV dword[EBP-8],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label321
JMP NoMemory
Label321:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_82+4]
PUSH dword[Number_82]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _SPACE___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-8]
MOV dword[EBP-8],EBX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label322
JMP NoMemory
Label322:
MOV dword[ParameterPool],EAX
PUSH dword[EBP-8]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_83+4]
PUSH dword[Number_83]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FREAD],0
JNE Label323
stdcall LoadLibraryA,_FREAD_Lib
MOV dword[_FREAD_LibHandle],EAX
CMP EAX,0
JNE Label324
PUSH _FREAD_Lib
JMP NoLibrary
Label324:
stdcall GetProcAddress,dword[_FREAD_LibHandle],_FREAD_Alias
MOV dword[_FREAD],EAX
CMP EAX,0
JNE Label325
PUSH _FREAD_Alias
JMP NoFunction
Label325:
Label323:
CALL dword[_FREAD]
ADD ESP,16
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
stdcall lstrlenA,dword[EBP-8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label326
JMP NoMemory
Label326:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-8]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef FILEWRITELINE_Used

global _FILEWRITELINE
_FILEWRITELINE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label327
JMP NoMemory
Label327:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label328
JMP NoMemory
Label328:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label329
JMP NoMemory
Label329:
MOV dword[ParameterPool],EAX
PUSH dword[Number_84+4]
PUSH dword[Number_84]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label330
JMP NoMemory
Label330:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label331
JMP NoMemory
Label331:
MOV dword[ParameterPool],EAX
PUSH dword[Number_85+4]
PUSH dword[Number_85]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label332
JMP NoMemory
Label332:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FPUTS],0
JNE Label333
stdcall LoadLibraryA,_FPUTS_Lib
MOV dword[_FPUTS_LibHandle],EAX
CMP EAX,0
JNE Label334
PUSH _FPUTS_Lib
JMP NoLibrary
Label334:
stdcall GetProcAddress,dword[_FPUTS_LibHandle],_FPUTS_Alias
MOV dword[_FPUTS],EAX
CMP EAX,0
JNE Label335
PUSH _FPUTS_Alias
JMP NoFunction
Label335:
Label333:
CALL dword[_FPUTS]
ADD ESP,8
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef FILEGETSIZE_Used

global _FILEGETSIZE
_FILEGETSIZE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
SUB ESP,4

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label336
JMP NoMemory
Label336:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FTELL],0
JNE Label337
stdcall LoadLibraryA,_FTELL_Lib
MOV dword[_FTELL_LibHandle],EAX
CMP EAX,0
JNE Label338
PUSH _FTELL_Lib
JMP NoLibrary
Label338:
stdcall GetProcAddress,dword[_FTELL_LibHandle],_FTELL_Alias
MOV dword[_FTELL],EAX
CMP EAX,0
JNE Label339
PUSH _FTELL_Alias
JMP NoFunction
Label339:
Label337:
CALL dword[_FTELL]
ADD ESP,4
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label340
JMP NoMemory
Label340:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_86+4]
PUSH dword[Number_86]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_87+4]
PUSH dword[Number_87]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FSEEK],0
JNE Label341
stdcall LoadLibraryA,_FSEEK_Lib
MOV dword[_FSEEK_LibHandle],EAX
CMP EAX,0
JNE Label342
PUSH _FSEEK_Lib
JMP NoLibrary
Label342:
stdcall GetProcAddress,dword[_FSEEK_LibHandle],_FSEEK_Alias
MOV dword[_FSEEK],EAX
CMP EAX,0
JNE Label343
PUSH _FSEEK_Alias
JMP NoFunction
Label343:
Label341:
CALL dword[_FSEEK]
ADD ESP,12
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label344
JMP NoMemory
Label344:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FTELL],0
JNE Label345
stdcall LoadLibraryA,_FTELL_Lib
MOV dword[_FTELL_LibHandle],EAX
CMP EAX,0
JNE Label346
PUSH _FTELL_Lib
JMP NoLibrary
Label346:
stdcall GetProcAddress,dword[_FTELL_LibHandle],_FTELL_Alias
MOV dword[_FTELL],EAX
CMP EAX,0
JNE Label347
PUSH _FTELL_Alias
JMP NoFunction
Label347:
Label345:
CALL dword[_FTELL]
ADD ESP,4
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label348
JMP NoMemory
Label348:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_88+4]
PUSH dword[Number_88]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FSEEK],0
JNE Label349
stdcall LoadLibraryA,_FSEEK_Lib
MOV dword[_FSEEK_LibHandle],EAX
CMP EAX,0
JNE Label350
PUSH _FSEEK_Lib
JMP NoLibrary
Label350:
stdcall GetProcAddress,dword[_FSEEK_LibHandle],_FSEEK_Alias
MOV dword[_FSEEK],EAX
CMP EAX,0
JNE Label351
PUSH _FSEEK_Alias
JMP NoFunction
Label351:
Label349:
CALL dword[_FSEEK]
ADD ESP,12
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


MOV EAX,dword[EBP-4]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef FILEREADLINE_Used

global _FILEREADLINE
_FILEREADLINE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label352
JMP NoMemory
Label352:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label353
JMP NoMemory
Label353:
MOV dword[EBP-8],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label354
JMP NoMemory
Label354:
MOV dword[ParameterPool],EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label355
JMP NoMemory
Label355:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _FILEGETSIZE
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_89+4]
PUSH dword[Number_89]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _SPACE___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-8]
MOV dword[EBP-8],EBX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label356
JMP NoMemory
Label356:
MOV dword[ParameterPool],EAX
PUSH dword[EBP-8]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label357
JMP NoMemory
Label357:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _FILEGETSIZE
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_90+4]
PUSH dword[Number_90]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FGETS],0
JNE Label358
stdcall LoadLibraryA,_FGETS_Lib
MOV dword[_FGETS_LibHandle],EAX
CMP EAX,0
JNE Label359
PUSH _FGETS_Lib
JMP NoLibrary
Label359:
stdcall GetProcAddress,dword[_FGETS_LibHandle],_FGETS_Alias
MOV dword[_FGETS],EAX
CMP EAX,0
JNE Label360
PUSH _FGETS_Alias
JMP NoFunction
Label360:
Label358:
CALL dword[_FGETS]
ADD ESP,12
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
stdcall lstrlenA,dword[EBP-8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label361
JMP NoMemory
Label361:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-8]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef FILEWRITEINT_Used

global _FILEWRITEINT
_FILEWRITEINT:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label362
JMP NoMemory
Label362:
MOV dword[ParameterPool],EAX
MOV EAX,EBP
ADD EAX,12
PUSH EAX
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_91+4]
PUSH dword[Number_91]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_92+4]
PUSH dword[Number_92]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FWRITE2],0
JNE Label363
stdcall LoadLibraryA,_FWRITE2_Lib
MOV dword[_FWRITE2_LibHandle],EAX
CMP EAX,0
JNE Label364
PUSH _FWRITE2_Lib
JMP NoLibrary
Label364:
stdcall GetProcAddress,dword[_FWRITE2_LibHandle],_FWRITE2_Alias
MOV dword[_FWRITE2],EAX
CMP EAX,0
JNE Label365
PUSH _FWRITE2_Alias
JMP NoFunction
Label365:
Label363:
CALL dword[_FWRITE2]
ADD ESP,16
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef FILEREADINT_Used

global _FILEREADINT
_FILEREADINT:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label366
JMP NoMemory
Label366:
MOV dword[ParameterPool],EAX
MOV EAX,EBP
SUB EAX,4
PUSH EAX
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_93+4]
PUSH dword[Number_93]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_94+4]
PUSH dword[Number_94]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FREAD2],0
JNE Label367
stdcall LoadLibraryA,_FREAD2_Lib
MOV dword[_FREAD2_LibHandle],EAX
CMP EAX,0
JNE Label368
PUSH _FREAD2_Lib
JMP NoLibrary
Label368:
stdcall GetProcAddress,dword[_FREAD2_LibHandle],_FREAD2_Alias
MOV dword[_FREAD2],EAX
CMP EAX,0
JNE Label369
PUSH _FREAD2_Alias
JMP NoFunction
Label369:
Label367:
CALL dword[_FREAD2]
ADD ESP,16
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef FILEWRITEDOUBLE_Used

global _FILEWRITEDOUBLE
_FILEWRITEDOUBLE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label370
JMP NoMemory
Label370:
MOV dword[ParameterPool],EAX
MOV EAX,EBP
ADD EAX,12
PUSH EAX
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_95+4]
PUSH dword[Number_95]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_96+4]
PUSH dword[Number_96]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FWRITE2],0
JNE Label371
stdcall LoadLibraryA,_FWRITE2_Lib
MOV dword[_FWRITE2_LibHandle],EAX
CMP EAX,0
JNE Label372
PUSH _FWRITE2_Lib
JMP NoLibrary
Label372:
stdcall GetProcAddress,dword[_FWRITE2_LibHandle],_FWRITE2_Alias
MOV dword[_FWRITE2],EAX
CMP EAX,0
JNE Label373
PUSH _FWRITE2_Alias
JMP NoFunction
Label373:
Label371:
CALL dword[_FWRITE2]
ADD ESP,16
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 12

%endif

%ifdef FILEREADDOUBLE_Used

global _FILEREADDOUBLE
_FILEREADDOUBLE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,8
MOV dword[ESP+8],0
MOV dword[ESP+4],0
MOV dword[EBP-4],0
MOV dword[EBP-8],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label374
JMP NoMemory
Label374:
MOV dword[ParameterPool],EAX
MOV EAX,EBP
SUB EAX,8
PUSH EAX
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_97+4]
PUSH dword[Number_97]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_98+4]
PUSH dword[Number_98]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FREAD2],0
JNE Label375
stdcall LoadLibraryA,_FREAD2_Lib
MOV dword[_FREAD2_LibHandle],EAX
CMP EAX,0
JNE Label376
PUSH _FREAD2_Lib
JMP NoLibrary
Label376:
stdcall GetProcAddress,dword[_FREAD2_LibHandle],_FREAD2_Alias
MOV dword[_FREAD2],EAX
CMP EAX,0
JNE Label377
PUSH _FREAD2_Alias
JMP NoFunction
Label377:
Label375:
CALL dword[_FREAD2]
ADD ESP,16
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


FINIT
FLD qword[EBP-8]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef FILESETPOS_Used

global _FILESETPOS
_FILESETPOS:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label378
JMP NoMemory
Label378:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_99+4]
PUSH dword[Number_99]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FSEEK],0
JNE Label379
stdcall LoadLibraryA,_FSEEK_Lib
MOV dword[_FSEEK_LibHandle],EAX
CMP EAX,0
JNE Label380
PUSH _FSEEK_Lib
JMP NoLibrary
Label380:
stdcall GetProcAddress,dword[_FSEEK_LibHandle],_FSEEK_Alias
MOV dword[_FSEEK],EAX
CMP EAX,0
JNE Label381
PUSH _FSEEK_Alias
JMP NoFunction
Label381:
Label379:
CALL dword[_FSEEK]
ADD ESP,12
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef FILEGETPOS_Used

global _FILEGETPOS
_FILEGETPOS:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label382
JMP NoMemory
Label382:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FTELL],0
JNE Label383
stdcall LoadLibraryA,_FTELL_Lib
MOV dword[_FTELL_LibHandle],EAX
CMP EAX,0
JNE Label384
PUSH _FTELL_Lib
JMP NoLibrary
Label384:
stdcall GetProcAddress,dword[_FTELL_LibHandle],_FTELL_Alias
MOV dword[_FTELL],EAX
CMP EAX,0
JNE Label385
PUSH _FTELL_Alias
JMP NoFunction
Label385:
Label383:
CALL dword[_FTELL]
ADD ESP,4
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef FILECLOSE_Used

global _FILECLOSE
_FILECLOSE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label386
JMP NoMemory
Label386:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FCLOSE],0
JNE Label387
stdcall LoadLibraryA,_FCLOSE_Lib
MOV dword[_FCLOSE_LibHandle],EAX
CMP EAX,0
JNE Label388
PUSH _FCLOSE_Lib
JMP NoLibrary
Label388:
stdcall GetProcAddress,dword[_FCLOSE_LibHandle],_FCLOSE_Alias
MOV dword[_FCLOSE],EAX
CMP EAX,0
JNE Label389
PUSH _FCLOSE_Alias
JMP NoFunction
Label389:
Label387:
CALL dword[_FCLOSE]
ADD ESP,4
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif
NoConsole:
MOV dword[ExitStatus],1
stdcall MessageBoxA,0,NoConsoleMessage,Error,0
JMP Exit



;Data section of the Chameleon app
section .data
NoMemMessage db "Could not allocate memory.",0
NoLibFound db "Could not find library: ",0
NoFunctionFound db "Cound not find function: ",0
Error db "Error!",0
ParameterPool dd 0
HandleToHeap dd 0
TempQWord1 dq 0.0
TempQWord2 dq 0.0
True dd -1
%ifdef ISALNUM_Used
_ISALNUM dd 0
_ISALNUM_LibHandle dd 0
_ISALNUM_Alias db "isalnum",0
_ISALNUM_Lib db "msvcrt.dll",0
_ISALNUM_Call db 1
%endif
%ifdef ISALPHA_Used
_ISALPHA dd 0
_ISALPHA_LibHandle dd 0
_ISALPHA_Alias db "isalpha",0
_ISALPHA_Lib db "msvcrt.dll",0
_ISALPHA_Call db 1
%endif
%ifdef ISCNTRL_Used
_ISCNTRL dd 0
_ISCNTRL_LibHandle dd 0
_ISCNTRL_Alias db "iscntrl",0
_ISCNTRL_Lib db "msvcrt.dll",0
_ISCNTRL_Call db 1
%endif
%ifdef ISDIGIT_Used
_ISDIGIT dd 0
_ISDIGIT_LibHandle dd 0
_ISDIGIT_Alias db "isdigit",0
_ISDIGIT_Lib db "msvcrt.dll",0
_ISDIGIT_Call db 1
%endif
%ifdef ISGRAPH_Used
_ISGRAPH dd 0
_ISGRAPH_LibHandle dd 0
_ISGRAPH_Alias db "isgraph",0
_ISGRAPH_Lib db "msvcrt.dll",0
_ISGRAPH_Call db 1
%endif
%ifdef ISLOWER_Used
_ISLOWER dd 0
_ISLOWER_LibHandle dd 0
_ISLOWER_Alias db "islower",0
_ISLOWER_Lib db "msvcrt.dll",0
_ISLOWER_Call db 1
%endif
%ifdef ISPRINT_Used
_ISPRINT dd 0
_ISPRINT_LibHandle dd 0
_ISPRINT_Alias db "isprint",0
_ISPRINT_Lib db "msvcrt.dll",0
_ISPRINT_Call db 1
%endif
%ifdef ISPUNCT_Used
_ISPUNCT dd 0
_ISPUNCT_LibHandle dd 0
_ISPUNCT_Alias db "ispunct",0
_ISPUNCT_Lib db "msvcrt.dll",0
_ISPUNCT_Call db 1
%endif
%ifdef ISSPACE_Used
_ISSPACE dd 0
_ISSPACE_LibHandle dd 0
_ISSPACE_Alias db "isspace",0
_ISSPACE_Lib db "msvcrt.dll",0
_ISSPACE_Call db 1
%endif
%ifdef ISUPPER_Used
_ISUPPER dd 0
_ISUPPER_LibHandle dd 0
_ISUPPER_Alias db "isupper",0
_ISUPPER_Lib db "msvcrt.dll",0
_ISUPPER_Call db 1
%endif
%ifdef ISXDIGIT_Used
_ISXDIGIT dd 0
_ISXDIGIT_LibHandle dd 0
_ISXDIGIT_Alias db "isxdigit",0
_ISXDIGIT_Lib db "msvcrt.dll",0
_ISXDIGIT_Call db 1
%endif
%ifdef TOLOWER_Used
_TOLOWER dd 0
_TOLOWER_LibHandle dd 0
_TOLOWER_Alias db "tolower",0
_TOLOWER_Lib db "msvcrt.dll",0
_TOLOWER_Call db 1
%endif
%ifdef TOUPPER_Used
_TOUPPER dd 0
_TOUPPER_LibHandle dd 0
_TOUPPER_Alias db "toupper",0
_TOUPPER_Lib db "msvcrt.dll",0
_TOUPPER_Call db 1
%endif
%ifdef LEN_Used
_LEN dd 0
_LEN_LibHandle dd 0
_LEN_Alias db "lstrlenA",0
_LEN_Lib db "Kernel32.dll",0
_LEN_Call db 0
%endif
%ifdef LSTRCPY_Used
_LSTRCPY dd 0
_LSTRCPY_LibHandle dd 0
_LSTRCPY_Alias db "lstrcpyA",0
_LSTRCPY_Lib db "Kernel32.dll",0
_LSTRCPY_Call db 0
%endif
Number_1 dq 1.0
String_2 db "",0
String_3 db " ",0
Number_4 dq 1.0
Number_5 dq 1.0
Number_6 dq 1.0
Number_7 dq 1.0
Number_8 dq 1.0
Number_9 dq 1.0
Number_10 dq 1.0
Number_11 dq 1.0
String_12 db "",0
Number_13 dq 1.0
Number_14 dq 1.0
Number_15 dq 1.0
Number_16 dq 1.0
String_17 db "",0
Number_18 dq 1.0
Number_19 dq 1.0
Number_20 dq 1.0
Number_21 dq 1.0
Number_22 dq 1.0
Number_23 dq 1.0
Number_24 dq 0.0
Number_25 dq 0.0
Number_26 dq 1.0
Number_27 dq 1.0
Number_28 dq 0.0
Number_29 dq 0.0
Number_30 dq 1.0
Number_31 dq 1.0
Number_32 dq 0.0
Number_33 dq 0.0
Number_34 dq 1.0
String_35 db " ",0
Number_36 dq 1.0
Number_37 dq 1.0
Number_38 dq 1.0
Number_39 dq 1.0
Number_40 dq 1.0
Number_41 dq 1.0
Number_42 dq 1.0
Number_43 dq 0.0
Number_44 dq 0.0
Number_45 dq 1.0
Number_46 dq 0.0
Number_47 dq 1.0
Number_48 dq 1.0
Number_49 dq 0.0
Number_50 dq 0.0
Number_51 dq 0.0
Number_52 dq 1.0
Number_53 dq 0.0
Number_54 dq 1.0
String_55 db " ",0
Number_56 dq 1.0
Number_57 dq 1.0
Number_58 dq 1.0
Number_59 dq 1.0
Number_60 dq 1.0
Number_61 dq 1.0
Number_62 dq 1.0
Number_63 dq 1.0
Number_64 dq 1.0
Number_65 dq 0.0
Number_66 dq 0.0
Number_67 dq 0.0
Number_68 dq 1.0
Number_69 dq 1.0
Number_70 dq 1.0
Number_71 dq 10.0
Number_72 dq 13.0
Number_73 dq 13.0
Number_74 dq 13.0
Number_75 dq 10.0
Number_76 dq 11.0
Number_77 dq 1.0
Number_78 dq 0.0
Number_79 dq 1.0
Number_80 dq 1.0
%ifdef FOPEN_Used
_FOPEN dd 0
_FOPEN_LibHandle dd 0
_FOPEN_Alias db "fopen",0
_FOPEN_Lib db "Msvcrt.dll",0
_FOPEN_Call db 1
%endif
%ifdef FWRITE_Used
_FWRITE dd 0
_FWRITE_LibHandle dd 0
_FWRITE_Alias db "fwrite",0
_FWRITE_Lib db "Msvcrt.dll",0
_FWRITE_Call db 1
%endif
%ifdef FWRITE2_Used
_FWRITE2 dd 0
_FWRITE2_LibHandle dd 0
_FWRITE2_Alias db "fwrite",0
_FWRITE2_Lib db "Msvcrt.dll",0
_FWRITE2_Call db 1
%endif
%ifdef FPUTS_Used
_FPUTS dd 0
_FPUTS_LibHandle dd 0
_FPUTS_Alias db "fputs",0
_FPUTS_Lib db "Msvcrt.dll",0
_FPUTS_Call db 1
%endif
%ifdef FREAD_Used
_FREAD dd 0
_FREAD_LibHandle dd 0
_FREAD_Alias db "fread",0
_FREAD_Lib db "Msvcrt.dll",0
_FREAD_Call db 1
%endif
%ifdef FREAD2_Used
_FREAD2 dd 0
_FREAD2_LibHandle dd 0
_FREAD2_Alias db "fread",0
_FREAD2_Lib db "Msvcrt.dll",0
_FREAD2_Call db 1
%endif
%ifdef FGETS_Used
_FGETS dd 0
_FGETS_LibHandle dd 0
_FGETS_Alias db "fgets",0
_FGETS_Lib db "Msvcrt.dll",0
_FGETS_Call db 1
%endif
%ifdef FTELL_Used
_FTELL dd 0
_FTELL_LibHandle dd 0
_FTELL_Alias db "ftell",0
_FTELL_Lib db "Msvcrt.dll",0
_FTELL_Call db 1
%endif
%ifdef FSEEK_Used
_FSEEK dd 0
_FSEEK_LibHandle dd 0
_FSEEK_Alias db "fseek",0
_FSEEK_Lib db "Msvcrt.dll",0
_FSEEK_Call db 1
%endif
%ifdef FGETPOS_Used
_FGETPOS dd 0
_FGETPOS_LibHandle dd 0
_FGETPOS_Alias db "fgetpos",0
_FGETPOS_Lib db "Msvcrt.dll",0
_FGETPOS_Call db 1
%endif
%ifdef FSETPOS_Used
_FSETPOS dd 0
_FSETPOS_LibHandle dd 0
_FSETPOS_Alias db "fsetpos",0
_FSETPOS_Lib db "Msvcrt.dll",0
_FSETPOS_Call db 1
%endif
%ifdef FCLOSE_Used
_FCLOSE dd 0
_FCLOSE_LibHandle dd 0
_FCLOSE_Alias db "fclose",0
_FCLOSE_Lib db "Msvcrt.dll",0
_FCLOSE_Call db 1
%endif
String_81 db "t",0
Number_82 dq 1.0
Number_83 dq 1.0
Number_84 dq 13.0
Number_85 dq 10.0
Number_86 dq 0.0
Number_87 dq 2.0
Number_88 dq 0.0
Number_89 dq 1.0
Number_90 dq 1.0
Number_91 dq 4.0
Number_92 dq 1.0
Number_93 dq 4.0
Number_94 dq 1.0
Number_95 dq 8.0
Number_96 dq 1.0
Number_97 dq 8.0
Number_98 dq 1.0
Number_99 dq 0.0
Internal_HInstance dd 0
Internal_CommandLine dd 0
NoConsoleMessage db 'Error - Cannot access the console handles for Input/Output.',0
ConsoleTemp dd 0
ConsoleNewLine db 10,0
ConsoleClear db 'CLS',0
ConsolePause db 'PAUSE',0
HandleToInput dd 0
HandleToOutput dd 0
Scope63__FILE_Integer dd 0
String_100 db "Hello.txt",0
String_101 db "w+",0
String_102 db "Hello, testing....",0
Number_103 dq 123.0
String_104 db "Wrote stuff to hello.txt!",0
ExitStatus dd 0

