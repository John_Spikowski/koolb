;Library functions to import to the Chameleon app
extern ExitProcess
extern MessageBoxA
extern lstrcat
extern lstrlen
extern lstrcpy
extern HeapCreate
extern HeapAlloc
extern HeapFree
extern GetModuleHandleA
extern GetCommandLineA
extern GetStdHandle
extern FreeLibrary
extern floor
extern lstrlenA
extern lstrcpyA
extern LoadLibraryA
extern GetProcAddress
extern RtlMoveMemory
extern lstrcatA
extern lstrcmpA
extern WriteFile
extern _gcvt
extern ReadFile
extern Sleep
extern atof
extern HeapDestroy
%define LEN_Used
%define LEN_Used
%define MID____Used
%define INSTR_Used
%define ASC_Used
%define CHR____Used
%define LEN_Used
%define LEN_Used
%define MID____Used
%define TOUPPER_Used
%define UCASE____Used
%define ASC_Used
%define CHR____Used
%define LEN_Used
%define LEN_Used
%define MID____Used
%define TOUPPER_Used
%define UCASE____Used
%define LEN_Used
%define ASC_Used
%define CHR____Used
%define LEN_Used
%define LEN_Used
%define MID____Used
%define TOUPPER_Used
%define UCASE____Used
%define ASC_Used
%define CHR____Used
%define LEN_Used
%define LEN_Used
%define MID____Used
%define TOLOWER_Used
%define LCASE____Used
%define LEN_Used
%define MID____Used
%define CHR____Used




;Initialize everything to prep the app to run
section .text
%include "C:\BASM\Bin\MACROS.INC"
global START
START:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH ESI
PUSH EDI
stdcall HeapCreate,0,16384,0
MOV dword[HandleToHeap],EAX
CMP dword[HandleToHeap],0
JNE Label1
JMP NoMemory
Label1:
stdcall GetModuleHandleA,0
MOV dword[Internal_HInstance],EAX
CALL GetCommandLineA
MOV dword[Internal_CommandLine],EAX
stdcall GetStdHandle,-10
MOV dword[HandleToInput],EAX
stdcall GetStdHandle,-11
MOV dword[HandleToOutput],EAX
CMP dword[HandleToInput],-1
JNE Label2
JMP NoConsole
Label2:
CMP dword[HandleToOutput],-1
JNE Label3
JMP NoConsole
Label3:
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label507
JMP NoMemory
Label507:
MOV dword[Scope199__A____String],EAX
MOV byte[EAX],0



;The main body where the app actually runs
MOV EBX,String_132
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label504
JMP NoMemory
Label504:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_132
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapAlloc,dword[HandleToHeap],8,16384
CMP EAX,0
JNE Label505
JMP NoMemory
Label505:
MOV EBX,EAX
stdcall ReadFile,dword[HandleToInput],EBX,16383,ConsoleTemp,0
stdcall lstrlenA,EBX
INC EAX
DEC EAX
DEC EAX
DEC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label506
JMP NoMemory
Label506:
MOV ESI,EAX
stdcall RtlMoveMemory,ESI,EBX,EDI
MOV byte[ESI+EDI],0
PUSH ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[Scope199__A____String]
MOV dword[Scope199__A____String],EBX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label508
JMP NoMemory
Label508:
MOV dword[ParameterPool],EAX
PUSH dword[Number_133+4]
PUSH dword[Number_133]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label509
JMP NoMemory
Label509:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[Scope199__A____String]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label510
JMP NoMemory
Label510:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[Scope199__A____String]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _UCASE___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label511
JMP NoMemory
Label511:
MOV dword[ParameterPool],EAX
MOV EBX,String_134
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label512
JMP NoMemory
Label512:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_134
PUSH EBX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _UCASE___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _INSTR
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_135+4]
PUSH dword[Number_135]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JNE Label513
FLDZ
Label513:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label515
JMP Label514
Label515:
MOV EBX,String_136
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label517
JMP NoMemory
Label517:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_136
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
JMP Label516
Label514:
MOV EBX,String_137
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label519
JMP NoMemory
Label519:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_137
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
JMP Label518
Label518:
Label516:
PUSH dword[Number_138+4]
PUSH dword[Number_138]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
MOV EBX,String_139
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label520
JMP NoMemory
Label520:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_139
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_140
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label521
JMP NoMemory
Label521:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_140
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_141
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label522
JMP NoMemory
Label522:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_141
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapAlloc,dword[HandleToHeap],8,16384
CMP EAX,0
JNE Label523
JMP NoMemory
Label523:
MOV EBX,EAX
stdcall ReadFile,dword[HandleToInput],EBX,16383,ConsoleTemp,0
stdcall lstrlenA,EBX
INC EAX
DEC EAX
DEC EAX
DEC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label524
JMP NoMemory
Label524:
MOV ESI,EAX
stdcall RtlMoveMemory,ESI,EBX,EDI
MOV byte[ESI+EDI],0
PUSH ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[Scope199__A____String]
MOV dword[Scope199__A____String],EBX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label525
JMP NoMemory
Label525:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[Scope199__A____String]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label526
JMP NoMemory
Label526:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[Scope199__A____String]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label527
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label528
PUSH _LEN_Lib
JMP NoLibrary
Label528:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label529
PUSH _LEN_Alias
JMP NoFunction
Label529:
Label527:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope199__LENGTH_Integer]
PUSH dword[Number_142+4]
PUSH dword[Number_142]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
MOV EBX,String_143
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label530
JMP NoMemory
Label530:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_143
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
FINIT
FILD dword[Scope199__LENGTH_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label534
JMP NoMemory
Label534:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label531:
CMP byte[EBX+ECX],0
JE Label532
INC ECX
CMP ECX,EDI
JL Label531
JMP Label533
Label532:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label533
MOV byte[EBX+ECX],0
Label533:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_144+4]
PUSH dword[Number_144]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
MOV EBX,String_145
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label535
JMP NoMemory
Label535:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_145
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label536
JMP NoMemory
Label536:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[Scope199__A____String]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label537
JMP NoMemory
Label537:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[Scope199__A____String]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _UCASE___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_146+4]
PUSH dword[Number_146]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
MOV EBX,String_147
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label538
JMP NoMemory
Label538:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_147
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label539
JMP NoMemory
Label539:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[Scope199__A____String]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label540
JMP NoMemory
Label540:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[Scope199__A____String]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _LCASE___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_148+4]
PUSH dword[Number_148]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
MOV EBX,String_149
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label541
JMP NoMemory
Label541:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_149
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label542
JMP NoMemory
Label542:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[Scope199__A____String]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label543
JMP NoMemory
Label543:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[Scope199__A____String]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[Scope199__LENGTH_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_150+4]
PUSH dword[Number_150]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FDIV ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_151+4]
PUSH dword[Number_151]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_152
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label544
JMP NoMemory
Label544:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_152
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_153
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label545
JMP NoMemory
Label545:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_153
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_154+4]
PUSH dword[Number_154]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
MOV EBX,String_155
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label546
JMP NoMemory
Label546:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_155
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapAlloc,dword[HandleToHeap],8,16384
CMP EAX,0
JNE Label547
JMP NoMemory
Label547:
MOV EBX,EAX
stdcall ReadFile,dword[HandleToInput],EBX,16383,ConsoleTemp,0
stdcall lstrlenA,EBX
INC EAX
DEC EAX
DEC EAX
DEC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label548
JMP NoMemory
Label548:
MOV ESI,EAX
stdcall RtlMoveMemory,ESI,EBX,EDI
MOV byte[ESI+EDI],0
PUSH ESI
POP EBX
ccall atof,EBX
stdcall HeapFree,dword[HandleToHeap],0,EBX
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope199__A__Integer]
PUSH dword[Number_156+4]
PUSH dword[Number_156]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
MOV EBX,String_157
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label549
JMP NoMemory
Label549:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_157
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label550
JMP NoMemory
Label550:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[Scope199__A__Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_158
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label551
JMP NoMemory
Label551:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_158
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_159
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label552
JMP NoMemory
Label552:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_159
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_160+4]
PUSH dword[Number_160]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
MOV EBX,String_161
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label553
JMP NoMemory
Label553:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_161
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_162
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label554
JMP NoMemory
Label554:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_162
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_163
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label555
JMP NoMemory
Label555:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_163
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_164+4]
PUSH dword[Number_164]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
MOV EBX,String_165
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label556
JMP NoMemory
Label556:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_165
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_166+4]
PUSH dword[Number_166]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX



;Prepare the app to exit and then terminate
Exit:
%ifdef BEGINPAINT_Used
stdcall FreeLibrary,dword[_BEGINPAINT_LibHandle]
%endif
%ifdef ENDPAINT_Used
stdcall FreeLibrary,dword[_ENDPAINT_LibHandle]
%endif
%ifdef TEXTOUT_Used
stdcall FreeLibrary,dword[_TEXTOUT_LibHandle]
%endif
%ifdef DRAWTEXT_Used
stdcall FreeLibrary,dword[_DRAWTEXT_LibHandle]
%endif
%ifdef ENUMCHILDWINDOWS_Used
stdcall FreeLibrary,dword[_ENUMCHILDWINDOWS_LibHandle]
%endif
%ifdef GETTOPWINDOW_Used
stdcall FreeLibrary,dword[_GETTOPWINDOW_LibHandle]
%endif
%ifdef GETNEXTWINDOW_Used
stdcall FreeLibrary,dword[_GETNEXTWINDOW_LibHandle]
%endif
%ifdef GETCLIENTRECT_Used
stdcall FreeLibrary,dword[_GETCLIENTRECT_LibHandle]
%endif
%ifdef GETWINDOWRECT_Used
stdcall FreeLibrary,dword[_GETWINDOWRECT_LibHandle]
%endif
%ifdef REGISTERCLASS_Used
stdcall FreeLibrary,dword[_REGISTERCLASS_LibHandle]
%endif
%ifdef REGISTERCLASSEX_Used
stdcall FreeLibrary,dword[_REGISTERCLASSEX_LibHandle]
%endif
%ifdef UNREGISTERCLASS_Used
stdcall FreeLibrary,dword[_UNREGISTERCLASS_LibHandle]
%endif
%ifdef GETMESSAGE_Used
stdcall FreeLibrary,dword[_GETMESSAGE_LibHandle]
%endif
%ifdef TRANSLATEMESSAGE_Used
stdcall FreeLibrary,dword[_TRANSLATEMESSAGE_LibHandle]
%endif
%ifdef DISPATCHMESSAGE_Used
stdcall FreeLibrary,dword[_DISPATCHMESSAGE_LibHandle]
%endif
%ifdef SENDMESSAGE_Used
stdcall FreeLibrary,dword[_SENDMESSAGE_LibHandle]
%endif
%ifdef LOADBITMAP_Used
stdcall FreeLibrary,dword[_LOADBITMAP_LibHandle]
%endif
%ifdef LOADCURSOR_Used
stdcall FreeLibrary,dword[_LOADCURSOR_LibHandle]
%endif
%ifdef LOADICON_Used
stdcall FreeLibrary,dword[_LOADICON_LibHandle]
%endif
%ifdef DESTROYICON_Used
stdcall FreeLibrary,dword[_DESTROYICON_LibHandle]
%endif
%ifdef GETCURSOR_Used
stdcall FreeLibrary,dword[_GETCURSOR_LibHandle]
%endif
%ifdef SETCURSOR_Used
stdcall FreeLibrary,dword[_SETCURSOR_LibHandle]
%endif
%ifdef COPYICON_Used
stdcall FreeLibrary,dword[_COPYICON_LibHandle]
%endif
%ifdef SETSYSTEMCURSOR_Used
stdcall FreeLibrary,dword[_SETSYSTEMCURSOR_LibHandle]
%endif
%ifdef CREATECOMPATIBLEDC_Used
stdcall FreeLibrary,dword[_CREATECOMPATIBLEDC_LibHandle]
%endif
%ifdef CREATEWINDOWEX_Used
stdcall FreeLibrary,dword[_CREATEWINDOWEX_LibHandle]
%endif
%ifdef UPDATEWINDOW_Used
stdcall FreeLibrary,dword[_UPDATEWINDOW_LibHandle]
%endif
%ifdef SHOWWINDOW_Used
stdcall FreeLibrary,dword[_SHOWWINDOW_LibHandle]
%endif
%ifdef DEFWINDOWPROC_Used
stdcall FreeLibrary,dword[_DEFWINDOWPROC_LibHandle]
%endif
%ifdef POSTQUITMESSAGE_Used
stdcall FreeLibrary,dword[_POSTQUITMESSAGE_LibHandle]
%endif
%ifdef GETMODULEHANDLE_Used
stdcall FreeLibrary,dword[_GETMODULEHANDLE_LibHandle]
%endif
%ifdef GETACTIVEWINDOW_Used
stdcall FreeLibrary,dword[_GETACTIVEWINDOW_LibHandle]
%endif
%ifdef EXITPROCESS_Used
stdcall FreeLibrary,dword[_EXITPROCESS_LibHandle]
%endif
%ifdef MSGBOX_Used
stdcall FreeLibrary,dword[_MSGBOX_LibHandle]
%endif
%ifdef MESSAGEBOX_Used
stdcall FreeLibrary,dword[_MESSAGEBOX_LibHandle]
%endif
%ifdef GETLASTERROR_Used
stdcall FreeLibrary,dword[_GETLASTERROR_LibHandle]
%endif
%ifdef SETLASTERROR_Used
stdcall FreeLibrary,dword[_SETLASTERROR_LibHandle]
%endif
%ifdef FORMATMESSAGE_Used
stdcall FreeLibrary,dword[_FORMATMESSAGE_LibHandle]
%endif
%ifdef LOCALFREE_Used
stdcall FreeLibrary,dword[_LOCALFREE_LibHandle]
%endif
%ifdef GETWINDOWLONG_Used
stdcall FreeLibrary,dword[_GETWINDOWLONG_LibHandle]
%endif
%ifdef SETWINDOWLONG_Used
stdcall FreeLibrary,dword[_SETWINDOWLONG_LibHandle]
%endif
%ifdef CALLWINDOWPROC_Used
stdcall FreeLibrary,dword[_CALLWINDOWPROC_LibHandle]
%endif
%ifdef GETCLASSLONG_Used
stdcall FreeLibrary,dword[_GETCLASSLONG_LibHandle]
%endif
%ifdef SETCLASSLONG_Used
stdcall FreeLibrary,dword[_SETCLASSLONG_LibHandle]
%endif
%ifdef GETDESKTOPWINDOW_Used
stdcall FreeLibrary,dword[_GETDESKTOPWINDOW_LibHandle]
%endif
%ifdef GETPROCADDRESS_Used
stdcall FreeLibrary,dword[_GETPROCADDRESS_LibHandle]
%endif
%ifdef DIALOGBOXPARAM_Used
stdcall FreeLibrary,dword[_DIALOGBOXPARAM_LibHandle]
%endif
%ifdef ENDDIALOG_Used
stdcall FreeLibrary,dword[_ENDDIALOG_LibHandle]
%endif
%ifdef GETDLGITEM_Used
stdcall FreeLibrary,dword[_GETDLGITEM_LibHandle]
%endif
%ifdef INITCOMMONCONTROLS_Used
stdcall FreeLibrary,dword[_INITCOMMONCONTROLS_LibHandle]
%endif
%ifdef INVALIDATERECT_Used
stdcall FreeLibrary,dword[_INVALIDATERECT_LibHandle]
%endif
%ifdef CREATEPEN_Used
stdcall FreeLibrary,dword[_CREATEPEN_LibHandle]
%endif
%ifdef SELECTOBJECT_Used
stdcall FreeLibrary,dword[_SELECTOBJECT_LibHandle]
%endif
%ifdef GETSTOCKOBJECT_Used
stdcall FreeLibrary,dword[_GETSTOCKOBJECT_LibHandle]
%endif
%ifdef CREATESOLIDBRUSH_Used
stdcall FreeLibrary,dword[_CREATESOLIDBRUSH_LibHandle]
%endif
%ifdef RECTANGLE_Used
stdcall FreeLibrary,dword[_RECTANGLE_LibHandle]
%endif
%ifdef DELETEOBJECT_Used
stdcall FreeLibrary,dword[_DELETEOBJECT_LibHandle]
%endif
%ifdef DELETEDC_Used
stdcall FreeLibrary,dword[_DELETEDC_LibHandle]
%endif
%ifdef DESTROYWINDOW_Used
stdcall FreeLibrary,dword[_DESTROYWINDOW_LibHandle]
%endif
%ifdef SETPIXEL_Used
stdcall FreeLibrary,dword[_SETPIXEL_LibHandle]
%endif
%ifdef BITBLT_Used
stdcall FreeLibrary,dword[_BITBLT_LibHandle]
%endif
%ifdef CREATEMENU_Used
stdcall FreeLibrary,dword[_CREATEMENU_LibHandle]
%endif
%ifdef APPENDMENU_Used
stdcall FreeLibrary,dword[_APPENDMENU_LibHandle]
%endif
%ifdef POSTMESSAGE_Used
stdcall FreeLibrary,dword[_POSTMESSAGE_LibHandle]
%endif
%ifdef GETSYSTEMMETRICS_Used
stdcall FreeLibrary,dword[_GETSYSTEMMETRICS_LibHandle]
%endif
%ifdef SETWINDOWPOS_Used
stdcall FreeLibrary,dword[_SETWINDOWPOS_LibHandle]
%endif
%ifdef ENABLEWINDOW_Used
stdcall FreeLibrary,dword[_ENABLEWINDOW_LibHandle]
%endif
%ifdef PLAYSOUND_Used
stdcall FreeLibrary,dword[_PLAYSOUND_LibHandle]
%endif
%ifdef TIMESETEVENT_Used
stdcall FreeLibrary,dword[_TIMESETEVENT_LibHandle]
%endif
%ifdef TIMEKILLEVENT_Used
stdcall FreeLibrary,dword[_TIMEKILLEVENT_LibHandle]
%endif
%ifdef CREATEFONT_Used
stdcall FreeLibrary,dword[_CREATEFONT_LibHandle]
%endif
%ifdef ABS_Used
stdcall FreeLibrary,dword[_ABS_LibHandle]
%endif
%ifdef ACOS_Used
stdcall FreeLibrary,dword[_ACOS_LibHandle]
%endif
%ifdef ASIN_Used
stdcall FreeLibrary,dword[_ASIN_LibHandle]
%endif
%ifdef ATAN_Used
stdcall FreeLibrary,dword[_ATAN_LibHandle]
%endif
%ifdef ATN_Used
stdcall FreeLibrary,dword[_ATN_LibHandle]
%endif
%ifdef CEIL_Used
stdcall FreeLibrary,dword[_CEIL_LibHandle]
%endif
%ifdef EXP_Used
stdcall FreeLibrary,dword[_EXP_LibHandle]
%endif
%ifdef FLOOR_Used
stdcall FreeLibrary,dword[_FLOOR_LibHandle]
%endif
%ifdef LOG_Used
stdcall FreeLibrary,dword[_LOG_LibHandle]
%endif
%ifdef SQR_Used
stdcall FreeLibrary,dword[_SQR_LibHandle]
%endif
%ifdef SQRT_Used
stdcall FreeLibrary,dword[_SQRT_LibHandle]
%endif
%ifdef TAN_Used
stdcall FreeLibrary,dword[_TAN_LibHandle]
%endif
%ifdef TIMER_Used
stdcall FreeLibrary,dword[_TIMER_LibHandle]
%endif
%ifdef POW_Used
stdcall FreeLibrary,dword[_POW_LibHandle]
%endif
%ifdef ISALNUM_Used
stdcall FreeLibrary,dword[_ISALNUM_LibHandle]
%endif
%ifdef ISALPHA_Used
stdcall FreeLibrary,dword[_ISALPHA_LibHandle]
%endif
%ifdef ISCNTRL_Used
stdcall FreeLibrary,dword[_ISCNTRL_LibHandle]
%endif
%ifdef ISDIGIT_Used
stdcall FreeLibrary,dword[_ISDIGIT_LibHandle]
%endif
%ifdef ISGRAPH_Used
stdcall FreeLibrary,dword[_ISGRAPH_LibHandle]
%endif
%ifdef ISLOWER_Used
stdcall FreeLibrary,dword[_ISLOWER_LibHandle]
%endif
%ifdef ISPRINT_Used
stdcall FreeLibrary,dword[_ISPRINT_LibHandle]
%endif
%ifdef ISPUNCT_Used
stdcall FreeLibrary,dword[_ISPUNCT_LibHandle]
%endif
%ifdef ISSPACE_Used
stdcall FreeLibrary,dword[_ISSPACE_LibHandle]
%endif
%ifdef ISUPPER_Used
stdcall FreeLibrary,dword[_ISUPPER_LibHandle]
%endif
%ifdef ISXDIGIT_Used
stdcall FreeLibrary,dword[_ISXDIGIT_LibHandle]
%endif
%ifdef TOLOWER_Used
stdcall FreeLibrary,dword[_TOLOWER_LibHandle]
%endif
%ifdef TOUPPER_Used
stdcall FreeLibrary,dword[_TOUPPER_LibHandle]
%endif
%ifdef CHDIR_Used
stdcall FreeLibrary,dword[_CHDIR_LibHandle]
%endif
%ifdef KILL_Used
stdcall FreeLibrary,dword[_KILL_LibHandle]
%endif
%ifdef CREATEDIRECTORY_Used
stdcall FreeLibrary,dword[_CREATEDIRECTORY_LibHandle]
%endif
%ifdef RANDOMIZE_Used
stdcall FreeLibrary,dword[_RANDOMIZE_LibHandle]
%endif
%ifdef RAND_Used
stdcall FreeLibrary,dword[_RAND_LibHandle]
%endif
%ifdef ENVIRON____Used
stdcall FreeLibrary,dword[_ENVIRON____LibHandle]
%endif
%ifdef RENAME_Used
stdcall FreeLibrary,dword[_RENAME_LibHandle]
%endif
%ifdef RMDIR_Used
stdcall FreeLibrary,dword[_RMDIR_LibHandle]
%endif
%ifdef RUN_Used
stdcall FreeLibrary,dword[_RUN_LibHandle]
%endif
%ifdef LEN_Used
stdcall FreeLibrary,dword[_LEN_LibHandle]
%endif
%ifdef LSTRCPY_Used
stdcall FreeLibrary,dword[_LSTRCPY_LibHandle]
%endif
%ifdef GCVT_Used
stdcall FreeLibrary,dword[_GCVT_LibHandle]
%endif
%ifdef VAL_Used
stdcall FreeLibrary,dword[_VAL_LibHandle]
%endif
%ifdef GETSTDHANDLE_Used
stdcall FreeLibrary,dword[_GETSTDHANDLE_LibHandle]
%endif
%ifdef SETCONSOLETITLEWINAPI_Used
stdcall FreeLibrary,dword[_SETCONSOLETITLEWINAPI_LibHandle]
%endif
%ifdef SETCONSOLETEXTATTRIBUTE_Used
stdcall FreeLibrary,dword[_SETCONSOLETEXTATTRIBUTE_LibHandle]
%endif
%ifdef GETLOCALTIME_Used
stdcall FreeLibrary,dword[_GETLOCALTIME_LibHandle]
%endif
%ifdef FOPEN_Used
stdcall FreeLibrary,dword[_FOPEN_LibHandle]
%endif
%ifdef FWRITE_Used
stdcall FreeLibrary,dword[_FWRITE_LibHandle]
%endif
%ifdef FWRITE2_Used
stdcall FreeLibrary,dword[_FWRITE2_LibHandle]
%endif
%ifdef FPUTS_Used
stdcall FreeLibrary,dword[_FPUTS_LibHandle]
%endif
%ifdef FREAD_Used
stdcall FreeLibrary,dword[_FREAD_LibHandle]
%endif
%ifdef FREAD2_Used
stdcall FreeLibrary,dword[_FREAD2_LibHandle]
%endif
%ifdef FGETS_Used
stdcall FreeLibrary,dword[_FGETS_LibHandle]
%endif
%ifdef FTELL_Used
stdcall FreeLibrary,dword[_FTELL_LibHandle]
%endif
%ifdef FSEEK_Used
stdcall FreeLibrary,dword[_FSEEK_LibHandle]
%endif
%ifdef FGETPOS_Used
stdcall FreeLibrary,dword[_FGETPOS_LibHandle]
%endif
%ifdef FSETPOS_Used
stdcall FreeLibrary,dword[_FSETPOS_LibHandle]
%endif
%ifdef FCLOSE_Used
stdcall FreeLibrary,dword[_FCLOSE_LibHandle]
%endif
%ifdef LTOA_Used
stdcall FreeLibrary,dword[_LTOA_LibHandle]
%endif
%ifdef ERRORMSGBOX_Used
stdcall FreeLibrary,dword[_ERRORMSGBOX_LibHandle]
%endif
stdcall HeapFree,dword[HandleToHeap],0,dword[Scope199__A____String]
stdcall HeapDestroy,dword[HandleToHeap]
POP EDI
POP ESI
POP EBX
MOV ESP,EBP
POP EBP
MOV EAX,dword[ExitStatus]
stdcall ExitProcess,dword[ExitStatus]
RET

NoMemory:
MOV dword[ExitStatus],1
stdcall MessageBoxA,0,NoMemMessage,Error,0
JMP Exit

NoLibrary:
MOV dword[ExitStatus],1
POP EBX
stdcall lstrlen,EBX
ADD EAX,30
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
MOV EDI,EAX
stdcall lstrcpy,EDI,NoLibFound
stdcall lstrcat,EDI,EBX
stdcall MessageBoxA,0,EDI,Error,0
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
JMP Exit

NoFunction:
MOV dword[ExitStatus],1
POP EBX
stdcall lstrlen,EBX
ADD EAX,30
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
MOV EDI,EAX
stdcall lstrcpy,EDI,NoFunctionFound
stdcall lstrcat,EDI,EBX
stdcall MessageBoxA,0,EDI,Error,0
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
JMP Exit
NoConsole:
MOV dword[ExitStatus],1
stdcall MessageBoxA,0,NoConsoleMessage,Error,0
JMP Exit






















































































































































































































%ifdef SHOWMESSAGE_Used

global _SHOWMESSAGE
_SHOWMESSAGE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label4
JMP NoMemory
Label4:
MOV dword[ParameterPool],EAX
PUSH dword[Number_1+4]
PUSH dword[Number_1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label5
JMP NoMemory
Label5:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EBX,String_2
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label6
JMP NoMemory
Label6:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_2
PUSH EBX
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Number_3+4]
PUSH dword[Number_3]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_MESSAGEBOX],0
JNE Label7
stdcall LoadLibraryA,_MESSAGEBOX_Lib
MOV dword[_MESSAGEBOX_LibHandle],EAX
CMP EAX,0
JNE Label8
PUSH _MESSAGEBOX_Lib
JMP NoLibrary
Label8:
stdcall GetProcAddress,dword[_MESSAGEBOX_LibHandle],_MESSAGEBOX_Alias
MOV dword[_MESSAGEBOX],EAX
CMP EAX,0
JNE Label9
PUSH _MESSAGEBOX_Alias
JMP NoFunction
Label9:
Label7:
CALL dword[_MESSAGEBOX]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef LOWORD_Used

global _LOWORD
_LOWORD:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

mov eax,dword[ebp+8]   ;dwValue
and eax,65535
mov dword[ebp-4],eax


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef HIWORD_Used

global _HIWORD
_HIWORD:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

mov eax,dword[ebp+8]
and ecx,16
shr eax,cl 
mov dword[ebp-4],eax


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef MAKEINTRESOURCE_Used

global _MAKEINTRESOURCE
_MAKEINTRESOURCE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label10
JMP NoMemory
Label10:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

extern sprintf
jmp forward
strFormat db '#%010ld',0
forward:
stdcall HeapAlloc,dword[HandleToHeap],8,12 
mov ebx,eax 
ccall sprintf,eax,strFormat,dword[ebp+8]
mov dword[ebp-4],ebx


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef RETURN_Used

global _RETURN
_RETURN:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

mov   eax,[ebp+8] 


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef OR_Used

global _OR
_OR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

MOV EBX,dword[EBP+8]
MOV EDI,dword[EBP+12]
OR  EBX,EDI
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef BOR_Used

global _BOR
_BOR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

MOV EBX,dword[EBP+8]
MOV EDI,dword[EBP+12]
OR  EBX,EDI
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef BAND_Used

global _BAND
_BAND:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

MOV EBX,dword[EBP+8]
MOV EDI,dword[EBP+12]
AND EBX,EDI
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef BXOR_Used

global _BXOR
_BXOR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

MOV EBX,dword[EBP+8]
MOV EDI,dword[EBP+12]
XOR EBX,EDI
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef SHIFTLEFT_Used

global _SHIFTLEFT
_SHIFTLEFT:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

mov eax,dword[ebp+8]   ;nBase
mov ecx,dword[ebp+12]  ;nBits
shl eax,cl  
mov dword[ebp-4],eax


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef SIN_Used

global _SIN
_SIN:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,8
MOV dword[ESP+8],0
MOV dword[ESP+4],0
MOV dword[EBP-4],0
MOV dword[EBP-8],0

fld qword[ebp+8]
fsin
fstp qword[ebp-8]


FINIT
FLD qword[EBP-8]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef COS_Used

global _COS
_COS:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,8
MOV dword[ESP+8],0
MOV dword[ESP+4],0
MOV dword[EBP-4],0
MOV dword[EBP-8],0

fld qword[ebp+8]
fcos
fstp qword[ebp-8]


FINIT
FLD qword[EBP-8]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif











































%ifdef DECREMENT_Used

global _DECREMENT
_DECREMENT:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,8
MOV dword[ESP+8],0
MOV dword[ESP+4],0
MOV dword[EBP-4],0
MOV dword[EBP-8],0

PUSH dword[EBP+8+4]
PUSH dword[EBP+8]
PUSH dword[Number_4+4]
PUSH dword[Number_4]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FST qword[EBP-8]


FINIT
FLD qword[EBP-8]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef INCREMENT_Used

global _INCREMENT
_INCREMENT:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,8
MOV dword[ESP+8],0
MOV dword[ESP+4],0
MOV dword[EBP-4],0
MOV dword[EBP-8],0

PUSH dword[EBP+8+4]
PUSH dword[EBP+8]
PUSH dword[Number_5+4]
PUSH dword[Number_5]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FST qword[EBP-8]


FINIT
FLD qword[EBP-8]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef FRAC_Used

global _FRAC
_FRAC:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,8
MOV dword[ESP+8],0
MOV dword[ESP+4],0
MOV dword[EBP-4],0
MOV dword[EBP-8],0

PUSH dword[EBP+8+4]
PUSH dword[EBP+8]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label11
JMP NoMemory
Label11:
MOV dword[ParameterPool],EAX
PUSH dword[EBP+8+4]
PUSH dword[EBP+8]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
POP dword[EAX+0+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0+4]
PUSH dword[EAX+0]
CMP dword[_FLOOR],0
JNE Label12
stdcall LoadLibraryA,_FLOOR_Lib
MOV dword[_FLOOR_LibHandle],EAX
CMP EAX,0
JNE Label13
PUSH _FLOOR_Lib
JMP NoLibrary
Label13:
stdcall GetProcAddress,dword[_FLOOR_LibHandle],_FLOOR_Alias
MOV dword[_FLOOR],EAX
CMP EAX,0
JNE Label14
PUSH _FLOOR_Alias
JMP NoFunction
Label14:
Label12:
CALL dword[_FLOOR]
ADD ESP,8
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FST qword[EBP-8]


FINIT
FLD qword[EBP-8]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif



































































%ifdef MKDIR_Used

global _MKDIR
_MKDIR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label15
JMP NoMemory
Label15:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label16
JMP NoMemory
Label16:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EDI,Scope113__SECURITYATTRIBUTES_TYPE_size
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label17
JMP NoMemory
Label17:
MOV EBX,EAX
stdcall RtlMoveMemory,EBX,Scope123__SECATTRIBUTES_UDT,EDI
PUSH EBX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
MOV EAX,dword[EAX+4]
PUSH dword[EAX]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_CREATEDIRECTORY],0
JNE Label18
stdcall LoadLibraryA,_CREATEDIRECTORY_Lib
MOV dword[_CREATEDIRECTORY_LibHandle],EAX
CMP EAX,0
JNE Label19
PUSH _CREATEDIRECTORY_Lib
JMP NoLibrary
Label19:
stdcall GetProcAddress,dword[_CREATEDIRECTORY_LibHandle],_CREATEDIRECTORY_Alias
MOV dword[_CREATEDIRECTORY],EAX
CMP EAX,0
JNE Label20
PUSH _CREATEDIRECTORY_Alias
JMP NoFunction
Label20:
Label18:
CALL dword[_CREATEDIRECTORY]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef RND_Used

global _RND
_RND:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

CMP dword[_RAND],0
JNE Label21
stdcall LoadLibraryA,_RAND_Lib
MOV dword[_RAND_LibHandle],EAX
CMP EAX,0
JNE Label22
PUSH _RAND_Lib
JMP NoLibrary
Label22:
stdcall GetProcAddress,dword[_RAND_LibHandle],_RAND_Alias
MOV dword[_RAND],EAX
CMP EAX,0
JNE Label23
PUSH _RAND_Alias
JMP NoFunction
Label23:
Label21:
CALL dword[_RAND]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_6+4]
PUSH dword[Number_6]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FDIV ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FMUL ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef SGN_Used

global _SGN
_SGN:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_7+4]
PUSH dword[Number_7]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label24
FLDZ
Label24:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label26
JMP Label25
Label26:
PUSH dword[Number_8+4]
PUSH dword[Number_8]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label27
Label25:
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_9+4]
PUSH dword[Number_9]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JA Label28
FLDZ
Label28:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label30
JMP Label29
Label30:
PUSH dword[Number_10+4]
PUSH dword[Number_10]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label27
Label29:
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_11+4]
PUSH dword[Number_11]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JB Label31
FLDZ
Label31:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label33
JMP Label32
Label33:
PUSH dword[Number_12+4]
PUSH dword[Number_12]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label32
Label32:
Label27:


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif







%ifdef SPACE____Used

global _SPACE___
_SPACE___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label34
JMP NoMemory
Label34:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4

PUSH dword[Number_13+4]
PUSH dword[Number_13]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
MOV EBX,String_14
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label35
JMP NoMemory
Label35:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_14
PUSH EBX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
Label36:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JBE Label37
FLDZ
Label37:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label39
JMP Label38
Label39:
stdcall lstrlenA,dword[EBP-4]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label40
JMP NoMemory
Label40:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-4]
PUSH EAX
MOV EBX,String_15
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label41
JMP NoMemory
Label41:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_15
PUSH EBX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label42
JMP NoMemory
Label42:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_16+4]
PUSH dword[Number_16]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
JMP Label36
Label38:


MOV EAX,dword[EBP-4]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef MID____Used

global _MID___
_MID___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label43
JMP NoMemory
Label43:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_17+4]
PUSH dword[Number_17]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JB Label44
FLDZ
Label44:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label46
JMP Label45
Label46:
PUSH dword[Number_18+4]
PUSH dword[Number_18]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP+12]
JMP Label45
Label45:
Label47:
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP+16]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label48
JMP NoMemory
Label48:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label49
JMP NoMemory
Label49:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label50
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label51
PUSH _LEN_Lib
JMP NoLibrary
Label51:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label52
PUSH _LEN_Alias
JMP NoFunction
Label52:
Label50:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JA Label53
FLDZ
Label53:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label55
JMP Label54
Label55:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label57
JMP NoMemory
Label57:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label58
JMP NoMemory
Label58:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label59
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label60
PUSH _LEN_Lib
JMP NoLibrary
Label60:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label61
PUSH _LEN_Alias
JMP NoFunction
Label61:
Label59:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_19+4]
PUSH dword[Number_19]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP+16]
JMP Label54
Label54:
Label56:
extern lstrlenA
extern lstrcpyA
extern lstrcpynA
MOV EBX,dword[EBP+8]   ;EBX = S
MOV ESI,dword[EBP+12]  ;ESI = Start
MOV EDI,dword[EBP+16]  ;EDI = Length
DEC ESI                ;Convert to zero-based 
INC EDI
ADD EBX,ESI            ;Move Ptr to Start
ADD ESI,EDI
INC ESI
stdcall HeapAlloc,dword[HandleToHeap],8,ESI
DEC ESI
stdcall lstrcpynA,EAX,EBX,EDI
MOV dword[EBP-4],EAX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 12

%endif

%ifdef ASC_Used

global _ASC
_ASC:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

MOV EAX,dword[EBP+8]
MOVSX EAX,byte[EAX]
MOV dword[EBP-4],EAX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef CHR____Used

global _CHR___
_CHR___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label62
JMP NoMemory
Label62:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

stdcall HeapAlloc,dword[HandleToHeap],8,2
MOV EBX,EAX
MOV AH,byte[EBP+8]
MOV byte[EBX],AH
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef DELETE____Used

global _DELETE___
_DELETE___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label63
JMP NoMemory
Label63:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label64
JMP NoMemory
Label64:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label65
JMP NoMemory
Label65:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_20+4]
PUSH dword[Number_20]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_21+4]
PUSH dword[Number_21]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label66
JMP NoMemory
Label66:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label67
JMP NoMemory
Label67:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP+16]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label68
JMP NoMemory
Label68:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label69
JMP NoMemory
Label69:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label70
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label71
PUSH _LEN_Lib
JMP NoLibrary
Label71:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label72
PUSH _LEN_Alias
JMP NoFunction
Label72:
Label70:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label73
JMP NoMemory
Label73:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 12

%endif

%ifdef INSERT____Used

global _INSERT___
_INSERT___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label74
JMP NoMemory
Label74:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label75
JMP NoMemory
Label75:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label76
JMP NoMemory
Label76:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_22+4]
PUSH dword[Number_22]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP+16]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_23+4]
PUSH dword[Number_23]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label77
JMP NoMemory
Label77:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label78
JMP NoMemory
Label78:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label79
JMP NoMemory
Label79:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label80
JMP NoMemory
Label80:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+16]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label81
JMP NoMemory
Label81:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label82
JMP NoMemory
Label82:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label83
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label84
PUSH _LEN_Lib
JMP NoLibrary
Label84:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label85
PUSH _LEN_Alias
JMP NoFunction
Label85:
Label83:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label86
JMP NoMemory
Label86:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 12

%endif

%ifdef LCASE____Used

global _LCASE___
_LCASE___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label87
JMP NoMemory
Label87:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4
SUB ESP,4

MOV EBX,String_24
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label88
JMP NoMemory
Label88:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_24
PUSH EBX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
PUSH dword[Number_25+4]
PUSH dword[Number_25]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label89
JMP NoMemory
Label89:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label90
JMP NoMemory
Label90:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label91
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label92
PUSH _LEN_Lib
JMP NoLibrary
Label92:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label93
PUSH _LEN_Alias
JMP NoFunction
Label93:
Label91:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_26+4]
PUSH dword[Number_26]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
Label94:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JB Label95
FLDZ
Label95:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label97
JMP Label96
Label97:
stdcall lstrlenA,dword[EBP-4]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label98
JMP NoMemory
Label98:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-4]
PUSH EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label99
JMP NoMemory
Label99:
MOV dword[ParameterPool],EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label100
JMP NoMemory
Label100:
MOV dword[ParameterPool],EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label101
JMP NoMemory
Label101:
MOV dword[ParameterPool],EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label102
JMP NoMemory
Label102:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label103
JMP NoMemory
Label103:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_27+4]
PUSH dword[Number_27]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _ASC
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_TOLOWER],0
JNE Label104
stdcall LoadLibraryA,_TOLOWER_Lib
MOV dword[_TOLOWER_LibHandle],EAX
CMP EAX,0
JNE Label105
PUSH _TOLOWER_Lib
JMP NoLibrary
Label105:
stdcall GetProcAddress,dword[_TOLOWER_LibHandle],_TOLOWER_Alias
MOV dword[_TOLOWER],EAX
CMP EAX,0
JNE Label106
PUSH _TOLOWER_Alias
JMP NoFunction
Label106:
Label104:
CALL dword[_TOLOWER]
ADD ESP,4
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label107
JMP NoMemory
Label107:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_28+4]
PUSH dword[Number_28]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
JMP Label94
Label96:


MOV EAX,dword[EBP-4]
ADD ESP,12
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef UCASE____Used

global _UCASE___
_UCASE___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label108
JMP NoMemory
Label108:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4
SUB ESP,4

MOV EBX,String_29
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label109
JMP NoMemory
Label109:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_29
PUSH EBX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
PUSH dword[Number_30+4]
PUSH dword[Number_30]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label110
JMP NoMemory
Label110:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label111
JMP NoMemory
Label111:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label112
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label113
PUSH _LEN_Lib
JMP NoLibrary
Label113:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label114
PUSH _LEN_Alias
JMP NoFunction
Label114:
Label112:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_31+4]
PUSH dword[Number_31]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
Label115:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JB Label116
FLDZ
Label116:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label118
JMP Label117
Label118:
stdcall lstrlenA,dword[EBP-4]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label119
JMP NoMemory
Label119:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-4]
PUSH EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label120
JMP NoMemory
Label120:
MOV dword[ParameterPool],EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label121
JMP NoMemory
Label121:
MOV dword[ParameterPool],EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label122
JMP NoMemory
Label122:
MOV dword[ParameterPool],EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label123
JMP NoMemory
Label123:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label124
JMP NoMemory
Label124:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_32+4]
PUSH dword[Number_32]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _ASC
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_TOUPPER],0
JNE Label125
stdcall LoadLibraryA,_TOUPPER_Lib
MOV dword[_TOUPPER_LibHandle],EAX
CMP EAX,0
JNE Label126
PUSH _TOUPPER_Lib
JMP NoLibrary
Label126:
stdcall GetProcAddress,dword[_TOUPPER_LibHandle],_TOUPPER_Alias
MOV dword[_TOUPPER],EAX
CMP EAX,0
JNE Label127
PUSH _TOUPPER_Alias
JMP NoFunction
Label127:
Label125:
CALL dword[_TOUPPER]
ADD ESP,4
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label128
JMP NoMemory
Label128:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_33+4]
PUSH dword[Number_33]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
JMP Label115
Label117:


MOV EAX,dword[EBP-4]
ADD ESP,12
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef INSTR_Used

global _INSTR
_INSTR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
SUB ESP,4
SUB ESP,4
SUB ESP,4

FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_34+4]
PUSH dword[Number_34]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JB Label129
FLDZ
Label129:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label131
JMP Label130
Label131:
PUSH dword[Number_35+4]
PUSH dword[Number_35]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP+8]
JMP Label130
Label130:
Label132:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label133
JMP NoMemory
Label133:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+16]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label134
JMP NoMemory
Label134:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+16]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label135
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label136
PUSH _LEN_Lib
JMP NoLibrary
Label136:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label137
PUSH _LEN_Alias
JMP NoFunction
Label137:
Label135:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
PUSH dword[Number_36+4]
PUSH dword[Number_36]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label138
JMP NoMemory
Label138:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label139
JMP NoMemory
Label139:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label140
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label141
PUSH _LEN_Lib
JMP NoLibrary
Label141:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label142
PUSH _LEN_Alias
JMP NoFunction
Label142:
Label140:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-16]
Label143:
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-16]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JBE Label144
FLDZ
Label144:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_37+4]
PUSH dword[Number_37]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label145
FLDZ
Label145:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
POP dword[TempQWord1]
POP dword[TempQWord1+4]
JNE Label148
JMP Label147
Label148:
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label146
JMP Label147
Label146:
FINIT
FILD dword[True]
JMP Label149
Label147:
FLDZ
Label149:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label151
JMP Label150
Label151:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label152
JMP NoMemory
Label152:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label153
JMP NoMemory
Label153:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
stdcall lstrlenA,dword[EBP+16]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label154
JMP NoMemory
Label154:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+16]
PUSH EAX
POP EBX
POP EDI
stdcall lstrcmpA,EDI,EBX
MOV ESI,EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,EDI
FINIT
FILD dword[True]
CMP ESI,0
JE Label155
FLDZ
Label155:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label157
JMP Label156
Label157:
PUSH dword[Number_38+4]
PUSH dword[Number_38]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label156
Label156:
Label158:
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_39+4]
PUSH dword[Number_39]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP+8]
JMP Label143
Label150:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_40+4]
PUSH dword[Number_40]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label159
FLDZ
Label159:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label161
JMP Label160
Label161:
PUSH dword[Number_41+4]
PUSH dword[Number_41]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label160
Label160:
Label162:


MOV EAX,dword[EBP-4]
ADD ESP,16
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 12

%endif

%ifdef LEFT____Used

global _LEFT___
_LEFT___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label163
JMP NoMemory
Label163:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label164
JMP NoMemory
Label164:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label165
JMP NoMemory
Label165:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_42+4]
PUSH dword[Number_42]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef LTRIM____Used

global _LTRIM___
_LTRIM___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label166
JMP NoMemory
Label166:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4
SUB ESP,4
SUB ESP,4

PUSH dword[Number_43+4]
PUSH dword[Number_43]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label167
JMP NoMemory
Label167:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label168
JMP NoMemory
Label168:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label169
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label170
PUSH _LEN_Lib
JMP NoLibrary
Label170:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label171
PUSH _LEN_Alias
JMP NoFunction
Label171:
Label169:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[Number_44+4]
PUSH dword[Number_44]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-16]
Label172:
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JB Label173
FLDZ
Label173:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-16]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_45+4]
PUSH dword[Number_45]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label174
FLDZ
Label174:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
POP dword[TempQWord1]
POP dword[TempQWord1+4]
JNE Label177
JMP Label176
Label177:
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label175
JMP Label176
Label175:
FINIT
FILD dword[True]
JMP Label178
Label176:
FLDZ
Label178:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label180
JMP Label179
Label180:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label181
JMP NoMemory
Label181:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label182
JMP NoMemory
Label182:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_46+4]
PUSH dword[Number_46]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
MOV EBX,String_47
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label183
JMP NoMemory
Label183:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_47
PUSH EBX
POP EBX
POP EDI
stdcall lstrcmpA,EDI,EBX
MOV ESI,EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,EDI
FINIT
FILD dword[True]
CMP ESI,0
JNE Label184
FLDZ
Label184:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label186
JMP Label185
Label186:
PUSH dword[Number_48+4]
PUSH dword[Number_48]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-16]
JMP Label185
Label185:
Label187:
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_49+4]
PUSH dword[Number_49]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
JMP Label172
Label179:
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_50+4]
PUSH dword[Number_50]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label188
JMP NoMemory
Label188:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label189
JMP NoMemory
Label189:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label190
JMP NoMemory
Label190:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label191
JMP NoMemory
Label191:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label192
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label193
PUSH _LEN_Lib
JMP NoLibrary
Label193:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label194
PUSH _LEN_Alias
JMP NoFunction
Label194:
Label192:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_51+4]
PUSH dword[Number_51]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,16
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef REVERSE____Used

global _REVERSE___
_REVERSE___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label195
JMP NoMemory
Label195:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4
SUB ESP,4

PUSH dword[Number_52+4]
PUSH dword[Number_52]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label196
JMP NoMemory
Label196:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label197
JMP NoMemory
Label197:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label198
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label199
PUSH _LEN_Lib
JMP NoLibrary
Label199:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label200
PUSH _LEN_Alias
JMP NoFunction
Label200:
Label198:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
Label201:
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JBE Label202
FLDZ
Label202:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label204
JMP Label203
Label204:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label205
JMP NoMemory
Label205:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label206
JMP NoMemory
Label206:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_53+4]
PUSH dword[Number_53]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
stdcall lstrlenA,dword[EBP-4]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label207
JMP NoMemory
Label207:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-4]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label208
JMP NoMemory
Label208:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_54+4]
PUSH dword[Number_54]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
JMP Label201
Label203:


MOV EAX,dword[EBP-4]
ADD ESP,12
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef RIGHT____Used

global _RIGHT___
_RIGHT___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label209
JMP NoMemory
Label209:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label210
JMP NoMemory
Label210:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label211
JMP NoMemory
Label211:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label212
JMP NoMemory
Label212:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label213
JMP NoMemory
Label213:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label214
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label215
PUSH _LEN_Lib
JMP NoLibrary
Label215:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label216
PUSH _LEN_Alias
JMP NoFunction
Label216:
Label214:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef INSTRREV_Used

global _INSTRREV
_INSTRREV:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
SUB ESP,4
SUB ESP,4
SUB ESP,4

FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_55+4]
PUSH dword[Number_55]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label217
FLDZ
Label217:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label219
JMP Label218
Label219:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label221
JMP NoMemory
Label221:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label222
JMP NoMemory
Label222:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label223
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label224
PUSH _LEN_Lib
JMP NoLibrary
Label224:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label225
PUSH _LEN_Alias
JMP NoFunction
Label225:
Label223:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label226
JMP NoMemory
Label226:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+16]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label227
JMP NoMemory
Label227:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+16]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label228
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label229
PUSH _LEN_Lib
JMP NoLibrary
Label229:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label230
PUSH _LEN_Alias
JMP NoFunction
Label230:
Label228:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP+8]
JMP Label218
Label218:
Label220:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label231
JMP NoMemory
Label231:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+16]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label232
JMP NoMemory
Label232:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+16]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label233
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label234
PUSH _LEN_Lib
JMP NoLibrary
Label234:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label235
PUSH _LEN_Alias
JMP NoFunction
Label235:
Label233:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
PUSH dword[Number_56+4]
PUSH dword[Number_56]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
Label236:
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_57+4]
PUSH dword[Number_57]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JAE Label237
FLDZ
Label237:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_58+4]
PUSH dword[Number_58]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label238
FLDZ
Label238:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
POP dword[TempQWord1]
POP dword[TempQWord1+4]
JNE Label241
JMP Label240
Label241:
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label239
JMP Label240
Label239:
FINIT
FILD dword[True]
JMP Label242
Label240:
FLDZ
Label242:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label244
JMP Label243
Label244:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label245
JMP NoMemory
Label245:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label246
JMP NoMemory
Label246:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
stdcall lstrlenA,dword[EBP+16]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label247
JMP NoMemory
Label247:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+16]
PUSH EAX
POP EBX
POP EDI
stdcall lstrcmpA,EDI,EBX
MOV ESI,EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,EDI
FINIT
FILD dword[True]
CMP ESI,0
JE Label248
FLDZ
Label248:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label250
JMP Label249
Label250:
PUSH dword[Number_59+4]
PUSH dword[Number_59]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label249
Label249:
Label251:
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_60+4]
PUSH dword[Number_60]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP+8]
JMP Label236
Label243:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_61+4]
PUSH dword[Number_61]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label252
FLDZ
Label252:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label254
JMP Label253
Label254:
PUSH dword[Number_62+4]
PUSH dword[Number_62]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label253
Label253:
Label255:


MOV EAX,dword[EBP-4]
ADD ESP,16
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 12

%endif

%ifdef RTRIM____Used

global _RTRIM___
_RTRIM___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label256
JMP NoMemory
Label256:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4
SUB ESP,4

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label257
JMP NoMemory
Label257:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label258
JMP NoMemory
Label258:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label259
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label260
PUSH _LEN_Lib
JMP NoLibrary
Label260:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label261
PUSH _LEN_Alias
JMP NoFunction
Label261:
Label259:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[Number_63+4]
PUSH dword[Number_63]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
Label262:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_64+4]
PUSH dword[Number_64]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JAE Label263
FLDZ
Label263:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_65+4]
PUSH dword[Number_65]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label264
FLDZ
Label264:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
POP dword[TempQWord1]
POP dword[TempQWord1+4]
JNE Label267
JMP Label266
Label267:
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label265
JMP Label266
Label265:
FINIT
FILD dword[True]
JMP Label268
Label266:
FLDZ
Label268:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label270
JMP Label269
Label270:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label271
JMP NoMemory
Label271:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label272
JMP NoMemory
Label272:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_66+4]
PUSH dword[Number_66]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
MOV EBX,String_67
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label273
JMP NoMemory
Label273:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_67
PUSH EBX
POP EBX
POP EDI
stdcall lstrcmpA,EDI,EBX
MOV ESI,EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,EDI
FINIT
FILD dword[True]
CMP ESI,0
JNE Label274
FLDZ
Label274:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label276
JMP Label275
Label276:
PUSH dword[Number_68+4]
PUSH dword[Number_68]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
JMP Label275
Label275:
Label277:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_69+4]
PUSH dword[Number_69]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
JMP Label262
Label269:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_70+4]
PUSH dword[Number_70]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label278
JMP NoMemory
Label278:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label279
JMP NoMemory
Label279:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_71+4]
PUSH dword[Number_71]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,12
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef STRING____Used

global _STRING___
_STRING___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label280
JMP NoMemory
Label280:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4

PUSH dword[Number_72+4]
PUSH dword[Number_72]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
Label281:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JBE Label282
FLDZ
Label282:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label284
JMP Label283
Label284:
stdcall lstrlenA,dword[EBP-4]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label285
JMP NoMemory
Label285:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-4]
PUSH EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label286
JMP NoMemory
Label286:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label287
JMP NoMemory
Label287:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_73+4]
PUSH dword[Number_73]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_74+4]
PUSH dword[Number_74]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _MID___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label288
JMP NoMemory
Label288:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_75+4]
PUSH dword[Number_75]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
JMP Label281
Label283:


MOV EAX,dword[EBP-4]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef TALLY_Used

global _TALLY
_TALLY:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
SUB ESP,4
SUB ESP,4

PUSH dword[Number_76+4]
PUSH dword[Number_76]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[Number_77+4]
PUSH dword[Number_77]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
Label289:
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_78+4]
PUSH dword[Number_78]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label290
FLDZ
Label290:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label292
JMP Label291
Label292:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label293
JMP NoMemory
Label293:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label294
JMP NoMemory
Label294:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label295
JMP NoMemory
Label295:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _INSTR
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_79+4]
PUSH dword[Number_79]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label296
FLDZ
Label296:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label298
JMP Label297
Label298:
PUSH dword[Number_80+4]
PUSH dword[Number_80]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
JMP Label297
Label297:
Label299:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_81+4]
PUSH dword[Number_81]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
FINIT
FILD dword[EBP-4]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_82+4]
PUSH dword[Number_82]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
JMP Label289
Label291:
FINIT
FILD dword[EBP-4]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]


MOV EAX,dword[EBP-4]
ADD ESP,12
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef CRLF_Used

global _CRLF
_CRLF:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label300
JMP NoMemory
Label300:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label301
JMP NoMemory
Label301:
MOV dword[ParameterPool],EAX
PUSH dword[Number_83+4]
PUSH dword[Number_83]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label302
JMP NoMemory
Label302:
MOV dword[ParameterPool],EAX
PUSH dword[Number_84+4]
PUSH dword[Number_84]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label303
JMP NoMemory
Label303:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0

%endif

%ifdef VBCRLF_Used

global _VBCRLF
_VBCRLF:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label304
JMP NoMemory
Label304:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label305
JMP NoMemory
Label305:
MOV dword[ParameterPool],EAX
PUSH dword[Number_85+4]
PUSH dword[Number_85]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0

%endif

%ifdef VBCR_Used

global _VBCR
_VBCR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label306
JMP NoMemory
Label306:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label307
JMP NoMemory
Label307:
MOV dword[ParameterPool],EAX
PUSH dword[Number_86+4]
PUSH dword[Number_86]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0

%endif

%ifdef VBLF_Used

global _VBLF
_VBLF:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label308
JMP NoMemory
Label308:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label309
JMP NoMemory
Label309:
MOV dword[ParameterPool],EAX
PUSH dword[Number_87+4]
PUSH dword[Number_87]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0

%endif

%ifdef VBTAB_Used

global _VBTAB
_VBTAB:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label310
JMP NoMemory
Label310:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label311
JMP NoMemory
Label311:
MOV dword[ParameterPool],EAX
PUSH dword[Number_88+4]
PUSH dword[Number_88]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0

%endif

%ifdef REPLACE_Used

global _REPLACE
_REPLACE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label312
JMP NoMemory
Label312:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label313
JMP NoMemory
Label313:
MOV dword[EBP-12],EAX
MOV byte[EAX],0

stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label314
JMP NoMemory
Label314:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-12]
MOV dword[EBP-12],EBX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label315
JMP NoMemory
Label315:
MOV dword[ParameterPool],EAX
PUSH dword[Number_89+4]
PUSH dword[Number_89]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
stdcall lstrlenA,dword[EBP-12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label316
JMP NoMemory
Label316:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label317
JMP NoMemory
Label317:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _INSTR
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
Label318:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_90+4]
PUSH dword[Number_90]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JA Label319
FLDZ
Label319:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label321
JMP Label320
Label321:
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label322
JMP NoMemory
Label322:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP-12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label323
JMP NoMemory
Label323:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_91+4]
PUSH dword[Number_91]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _LEFT___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
stdcall lstrlenA,dword[EBP+16]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label324
JMP NoMemory
Label324:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+16]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label325
JMP NoMemory
Label325:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label326
JMP NoMemory
Label326:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP-12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label327
JMP NoMemory
Label327:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label328
JMP NoMemory
Label328:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label329
JMP NoMemory
Label329:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LEN],0
JNE Label330
stdcall LoadLibraryA,_LEN_Lib
MOV dword[_LEN_LibHandle],EAX
CMP EAX,0
JNE Label331
PUSH _LEN_Lib
JMP NoLibrary
Label331:
stdcall GetProcAddress,dword[_LEN_LibHandle],_LEN_Alias
MOV dword[_LEN],EAX
CMP EAX,0
JNE Label332
PUSH _LEN_Alias
JMP NoFunction
Label332:
Label330:
CALL dword[_LEN]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _RIGHT___
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label333
JMP NoMemory
Label333:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-12]
MOV dword[EBP-12],EBX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label334
JMP NoMemory
Label334:
MOV dword[ParameterPool],EAX
PUSH dword[Number_92+4]
PUSH dword[Number_92]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
stdcall lstrlenA,dword[EBP-12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label335
JMP NoMemory
Label335:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label336
JMP NoMemory
Label336:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _INSTR
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
JMP Label318
Label320:
stdcall lstrlenA,dword[EBP-12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label337
JMP NoMemory
Label337:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-12]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,12
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 12

%endif







%ifdef INTTOSTR_Used

global _INTTOSTR
_INTTOSTR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label338
JMP NoMemory
Label338:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

extern sprintf
jmp forward
strFormat db '%ld',0
forward:
stdcall HeapAlloc,dword[HandleToHeap],8,11 
mov ebx,eax 
ccall sprintf,eax,strFormat,dword[ebp+8]
mov dword[ebp-4],ebx


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef STR____Used

global _STR___
_STR___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label339
JMP NoMemory
Label339:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4

PUSH dword[Number_93+4]
PUSH dword[Number_93]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label340
JMP NoMemory
Label340:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_94+4]
PUSH dword[Number_94]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _SPACE___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label341
JMP NoMemory
Label341:
MOV dword[ParameterPool],EAX
PUSH dword[EBP+8+4]
PUSH dword[EBP+8]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
POP dword[EAX+0+4]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[EBP-4]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0+4]
PUSH dword[EAX+0]
CMP dword[_GCVT],0
JNE Label342
stdcall LoadLibraryA,_GCVT_Lib
MOV dword[_GCVT_LibHandle],EAX
CMP EAX,0
JNE Label343
PUSH _GCVT_Lib
JMP NoLibrary
Label343:
stdcall GetProcAddress,dword[_GCVT_LibHandle],_GCVT_Alias
MOV dword[_GCVT],EAX
CMP EAX,0
JNE Label344
PUSH _GCVT_Alias
JMP NoFunction
Label344:
Label342:
CALL dword[_GCVT]
ADD ESP,16
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


MOV EAX,dword[EBP-4]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef STRI_Used

global _STRI
_STRI:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label345
JMP NoMemory
Label345:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

extern sprintf
stdcall HeapAlloc,dword[HandleToHeap],8,11 
mov ebx,eax 
invoke sprintf,eax,"%ld",dword[ebp+8]
mov dword[ebp-4],ebx


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef STRD_Used

global _STRD
_STRD:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label346
JMP NoMemory
Label346:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

extern sprintf
stdcall HeapAlloc,dword[HandleToHeap],8,128 
mov ebx,eax 
invoke sprintf,eax,"%f",dword[ebp+8],dword[ebp+12]
mov dword[ebp-4],ebx


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif










%ifdef COLOR_Used

global _COLOR
_COLOR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label347
JMP NoMemory
Label347:
MOV dword[ParameterPool],EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label348
JMP NoMemory
Label348:
MOV dword[ParameterPool],EAX
PUSH dword[Number_95+4]
PUSH dword[Number_95]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_GETSTDHANDLE],0
JNE Label349
stdcall LoadLibraryA,_GETSTDHANDLE_Lib
MOV dword[_GETSTDHANDLE_LibHandle],EAX
CMP EAX,0
JNE Label350
PUSH _GETSTDHANDLE_Lib
JMP NoLibrary
Label350:
stdcall GetProcAddress,dword[_GETSTDHANDLE_LibHandle],_GETSTDHANDLE_Alias
MOV dword[_GETSTDHANDLE],EAX
CMP EAX,0
JNE Label351
PUSH _GETSTDHANDLE_Alias
JMP NoFunction
Label351:
Label349:
CALL dword[_GETSTDHANDLE]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label352
JMP NoMemory
Label352:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _OR
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_SETCONSOLETEXTATTRIBUTE],0
JNE Label353
stdcall LoadLibraryA,_SETCONSOLETEXTATTRIBUTE_Lib
MOV dword[_SETCONSOLETEXTATTRIBUTE_LibHandle],EAX
CMP EAX,0
JNE Label354
PUSH _SETCONSOLETEXTATTRIBUTE_Lib
JMP NoLibrary
Label354:
stdcall GetProcAddress,dword[_SETCONSOLETEXTATTRIBUTE_LibHandle],_SETCONSOLETEXTATTRIBUTE_Alias
MOV dword[_SETCONSOLETEXTATTRIBUTE],EAX
CMP EAX,0
JNE Label355
PUSH _SETCONSOLETEXTATTRIBUTE_Alias
JMP NoFunction
Label355:
Label353:
CALL dword[_SETCONSOLETEXTATTRIBUTE]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef CSLRIN_Used

global _CSLRIN
_CSLRIN:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4



MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0

%endif

%ifdef GET____Used

global _GET___
_GET___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label356
JMP NoMemory
Label356:
MOV dword[EBP-4],EAX
MOV byte[EAX],0



MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef INKEY____Used

global _INKEY___
_INKEY___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label357
JMP NoMemory
Label357:
MOV dword[EBP-4],EAX
MOV byte[EAX],0



MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0

%endif

%ifdef LOCATE_Used

global _LOCATE
_LOCATE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

extern SetConsoleCursorPosition
extern GetStdHandle
MOV AX,word[EBP+12]
PUSH AX
MOV AX,word[EBP+8]
PUSH AX
stdcall GetStdHandle,-11
PUSH EAX
CALL SetConsoleCursorPosition


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef POS_Used

global _POS
_POS:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4



MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0

%endif

%ifdef GETCONSOLEREADHANDLE_Used

global _GETCONSOLEREADHANDLE
_GETCONSOLEREADHANDLE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label358
JMP NoMemory
Label358:
MOV dword[ParameterPool],EAX
PUSH dword[Number_96+4]
PUSH dword[Number_96]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_GETSTDHANDLE],0
JNE Label359
stdcall LoadLibraryA,_GETSTDHANDLE_Lib
MOV dword[_GETSTDHANDLE_LibHandle],EAX
CMP EAX,0
JNE Label360
PUSH _GETSTDHANDLE_Lib
JMP NoLibrary
Label360:
stdcall GetProcAddress,dword[_GETSTDHANDLE_LibHandle],_GETSTDHANDLE_Alias
MOV dword[_GETSTDHANDLE],EAX
CMP EAX,0
JNE Label361
PUSH _GETSTDHANDLE_Alias
JMP NoFunction
Label361:
Label359:
CALL dword[_GETSTDHANDLE]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0

%endif

%ifdef GETCONSOLEWRITEHANDLE_Used

global _GETCONSOLEWRITEHANDLE
_GETCONSOLEWRITEHANDLE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label362
JMP NoMemory
Label362:
MOV dword[ParameterPool],EAX
PUSH dword[Number_97+4]
PUSH dword[Number_97]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_GETSTDHANDLE],0
JNE Label363
stdcall LoadLibraryA,_GETSTDHANDLE_Lib
MOV dword[_GETSTDHANDLE_LibHandle],EAX
CMP EAX,0
JNE Label364
PUSH _GETSTDHANDLE_Lib
JMP NoLibrary
Label364:
stdcall GetProcAddress,dword[_GETSTDHANDLE_LibHandle],_GETSTDHANDLE_Alias
MOV dword[_GETSTDHANDLE],EAX
CMP EAX,0
JNE Label365
PUSH _GETSTDHANDLE_Alias
JMP NoFunction
Label365:
Label363:
CALL dword[_GETSTDHANDLE]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0

%endif

%ifdef SETCONSOLETITLE_Used

global _SETCONSOLETITLE
_SETCONSOLETITLE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label366
JMP NoMemory
Label366:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label367
JMP NoMemory
Label367:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_SETCONSOLETITLEWINAPI],0
JNE Label368
stdcall LoadLibraryA,_SETCONSOLETITLEWINAPI_Lib
MOV dword[_SETCONSOLETITLEWINAPI_LibHandle],EAX
CMP EAX,0
JNE Label369
PUSH _SETCONSOLETITLEWINAPI_Lib
JMP NoLibrary
Label369:
stdcall GetProcAddress,dword[_SETCONSOLETITLEWINAPI_LibHandle],_SETCONSOLETITLEWINAPI_Alias
MOV dword[_SETCONSOLETITLEWINAPI],EAX
CMP EAX,0
JNE Label370
PUSH _SETCONSOLETITLEWINAPI_Alias
JMP NoFunction
Label370:
Label368:
CALL dword[_SETCONSOLETITLEWINAPI]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif




%ifdef TIME____Used

global _TIME___
_TIME___:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label371
JMP NoMemory
Label371:
MOV dword[ParameterPool],EAX
PUSH Scope169__RETURNTIME_UDT
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_GETLOCALTIME],0
JNE Label372
stdcall LoadLibraryA,_GETLOCALTIME_Lib
MOV dword[_GETLOCALTIME_LibHandle],EAX
CMP EAX,0
JNE Label373
PUSH _GETLOCALTIME_Lib
JMP NoLibrary
Label373:
stdcall GetProcAddress,dword[_GETLOCALTIME_LibHandle],_GETLOCALTIME_Alias
MOV dword[_GETLOCALTIME],EAX
CMP EAX,0
JNE Label374
PUSH _GETLOCALTIME_Alias
JMP NoFunction
Label374:
Label372:
CALL dword[_GETLOCALTIME]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label378
JMP NoMemory
Label378:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label375:
CMP byte[EBX+ECX],0
JE Label376
INC ECX
CMP ECX,EDI
JL Label375
JMP Label377
Label376:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label377
MOV byte[EBX+ECX],0
Label377:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
FINIT
FILD dword[Scope169__RETURNTIME_UDT+Scope168__SYSTEMTIME_TYPE.Scope168__WMILLISECONDS_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label382
JMP NoMemory
Label382:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label379:
CMP byte[EBX+ECX],0
JE Label380
INC ECX
CMP ECX,EDI
JL Label379
JMP Label381
Label380:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label381
MOV byte[EBX+ECX],0
Label381:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0

%endif





































%ifdef FILEOPEN_Used

global _FILEOPEN
_FILEOPEN:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label383
JMP NoMemory
Label383:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label384
JMP NoMemory
Label384:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label385
JMP NoMemory
Label385:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EBX,String_98
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label386
JMP NoMemory
Label386:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_98
PUSH EBX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label387
JMP NoMemory
Label387:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FOPEN],0
JNE Label388
stdcall LoadLibraryA,_FOPEN_Lib
MOV dword[_FOPEN_LibHandle],EAX
CMP EAX,0
JNE Label389
PUSH _FOPEN_Lib
JMP NoLibrary
Label389:
stdcall GetProcAddress,dword[_FOPEN_LibHandle],_FOPEN_Alias
MOV dword[_FOPEN],EAX
CMP EAX,0
JNE Label390
PUSH _FOPEN_Alias
JMP NoFunction
Label390:
Label388:
CALL dword[_FOPEN]
ADD ESP,8
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef FILEWRITESTR_Used

global _FILEWRITESTR
_FILEWRITESTR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label391
JMP NoMemory
Label391:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label392
JMP NoMemory
Label392:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FPUTS],0
JNE Label393
stdcall LoadLibraryA,_FPUTS_Lib
MOV dword[_FPUTS_LibHandle],EAX
CMP EAX,0
JNE Label394
PUSH _FPUTS_Lib
JMP NoLibrary
Label394:
stdcall GetProcAddress,dword[_FPUTS_LibHandle],_FPUTS_Alias
MOV dword[_FPUTS],EAX
CMP EAX,0
JNE Label395
PUSH _FPUTS_Alias
JMP NoFunction
Label395:
Label393:
CALL dword[_FPUTS]
ADD ESP,8
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef FILEREADSTR_Used

global _FILEREADSTR
_FILEREADSTR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label396
JMP NoMemory
Label396:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label397
JMP NoMemory
Label397:
MOV dword[EBP-8],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label398
JMP NoMemory
Label398:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_99+4]
PUSH dword[Number_99]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _SPACE___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-8]
MOV dword[EBP-8],EBX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label399
JMP NoMemory
Label399:
MOV dword[ParameterPool],EAX
PUSH dword[EBP-8]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_100+4]
PUSH dword[Number_100]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FREAD],0
JNE Label400
stdcall LoadLibraryA,_FREAD_Lib
MOV dword[_FREAD_LibHandle],EAX
CMP EAX,0
JNE Label401
PUSH _FREAD_Lib
JMP NoLibrary
Label401:
stdcall GetProcAddress,dword[_FREAD_LibHandle],_FREAD_Alias
MOV dword[_FREAD],EAX
CMP EAX,0
JNE Label402
PUSH _FREAD_Alias
JMP NoFunction
Label402:
Label400:
CALL dword[_FREAD]
ADD ESP,16
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
stdcall lstrlenA,dword[EBP-8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label403
JMP NoMemory
Label403:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-8]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef FILEWRITELINE_Used

global _FILEWRITELINE
_FILEWRITELINE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,8
CMP EAX,0
JNE Label404
JMP NoMemory
Label404:
MOV dword[ParameterPool],EAX
stdcall lstrlenA,dword[EBP+12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label405
JMP NoMemory
Label405:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+12]
PUSH EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label406
JMP NoMemory
Label406:
MOV dword[ParameterPool],EAX
PUSH dword[Number_101+4]
PUSH dword[Number_101]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label407
JMP NoMemory
Label407:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label408
JMP NoMemory
Label408:
MOV dword[ParameterPool],EAX
PUSH dword[Number_102+4]
PUSH dword[Number_102]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _CHR___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label409
JMP NoMemory
Label409:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FPUTS],0
JNE Label410
stdcall LoadLibraryA,_FPUTS_Lib
MOV dword[_FPUTS_LibHandle],EAX
CMP EAX,0
JNE Label411
PUSH _FPUTS_Lib
JMP NoLibrary
Label411:
stdcall GetProcAddress,dword[_FPUTS_LibHandle],_FPUTS_Alias
MOV dword[_FPUTS],EAX
CMP EAX,0
JNE Label412
PUSH _FPUTS_Alias
JMP NoFunction
Label412:
Label410:
CALL dword[_FPUTS]
ADD ESP,8
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef FILEGETSIZE_Used

global _FILEGETSIZE
_FILEGETSIZE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
SUB ESP,4

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label413
JMP NoMemory
Label413:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FTELL],0
JNE Label414
stdcall LoadLibraryA,_FTELL_Lib
MOV dword[_FTELL_LibHandle],EAX
CMP EAX,0
JNE Label415
PUSH _FTELL_Lib
JMP NoLibrary
Label415:
stdcall GetProcAddress,dword[_FTELL_LibHandle],_FTELL_Alias
MOV dword[_FTELL],EAX
CMP EAX,0
JNE Label416
PUSH _FTELL_Alias
JMP NoFunction
Label416:
Label414:
CALL dword[_FTELL]
ADD ESP,4
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label417
JMP NoMemory
Label417:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_103+4]
PUSH dword[Number_103]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_104+4]
PUSH dword[Number_104]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FSEEK],0
JNE Label418
stdcall LoadLibraryA,_FSEEK_Lib
MOV dword[_FSEEK_LibHandle],EAX
CMP EAX,0
JNE Label419
PUSH _FSEEK_Lib
JMP NoLibrary
Label419:
stdcall GetProcAddress,dword[_FSEEK_LibHandle],_FSEEK_Alias
MOV dword[_FSEEK],EAX
CMP EAX,0
JNE Label420
PUSH _FSEEK_Alias
JMP NoFunction
Label420:
Label418:
CALL dword[_FSEEK]
ADD ESP,12
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label421
JMP NoMemory
Label421:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FTELL],0
JNE Label422
stdcall LoadLibraryA,_FTELL_Lib
MOV dword[_FTELL_LibHandle],EAX
CMP EAX,0
JNE Label423
PUSH _FTELL_Lib
JMP NoLibrary
Label423:
stdcall GetProcAddress,dword[_FTELL_LibHandle],_FTELL_Alias
MOV dword[_FTELL],EAX
CMP EAX,0
JNE Label424
PUSH _FTELL_Alias
JMP NoFunction
Label424:
Label422:
CALL dword[_FTELL]
ADD ESP,4
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label425
JMP NoMemory
Label425:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_105+4]
PUSH dword[Number_105]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FSEEK],0
JNE Label426
stdcall LoadLibraryA,_FSEEK_Lib
MOV dword[_FSEEK_LibHandle],EAX
CMP EAX,0
JNE Label427
PUSH _FSEEK_Lib
JMP NoLibrary
Label427:
stdcall GetProcAddress,dword[_FSEEK_LibHandle],_FSEEK_Alias
MOV dword[_FSEEK],EAX
CMP EAX,0
JNE Label428
PUSH _FSEEK_Alias
JMP NoFunction
Label428:
Label426:
CALL dword[_FSEEK]
ADD ESP,12
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


MOV EAX,dword[EBP-4]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef FILEREADLINE_Used

global _FILEREADLINE
_FILEREADLINE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label429
JMP NoMemory
Label429:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label430
JMP NoMemory
Label430:
MOV dword[EBP-8],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label431
JMP NoMemory
Label431:
MOV dword[ParameterPool],EAX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label432
JMP NoMemory
Label432:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _FILEGETSIZE
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_106+4]
PUSH dword[Number_106]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _SPACE___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-8]
MOV dword[EBP-8],EBX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label433
JMP NoMemory
Label433:
MOV dword[ParameterPool],EAX
PUSH dword[EBP-8]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label434
JMP NoMemory
Label434:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _FILEGETSIZE
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_107+4]
PUSH dword[Number_107]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FGETS],0
JNE Label435
stdcall LoadLibraryA,_FGETS_Lib
MOV dword[_FGETS_LibHandle],EAX
CMP EAX,0
JNE Label436
PUSH _FGETS_Lib
JMP NoLibrary
Label436:
stdcall GetProcAddress,dword[_FGETS_LibHandle],_FGETS_Alias
MOV dword[_FGETS],EAX
CMP EAX,0
JNE Label437
PUSH _FGETS_Alias
JMP NoFunction
Label437:
Label435:
CALL dword[_FGETS]
ADD ESP,12
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
stdcall lstrlenA,dword[EBP-8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label438
JMP NoMemory
Label438:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-8]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX


MOV EAX,dword[EBP-4]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef FILEWRITEINT_Used

global _FILEWRITEINT
_FILEWRITEINT:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label439
JMP NoMemory
Label439:
MOV dword[ParameterPool],EAX
MOV EAX,EBP
ADD EAX,12
PUSH EAX
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_108+4]
PUSH dword[Number_108]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_109+4]
PUSH dword[Number_109]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FWRITE2],0
JNE Label440
stdcall LoadLibraryA,_FWRITE2_Lib
MOV dword[_FWRITE2_LibHandle],EAX
CMP EAX,0
JNE Label441
PUSH _FWRITE2_Lib
JMP NoLibrary
Label441:
stdcall GetProcAddress,dword[_FWRITE2_LibHandle],_FWRITE2_Alias
MOV dword[_FWRITE2],EAX
CMP EAX,0
JNE Label442
PUSH _FWRITE2_Alias
JMP NoFunction
Label442:
Label440:
CALL dword[_FWRITE2]
ADD ESP,16
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef FILEREADINT_Used

global _FILEREADINT
_FILEREADINT:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label443
JMP NoMemory
Label443:
MOV dword[ParameterPool],EAX
MOV EAX,EBP
SUB EAX,4
PUSH EAX
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_110+4]
PUSH dword[Number_110]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_111+4]
PUSH dword[Number_111]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FREAD2],0
JNE Label444
stdcall LoadLibraryA,_FREAD2_Lib
MOV dword[_FREAD2_LibHandle],EAX
CMP EAX,0
JNE Label445
PUSH _FREAD2_Lib
JMP NoLibrary
Label445:
stdcall GetProcAddress,dword[_FREAD2_LibHandle],_FREAD2_Alias
MOV dword[_FREAD2],EAX
CMP EAX,0
JNE Label446
PUSH _FREAD2_Alias
JMP NoFunction
Label446:
Label444:
CALL dword[_FREAD2]
ADD ESP,16
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef FILEWRITEDOUBLE_Used

global _FILEWRITEDOUBLE
_FILEWRITEDOUBLE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label447
JMP NoMemory
Label447:
MOV dword[ParameterPool],EAX
MOV EAX,EBP
ADD EAX,12
PUSH EAX
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_112+4]
PUSH dword[Number_112]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_113+4]
PUSH dword[Number_113]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FWRITE2],0
JNE Label448
stdcall LoadLibraryA,_FWRITE2_Lib
MOV dword[_FWRITE2_LibHandle],EAX
CMP EAX,0
JNE Label449
PUSH _FWRITE2_Lib
JMP NoLibrary
Label449:
stdcall GetProcAddress,dword[_FWRITE2_LibHandle],_FWRITE2_Alias
MOV dword[_FWRITE2],EAX
CMP EAX,0
JNE Label450
PUSH _FWRITE2_Alias
JMP NoFunction
Label450:
Label448:
CALL dword[_FWRITE2]
ADD ESP,16
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 12

%endif

%ifdef FILEREADDOUBLE_Used

global _FILEREADDOUBLE
_FILEREADDOUBLE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,8
MOV dword[ESP+8],0
MOV dword[ESP+4],0
MOV dword[EBP-4],0
MOV dword[EBP-8],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label451
JMP NoMemory
Label451:
MOV dword[ParameterPool],EAX
MOV EAX,EBP
SUB EAX,8
PUSH EAX
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_114+4]
PUSH dword[Number_114]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_115+4]
PUSH dword[Number_115]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FREAD2],0
JNE Label452
stdcall LoadLibraryA,_FREAD2_Lib
MOV dword[_FREAD2_LibHandle],EAX
CMP EAX,0
JNE Label453
PUSH _FREAD2_Lib
JMP NoLibrary
Label453:
stdcall GetProcAddress,dword[_FREAD2_LibHandle],_FREAD2_Alias
MOV dword[_FREAD2],EAX
CMP EAX,0
JNE Label454
PUSH _FREAD2_Alias
JMP NoFunction
Label454:
Label452:
CALL dword[_FREAD2]
ADD ESP,16
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


FINIT
FLD qword[EBP-8]
ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef FILESETPOS_Used

global _FILESETPOS
_FILESETPOS:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label455
JMP NoMemory
Label455:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_116+4]
PUSH dword[Number_116]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FSEEK],0
JNE Label456
stdcall LoadLibraryA,_FSEEK_Lib
MOV dword[_FSEEK_LibHandle],EAX
CMP EAX,0
JNE Label457
PUSH _FSEEK_Lib
JMP NoLibrary
Label457:
stdcall GetProcAddress,dword[_FSEEK_LibHandle],_FSEEK_Alias
MOV dword[_FSEEK],EAX
CMP EAX,0
JNE Label458
PUSH _FSEEK_Alias
JMP NoFunction
Label458:
Label456:
CALL dword[_FSEEK]
ADD ESP,12
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 8

%endif

%ifdef FILEGETPOS_Used

global _FILEGETPOS
_FILEGETPOS:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label459
JMP NoMemory
Label459:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FTELL],0
JNE Label460
stdcall LoadLibraryA,_FTELL_Lib
MOV dword[_FTELL_LibHandle],EAX
CMP EAX,0
JNE Label461
PUSH _FTELL_Lib
JMP NoLibrary
Label461:
stdcall GetProcAddress,dword[_FTELL_LibHandle],_FTELL_Alias
MOV dword[_FTELL],EAX
CMP EAX,0
JNE Label462
PUSH _FTELL_Alias
JMP NoFunction
Label462:
Label460:
CALL dword[_FTELL]
ADD ESP,4
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-4]


MOV EAX,dword[EBP-4]
ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif

%ifdef FILECLOSE_Used

global _FILECLOSE
_FILECLOSE:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label463
JMP NoMemory
Label463:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FCLOSE],0
JNE Label464
stdcall LoadLibraryA,_FCLOSE_Lib
MOV dword[_FCLOSE_LibHandle],EAX
CMP EAX,0
JNE Label465
PUSH _FCLOSE_Lib
JMP NoLibrary
Label465:
stdcall GetProcAddress,dword[_FCLOSE_LibHandle],_FCLOSE_Alias
MOV dword[_FCLOSE],EAX
CMP EAX,0
JNE Label466
PUSH _FCLOSE_Alias
JMP NoFunction
Label466:
Label464:
CALL dword[_FCLOSE]
ADD ESP,4
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 4

%endif




%ifdef SHOWINT_Used

global _SHOWINT
_SHOWINT:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label467
JMP NoMemory
Label467:
MOV dword[EBP-4],EAX
MOV byte[EAX],0

PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label468
JMP NoMemory
Label468:
MOV dword[ParameterPool],EAX
PUSH dword[Number_117+4]
PUSH dword[Number_117]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _SPACE___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label469
JMP NoMemory
Label469:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP+12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[EBP-4]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP+16]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LTOA],0
JNE Label470
stdcall LoadLibraryA,_LTOA_Lib
MOV dword[_LTOA_LibHandle],EAX
CMP EAX,0
JNE Label471
PUSH _LTOA_Lib
JMP NoLibrary
Label471:
stdcall GetProcAddress,dword[_LTOA_LibHandle],_LTOA_Alias
MOV dword[_LTOA],EAX
CMP EAX,0
JNE Label472
PUSH _LTOA_Alias
JMP NoFunction
Label472:
Label470:
CALL dword[_LTOA]
ADD ESP,12
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label473
JMP NoMemory
Label473:
MOV dword[ParameterPool],EAX
PUSH dword[Number_118+4]
PUSH dword[Number_118]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
stdcall lstrlenA,dword[EBP-4]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label474
JMP NoMemory
Label474:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-4]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
stdcall lstrlenA,dword[EBP+8]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label475
JMP NoMemory
Label475:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP+8]
PUSH EAX
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Number_119+4]
PUSH dword[Number_119]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_MESSAGEBOX],0
JNE Label476
stdcall LoadLibraryA,_MESSAGEBOX_Lib
MOV dword[_MESSAGEBOX_LibHandle],EAX
CMP EAX,0
JNE Label477
PUSH _MESSAGEBOX_Lib
JMP NoLibrary
Label477:
stdcall GetProcAddress,dword[_MESSAGEBOX_LibHandle],_MESSAGEBOX_Alias
MOV dword[_MESSAGEBOX],EAX
CMP EAX,0
JNE Label478
PUSH _MESSAGEBOX_Alias
JMP NoFunction
Label478:
Label476:
CALL dword[_MESSAGEBOX]
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
POP ECX
stdcall HeapFree,dword[HandleToHeap],0,ECX
POP EAX
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


ADD ESP,4
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 12

%endif




%ifdef SHOWLASTERROR_Used

global _SHOWLASTERROR
_SHOWLASTERROR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
SUB ESP,4
SUB ESP,4
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label479
JMP NoMemory
Label479:
MOV dword[EBP-16],EAX
MOV byte[EAX],0
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label480
JMP NoMemory
Label480:
MOV dword[EBP-20],EAX
MOV byte[EAX],0

CMP dword[_GETLASTERROR],0
JNE Label481
stdcall LoadLibraryA,_GETLASTERROR_Lib
MOV dword[_GETLASTERROR_LibHandle],EAX
CMP EAX,0
JNE Label482
PUSH _GETLASTERROR_Lib
JMP NoLibrary
Label482:
stdcall GetProcAddress,dword[_GETLASTERROR_LibHandle],_GETLASTERROR_Alias
MOV dword[_GETLASTERROR],EAX
CMP EAX,0
JNE Label483
PUSH _GETLASTERROR_Alias
JMP NoFunction
Label483:
Label481:
CALL dword[_GETLASTERROR]
MOV dword[TempQWord1],EAX
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-12]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label484
JMP NoMemory
Label484:
MOV dword[ParameterPool],EAX
PUSH dword[Number_120+4]
PUSH dword[Number_120]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CALL _SPACE___
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH EAX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-16]
MOV dword[EBP-16],EBX
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,12
CMP EAX,0
JNE Label485
JMP NoMemory
Label485:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[EBP-16]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[Number_121+4]
PUSH dword[Number_121]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LTOA],0
JNE Label486
stdcall LoadLibraryA,_LTOA_Lib
MOV dword[_LTOA_LibHandle],EAX
CMP EAX,0
JNE Label487
PUSH _LTOA_Lib
JMP NoLibrary
Label487:
stdcall GetProcAddress,dword[_LTOA_LibHandle],_LTOA_Alias
MOV dword[_LTOA],EAX
CMP EAX,0
JNE Label488
PUSH _LTOA_Alias
JMP NoFunction
Label488:
Label486:
CALL dword[_LTOA]
ADD ESP,12
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
MOV EBX,String_122
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label489
JMP NoMemory
Label489:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_122
PUSH EBX
stdcall lstrlenA,dword[EBP-16]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label490
JMP NoMemory
Label490:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-16]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label491
JMP NoMemory
Label491:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-20]
MOV dword[EBP-20],EBX
PUSH dword[Number_123+4]
PUSH dword[Number_123]
PUSH dword[Number_124+4]
PUSH dword[Number_124]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_125+4]
PUSH dword[Number_125]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,28
CMP EAX,0
JNE Label492
JMP NoMemory
Label492:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
PUSH dword[Number_126+4]
PUSH dword[Number_126]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
FINIT
FILD dword[EBP-12]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Number_127+4]
PUSH dword[Number_127]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,EBP
SUB EAX,4
PUSH EAX
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+16]
PUSH dword[Number_128+4]
PUSH dword[Number_128]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+20]
PUSH dword[Number_129+4]
PUSH dword[Number_129]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+24]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+24]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+20]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+16]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_FORMATMESSAGE],0
JNE Label493
stdcall LoadLibraryA,_FORMATMESSAGE_Lib
MOV dword[_FORMATMESSAGE_LibHandle],EAX
CMP EAX,0
JNE Label494
PUSH _FORMATMESSAGE_Lib
JMP NoLibrary
Label494:
stdcall GetProcAddress,dword[_FORMATMESSAGE_LibHandle],_FORMATMESSAGE_Alias
MOV dword[_FORMATMESSAGE],EAX
CMP EAX,0
JNE Label495
PUSH _FORMATMESSAGE_Alias
JMP NoFunction
Label495:
Label493:
CALL dword[_FORMATMESSAGE]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,16
CMP EAX,0
JNE Label496
JMP NoMemory
Label496:
MOV dword[ParameterPool],EAX
PUSH dword[Number_130+4]
PUSH dword[Number_130]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
FINIT
FILD dword[EBP-4]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+4]
PUSH dword[EBP-20]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+8]
PUSH dword[Number_131+4]
PUSH dword[Number_131]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+12]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+8]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+4]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_ERRORMSGBOX],0
JNE Label497
stdcall LoadLibraryA,_ERRORMSGBOX_Lib
MOV dword[_ERRORMSGBOX_LibHandle],EAX
CMP EAX,0
JNE Label498
PUSH _ERRORMSGBOX_Lib
JMP NoLibrary
Label498:
stdcall GetProcAddress,dword[_ERRORMSGBOX_LibHandle],_ERRORMSGBOX_Alias
MOV dword[_ERRORMSGBOX],EAX
CMP EAX,0
JNE Label499
PUSH _ERRORMSGBOX_Alias
JMP NoFunction
Label499:
Label497:
CALL dword[_ERRORMSGBOX]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]
PUSH dword[ParameterPool]
stdcall HeapAlloc,dword[HandleToHeap],8,4
CMP EAX,0
JNE Label500
JMP NoMemory
Label500:
MOV dword[ParameterPool],EAX
FINIT
FILD dword[EBP-4]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
MOV EAX,dword[ParameterPool]
POP dword[EAX+0]
MOV EAX,dword[ParameterPool]
PUSH dword[EAX+0]
CMP dword[_LOCALFREE],0
JNE Label501
stdcall LoadLibraryA,_LOCALFREE_Lib
MOV dword[_LOCALFREE_LibHandle],EAX
CMP EAX,0
JNE Label502
PUSH _LOCALFREE_Lib
JMP NoLibrary
Label502:
stdcall GetProcAddress,dword[_LOCALFREE_LibHandle],_LOCALFREE_Alias
MOV dword[_LOCALFREE],EAX
CMP EAX,0
JNE Label503
PUSH _LOCALFREE_Alias
JMP NoFunction
Label503:
Label501:
CALL dword[_LOCALFREE]
PUSH EAX
MOV EAX,dword[ParameterPool]
stdcall HeapFree,dword[HandleToHeap],0,EAX
POP EAX
POP dword[ParameterPool]


ADD ESP,20
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0

%endif



;Data section of the Chameleon app
section .data
NoMemMessage db "Could not allocate memory.",0
NoLibFound db "Could not find library: ",0
NoFunctionFound db "Cound not find function: ",0
Error db "Error!",0
ParameterPool dd 0
HandleToHeap dd 0
TempQWord1 dq 0.0
TempQWord2 dq 0.0
True dd -1
Internal_HInstance dd 0
Internal_CommandLine dd 0
NoConsoleMessage db 'Error - Cannot access the console handles for Input/Output.',0
ConsoleTemp dd 0
ConsoleNewLine db 10,0
ConsoleClear db 'CLS',0
ConsolePause db 'PAUSE',0
HandleToInput dd 0
HandleToOutput dd 0
STRUC Scope0__POINT_TYPE
.Scope0__X_Integer resd 1
.Scope0__Y_Integer resd 1
ENDSTRUC
STRUC Scope0__RECT_TYPE
.Scope0__LEFT_Integer resd 1
.Scope0__TOP_Integer resd 1
.Scope0__RIGHT_Integer resd 1
.Scope0__BOTTOM_Integer resd 1
ENDSTRUC
STRUC Scope0__SIZE_TYPE
.Scope0__CX_Integer resd 1
.Scope0__CY_Integer resd 1
ENDSTRUC
STRUC Scope0__STARTUPINFO_TYPE
.Scope0__CB_Integer resd 1
.Scope0__LPRESERVED_String resd 1
.Scope0__LPDESKTOP_String resd 1
.Scope0__LPTITLE_String resd 1
.Scope0__DWX_Integer resd 1
.Scope0__DWY_Integer resd 1
.Scope0__DWXSIZE_Integer resd 1
.Scope0__DWYSIZE_Integer resd 1
.Scope0__DWXCOUNTCHARS_Integer resd 1
.Scope0__DWYCOUNTCHARS_Integer resd 1
.Scope0__DWFILLATTRIBUTE_Integer resd 1
.Scope0__DWFLAGS_Integer resd 1
.Scope0__WSHOWWINDOW_Integer resd 1
.Scope0__CBRESERVED2_Integer resd 1
.Scope0__LPRESERVED2_Integer resd 1
.Scope0__HSTDINPUT_Integer resd 1
.Scope0__HSTDOUTPUT_Integer resd 1
.Scope0__HSTDERROR_Integer resd 1
ENDSTRUC
STRUC Scope0__NMHDR_TYPE
.Scope0__HWNDFROM_Integer resd 1
.Scope0__IDFROM_Integer resd 1
.Scope0__CODE_Integer resd 1
ENDSTRUC
STRUC Scope0__WNDCLASS_TYPE
.Scope0__STYLE_Integer resd 1
.Scope0__LPFNWNDPROC_Integer resd 1
.Scope0__CBCLSEXTRA_Integer resd 1
.Scope0__CBWNDEXTRA2_Integer resd 1
.Scope0__HINSTANCE_Integer resd 1
.Scope0__HICON_Integer resd 1
.Scope0__HCURSOR_Integer resd 1
.Scope0__HBRBACKGROUND_Integer resd 1
.Scope0__LPSZMENUNAME_String resd 1
.Scope0__LPSZCLASSNAME_String resd 1
ENDSTRUC
STRUC Scope0__WNDCLASSEX_TYPE
.Scope0__CBSIZE_Integer resd 1
.Scope0__STYLE_Integer resd 1
.Scope0__LPFNWNDPROC_Integer resd 1
.Scope0__CBCLSEXTRA_Integer resd 1
.Scope0__CBWNDEXTRA_Integer resd 1
.Scope0__HINSTANCE_Integer resd 1
.Scope0__HICON_Integer resd 1
.Scope0__HCURSOR_Integer resd 1
.Scope0__HBRBACKGROUND_Integer resd 1
.Scope0__LPSZMENUNAME_String resd 1
.Scope0__LPSZCLASSNAME_String resd 1
.Scope0__HICONSM_Integer resd 1
ENDSTRUC
STRUC Scope0__CREATESTRUCT_TYPE
.Scope0__LPCREATEPARAMS_Integer resd 1
.Scope0__HINSTANCE_Integer resd 1
.Scope0__HMENU_Integer resd 1
.Scope0__HWNDPARENT_Integer resd 1
.Scope0__CY_Integer resd 1
.Scope0__CX_Integer resd 1
.Scope0__Y_Integer resd 1
.Scope0__X_Integer resd 1
.Scope0__STYLE_Integer resd 1
.Scope0__LPSZNAME_String resd 1
.Scope0__LPSZCLASS_String resd 1
.Scope0__EXSTYLE_Integer resd 1
ENDSTRUC
STRUC Scope0__MSG_TYPE
.Scope0__HWND_Integer resd 1
.Scope0__MESSAGE_Integer resd 1
.Scope0__WPARAM_Integer resd 1
.Scope0__LPARAM_Integer resd 1
.Scope0__TIME_Integer resd 1
.Scope0__X_Integer resd 1
.Scope0__Y_Integer resd 1
ENDSTRUC
STRUC Scope0__PAINTSTRUCT_TYPE
.Scope0__HDC_Integer resd 1
.Scope0__FERASE_Integer resd 1
.Scope0__LEFT_Integer resd 1
.Scope0__TOP_Integer resd 1
.Scope0__RIGHT_Integer resd 1
.Scope0__BOTTOM_Integer resd 1
.Scope0__FRESTORE_Integer resd 1
.Scope0__FINCUPDATE_Integer resd 1
.Scope0__RGBRESERVED1_Integer resd 1
.Scope0__RGBRESERVED2_Integer resd 1
.Scope0__RGBRESERVED3_Integer resd 1
.Scope0__RGBRESERVED4_Integer resd 1
.Scope0__RGBRESERVED5_Integer resd 1
.Scope0__RGBRESERVED6_Integer resd 1
.Scope0__RGBRESERVED7_Integer resd 1
.Scope0__RGBRESERVED8_Integer resd 1
ENDSTRUC
%ifdef BEGINPAINT_Used
_BEGINPAINT dd 0
_BEGINPAINT_LibHandle dd 0
_BEGINPAINT_Alias db "BeginPaint",0
_BEGINPAINT_Lib db "user32",0
_BEGINPAINT_Call db 0
%endif
%ifdef ENDPAINT_Used
_ENDPAINT dd 0
_ENDPAINT_LibHandle dd 0
_ENDPAINT_Alias db "EndPaint",0
_ENDPAINT_Lib db "user32",0
_ENDPAINT_Call db 0
%endif
%ifdef TEXTOUT_Used
_TEXTOUT dd 0
_TEXTOUT_LibHandle dd 0
_TEXTOUT_Alias db "TextOutA",0
_TEXTOUT_Lib db "gdi32",0
_TEXTOUT_Call db 0
%endif
%ifdef DRAWTEXT_Used
_DRAWTEXT dd 0
_DRAWTEXT_LibHandle dd 0
_DRAWTEXT_Alias db "DrawTextA",0
_DRAWTEXT_Lib db "user32",0
_DRAWTEXT_Call db 0
%endif
%ifdef ENUMCHILDWINDOWS_Used
_ENUMCHILDWINDOWS dd 0
_ENUMCHILDWINDOWS_LibHandle dd 0
_ENUMCHILDWINDOWS_Alias db "EnumChildWindows",0
_ENUMCHILDWINDOWS_Lib db "user32",0
_ENUMCHILDWINDOWS_Call db 0
%endif
%ifdef GETTOPWINDOW_Used
_GETTOPWINDOW dd 0
_GETTOPWINDOW_LibHandle dd 0
_GETTOPWINDOW_Alias db "GetTopWindow",0
_GETTOPWINDOW_Lib db "user32",0
_GETTOPWINDOW_Call db 0
%endif
%ifdef GETNEXTWINDOW_Used
_GETNEXTWINDOW dd 0
_GETNEXTWINDOW_LibHandle dd 0
_GETNEXTWINDOW_Alias db "GetWindow",0
_GETNEXTWINDOW_Lib db "user32",0
_GETNEXTWINDOW_Call db 0
%endif
%ifdef GETCLIENTRECT_Used
_GETCLIENTRECT dd 0
_GETCLIENTRECT_LibHandle dd 0
_GETCLIENTRECT_Alias db "GetClientRect",0
_GETCLIENTRECT_Lib db "user32",0
_GETCLIENTRECT_Call db 0
%endif
%ifdef GETWINDOWRECT_Used
_GETWINDOWRECT dd 0
_GETWINDOWRECT_LibHandle dd 0
_GETWINDOWRECT_Alias db "GetWindowRect",0
_GETWINDOWRECT_Lib db "user32",0
_GETWINDOWRECT_Call db 0
%endif
%ifdef REGISTERCLASS_Used
_REGISTERCLASS dd 0
_REGISTERCLASS_LibHandle dd 0
_REGISTERCLASS_Alias db "RegisterClassA",0
_REGISTERCLASS_Lib db "user32",0
_REGISTERCLASS_Call db 0
%endif
%ifdef REGISTERCLASSEX_Used
_REGISTERCLASSEX dd 0
_REGISTERCLASSEX_LibHandle dd 0
_REGISTERCLASSEX_Alias db "RegisterClassExA",0
_REGISTERCLASSEX_Lib db "USER32",0
_REGISTERCLASSEX_Call db 0
%endif
%ifdef UNREGISTERCLASS_Used
_UNREGISTERCLASS dd 0
_UNREGISTERCLASS_LibHandle dd 0
_UNREGISTERCLASS_Alias db "UnregisterClassA",0
_UNREGISTERCLASS_Lib db "user32",0
_UNREGISTERCLASS_Call db 0
%endif
%ifdef GETMESSAGE_Used
_GETMESSAGE dd 0
_GETMESSAGE_LibHandle dd 0
_GETMESSAGE_Alias db "GetMessageA",0
_GETMESSAGE_Lib db "user32",0
_GETMESSAGE_Call db 0
%endif
%ifdef TRANSLATEMESSAGE_Used
_TRANSLATEMESSAGE dd 0
_TRANSLATEMESSAGE_LibHandle dd 0
_TRANSLATEMESSAGE_Alias db "TranslateMessage",0
_TRANSLATEMESSAGE_Lib db "user32",0
_TRANSLATEMESSAGE_Call db 0
%endif
%ifdef DISPATCHMESSAGE_Used
_DISPATCHMESSAGE dd 0
_DISPATCHMESSAGE_LibHandle dd 0
_DISPATCHMESSAGE_Alias db "DispatchMessageA",0
_DISPATCHMESSAGE_Lib db "user32",0
_DISPATCHMESSAGE_Call db 0
%endif
%ifdef SENDMESSAGE_Used
_SENDMESSAGE dd 0
_SENDMESSAGE_LibHandle dd 0
_SENDMESSAGE_Alias db "SendMessageA",0
_SENDMESSAGE_Lib db "user32",0
_SENDMESSAGE_Call db 0
%endif
%ifdef LOADBITMAP_Used
_LOADBITMAP dd 0
_LOADBITMAP_LibHandle dd 0
_LOADBITMAP_Alias db "LoadBitmapA",0
_LOADBITMAP_Lib db "user32",0
_LOADBITMAP_Call db 0
%endif
%ifdef LOADCURSOR_Used
_LOADCURSOR dd 0
_LOADCURSOR_LibHandle dd 0
_LOADCURSOR_Alias db "LoadCursorA",0
_LOADCURSOR_Lib db "USER32",0
_LOADCURSOR_Call db 0
%endif
%ifdef LOADICON_Used
_LOADICON dd 0
_LOADICON_LibHandle dd 0
_LOADICON_Alias db "LoadIconA",0
_LOADICON_Lib db "user32",0
_LOADICON_Call db 0
%endif
%ifdef DESTROYICON_Used
_DESTROYICON dd 0
_DESTROYICON_LibHandle dd 0
_DESTROYICON_Alias db "DestroyIcon",0
_DESTROYICON_Lib db "user32",0
_DESTROYICON_Call db 0
%endif
%ifdef GETCURSOR_Used
_GETCURSOR dd 0
_GETCURSOR_LibHandle dd 0
_GETCURSOR_Alias db "GetCursor",0
_GETCURSOR_Lib db "user32",0
_GETCURSOR_Call db 0
%endif
%ifdef SETCURSOR_Used
_SETCURSOR dd 0
_SETCURSOR_LibHandle dd 0
_SETCURSOR_Alias db "SetCursor",0
_SETCURSOR_Lib db "user32",0
_SETCURSOR_Call db 0
%endif
%ifdef COPYICON_Used
_COPYICON dd 0
_COPYICON_LibHandle dd 0
_COPYICON_Alias db "CopyIcon",0
_COPYICON_Lib db "user32",0
_COPYICON_Call db 0
%endif
%ifdef SETSYSTEMCURSOR_Used
_SETSYSTEMCURSOR dd 0
_SETSYSTEMCURSOR_LibHandle dd 0
_SETSYSTEMCURSOR_Alias db "SetSystemCursor",0
_SETSYSTEMCURSOR_Lib db "user32",0
_SETSYSTEMCURSOR_Call db 0
%endif
%ifdef CREATECOMPATIBLEDC_Used
_CREATECOMPATIBLEDC dd 0
_CREATECOMPATIBLEDC_LibHandle dd 0
_CREATECOMPATIBLEDC_Alias db "CreateCompatibleDC",0
_CREATECOMPATIBLEDC_Lib db "gdi32",0
_CREATECOMPATIBLEDC_Call db 0
%endif
%ifdef CREATEWINDOWEX_Used
_CREATEWINDOWEX dd 0
_CREATEWINDOWEX_LibHandle dd 0
_CREATEWINDOWEX_Alias db "CreateWindowExA",0
_CREATEWINDOWEX_Lib db "user32",0
_CREATEWINDOWEX_Call db 0
%endif
%ifdef UPDATEWINDOW_Used
_UPDATEWINDOW dd 0
_UPDATEWINDOW_LibHandle dd 0
_UPDATEWINDOW_Alias db "UpdateWindow",0
_UPDATEWINDOW_Lib db "USER32",0
_UPDATEWINDOW_Call db 0
%endif
%ifdef SHOWWINDOW_Used
_SHOWWINDOW dd 0
_SHOWWINDOW_LibHandle dd 0
_SHOWWINDOW_Alias db "ShowWindow",0
_SHOWWINDOW_Lib db "USER32",0
_SHOWWINDOW_Call db 0
%endif
%ifdef DEFWINDOWPROC_Used
_DEFWINDOWPROC dd 0
_DEFWINDOWPROC_LibHandle dd 0
_DEFWINDOWPROC_Alias db "DefWindowProcA",0
_DEFWINDOWPROC_Lib db "user32",0
_DEFWINDOWPROC_Call db 0
%endif
%ifdef POSTQUITMESSAGE_Used
_POSTQUITMESSAGE dd 0
_POSTQUITMESSAGE_LibHandle dd 0
_POSTQUITMESSAGE_Alias db "PostQuitMessage",0
_POSTQUITMESSAGE_Lib db "user32",0
_POSTQUITMESSAGE_Call db 0
%endif
%ifdef GETMODULEHANDLE_Used
_GETMODULEHANDLE dd 0
_GETMODULEHANDLE_LibHandle dd 0
_GETMODULEHANDLE_Alias db "GetModuleHandleA",0
_GETMODULEHANDLE_Lib db "kernel32",0
_GETMODULEHANDLE_Call db 0
%endif
%ifdef GETACTIVEWINDOW_Used
_GETACTIVEWINDOW dd 0
_GETACTIVEWINDOW_LibHandle dd 0
_GETACTIVEWINDOW_Alias db "GetActiveWindow",0
_GETACTIVEWINDOW_Lib db "user32",0
_GETACTIVEWINDOW_Call db 0
%endif
%ifdef EXITPROCESS_Used
_EXITPROCESS dd 0
_EXITPROCESS_LibHandle dd 0
_EXITPROCESS_Alias db "ExitProcess",0
_EXITPROCESS_Lib db "kernel32",0
_EXITPROCESS_Call db 0
%endif
%ifdef MSGBOX_Used
_MSGBOX dd 0
_MSGBOX_LibHandle dd 0
_MSGBOX_Alias db "MessageBoxA",0
_MSGBOX_Lib db "user32",0
_MSGBOX_Call db 0
%endif
%ifdef MESSAGEBOX_Used
_MESSAGEBOX dd 0
_MESSAGEBOX_LibHandle dd 0
_MESSAGEBOX_Alias db "MessageBoxA",0
_MESSAGEBOX_Lib db "user32",0
_MESSAGEBOX_Call db 0
%endif
%ifdef GETLASTERROR_Used
_GETLASTERROR dd 0
_GETLASTERROR_LibHandle dd 0
_GETLASTERROR_Alias db "GetLastError",0
_GETLASTERROR_Lib db "kernel32",0
_GETLASTERROR_Call db 0
%endif
%ifdef SETLASTERROR_Used
_SETLASTERROR dd 0
_SETLASTERROR_LibHandle dd 0
_SETLASTERROR_Alias db "SetLastError",0
_SETLASTERROR_Lib db "kernel32",0
_SETLASTERROR_Call db 0
%endif
%ifdef FORMATMESSAGE_Used
_FORMATMESSAGE dd 0
_FORMATMESSAGE_LibHandle dd 0
_FORMATMESSAGE_Alias db "FormatMessageA",0
_FORMATMESSAGE_Lib db "kernel32",0
_FORMATMESSAGE_Call db 0
%endif
%ifdef LOCALFREE_Used
_LOCALFREE dd 0
_LOCALFREE_LibHandle dd 0
_LOCALFREE_Alias db "LocalFree",0
_LOCALFREE_Lib db "kernel32",0
_LOCALFREE_Call db 0
%endif
%ifdef GETWINDOWLONG_Used
_GETWINDOWLONG dd 0
_GETWINDOWLONG_LibHandle dd 0
_GETWINDOWLONG_Alias db "GetWindowIntegerA",0
_GETWINDOWLONG_Lib db "user32",0
_GETWINDOWLONG_Call db 0
%endif
%ifdef SETWINDOWLONG_Used
_SETWINDOWLONG dd 0
_SETWINDOWLONG_LibHandle dd 0
_SETWINDOWLONG_Alias db "SetWindowIntegerA",0
_SETWINDOWLONG_Lib db "user32",0
_SETWINDOWLONG_Call db 0
%endif
%ifdef CALLWINDOWPROC_Used
_CALLWINDOWPROC dd 0
_CALLWINDOWPROC_LibHandle dd 0
_CALLWINDOWPROC_Alias db "CallWindowProcA",0
_CALLWINDOWPROC_Lib db "user32",0
_CALLWINDOWPROC_Call db 0
%endif
%ifdef GETCLASSLONG_Used
_GETCLASSLONG dd 0
_GETCLASSLONG_LibHandle dd 0
_GETCLASSLONG_Alias db "GetClassLongA",0
_GETCLASSLONG_Lib db "user32",0
_GETCLASSLONG_Call db 0
%endif
%ifdef SETCLASSLONG_Used
_SETCLASSLONG dd 0
_SETCLASSLONG_LibHandle dd 0
_SETCLASSLONG_Alias db "SetClassLongA",0
_SETCLASSLONG_Lib db "user32",0
_SETCLASSLONG_Call db 0
%endif
%ifdef GETDESKTOPWINDOW_Used
_GETDESKTOPWINDOW dd 0
_GETDESKTOPWINDOW_LibHandle dd 0
_GETDESKTOPWINDOW_Alias db "GetDesktopWindow",0
_GETDESKTOPWINDOW_Lib db "user32",0
_GETDESKTOPWINDOW_Call db 0
%endif
%ifdef GETPROCADDRESS_Used
_GETPROCADDRESS dd 0
_GETPROCADDRESS_LibHandle dd 0
_GETPROCADDRESS_Alias db "GetProcAddress",0
_GETPROCADDRESS_Lib db "kernel32",0
_GETPROCADDRESS_Call db 0
%endif
%ifdef DIALOGBOXPARAM_Used
_DIALOGBOXPARAM dd 0
_DIALOGBOXPARAM_LibHandle dd 0
_DIALOGBOXPARAM_Alias db "DialogBoxParamA",0
_DIALOGBOXPARAM_Lib db "user32",0
_DIALOGBOXPARAM_Call db 0
%endif
%ifdef ENDDIALOG_Used
_ENDDIALOG dd 0
_ENDDIALOG_LibHandle dd 0
_ENDDIALOG_Alias db "EndDialog",0
_ENDDIALOG_Lib db "user32",0
_ENDDIALOG_Call db 0
%endif
%ifdef GETDLGITEM_Used
_GETDLGITEM dd 0
_GETDLGITEM_LibHandle dd 0
_GETDLGITEM_Alias db "GetDlgItem",0
_GETDLGITEM_Lib db "user32",0
_GETDLGITEM_Call db 0
%endif
%ifdef INITCOMMONCONTROLS_Used
_INITCOMMONCONTROLS dd 0
_INITCOMMONCONTROLS_LibHandle dd 0
_INITCOMMONCONTROLS_Alias db "InitCommonControls",0
_INITCOMMONCONTROLS_Lib db "comctl32",0
_INITCOMMONCONTROLS_Call db 0
%endif
%ifdef INVALIDATERECT_Used
_INVALIDATERECT dd 0
_INVALIDATERECT_LibHandle dd 0
_INVALIDATERECT_Alias db "InvalidateRect",0
_INVALIDATERECT_Lib db "user32",0
_INVALIDATERECT_Call db 0
%endif
%ifdef CREATEPEN_Used
_CREATEPEN dd 0
_CREATEPEN_LibHandle dd 0
_CREATEPEN_Alias db "CreatePen",0
_CREATEPEN_Lib db "gdi32",0
_CREATEPEN_Call db 0
%endif
%ifdef SELECTOBJECT_Used
_SELECTOBJECT dd 0
_SELECTOBJECT_LibHandle dd 0
_SELECTOBJECT_Alias db "SelectObject",0
_SELECTOBJECT_Lib db "gdi32",0
_SELECTOBJECT_Call db 0
%endif
%ifdef GETSTOCKOBJECT_Used
_GETSTOCKOBJECT dd 0
_GETSTOCKOBJECT_LibHandle dd 0
_GETSTOCKOBJECT_Alias db "GetStockObject",0
_GETSTOCKOBJECT_Lib db "gdi32",0
_GETSTOCKOBJECT_Call db 0
%endif
%ifdef CREATESOLIDBRUSH_Used
_CREATESOLIDBRUSH dd 0
_CREATESOLIDBRUSH_LibHandle dd 0
_CREATESOLIDBRUSH_Alias db "CreateSolidBrush",0
_CREATESOLIDBRUSH_Lib db "gdi32",0
_CREATESOLIDBRUSH_Call db 0
%endif
%ifdef RECTANGLE_Used
_RECTANGLE dd 0
_RECTANGLE_LibHandle dd 0
_RECTANGLE_Alias db "Rectangle",0
_RECTANGLE_Lib db "gdi32",0
_RECTANGLE_Call db 0
%endif
%ifdef DELETEOBJECT_Used
_DELETEOBJECT dd 0
_DELETEOBJECT_LibHandle dd 0
_DELETEOBJECT_Alias db "DeleteObject",0
_DELETEOBJECT_Lib db "gdi32",0
_DELETEOBJECT_Call db 0
%endif
%ifdef DELETEDC_Used
_DELETEDC dd 0
_DELETEDC_LibHandle dd 0
_DELETEDC_Alias db "DeleteDC",0
_DELETEDC_Lib db "gdi32",0
_DELETEDC_Call db 0
%endif
%ifdef DESTROYWINDOW_Used
_DESTROYWINDOW dd 0
_DESTROYWINDOW_LibHandle dd 0
_DESTROYWINDOW_Alias db "DestroyWindow",0
_DESTROYWINDOW_Lib db "user32",0
_DESTROYWINDOW_Call db 0
%endif
%ifdef SETPIXEL_Used
_SETPIXEL dd 0
_SETPIXEL_LibHandle dd 0
_SETPIXEL_Alias db "SetPixel",0
_SETPIXEL_Lib db "gdi32",0
_SETPIXEL_Call db 0
%endif
%ifdef BITBLT_Used
_BITBLT dd 0
_BITBLT_LibHandle dd 0
_BITBLT_Alias db "BitBlt",0
_BITBLT_Lib db "gdi32",0
_BITBLT_Call db 0
%endif
%ifdef CREATEMENU_Used
_CREATEMENU dd 0
_CREATEMENU_LibHandle dd 0
_CREATEMENU_Alias db "CreateMenu",0
_CREATEMENU_Lib db "user32",0
_CREATEMENU_Call db 0
%endif
%ifdef APPENDMENU_Used
_APPENDMENU dd 0
_APPENDMENU_LibHandle dd 0
_APPENDMENU_Alias db "AppendMenuA",0
_APPENDMENU_Lib db "user32",0
_APPENDMENU_Call db 0
%endif
%ifdef POSTMESSAGE_Used
_POSTMESSAGE dd 0
_POSTMESSAGE_LibHandle dd 0
_POSTMESSAGE_Alias db "PostMessageA",0
_POSTMESSAGE_Lib db "user32",0
_POSTMESSAGE_Call db 0
%endif
%ifdef GETSYSTEMMETRICS_Used
_GETSYSTEMMETRICS dd 0
_GETSYSTEMMETRICS_LibHandle dd 0
_GETSYSTEMMETRICS_Alias db "GetSystemMetrics",0
_GETSYSTEMMETRICS_Lib db "user32",0
_GETSYSTEMMETRICS_Call db 0
%endif
%ifdef SETWINDOWPOS_Used
_SETWINDOWPOS dd 0
_SETWINDOWPOS_LibHandle dd 0
_SETWINDOWPOS_Alias db "SetWindowPos",0
_SETWINDOWPOS_Lib db "user32",0
_SETWINDOWPOS_Call db 0
%endif
%ifdef ENABLEWINDOW_Used
_ENABLEWINDOW dd 0
_ENABLEWINDOW_LibHandle dd 0
_ENABLEWINDOW_Alias db "EnableWindow",0
_ENABLEWINDOW_Lib db "user32",0
_ENABLEWINDOW_Call db 0
%endif
%ifdef PLAYSOUND_Used
_PLAYSOUND dd 0
_PLAYSOUND_LibHandle dd 0
_PLAYSOUND_Alias db "PlaySoundA",0
_PLAYSOUND_Lib db "winmm",0
_PLAYSOUND_Call db 0
%endif
STRUC Scope68__MMTIME_TYPE
.Scope68__WTYPE_Integer resd 1
.Scope68__U_Integer resd 1
ENDSTRUC
%ifdef TIMESETEVENT_Used
_TIMESETEVENT dd 0
_TIMESETEVENT_LibHandle dd 0
_TIMESETEVENT_Alias db "timeSetEvent",0
_TIMESETEVENT_Lib db "winmm.dll",0
_TIMESETEVENT_Call db 0
%endif
%ifdef TIMEKILLEVENT_Used
_TIMEKILLEVENT dd 0
_TIMEKILLEVENT_LibHandle dd 0
_TIMEKILLEVENT_Alias db "timeKillEvent",0
_TIMEKILLEVENT_Lib db "winmm.dll",0
_TIMEKILLEVENT_Call db 0
%endif
%ifdef CREATEFONT_Used
_CREATEFONT dd 0
_CREATEFONT_LibHandle dd 0
_CREATEFONT_Alias db "CreateFontA",0
_CREATEFONT_Lib db "gdi32",0
_CREATEFONT_Call db 0
%endif
Number_1 dq 0.0
String_2 db "Message:",0
Number_3 dq 0.0
%ifdef ABS_Used
_ABS dd 0
_ABS_LibHandle dd 0
_ABS_Alias db "fabs",0
_ABS_Lib db "msvcrt.dll",0
_ABS_Call db 1
%endif
%ifdef ACOS_Used
_ACOS dd 0
_ACOS_LibHandle dd 0
_ACOS_Alias db "acos",0
_ACOS_Lib db "msvcrt.dll",0
_ACOS_Call db 1
%endif
%ifdef ASIN_Used
_ASIN dd 0
_ASIN_LibHandle dd 0
_ASIN_Alias db "asin",0
_ASIN_Lib db "msvcrt.dll",0
_ASIN_Call db 1
%endif
%ifdef ATAN_Used
_ATAN dd 0
_ATAN_LibHandle dd 0
_ATAN_Alias db "atan",0
_ATAN_Lib db "msvcrt.dll",0
_ATAN_Call db 1
%endif
%ifdef ATN_Used
_ATN dd 0
_ATN_LibHandle dd 0
_ATN_Alias db "atan",0
_ATN_Lib db "msvcrt.dll",0
_ATN_Call db 1
%endif
%ifdef CEIL_Used
_CEIL dd 0
_CEIL_LibHandle dd 0
_CEIL_Alias db "ceil",0
_CEIL_Lib db "msvcrt.dll",0
_CEIL_Call db 1
%endif
%ifdef EXP_Used
_EXP dd 0
_EXP_LibHandle dd 0
_EXP_Alias db "exp",0
_EXP_Lib db "msvcrt.dll",0
_EXP_Call db 1
%endif
%ifdef FLOOR_Used
_FLOOR dd 0
_FLOOR_LibHandle dd 0
_FLOOR_Alias db "floor",0
_FLOOR_Lib db "msvcrt.dll",0
_FLOOR_Call db 1
%endif
%ifdef LOG_Used
_LOG dd 0
_LOG_LibHandle dd 0
_LOG_Alias db "log",0
_LOG_Lib db "msvcrt.dll",0
_LOG_Call db 1
%endif
%ifdef SQR_Used
_SQR dd 0
_SQR_LibHandle dd 0
_SQR_Alias db "sqrt",0
_SQR_Lib db "msvcrt.dll",0
_SQR_Call db 1
%endif
%ifdef SQRT_Used
_SQRT dd 0
_SQRT_LibHandle dd 0
_SQRT_Alias db "sqrt",0
_SQRT_Lib db "msvcrt.dll",0
_SQRT_Call db 1
%endif
%ifdef TAN_Used
_TAN dd 0
_TAN_LibHandle dd 0
_TAN_Alias db "tan",0
_TAN_Lib db "msvcrt.dll",0
_TAN_Call db 1
%endif
%ifdef TIMER_Used
_TIMER dd 0
_TIMER_LibHandle dd 0
_TIMER_Alias db "timer",0
_TIMER_Lib db "msvcrt.dll",0
_TIMER_Call db 1
%endif
%ifdef POW_Used
_POW dd 0
_POW_LibHandle dd 0
_POW_Alias db "pow",0
_POW_Lib db "msvcrt.dll",0
_POW_Call db 1
%endif
Number_4 dq 1.0
Number_5 dq 1.0
%ifdef ISALNUM_Used
_ISALNUM dd 0
_ISALNUM_LibHandle dd 0
_ISALNUM_Alias db "isalnum",0
_ISALNUM_Lib db "msvcrt.dll",0
_ISALNUM_Call db 1
%endif
%ifdef ISALPHA_Used
_ISALPHA dd 0
_ISALPHA_LibHandle dd 0
_ISALPHA_Alias db "isalpha",0
_ISALPHA_Lib db "msvcrt.dll",0
_ISALPHA_Call db 1
%endif
%ifdef ISCNTRL_Used
_ISCNTRL dd 0
_ISCNTRL_LibHandle dd 0
_ISCNTRL_Alias db "iscntrl",0
_ISCNTRL_Lib db "msvcrt.dll",0
_ISCNTRL_Call db 1
%endif
%ifdef ISDIGIT_Used
_ISDIGIT dd 0
_ISDIGIT_LibHandle dd 0
_ISDIGIT_Alias db "isdigit",0
_ISDIGIT_Lib db "msvcrt.dll",0
_ISDIGIT_Call db 1
%endif
%ifdef ISGRAPH_Used
_ISGRAPH dd 0
_ISGRAPH_LibHandle dd 0
_ISGRAPH_Alias db "isgraph",0
_ISGRAPH_Lib db "msvcrt.dll",0
_ISGRAPH_Call db 1
%endif
%ifdef ISLOWER_Used
_ISLOWER dd 0
_ISLOWER_LibHandle dd 0
_ISLOWER_Alias db "islower",0
_ISLOWER_Lib db "msvcrt.dll",0
_ISLOWER_Call db 1
%endif
%ifdef ISPRINT_Used
_ISPRINT dd 0
_ISPRINT_LibHandle dd 0
_ISPRINT_Alias db "isprint",0
_ISPRINT_Lib db "msvcrt.dll",0
_ISPRINT_Call db 1
%endif
%ifdef ISPUNCT_Used
_ISPUNCT dd 0
_ISPUNCT_LibHandle dd 0
_ISPUNCT_Alias db "ispunct",0
_ISPUNCT_Lib db "msvcrt.dll",0
_ISPUNCT_Call db 1
%endif
%ifdef ISSPACE_Used
_ISSPACE dd 0
_ISSPACE_LibHandle dd 0
_ISSPACE_Alias db "isspace",0
_ISSPACE_Lib db "msvcrt.dll",0
_ISSPACE_Call db 1
%endif
%ifdef ISUPPER_Used
_ISUPPER dd 0
_ISUPPER_LibHandle dd 0
_ISUPPER_Alias db "isupper",0
_ISUPPER_Lib db "msvcrt.dll",0
_ISUPPER_Call db 1
%endif
%ifdef ISXDIGIT_Used
_ISXDIGIT dd 0
_ISXDIGIT_LibHandle dd 0
_ISXDIGIT_Alias db "isxdigit",0
_ISXDIGIT_Lib db "msvcrt.dll",0
_ISXDIGIT_Call db 1
%endif
%ifdef TOLOWER_Used
_TOLOWER dd 0
_TOLOWER_LibHandle dd 0
_TOLOWER_Alias db "tolower",0
_TOLOWER_Lib db "msvcrt.dll",0
_TOLOWER_Call db 1
%endif
%ifdef TOUPPER_Used
_TOUPPER dd 0
_TOUPPER_LibHandle dd 0
_TOUPPER_Alias db "toupper",0
_TOUPPER_Lib db "msvcrt.dll",0
_TOUPPER_Call db 1
%endif
STRUC Scope113__SECURITYATTRIBUTES_TYPE
.Scope113__LENGTH_Integer resd 1
.Scope113__SECURITYDESCRIPTOR_Integer resd 1
.Scope113__INHERITHANDLE_Integer resd 1
ENDSTRUC
%ifdef CHDIR_Used
_CHDIR dd 0
_CHDIR_LibHandle dd 0
_CHDIR_Alias db "SetCurrentDirectoryA",0
_CHDIR_Lib db "kernel32.dll",0
_CHDIR_Call db 0
%endif
%ifdef KILL_Used
_KILL dd 0
_KILL_LibHandle dd 0
_KILL_Alias db "DeleteFileA",0
_KILL_Lib db "kernel32.dll",0
_KILL_Call db 0
%endif
%ifdef CREATEDIRECTORY_Used
_CREATEDIRECTORY dd 0
_CREATEDIRECTORY_LibHandle dd 0
_CREATEDIRECTORY_Alias db "CreateDirectoryA",0
_CREATEDIRECTORY_Lib db "kernel32.dll",0
_CREATEDIRECTORY_Call db 0
%endif
%ifdef RANDOMIZE_Used
_RANDOMIZE dd 0
_RANDOMIZE_LibHandle dd 0
_RANDOMIZE_Alias db "srand",0
_RANDOMIZE_Lib db "msvcrt.dll",0
_RANDOMIZE_Call db 1
%endif
%ifdef RAND_Used
_RAND dd 0
_RAND_LibHandle dd 0
_RAND_Alias db "rand",0
_RAND_Lib db "msvcrt.dll",0
_RAND_Call db 1
%endif
%ifdef ENVIRON____Used
_ENVIRON___ dd 0
_ENVIRON____LibHandle dd 0
_ENVIRON____Alias db "getenv",0
_ENVIRON____Lib db "msvcrt.dll",0
_ENVIRON____Call db 1
%endif
%ifdef RENAME_Used
_RENAME dd 0
_RENAME_LibHandle dd 0
_RENAME_Alias db "MoveFileA",0
_RENAME_Lib db "kernel32.dll",0
_RENAME_Call db 0
%endif
%ifdef RMDIR_Used
_RMDIR dd 0
_RMDIR_LibHandle dd 0
_RMDIR_Alias db "RemoveDirectoryA",0
_RMDIR_Lib db "kernel32.dll",0
_RMDIR_Call db 0
%endif
%ifdef RUN_Used
_RUN dd 0
_RUN_LibHandle dd 0
_RUN_Alias db "system",0
_RUN_Lib db "msvcrt.dll",0
_RUN_Call db 1
%endif
Scope123__SECATTRIBUTES_UDT ISTRUC Scope113__SECURITYATTRIBUTES_TYPE
IEND
Number_6 dq 32767.0
Number_7 dq 0.0
Number_8 dq 0.0
Number_9 dq 0.0
Number_10 dq 1.0
Number_11 dq 0.0
Number_12 dq -1.0
%ifdef LEN_Used
_LEN dd 0
_LEN_LibHandle dd 0
_LEN_Alias db "lstrlenA",0
_LEN_Lib db "Kernel32.dll",0
_LEN_Call db 0
%endif
%ifdef LSTRCPY_Used
_LSTRCPY dd 0
_LSTRCPY_LibHandle dd 0
_LSTRCPY_Alias db "lstrcpyA",0
_LSTRCPY_Lib db "Kernel32.dll",0
_LSTRCPY_Call db 0
%endif
Number_13 dq 1.0
String_14 db "",0
String_15 db " ",0
Number_16 dq 1.0
Number_17 dq 1.0
Number_18 dq 1.0
Number_19 dq 1.0
Number_20 dq 1.0
Number_21 dq 1.0
Number_22 dq 1.0
Number_23 dq 1.0
String_24 db "",0
Number_25 dq 1.0
Number_26 dq 1.0
Number_27 dq 1.0
Number_28 dq 1.0
String_29 db "",0
Number_30 dq 1.0
Number_31 dq 1.0
Number_32 dq 1.0
Number_33 dq 1.0
Number_34 dq 1.0
Number_35 dq 1.0
Number_36 dq 0.0
Number_37 dq 0.0
Number_38 dq 1.0
Number_39 dq 1.0
Number_40 dq 0.0
Number_41 dq 0.0
Number_42 dq 1.0
Number_43 dq 1.0
Number_44 dq 0.0
Number_45 dq 0.0
Number_46 dq 1.0
String_47 db " ",0
Number_48 dq 1.0
Number_49 dq 1.0
Number_50 dq 1.0
Number_51 dq 1.0
Number_52 dq 1.0
Number_53 dq 1.0
Number_54 dq 1.0
Number_55 dq 0.0
Number_56 dq 0.0
Number_57 dq 1.0
Number_58 dq 0.0
Number_59 dq 1.0
Number_60 dq 1.0
Number_61 dq 0.0
Number_62 dq 0.0
Number_63 dq 0.0
Number_64 dq 1.0
Number_65 dq 0.0
Number_66 dq 1.0
String_67 db " ",0
Number_68 dq 1.0
Number_69 dq 1.0
Number_70 dq 1.0
Number_71 dq 1.0
Number_72 dq 1.0
Number_73 dq 1.0
Number_74 dq 1.0
Number_75 dq 1.0
Number_76 dq 1.0
Number_77 dq 0.0
Number_78 dq 0.0
Number_79 dq 0.0
Number_80 dq 1.0
Number_81 dq 1.0
Number_82 dq 1.0
Number_83 dq 10.0
Number_84 dq 13.0
Number_85 dq 13.0
Number_86 dq 13.0
Number_87 dq 10.0
Number_88 dq 11.0
Number_89 dq 1.0
Number_90 dq 0.0
Number_91 dq 1.0
Number_92 dq 1.0
%ifdef GCVT_Used
_GCVT dd 0
_GCVT_LibHandle dd 0
_GCVT_Alias db "_gcvt",0
_GCVT_Lib db "msvcrt.dll",0
_GCVT_Call db 1
%endif
%ifdef VAL_Used
_VAL dd 0
_VAL_LibHandle dd 0
_VAL_Alias db "atof",0
_VAL_Lib db "msvcrt.dll",0
_VAL_Call db 1
%endif
Number_93 dq 100.0
Number_94 dq 1.0
%ifdef GETSTDHANDLE_Used
_GETSTDHANDLE dd 0
_GETSTDHANDLE_LibHandle dd 0
_GETSTDHANDLE_Alias db "GetStdHandle",0
_GETSTDHANDLE_Lib db "Kernel32.dll",0
_GETSTDHANDLE_Call db 0
%endif
%ifdef SETCONSOLETITLEWINAPI_Used
_SETCONSOLETITLEWINAPI dd 0
_SETCONSOLETITLEWINAPI_LibHandle dd 0
_SETCONSOLETITLEWINAPI_Alias db "SetConsoleTitleA",0
_SETCONSOLETITLEWINAPI_Lib db "Kernel32.dll",0
_SETCONSOLETITLEWINAPI_Call db 0
%endif
%ifdef SETCONSOLETEXTATTRIBUTE_Used
_SETCONSOLETEXTATTRIBUTE dd 0
_SETCONSOLETEXTATTRIBUTE_LibHandle dd 0
_SETCONSOLETEXTATTRIBUTE_Alias db "SetConsoleTextAttribute",0
_SETCONSOLETEXTATTRIBUTE_Lib db "Kernel32.dll",0
_SETCONSOLETEXTATTRIBUTE_Call db 0
%endif
Number_95 dq -11.0
Number_96 dq -10.0
Number_97 dq -11.0
STRUC Scope168__SYSTEMTIME_TYPE
.Scope168__WYEAR_Integer resd 1
.Scope168__WMONTH_Integer resd 1
.Scope168__WDAYOFWEEK_Integer resd 1
.Scope168__WDAY_Integer resd 1
.Scope168__WHOUR_Integer resd 1
.Scope168__WMINUTE_Integer resd 1
.Scope168__WSECOND_Integer resd 1
.Scope168__WMILLISECONDS_Integer resd 1
ENDSTRUC
STRUC Scope168__TM_TYPE
.Scope168__HOUR_Integer resd 1
.Scope168__ISDST_Integer resd 1
.Scope168__MDAY_Integer resd 1
.Scope168__MIN_Integer resd 1
.Scope168__MON_Integer resd 1
.Scope168__SEC_Integer resd 1
.Scope168__WDAY_Integer resd 1
.Scope168__YDAY_Integer resd 1
.Scope168__YEAR_Integer resd 1
ENDSTRUC
%ifdef GETLOCALTIME_Used
_GETLOCALTIME dd 0
_GETLOCALTIME_LibHandle dd 0
_GETLOCALTIME_Alias db "GetLocalTime",0
_GETLOCALTIME_Lib db "kernel32.dll",0
_GETLOCALTIME_Call db 0
%endif
Scope169__RETURNTIME_UDT ISTRUC Scope168__SYSTEMTIME_TYPE
IEND
%ifdef FOPEN_Used
_FOPEN dd 0
_FOPEN_LibHandle dd 0
_FOPEN_Alias db "fopen",0
_FOPEN_Lib db "Msvcrt.dll",0
_FOPEN_Call db 1
%endif
%ifdef FWRITE_Used
_FWRITE dd 0
_FWRITE_LibHandle dd 0
_FWRITE_Alias db "fwrite",0
_FWRITE_Lib db "Msvcrt.dll",0
_FWRITE_Call db 1
%endif
%ifdef FWRITE2_Used
_FWRITE2 dd 0
_FWRITE2_LibHandle dd 0
_FWRITE2_Alias db "fwrite",0
_FWRITE2_Lib db "Msvcrt.dll",0
_FWRITE2_Call db 1
%endif
%ifdef FPUTS_Used
_FPUTS dd 0
_FPUTS_LibHandle dd 0
_FPUTS_Alias db "fputs",0
_FPUTS_Lib db "Msvcrt.dll",0
_FPUTS_Call db 1
%endif
%ifdef FREAD_Used
_FREAD dd 0
_FREAD_LibHandle dd 0
_FREAD_Alias db "fread",0
_FREAD_Lib db "Msvcrt.dll",0
_FREAD_Call db 1
%endif
%ifdef FREAD2_Used
_FREAD2 dd 0
_FREAD2_LibHandle dd 0
_FREAD2_Alias db "fread",0
_FREAD2_Lib db "Msvcrt.dll",0
_FREAD2_Call db 1
%endif
%ifdef FGETS_Used
_FGETS dd 0
_FGETS_LibHandle dd 0
_FGETS_Alias db "fgets",0
_FGETS_Lib db "Msvcrt.dll",0
_FGETS_Call db 1
%endif
%ifdef FTELL_Used
_FTELL dd 0
_FTELL_LibHandle dd 0
_FTELL_Alias db "ftell",0
_FTELL_Lib db "Msvcrt.dll",0
_FTELL_Call db 1
%endif
%ifdef FSEEK_Used
_FSEEK dd 0
_FSEEK_LibHandle dd 0
_FSEEK_Alias db "fseek",0
_FSEEK_Lib db "Msvcrt.dll",0
_FSEEK_Call db 1
%endif
%ifdef FGETPOS_Used
_FGETPOS dd 0
_FGETPOS_LibHandle dd 0
_FGETPOS_Alias db "fgetpos",0
_FGETPOS_Lib db "Msvcrt.dll",0
_FGETPOS_Call db 1
%endif
%ifdef FSETPOS_Used
_FSETPOS dd 0
_FSETPOS_LibHandle dd 0
_FSETPOS_Alias db "fsetpos",0
_FSETPOS_Lib db "Msvcrt.dll",0
_FSETPOS_Call db 1
%endif
%ifdef FCLOSE_Used
_FCLOSE dd 0
_FCLOSE_LibHandle dd 0
_FCLOSE_Alias db "fclose",0
_FCLOSE_Lib db "Msvcrt.dll",0
_FCLOSE_Call db 1
%endif
String_98 db "t",0
Number_99 dq 1.0
Number_100 dq 1.0
Number_101 dq 13.0
Number_102 dq 10.0
Number_103 dq 0.0
Number_104 dq 2.0
Number_105 dq 0.0
Number_106 dq 1.0
Number_107 dq 1.0
Number_108 dq 4.0
Number_109 dq 1.0
Number_110 dq 4.0
Number_111 dq 1.0
Number_112 dq 8.0
Number_113 dq 1.0
Number_114 dq 8.0
Number_115 dq 1.0
Number_116 dq 0.0
%ifdef LTOA_Used
_LTOA dd 0
_LTOA_LibHandle dd 0
_LTOA_Alias db "_ltoa",0
_LTOA_Lib db "msvcrt.dll",0
_LTOA_Call db 1
%endif
Number_117 dq 40.0
Number_118 dq 0.0
Number_119 dq 0.0
%ifdef ERRORMSGBOX_Used
_ERRORMSGBOX dd 0
_ERRORMSGBOX_LibHandle dd 0
_ERRORMSGBOX_Alias db "MessageBoxA",0
_ERRORMSGBOX_Lib db "user32",0
_ERRORMSGBOX_Call db 0
%endif
Number_120 dq 40.0
Number_121 dq 10.0
String_122 db "System error code ",0
Number_123 dq 256.0
Number_124 dq 4096.0
Number_125 dq 512.0
Number_126 dq 0.0
Number_127 dq 0.0
Number_128 dq 0.0
Number_129 dq 0.0
Number_130 dq 0.0
Number_131 dq 0.0
String_132 db "Enter your name: ",0
Scope199__A____String dd 0
Number_133 dq 1.0
String_134 db "Brian",0
Number_135 dq 0.0
String_136 db "Hey, we have the same name!",0
String_137 db "Well, nice knowing you",0
Number_138 dq 2.0
String_139 db "",0
String_140 db "",0
String_141 db "Enter random phrase: ",0
Scope199__LENGTH_Integer dd 0
Number_142 dq 1.0
String_143 db "Length of your random phrase: ",0
Number_144 dq 1.0
String_145 db "Uppercase rendition of your phrase: ",0
Number_146 dq 1.0
String_147 db "Lowercase rendition of your phrase: ",0
Number_148 dq 1.0
String_149 db "Middle 3 letters of your phrase: ",0
Number_150 dq 2.0
Number_151 dq 3.0
String_152 db "",0
String_153 db "",0
Number_154 dq 2.0
String_155 db "Enter a number between 1-255: ",0
Scope199__A__Integer dd 0
Number_156 dq 1.0
String_157 db "Your number is ASCII character: ",0
String_158 db "",0
String_159 db "",0
Number_160 dq 2.0
String_161 db "Well, there is a lot more to do with strings, but I've got to run now.",0
String_162 db "Look in String.inc for more functions related to strings.",0
String_163 db "",0
Number_164 dq 4.0
String_165 db "Bye now!",0
Number_166 dq 2.0
ExitStatus dd 0

