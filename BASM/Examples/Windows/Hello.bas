' 
' --- Hello.kb --- 
' 
' For KoolB version 15, the $Optimize directive appears 
' to have no effect. But the $Compress directive shrunk 
' the EXE from 48,640 bytes to 10,240. 
$AppType GUI 
$Optimize On 
$Compress On 

$Include "Win32API.inc" 
$Include "Debug.inc" 



$Const CS_VREDRAW = 1 
$Const CS_HREDRAW = 2 
$Const CS_CLASSDC = 64 

$Const COLOR_WINDOW = 5 

$Const IDC_ARROW = 32512 

$Const IDI_APPLICATION = 32512 
$Const IDI_WINLOGO = 32517 

' *** WS_POPUP will prevent the window from being 
' *** displayed even when RegisterClassEx and 
' *** CreateWindowEx return success. 
'$Const WS_POPUP = 2147483648 

' -- Begin WS_DEFAULT -- 
$Const WS_VISIBLE = 268435456 
$Const WS_CAPTION = 12582912 
$Const WS_SIZEBOX = 262144 
$Const WS_SYSMENU = 524288 
$Const WS_MINIMIZEBOX = 131072 
$Const WS_MAXIMIZEBOX = 65536 
' -- End WS_DEFAULT -- 
$Const WS_DEFAULT = 282001408 

$Const CW_USEDEFAULT = 2147483648 

$Const WM_CREATE = 1 
$Const WM_DESTROY = 2 
$Const WM_MOVE = 3 
$Const WM_SIZE = 5 
$Const WM_PAINT = 15 
$Const WM_QUIT = 18 

Dim ps As TPAINTSTRUCT 
Dim hDc As Integer 
Dim hello As String 
hello = "Hello, World!" 

'ShowInt("ShowInt",123456789,10) 
'ShowInt("ShowInt",65535,16) 
'ShowInt("ShowInt",63,2) 

' A KoolB function cannot be used for a Window Procedure 
' or a Callback because the entrance code: 
' PUSH EBP 
' MOV EBP,ESP 
' PUSH EBX 
' PUSH EDI 
' PUSH ESI 
' SUB ESP,4 
' MOV dword[ESP+4],0 
' MOV dword[EBP-4],0 
' effectively destroys the value in ESI. A KoolB Sub 
' (at least one w/o local data) does not include the 
' problem entrance code. 
Sub WindowProc _ 
(hWnd AS Integer, _ 
uMsg AS Integer, _ 
wParam AS Integer, _ 
lParam AS Integer) 

If uMsg = WM_DESTROY Then 
PostQuitMessage(0) 
ElseIf uMsg = WM_CREATE Then 
Return(0) 
ElseIf uMsg = WM_PAINT Then 
hDC = BeginPaint(hWnd,ps) 
TextOut(hDC,20,20,AddressOf(hello),13) 
EndPaint(hWnd,ps) 
Return(0) 
Else 
DefWindowProc(hWnd,uMsg,wParam,lParam) 
End If 
End Sub 

Dim className As String 
className = "KoolB" 
Dim hWnd As Integer 
Dim wcex As TWNDCLASSEX 
Dim lpMsg as Integer 
Dim msg As TMSG 
lpMsg = AddressOf(msg) 
Dim r as Integer 

wcex.cbSize = SizeOf(TWNDCLASSEX) 
wcex.style = CS_VREDRAW+CS_HREDRAW+CS_CLASSDC 
wcex.lpfnwndproc = CodePtr(WindowProc) 
wcex.cbClsExtra = 0 
wcex.cbWndExtra = 0 
wcex.hInstance = GetModuleHandle(0) 
wcex.hIcon = LoadIcon(0,IDI_APPLICATION) 
wcex.hCursor = LoadCursor(0,IDC_ARROW) 
wcex.hbrBackground = COLOR_WINDOW +1 
wcex.lpszMenuName = 0 
wcex.lpszClassName = AddressOf(className) 
wcex.hIconSm = 0 

If (RegisterClassEx(wcex)) = 0 Then 
ShowLastError() 
End If 

' *** KoolB reports that it cannot find the function 
' *** for CreateWindow and CreateWindowA ?? 

title$ = "Hello World window made by KoolB" 
hWnd = CreateWindowEx _ 
(0, _ 
AddressOf(className), _ 
AddressOf(title$), _ 
WS_DEFAULT, _ 
CW_USEDEFAULT, _ 
CW_USEDEFAULT, _ 
CW_USEDEFAULT, _ 
CW_USEDEFAULT, _ 
0, _ 
0, _ 
wcex.hInstance, _ 
0) 

If hWnd = 0 Then 
ShowLastError() 
End If 

' --- Main message loop --- 
WHILE 0=0 
r = GetMessage(lpMsg,0,0,0) 
If r = -1 Then 
ShowLastError() 
End If 
If r = 0 Then 
MsgBox(0,"WM_QUIT","",0) 
UnregisterClass(AddressOf(className),wcex.hInstance) 
ExitProcess(0) 
End If 
TranslateMessage(lpMsg) 
DispatchMessage(lpMsg) 
WEND 
