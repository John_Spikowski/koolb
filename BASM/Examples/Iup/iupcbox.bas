' BASM IUP CBox (fixed postioning example)

$AppType Console
$XPSTYLE
$INCLUDE "String.inc"
$INCLUDE "Convert.inc"
$INCLUDE "IUP.inc"

DIM dlg AS INTEGER
DIM cbox AS INTEGER
DIM frm1 AS INTEGER, frm2 AS INTEGER, frm3 AS INTEGER, frm3a AS INTEGER
DIM vbx1 AS INTEGER, vbx2 AS INTEGER, vbx3 AS INTEGER, vbx3a AS INTEGER
DIM hbox AS INTEGER
DIM mat AS INTEGER
DIM tree AS INTEGER
DIM img AS INTEGER
DIM img1 AS INTEGER, img2 AS INTEGER
DIM imgbits1 AS STRING, imgbits2 AS STRING
DIM list1 AS INTEGER, list2 AS INTEGER, list3 AS INTEGER
DIM but1 AS INTEGER, but2 AS INTEGER, but3 AS INTEGER
DIM lbl1 AS INTEGER, lbl2 AS INTEGER, lbl3 AS INTEGER
DIM tog1 AS INTEGER, tog2 AS INTEGER, tog3 AS INTEGER, tog4 AS INTEGER
DIM rad1 AS INTEGER
DIM cnv1 AS INTEGER
DIM ctrl1 AS INTEGER
DIM text1 AS INTEGER
DIM ml1 AS INTEGER

imgbits1 =  _
 "1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1" & _
",1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1" & _
",1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1" & _
",1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1" & _
",1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,1" & _
",1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,1,1" & _
",1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,1,1,1" & _
",1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1" & _
",2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2" & _
",2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2" & _
",2,2,2,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2" & _
",2,2,2,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2" & _
",2,2,2,0,2,0,2,0,2,2,0,2,2,2,0,0,0,2,2,2,0,0,2,0,2,2,0,0,0,2,2,2" & _
",2,2,2,0,2,0,0,2,0,0,2,0,2,0,2,2,2,0,2,0,2,2,0,0,2,0,2,2,2,0,2,2" & _
",2,2,2,0,2,0,2,2,0,2,2,0,2,2,2,2,2,0,2,0,2,2,2,0,2,0,2,2,2,0,2,2" & _
",2,2,2,0,2,0,2,2,0,2,2,0,2,2,0,0,0,0,2,0,2,2,2,0,2,0,0,0,0,0,2,2" & _
",2,2,2,0,2,0,2,2,0,2,2,0,2,0,2,2,2,0,2,0,2,2,2,0,2,0,2,2,2,2,2,2" & _
",2,2,2,0,2,0,2,2,0,2,2,0,2,0,2,2,2,0,2,0,2,2,0,0,2,0,2,2,2,0,2,2" & _
",2,2,2,0,2,0,2,2,0,2,2,0,2,2,0,0,0,0,2,2,0,0,2,0,2,2,0,0,0,2,2,2" & _
",2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,0,2,2,2,2,2,2,2,2" & _
",2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,0,2,2,2,0,2,2,2,2,2,2,2,2" & _
",2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,0,0,0,2,2,2,2,2,2,2,2,2" & _
",2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2" & _
",2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2" & _
",1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1,1" & _
",1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1" & _
",1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1" & _
",1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1" & _
",1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1" & _
",1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1" & _
",1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1" & _
",2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1"

imgbits2 = _
 "2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,2" & _
",2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,2,2" & _
",2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,2,2,2" & _
",2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,2,2,2,2" & _
",2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,2,2,2,2,2" & _
",2,2,2,2,2,2,2,2,2,2,3,3,3,3,1,1,3,3,3,3,3,3,3,3,3,3,2,2,2,2,2,2" & _
",2,2,2,2,2,2,2,2,2,3,3,3,3,3,1,1,3,3,3,3,3,3,3,3,3,2,2,2,2,2,2,2" & _
",2,2,2,2,2,2,2,2,3,3,3,3,3,3,1,1,3,3,3,3,3,3,3,3,2,2,2,2,2,2,2,2" & _
",3,3,3,3,3,3,3,3,3,3,3,3,3,3,1,1,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3" & _
",3,3,3,3,3,3,3,3,3,3,3,3,3,3,1,1,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3" & _
",3,3,3,0,3,3,3,3,3,3,3,3,3,3,1,1,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3" & _
",3,3,3,0,3,3,3,3,3,3,3,3,3,3,1,1,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3" & _
",3,3,3,0,3,0,3,0,3,3,0,3,3,3,1,1,0,3,3,3,0,0,3,0,3,3,0,0,0,3,3,3" & _
",3,3,3,0,3,0,0,3,0,0,3,0,3,0,1,1,3,0,3,0,3,3,0,0,3,0,3,3,3,0,3,3" & _
",3,3,3,0,3,0,3,3,0,3,3,0,3,3,1,1,3,0,3,0,3,3,3,0,3,0,3,3,3,0,3,3" & _
",1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1" & _
",1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1" & _
",3,3,3,0,3,0,3,3,0,3,3,0,3,0,1,1,3,0,3,0,3,3,0,0,3,0,3,3,3,0,3,3" & _
",3,3,3,0,3,0,3,3,0,3,3,0,3,3,1,1,0,0,3,3,0,0,3,0,3,3,0,0,0,3,3,3" & _
",3,3,3,3,3,3,3,3,3,3,3,3,3,3,1,1,3,3,3,3,3,3,3,0,3,3,3,3,3,3,3,3" & _
",3,3,3,3,3,3,3,3,3,3,3,3,3,3,1,1,3,3,3,0,3,3,3,0,3,3,3,3,3,3,3,3" & _
",3,3,3,3,3,3,3,3,3,3,3,3,3,3,1,1,3,3,3,3,0,0,0,3,3,3,3,3,3,3,3,3" & _
",3,3,3,3,3,3,3,3,3,3,3,3,3,3,1,1,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3" & _
",3,3,3,3,3,3,3,3,3,3,3,3,3,3,1,1,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3" & _
",2,2,2,2,2,2,2,3,3,3,3,3,3,3,1,1,3,3,3,3,3,2,2,2,2,2,2,2,2,2,2,2" & _
",2,2,2,2,2,2,3,3,3,3,3,3,3,3,1,1,3,3,3,3,2,2,2,2,2,2,2,2,2,2,2,2" & _
",2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,2,2,2,2,2,2,2,2,2,2,2,2,2" & _
",2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2" & _
",2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2" & _
",2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2" & _
",2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2" & _
",3,3,3,3,3,3,3,3,3,3,3,3,3,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2"

IupOpen(0,0)
IupControlsOpen()

mat = IupMatrix("")

IupSetStrAttribute(mat, "NUMCOL", "1")
IupSetStrAttribute(mat, "NUMLIN", "3")
IupSetStrAttribute(mat, "NUMCOL_VISIBLE", "1")
IupSetStrAttribute(mat, "NUMLIN_VISIBLE", "3")
IupSetStrAttribute(mat, "EXPAND", "NO")
IupSetStrAttribute(mat, "SCROLLBAR", "NO")

IupSetStrAttribute(mat, "0:0", "Inflation")
IupSetStrAttribute(mat, "1:0", "Medicine ")
IupSetStrAttribute(mat, "2:0", "Food")
IupSetStrAttribute(mat, "3:0", "Energy")
IupSetStrAttribute(mat, "0:1", "January 2000")
IupSetStrAttribute(mat, "1:1", "5.6")
IupSetStrAttribute(mat, "2:1", "2.2")
IupSetStrAttribute(mat, "3:1", "7.2")

IupSetStrAttribute(mat, "BGCOLOR", "255 255 255")
IupSetStrAttribute(mat, "BGCOLOR1:0", "255 128 0")
IupSetStrAttribute(mat, "BGCOLOR2:1", "255 128 0")
IupSetStrAttribute(mat, "FGCOLOR2:0", "255 0 128")
IupSetStrAttribute(mat, "FGCOLOR1:1", "255 0 128")

IupSetStrAttribute(mat, "CX", "600")
IupSetStrAttribute(mat, "CY", "250")

tree = IupTree()
IupSetAttributes(tree, _
  "FONT=COURIER_NORMAL_10, " & _
  "NAME=Figures, " & _
  "ADDBRANCH=3D, " & _
  "ADDBRANCH=2D, " & _
  "ADDLEAF1=trapeze, " & _
  "ADDBRANCH1=parallelogram, " & _
  "ADDLEAF2=diamond, " & _
  "ADDLEAF2=square, " & _
  "ADDBRANCH4=triangle, " & _
  "ADDLEAF5=scalenus, " & _
  "ADDLEAF5=isosceles, " & _
  "ADDLEAF5=equilateral, " & _
  "RASTERSIZE=180x180, " & _
  "VALUE=6, " & _
  "CTRL=ON, " & _
  "SHIFT=ON, " & _
  "CX=600, " & _
  "CY=10, " & _
  "ADDEXPANDED=NO")
' img1bin = CreateImg(imgbits1)
img1 = IupImage(32, 32, CreateImg(imgbits1, 0))
IupSetHandle ("img1", img1)
IupSetStrAttribute (img1, "1", "0 0 0")
IupSetStrAttribute (img1, "2", "BGCOLOR")
IupSetStrAttribute (img1, "3", "255 0 0")

' img2bin = CreateImg(imgbits2)
img2 = IupImage(32, 32, CreateImg(imgbits2, 0))
IupSetHandle ("img2", img2)
IupSetStrAttribute (img2, "1", "0 0 0")
IupSetStrAttribute (img2, "2", "0 255 0")
IupSetStrAttribute (img2, "3", "BGCOLOR")
IupSetStrAttribute (img2, "4", "255 0 0")

frm1 = IupFrame(0)
vbx1 = IupVbox(0)
but1 = IupButton("Button Text", "")
IupSetAttributes(but1, "CINDEX=1")
but2 = IupButton("", "")
IupSetAttributes(but2, "IMAGE=img1, CINDEX=2")
but3 =IupButton("", "")
IupSetAttributes(but3, "IMAGE=img1, IMPRESS=img2, CINDEX=3")
IupAppend(vbx1, but1)
IupAppend(vbx1, but2)
IupAppend(vbx1, but3)
IupAppend(frm1, vbx1)
IupSetStrAttribute(frm1, "TITLE", "IupButton")
IupSetStrAttribute(frm1, "CX", "10")
IupSetStrAttribute(frm1, "CY", "180")

frm2 = IupFrame(0)
vbx2 = IupVbox(0)
lbl1 = IupLabel("Label Text")
IupSetAttributes(lbl1, "CINDEX=1")
lbl2 = IupLabel("")
IupSetAttributes(lbl2, "SEPARATOR=HORIZONTAL,CINDEX=2")
lbl3 = IupLabel("")
IupSetAttributes(lbl3, "IMAGE=img1,CINDEX=3")
IupAppend(vbx2, lbl1)
IupAppend(vbx2, lbl2)
IupAppend(vbx2, lbl3)
IupAppend(frm2, vbx2)
IupSetStrAttribute(frm2, "TITLE","IupLabel")
IupSetStrAttribute(frm2, "CX","200")
IupSetStrAttribute(frm2, "CY","250")

frm3 = IupFrame(0)
vbx3 = IupVbox(0)
tog1 = IupToggle("Toggle Text", "")
IupSetAttributes(tog1, "VALUE=ON,CINDEX=1")
tog2 = IupToggle("", "")
IupSetAttributes(tog2, "IMAGE=img1,IMPRESS=img2,CINDEX=2")
frm3a = IupFrame(0)
rad1 = IupRadio(0)
vbx3a = IupVbox(0)
IupSetAttributes(vbx3a, "TITLE=IupRadio")
tog3 = IupToggle("Toggle Text", "")
IupSetAttributes(tog3, "CINDEX=3")
tog4 = IupToggle("Toggle Text", "")
IupSetAttributes(tog4, "CINDEX=4")
IupAppend(vbx3, tog1)
IupAppend(vbx3, tog2)
IupAppend(vbx3, frm3a)
IupAppend(vbx3, rad1)
IupAppend(vbx3, vbx3a)
IupAppend(vbx3, tog3)
IupAppend(vbx3, tog4)
IupAppend(frm3, vbx3)
IupSetStrAttribute(frm3, "TITLE", "IupToggle")
IupSetStrAttribute(frm3, "CX", "400")
IupSetStrAttribute(frm3, "CY", "250")

text1 = IupText("")
IupSetStrAttribute(text1,"VALUE", "IupText Text")
IupSetStrAttribute(text1,"SIZE", "80x")
IupSetStrAttribute(text1,"CINDEX", "1")
IupSetStrAttribute(text1,"CX", "10")
IupSetStrAttribute(text1,"CY", "100")

ml1 = IupMultiLine("")
IupSetStrAttribute(ml1, "VALUE", "IupMultiline Text\nSecond Line\nThird Line")
IupSetStrAttribute(ml1, "SIZE", "80x60")
IupSetStrAttribute(ml1, "CINDEX", "1")
IupSetStrAttribute(ml1, "CX", "200")
IupSetStrAttribute(ml1, "CY", "100")

list1 = IupList("")
IupSetStrAttribute(list1, "VALUE", "1")
IupSetStrAttribute(list1, "1", "Item 1 Text")
IupSetStrAttribute(list1, "2", "Item 2 Text")
IupSetStrAttribute(list1, "3", "Item 3 Text")
IupSetStrAttribute(list1, "CINDEX", "1")
IupSetStrAttribute(list1, "CX", "10")
IupSetStrAttribute(list1, "CY", "10")

list2 = IupList("")
IupSetStrAttribute(list2, "DROPDOWN", "YES")
IupSetStrAttribute(list2, "VALUE", "2")
IupSetStrAttribute(list2, "1" ,"Item 1 Text")
IupSetStrAttribute(list2, "2" ,"Item 2 Text")
IupSetStrAttribute(list2, "3" ,"Item 3 Text")
IupSetStrAttribute(list2, "CINDEX", "2")
IupSetStrAttribute(list2, "CX", "200")
IupSetStrAttribute(list2, "CY", "10")

list3 = IupList("")
IupSetStrAttribute(list3, "EDITBOX", "YES")
IupSetStrAttribute(list3, "VALUE", "3")
IupSetStrAttribute(list3, "1", "Item 1 Text")
IupSetStrAttribute(list3, "2", "Item 2 Text")
IupSetStrAttribute(list3, "3", "Item 3 Text")
IupSetStrAttribute(list3, "CINDEX", "3")
IupSetStrAttribute(list3, "CX", "400")
IupSetStrAttribute(list3, "CY", "10")

cnv1 = IupCanvas("")
IupSetStrAttribute(cnv1, "RASTERSIZE", "100x100")
IupSetStrAttribute(cnv1, "POSX", "0")
IupSetStrAttribute(cnv1, "POSY", "0")
IupSetStrAttribute(cnv1, "BGCOLOR", "128 255 0")
IupSetStrAttribute(cnv1, "CX", "400")
IupSetStrAttribute(cnv1, "CY", "150")

ctrl1 = IupVal("")
IupSetStrAttribute(ctrl1, "CX", "600")
IupSetStrAttribute(ctrl1, "CY", "200")
cbox = IupCbox(0)
IupAppend(cbox, text1)
IupAppend(cbox, ml1)
IupAppend(cbox, list1)
IupAppend(cbox, list2)
IupAppend(cbox, list3)
IupAppend(cbox, cnv1)
IupAppend(cbox, ctrl1)
IupAppend(cbox, tree)
IupAppend(cbox, mat)
IupAppend(cbox, frm1)
IupAppend(cbox, frm1)
IupAppend(cbox, frm2)
IupAppend(cbox, frm3)
IupSetStrAttribute(cbox, "SIZE", "480x200")

hbox = IupHbox(0)
IupAppend(hbox, cbox)
IupSetAttributes(hbox, "MARGIN=10x10")
dlg = IupDialog(0)
IupAppend(dlg, hbox)
IupSetHandle("dlg", dlg)
IupSetStrAttribute(dlg, "TITLE", "BASM IUPCbox")
IupShow(dlg)
' IupShowXY(IupGetHandle("dlg"), IUP_CENTER, IUP_CENTER)

IupMainLoop()
IupClose()
