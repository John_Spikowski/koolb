;Library functions to import to the Chameleon app
extern ExitProcess
extern MessageBoxA
extern lstrcat
extern lstrlen
extern lstrcpy
extern HeapCreate
extern HeapAlloc
extern HeapFree
extern GetModuleHandleA
extern GetCommandLineA
extern GetStdHandle
extern system
extern lstrlenA
extern lstrcpyA
extern WriteFile
extern Sleep
extern floor
extern pow
extern _gcvt
extern ReadFile
extern RtlMoveMemory
extern atof
extern lstrcatA
extern HeapDestroy



;Initialize everything to prep the app to run
section .text
%include "C:\BASM\Bin\MACROS.INC"
global START
START:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH ESI
PUSH EDI
stdcall HeapCreate,0,16384,0
MOV dword[HandleToHeap],EAX
CMP dword[HandleToHeap],0
JNE Label1
JMP NoMemory
Label1:
stdcall GetModuleHandleA,0
MOV dword[Internal_HInstance],EAX
CALL GetCommandLineA
MOV dword[Internal_CommandLine],EAX
stdcall GetStdHandle,-10
MOV dword[HandleToInput],EAX
stdcall GetStdHandle,-11
MOV dword[HandleToOutput],EAX
CMP dword[HandleToInput],-1
JNE Label2
JMP NoConsole
Label2:
CMP dword[HandleToOutput],-1
JNE Label3
JMP NoConsole
Label3:
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label4
JMP NoMemory
Label4:
MOV dword[Scope0__EMPTYLINE_String],EAX
MOV byte[EAX],0
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label41
JMP NoMemory
Label41:
MOV dword[Scope0__WORTHLESSVARIABLE____String],EAX
MOV byte[EAX],0
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label129
JMP NoMemory
Label129:
MOV dword[Scope0__A____String],EAX
MOV byte[EAX],0



;The main body where the app actually runs
ccall system,ConsoleClear
stdcall lstrlenA,dword[Scope0__EMPTYLINE_String]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label5
JMP NoMemory
Label5:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[Scope0__EMPTYLINE_String]
PUSH EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
stdcall lstrlenA,dword[Scope0__EMPTYLINE_String]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label6
JMP NoMemory
Label6:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[Scope0__EMPTYLINE_String]
PUSH EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
stdcall lstrlenA,dword[Scope0__EMPTYLINE_String]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label7
JMP NoMemory
Label7:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[Scope0__EMPTYLINE_String]
PUSH EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
stdcall lstrlenA,dword[Scope0__EMPTYLINE_String]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label8
JMP NoMemory
Label8:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[Scope0__EMPTYLINE_String]
PUSH EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
stdcall lstrlenA,dword[Scope0__EMPTYLINE_String]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label9
JMP NoMemory
Label9:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[Scope0__EMPTYLINE_String]
PUSH EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
stdcall lstrlenA,dword[Scope0__EMPTYLINE_String]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label10
JMP NoMemory
Label10:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[Scope0__EMPTYLINE_String]
PUSH EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
stdcall lstrlenA,dword[Scope0__EMPTYLINE_String]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label11
JMP NoMemory
Label11:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[Scope0__EMPTYLINE_String]
PUSH EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
stdcall lstrlenA,dword[Scope0__EMPTYLINE_String]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label12
JMP NoMemory
Label12:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[Scope0__EMPTYLINE_String]
PUSH EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
stdcall lstrlenA,dword[Scope0__EMPTYLINE_String]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label13
JMP NoMemory
Label13:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[Scope0__EMPTYLINE_String]
PUSH EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_1
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label14
JMP NoMemory
Label14:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_1
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_2
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label15
JMP NoMemory
Label15:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_2
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_3
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label16
JMP NoMemory
Label16:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_3
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_4
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label17
JMP NoMemory
Label17:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_4
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_5
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label18
JMP NoMemory
Label18:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_5
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_6
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label19
JMP NoMemory
Label19:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_6
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_7+4]
PUSH dword[Number_7]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
ccall system,ConsoleClear
PUSH dword[Number_8+4]
PUSH dword[Number_8]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope0__MYINFO_UDT+Scope0__PERSONALINFO_TYPE.Scope0__AGE_Integer]
MOV EBX,String_9
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label20
JMP NoMemory
Label20:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_9
PUSH EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[Scope0__MYINFO_UDT+Scope0__PERSONALINFO_TYPE.Scope0__NAME_String]
POP ESI
MOV dword[Scope0__MYINFO_UDT+Scope0__PERSONALINFO_TYPE.Scope0__NAME_String],ESI
PUSH dword[Number_10+4]
PUSH dword[Number_10]
PUSH dword[Number_11+4]
PUSH dword[Number_11]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
ccall pow,dword[TempQWord2],dword[TempQWord2+4],dword[TempQWord1],dword[TempQWord1+4]
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[Scope0__MYINFO_UDT+Scope0__PERSONALINFO_TYPE.Scope0__FAVNUM_Double]
POP dword[Scope0__MYINFO_UDT+Scope0__PERSONALINFO_TYPE.Scope0__FAVNUM_Double+4]
MOV EBX,String_12
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label21
JMP NoMemory
Label21:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_12
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_13+4]
PUSH dword[Number_13]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
MOV EBX,String_14
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label22
JMP NoMemory
Label22:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_14
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
MOV ESI,dword[Scope0__MYINFO_UDT+Scope0__PERSONALINFO_TYPE.Scope0__NAME_String]
stdcall lstrlenA,ESI
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label23
JMP NoMemory
Label23:
MOV EDI,EAX
stdcall lstrcpyA,EDI,ESI
PUSH EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
MOV EBX,String_15
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label24
JMP NoMemory
Label24:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_15
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_16+4]
PUSH dword[Number_16]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
MOV EBX,String_17
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label25
JMP NoMemory
Label25:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_17
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
FINIT
FILD dword[Scope0__MYINFO_UDT+Scope0__PERSONALINFO_TYPE.Scope0__AGE_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label29
JMP NoMemory
Label29:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label26:
CMP byte[EBX+ECX],0
JE Label27
INC ECX
CMP ECX,EDI
JL Label26
JMP Label28
Label27:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label28
MOV byte[EBX+ECX],0
Label28:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
MOV EBX,String_18
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label30
JMP NoMemory
Label30:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_18
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_19+4]
PUSH dword[Number_19]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
MOV EBX,String_20
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label31
JMP NoMemory
Label31:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_20
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
PUSH dword[Scope0__MYINFO_UDT+Scope0__PERSONALINFO_TYPE.Scope0__FAVNUM_Double+4]
PUSH dword[Scope0__MYINFO_UDT+Scope0__PERSONALINFO_TYPE.Scope0__FAVNUM_Double]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label35
JMP NoMemory
Label35:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label32:
CMP byte[EBX+ECX],0
JE Label33
INC ECX
CMP ECX,EDI
JL Label32
JMP Label34
Label33:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label34
MOV byte[EBX+ECX],0
Label34:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
MOV EBX,String_21
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label36
JMP NoMemory
Label36:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_21
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
stdcall lstrlenA,dword[Scope0__EMPTYLINE_String]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label37
JMP NoMemory
Label37:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[Scope0__EMPTYLINE_String]
PUSH EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_22+4]
PUSH dword[Number_22]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
MOV EBX,String_23
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label38
JMP NoMemory
Label38:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_23
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapAlloc,dword[HandleToHeap],8,16384
CMP EAX,0
JNE Label39
JMP NoMemory
Label39:
MOV EBX,EAX
stdcall ReadFile,dword[HandleToInput],EBX,16383,ConsoleTemp,0
stdcall lstrlenA,EBX
INC EAX
DEC EAX
DEC EAX
DEC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label40
JMP NoMemory
Label40:
MOV ESI,EAX
stdcall RtlMoveMemory,ESI,EBX,EDI
MOV byte[ESI+EDI],0
PUSH ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[Scope0__WORTHLESSVARIABLE____String]
MOV dword[Scope0__WORTHLESSVARIABLE____String],EBX
MOV EBX,String_24
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label42
JMP NoMemory
Label42:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_24
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
stdcall lstrlenA,dword[Scope0__EMPTYLINE_String]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label43
JMP NoMemory
Label43:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[Scope0__EMPTYLINE_String]
PUSH EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_25+4]
PUSH dword[Number_25]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
stdcall lstrlenA,dword[Scope0__EMPTYLINE_String]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label44
JMP NoMemory
Label44:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[Scope0__EMPTYLINE_String]
PUSH EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_26
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label45
JMP NoMemory
Label45:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_26
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_27+4]
PUSH dword[Number_27]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
MOV EBX,String_28
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label46
JMP NoMemory
Label46:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_28
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_29+4]
PUSH dword[Number_29]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
MOV EBX,String_30
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label47
JMP NoMemory
Label47:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_30
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapAlloc,dword[HandleToHeap],8,16384
CMP EAX,0
JNE Label48
JMP NoMemory
Label48:
MOV EBX,EAX
stdcall ReadFile,dword[HandleToInput],EBX,16383,ConsoleTemp,0
stdcall lstrlenA,EBX
INC EAX
DEC EAX
DEC EAX
DEC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label49
JMP NoMemory
Label49:
MOV ESI,EAX
stdcall RtlMoveMemory,ESI,EBX,EDI
MOV byte[ESI+EDI],0
PUSH ESI
stdcall HeapFree,dword[HandleToHeap],0,dword[Scope0__YOURINFO_UDT+Scope0__PERSONALINFO_TYPE.Scope0__NAME_String]
POP ESI
MOV dword[Scope0__YOURINFO_UDT+Scope0__PERSONALINFO_TYPE.Scope0__NAME_String],ESI
MOV EBX,String_31
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label50
JMP NoMemory
Label50:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_31
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapAlloc,dword[HandleToHeap],8,16384
CMP EAX,0
JNE Label51
JMP NoMemory
Label51:
MOV EBX,EAX
stdcall ReadFile,dword[HandleToInput],EBX,16383,ConsoleTemp,0
stdcall lstrlenA,EBX
INC EAX
DEC EAX
DEC EAX
DEC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label52
JMP NoMemory
Label52:
MOV ESI,EAX
stdcall RtlMoveMemory,ESI,EBX,EDI
MOV byte[ESI+EDI],0
PUSH ESI
POP EBX
ccall atof,EBX
stdcall HeapFree,dword[HandleToHeap],0,EBX
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[Scope0__YOURINFO_UDT+Scope0__PERSONALINFO_TYPE.Scope0__AGE_Integer]
MOV EBX,String_32
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label53
JMP NoMemory
Label53:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_32
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapAlloc,dword[HandleToHeap],8,16384
CMP EAX,0
JNE Label54
JMP NoMemory
Label54:
MOV EBX,EAX
stdcall ReadFile,dword[HandleToInput],EBX,16383,ConsoleTemp,0
stdcall lstrlenA,EBX
INC EAX
DEC EAX
DEC EAX
DEC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label55
JMP NoMemory
Label55:
MOV ESI,EAX
stdcall RtlMoveMemory,ESI,EBX,EDI
MOV byte[ESI+EDI],0
PUSH ESI
POP EBX
ccall atof,EBX
stdcall HeapFree,dword[HandleToHeap],0,EBX
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[Scope0__YOURINFO_UDT+Scope0__PERSONALINFO_TYPE.Scope0__FAVNUM_Double]
POP dword[Scope0__YOURINFO_UDT+Scope0__PERSONALINFO_TYPE.Scope0__FAVNUM_Double+4]
stdcall lstrlenA,dword[Scope0__EMPTYLINE_String]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label56
JMP NoMemory
Label56:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[Scope0__EMPTYLINE_String]
PUSH EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_33
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label57
JMP NoMemory
Label57:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_33
PUSH EBX
MOV ESI,dword[Scope0__YOURINFO_UDT+Scope0__PERSONALINFO_TYPE.Scope0__NAME_String]
stdcall lstrlenA,ESI
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label58
JMP NoMemory
Label58:
MOV EDI,EAX
stdcall lstrcpyA,EDI,ESI
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label59
JMP NoMemory
Label59:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
MOV EBX,String_34
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label60
JMP NoMemory
Label60:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_34
PUSH EBX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label61
JMP NoMemory
Label61:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_35+4]
PUSH dword[Number_35]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
MOV EBX,String_36
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label62
JMP NoMemory
Label62:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_36
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
PUSH dword[Scope0__YOURINFO_UDT+Scope0__PERSONALINFO_TYPE.Scope0__FAVNUM_Double+4]
PUSH dword[Scope0__YOURINFO_UDT+Scope0__PERSONALINFO_TYPE.Scope0__FAVNUM_Double]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label66
JMP NoMemory
Label66:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label63:
CMP byte[EBX+ECX],0
JE Label64
INC ECX
CMP ECX,EDI
JL Label63
JMP Label65
Label64:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label65
MOV byte[EBX+ECX],0
Label65:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
MOV EBX,String_37
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label67
JMP NoMemory
Label67:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_37
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
PUSH dword[Scope0__YOURINFO_UDT+Scope0__PERSONALINFO_TYPE.Scope0__FAVNUM_Double+4]
PUSH dword[Scope0__YOURINFO_UDT+Scope0__PERSONALINFO_TYPE.Scope0__FAVNUM_Double]
FINIT
FILD dword[Scope0__YOURINFO_UDT+Scope0__PERSONALINFO_TYPE.Scope0__AGE_Integer]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label71
JMP NoMemory
Label71:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label68:
CMP byte[EBX+ECX],0
JE Label69
INC ECX
CMP ECX,EDI
JL Label68
JMP Label70
Label69:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label70
MOV byte[EBX+ECX],0
Label70:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
MOV EBX,String_38
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label72
JMP NoMemory
Label72:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_38
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_39+4]
PUSH dword[Number_39]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
PUSH dword[Number_40+4]
PUSH dword[Number_40]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
INC EBX
MOV dword[Scope0__RANDOMNUMBERS_Double_Size],EBX
MOV EAX,8
MUL EBX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label73
JMP NoMemory
Label73:
MOV dword[Scope0__RANDOMNUMBERS_Double],EAX
stdcall lstrlenA,dword[Scope0__EMPTYLINE_String]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label74
JMP NoMemory
Label74:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[Scope0__EMPTYLINE_String]
PUSH EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_41+4]
PUSH dword[Number_41]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
MOV EBX,String_42
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label75
JMP NoMemory
Label75:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_42
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_43
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label76
JMP NoMemory
Label76:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_43
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapAlloc,dword[HandleToHeap],8,16384
CMP EAX,0
JNE Label77
JMP NoMemory
Label77:
MOV EBX,EAX
stdcall ReadFile,dword[HandleToInput],EBX,16383,ConsoleTemp,0
stdcall lstrlenA,EBX
INC EAX
DEC EAX
DEC EAX
DEC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label78
JMP NoMemory
Label78:
MOV ESI,EAX
stdcall RtlMoveMemory,ESI,EBX,EDI
MOV byte[ESI+EDI],0
PUSH ESI
PUSH dword[Number_44+4]
PUSH dword[Number_44]
POP EBX
POP EDI
POP ESI
PUSH EDI
PUSH EBX
PUSH ESI
POP EBX
ccall atof,EBX
stdcall HeapFree,dword[HandleToHeap],0,EBX
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
CMP EBX,dword[Scope0__RANDOMNUMBERS_Double_Size]
JGE Label79
CMP EBX,0
JLE Label79
MOV EDI,dword[Scope0__RANDOMNUMBERS_Double]
FINIT
FLD qword[TempQWord1]
FST qword[EDI+EBX*8]
Label79:
MOV EBX,String_45
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label80
JMP NoMemory
Label80:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_45
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapAlloc,dword[HandleToHeap],8,16384
CMP EAX,0
JNE Label81
JMP NoMemory
Label81:
MOV EBX,EAX
stdcall ReadFile,dword[HandleToInput],EBX,16383,ConsoleTemp,0
stdcall lstrlenA,EBX
INC EAX
DEC EAX
DEC EAX
DEC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label82
JMP NoMemory
Label82:
MOV ESI,EAX
stdcall RtlMoveMemory,ESI,EBX,EDI
MOV byte[ESI+EDI],0
PUSH ESI
PUSH dword[Number_46+4]
PUSH dword[Number_46]
POP EBX
POP EDI
POP ESI
PUSH EDI
PUSH EBX
PUSH ESI
POP EBX
ccall atof,EBX
stdcall HeapFree,dword[HandleToHeap],0,EBX
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
CMP EBX,dword[Scope0__RANDOMNUMBERS_Double_Size]
JGE Label83
CMP EBX,0
JLE Label83
MOV EDI,dword[Scope0__RANDOMNUMBERS_Double]
FINIT
FLD qword[TempQWord1]
FST qword[EDI+EBX*8]
Label83:
MOV EBX,String_47
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label84
JMP NoMemory
Label84:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_47
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapAlloc,dword[HandleToHeap],8,16384
CMP EAX,0
JNE Label85
JMP NoMemory
Label85:
MOV EBX,EAX
stdcall ReadFile,dword[HandleToInput],EBX,16383,ConsoleTemp,0
stdcall lstrlenA,EBX
INC EAX
DEC EAX
DEC EAX
DEC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label86
JMP NoMemory
Label86:
MOV ESI,EAX
stdcall RtlMoveMemory,ESI,EBX,EDI
MOV byte[ESI+EDI],0
PUSH ESI
PUSH dword[Number_48+4]
PUSH dword[Number_48]
POP EBX
POP EDI
POP ESI
PUSH EDI
PUSH EBX
PUSH ESI
POP EBX
ccall atof,EBX
stdcall HeapFree,dword[HandleToHeap],0,EBX
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
CMP EBX,dword[Scope0__RANDOMNUMBERS_Double_Size]
JGE Label87
CMP EBX,0
JLE Label87
MOV EDI,dword[Scope0__RANDOMNUMBERS_Double]
FINIT
FLD qword[TempQWord1]
FST qword[EDI+EBX*8]
Label87:
MOV EBX,String_49
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label88
JMP NoMemory
Label88:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_49
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapAlloc,dword[HandleToHeap],8,16384
CMP EAX,0
JNE Label89
JMP NoMemory
Label89:
MOV EBX,EAX
stdcall ReadFile,dword[HandleToInput],EBX,16383,ConsoleTemp,0
stdcall lstrlenA,EBX
INC EAX
DEC EAX
DEC EAX
DEC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label90
JMP NoMemory
Label90:
MOV ESI,EAX
stdcall RtlMoveMemory,ESI,EBX,EDI
MOV byte[ESI+EDI],0
PUSH ESI
PUSH dword[Number_50+4]
PUSH dword[Number_50]
POP EBX
POP EDI
POP ESI
PUSH EDI
PUSH EBX
PUSH ESI
POP EBX
ccall atof,EBX
stdcall HeapFree,dword[HandleToHeap],0,EBX
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
CMP EBX,dword[Scope0__RANDOMNUMBERS_Double_Size]
JGE Label91
CMP EBX,0
JLE Label91
MOV EDI,dword[Scope0__RANDOMNUMBERS_Double]
FINIT
FLD qword[TempQWord1]
FST qword[EDI+EBX*8]
Label91:
MOV EBX,String_51
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label92
JMP NoMemory
Label92:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_51
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapAlloc,dword[HandleToHeap],8,16384
CMP EAX,0
JNE Label93
JMP NoMemory
Label93:
MOV EBX,EAX
stdcall ReadFile,dword[HandleToInput],EBX,16383,ConsoleTemp,0
stdcall lstrlenA,EBX
INC EAX
DEC EAX
DEC EAX
DEC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label94
JMP NoMemory
Label94:
MOV ESI,EAX
stdcall RtlMoveMemory,ESI,EBX,EDI
MOV byte[ESI+EDI],0
PUSH ESI
PUSH dword[Number_52+4]
PUSH dword[Number_52]
POP EBX
POP EDI
POP ESI
PUSH EDI
PUSH EBX
PUSH ESI
POP EBX
ccall atof,EBX
stdcall HeapFree,dword[HandleToHeap],0,EBX
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
CMP EBX,dword[Scope0__RANDOMNUMBERS_Double_Size]
JGE Label95
CMP EBX,0
JLE Label95
MOV EDI,dword[Scope0__RANDOMNUMBERS_Double]
FINIT
FLD qword[TempQWord1]
FST qword[EDI+EBX*8]
Label95:
PUSH dword[Number_53+4]
PUSH dword[Number_53]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
CMP EBX,dword[Scope0__RANDOMNUMBERS_Double_Size]
JGE Label96
CMP EBX,0
JLE Label96
MOV EDI,dword[Scope0__RANDOMNUMBERS_Double]
FINIT
FLD qword[EDI+EBX*8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
JMP Label97
Label96:
FINIT
FLDZ
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
Label97:
PUSH dword[Number_54+4]
PUSH dword[Number_54]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
CMP EBX,dword[Scope0__RANDOMNUMBERS_Double_Size]
JGE Label98
CMP EBX,0
JLE Label98
MOV EDI,dword[Scope0__RANDOMNUMBERS_Double]
FINIT
FLD qword[EDI+EBX*8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
JMP Label99
Label98:
FINIT
FLDZ
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
Label99:
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_55+4]
PUSH dword[Number_55]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
CMP EBX,dword[Scope0__RANDOMNUMBERS_Double_Size]
JGE Label100
CMP EBX,0
JLE Label100
MOV EDI,dword[Scope0__RANDOMNUMBERS_Double]
FINIT
FLD qword[EDI+EBX*8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
JMP Label101
Label100:
FINIT
FLDZ
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
Label101:
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_56+4]
PUSH dword[Number_56]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
CMP EBX,dword[Scope0__RANDOMNUMBERS_Double_Size]
JGE Label102
CMP EBX,0
JLE Label102
MOV EDI,dword[Scope0__RANDOMNUMBERS_Double]
FINIT
FLD qword[EDI+EBX*8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
JMP Label103
Label102:
FINIT
FLDZ
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
Label103:
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_57+4]
PUSH dword[Number_57]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP EBX
CMP EBX,dword[Scope0__RANDOMNUMBERS_Double_Size]
JGE Label104
CMP EBX,0
JLE Label104
MOV EDI,dword[Scope0__RANDOMNUMBERS_Double]
FINIT
FLD qword[EDI+EBX*8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
JMP Label105
Label104:
FINIT
FLDZ
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
Label105:
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FST qword[Scope0__SUM___Double]
PUSH dword[Scope0__SUM___Double+4]
PUSH dword[Scope0__SUM___Double]
PUSH dword[Number_58+4]
PUSH dword[Number_58]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FDIV ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FST qword[Scope0__AVERAGE___Double]
stdcall lstrlenA,dword[Scope0__EMPTYLINE_String]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label106
JMP NoMemory
Label106:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[Scope0__EMPTYLINE_String]
PUSH EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_59+4]
PUSH dword[Number_59]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
MOV EBX,String_60
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label107
JMP NoMemory
Label107:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_60
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_61+4]
PUSH dword[Number_61]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
MOV EBX,String_62
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label108
JMP NoMemory
Label108:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_62
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
PUSH dword[Scope0__SUM___Double+4]
PUSH dword[Scope0__SUM___Double]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label112
JMP NoMemory
Label112:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label109:
CMP byte[EBX+ECX],0
JE Label110
INC ECX
CMP ECX,EDI
JL Label109
JMP Label111
Label110:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label111
MOV byte[EBX+ECX],0
Label111:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_63+4]
PUSH dword[Number_63]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
MOV EBX,String_64
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label113
JMP NoMemory
Label113:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_64
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
PUSH dword[Scope0__AVERAGE___Double+4]
PUSH dword[Scope0__AVERAGE___Double]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label117
JMP NoMemory
Label117:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label114:
CMP byte[EBX+ECX],0
JE Label115
INC ECX
CMP ECX,EDI
JL Label114
JMP Label116
Label115:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label116
MOV byte[EBX+ECX],0
Label116:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_65+4]
PUSH dword[Number_65]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
MOV EBX,String_66
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label118
JMP NoMemory
Label118:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_66
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
PUSH dword[Scope0__SUM___Double+4]
PUSH dword[Scope0__SUM___Double]
PUSH dword[Scope0__AVERAGE___Double+4]
PUSH dword[Scope0__AVERAGE___Double]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FMUL ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_67+4]
PUSH dword[Number_67]
PUSH dword[Number_68+4]
PUSH dword[Number_68]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
ccall pow,dword[TempQWord2],dword[TempQWord2+4],dword[TempQWord1],dword[TempQWord1+4]
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FDIV ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_69+4]
PUSH dword[Number_69]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label122
JMP NoMemory
Label122:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label119:
CMP byte[EBX+ECX],0
JE Label120
INC ECX
CMP ECX,EDI
JL Label119
JMP Label121
Label120:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label121
MOV byte[EBX+ECX],0
Label121:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_70+4]
PUSH dword[Number_70]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
stdcall lstrlenA,dword[Scope0__EMPTYLINE_String]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label123
JMP NoMemory
Label123:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[Scope0__EMPTYLINE_String]
PUSH EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
stdcall lstrlenA,dword[Scope0__EMPTYLINE_String]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label124
JMP NoMemory
Label124:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[Scope0__EMPTYLINE_String]
PUSH EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_71
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label125
JMP NoMemory
Label125:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_71
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_72
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label126
JMP NoMemory
Label126:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_72
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapAlloc,dword[HandleToHeap],8,16384
CMP EAX,0
JNE Label127
JMP NoMemory
Label127:
MOV EBX,EAX
stdcall ReadFile,dword[HandleToInput],EBX,16383,ConsoleTemp,0
stdcall lstrlenA,EBX
INC EAX
DEC EAX
DEC EAX
DEC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label128
JMP NoMemory
Label128:
MOV ESI,EAX
stdcall RtlMoveMemory,ESI,EBX,EDI
MOV byte[ESI+EDI],0
PUSH ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[Scope0__A____String]
MOV dword[Scope0__A____String],EBX



;Prepare the app to exit and then terminate
Exit:
stdcall HeapFree,dword[HandleToHeap],0,dword[Scope0__EMPTYLINE_String]
stdcall HeapFree,dword[HandleToHeap],0,dword[Scope0__WORTHLESSVARIABLE____String]
stdcall HeapFree,dword[HandleToHeap],0,dword[Scope0__RANDOMNUMBERS_Double]
stdcall HeapFree,dword[HandleToHeap],0,dword[Scope0__A____String]
stdcall HeapDestroy,dword[HandleToHeap]
POP EDI
POP ESI
POP EBX
MOV ESP,EBP
POP EBP
MOV EAX,dword[ExitStatus]
stdcall ExitProcess,dword[ExitStatus]
RET

NoMemory:
MOV dword[ExitStatus],1
stdcall MessageBoxA,0,NoMemMessage,Error,0
JMP Exit

NoLibrary:
MOV dword[ExitStatus],1
POP EBX
stdcall lstrlen,EBX
ADD EAX,30
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
MOV EDI,EAX
stdcall lstrcpy,EDI,NoLibFound
stdcall lstrcat,EDI,EBX
stdcall MessageBoxA,0,EDI,Error,0
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
JMP Exit

NoFunction:
MOV dword[ExitStatus],1
POP EBX
stdcall lstrlen,EBX
ADD EAX,30
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
MOV EDI,EAX
stdcall lstrcpy,EDI,NoFunctionFound
stdcall lstrcat,EDI,EBX
stdcall MessageBoxA,0,EDI,Error,0
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
JMP Exit
NoConsole:
MOV dword[ExitStatus],1
stdcall MessageBoxA,0,NoConsoleMessage,Error,0
JMP Exit



;Data section of the Chameleon app
section .data
NoMemMessage db "Could not allocate memory.",0
NoLibFound db "Could not find library: ",0
NoFunctionFound db "Cound not find function: ",0
Error db "Error!",0
ParameterPool dd 0
HandleToHeap dd 0
TempQWord1 dq 0.0
TempQWord2 dq 0.0
True dd -1
Internal_HInstance dd 0
Internal_CommandLine dd 0
NoConsoleMessage db 'Error - Cannot access the console handles for Input/Output.',0
ConsoleTemp dd 0
ConsoleNewLine db 10,0
ConsoleClear db 'CLS',0
ConsolePause db 'PAUSE',0
HandleToInput dd 0
HandleToOutput dd 0
Scope0__EMPTYLINE_String dd 0
String_1 db "*******************************************************************************",0
String_2 db "*|---------------------------------------------------------------------------|*",0
String_3 db "*|                             Hello, there!                                 |*",0
String_4 db "*|             And welcome to my KoolB demonstration program!                |*",0
String_5 db "*|---------------------------------------------------------------------------|*",0
String_6 db "*******************************************************************************",0
Number_7 dq 4.0
STRUC Scope0__PERSONALINFO_TYPE
.Scope0__AGE_Integer resd 1
.Scope0__NAME_String resd 1
.Scope0__FAVNUM_Double resq 1
ENDSTRUC
Scope0__MYINFO_UDT ISTRUC Scope0__PERSONALINFO_TYPE
IEND
Number_8 dq 17.0
String_9 db "Brian C. Becker",0
Number_10 dq 03.14
Number_11 dq 03.14
String_12 db "                    Here's my life story in three sentences:                     ",0
Number_13 dq 1.0
String_14 db "My name is ",0
String_15 db " - Yippe!",0
Number_16 dq 1.0
String_17 db "I am ",0
String_18 db " years old.",0
Number_19 dq 1.0
String_20 db "My favorite number is ",0
String_21 db " (yeah right)!",0
Number_22 dq 3.0
String_23 db "Are U Ready??? Yes or no?",0
Scope0__WORTHLESSVARIABLE____String dd 0
String_24 db "Read or not, here comes the questionare!",0
Number_25 dq 2.0
Scope0__YOURINFO_UDT ISTRUC Scope0__PERSONALINFO_TYPE
IEND
String_26 db "       Now it's your turn to enter all the gory details of your life!          ",0
Number_27 dq 3.0
String_28 db "Please enter the following information:",0
Number_29 dq 2.0
String_30 db "Name:            ",0
String_31 db "Age:             ",0
String_32 db "Favorite Number: ",0
String_33 db "Hello ",0
String_34 db ", how are you?",0
Number_35 dq 1.0
String_36 db "In ",0
String_37 db " years, you will be ",0
String_38 db ".",0
Number_39 dq 2.0
Number_40 dq 5.0
Scope0__RANDOMNUMBERS_Double dd 0
Scope0__RANDOMNUMBERS_Double_Size dd 0
Number_41 dq 1.0
String_42 db "Please type in random numbers:",0
String_43 db "Random Number 1: ",0
Number_44 dq 1.0
String_45 db "Random Number 2: ",0
Number_46 dq 2.0
String_47 db "Random Number 3: ",0
Number_48 dq 3.0
String_49 db "Random Number 4: ",0
Number_50 dq 4.0
String_51 db "Random Number 5: ",0
Number_52 dq 5.0
Scope0__SUM___Double dq 0.0
Number_53 dq 1.0
Number_54 dq 2.0
Number_55 dq 3.0
Number_56 dq 4.0
Number_57 dq 5.0
Scope0__AVERAGE___Double dq 0.0
Number_58 dq 5.0
Number_59 dq 1.0
String_60 db "Random Number Stats:",0
Number_61 dq 1.0
String_62 db "Sum: ",0
Number_63 dq 1.0
String_64 db "Average: ",0
Number_65 dq 1.0
String_66 db "Weird Calculation: ",0
Number_67 dq 2.0
Number_68 dq 4.0
Number_69 dq 2.0
Number_70 dq 2.0
String_71 db "                             Press ENTER to exit...",0
String_72 db "                                     ",0
Scope0__A____String dd 0
ExitStatus dd 0

