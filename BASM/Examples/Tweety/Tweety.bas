'**************
'* Tweety.bas *
'**************

$AppType GUI 
$Optimize On
$Compress On

$Resource Tweety As "Tweety.bmp"

$Include "Windows.inc" 

Dim wcex As WNDCLASSEX 
Dim message As MSG 
Dim ps As PAINTSTRUCT 
Dim rc As RECT
Dim hWindow As Integer 
Dim hInst As Integer
Dim hBitmap As Integer
Dim strClassName As String
Dim strAppTitle As String

Function OnCreate(hWnd As Integer, uMsg As Integer, _ 
                  wParam As Integer, lParam As Integer) As Integer
  hBitmap = LoadBitmap(hInst, MakeIntResource(Tweety))
  Result = 0
End Function

Function OnPaint(hWnd As Integer, uMsg As Integer, _ 
                 wParam As Integer, lParam As Integer) As Integer
  Dim hdc As Integer 
  Dim hMemDc As Integer 

  hdc = BeginPaint(hWnd, ps)
  hMemDc = CreateCompatibleDC(hdc) 
  SelectObject(hMemDc, hBitmap)
  GetClientRect(hWnd, rc)
  BitBlt(hdc, 0, 0, rc.right, rc.bottom, hMemDc, 0, 0, SRCCOPY)
  DeleteDC(hMemDc)
  EndPaint(hWnd, ps) 
  Result = 0
End Function

Function OnDestroy(hWnd As Integer, uMsg As Integer, _ 
                   wParam As Integer, lParam As Integer) As Integer
  DeleteObject(hBitmap)
  PostQuitMessage(0)
  Result = 0
End Function

Function WindowProc(hWnd As Integer, uMsg As Integer, _ 
                    wParam As Integer, lParam As Integer) As Integer
  If uMsg = WM_CREATE Then
    Result = OnCreate(hWnd, uMsg, wParam, lParam)
  ElseIf uMsg = WM_PAINT Then 
    Result = OnPaint(hWnd, uMsg, wParam, lParam)
  ElseIf uMsg = WM_DESTROY Then 
    Result = OnDestroy(hWnd, uMsg, wParam, lParam)
  Else 
    Result = DefWindowProc(hWnd, uMsg, wParam, lParam)
  End If 
End Function

'***

strAppTitle = "Tweety"
strClassName = "TweetyClass"

hInst = GetModuleHandle(0)
wcex.cbSize = SizeOf(WNDCLASSEX) 
wcex.style = CS_VREDRAW + CS_HREDRAW
wcex.lpfnwndproc = CodePtr(WindowProc) 
wcex.cbClsExtra = 0 
wcex.cbWndExtra = 0 
wcex.hInstance = hInst
wcex.hIcon = LoadIcon(0, MakeIntResource(IDI_APPLICATION)) 
wcex.hCursor = LoadCursor(0, MakeIntResource(IDC_ARROW)) 
wcex.hbrBackground = COLOR_WINDOW + 3
wcex.lpszMenuName = ""
wcex.lpszClassName = strClassName
wcex.hIconSm = 0 

If (RegisterClassEx(wcex)) = 0 Then 
  MessageBox(0, "RegisterClassEx failed.", strAppTitle, 0)
  ExitProcess(0)
End If 

hWindow = CreateWindowEx(WS_EX_CLIENTEDGE, strClassName, strAppTitle, _ 
                         WS_OVERLAPPEDWINDOW + WS_VISIBLE - WS_THICKFRAME, _
                         300, 200, _ 
                         294, 313, _ 
                         0, 0, hInst, 0) 
If hWindow = 0 Then 
  MessageBox(0, "CreateWindowEx failed.", strAppTitle, 0)
  ExitProcess(0)
End If 

While GetMessage(message, 0, 0, 0) > 0
  TranslateMessage(message) 
  DispatchMessage(message) 
Wend 
