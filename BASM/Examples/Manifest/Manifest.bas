'****************
'* Manifest.bas *
'****************

' Manifest.bas does absolutely nothing.
' It does however demonstrate the use of $XPStyle.

' Worth pointing out, is that when using $Resource
' and dialogs, the varible "StupidDialog" is never
' used.  Refer to the name in the "dlg" file when
' working with them (in this case 102).

$AppType GUI
$XPStyle

$Include "Windows.inc"

$Resource StupidDialog As "Manifest.dlg"

Function DialogProc(hWndDlg As Integer, uMsg As Integer, _
                    wParam As Integer, lParam As Integer) As Integer
  If uMsg = WM_CLOSE then
    EndDialog(hwndDlg, 0)
    Result = 0
  Else
    Result = 0
  End If  
End Function

DialogBoxParam(GetModuleHandle(0), MakeIntResource(102), _
               HWND_DESKTOP, CodePtr(DialogProc), 0)


