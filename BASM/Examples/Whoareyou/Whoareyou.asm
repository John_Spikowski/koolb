;Library functions to import to the Chameleon app
extern ExitProcess
extern MessageBoxA
extern lstrcat
extern lstrlen
extern lstrcpy
extern HeapCreate
extern HeapAlloc
extern HeapFree
extern GetModuleHandleA
extern GetCommandLineA
extern GetStdHandle
extern lstrlenA
extern lstrcpyA
extern WriteFile
extern ReadFile
extern RtlMoveMemory
extern lstrcmpA
extern lstrcatA
extern Sleep
extern floor
extern _gcvt
extern atof
extern HeapDestroy



;Initialize everything to prep the app to run
section .text
%include "C:\BASM\Bin\MACROS.INC"
global START
START:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH ESI
PUSH EDI
stdcall HeapCreate,0,16384,0
MOV dword[HandleToHeap],EAX
CMP dword[HandleToHeap],0
JNE Label1
JMP NoMemory
Label1:
stdcall GetModuleHandleA,0
MOV dword[Internal_HInstance],EAX
CALL GetCommandLineA
MOV dword[Internal_CommandLine],EAX
stdcall GetStdHandle,-10
MOV dword[HandleToInput],EAX
stdcall GetStdHandle,-11
MOV dword[HandleToOutput],EAX
CMP dword[HandleToInput],-1
JNE Label2
JMP NoConsole
Label2:
CMP dword[HandleToOutput],-1
JNE Label3
JMP NoConsole
Label3:



;The main body where the app actually runs
MOV EBX,String_61
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label125
JMP NoMemory
Label125:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_61
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_62
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label126
JMP NoMemory
Label126:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_62
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_63
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label127
JMP NoMemory
Label127:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_63
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_64
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label128
JMP NoMemory
Label128:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_64
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_65
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label129
JMP NoMemory
Label129:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_65
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_66
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label130
JMP NoMemory
Label130:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_66
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_67
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label131
JMP NoMemory
Label131:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_67
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_68
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label132
JMP NoMemory
Label132:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_68
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapAlloc,dword[HandleToHeap],8,16384
CMP EAX,0
JNE Label133
JMP NoMemory
Label133:
MOV EBX,EAX
stdcall ReadFile,dword[HandleToInput],EBX,16383,ConsoleTemp,0
stdcall lstrlenA,EBX
INC EAX
DEC EAX
DEC EAX
DEC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label134
JMP NoMemory
Label134:
MOV ESI,EAX
stdcall RtlMoveMemory,ESI,EBX,EDI
MOV byte[ESI+EDI],0
PUSH ESI
POP EBX
ccall atof,EBX
stdcall HeapFree,dword[HandleToHeap],0,EBX
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FST qword[Scope3__CHOICE___Double]
PUSH dword[Scope3__CHOICE___Double+4]
PUSH dword[Scope3__CHOICE___Double]
PUSH dword[Number_69+4]
PUSH dword[Number_69]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label135
FLDZ
Label135:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label137
JMP Label136
Label137:
CALL _DOVISITOR
JMP Label138
Label136:
PUSH dword[Scope3__CHOICE___Double+4]
PUSH dword[Scope3__CHOICE___Double]
PUSH dword[Number_70+4]
PUSH dword[Number_70]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label139
FLDZ
Label139:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label141
JMP Label140
Label141:
CALL _DOUSER
JMP Label138
Label140:
PUSH dword[Scope3__CHOICE___Double+4]
PUSH dword[Scope3__CHOICE___Double]
PUSH dword[Number_71+4]
PUSH dword[Number_71]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label142
FLDZ
Label142:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label144
JMP Label143
Label144:
CALL _DODEVELOPER
JMP Label138
Label143:
MOV EBX,String_72
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label146
JMP NoMemory
Label146:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_72
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_73+4]
PUSH dword[Number_73]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
JMP Exit
JMP Label145
Label145:
Label138:



;Prepare the app to exit and then terminate
Exit:
stdcall HeapDestroy,dword[HandleToHeap]
POP EDI
POP ESI
POP EBX
MOV ESP,EBP
POP EBP
MOV EAX,dword[ExitStatus]
stdcall ExitProcess,dword[ExitStatus]
RET

NoMemory:
MOV dword[ExitStatus],1
stdcall MessageBoxA,0,NoMemMessage,Error,0
JMP Exit

NoLibrary:
MOV dword[ExitStatus],1
POP EBX
stdcall lstrlen,EBX
ADD EAX,30
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
MOV EDI,EAX
stdcall lstrcpy,EDI,NoLibFound
stdcall lstrcat,EDI,EBX
stdcall MessageBoxA,0,EDI,Error,0
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
JMP Exit

NoFunction:
MOV dword[ExitStatus],1
POP EBX
stdcall lstrlen,EBX
ADD EAX,30
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
MOV EDI,EAX
stdcall lstrcpy,EDI,NoFunctionFound
stdcall lstrcat,EDI,EBX
stdcall MessageBoxA,0,EDI,Error,0
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
JMP Exit
NoConsole:
MOV dword[ExitStatus],1
stdcall MessageBoxA,0,NoConsoleMessage,Error,0
JMP Exit


global _DOVISITOR
_DOVISITOR:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label10
JMP NoMemory
Label10:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label28
JMP NoMemory
Label28:
MOV dword[EBP-12],EAX
MOV byte[EAX],0
SUB ESP,4

MOV EBX,String_1
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label4
JMP NoMemory
Label4:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_1
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_2
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label5
JMP NoMemory
Label5:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_2
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_3
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label6
JMP NoMemory
Label6:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_3
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_4
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label7
JMP NoMemory
Label7:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_4
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapAlloc,dword[HandleToHeap],8,16384
CMP EAX,0
JNE Label8
JMP NoMemory
Label8:
MOV EBX,EAX
stdcall ReadFile,dword[HandleToInput],EBX,16383,ConsoleTemp,0
stdcall lstrlenA,EBX
INC EAX
DEC EAX
DEC EAX
DEC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label9
JMP NoMemory
Label9:
MOV ESI,EAX
stdcall RtlMoveMemory,ESI,EBX,EDI
MOV byte[ESI+EDI],0
PUSH ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
stdcall lstrlenA,dword[EBP-4]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label11
JMP NoMemory
Label11:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-4]
PUSH EAX
MOV EBX,String_5
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label12
JMP NoMemory
Label12:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_5
PUSH EBX
POP EBX
POP EDI
stdcall lstrcmpA,EDI,EBX
MOV ESI,EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,EDI
FINIT
FILD dword[True]
CMP ESI,0
JE Label13
FLDZ
Label13:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label15
JMP Label14
Label15:
MOV EBX,String_6
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label17
JMP NoMemory
Label17:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_6
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
JMP Label16
Label14:
MOV EBX,String_7
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label19
JMP NoMemory
Label19:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_7
PUSH EBX
stdcall lstrlenA,dword[EBP-4]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label20
JMP NoMemory
Label20:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-4]
PUSH EAX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label21
JMP NoMemory
Label21:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
JMP Label18
Label18:
Label16:
MOV EBX,String_8
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label22
JMP NoMemory
Label22:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_8
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_9
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label23
JMP NoMemory
Label23:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_9
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_10+4]
PUSH dword[Number_10]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
PUSH dword[Number_11+4]
PUSH dword[Number_11]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
Label24:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_12+4]
PUSH dword[Number_12]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JB Label25
FLDZ
Label25:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label27
JMP Label26
Label27:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_13+4]
PUSH dword[Number_13]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
stdcall lstrlenA,dword[EBP-12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label29
JMP NoMemory
Label29:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-12]
PUSH EAX
MOV EBX,String_14
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label30
JMP NoMemory
Label30:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_14
PUSH EBX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label31
JMP NoMemory
Label31:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-12]
MOV dword[EBP-12],EBX
stdcall lstrlenA,dword[EBP-12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label32
JMP NoMemory
Label32:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-12]
PUSH EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_15+4]
PUSH dword[Number_15]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
JMP Label24
Label26:
Label33:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_16+4]
PUSH dword[Number_16]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JA Label34
FLDZ
Label34:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label36
JMP Label35
Label36:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_17+4]
PUSH dword[Number_17]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
PUSH dword[Number_18+4]
PUSH dword[Number_18]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-16]
MOV EBX,String_19
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label37
JMP NoMemory
Label37:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_19
PUSH EBX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-12]
MOV dword[EBP-12],EBX
Label38:
FINIT
FILD dword[EBP-16]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_20+4]
PUSH dword[Number_20]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JB Label39
FLDZ
Label39:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label41
JMP Label40
Label41:
stdcall lstrlenA,dword[EBP-12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label42
JMP NoMemory
Label42:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-12]
PUSH EAX
MOV EBX,String_21
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label43
JMP NoMemory
Label43:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_21
PUSH EBX
POP ESI
stdcall lstrlenA,ESI
INC EAX
MOV EDI,EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
ADD EAX,EDI
DEC EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EAX
CMP EAX,0
JNE Label44
JMP NoMemory
Label44:
stdcall lstrcpyA,EAX,EBX
stdcall lstrcatA,EAX,ESI
PUSH EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-12]
MOV dword[EBP-12],EBX
FINIT
FILD dword[EBP-16]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_22+4]
PUSH dword[Number_22]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-16]
JMP Label38
Label40:
stdcall lstrlenA,dword[EBP-12]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label45
JMP NoMemory
Label45:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-12]
PUSH EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_23+4]
PUSH dword[Number_23]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
JMP Label33
Label35:
MOV EBX,String_24
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label46
JMP NoMemory
Label46:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_24
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_25
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label47
JMP NoMemory
Label47:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_25
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_26+4]
PUSH dword[Number_26]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
JMP Exit


ADD ESP,16
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0


global _DOUSER
_DOUSER:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label51
JMP NoMemory
Label51:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,4

MOV EBX,String_27
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label48
JMP NoMemory
Label48:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_27
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_28
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label49
JMP NoMemory
Label49:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_28
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_29
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label50
JMP NoMemory
Label50:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_29
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_30+4]
PUSH dword[Number_30]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
MOV EBX,String_31
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label52
JMP NoMemory
Label52:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_31
PUSH EBX
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
PUSH dword[Number_32+4]
PUSH dword[Number_32]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
Label53:
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_33+4]
PUSH dword[Number_33]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JA Label54
FLDZ
Label54:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label56
JMP Label55
Label56:
stdcall lstrlenA,dword[EBP-4]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label57
JMP NoMemory
Label57:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-4]
PUSH EAX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
stdcall HeapAlloc,dword[HandleToHeap],8,100
CMP EAX,0
JNE Label61
JMP NoMemory
Label61:
MOV EBX,EAX
ccall _gcvt,dword[TempQWord1],dword[TempQWord1+4],50,EBX
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
MOV ECX,0
Label58:
CMP byte[EBX+ECX],0
JE Label59
INC ECX
CMP ECX,EDI
JL Label58
JMP Label60
Label59:
DEC ECX
CMP byte[EBX+ECX],'.'
JNE Label60
MOV byte[EBX+ECX],0
Label60:
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_34+4]
PUSH dword[Number_34]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FSUB ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
ccall floor,dword[TempQWord2],dword[TempQWord2+4]
FISTP dword[TempQWord2]
PUSH dword[TempQWord2]
POP dword[TempQWord1]
FINIT
FILD dword[TempQWord1]
FIST dword[EBP-8]
FINIT
FILD dword[EBP-8]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_35+4]
PUSH dword[Number_35]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FDIV ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[Number_36+4]
PUSH dword[Number_36]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FADD ST0,ST1
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
JMP Label53
Label55:
MOV EBX,String_37
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label62
JMP NoMemory
Label62:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_37
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_38+4]
PUSH dword[Number_38]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
JMP Exit


ADD ESP,8
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0


global _DODEVELOPER
_DODEVELOPER:
PUSH EBP
MOV EBP,ESP
PUSH EBX
PUSH EDI
PUSH ESI
SUB ESP,4
stdcall HeapAlloc,dword[HandleToHeap],8,1
CMP EAX,0
JNE Label68
JMP NoMemory
Label68:
MOV dword[EBP-4],EAX
MOV byte[EAX],0
SUB ESP,8
MOV dword[ESP+8],0
MOV dword[ESP+4],0

MOV EBX,String_39
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label63
JMP NoMemory
Label63:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_39
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_40
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label64
JMP NoMemory
Label64:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_40
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_41
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label65
JMP NoMemory
Label65:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_41
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapAlloc,dword[HandleToHeap],8,16384
CMP EAX,0
JNE Label66
JMP NoMemory
Label66:
MOV EBX,EAX
stdcall ReadFile,dword[HandleToInput],EBX,16383,ConsoleTemp,0
stdcall lstrlenA,EBX
INC EAX
DEC EAX
DEC EAX
DEC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label67
JMP NoMemory
Label67:
MOV ESI,EAX
stdcall RtlMoveMemory,ESI,EBX,EDI
MOV byte[ESI+EDI],0
PUSH ESI
POP EBX
stdcall HeapFree,dword[HandleToHeap],0,dword[EBP-4]
MOV dword[EBP-4],EBX
stdcall lstrlenA,dword[EBP-4]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label69
JMP NoMemory
Label69:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-4]
PUSH EAX
MOV EBX,String_42
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label70
JMP NoMemory
Label70:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_42
PUSH EBX
POP EBX
POP EDI
stdcall lstrcmpA,EDI,EBX
MOV ESI,EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,EDI
FINIT
FILD dword[True]
CMP ESI,0
JE Label71
FLDZ
Label71:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
stdcall lstrlenA,dword[EBP-4]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label72
JMP NoMemory
Label72:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-4]
PUSH EAX
MOV EBX,String_43
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label73
JMP NoMemory
Label73:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_43
PUSH EBX
POP EBX
POP EDI
stdcall lstrcmpA,EDI,EBX
MOV ESI,EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,EDI
FINIT
FILD dword[True]
CMP ESI,0
JE Label74
FLDZ
Label74:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JE Label77
POP dword[TempQWord1]
POP dword[TempQWord1+4]
JMP Label75
Label77:
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JE Label76
Label75:
FINIT
FILD dword[True]
JMP Label78
Label76:
FINIT
FLDZ
Label78:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
stdcall lstrlenA,dword[EBP-4]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label79
JMP NoMemory
Label79:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-4]
PUSH EAX
MOV EBX,String_44
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label80
JMP NoMemory
Label80:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_44
PUSH EBX
POP EBX
POP EDI
stdcall lstrcmpA,EDI,EBX
MOV ESI,EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,EDI
FINIT
FILD dword[True]
CMP ESI,0
JE Label81
FLDZ
Label81:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JE Label84
POP dword[TempQWord1]
POP dword[TempQWord1+4]
JMP Label82
Label84:
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JE Label83
Label82:
FINIT
FILD dword[True]
JMP Label85
Label83:
FINIT
FLDZ
Label85:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
stdcall lstrlenA,dword[EBP-4]
INC EAX
MOV EBX,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EBX
CMP EAX,0
JNE Label86
JMP NoMemory
Label86:
MOV EDI,EAX
stdcall lstrcpyA,EDI,dword[EBP-4]
PUSH EAX
MOV EBX,String_45
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label87
JMP NoMemory
Label87:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_45
PUSH EBX
POP EBX
POP EDI
stdcall lstrcmpA,EDI,EBX
MOV ESI,EAX
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapFree,dword[HandleToHeap],0,EDI
FINIT
FILD dword[True]
CMP ESI,0
JE Label88
FLDZ
Label88:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JE Label91
POP dword[TempQWord1]
POP dword[TempQWord1+4]
JMP Label89
Label91:
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JE Label90
Label89:
FINIT
FILD dword[True]
JMP Label92
Label90:
FINIT
FLDZ
Label92:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label94
JMP Label93
Label94:
MOV EBX,String_46
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label96
JMP NoMemory
Label96:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_46
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_47
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label97
JMP NoMemory
Label97:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_47
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall HeapAlloc,dword[HandleToHeap],8,16384
CMP EAX,0
JNE Label98
JMP NoMemory
Label98:
MOV EBX,EAX
stdcall ReadFile,dword[HandleToInput],EBX,16383,ConsoleTemp,0
stdcall lstrlenA,EBX
INC EAX
DEC EAX
DEC EAX
DEC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label99
JMP NoMemory
Label99:
MOV ESI,EAX
stdcall RtlMoveMemory,ESI,EBX,EDI
MOV byte[ESI+EDI],0
PUSH ESI
POP EBX
ccall atof,EBX
stdcall HeapFree,dword[HandleToHeap],0,EBX
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FST qword[EBP-12]
PUSH dword[EBP-12+4]
PUSH dword[EBP-12]
PUSH dword[Number_48+4]
PUSH dword[Number_48]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JB Label100
FLDZ
Label100:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[EBP-12+4]
PUSH dword[EBP-12]
PUSH dword[Number_49+4]
PUSH dword[Number_49]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label101
FLDZ
Label101:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label102
FINIT
FILD dword[True]
Label102:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
POP dword[TempQWord1]
POP dword[TempQWord1+4]
JNE Label106
JMP Label105
Label106:
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label104
JMP Label105
Label104:
FINIT
FILD dword[True]
JMP Label107
Label105:
FLDZ
Label107:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
PUSH dword[EBP-12+4]
PUSH dword[EBP-12]
PUSH dword[Number_50+4]
PUSH dword[Number_50]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JA Label108
FLDZ
Label108:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JE Label111
POP dword[TempQWord1]
POP dword[TempQWord1+4]
JMP Label109
Label111:
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JE Label110
Label109:
FINIT
FILD dword[True]
JMP Label112
Label110:
FINIT
FLDZ
Label112:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label114
JMP Label113
Label114:
MOV EBX,String_51
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label116
JMP NoMemory
Label116:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_51
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_52+4]
PUSH dword[Number_52]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
JMP Exit
JMP Label113
Label113:
Label115:
MOV EBX,String_53
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label117
JMP NoMemory
Label117:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_53
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
MOV EBX,String_54
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label118
JMP NoMemory
Label118:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_54
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_55+4]
PUSH dword[Number_55]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
JMP Exit
JMP Label93
Label93:
Label95:
MOV EBX,String_56
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label119
JMP NoMemory
Label119:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_56
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
PUSH dword[Number_57+4]
PUSH dword[Number_57]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
MOV dword[TempQWord2],1000
FINIT
FLD qword[TempQWord1]
FILD dword[TempQWord2]
FMUL ST0,ST1
FRNDINT
FIST dword[TempQWord1]
PUSH dword[TempQWord1]
POP EBX
stdcall Sleep,EBX
Label120:
PUSH dword[Number_58+4]
PUSH dword[Number_58]
PUSH dword[Number_59+4]
PUSH dword[Number_59]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
POP dword[TempQWord2]
POP dword[TempQWord2+4]
FINIT
FLD qword[TempQWord1]
FLD qword[TempQWord2]
FCOM ST1
FSTSW AX
WAIT
SAHF
FINIT
FILD dword[True]
JE Label121
FLDZ
Label121:
MOV EAX,dword[TempQWord1]
FST qword[TempQWord1]
PUSH dword[TempQWord1+4]
PUSH dword[TempQWord1]
POP dword[TempQWord1]
POP dword[TempQWord1+4]
FINIT
FLD qword[TempQWord1]
FLDZ
FCOM ST1
FSTSW AX
WAIT
SAHF
JNE Label123
JMP Label122
Label123:
MOV EBX,String_60
stdcall lstrlenA,EBX
INC EAX
MOV EDI,EAX
stdcall HeapAlloc,dword[HandleToHeap],8,EDI
CMP EAX,0
JNE Label124
JMP NoMemory
Label124:
MOV EBX,EAX
stdcall lstrcpyA,EBX,String_60
PUSH EBX
POP EBX
stdcall lstrlenA,EBX
INC EAX
DEC EAX
stdcall WriteFile,dword[HandleToOutput],EBX,EAX,ConsoleTemp,0
stdcall HeapFree,dword[HandleToHeap],0,EBX
stdcall WriteFile,dword[HandleToOutput],ConsoleNewLine,1,ConsoleTemp,0
JMP Label120
Label122:


ADD ESP,12
POP ESI
POP EDI
POP EBX
MOV ESP,EBP
POP EBP
RET 0



;Data section of the Chameleon app
section .data
NoMemMessage db "Could not allocate memory.",0
NoLibFound db "Could not find library: ",0
NoFunctionFound db "Cound not find function: ",0
Error db "Error!",0
ParameterPool dd 0
HandleToHeap dd 0
TempQWord1 dq 0.0
TempQWord2 dq 0.0
True dd -1
Internal_HInstance dd 0
Internal_CommandLine dd 0
NoConsoleMessage db 'Error - Cannot access the console handles for Input/Output.',0
ConsoleTemp dd 0
ConsoleNewLine db 10,0
ConsoleClear db 'CLS',0
ConsolePause db 'PAUSE',0
HandleToInput dd 0
HandleToOutput dd 0
String_1 db "",0
String_2 db "           Welcome vistor. Would you please identify yourself?",0
String_3 db " ",0
String_4 db "What is your name? ",0
String_5 db "Jared",0
String_6 db "A very special welcome to you, Jared!",0
String_7 db "Welcome, ",0
String_8 db " ",0
String_9 db "Have a seat and I hope you enjoy your stay! Right now, I'll demonstrate KoolB's capabilities:",0
Number_10 dq 2.0
Number_11 dq 0.0
Number_12 dq 79.0
Number_13 dq 1.0
String_14 db ".",0
Number_15 dq 0.1
Number_16 dq 0.0
Number_17 dq 1.0
Number_18 dq 0.0
String_19 db " ",0
Number_20 dq 1.0
String_21 db ".",0
Number_22 dq 1.0
Number_23 dq 0.1
String_24 db " ",0
String_25 db "I'm terribly sorry, but I have to go now!",0
Number_26 dq 4.0
String_27 db " ",0
String_28 db "                               Welcome, user!",0
String_29 db "Well, then, don't let me get in your way of your using KoolB - I'll just leave you alone and let you check out the source code to this wonderful app.",0
Number_30 dq 4.0
String_31 db "                                    ",0
Number_32 dq 10.0
Number_33 dq 0.0
Number_34 dq 1.0
Number_35 dq 5.0
Number_36 dq 0.5
String_37 db "                               Blast-off!",0
Number_38 dq 1.0
String_39 db "Really now? You think you are a developer?",0
String_40 db "I don't buy that - prove it to me!",0
String_41 db "Give me your name: ",0
String_42 db "Brian",0
String_43 db "Brian Becker",0
String_44 db "Brian C. Becker",0
String_45 db "Jared",0
String_46 db "Hmm, you seem to check out - but here's one last question to make sure you are who you say you are.",0
String_47 db "How old are you: ",0
Number_48 dq 10.0
Number_49 dq 2.0
Number_50 dq 80.0
String_51 db "Security! Have these people shown to the doors right now!",0
Number_52 dq 4.0
String_53 db "Welcome developer!",0
String_54 db "You have way to much work to do on KoolB to be messing around with this - go do it now!",0
Number_55 dq 4.0
String_56 db "Bah! I didn't think you were a developer! You just a fake - don't even bother coming in!",0
Number_57 dq 5.0
Number_58 dq 1.0
Number_59 dq 1.0
String_60 db "Ha! HA! HAAA!!! You deserve this for lying! Hint: Control + C ",0
String_61 db "                   Welcome to the wonderful word of Wizz!",0
String_62 db " ",0
String_63 db "Are you a:",0
String_64 db "  1. Visitor",0
String_65 db "  2. User",0
String_66 db "  3. Developer",0
String_67 db " ",0
String_68 db "So! What will it be? ",0
Scope3__CHOICE___Double dq 0.0
Number_69 dq 1.0
Number_70 dq 2.0
Number_71 dq 3.0
String_72 db "Hey, stop messing with me! I'm not happy with your choice, so I'll just go and sulk. ",0
Number_73 dq 3.0
ExitStatus dd 0

