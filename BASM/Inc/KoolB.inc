'*************
'* KoolB.inc *
'*************

$IfNDef KoolBinc
$Define KoolBinc

$CONST False = 0
$CONST True = -1

$Include "Windows.inc"

$Include "Bitwise.inc"
$Include "Math.inc"
$Include "Type.inc"
$Include "System.inc"
$Include "String.inc"
$Include "Convert.inc"
$Include "Console.inc"
$Include "Time.inc"
$Include "File.inc"
$Include "Debug.inc"

$End If