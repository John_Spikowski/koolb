' IUP.inc - IUP Include FIle

'************************************************************************
'*                   Common Flags and Return Values                     *
'************************************************************************
$CONST IUP_ERROR      =  1
$CONST IUP_NOERROR    =  0
$CONST IUP_OPENED     = -1
$CONST IUP_INVALID    = -1
$CONST IUP_INVALID_ID = -10


'************************************************************************
'*                   Callback Return Values                             *
'************************************************************************
$CONST IUP_IGNORE    = -1
$CONST IUP_DEFAULT   = -2
$CONST IUP_CLOSE     = -3
$CONST IUP_CONTINUE  = -4


'************************************************************************
'*           IupPopup and IupShowXY Parameter Values                    *
'************************************************************************
$CONST IUP_CENTER       = 0xFFFF  ' 65535
$CONST IUP_LEFT         = 0xFFFE  ' 65534
$CONST IUP_RIGHT        = 0xFFFD  ' 65533
$CONST IUP_MOUSEPOS     = 0xFFFC  ' 65532
$CONST IUP_CURRENT      = 0xFFFB  ' 65531
$CONST IUP_CENTERPARENT = 0xFFFA  ' 65530
$CONST IUP_TOP          = IUP_LEFT
$CONST IUP_BOTTOM       = IUP_RIGHT

'************************************************************************
'*                       Windows API                                    *
'************************************************************************
' DECLARE FUNCTION VAL LIB "msvcrt.dll" ALIAS "atof" CALL C (S AS STRING) AS DOUBLE



'************************************************************************
'*                        Main API                                      *
'************************************************************************

DECLARE FUNCTION IupOpen LIB "iup.dll" ALIAS "IupOpen" (argc AS INTEGER, argv AS INTEGER) AS INTEGER
DECLARE FUNCTION IupClose LIB "iup.dll" ALIAS "IupClose" () AS VOID
DECLARE FUNCTION IupImageLibOpen LIB "iup.dll" ALIAS "IupImageLibOpen" () AS VOID

DECLARE FUNCTION IupMainLoop LIB "iup.dll" ALIAS "IupMainLoop" () AS INTEGER
DECLARE FUNCTION IupLoopStep LIB "iup.dll" ALIAS "IupLoopStep" () AS INTEGER
DECLARE FUNCTION IupLoopStepWait LIB "iup.dll" ALIAS "IupLoopStepWait" () AS INTEGER
DECLARE FUNCTION IupMainLoopLevel LIB "iup.dll" ALIAS "IupMainLoopLevel" () AS INTEGER
DECLARE FUNCTION IupFlush LIB "iup.dll" ALIAS "IupFlush" () AS VOID
DECLARE FUNCTION IupExitLoop LIB "iup.dll" ALIAS "IupExitLoop" () AS VOID

DECLARE FUNCTION IupRecordInput LIB "iup.dll" ALIAS "IupRecordInput" (filename AS STRING, mode AS INTEGER) AS INTEGER
DECLARE FUNCTION IupPlayInput LIB "iup.dll" ALIAS "" (filename AS STRING) AS INTEGER

DECLARE FUNCTION IupUpdate LIB "iup.dll" ALIAS "IupUpdate" (ih AS INTEGER) AS VOID
DECLARE FUNCTION IupUpdateChildren LIB "iup.dll" ALIAS "IupUpdateChildren" (ih AS INTEGER) AS VOID
DECLARE FUNCTION IupRedraw LIB "iup.dll" ALIAS "IupRedraw" (ih AS INTEGER, children AS INTEGER) AS  VOID
DECLARE FUNCTION IupRefresh LIB "iup.dll" ALIAS "IupRefresh" (ih AS INTEGER) AS VOID
DECLARE FUNCTION IupRefreshChildren LIB "iup.dll" ALIAS "IupRefreshChildren" (ih AS INTEGER) AS VOID

DECLARE FUNCTION IupHelp LIB "iup.dll" ALIAS "IupHelp" (url AS STRING) AS INTEGER
DECLARE FUNCTION IupLoad LIB "iup.dll" ALIAS "IupLoad" (filename AS STRING) AS STRING
DECLARE FUNCTION IupLoadBuffer LIB "iup.dll" ALIAS "IupLoadBuffer" (buffer AS STRING) AS STRING

DECLARE FUNCTION IupVersion LIB "iup.dll" ALIAS "IupVersion" () AS STRING
DECLARE FUNCTION IupVersionDate LIB "iup.dll" ALIAS "IupVersionDate" () AS  STRING
DECLARE FUNCTION IupVersionNumber LIB "iup.dll" ALIAS "IupVersionNumber"  () AS INTEGER

DECLARE FUNCTION IupSetLanguage LIB "iup.dll" ALIAS "IupSetLanguage" (lng AS STRING) AS VOID
DECLARE FUNCTION IupGetLanguage LIB "iup.dll" ALIAS "IupGetLanguage" () AS STRING
DECLARE FUNCTION IupSetLanguageString LIB "iup.dll" ALIAS "IupSetLanguageString" (name AS STRING, str AS STRING) AS VOID
DECLARE FUNCTION IupStoreLanguageString LIB "iup.dll" ALIAS "IupStoreLanguageString" (name AS STRING, str AS STRING) AS VOID
DECLARE FUNCTION IupGetLanguageString LIB "iup.dll" ALIAS "IupGetLanguageString" (name AS STRING) AS STRING
DECLARE FUNCTION IupSetLanguagePack LIB "iup.dll" ALIAS "IupSetLanguagePack" (ih AS INTEGER) AS VOID

DECLARE FUNCTION IupDestroy LIB "iup.dll" ALIAS "IupDestroy" (ih AS INTEGER) AS VOID
DECLARE FUNCTION IupDetach LIB "iup.dll" ALIAS "IupDetach" (child AS INTEGER) AS VOID
DECLARE FUNCTION IupAppend LIB "iup.dll" ALIAS "IupAppend" (ih AS INTEGER, child AS INTEGER) AS INTEGER
DECLARE FUNCTION IupInsert LIB "iup.dll" ALIAS "IupInsert" (ih AS INTEGER, ref_child AS INTEGER, child AS INTEGER) AS INTEGER
DECLARE FUNCTION IupGetChild LIB "iup.dll" ALIAS "IupGetChild" (ih AS INTEGER, pos AS INTEGER) AS INTEGER
DECLARE FUNCTION IupGetChildPos LIB "iup.dll" ALIAS "IupGetChildPos" (ih AS INTEGER, child AS INTEGER) AS INTEGER
DECLARE FUNCTION IupGetChildCount LIB "iup.dll" ALIAS "IupGetChildCount" (ih AS INTEGER) AS INTEGER
DECLARE FUNCTION IupGetNextChild LIB "iup.dll" ALIAS "IupGetNextChild" (ih AS INTEGER, child AS INTEGER) AS INTEGER
DECLARE FUNCTION IupGetBrother LIB "iup.dll" ALIAS "IupGetBrother" (ih AS INTEGER) AS INTEGER
DECLARE FUNCTION IupGetParent LIB "iup.dll" ALIAS "IupGetParent" (ih AS INTEGER) AS INTEGER
DECLARE FUNCTION IupGetDialog LIB "iup.dll" ALIAS "IupGetDialog" (ih AS INTEGER) AS INTEGER
DECLARE FUNCTION IupGetDialogChild LIB "iup.dll" ALIAS "IupGetDialogChild" (ih AS INTEGER, name AS STRING) AS INTEGER
DECLARE FUNCTION IupReparent LIB "iup.dll" ALIAS "IupReparent" (ih AS INTEGER, new_parent AS INTEGER, ref_child AS INTEGER) AS INTEGER

DECLARE FUNCTION IupPopup LIB "iup.dll" ALIAS "IupPopup" (ih AS INTEGER, x AS INTEGER, y AS INTEGER) AS INTEGER
DECLARE FUNCTION IupShow LIB "iup.dll" ALIAS "IupShow" (ih AS INTEGER) AS INTEGER
DECLARE FUNCTION IupShowXY LIB "iup.dll" ALIAS "IupShowXY" (ih AS INTEGER, x AS INTEGER, y AS INTEGER) AS INTEGER
DECLARE FUNCTION IupHide LIB "iup.dll" ALIAS "IupHide" (ih AS INTEGER) AS INTEGER
DECLARE FUNCTION IupMap LIB "iup.dll" ALIAS "IupMap" (ih AS INTEGER) AS INTEGER
DECLARE FUNCTION IupUnmap LIB "iup.dll" ALIAS "IupUnmap" (ih AS INTEGER) AS VOID

DECLARE FUNCTION IupResetAttribute LIB "iup.dll" ALIAS "IupResetAttribute" (ih AS INTEGER, name AS STRING) AS VOID
DECLARE FUNCTION IupGetAllAttributes LIB "iup.dll" ALIAS "IupGetAllAttributes" (ih AS INTEGER, names AS STRING, n AS INTEGER) AS INTEGER
' <variadic> DECLARE FUNCTION IupSetAtt LIB "iup.dll" ALIAS "IupSetAtt" (const STRING  handle_name, INTEGER ih, const STRING  name, ...) AS INTEGER
DECLARE FUNCTION IupSetAttributes LIB "iup.dll" ALIAS "IupSetAttributes"  (ih AS INTEGER, strvar AS STRING) AS INTEGER
DECLARE FUNCTION IupGetAttributes LIB "iup.dll" ALIAS "IupGetAttributes"  (ih AS INTEGER) AS STRING

DECLARE FUNCTION IupSetAttribute LIB "iup.dll" ALIAS "IupSetAttribute" (ih AS INTEGER, name AS STRING, value AS STRING) AS VOID
DECLARE FUNCTION IupSetStrAttribute LIB "iup.dll" ALIAS "IupSetStrAttribute" (ih AS INTEGER, name AS STRING, value AS STRING) AS VOID
' <variadic> DECLARE FUNCTION IupSetStrf LIB "iup.dll" ALIAS "IupSetStrf" (ih AS INTEGER, name AS STRING, format AS STRING, ...) AS VOID
DECLARE FUNCTION IupSetInt LIB "iup.dll" ALIAS "IupSetInt" (ih AS INTEGER, name AS STRING, value AS INTEGER) AS VOID
DECLARE FUNCTION IupSetFloat LIB "iup.dll" ALIAS "IupSetFloat" (ih AS INTEGER, name AS STRING, value AS DOUBLE) AS VOID
DECLARE FUNCTION IupSetDouble LIB "iup.dll" ALIAS "IupSetDouble" (ih AS INTEGER, name AS STRING, value AS DOUBLE) AS VOID
DECLARE FUNCTION IupSetRGB LIB "iup.dll" ALIAS "IupSetRGB" (ih AS INTEGER, name AS STRING, r AS BYTE, g AS BYTE, b AS BYTE) AS VOID

DECLARE FUNCTION IupGetAttribute LIB "iup.dll" ALIAS "IupGetAttribute" (ih AS INTEGER, name AS STRING) AS STRING
DECLARE FUNCTION IupGetInt LIB "iup.dll" ALIAS "IupGetInt" (ih AS INTEGER, name AS STRING) AS INTEGER
DECLARE FUNCTION IupGetInt2 LIB "iup.dll" ALIAS "IupGetInt2" (ih AS INTEGER, name AS STRING) AS INTEGER
DECLARE FUNCTION IupGetIntInt LIB "iup.dll" ALIAS "IupGetIntInt" (ih AS INTEGER, name AS STRING, i1 AS INTEGER, i2 AS INTEGER) AS INTEGER
DECLARE FUNCTION IupGetFloat LIB "iup.dll" ALIAS "IupGetFloat" (ih AS INTEGER, name AS STRING) AS DOUBLE
DECLARE FUNCTION IupGetDouble LIB "iup.dll" ALIAS "IupGetDouble" (ih AS INTEGER, name AS STRING) AS DOUBLE
DECLARE FUNCTION IupGetRGB LIB "iup.dll" ALIAS "IupGetRGB" (ih AS INTEGER, name AS STRING, r AS BYTE, g AS BYTE, b AS BYTE) AS VOID

DECLARE FUNCTION IupSetAttributeId LIB "iup.dll" ALIAS "IupSetAttributeId" (ih AS INTEGER, name AS STRING, id AS INTEGER, value AS STRING) AS VOID
DECLARE FUNCTION IupSetStrAttributeId LIB "iup.dll" ALIAS "IupSetStrAttributeId" (ih AS INTEGER, name AS STRING, id AS INTEGER, value AS STRING) AS VOID
' <variadic> DECLARE FUNCTION IupSetStrfId LIB "iup.dll" ALIAS "IupSetStrfId" (ih AS INTEGER, name AS STRING, id AS INTEGER, format AS STRING  format, ...) AS VOID
DECLARE FUNCTION IupSetIntId LIB "iup.dll" ALIAS "IupSetIntId" (ih AS INTEGER, name AS STRING, id AS INTEGER, value AS INTEGER) AS VOID
DECLARE FUNCTION IupSetFloatId LIB "iup.dll" ALIAS "IupSetFloatId" (ih AS INTEGER, name AS STRING, id AS INTEGER, value AS DOUBLE) AS VOID
DECLARE FUNCTION IupSetDoubleId LIB "iup.dll" ALIAS "IupSetDoubleId" (ih AS INTEGER, name AS STRING, id AS INTEGER, value AS DOUBLE) AS VOID
DECLARE FUNCTION IupSetRGBId LIB "iup.dll" ALIAS "IupSetRGBId" (ih AS INTEGER, name AS STRING, id AS INTEGER, r AS BYTE, g AS BYTE, b AS BYTE) AS VOID

DECLARE FUNCTION IupGetAttributeId LIB "iup.dll" ALIAS "IupGetAttributeId" (ih AS INTEGER, name AS STRING, id AS INTEGER) AS STRING
DECLARE FUNCTION IupGetIntId LIB "iup.dll" ALIAS "IupGetIntId" (ih AS INTEGER, name AS STRING, id AS INTEGER) AS INTEGER
DECLARE FUNCTION IupGetFloatId LIB "iup.dll" ALIAS "IupGetFloatId" (ih AS INTEGER, name AS STRING, id AS INTEGER) AS DOUBLE
DECLARE FUNCTION IupGetDoubleId LIB "iup.dll" ALIAS "IupGetDoubleId" (ih AS INTEGER, name AS STRING, id AS INTEGER) AS DOUBLE
DECLARE FUNCTION IupGetRGBId LIB "iup.dll" ALIAS "IupGetRGBId" (ih AS INTEGER, name AS STRING, id AS INTEGER, r AS BYTE, g AS BYTE, b AS BYTE) AS VOID

DECLARE FUNCTION IupSetAttributeId2 LIB "iup.dll" ALIAS "IupSetAttributeId2" (ih AS INTEGER, name AS STRING, lin AS INTEGER, col AS INTEGER, value AS STRING) AS VOID
DECLARE FUNCTION IupSetStrAttributeId2 LIB "iup.dll" ALIAS "IupSetStrAttributeId2" (ih AS INTEGER, name AS STRING, lin AS INTEGER, col AS INTEGER, value AS STRING) AS VOID
' <variadic> DECLARE FUNCTION IupSetStrfId2 LIB "iup.dll" ALIAS "IupSetStrfId2" (ih AS INTEGER, name AS STRING, lin AS INTEGER, col AS INTEGER, format AS STRING  format, ...) AS VOID
DECLARE FUNCTION IupSetIntId2 LIB "iup.dll" ALIAS "IupSetIntId2" (ih AS INTEGER, name AS STRING, lin AS INTEGER, col AS INTEGER, value AS INTEGER) AS VOID
DECLARE FUNCTION IupSetFloatId2 LIB "iup.dll" ALIAS "IupSetFloatId2" (ih AS INTEGER, name AS STRING, lin AS INTEGER, col AS INTEGER, value AS DOUBLE) AS VOID
DECLARE FUNCTION IupSetDoubleId2 LIB "iup.dll" ALIAS "IupSetDoubleId2" (ih AS INTEGER, name AS STRING, lin AS INTEGER, col AS INTEGER, value AS DOUBLE) AS VOID
DECLARE FUNCTION IupSetRGBId2 LIB "iup.dll" ALIAS "IupSetRGBId2" (ih AS INTEGER, name AS STRING, lin AS INTEGER, col AS INTEGER, r AS BYTE, g AS BYTE, b AS BYTE) AS VOID

DECLARE FUNCTION IupGetAttributeId2 LIB "iup.dll" ALIAS "IupGetAttributeId2" (ih AS INTEGER, name AS STRING, lin AS INTEGER, col AS INTEGER) AS STRING
DECLARE FUNCTION IupGetIntId2 LIB "iup.dll" ALIAS "IupGetIntId2" (ih AS INTEGER, name AS STRING, lin AS INTEGER, col AS INTEGER) AS INTEGER
DECLARE FUNCTION IupGetFloatId2 LIB "iup.dll" ALIAS "IupGetFloatId2" (ih AS INTEGER, name AS STRING, lin AS INTEGER, col AS INTEGER) AS DOUBLE
DECLARE FUNCTION IupGetDoubleId2 LIB "iup.dll" ALIAS "IupGetDoubleId2" (ih AS INTEGER, name AS STRING, lin AS INTEGER, col AS INTEGER) AS DOUBLE
DECLARE FUNCTION IupGetRGBId2 LIB "iup.dll" ALIAS "IupGetRGBId2" (ih AS INTEGER, name AS STRING, lin AS INTEGER, col AS INTEGER, r AS BYTE, g AS BYTE, b AS BYTE) AS  VOID

DECLARE FUNCTION IupSetGlobal LIB "iup.dll" ALIAS "IupSetGlobal" (name AS STRING, value AS STRING) AS VOID
DECLARE FUNCTION IupSetStrGlobal LIB "iup.dll" ALIAS "IupSetStrGlobal" (name AS STRING, value AS STRING) AS VOID
DECLARE FUNCTION IupGetGlobal LIB "iup.dll" ALIAS "IupGetGlobal" (name AS STRING) AS STRING

DECLARE FUNCTION IupSetFocus LIB "iup.dll" ALIAS "IupSetFocus" (ih AS INTEGER) AS INTEGER
DECLARE FUNCTION IupGetFocus LIB "iup.dll" ALIAS "IupGetFocus" () AS INTEGER
DECLARE FUNCTION IupPreviousField LIB "iup.dll" ALIAS "IupPreviousField" (ih AS INTEGER) AS INTEGER
DECLARE FUNCTION IupNextField LIB "iup.dll" ALIAS "IupNextField" (ih AS INTEGER) AS INTEGER

DECLARE FUNCTION IupGetCallback LIB "iup.dll" ALIAS "IupGetCallback" (ih AS INTEGER, name AS STRING) AS INTEGER
DECLARE FUNCTION IupSetCallback LIB "iup.dll" ALIAS "IupSetCallback" (ih AS INTEGER, name AS STRING, func AS INTEGER) AS INTEGER
' <variadic> DECLARE FUNCTION IupSetCallbacks LIB "iup.dll" ALIAS "IupSetCallbacks" (ih AS INTEGER, name AS STRING, func AS INTEGER func, ...) AS INTEGER

DECLARE FUNCTION IupGetFunction LIB "iup.dll" ALIAS "IupGetFunction" (name AS STRING) AS INTEGER
DECLARE FUNCTION IupSetFunction LIB "iup.dll" ALIAS "IupSetFunction" (name AS STRING, func AS INTEGER) AS INTEGER

DECLARE FUNCTION IupGetHandle LIB "iup.dll" ALIAS "IupGetHandle" (name AS STRING) AS INTEGER
DECLARE FUNCTION IupSetHandle LIB "iup.dll" ALIAS "IupSetHandle" (name AS STRING, ih AS INTEGER) AS INTEGER
DECLARE FUNCTION IupGetAllNames LIB "iup.dll" ALIAS "IupGetAllNames" (name AS STRING, n AS INTEGER) AS INTEGER
DECLARE FUNCTION IupGetAllDialogs LIB "iup.dll" ALIAS "IupGetAllDialogs" (name AS STRING, n AS INTEGER) AS INTEGER
DECLARE FUNCTION IupGetName LIB "iup.dll" ALIAS "IupGetName" (ih AS INTEGER) AS STRING

DECLARE FUNCTION IupSetAttributeHandle LIB "iup.dll" ALIAS "IupSetAttributeHandle" (ih AS INTEGER, name AS STRING, ih_named AS INTEGER) AS VOID
DECLARE FUNCTION IupGetAttributeHandle LIB "iup.dll" ALIAS "IupGetAttributeHandle" (ih AS INTEGER, name AS STRING) AS INTEGER

DECLARE FUNCTION IupGetClassName LIB "iup.dll" ALIAS "IupGetClassName" (ih AS INTEGER) AS STRING
DECLARE FUNCTION IupGetClassType LIB "iup.dll" ALIAS "IupGetClassType" (ih AS INTEGER) AS STRING
DECLARE FUNCTION IupGetAllClasses LIB "iup.dll" ALIAS "IupGetAllClasses" (names AS STRING, n AS INTEGER) AS INTEGER
DECLARE FUNCTION IupGetClassAttributes LIB "iup.dll" ALIAS "IupGetClassAttributes" (classname AS STRING, names AS STRING, n AS INTEGER) AS INTEGER
DECLARE FUNCTION IupGetClassCallbacks LIB "iup.dll" ALIAS "IupGetClassCallbacks" (classname AS STRING, names AS STRING, n AS INTEGER) AS INTEGER
DECLARE FUNCTION IupSaveClassAttributes LIB "iup.dll" ALIAS "IupSaveClassAttributes" (ih AS INTEGER) AS VOID
DECLARE FUNCTION IupCopyClassAttributes LIB "iup.dll" ALIAS "IupCopyClassAttributes" (src_ih AS INTEGER, dst_ih AS INTEGER) AS VOID
DECLARE FUNCTION IupSetClassDefaultAttribute LIB "iup.dll" ALIAS "IupSetClassDefaultAttribute" (classname AS STRING, name AS STRING, value AS STRING) AS VOID
DECLARE FUNCTION IupClassMatch LIB "iup.dll" ALIAS "IupClassMatch" (ih AS INTEGER, classname AS STRING) AS INTEGER

DECLARE FUNCTION IupCreate LIB "iup.dll" ALIAS "IupCreate" (classname AS STRING) AS INTEGER
' DECLARE FUNCTION IupCreatev LIB "iup.dll" ALIAS "IupCreatev" (classname AS STRING, VOID* *params) AS INTEGER
' <variadic> DECLARE FUNCTION IupCreatep LIB "iup.dll" ALIAS "IupCreatep" (classname AS STRING, VOID *first, ...) AS INTEGER


'************************************************************************
'*                        Elements                                      *
'************************************************************************

DECLARE FUNCTION IupFill LIB "iup.dll" ALIAS "IupFill" () AS INTEGER
DECLARE FUNCTION IupRadio LIB "iup.dll" ALIAS "IupRadio" (ih AS INTEGER) AS INTEGER
DECLARE FUNCTION IupVbox LIB "iup.dll" ALIAS "IupVbox" (ih AS INTEGER) AS INTEGER
DECLARE FUNCTION IupVboxv LIB "iup.dll" ALIAS "IupVboxv" (children AS INTEGER) AS INTEGER
DECLARE FUNCTION IupZbox LIB "iup.dll" ALIAS "IupZbox" () AS INTEGER
DECLARE FUNCTION IupZboxv LIB "iup.dll" ALIAS "IupZboxv" (children AS INTEGER) AS INTEGER
DECLARE FUNCTION IupHbox LIB "iup.dll" ALIAS "IupHbox" (ih AS INTEGER) AS INTEGER
DECLARE FUNCTION IupHboxv LIB "iup.dll" ALIAS "IupHboxv" (children AS INTEGER) AS INTEGER

' <variadic> DECLARE FUNCTION IupNormalizer  LIB "iup.dll" ALIAS "" (Ihandle* ih_first, ...) AS INTEGER
DECLARE FUNCTION IupNormalizerv LIB "iup.dll" ALIAS "" (ih_list AS INTEGER) AS INTEGER

DECLARE FUNCTION IupCbox LIB "iup.dll" ALIAS "IupCbox" (ih AS INTEGER) AS INTEGER
DECLARE FUNCTION IupCboxv LIB "iup.dll" ALIAS "IupCboxv" (children AS INTEGER) AS INTEGER
DECLARE FUNCTION IupSbox LIB "iup.dll" ALIAS "IupSbox" () AS INTEGER
DECLARE FUNCTION IupSplit LIB "iup.dll" ALIAS "IupSplit" (child1 AS INTEGER, child2 AS INTEGER) AS INTEGER
DECLARE FUNCTION IupScrollBox LIB "iup.dll" ALIAS "IupScrollBox" () AS INTEGER
DECLARE FUNCTION IupGridBox LIB "iup.dll" ALIAS "IupGridBox" () AS INTEGER
DECLARE FUNCTION IupGridBoxv LIB "iup.dll" ALIAS "IupGridBoxv" (children AS INTEGER) AS INTEGER
DECLARE FUNCTION IupExpander LIB "iup.dll" ALIAS "IupExpander" () AS INTEGER
DECLARE FUNCTION IupDetachBox LIB "iup.dll" ALIAS "IupDetachBox" () AS INTEGER
DECLARE FUNCTION IupBackgroundBox LIB "iup.dll" ALIAS "IupBackgroundBox" () AS INTEGER

DECLARE FUNCTION IupFrame LIB "iup.dll" ALIAS "IupFrame" (ih AS INTEGER) AS INTEGER

DECLARE FUNCTION IupImage LIB "iup.dll" ALIAS "IupImage" (width AS INTEGER, height AS INTEGER, pixmap AS STRING) AS INTEGER
DECLARE FUNCTION IupImageRGB LIB "iup.dll" ALIAS "IupImageRGB" (width AS INTEGER, height AS INTEGER, pixmap AS STRING) AS INTEGER
DECLARE FUNCTION IupImageRGBA LIB "iup.dll" ALIAS "IupImageRGBA" (width AS INTEGER, height AS INTEGER, pixmap AS STRING) AS INTEGER

DECLARE FUNCTION IupItem LIB "iup.dll" ALIAS "IupItem" (title AS STRING, action AS STRING) AS INTEGER
DECLARE FUNCTION IupSubmenu LIB "iup.dll" ALIAS "IupSubmenu" (title AS STRING, child AS INTEGER) AS INTEGER
DECLARE FUNCTION IupSeparator LIB "iup.dll" ALIAS "IupSeparator" () AS INTEGER
DECLARE FUNCTION IupMenu LIB "iup.dll" ALIAS "IupMenu" () AS INTEGER
DECLARE FUNCTION IupMenuv LIB "iup.dll" ALIAS "IupMenuv" (children AS INTEGER) AS INTEGER

DECLARE FUNCTION IupButton LIB "iup.dll" ALIAS "IupButton" (title AS STRING, action AS STRING) AS INTEGER
DECLARE FUNCTION IupCanvas LIB "iup.dll" ALIAS "IupCanvas" (action AS STRING) AS INTEGER
DECLARE FUNCTION IupDialog LIB "iup.dll" ALIAS "IupDialog" (ih AS INTEGER) AS INTEGER
DECLARE FUNCTION IupUser LIB "iup.dll" ALIAS "IupUser" () AS INTEGER
DECLARE FUNCTION IupLabel LIB "iup.dll" ALIAS "IupLabel" (title AS STRING) AS INTEGER
DECLARE FUNCTION IupList LIB "iup.dll" ALIAS "IupList" (action AS STRING) AS INTEGER
DECLARE FUNCTION IupText LIB "iup.dll" ALIAS "IupText" (action AS STRING) AS INTEGER
DECLARE FUNCTION IupMultiLine LIB "iup.dll" ALIAS "IupMultiLine" (action AS STRING) AS INTEGER
DECLARE FUNCTION IupToggle LIB "iup.dll" ALIAS "IupToggle" (title AS STRING, action AS STRING) AS INTEGER
DECLARE FUNCTION IupTimer LIB "iup.dll" ALIAS "IupTimer" () AS INTEGER
DECLARE FUNCTION IupClipboard LIB "iup.dll" ALIAS "IupClipboard" () AS INTEGER
DECLARE FUNCTION IupProgressBar LIB "iup.dll" ALIAS "IupProgressBar" () AS INTEGER
DECLARE FUNCTION IupVal LIB "iup.dll" ALIAS "IupVal" (type AS STRING) AS INTEGER
DECLARE FUNCTION IupTabs LIB "iup.dll" ALIAS "IupTabs" () AS INTEGER
DECLARE FUNCTION IupTabsv LIB "iup.dll" ALIAS "IupTabsv" (children AS INTEGER) AS INTEGER
DECLARE FUNCTION IupTree LIB "iup.dll" ALIAS "IupTree" () AS INTEGER
DECLARE FUNCTION IupLink LIB "iup.dll" ALIAS "IupLink" (url AS STRING, title AS STRING) AS INTEGER

' Old controls, use SPIN attribute of IupText
' Ihandle*  IupSpin       (void) AS INTEGER
' Ihandle*  IupSpinbox    (Ihandle* child) AS INTEGER

'************************************************************************
'*                           IUP Controls                               *
'************************************************************************
DECLARE FUNCTION IupControlsOpen LIB "iupcontrols.dll" ALIAS "IupControlsOpen" () AS INTEGER
DECLARE FUNCTION IupMatrix LIB "iupcontrols.dll" ALIAS "IupMatrix" (action AS STRING) AS INTEGER


'************************************************************************
'*                        Support Functions                             *
'************************************************************************
FUNCTION CreateImg(s AS STRING, base AS INTEGER) AS STRING
  DIM slen AS INTEGER
  DIM pos AS INTEGER
  DIM tchr AS STRING
  DIM tstr AS STRING
  DIM srtn AS STRING
  slen = LEN(s)
  pos = 1
  tstr = ""
  tchr = ""
  srtn = ""
  IF base THEN
  	base = 0
  ELSE
  	base = 1
  END IF
  WHILE pos <= slen
    tchr = MID$(s, pos, 1)
    IF tchr = "," THEN
      srtn = srtn & CHR$(VAL(tstr) + base)
      tstr = ""
      tchr = ""
    ELSE
      tstr = tstr & tchr
    END IF
    pos = pos + 1
  WEND
  srtn = srtn & CHR$(VAL(tstr)+ base)
  RESULT = srtn
END FUNCTION
