'***************
'* Windows.inc *
'***************

$IfNDef Windowsinc
$Define Windowsinc

$Const IDI_APPLICATION = 32512
$Const IDI_HAND = 32513
$Const IDI_QUESTION = 32514
$Const IDI_EXCLAMATION = 32515
$Const IDI_ASTERISK = 32516
$Const IDI_WINLOGO = 32517

$Const CS_VREDRAW = 1
$Const CS_HREDRAW = 2
$Const CS_KEYCVTWINDOW = 4
$Const CS_DBLCLKS = 8
$Const CS_CLASSDC = 64
$Const CS_PARENTDC = 128

$Const CW_USEDEFAULT = 2147483648
$Const HWND_DESKTOP = 0

$Const WS_OVERLAPPED    = 0x00000000
$Const WS_POPUP         = 0x80000000
$Const WS_CHILD         = 0x40000000
$Const WS_MINIMIZE      = 0x20000000
$Const WS_VISIBLE       = 0x10000000
$Const WS_DISABLED      = 0x08000000
$Const WS_CLIPSIBLINGS  = 0x04000000
$Const WS_CLIPCHILDREN  = 0x02000000
$Const WS_MAXIMIZE      = 0x01000000
$Const WS_CAPTION       = 0x00C00000   ' WS_BORDER | WS_DLGFRAME
$Const WS_BORDER        = 0x00800000
$Const WS_DLGFRAME      = 0x00400000
$Const WS_VSCROLL       = 0x00200000
$Const WS_HSCROLL       = 0x00100000
$Const WS_SYSMENU       = 0x00080000
$Const WS_THICKFRAME    = 0x00040000
$Const WS_GROUP         = 0x00020000
$Const WS_TABSTOP       = 0x00010000
$Const WS_MINIMIZEBOX   = 0x00020000
$Const WS_MAXIMIZEBOX   = 0x00010000
$Const WS_TILED         = 0x00000000   ' WS_OVERLAPPED
$Const WS_ICONIC        = 0x20000000   ' WS_MINIMIZE
$Const WS_SIZEBOX       = 0x00040000   ' WS_THICKFRAME
$Const WS_TILEDWINDOW   = 0x00CF0000   ' WS_OVERLAPPEDWINDOW

' WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | 
' WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX
$Const WS_OVERLAPPEDWINDOW = 0x00CF0000 

$Const WS_POPUPWINDOW   = 0X80880000   ' WS_POPUP | WS_BORDER | WS_SYSMENU
$Const WS_CHILDWINDOW   = 0x40000000   ' WS_CHILD

$Const WS_EX_DLGMODALFRAME  = 0x00000001
$Const WS_EX_NOPARENTNOTIFY = 0x00000004
$Const WS_EX_TOPMOST        = 0x00000008
$Const WS_EX_ACCEPTFILES    = 0x00000010
$Const WS_EX_TRANSPARENT    = 0x00000020
$Const WS_EX_MDICHILD       = 0x00000040
$Const WS_EX_TOOLWINDOW     = 0x00000080
$Const WS_EX_WINDOWEDGE     = 0x00000100
$Const WS_EX_CLIENTEDGE     = 0x00000200
$Const WS_EX_CONTEXTHELP    = 0x00000400
$Const WS_EX_RIGHT          = 0x00001000
$Const WS_EX_LEFT           = 0x00000000
$Const WS_EX_RTLREADING     = 0x00002000
$Const WS_EX_LTRREADING     = 0x00000000
$Const WS_EX_LEFTSCROLLBAR  = 0x00004000
$Const WS_EX_RIGHTSCROLLBAR = 0x00000000
$Const WS_EX_CONTROLPARENT  = 0x00010000
$Const WS_EX_STATICEDGE     = 0x00020000
$Const WS_EX_APPWINDOW      = 0x00040000

' WS_EX_WINDOWEDGE | WS_EX_CLIENTEDGE
$Const WS_EX_OVERLAPPEDWINDOW  = 0x00000300 
' WS_EX_WINDOWEDGE | WS_EX_TOOLWINDOW | WS_EX_TOPMOST
$Const WS_EX_PALETTEWINDOW     = 0X00000488 

$Const SS_LEFT            = 0x00000000
$Const SS_CENTER          = 0x00000001
$Const SS_RIGHT           = 0x00000002
$Const SS_ICON            = 0x00000003
$Const SS_BLACKRECT       = 0x00000004
$Const SS_GRAYRECT        = 0x00000005
$Const SS_WHITERECT       = 0x00000006
$Const SS_BLACKFRAME      = 0x00000007
$Const SS_GRAYFRAME       = 0x00000008
$Const SS_WHITEFRAME      = 0x00000009
$Const SS_USERITEM        = 0x0000000A
$Const SS_SIMPLE          = 0x0000000B
$Const SS_LEFTNOWORDWRAP  = 0x0000000C
$Const SS_OWNERDRAW       = 0x0000000D
$Const SS_BITMAP          = 0x0000000E
$Const SS_ENHMETAFILE     = 0x0000000F
$Const SS_ETCHEDHORZ      = 0x00000010
$Const SS_ETCHEDVERT      = 0x00000011
$Const SS_ETCHEDFRAME     = 0x00000012
$Const SS_TYPEMASK        = 0x0000001F
$Const SS_REALSIZECONTROL = 0x00000040
$Const SS_NOPREFIX        = 0x00000080 'Don't do "&" character translation
$Const SS_NOTIFY          = 0x00000100
$Const SS_CENTERIMAGE     = 0x00000200
$Const SS_RIGHTJUST       = 0x00000400
$Const SS_REALSIZEIMAGE   = 0x00000800
$Const SS_SUNKEN          = 0x00001000
$Const SS_EDITCONTROL     = 0x00002000
$Const SS_ENDELLIPSIS     = 0x00004000
$Const SS_PATHELLIPSIS    = 0x00008000
$Const SS_WORDELLIPSIS    = 0x0000C000
$Const SS_ELLIPSISMASK    = 0x0000C000

$Const STM_SETICON  = 0x0170
$Const STM_GETICON  = 0x0171
$Const STM_SETIMAGE = 0x0172
$Const STM_GETIMAGE = 0x0173
$Const STN_CLICKED  = 0
$Const STN_DBLCLK   = 1
$Const STN_ENABLE   = 2
$Const STN_DISABLE  = 3
$Const STM_MSGMAX   = 0x0174

$Const BS_PUSHBUTTON = 0
$Const BS_DEFPUSHBUTTON = 1
$Const BS_CHECKBOX = 2
$Const BS_AUTOCHECKBOX = 3
$Const BS_RADIOBUTTON = 4
$Const BS_3STATE = 5
$Const BS_AUTO3STATE = 6
$Const BS_GROUPBOX = 7
$Const BS_USERBUTTON = 8
$Const BS_AUTORADIOBUTTON = 9
$Const BS_OWNERDRAW = 11
$Const BS_LEFTTEXT = 32
$Const BS_TEXT = 0
$Const BS_ICON = 64
$Const BS_BITMAP = 128
$Const BS_LEFT = 256
$Const BS_RIGHT = 512
$Const BSCENTER = 768
$Const BSTOP = 1024
$Const BSBOTTOM = 2024
$Const BSVCENTER = 3072
$Const BSPUSHLIKE = 4096
$Const BSMULTILINE = 8192
$Const BSNOTIFY = 16384
$Const BSFLAT = 32768
$Const BSRIGHTBUTTON = 32

$Const BM_GETCHECK      = 0x00F0
$Const BM_SETCHECK      = 0x00F1
$Const BM_GETSTATE      = 0x00F2
$Const BM_SETSTATE      = 0x00F3
$Const BM_SETSTYLE      = 0x00F4
$Const BM_CLICK         = 0x00F5
$Const BM_GETIMAGE      = 0x00F6
$Const BM_SETIMAGE      = 0x00F7

$Const BST_UNCHECKED     = 0x0000
$Const BST_CHECKED       = 0x0001
$Const BST_INDETERMINATE = 0x0002
$Const BST_PUSHED        = 0x0004
$Const BST_FOCUS         = 0x0008

$Const BN_CLICKED       = 0
$Const BN_PAINT         = 1
$Const BN_HILITE        = 2
$Const BN_UNHILITE      = 3
$Const BN_DISABLE       = 4
$Const BN_DOUBLECLICKED = 5
$Const BN_PUSHED        = 1   ' BN_HILITE
$Const BN_UNPUSHED      = 3   ' BN_UNHILITE
$Const BN_DBLCLK        = 5   ' BN_DOUBLECLICKED
$Const BN_SETFOCUS      = 6
$Const BN_KILLFOCUS     = 7

$Const LB_OKAY        = 0
$Const LB_ERR         = -1
$Const LB_ERRSPACE    = -2

$Const LBN_ERRSPACE = -2
$Const LBN_SELCHANGE = 1
$Const LBN_DBLCLK = 2
$Const LBN_SELCANCEL = 3
$Const LBN_SETFOCUS = 4
$Const LBN_KILLFOCUS = 5

$Const OCR_NORMAL = 32512
$Const OCR_IBEAM = 32513
$Const OCR_WAIT = 32514
$Const OCR_CROSS = 32515
$Const OCR_UP = 32516
$Const OCR_SIZE = 32640
$Const OCR_ICON = 32641
$Const OCR_SIZENWSE = 32642
$Const OCR_SIZENESW = 32643
$Const OCR_SIZEWE = 32644
$Const OCR_SIZENS = 32645
$Const OCR_SIZEALL = 32646
$Const OCR_ICOCUR = 32647
$Const OCR_NO = 32648

$Const LB_ADDSTRING           = 0x0180
$Const LB_INSERTSTRING        = 0x0181
$Const LB_DELETESTRING        = 0x0182
$Const LB_SELITEMRANGEEX      = 0x0183
$Const LB_RESETCONTENT        = 0x0184
$Const LB_SETSEL              = 0x0185
$Const LB_SETCURSEL           = 0x0186
$Const LB_GETSEL              = 0x0187
$Const LB_GETCURSEL           = 0x0188
$Const LB_GETTEXT             = 0x0189
$Const LB_GETTEXTLEN          = 0x018A
$Const LB_GETCOUNT            = 0x018B
$Const LB_SELECTSTRING        = 0x018C
$Const LB_DIR                 = 0x018D
$Const LB_GETTOPINDEX         = 0x018E
$Const LB_FINDSTRING          = 0x018F
$Const LB_GETSELCOUNT         = 0x0190
$Const LB_GETSELITEMS         = 0x0191
$Const LB_SETTABSTOPS         = 0x0192
$Const LB_GETHORIZONTALEXTENT = 0x0193
$Const LB_SETHORIZONTALEXTENT = 0x0194
$Const LB_SETCOLUMNWIDTH      = 0x0195
$Const LB_ADDFILE             = 0x0196
$Const LB_SETTOPINDEX         = 0x0197
$Const LB_GETITEMRECT         = 0x0198
$Const LB_GETITEMDATA         = 0x0199
$Const LB_SETITEMDATA         = 0x019A
$Const LB_SELITEMRANGE        = 0x019B
$Const LB_SETANCHORINDEX      = 0x019C
$Const LB_GETANCHORINDEX      = 0x019D
$Const LB_SETCARETINDEX       = 0x019E
$Const LB_GETCARETINDEX       = 0x019F
$Const LB_SETITEMHEIGHT       = 0x01A0
$Const LB_GETITEMHEIGHT       = 0x01A1
$Const LB_FINDSTRINGEXACT     = 0x01A2
$Const LB_SETLOCALE           = 0x01A5
$Const LB_GETLOCALE           = 0x01A6
$Const LB_SETCOUNT            = 0x01A7
$Const LB_INITSTORAGE         = 0x01A8
$Const LB_ITEMFROMPOINT       = 0x01A9
$Const LB_GETLISTBOXINFO      = 0x01B2

$Const LBS_NOTIFY           = 0x0001
$Const LBS_SORT             = 0x0002
$Const LBS_NOREDRAW         = 0x0004
$Const LBS_MULTIPLESEL      = 0x0008
$Const LBS_OWNERDRAWFIXED   = 0x0010
$Const LBS_OWNERDRAWVARIABLE= 0x0020
$Const LBS_HASSTRINGS       = 0x0040
$Const LBS_USETABSTOPS      = 0x0080
$Const LBS_NOINTEGRALHEIGHT = 0x0100
$Const LBS_MULTICOLUMN      = 0x0200
$Const LBS_WANTKEYBOARDINPUT= 0x0400
$Const LBS_EXTENDEDSEL      = 0x0800
$Const LBS_DISABLENOSCROLL  = 0x1000
$Const LBS_NODATA           = 0x2000
$Const LBS_NOSEL            = 0x4000
$Const LBS_COMBOBOX         = 0x8000
'$Const LBS_STANDARD          (LBS_NOTIFY | LBS_SORT | WS_VSCROLL | WS_BORDER)

$Const clScrollBar = -2147483648
$Const clBackGround = -2147483647
$Const clActiveCaption = -2147483646
$Const clInActiveCaption = -2147483645
$Const clMenu = -2147483644
$Const clWindow = -2147483643
$Const clWindowFrame = -2147483642
$Const clMenuText = -2147483641
$Const clWindowText = -2147483640
$Const clCaptionText = -2147483639
$Const clActiveBorder = -2147483638
$Const clInActiveBorder = -2147483637
$Const clAppWorkSpace = -2147483636
$Const clHilight = -2147483635
$Const clHilightText = -2147483634
$Const clBtnFace = -2147483633
$Const clBtnShadow = -2147483632
$Const clGrayText = -2147483631
$Const clBtnText = -2147483630
$Const clInActiveCaptionText = -2147483629
$Const clBtnHighlight = -2147483628
$Const cl3DDkShadow = -2147483627
$Const cl3DLight = -2147483626
$Const clInfoText = -2147483625
$Const clInfoBk3DDkShadow = -2147483624
$Const clNone = 536870911
$Const clDefault = 536870912

$Const COLOR_SCROLLBAR = 0
$Const COLOR_BACKGROUND = 1
$Const COLOR_ACTIVECAPTION = 2
$Const COLOR_INACTIVECAPTION = 3
$Const COLOR_MENU = 4
$Const COLOR_WINDOW = 5
$Const COLOR_WINDOWFRAME = 6
$Const COLOR_MENUTEXT = 7
$Const COLOR_WINDOWTEXT = 8
$Const COLOR_CAPTIONTEXT = 9
$Const COLOR_ACTIVEBORDER = 10
$Const COLOR_INACTIVEBORDER = 11
$Const COLOR_APPWORKSPACE = 12
$Const COLOR_HIGHLIGHT = 13
$Const COLOR_HIGHLIGHTTEXT = 14
$Const COLOR_BTNFACE = 15
$Const COLOR_BTNSHADOW = 16
$Const COLOR_GRAYTEXT = 17
$Const COLOR_BTNTEXT = 18
$Const COLOR_INACTIVECAPTIONTEXT = 19
$Const COLOR_BTNHIGHLIGHT = 20

$Const WM_NULL                         = 0x0000
$Const WM_CREATE                       = 0x0001
$Const WM_DESTROY                      = 0x0002
$Const WM_MOVE                         = 0x0003
$Const WM_SIZE                         = 0x0005

$Const WM_ACTIVATE                     = 0x0006

$Const     WA_INACTIVE    =  0
$Const     WA_ACTIVE      =  1
$Const     WA_CLICKACTIVE =  2

$Const WM_SETFOCUS                     = 0x0007
$Const WM_KILLFOCUS                    = 0x0008
$Const WM_ENABLE                       = 0x000A
$Const WM_SETREDRAW                    = 0x000B
$Const WM_SETTEXT                      = 0x000C
$Const WM_GETTEXT                      = 0x000D
$Const WM_GETTEXTLENGTH                = 0x000E
$Const WM_PAINT                        = 0x000F
$Const WM_CLOSE                        = 0x0010
$Const WM_QUERYENDSESSION              = 0x0011
$Const WM_QUERYOPEN                    = 0x0013
$Const WM_ENDSESSION                   = 0x0016
$Const WM_QUIT                         = 0x0012
$Const WM_ERASEBKGND                   = 0x0014
$Const WM_SYSCOLORCHANGE               = 0x0015
$Const WM_SHOWWINDOW                   = 0x0018
$Const WM_WININICHANGE                 = 0x001A
$Const WM_SETTINGCHANGE                = 0x001A 'WM_WININICHANGE


$Const WM_DEVMODECHANGE                = 0x001B
$Const WM_ACTIVATEAPP                  = 0x001C
$Const WM_FONTCHANGE                   = 0x001D
$Const WM_TIMECHANGE                   = 0x001E
$Const WM_CANCELMODE                   = 0x001F
$Const WM_SETCURSOR                    = 0x0020
$Const WM_MOUSEACTIVATE                = 0x0021
$Const WM_CHILDACTIVATE                = 0x0022
$Const WM_QUEUESYNC                    = 0x0023

$Const WM_GETMINMAXINFO                = 0x0024


$Const WM_INITDIALOG =    0x0110
$Const WM_COMMAND =       0x0111
$Const WM_SYSCOMMAND =    0x0112
$Const WM_TIMER =         0x0113
$Const WM_HSCROLL =       0x0114
$Const WM_VSCROLL =       0x0115
$Const WM_INITMENU =      0x0116
$Const WM_INITMENUPOPUP = 0x0117
$Const WM_MENUSELECT =    0x011F
$Const WM_MENUCHAR =      0x0120
$Const WM_ENTERIDLE =     0x0121

$Const WM_GETICON = 0x007F
$Const WM_SETICON = 0x0080

$Const WM_PAINTICON         = 0x0026
$Const WM_ICONERASEBKGND    = 0x0027
$Const WM_NEXTDLGCTL        = 0x0028
$Const WM_SPOOLERSTATUS     = 0x002A
$Const WM_DRAWITEM          = 0x002B
$Const WM_MEASUREITEM       = 0x002C
$Const WM_DELETEITEM        = 0x002D
$Const WM_VKEYTOITEM        = 0x002E
$Const WM_CHARTOITEM        = 0x002F
$Const WM_SETFONT           = 0x0030
$Const WM_GETFONT           = 0x0031
$Const WM_SETHOTKEY         = 0x0032
$Const WM_GETHOTKEY         = 0x0033
$Const WM_QUERYDRAGICON     = 0x0037
$Const WM_COMPAREITEM       = 0x0039
$Const WM_COMPACTING        = 0x0041
$Const WM_WINDOWPOSCHANGING = 0x0046
$Const WM_WINDOWPOSCHANGED  = 0x0047
$Const WM_POWER             = 0x0048

$Const WM_MOUSEFIRST              = 0x0200
$Const WM_MOUSEMOVE               = 0x0200
$Const WM_LBUTTONDOWN             = 0x0201
$Const WM_LBUTTONUP               = 0x0202
$Const WM_LBUTTONDBLCLK           = 0x0203
$Const WM_RBUTTONDOWN             = 0x0204
$Const WM_RBUTTONUP               = 0x0205
$Const WM_RBUTTONDBLCLK           = 0x0206
$Const WM_MBUTTONDOWN             = 0x0207
$Const WM_MBUTTONUP               = 0x0208
$Const WM_MBUTTONDBLCLK           = 0x0209

$Const ICON_SMALL = 0
$Const ICON_BIG = 1
$Const ICON_SMALL2 = 2

$Const SW_HIDE = 0
$Const SW_SHOWNORMAL = 1
$Const SW_NORMAL = 1
$Const SW_SHOWMINIMIZED = 2
$Const SW_SHOWMAXIMIZED = 3
$Const SW_MAXIMIZE = 3
$Const SW_SHOWNOACTIVATE = 4
$Const SW_SHOW = 5
$Const SW_MINIMIZE = 6
$Const SW_SHOWMINNOACTIVE = 7
$Const SW_SHOWNA = 8
$Const SW_RESTORE = 9
$Const SW_SHOWDEFAULT = 10
$Const SW_MAX = 10

$Const IDC_ARROW = 32512
$Const IDC_IBEAM = 32513
$Const IDC_WAIT = 32514
$Const IDC_CROSS = 32515
$Const IDC_UPARROW = 32516
$Const IDC_SIZE = 32640
$Const IDC_ICON = 32641
$Const IDC_SIZENWSE = 32642
$Const IDC_SIZENESW = 32643
$Const IDC_SIZEWE = 32644
$Const IDC_SIZENS = 32645
$Const IDC_SIZEALL = 32646
$Const IDC_NO = 32648
$Const IDC_APPSTARTING = 32650

Type POINT
  x As Integer
  y As Integer
End Type

Type RECT
  Left As Integer
  Top As Integer
  Right As Integer
  Bottom As Integer
End Type

Type Size
  cx As Integer
  cy As Integer
End Type

Type STARTUPINFO
  cb As Integer
  lpReserved As String
  lpDesktop As String
  lpTitle As String
  dwX As Integer
  dwY As Integer
  dwXSize As Integer
  dwYSize As Integer
  dwXCountChars As Integer
  dwYCountChars As Integer
  dwFillAttribute As Integer
  dwFlags As Integer
  wShowWindow As Integer
  cbReserved2 As Integer
  lpReserved2 As Integer
  hStdInput As Integer
  hStdOutput As Integer
  hStdError As Integer
End Type

Type NMHDR
  hwndFrom As Integer 
  idFrom As Integer 
  code As Integer 
End Type

Type WNDCLASS
  style As Integer
  lpfnwndproc As Integer
  cbClsextra As Integer
  cbWndExtra2 As Integer
  hInstance As Integer
  hIcon As Integer
  hCursor As Integer
  hbrBackground As Integer
  lpszMenuName As String
  lpszClassName As String
End Type

Type WNDCLASSEX
  cbSize As Integer
  style As Integer
  lpfnWndProc As Integer
  cbClsExtra As Integer
  cbWndExtra As Integer
  hInstance As Integer
  hIcon As Integer
  hCursor As Integer
  hbrBackground As Integer
  lpszMenuName As String
  lpszClassName As String
  hIconSm As Integer
End Type

Type CREATESTRUCT
  lpCreateParams As Integer
  hInstance As Integer
  hMenu As Integer
  hWndParent As Integer
  cy As Integer
  cx As Integer
  y As Integer
  x As Integer
  style As Integer
  lpszName As String
  lpszClass As String
  ExStyle As Integer
End Type

TYPE MSG
    hwnd As Integer
    message As Integer
    wParam As Integer
    lParam As Integer
    time As Integer
    x As Integer    'HERE NEED TO BE (pt As POINTAPI)
    y As Integer
'    pt As Integer 
END TYPE

Type PAINTSTRUCT 
   hdc As Integer 
   fErase As Integer 
   ' -- Begin rcPaint As RECT -- 
   left As Integer 
   top As Integer 
   right As Integer 
   bottom As Integer 
   ' -- End rcPaint As RECT -- 
   fRestore As Integer 
   fIncUpdate As Integer 
   ' -- Begin BYTE rgbReserved[32] -- 
   rgbReserved1 As Integer 
   rgbReserved2 As Integer 
   rgbReserved3 As Integer 
   rgbReserved4 As Integer 
   rgbReserved5 As Integer 
   rgbReserved6 As Integer 
   rgbReserved7 As Integer 
   rgbReserved8 As Integer 
   ' -- End BYTE rgbReserved[32] -- 
End Type 

Declare Function BeginPaint Lib "user32" Alias "BeginPaint" _ 
  (ByVal hWnd As Integer, _ 
   ByRef lpPaint As PAINTSTRUCT) _ 
   As Integer 

Declare Function EndPaint Lib "user32" Alias "EndPaint" _ 
  (ByVal hWnd As Integer, _ 
   ByRef lpPaint As PAINTSTRUCT) _ 
   As Integer 

Declare Function TextOut Lib "gdi32" Alias "TextOutA" _ 
  (ByVal hdc As Integer, _ 
   ByVal xStart As Integer, _ 
   ByVal yStart As Integer, _ 
   ByVal lpString As String, _ 
   ByVal nCount As Integer) _ 
   As Integer 

$Const DT_TOP =  0x0000
$Const DT_LEFT = 0x0000
$Const DT_CENTER = 0x0001
$Const DT_RIGHT =  0x0002
$Const DT_VCENTER = 0x0004
$Const DT_BOTTOM =  0x0008
$Const DT_WORDBREAK = 0x0010
$Const DT_SINGLELINE = 0x0020
$Const DT_EXPANDTABS = 0x0040
$Const DT_TABSTOP = 0x0080
$Const DT_NOCLIP = 0x0100
$Const DT_EXTERNALLEADING = 0x0200
$Const DT_CALCRECT = 0x0400
$Const DT_NOPREFIX = 0x0800
$Const DT_INTERNAL = 0x1000

Declare Function DrawText Lib "user32" Alias "DrawTextA" _
  (ByVal hdc As Integer, _ 
   ByVal lpStr As String, _
   ByVal nCount As Integer, _
   ByRef lpRect As RECT, _
   ByVal wFormat As Integer) _
   As Integer

Declare Function EnumChildWindows Lib "user32" Alias "EnumChildWindows" _
  (ByVal hWndParent As Integer, _
   ByVal lpEnumFunc As Integer, _
   ByVal lParam As Integer) _
   As Integer

$Const GW_HWNDFIRST    = 0
$Const GW_HWNDLAST     = 1
$Const GW_HWNDNEXT     = 2
$Const GW_HWNDPREV     = 3
$Const GW_OWNER        = 4
$Const GW_CHILD        = 5
$Const GW_ENABLEDPOPUP = 6
$Const GW_MAX          = 6

Declare Function GetTopWindow Lib "user32" Alias "GetTopWindow" _
  (ByVal hwnd As Integer) _
   As Integer

Declare Function GetNextWindow Lib "user32" Alias "GetWindow" _
  (ByVal hwnd As Integer, _
   ByVal wFlag As Integer) _
   As Integer

Declare Function GetClientRect Lib "user32" Alias "GetClientRect" _
  (ByVal hWnd As Integer, _ 
   ByRef lpRect As RECT) _
   As Integer

Declare Function GetWindowRect Lib "user32" Alias "GetWindowRect" _
  (ByVal hwnd As Integer, _
   ByRef lpRect As RECT) _
   As Integer

Declare Function RegisterClass Lib "user32" Alias "RegisterClassA" _
  (ByRef Class As WNDCLASS) _
   As Integer

Declare Function RegisterClassEx LIB "USER32" Alias "RegisterClassExA" _
  (ByRef wndcls As WNDCLASSEX) _
   As Integer

Declare Function UnregisterClass Lib "user32" Alias "UnregisterClassA" _ 
  (ByVal lpClassName As String, _ 
   ByVal hInstance As Integer) _ 
   As Integer 

Declare Function GetMessage Lib "user32" Alias "GetMessageA" _ 
  (ByRef lpMsg As MSG, _ 
   ByVal hWnd As Integer, _ 
   ByVal wMsgFilterMin As Integer, _ 
   ByVal wMsgFilterMax As Integer) _ 
   As Integer 

Declare Function TranslateMessage Lib "user32" Alias "TranslateMessage" _ 
  (ByRef lpMsg As MSG) _ 
   As Integer 

Declare Function DispatchMessage Lib "user32" Alias "DispatchMessageA" _ 
  (ByRef lpMsg As MSG) _ 
   As Integer 

Declare Function SendMessage Lib "user32" Alias "SendMessageA" _ 
  (hWnd As Integer, _ 
   wMsg As Integer, _ 
   wParam As Integer, _ 
   lParam As Integer) _ 
   As Integer 

Declare Function LoadBitmap Lib "user32" Alias "LoadBitmapA" _
  (ByVal hInstance As Integer, _
   ByVal lpBitmapName As String) _
   As Integer

Declare Function LoadCursor LIB "USER32" Alias "LoadCursorA" _
  (hInstance As Integer, _ 
   lpCursorName As String) _
   As Integer

Declare Function LoadIcon Lib "user32" Alias "LoadIconA" _ 
  (hInstance As Integer, _ 
   lpIconName As String) _ 
   As Integer 

Declare Function DestroyIcon Lib "user32" Alias "DestroyIcon" _ 
  (hIcon As Integer) _
   As Integer

Declare Function GetCursor Lib "user32" Alias "GetCursor" () As Integer

Declare Function SetCursor Lib "user32" Alias "SetCursor" _
  (ByVal hCursor As Integer) _
   As Integer

'Declare Function CopyCursor Lib "user32" Alias "CopyCursor" _
'  (ByVal hcur As Integer) As Integer
  
Declare Function CopyIcon Lib "user32" Alias "CopyIcon" _
  (ByVal hIcon As Integer) As Integer
  
Declare Function SetSystemCursor Lib "user32" Alias "SetSystemCursor" _
  (ByVal hcur As Integer, _
   ByVal id As Integer) _
   As Integer

Declare Function CreateCompatibleDC Lib "gdi32" Alias "CreateCompatibleDC" _
  (ByVal hdc As Integer) _
   As Integer

Declare Function CreateWindowEx Lib "user32" Alias "CreateWindowExA" _ 
 (ByVal dwExStyle As Integer, _ 
  ByVal lpClassName As String, _ 
  ByVal lpWindowName As String, _ 
  ByVal dwStyle As Integer, _ 
  ByVal x As Integer, _ 
  ByVal y As Integer, _ 
  ByVal nWidth As Integer, _ 
  ByVal nHeight As Integer, _ 
  ByVal hWndParent As Integer, _ 
  ByVal hMenu As Integer, _ 
  ByVal hInstance As Integer, _ 
  ByVal lpParam As Integer) _ 
  As Integer 

Declare Function UpdateWindow LIB "USER32" Alias "UpdateWindow" _
  (ByVal hWnd As Integer) _
   As Integer

Declare Function ShowWindow LIB "USER32" Alias "ShowWindow" _
  (ByVal hWnd As Integer, _
   ByVal nCmdShow As Integer) _
   As Integer

Declare Function DefWindowProc Lib "user32" Alias "DefWindowProcA" _ 
  (ByVal hWnd As Integer, _ 
   ByVal wMsg As Integer, _ 
   ByVal wParam As Integer, _ 
   ByVal lParam As Integer) _ 
   As Integer 

Declare Sub PostQuitMessage Lib "user32" Alias "PostQuitMessage" _ 
  (ByVal nExitCode As Integer) 

' This declaration assumes that lpModuleName will be set 
' to zero to get the handle to the file used to create 
' the calling process. 

Declare Function GetModuleHandle Lib "kernel32" Alias "GetModuleHandleA" _ 
   (lpModuleName As Integer) _ 
   As Integer 

Declare Function GetActiveWindow Lib "user32" Alias "GetActiveWindow" _ 
   () _ 
   As Integer 

Declare Sub ExitProcess Lib "kernel32" Alias "ExitProcess" _ 
   (uExitCode As Integer) 
    
Declare Function MsgBox Lib "user32" Alias "MessageBoxA" _ 
   (hWnd As Integer, _ 
   lpText As String, _ 
   lpcaption As String, _ 
   wType As Integer) _ 
   As Integer 
    
Declare Function MessageBox Lib "user32" Alias "MessageBoxA" _ 
  (ByVal hWnd As Integer, _ 
   ByVal lpText As String, _ 
   ByVal lpcaption As String, _ 
   uType As Integer) _ 
   As Integer 

$Const FORMAT_MESSAGE_ALLOCATE_BUFFER = 256 
$Const FORMAT_MESSAGE_IGNORE_INSERTS = 512 
$Const FORMAT_MESSAGE_FROM_STRING = 1024 
$Const FORMAT_MESSAGE_FROM_HMODULE = 2048 
$Const FORMAT_MESSAGE_FROM_SYSTEM = 4096 
$Const FORMAT_MESSAGE_ARGUMENT_ARRAY = 8192 
$Const FORMAT_MESSAGE_MAX_WIDTH_MASK = 255 

Declare Function GetLastError Lib "kernel32" Alias "GetLastError" _ 
  () As Integer 

Declare Sub SetLastError Lib "kernel32" Alias "SetLastError" _ 
   (dwErrCode As Integer) 

' Use this for LANG_NEUTRAL+SUBLANG_NEUTRAL. 
$Const LANG_NEUTRAL = 0 

Declare Function FormatMessage Lib "kernel32" Alias "FormatMessageA" _ 
  (ByVal dwFlags As Integer, _ 
   lpSource As Integer, _ 
   ByVal dwMessageId As Integer, _ 
   ByVal dwLanguageId As Integer, _ 
   ByVal lpBuffer As Integer, _ 
   ByVal nSize As Integer, _ 
   Arguments As Integer) _ 
   As Integer 

Declare Function LocalFree Lib "kernel32" Alias "LocalFree" _ 
   (hMem As Integer) _ 
   As Integer 
    
$Const MB_DEFBUTTON1 = 0
$Const MB_DEFBUTTON2 = 256
$Const MB_DEFBUTTON3 = 512
$Const MB_ICONASTERISK = 64
$Const MB_ICONEXCLAMATION = 48
$Const MB_ICONHAND = 16
$Const MB_ICONINFORMATION = 64
$Const MB_ICONQUESTION = 32
$Const MB_ICONSTOP = 16
$Const MB_OK = 0
$Const VBOKONLY = 0 'VB COMPATIBILITY
$Const MB_OKCANCEL = 1
$Const MB_YESNO = 4
$Const MB_YESNOCANCEL = 3
$Const MB_ABORTRETRYIGNORE = 2
$Const MB_RETRYCANCEL = 5

$Const GWL_WNDPROC = -4

$Const GCL_MENUNAME      = -8
$Const GCL_HBRBACKGROUND = -10
$Const GCL_HCURSOR       = -12
$Const GCL_HICON         = -14
$Const GCL_HMODULE       = -16
$Const GCL_CBWNDEXTRA    = -18
$Const GCL_CBCLSEXTRA    = -20
$Const GCL_WNDPROC       = -24
$Const GCL_STYLE         = -26
$Const GCW_ATOM          = -32

$Const IMAGE_BITMAP   =     0
$Const IMAGE_ICON     =     1
$Const IMAGE_CURSOR   =     2

Declare Function GetWindowLong Lib "user32" Alias "GetWindowIntegerA" _ 
  (hwnd As Integer, _ 
   nIndex As Integer) _ 
   As Integer 

Declare Function SetWindowLong Lib "user32" Alias "SetWindowIntegerA" _ 
  (hwnd As Integer, _ 
   nIndex As Integer, _ 
   dwNewLong As Integer) _ 
   As Integer 

Declare Function CallWindowProc Lib "user32" Alias "CallWindowProcA" _ 
  (lpPrevWndFunc As Integer, _ 
   hWnd As Integer, _ 
   wMsg As Integer, _ 
   wParam As Integer, _ 
   lParam As Integer) _ 
   As Integer 

Declare Function GetClassLong Lib "user32" Alias "GetClassLongA" _
  (ByVal hwnd As Integer, _
   ByVal nIndex As Integer) _
   As Integer

Declare Function SetClassLong Lib "user32" Alias "SetClassLongA" _
  (ByVal hwnd As Integer, _
   ByVal nIndex As Integer, _
   ByVal dwNewLong As Integer) _
   As Integer

Declare Function GetDesktopWindow Lib "user32" Alias "GetDesktopWindow" () As Integer

Declare Function GetProcAddress Lib "kernel32" Alias "GetProcAddress" _
  (hModule As Integer, _
   lpProcName As String) _
   As Integer

$Const IDOK     = 1
$Const IDCANCEL = 2
$Const IDABORT  = 3
$Const IDRETRY  = 4
$Const IDIGNORE = 5
$Const IDYES    = 6
$Const IDNO     = 7
$Const IDCLOSE  = 8
$Const IDHELP   = 9

Declare Function DialogBoxParam Lib "user32" Alias "DialogBoxParamA" _
  (hInstance As Integer, _
   lpTemplateName As String, _
   hWndParent As Integer, _
   lpDialogFunc As Integer, _
   dwInitParam As Integer) _
   As Integer

Declare Function EndDialog Lib "user32" Alias "EndDialog" _
  (hDlg As Integer, _
   nResult As Integer) _
   As Integer

Declare Function GetDlgItem Lib "user32" Alias "GetDlgItem" _
  (ByVal hDlg As Integer, _
   ByVal nIDDlgItem As Integer) _
   As Integer

Declare Function InitCommonControls Lib "comctl32" Alias "InitCommonControls" _
  () As Integer

Declare Function InvalidateRect Lib "user32" Alias "InvalidateRect" _
  (ByVal hwnd As Integer, _
   ByRef lpRect As RECT, _
   ByVal bErase As Integer) _
   As Integer

$Const PS_SOLID       =  0
$Const PS_DASH        =  1
$Const PS_DOT         =  2
$Const PS_DASHDOT     =  3
$Const PS_DASHDOTDOT  =  4
$Const PS_NULL        =  5
$Const PS_INSIDEFRAME =  6
$Const PS_USERSTYLE   =  7
$Const PS_ALTERNATE   =  8
$Const PS_STYLE_MASK  =  0x0000000F

$Const WHITE_BRUSH         = 0
$Const LTGRAY_BRUSH        = 1
$Const GRAY_BRUSH          = 2
$Const DKGRAY_BRUSH        = 3
$Const BLACK_BRUSH         = 4
$Const NULL_BRUSH          = 5
$Const HOLLOW_BRUSH        = NULL_BRUSH
$Const WHITE_PEN           = 6
$Const BLACK_PEN           = 7
$Const NULL_PEN            = 8
$Const OEM_FIXED_FONT      = 10
$Const ANSI_FIXED_FONT     = 11
$Const ANSI_VAR_FONT       = 12
$Const SYSTEM_FONT         = 13
$Const DEVICE_DEFAULT_FONT = 14
$Const DEFAULT_PALETTE     = 15
$Const SYSTEM_FIXED_FONT   = 16

Declare Function CreatePen Lib "gdi32" Alias "CreatePen" _
  (ByVal nPenStyle As Integer, _
   ByVal nWidth As Integer, _
   ByVal crColor As Integer) _
   As Integer

Declare Function SelectObject Lib "gdi32" Alias "SelectObject" _
  (ByVal hdc As Integer, _
   ByVal hObject As Integer) _
   As Integer

Declare Function GetStockObject Lib "gdi32" Alias "GetStockObject" _
  (ByVal nIndex As Integer) As Integer

Declare Function CreateSolidBrush Lib "gdi32" Alias "CreateSolidBrush" _
  (ByVal crColor As Integer) _
   As Integer

Declare Function Rectangle Lib "gdi32" Alias "Rectangle" _
  (ByVal hdc As Integer, _ 
   ByVal X1 As Integer, _ 
   ByVal Y1 As Integer, _ 
   ByVal X2 As Integer, _ 
   ByVal Y2 As Integer) _
   As Integer

Declare Function DeleteObject Lib "gdi32" Alias "DeleteObject" _
  (ByVal hObject As Integer) As Integer

Declare Function DeleteDC Lib "gdi32" Alias "DeleteDC" _ 
  (ByVal hdc As Integer) _
   As Integer

Declare Function DestroyWindow Lib "user32" Alias "DestroyWindow" _
  (ByVal hwnd As Integer) As Integer

Declare Function SetPixel Lib "gdi32" Alias "SetPixel" _
  (ByVal hdc As Integer, _ 
   ByVal x As Integer, _ 
   ByVal y As Integer, _ 
   ByVal crColor As Integer) _
   As Integer

$Const SRCCOPY          = 0x00CC0020 ' dest = source                   
$Const SRCPAINT         = 0x00EE0086 ' dest = source OR dest           
$Const SRCAND           = 0x008800C6 ' dest = source AND dest          
$Const SRCINVERT        = 0x00660046 ' dest = source XOR dest          
$Const SRCERASE         = 0x00440328 ' dest = source AND (NOT dest )   
$Const NOTSRCCOPY       = 0x00330008 ' dest = (NOT source)             
$Const NOTSRCERASE      = 0x001100A6 ' dest = (NOT src) AND (NOT dest) 
$Const MERGECOPY        = 0x00C000CA ' dest = (source AND pattern)     
$Const MERGEPAINT       = 0x00BB0226 ' dest = (NOT source) OR dest     
$Const PATCOPY          = 0x00F00021 ' dest = pattern                  
$Const PATPAINT         = 0x00FB0A09 ' dest = DPSnoo                   
$Const PATINVERT        = 0x005A0049 ' dest = pattern XOR dest         
$Const DSTINVERT        = 0x00550009 ' dest = (NOT dest)               
$Const BLACKNESS        = 0x00000042 ' dest = BLACK                    
$Const WHITENESS        = 0x00FF0062 ' dest = WHITE                    
$Const NOMIRRORBITMAP   = 0x80000000 ' Do not Mirror the bitmap in this call 
$Const CAPTUREBLT       = 0x40000000 ' Include layered windows 

Declare Function BitBlt Lib "gdi32" Alias "BitBlt" _
  (ByVal hDestDC As Integer, _
   ByVal x As Integer, _
   ByVal y As Integer, _
   ByVal nWidth As Integer, _
   ByVal nHeight As Integer, _
   ByVal hSrcDC As Integer, _
   ByVal xSrc As integer, _
   ByVal ySrc As Integer, _
   ByVal dwRop As Integer) _
   As Integer

$Const MF_INSERT          = 0x00000000
$Const MF_CHANGE          = 0x00000080
$Const MF_APPEND          = 0x00000100
$Const MF_DELETE          = 0x00000200
$Const MF_REMOVE          = 0x00001000
$Const MF_BYCOMMAND       = 0x00000000
$Const MF_BYPOSITION      = 0x00000400
$Const MF_SEPARATOR       = 0x00000800
$Const MF_ENABLED         = 0x00000000
$Const MF_GRAYED          = 0x00000001
$Const MF_DISABLED        = 0x00000002
$Const MF_UNCHECKED       = 0x00000000
$Const MF_CHECKED         = 0x00000008
$Const MF_USECHECKBITMAPS = 0x00000200
$Const MF_STRING          = 0x00000000
$Const MF_BITMAP          = 0x00000004
$Const MF_OWNERDRAW       = 0x00000100
$Const MF_POPUP           = 0x00000010
$Const MF_MENUBARBREAK    = 0x00000020
$Const MF_MENUBREAK       = 0x00000040
$Const MF_UNHILITE        = 0x00000000
$Const MF_HILITE          = 0x00000080

Declare Function CreateMenu Lib "user32" Alias "CreateMenu" () As Integer

Declare Function AppendMenu Lib "user32" Alias "AppendMenuA" _
  (ByVal hMenu As Integer, _
   ByVal wFlags As Integer, _
   ByVal wIDNewItem As Integer, _
   ByVal lpNewItem As String) As Integer

Declare Function PostMessage Lib "user32" Alias "PostMessageA" _
  (ByVal hwnd As Integer, _
   ByVal wMsg As Integer, _
   ByVal wParam As Integer, _
   ByVal lParam As Integer) _
   As Integer

$Const SM_CXSCREEN          = 0
$Const SM_CYSCREEN          = 1
$Const SM_CXVSCROLL         = 2
$Const SM_CYHSCROLL         = 3
$Const SM_CYCAPTION         = 4
$Const SM_CXBORDER          = 5
$Const SM_CYBORDER          = 6
$Const SM_CXDLGFRAME        = 7
$Const SM_CYDLGFRAME        = 8
$Const SM_CYVTHUMB          = 9
$Const SM_CXHTHUMB          = 10
$Const SM_CXICON            = 11
$Const SM_CYICON            = 12
$Const SM_CXCURSOR          = 13
$Const SM_CYCURSOR          = 14
$Const SM_CYMENU            = 15
$Const SM_CXFULLSCREEN      = 16
$Const SM_CYFULLSCREEN      = 17
$Const SM_CYKANJIWINDOW     = 18
$Const SM_MOUSEPRESENT      = 19
$Const SM_CYVSCROLL         = 20
$Const SM_CXHSCROLL         = 21
$Const SM_DEBUG             = 22
$Const SM_SWAPBUTTON        = 23
$Const SM_RESERVED1         = 24
$Const SM_RESERVED2         = 25
$Const SM_RESERVED3         = 26
$Const SM_RESERVED4         = 27
$Const SM_CXMIN             = 28
$Const SM_CYMIN             = 29
$Const SM_CXSIZE            = 30
$Const SM_CYSIZE            = 31
$Const SM_CXFRAME           = 32
$Const SM_CYFRAME           = 33
$Const SM_CXMINTRACK        = 34
$Const SM_CYMINTRACK        = 35
$Const SM_CXDOUBLECLK       = 36
$Const SM_CYDOUBLECLK       = 37
$Const SM_CXICONSPACING     = 38
$Const SM_CYICONSPACING     = 39
$Const SM_MENUDROPALIGNMENT = 40
$Const SM_PENWINDOWS        = 41
$Const SM_DBCSENABLED       = 42
$Const SM_CMOUSEBUTTONS     = 43

Declare Function GetSystemMetrics Lib "user32" Alias "GetSystemMetrics" _
  (ByVal nIndex As Integer) _
   As Integer

$Const SWP_NOSIZE         = 0x0001
$Const SWP_NOMOVE         = 0x0002
$Const SWP_NOZORDER       = 0x0004
$Const SWP_NOREDRAW       = 0x0008
$Const SWP_NOACTIVATE     = 0x0010
$Const SWP_FRAMECHANGED   = 0x0020 'The frame changed: send WM_NCCALCSIZE
$Const SWP_SHOWWINDOW     = 0x0040
$Const SWP_HIDEWINDOW     = 0x0080
$Const SWP_NOCOPYBITS     = 0x0100
$Const SWP_NOOWNERZORDER  = 0x0200 'Don't do owner Z ordering
$Const SWP_NOSENDCHANGING = 0x0400 'Don't send WM_WINDOWPOSCHANGING
$Const SWP_DRAWFRAME      = 0x0020 'SWP_FRAMECHANGED
$Const SWP_NOREPOSITION   = 0x0200 'SWP_NOOWNERZORDER

$Const HWND_TOP       = 0
$Const HWND_BOTTOM    = 1
$Const HWND_TOPMOST   = -1
$Const HWND_NOTOPMOST = -2

Declare Function SetWindowPos Lib "user32" Alias "SetWindowPos" _
  (ByVal hwnd As Integer, _
   ByVal hWndInsertAfter As Integer, _
   ByVal x As Integer, _
   ByVal y As Integer, _
   ByVal cx As Integer, _
   ByVal cy As Integer, _
   ByVal wFlags As Integer) _
   As Integer

Declare Function EnableWindow Lib "user32" Alias "EnableWindow" _
  (ByVal hwnd As Integer, _
   ByVal fEnable As Integer) _
   As Integer

'*************
'* winmm.lib *
'*************

$Const SND_SYNC        = 0x0000      ' play synchronously (default)
$Const SND_ASYNC       = 0x0001      ' play asynchronously
$Const SND_NODEFAULT   = 0x0002      ' silence (!default) if sound not found
$Const SND_MEMORY      = 0x0004      ' pszSound points to a memory file
$Const SND_LOOP        = 0x0008      ' loop the sound until next sndPlaySound
$Const SND_NOSTOP      = 0x0010      ' don't stop any currently playing sound

$Const SND_NOWAIT      = 0x00002000  ' don't wait if the driver is busy
$Const SND_ALIAS       = 0x00010000  ' name is a registry alias
$Const SND_ALIAS_ID    = 0x00110000  ' alias is a predefined ID
$Const SND_FILENAME    = 0x00020000  ' name is file name
$Const SND_RESOURCE    = 0x00040004  ' name is resource name or atom

Declare Function PlaySound Lib "winmm" Alias "PlaySoundA" _
  (ByVal lpszName As String, _
   ByVal hModule As Integer, _ 
   ByVal dwFlags As Integer) _
   As Integer

Type MMTIME
  wType As Integer
  u As Integer
End Type

$Const TIME_ONESHOT  = 0
$Const TIME_PERIODIC = 1

Declare Function timeSetEvent Lib "winmm.dll" Alias "timeSetEvent" _ 
  (ByVal uDelay As Integer, _
   ByVal uResolution As Integer, _
   ByVal lpFunction As Integer, _
   ByVal dwUser As Integer, _
   ByVal uFlags As Integer) _
   As Integer

Declare Function timeKillEvent Lib "winmm.dll" Alias "timeKillEvent" _ 
  (ByVal uID As Integer) As Integer


'*************

$Const OUT_DEFAULT_PRECIS        =  0
$Const OUT_STRING_PRECIS         =  1
$Const OUT_CHARACTER_PRECIS      =  2
$Const OUT_STROKE_PRECIS         =  3
$Const OUT_TT_PRECIS             =  4
$Const OUT_DEVICE_PRECIS         =  5
$Const OUT_RASTER_PRECIS         =  6
$Const OUT_TT_ONLY_PRECIS        =  7
$Const OUT_OUTLINE_PRECIS        =  8
$Const OUT_SCREEN_OUTLINE_PRECIS =  9
$Const OUT_PS_ONLY_PRECIS        =  10

$Const CLIP_DEFAULT_PRECIS    = 0
$Const CLIP_CHARACTER_PRECIS  = 1
$Const CLIP_STROKE_PRECIS     = 2
$Const CLIP_MASK              = 0xf

$Const DEFAULT_QUALITY = 0
$Const DRAFT_QUALITY = 1
$Const PROOF_QUALITY = 2
$Const DEFAULT_PITCH = 0
$Const FIXED_PITCH = 1
$Const VARIABLE_PITCH = 2
$Const ANSI_CHARSET = 0
$Const DEFAULT_CHARSET = 1
$Const SYMBOL_CHARSET = 2
$Const SHIFTJIS_CHARSET = 128
$Const HANGEUL_CHARSET = 129
$Const CHINESEBIG5_CHARSET = 136
$Const OEM_CHARSET = 255

$Const FF_DONTCARE = 0
$Const FF_ROMAN = 16
$Const FF_SWISS = 32
$Const FF_MODERN = 48
$Const FF_SCRIPT = 64
$Const FF_DECORATIVE = 80

$Const FW_DONTCARE = 0
$Const FW_THIN = 100
$Const FW_EXTRALIGHT = 200
$Const FW_LIGHT = 300
$Const FW_NORMAL = 400
$Const FW_MEDIUM = 500
$Const FW_SEMIBOLD = 600
$Const FW_BOLD = 700
$Const FW_EXTRABOLD = 800
$Const FW_HEAVY = 900

$Const FW_ULTRALIGHT = 200 'FW_EXTRALIGHT
$Const FW_REGULAR = 400 'FW_NORMAL
$Const FW_DEMIBOLD = 600 'FW_SEMIBOLD
$Const FW_ULTRABOLD = 800 'FW_EXTRABOLD
$Const FW_BLACK = 900 'FW_HEAVY

Declare Function CreateFont Lib "gdi32" Alias "CreateFontA" _
  (nHeight As Integer, _
   nWidth As Integer, _
   nEscapement As Integer, _
   nOrientation As Integer, _
   fnWeight As Integer, _
   fdwItalic As Integer, _
   fdwUnderline As Integer, _
   fdwStrikeOut As Integer, _
   fdwCharSet As Integer, _
   fdwOutputPrecision As Integer, _
   fdwClipPrecision As Integer, _
   fdwQuality As Integer, _
   fdwPitchAndFamily As Integer, _
   lpszFace As String) _
   As Integer 

'*************

$Include "WinHelper.inc"

Sub Return(i As Integer) 
   $Asm 
      mov   eax,[ebp+8] 
   $End Asm    
End Sub    

$End If


