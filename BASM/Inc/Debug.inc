
' 
' --- Debug.inc --- 
' 
' KoolB debug functions. 
' 
$Include "Windows.inc" 
$Include "String.inc" 

' The ByRef here is necessary. 
Declare Function LTOA Lib "msvcrt.dll" Alias "_ltoa" _ 
   Call C _ 
   (number As Integer, _ 
   ByRef lpString As String, _ 
   radix As Integer) _ 
   As Integer 

Sub ShowInt _ 
   (caption As String, _ 
   i As Integer, _ 
   radix As Integer) 
    
   Dim iString As String 
   iString = Space$(40) 
   LTOA(i,iString,radix) 
    
   MessageBox(0,iString,caption,0)    
'   MessageBox(0,AddressOf(iString),AddressOf(caption),0)    
End Sub 

Declare Function ErrorMsgBox Lib "user32" Alias "MessageBoxA" _ 
   (hWnd As Integer, _ 
   lpText As Integer, _ 
   lpcaption As Integer, _ 
   wType As Integer) _ 
   As Integer 

Sub ShowLastError() 

   Dim lpMsgBuf As Integer 
   Dim dwFlags As Integer 
   Dim dwMessageId As Integer    
   Dim idString As String 
   Dim caption as String 

   dwMessageId = GetLastError() 
   idString = Space$(40) 
   LTOA(dwMessageId,idString,10) 
   caption = "System error code "+idString 
    
   dwFlags = FORMAT_MESSAGE_ALLOCATE_BUFFER + _ 
      FORMAT_MESSAGE_FROM_SYSTEM + _ 
      FORMAT_MESSAGE_IGNORE_INSERTS    

   FormatMessage _ 
      (dwFlags, _ 
      0, _ 
      dwMessageId, _ 
      LANG_NEUTRAL, _ 
      AddressOf(lpMsgBuf), _ 
      0, _ 
      0)    
       
'   MessageBox(0,lpMsgBuf,AddressOf(caption),0) 
   ErrorMsgBox(0,lpMsgBuf,AddressOf(caption),0) 

   LocalFree(lpMsgBuf) 
End Sub 
