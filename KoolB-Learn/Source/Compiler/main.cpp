/*******************************************************************
*                       Simply KoolB Compiler                      *
*                                -                                 *
*            Your example compiler for Windows and Linux!          *
*                                -                                 *
*                    From: Compiler Tutorial,                      *
*       Your How-To Guide on Creating Real-World Compilers         *
********************************************************************
* Author:      Brian C. Becker                                     *
* Website:     http://www.BrianCBecker.com                         *
* Date:        04-18-2004                                          *
********************************************************************
* Tip for Eternity:                                                *
*      So in everything, do unto others what you would have them   *
*      do unto you. Matthew 7:12.                                  *
********************************************************************
* Simply KoolB is an extremely simple BASIC-like compiler that     *
* translates your code to assembly language and then to a program. *
* Because it is so small (less than 1000 lines), it's primary use  *
* is to show you the absolute basics of building a complete        *
* compiler for both Windows and Linux                              *
*                                                                  *
* To use it just pass it the name of the BASIC file to compile     *
* For instance: KoolB test.bas"                                    *
*                                                                  *
* KoolB can just compile your BASIC file (translate it to assembly)*
* or it can go ahead and build your file too by assembling and     *
* linking. By default, KoolB will build your file. To change this, *
* simply pass "-compile" after the name of your file.              *
* For instance: KoolB test.bas -compile                            *
*                                                                  *
* This will compile Test.bas to Test.asm                           *
*                                                                  *
* Simply KoolB accepts 7 commands (case doesn't matter):           *
* - Blank line                                                     *
*      I know, techinically not a command, but it is very helpful  *
* - REM                                                            *
*      Comments out the rest of the line                           *
* - CLS                                                            *
*      Clears the screen                                           *
* - PRINT "STRING"                                                 *
*      Prints a single string on the screen                        *
* - RUN "COMMAND"                                                  *
*      Runs a system command, like DIR or ls                       *
* - SLEEP 1                                                        *
*      Pauses the program for the specified number of seconds      *
* - WAIT                                                           *
*      Waits for the user to press ENTER                           *
*                                                                  *
* See Simple.KoolB & Complex.KoolB in the Examples folder          *
*******************************************************************/

//	Include all the libraries other people have written ;-)
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string>
using namespace std;

//	Switches between Windows and Linux versions by adjusting the
//	assembly language. Comment out the one you don't need		
#define Windows	1
//#define Linux	2

//	The list of things that can go wrong. Error codes start at 
//	-1 and progress upwards.
enum Error{ERROR_FATAL = -1, ERROR_COMMANDLINE, ERROR_FILENAME, 
		   ERROR_BADCOMMAND, ERROR_STARTSTRING, ERROR_ENDSTRING, 
		   ERROR_NOEOL, ERROR_EOLFOUND, ERROR_NOASSEMBLY};

//	Global variables to use in Simply KoolB
bool    EndPause = false; // Keep console open if necessary
FILE *  File;             // File to read/write source/assembly
string  FileName;         // Filename of the source code
char *  Source;           // Array to hold entire source code
int     Size;             // Size of the source code (bytes)
int	    Position;         // Current position in source code
int	    Line = 1;         // Current line in the BASIC source
string  Command;          // Current command (PRINT, CLS, etc)
string  AsmCode;          // Assembly language code to output
string  AsmData;          // Assembly language data (variables)
int	    DataCounter = 1;  // Counter to store unique data
bool    Indent = false;   // Should we indent the assembly code?
bool    Build = true;     // Generate program or just compile?

//	General functions (routines) 
void    RequestFile();    // Requests the file to compile
void    ReadSource();     // Load the BASIC source code
void    StartAssembly();  // Generate skeleton assembly language
void    Compile();        // Translate the BASIC to assembly
void    EndAssembly();    // Finish & write the assembly to file
void    BuildProgram();   // Use NASM & GoLink to finish
void    Error(int);       // Report an error to the user

//	Functions to read the BASIC source code
string  GetCommand();     // Try to get a command like PRINT
string  GetString();      // Try to get a string "string"
string  GetNumber();      // Try to get an integer number 
void	SkipWhitespace(); // Ignore whitespace
void	GetEOL();         // Try to find the end of the line

//	Functions to generate assembly language for the BASIC source
void    WriteLine(string);// Add assembly line to program
void    WriteData(string);// Add assembly data to program
void    Cls();            // Generate code to clear the screen
void    Rem();            // Eat the rest of the line (comment)
void    Print(string);    // Print a string on the screen
void    Run(string);      // Run a system command like DIR
void    Sleep(string);    // Sleep for X seconds
void    Wait();           // Wait until user presses ENTER

//	Miscellaneous functions
string 	ToStr(int Number);// Convert an integer to string
string  StripExt(string); // Remove extension from filename

/*******************************************************************
main:
	Greets the user and checks to see if the command line
	is correct is in the format KoolB <FileName>. So if you 
	want to compile "Test.bas", type: KoolB Test.bas
	Then the file is compiled. If you just want the assembly 
	language file, pass the switch "-compile" at the end of 
	the filename like such: KoolB Test.bas -compile.
*******************************************************************/
int main(int argc, char *argv[])
{
	// Make sure the command line parameters are formatted:
	// KoolB <filename> [switch]	(two or three parameters)
	// If not, manually request the filename from the user
	if (argc < 2 || argc > 3)
	{
		RequestFile();
	}
	else
 	{
		// Store the second parameter as the filename
		FileName = argv[1];

		// Check for a third (optional) switch
		if (argc == 3)
		{
			string Switch = argv[2];

			// The only valid switches are -compile and -build
			// -build is on by default. Other switches give an error
			if (Switch == "-compile")
				Build = false;
			else if (Switch == "-build")
				Build = true;
			else
				printf("Unknown switch: %s\n\n", argv[2]);
		}
	}

	// If just compiling, KoolB is probably being run from the
	// IDE, so don't print out the normal welcoming banner
	if (Build && !EndPause)
	{
		// Print out the welcoming banner
		printf("Welcome to Simply KoolB by Brian C. Becker\n");
		printf("Written for the Compiler Tutorial ");
		printf("(www.BrianCBecker.com).\n\n");
	}
	
	// Go ahead and start the compile process
	ReadSource();
	StartAssembly();
	Compile();
	EndAssembly();

	// Only assemble and link the program if necessary
	if (Build)
	{
		BuildProgram();

		// If we have reached this far, everything should be done
		printf("Simply KoolB has successfully compiled your file!\n");
	}
	
	// If this program was launched without a filename parameter
	// (which would happen when a Windows user double-clicked
	// KoolB from Windows Explorer), pause so that the output
	// can be displayed before the console window closes. This
	// only applies to Windows. 
	#ifdef Windows
		if (EndPause)
			system("pause");
	#endif

	return 0;
}

/*******************************************************************
RequestFile:
	If KoolB is run without any parameters, ask for the name of the
	file to compile
*******************************************************************/
void RequestFile()
{
	// Print out the welcoming banner
	printf("Welcome to Simply KoolB by Brian C. Becker\n");
	printf("Written for the Compiler Tutorial ");
	printf("(www.BrianCBecker.com).\n\n");

	// Temporary string to hold input from console
	char Temp[1024];
	
	// Get the filename from the user and store in FileName
	printf("Please enter the file to compile: ");
	scanf("%s", &Temp);
	FileName = Temp;

	printf("\n\n");
	
	// Assume that the user doulbe-clicked this from the
	// Windows Explorer, so keep console window open at end
	EndPause = true;
}

/*******************************************************************
ReadSource:
	Attempt to open the BASIC source file and read it into memory.
	Because the last line will not have a newline (\n), just add
	a new line to the end of the file. That way each line will
	always end with the newline character (\n).
*******************************************************************/
void ReadSource()
{
	// Try to open the BASIC file
	string BasicFile = FileName;
	File = fopen(BasicFile.c_str(), "rb");

	// Make sure the file actually exists!
	if (!File)
		Error(ERROR_FILENAME);
	
	// Find the size of the file by subtracting end from beginning
	fseek(File, 0, SEEK_END);
	Size = ftell(File);
	rewind(File);
	
	// The size is file contents plus newline and null terminator
	Size = Size - ftell(File) + 1;
	
	// Allocate new memory to hold the BASIC source & clear it
	Source = new char[Size];
	
	// Fill the memory with newlines so that the end of the file
	// has a newline instead of EOF.
	memset(Source, '\n', Size);
	
	// Read in the source code and close the file
	fread(Source, Size - 1, 1, File);
	fclose(File);

	// Now, we don't need the extension anymore, so remove it 
	// so we can add other extensions like .asm & .obj & .exe
	FileName = StripExt(FileName);
}

/*******************************************************************
StartAssembly:
	Generates all the assembly language needed to initialize the
	program. This includes external system functions used,
	data used internally, and the code to start the program.
*******************************************************************/
void StartAssembly()
{
	// Tell the assembler these functions are system functions
	WriteLine("EXTERN	system");
	WriteLine("EXTERN	puts");
	WriteLine("EXTERN	getchar");
	WriteLine("EXTERN 	exit");
	
	// Windows uses _sleep, but Linux uses sleep
	#ifdef Windows
		WriteLine("EXTERN	_sleep");
	#endif
	#ifdef Linux
		WriteLine("EXTERN	sleep");
	#endif
	
	// Separate with an empty line
	WriteLine("");
	
	// The .data section holds all the data for the program
	WriteData("section .data");
	WriteData("");
	
	// To clear the screen, we use cls in Windows, clear in Linux
	// These are 1-byte strings (db) ending with a null terminator
	#ifdef Windows
		WriteData("ClearScreen 		db \"cls\",0");
	#endif
	#ifdef Linux
		WriteData("ClearScreen 		db \"clear\",0");
	#endif
	
	// The .text section holds the actual assembly language code
	// The START label is where the program begins
	WriteLine("section .text");
	WriteLine("");
	
	#ifdef Windows
		WriteLine("global	START");
		WriteLine("START:	");
	#endif
	#ifdef Linux
		WriteLine("global	_start");
		WriteLine("_start:	");
	#endif

	// Indent all the rest of the code
	Indent = true;
	
	// Do some routine initialize to prevent bad things ;-)
	WriteLine("PUSHA	");
	WriteLine("PUSH	EBP");
	WriteLine("MOV	EBP, ESP");
}

/*******************************************************************
Compile:
	Compile the BASIC source code line by line until the end of 
	the file is reached (i.e. Position reaches the Size of the
	file). 
	The first word of each line is taken and compared to the list
	of commands KoolB accepts (REM, CLS, PRINT, RUN, SLEEP, WAIT).
	If a match is made, simply transfer control to let another 
	function deal with it.
	
	Each line should begin with a command. Sometimes a command
	stands alone, such as WAIT or CLS.
	In other times, the command needs some additional information,
	such as SLEEP or PRINT. So additional information is retrieved
	from the line.
*******************************************************************/
void Compile()
{
	// Loop until we have compiled the entire BASIC source code
	while (Position < Size)
	{
		// Prime the pump by removing any beginning whitespace
		while (Position < Size && isspace(Source[Position]))
			Position++;
			
		// Get the command to compile
		Command = GetCommand();
		
		// For each command, call a function to handle it
		if (Command == "")
		{
			// Skip blank lines, so just keep going
			++Position;
			continue;
		}
		else if (Command == "REM")
		{
			// Handles comments
			Rem();
		}
		else if (Command == "CLS")
		{
			// Clear the screen
			Cls();
		}
		else if (Command == "PRINT")
		{
			// Get the string after PRINT and display it
			Print(GetString());
		}
		else if (Command == "RUN")
		{
			// Get the string after RUN and execute it
			Run(GetString());
		}
		else if (Command == "SLEEP")
		{
			// Get the number after SLEEP and sleep X seconds
			Sleep(GetNumber());
		}
		else if (Command == "WAIT")
		{
			// Wait until the user presses ENTER
			Wait();
		}
		else
		{
			// Oops, this is an invalid command!
			Error(ERROR_BADCOMMAND);
		}
		
		// Get the end of the line
		GetEOL();
	}
	
	// Once the compile is done, free memory occupied by source
	delete[] Source;
}

/*******************************************************************
EndAssembly:
	Generates some last minute assembly language to close down the
	program. One of these is a set of commands to print:
	"Press ENTER to exit..." at the end of each program and then
	wait for the user to press enter. This helps avoid the 
	problem of a flashing console if the user double-clicks it 
	from Windows Explorer. 
	Once all the assembly language is generated, we write it to 
	a file so it can be assembled.
*******************************************************************/
void EndAssembly()
{
	// Open a file to write the assembly language to
	string AssemblyFile = FileName + ".asm";
	File = fopen(AssemblyFile.c_str(), "w");
	
	// Make sure the file is open for writing
	if (!File)
		Error(ERROR_NOASSEMBLY);
	
	// To finish the each program, let's add some custom code
	// This is like Visual C++'s debug programs that wait for
	// the user to press a key to exit the program
	Print("Press ENETR to exit...");
	Wait();
	
	// Before we write the file, make sure to end the program
	WriteLine("MOV	ESP, EBP");
	WriteLine("POP	EBP");
	WriteLine("POPA	");
	WriteLine("PUSH	0");
	WriteLine("CALL	exit");
	WriteLine("");
	WriteLine("");
	
	// Write assembly language code & data and close the file
	fwrite(AsmCode.c_str(), AsmCode.length(), 1, File);
	fwrite(AsmData.c_str(), AsmData.length(), 1, File);
	fclose(File);
}

/*******************************************************************
BuildProgram:
	Runs the assembler and linker to do the final step of 
	producing a program from the assembly language. One thing to
	note is that in case the FileName contains spaces, such as
	"This is a test.bas", we put quotes around the filename. 
*******************************************************************/
void BuildProgram()
{
	#ifdef Windows
		// Assemble the assembly language to an object file
		string Command = "nasm -fwin32 \"" +FileName+ ".asm\"";
		system(Command.c_str());
		
		// Link the object file and MSVCRT.DLL to a program
		// MSSVCRT.DLL is the Microsoft Standard C Library 
		Command = "golink /CONSOLE /ni \"" +FileName + ".obj\"";
		Command = Command + " msvcrt.dll";
		system(Command.c_str());
	#endif
	#ifdef Linux
		// Assemble the assembly language to an object file
		string Command = "nasm -felf \"" + FileName + ".asm\"";
		system(Command.c_str());
		
		// Link the object file and libc to a program
		Command = "ld -s -dynamic-linker /lib/ld-linux.* -o ";
		Command+= "\"" + FileName + "\" \"" + FileName + ".o\"";
		Command+= " /lib/libc.*";
		system(Command.c_str());
	#endif
	
	return;
}

/*******************************************************************
Error:
	Recieves an error code and prints out the corresponding error
	message. If it is syntax related (like you misspelled PRINT),
	a line number is added as well. Once the error message is
	printed, Simply KoolB exits.
*******************************************************************/
void Error(int Code)
{
	// Create an generic error message with location of error
	string Message = "Error on line " + ToStr(Line) + ": ";
	
	// Now give some specifics for each error
	// Note: some error messages don't need the generic message
	switch (Code)
	{
		case ERROR_COMMANDLINE:
			printf("Error! Bad command line.\n");
			printf("Usage: KoolB <filename> [-compile -build]");
			break;
		case ERROR_FILENAME:
			Message = "Error! File \"" + FileName;
			Message = Message + "\" does not exist.";
			printf(Message.c_str());
			break;
		case ERROR_BADCOMMAND:
			printf(Message.c_str());
			Message = "Unknown command: " + Command;
			printf(Message.c_str());
			break;
		case ERROR_STARTSTRING:
			printf(Message.c_str());
			printf("Expected start of a string.");
			break;
		case ERROR_ENDSTRING:
			printf(Message.c_str());
			printf("String does not end.");
			break;
		case ERROR_NOEOL:
			printf(Message.c_str());
			printf("End of Line expected.");
			break;
		case ERROR_EOLFOUND:
			printf(Message.c_str());
			printf("Command not finished before EOL.");
			break;
		case ERROR_NOASSEMBLY:
			printf("Error! Could not open assembly file.");
			break;
		default:
			printf("Error! Something really bad happened!");
	}
	
	printf("\n");
	
	// No point in trying to go further, if EndPause, pause before
	// exiting. However, this only applies to Windows (see main)
	#ifdef Windows
		if (EndPause)
			system("pause");
	#endif
	
	exit(1);
}

/*******************************************************************
GetCommand:
	Gets the first word from a line. A word is a string of
	alphabetic characters. Before the word is retreived, ignore
	any whitespace, including new lines (remember that you might 
	have several blank lines in a row). To ease comparison, the 
	word is converted to uppercase. 
*******************************************************************/
string GetCommand()
{
	// Initialize Command string and then ignore any whitespace
	string Command = "";
	SkipWhitespace();
	
	// Get all alphabetic characters and convert them to uppercase
	while (Position < Size && isalpha(Source[Position]))
	{
		Command += toupper(Source[Position]);
		++Position;
	}
	
	// Return the command we got
	return Command;
}

/*******************************************************************
GetString:
	Gets a string in a line. A string is any character between
	two quotation marks. Of course, we want to preserve the case!
	And because a string does not begin a line, when we ignore any
	leading whitespace, we don't want to see any newlines:
	
	For example:
	PRINT
	"HELLO!"
	
	No, a command must all be on the same line. So when we 
	SkipWhitespace newlines will result in an error.
*******************************************************************/
string GetString()
{
	string String = "";
	
	// Ignore all whitespace, except newlines
	SkipWhitespace();

	// A string must begin with a quotation mark
	if (Source[Position] != '"')
		Error(ERROR_STARTSTRING);

	// Move to the first character of the string
	++Position;

	// Grab all characters until we see the ending quotation mark
	while (Source[Position] != '"')
	{
		// Make sure the string is all on the same line
		if (Position > Size || Source[Position] == '\n')
			Error(ERROR_ENDSTRING);

		String += Source[Position];
		++Position;
	}
	
	// Discard the ending quotation mark, we don't need it
	++Position;

	return String;
}

/*******************************************************************
GetNumber:
	Gets a number from a line. A number in this case is a simple
	integer. First we skip any whitespace, except newlines (see
	GetString for a more in depth explination).
*******************************************************************/
string GetNumber()
{
	string Number = "";

	// Skip all whitespace except newlines
	SkipWhitespace();

	// Grab all digits that make up the number
	while (Position < Size && isdigit(Source[Position]))
	{
		Number += Source[Position];
		++Position;
	}
	
	return Number;
}

/*******************************************************************
GetEOL:
	Each line MUST end with a newline. So after a command is 
	finished, GetEOL will discard all trailing whitespace and 
	make sure that a newlin was enocunterd. 
	This also takes care of multiple newlines at the end of a 
	command. For instance:
	
	PRINT "HELLO!"
	
	PRINT "BYE!"
	
	GetEOL will actually eat both the newline after PRINT "HELLO!"
	and the newline after the blank line.
*******************************************************************/
void GetEOL()
{
	// No, we haven't found the newline yet
	bool FoundEOL = false;

	// Skip past all the trailing whitespace at end of the line
	while (Position < Size && isspace(Source[Position]))
	{
		// If we find newline, indicate it & update line counter
		if (Source[Position] == '\n')
		{
			FoundEOL = true;
			Line++;
		}
		
		++Position;
	}
	
	// If we didn't find at least one newline, report an error
	if (FoundEOL == false)
		Error(ERROR_NOEOL);
}

/*******************************************************************
SkipWhitespace:
	Skips past whitespace, except newlines! This is for use within
	a line. To skip whitespace at the end of a line, we use GetEOL
	For instance:
	
	PRINT "HELLO"
	     ^--Skips whitespace
*******************************************************************/
void SkipWhitespace()
{
	// Ignore all whitespace
	while (isspace(Source[Position]))
	{
		// Make sure newlines doen't occur within a line
		if ((Source[Position] == '\n' || Position > Size))
			Error(ERROR_EOLFOUND);
			
		++Position;
	}
}

/*******************************************************************
WriteLine:
	Writes a single line to the assembly language. This is used
	for code, not data. Indention helps make it easier to read.
*******************************************************************/
void WriteLine(string AsmLine)
{
	// Add a line of code to our assembly language code
	if (Indent == true)
		AsmCode += "\t" + AsmLine + "\n";
	else
		AsmCode += AsmLine + "\n";
	return;
}

/*******************************************************************
WriteData:
	Adds a line of data to the assembly language. Common data 
	include strings and numbers
*******************************************************************/
void WriteData(string AsmLine)
{
	AsmData += AsmLine + "\n";
}

/*******************************************************************
Cls:
	Generates the code to clear the screen. Both Windows & Linux
	have system commands to do this. For Windows it is CLS and for
	Linux it is clear. In StartAssembly, this string was stored 
	in ClearString. So we push ClearScreen to the stack and then
	execute it as a system command by calling system. The system
	will then clear the screen. However, ClearString is still on 
	the stack, so we pop it off into any register (doesn't matter)
*******************************************************************/
void Cls()
{
	WriteLine("PUSH	ClearScreen");
	WriteLine("CALL	system");
	WriteLine("POP	EAX");
}

/*******************************************************************
Print:
	Prints a string to the screen. Each string must be stored
	in the data section of the assembly language with a unique
	name, so we generate unique names with String_1, String_2, etc
	Then we push the string to the stack (using the name from the
	data section) and call puts, which prints the string. Finally,
	we pop the string off the stack.
*******************************************************************/
void Print(string Line)
{
	// Create the string name and increment the counter
	string Name = "String_" + ToStr(DataCounter);
	DataCounter++;
	
	// Put the string in the data section of the assembly language
	WriteData(Name + " 		db \"" + Line + "\",0");
	
	// Push the string, call puts, and then pop the string
	WriteLine("PUSH	" + Name);
	WriteLine("CALL	puts");
	WriteLine("POP	EAX");
}

/*******************************************************************
Run:
	Runs a system command. Like PRINT, we need to create a unique
	name to store the command string in the data section of the
	assembly languge. Then we let the system run the command
*******************************************************************/
void Run(string Command)
{
	// Create the string name and increment the counter
	string Name = "String_" + ToStr(DataCounter);
	DataCounter++;
	
	// Put the string in the data section of the assembly language
	WriteData(Name + " 		db \"" + Command + "\",0");
	
	// Push the string, call puts, and then pop the string
	WriteLine("PUSH	" + Name);
	WriteLine("CALL	system");
	WriteLine("POP	EAX");
}

/*******************************************************************
Sleep:
	Pause the program by a certain number of seconds. In this
	case, we don't have to store the number of seconds in
	the data section of the assembly language. We can directly 
	push the number. Then, depending on whether we are compiling 
	for Windows or Linux, we call the right function and pop the
	number off the stack.
*******************************************************************/
void Sleep(string Seconds)
{
	// Push the number of seconds to sleep
	#ifdef Windows
		// Windows expects milliseconds, not seconds, so convert
		// by loading the seconds and multiplying by 1000
		WriteLine("MOV	EAX," + Seconds);
		WriteLine("MOV	EDX,1000");
		WriteLine("MUL	EDX");
		WriteLine("PUSH	EAX");
	#endif
	#ifdef Linux
		WriteLine("PUSH	" + Seconds);
	#endif
	
	// Call either _sleep or sleep
	#ifdef Windows
		WriteLine("CALL	_sleep");
	#endif
	#ifdef Linux
		WriteLine("CALL	sleep");
	#endif
	
	// Finally, pop off the number of seconds
	WriteLine("POP	EAX");
}

/*******************************************************************
Wait:
	Generates the assembly language code to wait for the user to
	press ENTER. It is very simple, just a single call to getchar.
*******************************************************************/
void Wait()
{
	WriteLine("CALL	getchar");
}

/*******************************************************************
Rem:
	REM comments out the rest of the line. This means that the 
	compiler just ignores the rest of the line. So we skip just
	skip over characters until we come to the end of the line
*******************************************************************/
void Rem()
{
	while (Position < Size && Source[Position] != '\n')
		++Position;
}

/*******************************************************************
ToStr:
	Converts an integer to a string
*******************************************************************/
string ToStr(int Number){
	string Result;
	char Temp[128];
	
	sprintf(Temp, "%i", Number);
	
	Result = Temp;
	return Result;
}

/*******************************************************************
ToStr:
	Returns a filename without the extension. For instance, 
	if "test.bas" becomes just "test"
*******************************************************************/
string StripExt(string FileName)
{
	unsigned int Dot = FileName.rfind(".");

	if (Dot != string::npos)
		return FileName.substr(0, Dot);

	return FileName;
}

