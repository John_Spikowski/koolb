/*******************************************************************
*                    Simply KoolB Compiler IDE                     *
*                                -                                 *
*          Your example compiler for Windows and Linux!			   *
*                                -                                 *
*                    From: Compiler Tutorial,                      *
*       Your How-To Guide on Creating Real-World Compilers         *
********************************************************************
* Author:      Brian C. Becker                                     *
* Website:     http://www.BrianCBecker.com                         *
* Date:        04-18-2004                                          *
********************************************************************
* Tip for Eternity:                                                *
*      So in everything, do unto others what you would have them   *
*      do unto you. Matthew 7:12.                                  *
********************************************************************
* Simply KoolB IDE is an extremely simple NotePad-like IDE that    *
* allows a user to easily write and build Simply KoolB programs    *
* without having to work with the command line. Because it is so   *
* small (less than 1,000 lines of code), it's primary use is to    *
* show you the absolute basics of building a complete IDE for both *
* Windows and Linux.                                               *
*                                                                  *
* It uses C++ wxWidgets framework (www.wxWidgets.org) and the      *
* Scintilla code editing control (www.Scintilla.org) to show how   *
* easy it is to build a Windows and Linux IDE.                     *
*                                                                  *
* The Simply KoolB IDE does not compile on its own. Instead,       *
* it will execute the Simply KoolB compiler in the background      *
* and report the results. It uses 'smart' compilation to avoid     *
* recompiling a file if nothing has changed.                       *
*                                                                  *
* See Simple.KoolB & Complex.KoolB in the Examples folder          *
*******************************************************************/

// Which do we use? Windows or Linux? You pick ;-)
#define Windows 1
//#define	Linux 2

// For compilers that support precompilation, includes "wx.h".
#include <wx/wxprec.h>

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
// Include your minimal set of headers here, or wx.h
#include <wx/wx.h>
#endif

// Include some specialized wxWindows components
#include <wx/stc/stc.h>
#include <wx/splitter.h>
#include <wx/sizer.h>
#include <wx/process.h>
#include <wx/filename.h>
#include <wx/image.h>
#include <wx/splash.h>
#include <wx/mimetype.h>

// List all the IDs for menus, buttons, etc
enum
{
	ID_FileNew = 1, ID_FileOpen, ID_FileSave, ID_FileSaveAs,
	ID_FileExit,
	
	ID_EditUndo, ID_EditRedo, ID_EditCut, ID_EditCopy, 
	ID_EditPaste, ID_EditDelete, ID_EditSelectAll,
	ID_EditGotoLine,
	
	ID_CompileBuildAndRun, ID_CompileCompile, ID_CompileBuild,
	ID_CompileRun, ID_CompileDone,
	
	ID_HelpHelp, ID_HelpAbout
};

// wxWindows allows us to inherit standard wxWindows classes to
// extend classes.
// In this case, we are creating a KoolB application from the
// standard wxWindows application. This allows use to override
// the OnInit routine so we can intialize windows & whatnot.
class kbApp : public wxApp
{
	virtual bool OnInit();
};

// Finally, we inherit a main window for the IDE from a wxFrame 
// window. This main window handles all our menus, syntax
// control editing, and all other components. It responds to 
// events too, such as resizing the window
class kbMainWindow : public wxFrame
{
public:
	// Constructor & destructor for the window
	kbMainWindow(const wxString &Title, const wxPoint &Pos,
			 const wxSize &Size);
	void MakeMainMenu();
	void MakeScintilla(wxFont font);

	// Functions to handle File menu
	void OnFileNew(wxCommandEvent &Event);
	void OnFileOpen(wxCommandEvent &Event);
	void OnFileSave(wxCommandEvent &Event);
	void OnFileSaveAs(wxCommandEvent &Event);
	void OnFileExit(wxCommandEvent &Event);

	// Events to handle Edit menu
	void OnEditUndo(wxCommandEvent &Event);
	void OnEditRedo(wxCommandEvent &Event);
	void OnEditCut(wxCommandEvent &Event);
	void OnEditCopy(wxCommandEvent &Event);
	void OnEditPaste(wxCommandEvent &Event);
	void OnEditDelete(wxCommandEvent &Event);
	void OnEditSelectAll(wxCommandEvent &Event);
	void OnEditGotoLine(wxCommandEvent &Event);

	// Events to handle Compile menu
	void OnCompileBuildAndRun(wxCommandEvent &Event);
	void OnCompileBuild(wxCommandEvent &Event);
	void OnCompileCompile(wxCommandEvent &Event);
	void OnCompileRun(wxCommandEvent &Event);

	// Events to handle Help menu
	void OnHelpHelp(wxCommandEvent &Event);
	void OnHelpAbout(wxCommandEvent &Event);

	// Handle when the main window is resized
	void OnResize(wxSizeEvent &Event);

	// Micsellaneous functions
	wxString RunAndCatchOutput(wxString Comamnd);
	void OpenFile(wxString FileName);
	void OnClose(wxCloseEvent &Event);
	void UpdateFileNames();
	bool IsUpToDate();

private:
	wxSplitterWindow *  Splitter;
	wxStyledTextCtrl *  Scintilla;
	wxFileName          FileName;
	wxFileName	        Assembly;
	wxFileName	        Object;
	wxFileName	        Program;
	wxFileName          MainProgram;
	wxBoxSizer *        Sizer;
	wxTextCtrl *        Status;
	wxProcess *	        Compile;
	wxString            BaseTitle;
	int	                StatusHeight;
	wxString            Validator;

	DECLARE_EVENT_TABLE();		// Enables events like clicks
};

// Hooks up different types of events for the main window with
// the corresponding functions
BEGIN_EVENT_TABLE(kbMainWindow, wxFrame)
	// Events for the menu
	EVT_MENU(ID_FileNew, kbMainWindow::OnFileNew)
	EVT_MENU(ID_FileOpen, kbMainWindow::OnFileOpen)
	EVT_MENU(ID_FileSave, kbMainWindow::OnFileSave)
	EVT_MENU(ID_FileSaveAs, kbMainWindow::OnFileSaveAs)
	EVT_MENU(ID_FileExit, kbMainWindow::OnFileExit)

	EVT_MENU(ID_EditUndo, kbMainWindow::OnEditUndo)
	EVT_MENU(ID_EditRedo, kbMainWindow::OnEditRedo)
	EVT_MENU(ID_EditCut, kbMainWindow::OnEditCut)
	EVT_MENU(ID_EditCopy, kbMainWindow::OnEditCopy)
	EVT_MENU(ID_EditPaste, kbMainWindow::OnEditPaste)
	EVT_MENU(ID_EditDelete, kbMainWindow::OnEditDelete)
	EVT_MENU(ID_EditSelectAll, kbMainWindow::OnEditSelectAll)
	EVT_MENU(ID_EditGotoLine, kbMainWindow::OnEditGotoLine)

	EVT_MENU(ID_CompileBuildAndRun, 
	kbMainWindow::OnCompileBuildAndRun)
	EVT_MENU(ID_CompileCompile, kbMainWindow::OnCompileCompile)
	EVT_MENU(ID_CompileBuild, kbMainWindow::OnCompileBuild)
	EVT_MENU(ID_CompileRun, kbMainWindow::OnCompileRun)

	EVT_MENU(ID_HelpHelp, kbMainWindow::OnHelpHelp)
	EVT_MENU(ID_HelpAbout, kbMainWindow::OnHelpAbout)

	// Events for close & resize events
	EVT_CLOSE(kbMainWindow::OnClose)
	EVT_SIZE(kbMainWindow::OnResize)
END_EVENT_TABLE()

// wxWindows has no main or WinMain function to initialize the 
// application. Instead, it has a macro called IMPLEMENT_APP to
// do this for us, which basically calls kbApp::OnInit.
IMPLEMENT_APP(kbApp)

/*******************************************************************
kbApp::OnInit
	This function is called when our application has been 
	initialized. It serves as the main or WinMain function. 
	Here we create the splash screen (About box) and then the 
	main window for the IDE.
*******************************************************************/
bool kbApp::OnInit()
{
	wxBitmap KoolBLogo;
	wxFileName MainProgram;

	// Allows us to load the JPEG KoolB logo
	wxImage::AddHandler(new wxJPEGHandler);

	// The first command line parameter should always be the
	// complete path and filename of the program (ie the IDE)
	MainProgram = this->argv[0];
	
	// Create the main window, center it, but don't show it yet
	kbMainWindow * MainWindow = new kbMainWindow("KoolB IDE", 
		wxPoint(0,0), wxSize(640,460));
	MainWindow->Center();

	// Fake a File -> About event to show the KoolB splash screen
	wxCommandEvent FakeEvent;
	MainWindow->OnHelpAbout(FakeEvent);

	MainWindow->Show(true);

	// Open a file if passed through the command line
	if (this->argc == 2)
	{
		// The second command line parameter should be the file
		// But first convert it to a wxString
		wxFileName StartFile(this->argv[1]);
		
		// Silly Windows passes us the 8.3 DOS name, so convert it
		#ifdef Windows
			StartFile = StartFile.GetLongPath();
		#endif
		

		// Before attempting to open it, make sure it exists
		if (StartFile.FileExists())
			MainWindow->OpenFile(StartFile.GetFullPath());
	}

	// Return that the application has succesfully initialized
	return true;
}

/*******************************************************************
kbMainWindow::kbMainWindow
	Creates the main window for our Simply KoolB IDE. Creates 
	menus, code edit controls (through the Scintilla control),
	status controls, and other neat stuff. It ends by faking
	a File -> New event to reset the code edit.
*******************************************************************/
kbMainWindow::kbMainWindow(const wxString &Title, const wxPoint &Pos, 
               const wxSize &Size) : 
               wxFrame((wxFrame*)NULL, 0, Title, Pos, Size)
{
	// Initialize members to NULL or default values
	this->Splitter = NULL;     this->Scintilla = NULL;
	this->FileName.Clear();    this->MainProgram.Clear();
	this->Sizer = NULL;        this->Status = NULL;
	this->Compile = NULL;      this->BaseTitle = "Simply KoolB IDE";
	this->StatusHeight = 75;   this->Validator = "";

	// Retrieve the filename of the program (mainly so we can get
	// the home folder of the IDE
	MainProgram = wxGetApp().argv[0];

	// Load the KoolB icon from the file and set for this window
	// Linux uses the XPM format instead of icons
	wxIcon Icon;
	#ifdef Windows
		if (wxFileExists(MainProgram.GetPath() + "/KoolB.ico"))
			Icon.LoadFile(MainProgram.GetPath() + "/KoolB.ico", 
				wxBITMAP_TYPE_ICO);
	#endif
	#ifdef Linux
		if (wxFileExists(MainProgram.GetPath() + "/KoolB.xpm"))
			Icon.LoadFile(MainProgram.GetPath() + "/KoolB.xpm", 
				wxBITMAP_TYPE_XPM);
	#endif
	
	SetIcon(Icon);

	// Create the main File, Edit, Compile, and Help menus
	MakeMainMenu();

	// First, set the font to 10-point Courier New
	// Strangely enough, this size turns out to be about 12 on Linux
	#ifdef Windows
		wxFont font(10, wxMODERN, wxNORMAL, wxNORMAL, false,
			"Courier New");
	#endif
	#ifdef Linux
		wxFont font(12, wxMODERN, wxNORMAL, wxNORMAL, false, 
			"Courier");	
	#endif

	// Create a splitter to separate the Scintilla syntax 
	// highlighting control and the status edit control
	// Then add a sizer so we can re-size the splitter
	Sizer = new wxBoxSizer(wxHORIZONTAL);
	Splitter = new wxSplitterWindow(this, -1, wxPoint(0, 0), 
		wxSize(200,200), wxSP_3D);
	Sizer->Add(Splitter, 1, wxGROW, 0);	

	// Customize the Scintilla control for the KoolB language
	MakeScintilla(font);

	// Create the status as a readonly text edit control
	Status = new wxTextCtrl(Splitter, -1, "", wxPoint(0, 0), 
		wxDefaultSize, wxTE_MULTILINE);
	Status->SetFont(font);
	Status->SetEditable(false);

	// Finalize the splitters and add the Scintill and Status
	// controls
	Splitter->SetMinimumPaneSize(20);
	Splitter->SplitHorizontally(Scintilla, Status, 300);
	SetSizer(Sizer);

	// Fake a File -> New event when the main window starts up
	wxCommandEvent FakeEvent;
	OnFileNew(FakeEvent);
}

/*******************************************************************
kbMainWindow::MakeMenuBar
	Create menus for the IDE. These menus will be automatically
	deleted when the program ends
*******************************************************************/
	// 
void kbMainWindow::MakeMainMenu()
{
	wxMenu * FileMenu = new wxMenu;
	FileMenu->Append(ID_FileNew, "&New\tCtrl+N");
	FileMenu->AppendSeparator();
	FileMenu->Append(ID_FileOpen, "&Open...\tCtrl+O");
	FileMenu->AppendSeparator();
	FileMenu->Append(ID_FileSave, "&Save\tCtrl+S");
	FileMenu->Append(ID_FileSaveAs, "Save &As...\tCtrl+Shift+A");
	FileMenu->AppendSeparator();
	FileMenu->Append(ID_FileExit, "E&xit\tCtrl+Q");

	wxMenu * EditMenu = new wxMenu;
	EditMenu->Append(ID_EditUndo, "&Undo\tCtrl+Z");
	EditMenu->Append(ID_EditRedo, "&Redo\tCtrl+Y");
	EditMenu->AppendSeparator();
	EditMenu->Append(ID_EditCut, "Cu&t\tCtrl+X");
	EditMenu->Append(ID_EditCopy, "&Copy\tCtrl+C");
	EditMenu->Append(ID_EditPaste, "&Paste\tCtrl+V");
	EditMenu->Append(ID_EditDelete, "&Delete\tDel");
	EditMenu->AppendSeparator();
	EditMenu->Append(ID_EditSelectAll, "Select A&ll\tCtrl+A");
	EditMenu->AppendSeparator();
	EditMenu->Append(ID_EditGotoLine, "&Goto Line...\tCtrl+G");

	wxMenu * CompileMenu = new wxMenu;
	CompileMenu->Append(ID_CompileBuildAndRun,"Buil&d && Run\tF5");
	CompileMenu->AppendSeparator();
	CompileMenu->Append(ID_CompileCompile, "Compil&e\tCtrl+F7");
	CompileMenu->Append(ID_CompileBuild, "&Build\tF7");
	CompileMenu->AppendSeparator();
	CompileMenu->Append(ID_CompileRun, "Run\tCtrl+F5");

	wxMenu * HelpMenu = new wxMenu;
	HelpMenu->Append(ID_HelpHelp, "&Launch HTML Help!");
	HelpMenu->Append(ID_HelpAbout, "&About Simply KoolB");

	// Add the menubar with menus to the main window
	wxMenuBar * MenuBar = new wxMenuBar;
	MenuBar->Append(FileMenu, "&File");
	MenuBar->Append(EditMenu, "&Edit");
	MenuBar->Append(CompileMenu, "&Compile");
	MenuBar->Append(HelpMenu, "&Help");
	SetMenuBar(MenuBar);
}

/*******************************************************************
kbMainWindow::MakeScintilla
	Creates and customizes the syntax-highlighting Scintilla control
*******************************************************************/
void kbMainWindow::MakeScintilla(wxFont font)
{
	// Create the Scintilla syntax-higlighting control 
	Scintilla = new wxStyledTextCtrl(Splitter, -1, wxPoint(0,0), 
		wxDefaultSize,  0, "Scintilla");

	Scintilla->StyleSetFont(wxSTC_STYLE_DEFAULT, font);

	// Clear all the styles and then set each one individually
	// This allows us to set the different styles for 
	// keywords, comments, operators, etc
	Scintilla->StyleClearAll();
	Scintilla->StyleSetForeground(wxSTC_B_DEFAULT, *wxBLACK);
	Scintilla->StyleSetForeground(wxSTC_B_COMMENT, 
		wxColour(0, 129, 0));
	Scintilla->StyleSetForeground(wxSTC_B_NUMBER, 
		wxColour(131, 0, 131));
	Scintilla->StyleSetForeground(wxSTC_B_KEYWORD, *wxBLUE);
	Scintilla->StyleSetForeground(wxSTC_B_STRING, *wxRED);
	Scintilla->StyleSetForeground(wxSTC_B_PREPROCESSOR, *wxBLACK);
	Scintilla->StyleSetForeground(wxSTC_B_OPERATOR, *wxBLACK);
	Scintilla->StyleSetForeground(wxSTC_B_IDENTIFIER, *wxBLACK);
	Scintilla->StyleSetForeground(wxSTC_B_DATE, *wxBLACK);
	Scintilla->StyleSetBold(wxSTC_B_KEYWORD,  TRUE);
	Scintilla->StyleSetItalic(wxSTC_B_COMMENT,  TRUE);

	// Add the keywords Simply KoolB supports. Note that these
	// keywords MUST be lowercase or they will not take! Also,
	// even though REM is not really a keyword, it must be
	// added for it to work as a comment
	Scintilla->SetKeyWords(0, "rem cls print run sleep wait");

	// Finally, we set the language as VB Script, the closest
	// to KoolB Basic
	Scintilla->SetLexer(wxSTC_LEX_VBSCRIPT);

	// Set some other settings, such as tab stops and line numbers
	Scintilla->SetTabWidth(6);
	Scintilla->StyleSetForeground(wxSTC_STYLE_LINENUMBER, 
		wxColor("DARK GREY"));
	Scintilla->StyleSetBackground(wxSTC_STYLE_LINENUMBER, 
		wxColour("LIGHT GREY"));
	Scintilla->SetMarginWidth(0, 
		Scintilla->TextWidth(wxSTC_STYLE_LINENUMBER, "9999"));
	Scintilla->MarkerDefine(0, wxSTC_MARK_SHORTARROW, *wxBLUE, 
		*wxCYAN);

	// Show the Scintilla control
	Scintilla->Show(true);
}

/*******************************************************************
kbMainWindow::OnFileNew
	Clears out the Scintilla code edit control, the filename,
	and then sets the main window caption to reflect the changes.
*******************************************************************/
void kbMainWindow::OnFileNew(wxCommandEvent &Event)
{
	// Reset the Scintilla control & reset the filename
	Scintilla->ClearAll();
	Scintilla->EmptyUndoBuffer();
	FileName.Clear();
	SetTitle("Untitled - " + BaseTitle);
}

/*******************************************************************
kbMainWindow::OnFileOpen
	Asks the user to locate and open a new KoolB BASIC file 
	(with the .KoolB extension) so we can then use the OpenFile
	function to actually open the file and load it into Scintilla.
*******************************************************************/
void kbMainWindow::OnFileOpen(wxCommandEvent &Event)
{
	// Create a new open dialog 
	wxFileDialog * OpenDialog = new wxFileDialog(this, 
		"Open a Simply KoolB File", "", "", 
		"KoolB BASIC Files (*.KoolB)|*.KoolB"
		"|All Files (*.*)|*.*", wxOPEN, wxPoint(0,0));

	// Only open the file if the user actually selected a file
	if (OpenDialog->ShowModal() == wxID_OK)
		OpenFile(OpenDialog->GetPath());

	OpenDialog->Destroy();
}

/*******************************************************************
kbMainWindow::OpenFile
	Opens a new KoolB BASIC file (with the .KoolB extension) into
	the Scintilla code edit control and resets the window bar
*******************************************************************/
void kbMainWindow::OpenFile(wxString FileToOpen)
{
	// Only open the file if the name of the file is valid
	if (wxFileExists(FileToOpen))
	{
		// Store the name of the file for future use
		FileName = FileToOpen;

		// Load the file, reset undo, & change the window's caption
		Scintilla->LoadFile(FileName.GetFullPath());
		Scintilla->EmptyUndoBuffer();
		SetTitle(FileName.GetName() + " - " + BaseTitle);
	}
}

/*******************************************************************
kbMainWindow::OnFileSave
	Save the file to disk
*******************************************************************/
void kbMainWindow::OnFileSave(wxCommandEvent &Event)
{
	// If the filename to save the BASIC file to is bad, then
	// create a save dialog to let the user select where to
	// save the BASIC file to
	if (!FileName.IsOk())
	{
		wxFileDialog * SaveDialog = new wxFileDialog(this, 
			"Save a Simply KoolB File", "", "", 
			"KoolB BASIC Files (*.KoolB)|*.KoolB", wxSAVE, 
			wxPoint(0, 0));

		// Retrieve the filename if the user selects a filename
		if (SaveDialog->ShowModal() == wxID_OK)
			FileName = SaveDialog->GetPath();

		SaveDialog->Destroy();
	}

	// Save the file and set the caption of the main window 
	if (FileName.IsOk())
	{
		Scintilla->SaveFile(FileName.GetFullPath());
		SetTitle(FileName.GetName() + " - " + BaseTitle);
	}
}

/*******************************************************************
kbMainWindow::OnFileSaveAs
	Saves the file under a new name
*******************************************************************/
void kbMainWindow::OnFileSaveAs(wxCommandEvent &Event)
{
	// Save the old filename in case the user aborts
	wxFileName OldFileName = FileName;
	FileName.Clear();

	// Generate a save event. Since the FileName is bad, the IDE
	// will prompt the user to save the file under a new name
	OnFileSave(Event);

	// Revert back to the old filename if the user aborted 
	// (ie the file was not saved)
	if (!FileName.IsOk())
		FileName = OldFileName;
}

/*******************************************************************
kbMainWindow::OnFileExit
	Close down the IDE. If the current BASIC file is not saved,
	prompt the user.
*******************************************************************/
void kbMainWindow::OnFileExit(wxCommandEvent &Event)
{
	// Ask the user to save if the file is unsaved
	if (Scintilla && Scintilla->GetModify())
		if (wxMessageBox("Would you like to save before exiting?",
			"Save?", wxYES_NO) == wxYES)
			OnFileSave(Event);

		// This seems to speed up the deletion of the Scintilla
		Scintilla->EmptyUndoBuffer();
		
		// Safely delete the main window
		Destroy();
}

/*******************************************************************
kbMainWindow::OnClose
	Close down the IDE. Pretends the user clicked File -> Exit
*******************************************************************/
void kbMainWindow::OnClose(wxCloseEvent &Event)
{
	wxCommandEvent event;
	OnFileExit(event);
}

/*******************************************************************
kbMainWindow::OnEditXXX
	Because the Edit menu events are so commonplace, I will not
	discuss them indepth. You should be able to figure out what
	they do ;-)
*******************************************************************/
void kbMainWindow::OnEditUndo(wxCommandEvent &Event)
{	Scintilla->Undo();	}

void kbMainWindow::OnEditRedo(wxCommandEvent &Event)
{	Scintilla->Redo();	}

void kbMainWindow::OnEditCut(wxCommandEvent &Event)
{	Scintilla->Cut();	}

void kbMainWindow::OnEditCopy(wxCommandEvent &Event)
{	Scintilla->Copy();	}

void kbMainWindow::OnEditPaste(wxCommandEvent &Event)
{	Scintilla->Paste();	}

void kbMainWindow::OnEditDelete(wxCommandEvent &Event)
{	Scintilla->Clear();	}

void kbMainWindow::OnEditSelectAll(wxCommandEvent &Event)
{	Scintilla->SelectAll();	}

/*******************************************************************
kbMainWindow::OnEditGotoLine
	Displays a goto dialog box that lets the user type in a line
	number and press enter to go to it or dscape to cancel
*******************************************************************/
void kbMainWindow::OnEditGotoLine(wxCommandEvent &Event)
{
	// Clear the validator - doesn't seem to do much, but it is
	// necessary
	Validator = "";
	long Line;

	// Create a dialog to display a Go to dialog box
	wxDialog * GotoDialog = new wxDialog(this, -1, "Goto Line:", 
		wxPoint(-1, -1), wxSize(100,45), wxCAPTION);
	GotoDialog->SetClientSize(100, 20);
	GotoDialog->Center();

	// Create a text edit control in the goto box with a numeric
	// validator. This means only numbers can be entered into
	// the edit control
	wxTextCtrl LineBox (GotoDialog, -1, "", wxPoint(0, 0),
		wxSize(100, 20), 0, 
		wxTextValidator(wxFILTER_NUMERIC, &Validator));

	// To make the ENTER and ESCAPE keys work, we need to create
	// buttons with wxID_OK and wxID_CANCEL
	wxButton OK(GotoDialog, wxID_OK, "", wxPoint(0,0), wxSize(0,0));
	wxButton Cancel(GotoDialog, wxID_CANCEL, "", wxPoint(0,0), 
		wxSize(0,0));

	// The default is the OK button
	OK.SetDefault();

	// Show the dialog box and check the return value
	if (GotoDialog->ShowModal() == wxID_OK)
	{
		// If the user hit enter, retrieve the line number
		// and goto that line in the Scintill acontrol
		LineBox.GetValue().ToLong(&Line, 10);
		Scintilla->GotoLine(Line - 1);
	}

	GotoDialog->Destroy();
}

/*******************************************************************
kbMainWindow::OnCompileBuildAndRun
	Builds and runs the KoolB BASIC file (if necessary) and then
	runs it.
*******************************************************************/
void kbMainWindow::OnCompileBuildAndRun(wxCommandEvent &Event)
{
	// Update asm, object, and program filenames
	UpdateFileNames();
	
	// Do wee need to compile & build the file first?
	if (!IsUpToDate())
		OnCompileBuild(Event);

	// Run the program, but pass this Event so the ID is 
	// ID_CompileBuildAndRun
	OnCompileRun(Event);
}

/*******************************************************************
kbMainWindow::OnCompileBuild
	Actually builds the KoolB BASIC file. First, we compile the
	BASIC file (if necessary). Then we assemble and link the
	program.
*******************************************************************/
void kbMainWindow::OnCompileBuild(wxCommandEvent &Event)
{
	// Get the home folder for the IDE
	wxString Path = MainProgram.GetPath();
	wxString Output;
	Status->Clear();

	// Update asm, object, and program filenames
	UpdateFileNames();

	if (!IsUpToDate())
		OnCompileCompile(Event);

	// Only build the program if the compile succeeded
	if (Assembly.FileExists())
	{
		// Make sure the Netwide Assembler exists
		if (!wxFileExists(Path + "/nasm.exe"))
		{
			Status->AppendText("Error locating the "
				"Netwide Assembler!\r\n");
			return;
		}

		// Try to delete the program file. If this fails, that
		// means the program is running.
		if (Program.FileExists() && 
			!wxRemoveFile(Program.GetFullPath()))
		{
			Status->AppendText("Please close " + 
				Program.GetFullPath() + " before building!");
			return;
		}

		Status->AppendText("Generating program...\r\n");

		// Run the Netwide Assembler, catch the output, and 
		// display it. Use the right settings for the OS
		#ifdef Windows
			Output = RunAndCatchOutput("\"" + Path + "/nasm.exe\" "
				"-fwin32 \"" + Assembly.GetFullPath() + "\"");
		#endif
		#ifdef Linux
			Output = RunAndCatchOutput("\"" + Path + "/nasm.exe\" "
				"-felf \"" + Assembly.GetFullPath() + "\"");
		#endif

		Status->AppendText(Output);

		// Verify that NASM succeeded by generating an object file
		if (Object.FileExists())
		{
			#ifdef Windows
				// Verify that the Go Linker exists
				if (!wxFileExists(Path + "/golink.exe"))
				{
					Status->AppendText("Error locating the "
						"GoLink Linker!\r\n");
					return;
				}
	
				// Run the Go Linker, catch the output, and
				// display it
				Output = RunAndCatchOutput("\"" + Path + 
					"/golink.exe\" /CONSOLE /ni \"" + 
					Object.GetFullPath() + "\" msvcrt.dll");
			#endif
			#ifdef Linux
				// Run the Linux linker, catch the output, and
				// display it. Unfortunately, wxExecute doesn't
				// seem to want to preserve the parameters
				system("ld -s -dynamic-linker /lib/ld-linux.* -o \""
					+ Program.GetFullPath() + "\" \"" + 
					Object.GetFullPath() + "\" /lib/libc.*");
			#endif
			
			Status->AppendText(Output);
		}
		else
			Status->AppendText("Netwide Assembler failed!\r\n");

		// Make sure the program file exists and that this 
		// whole operation succeeded
		if (Program.FileExists())
			Status->AppendText("Done. " + FileName.GetName() +
			" built!\r\n");
		else
			Status->AppendText("Error! " + FileName.GetName() +
			" not built!\r\n");
	}

	// Move the cursor (empty selection) back to the top of the
	// status edit control so the whole message can be viewed
	Status->SetSelection(0,0);
}

/*******************************************************************
kbMainWindow::OnCompileCompile
	Runs the KoolB Compile to compile the current BASIC file 
	into assembly language. Unlike other IDE configurations,
	the Compile does not assemble the BASIC file to an object 
	file.
*******************************************************************/
void kbMainWindow::OnCompileCompile(wxCommandEvent &Event)
{
	// Get the home folder for the IDE
	wxString Path = MainProgram.GetPath();
	wxString Output;

	// Make sure the BASIC file is saved before compiling
	if (Scintilla->GetModify() || !FileName.FileExists())
		this->OnFileSave(Event);

	// Only compile if the file is saved
	if (FileName.IsOk())
	{
		// Clear the status edit control and the line markers
		// in the Scintill code control
		Status->Clear();
		Scintilla->MarkerDeleteAll(0);

		// Remove outdated files after updating the file names
		UpdateFileNames();
		wxRemoveFile(Assembly.GetFullPath());
		wxRemoveFile(Object.GetFullPath());
		wxRemoveFile(Program.GetFullPath());

		Status->SetValue("Compiling " + FileName.GetFullName() + 
			"...\r\n");

		// Make sure the KoolB Compiler exists
		if (!wxFileExists(Path + "/koolb.exe"))
		{
			Status->AppendText("Error locating Simply KoolB "
				"Compiler!\r\n");
			return;
		}

		// Run KoolB Compiler and catch & display the output
		Output = RunAndCatchOutput("\"" + Path + "/koolb.exe\" \"" +
			FileName.GetFullPath() + "\" -compile");
		Status->AppendText(Output);

		// Check to see if the KoolB Compiler reported an error
		int Pos = Output.find("Error on line ", 0);
		if (Pos != -1)
		{
			long Line;
			wxString StringLine;

			// Retrieve the part of the output containing the
			// line number
			StringLine = Output.substr(Pos + 
				strlen("Error on line "), 5);

			// Convert the line number to an integer, go to 
			// the line in the Scintilla control, and then
			// mark the line with a blue arrow (defined in
			// the constructor)
			StringLine.ToLong(&Line, 10);
			Scintilla->GotoLine(Line - 1);
			Scintilla->MarkerAdd(Line - 1, 0);
		}

		// If there is no output (the KoolB compiler only 
		// outputs errors, so no output is good) and this
		// event was generated by the Compile -> Compile 
		// menu (not the Build and Run menus)
		if (Output == "" && Event.GetId() == ID_CompileCompile)
			Status->AppendText("Compile Done!\r\n");
	}

	// Move the cursor (empty selection) back to the top of the
	// status edit control so the whole message can be viewed
	Status->SetSelection(0,0);
}

/*******************************************************************
kbMainWindow::OnCompileRun
	Runs the built KoolB program. If it the BASIC file has not
	been built yet, build the program first.
*******************************************************************/
void kbMainWindow::OnCompileRun(wxCommandEvent &Event)
{
	// Update program file name so we know what to run
	UpdateFileNames();
	
	// Make sure the program file is up to date
	// Also, if this event has not been generated
	// by the Build & Run menu, Build & Run the program
	// This check is done to make sure that we don't
	// go into an infinite loop if a build fails
	if (!IsUpToDate() && Event.GetId() == ID_CompileRun)
	{
		Event.SetId(ID_CompileBuildAndRun);
		OnCompileBuildAndRun(Event);
	}
	// If the program exists, then run it
	if (wxFileExists(Program.GetFullPath()))
	{
		#ifdef Windows
			wxExecute(Program.GetFullPath(), wxEXEC_NOHIDE | 
				wxEXEC_ASYNC);
		#endif
		#ifdef Linux // On linux, we have to use a console like xterm
			wxExecute("xterm -e \"" + Program.GetFullPath()	+ "\"");
		#endif
	}
}

/*******************************************************************
kbMainWindow::OnHelpHelp
	Launch HTML help if available
*******************************************************************/
void kbMainWindow::OnHelpHelp(wxCommandEvent &Event)
{
	// Help is located in Help\index.html
	wxString helpFile = MainProgram.GetPath() + "/Help/index.html";
	wxString Command;
	wxMimeTypesManager MimeManager;

	// Attempt to launch the help file
	if (wxFileExists(helpFile))
	{
		wxFileType * HTML = 
			MimeManager.GetFileTypeFromExtension("html");

		if (HTML)
		{
			Command = HTML->GetOpenCommand(helpFile);

			if (Command != "")
				wxExecute(Command);
			return;
		}
		
		wxMessageBox("I cannot directly open the help files. Please"
			" open \"" + helpFile + "\" manually. Thank you!");
		
		return;
	}

	wxMessageBox("Help is unavailable!", "Error!");
}

/*******************************************************************
kbMainWindow::OnHelpAbout
	Creates an AboutWindow and shows it.
*******************************************************************/
void kbMainWindow::OnHelpAbout(wxCommandEvent &Event)
{
	// Create and load the KoolB logo if it exists
	// By default, we want the splash screen to stay on the screen
	wxBitmap KoolBLogo;
	long TimeoutFlag = wxSPLASH_NO_TIMEOUT;

	if (wxFileExists(MainProgram.GetPath() + "/KoolB.jpg"))
		KoolBLogo.LoadFile(MainProgram.GetPath() + "/KoolB.jpg",
			wxBITMAP_TYPE_JPEG);

	// However, if this event didn't come from Help -> About, 
	// show the KoolB logo as a brief splash screen
	if (Event.GetId() != ID_HelpAbout)
		TimeoutFlag = wxSPLASH_TIMEOUT;

	// Create the splash screen. A click will close the box.
	// Also, if you notice that we create this splash screen
	// with new, but do not have to free it, wxWindows will
	// do this for us automatically. If we try, the splash
	// screen only pops up for a split second
	wxSplashScreen * AboutKoolB = new wxSplashScreen(KoolBLogo,
		wxSPLASH_CENTRE_ON_PARENT | TimeoutFlag, 2000, 
		this, -1, wxDefaultPosition, wxDefaultSize, 
		wxSIMPLE_BORDER | wxSTAY_ON_TOP);
		
	AboutKoolB->Show();
}

/*******************************************************************
kbMainWindow::OnResize
	Responds to when the user resizes the main window so the
	splitter does not grow too much and swallow the Scintilla
	control.
*******************************************************************/
void kbMainWindow::OnResize(wxSizeEvent &Event)
{
	// To resize the splitter, we main window size
	wxSize Size = GetClientSize();

	if (Splitter)
	{
		// Resize splitter window & set actual splitter position
		Splitter->SetSize(Size.GetWidth(), Size.GetHeight());
		Splitter->SetSashPosition(Size.GetHeight() - 
			StatusHeight, true);
	}
}

/*******************************************************************
kbMainWindow::RunAndCatchOutput
	Runs a command (like a the KoolB Compiler) and then returns
	any output from that program. Blocks until done.
*******************************************************************/
wxString kbMainWindow::RunAndCatchOutput(wxString Command)
{
	wxString Output; 	
	wxArrayString OutputArray, ErrorArray;
	int i;

	// Execute the command and store the output lines
	// in the string arrays so we can retrieve them later
	wxExecute(Command, OutputArray, ErrorArray);

	int Count = OutputArray.GetCount();
	for (i = 0; i < Count; i++ )
		Output += OutputArray[i] + "\r\n";

	Count = ErrorArray.GetCount();
	for (i = 0; i < Count; i++ )
		Output += ErrorArray[i] + "\r\n";

	return Output;
}

/*******************************************************************
kbMainWindow::UpdateFileNames
	Updates the filenames for the KoolB program's assembly language,
	object, and program files. Note the difference between the OSes
*******************************************************************/
void kbMainWindow::UpdateFileNames()
{
	// Create filenames for the generated assembly language,
	// object, and program files
	Assembly = FileName, Object = FileName, Program = FileName;
		
	Assembly.SetExt("asm");
	#ifdef Windows
		Object.SetExt("obj");
		Program.SetExt("exe");
	#endif
	#ifdef Linux
		Object.SetExt("o");
		Program.SetExt("");
	#endif
}

/*******************************************************************
kbMainWindow::UpdateFileNames
	Checks to see if the BASIC file needs to be re-compiled
*******************************************************************/
bool kbMainWindow::IsUpToDate()
{
	// The BASIC file is not up-to-date if:
	// 1. The BASIC file is modified and unsaved
	// 2. The assembly language file does not exist
	// 3. The assembly file is older than the BASIC file
	if (Scintilla->GetModify() || !Assembly.FileExists() || 
		Assembly.GetModificationTime() < 
		FileName.GetModificationTime() || 
		!wxFileExists(FileName.GetFullPath()))
		return false;
	else
		return true;
}