#Simply KoolB (learning edition)#

Just about the time I was finishing KoolB, I had a crazy idea to go back and make the most basic compiler and IDE I could with under 1,000 lines of code. The result was Simply KoolB, a heavily commented 7 command BASIC -> assembly compiler with IDE. It should be fairly instructional to somebody interested in building a compiler.