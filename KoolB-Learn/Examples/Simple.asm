EXTERN	system
EXTERN	puts
EXTERN	getchar
EXTERN 	exit
EXTERN	_sleep

section .text

global	START
START:	
	PUSHA	
	PUSH	EBP
	MOV	EBP, ESP
	PUSH	String_1
	CALL	puts
	POP	EAX
	MOV	EAX,4
	MOV	EDX,1000
	MUL	EDX
	PUSH	EAX
	CALL	_sleep
	POP	EAX
	PUSH	ClearScreen
	CALL	system
	POP	EAX
	PUSH	String_2
	CALL	puts
	POP	EAX
	PUSH	String_3
	CALL	system
	POP	EAX
	PUSH	String_4
	CALL	puts
	POP	EAX
	PUSH	String_5
	CALL	puts
	POP	EAX
	CALL	getchar
	PUSH	ClearScreen
	CALL	system
	POP	EAX
	PUSH	String_6
	CALL	puts
	POP	EAX
	PUSH	String_7
	CALL	puts
	POP	EAX
	PUSH	String_8
	CALL	puts
	POP	EAX
	CALL	getchar
	MOV	ESP, EBP
	POP	EBP
	POPA	
	PUSH	0
	CALL	exit
	
	
section .data

ClearScreen 		db "cls",0
String_1 		db "            Welcome to a demo of Simply KoolB by Brian C. Becker!",0
String_2 		db "Printing the folders in your C folder:",0
String_3 		db "DIR C:\ /a:d /w",0
String_4 		db "",0
String_5 		db "Press ENTER to view a goodbye:",0
String_6 		db "Goodbye!",0
String_7 		db "",0
String_8 		db "Press ENETR to exit...",0
