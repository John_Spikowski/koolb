-----------------------------------------------------------------------
About KoolB by Brain C. Becker
-----------------------------------------------------------------------
KoolB is a free, open-source Windows and Linux BASIC compiler written 
in C++. It works by translating your BASIC source code to assembly 
language and then uses the Netwide Assembler and a linker to produce 
a native Windows or Linux executable. 

It has many of the features found in QBASIC and can be used as a 
learning tool (by studying the source) or for simple programming
projects.

-----------------------------------------------------------------------
KoolB's Features:
-----------------------------------------------------------------------
 - Completely cross-platform for both Windows & Linux
 - Produces small and fast programs (minimal program is 3KB)
 - Handles simple data types (Integer, Double, String)
 - Handles arrays of simple data types
 - Handles UDTs of simple data types
 - Full support for both string and numeric expressions
 - Primitive console Input & Output
 - Boolean expressions (And, Or, Not)
 - Relational operators (=, <>, etc)
 - If...Then...ElseIf...Else...End If statements
 - While...Wend loops
 - User defined functions & subs
 - Importing external functions (WinAPI & custom DLLs)
 - Directives ($Const, $Define, $IfDef, $End If, etc)
 - $Asm directive to use assembly language inline
 - Support for GUI, Console, and DLL programs
 - Optimization to remove unused functions in libraries
 - Automatic compression to your program or DLL